# Warhammer Online

- https://www.returnofreckoning.com/
- fun things
  - [colored text in chat](https://www.returnofreckoning.com/forum/viewtopic.php?f=8&t=13575#p148909)
  - [bestial trophy looks](https://imgur.com/a/XHhJO)
- Optimizing game
  - https://www.returnofreckoning.com/forum/viewtopic.php?f=5&t=20075
  - https://www.returnofreckoning.com/forum/viewtopic.php?f=4&t=13977
- [addons](https://tools.idrinth.de/addons/)

## RoR on linux

```bash
sudo pacman -Syu wine winetricks wine-mono wine_gecko
winetricks d3dx9 dotnet40
winecfg # select windows 7
```

### Addon creation

- [tutorial in french](http://forums.jeuxonline.info/showthread.php?t=943627)
- [wardiff](https://www.google.com/search?q=wardiff+site:https://searchcode.com&ie=utf-8&oe=utf-8)
- extracting interface from the game: #TODO
- (DEAD) https://code.google.com/archive/p/devbar/

#### API

- http://warhammerfantasy.wikia.com/wiki/API
- http://warapi.wikia.com/wiki/WAR_API

## Gear

- [set gear stats/bonuses sheet](https://docs.google.com/spreadsheets/d/1EFNAP_IYPHAYAEL1IbbxstkkMtN6VIjK5l9Z8tg2IAQ/edit)

### Gear prices

#### SC armor

| lvl | type | name       | price                          |
| --- | ---- | ---------- | ------------------------------ |
| 8   | sc   | barggart   | 53                             |
| 17  | sc   | challenger | 89                             |
| 29  | sc   | duelist    | 165                            |
| 35  | sc   | merc       | 410 = 150 + 115 + 75 + 37 + 33 |

#### RVR armor

| lvl | type | name        | price                         |
| --- | ---- | ----------- | ----------------------------- |
| 8   | rvr  | decimator   | 12                            |
| 17  | rvr  | obliterator | 33                            |
| 29  | rvr  | devastator  | 81                            |
| 35  | rvr  | annihilator | 259 = 100 + 65 + 45 + 26 + 23 |

### SC weapons

| lvl | type | name    | price 1h/2h |
| --- | ---- | ------- | ----------- |
| 9   | sc   | weapons | 11/22       |
| 20  | sc   | weapons | 17/34       |
| 24  | sc   | weapons | 19/38       |
| 29  | sc   | weapons | 30/60       |
| 34  | sc   | weapons | 70/140      |
| 39  | sc   | weapons | 200/400     |

## crafting

### Apothecary

| skill | lvl | stabs                            |
| ----- | --- | -------------------------------- |
| 1     | 5   | 1 stab                           |
| 25    | 10  | 1 stab                           |
| 50    | 15  | 1 stab                           |
| 75    | 20  | 1 stab                           |
| 100   | 25  | 2 stabs (x2) OR blue bottle (x5) |
| 125\* | 30  | 2 stabs                          |
| 150\* | 32  | 2 stabs                          |
| 175   | 35  | 2 stabs                          |
| 200   | 38  | 2 stabs AND blue bottle          |

### Talisman making

+24 tali: 39 + 13 = 52 power

- 18 power - tali
- 9 power - flawless gold dust
- 9 power - harmonious essence
- 9 - forgotten soldier's insignia / bitterstone arrowhead
- 18 + 27 = 45 -> 22 tali

fragments

- roaring bister core - bs
- gust of cerulean gloom - willpower
- lethal perse ice - int

1 simplest tali costs: 2 + 0.2 + 0.02 + 0.3 = 2.52 silver

zero-profit (10% auction cut): 2.52/0.9 = 2.8 silver

## Epic quests links

```
<LINK data="QUEST:36150" text="[Unmasking the Enemy]" color="242,169,53"> <LINK data="QUEST:31149" text="[Faith is Madness]" color="242,169,53"> <LINK data="QUEST:10890" text="[Battle for Remembrance]" color="242,169,53">

<LINK data="QUEST:45338" text="[Ebon Flame]" color="242,169,53"> <LINK data="QUEST:41133" text="[Annihilation]" color="242,169,53"> <LINK data="QUEST:20894" text="[Time to Waaagh]" color="242,169,53">
```

## Macros

Timer (requires standard unitframes enabled)

```lua
/script local t,s,p,l=5,220,PlayerWindow,"PlayerWindowRvRFlagCountDown"local u=p.Update p.OnStartRvRFlagTimer()p.Update(-t+10) p.Update=function(e)u(e)if LabelGetText(l)==L"1"then WindowSetShowing(l, false)p.Update=u PlaySound(s) end end
```

Join all scenario queues

```lua
/script BroadcastEvent(SystemData.Events.INTERACT_JOIN_SCENARIO_QUEUE_ALL)
```

Group join all scenario queues

```lua
/script BroadcastEvent(SystemData.Events.INTERACT_GROUP_JOIN_SCENARIO_QUEUE_ALL )
```

Say target name

```lua
/script local name = TargetInfo:UnitName("selffriendlytarget");SendChatText( L"/s I see "..name, L"" )
/script local name = TargetInfo:UnitName("selfhostiletarget");SendChatText( L"/s I see "..name, L"" )
```

Switch tactics

```lua
/script TacticsEditor.OnSetMenuSelectionChanged (1)
/script TacticsEditor.OnSetMenuSelectionChanged (2)
/script TacticsEditor.OnSetMenuSelectionChanged (3)
/script TacticsEditor.OnSetMenuSelectionChanged (4)
/script TacticsEditor.OnSetMenuSelectionChanged (5)
/script TacticsEditor.OnSetMenuSelectionChanged (6)
```

Search for guilds more precisely (e.g. > 1 online players, not just > 0 and > 10; need to have guild search window open)

```lua
/script local mintotal, minonline = 1, 1 GuildRecruitmentSearch( 1, 1, 4, false, mintotal, minonline, 0 )

-- GuildRecruitmentSearch( playStyle, atmosphere, searchType, isRecruitingPlayer, minTotalPlayers, minOnlinePlayers, minGuildRank )
--     playStyle - flagId (number. 1 - any)
--     atmosphere - flagId (number. 1 - any)
--     searchType - flagId (number. 2 - join/form alliance, 3 - new alliance guilds, 4 - players, 5 - all)
--     isRecruitingPlayer - bool (recruiting your carreer)
--     minTotalPlayers - number
--     minOnlinePlayers - number
--     minGuildRank - number
```
