# Lutris scripts for RoR

You can install local scripts with with: `lutris -i script.yaml`.

Here are commands to download a script into the current folder and run it:

```sh
# replace ror-copy.yaml with the one you want
f=ror-copy.yaml
curl "https://gitlab.com/cupnoodles14/war-ui/raw/master/lutris/$f" >"$f"
lutris -i "$PWD/$f"
```

- `ror-copy.yaml` is recommended if you want to keep seeding the torrent.
- `ror-download.yaml` takes a loooong time and will probably lose progress if interrupted.

I could probably put it all into one script and use `execute:` and `input_menu:` to choose what to do but i'm not sure if it's "Lutris-way".

## References

- [Writing installers](https://github.com/lutris/lutris/blob/master/docs/installers.rst)
