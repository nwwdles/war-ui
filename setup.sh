#!/usr/bin/env bash
set -x
set -euo pipefail

link() {
    SRC=$1
    DEST=$2
    if [ -d "$SRC" ]; then
        if [ -e "$DEST" ]; then
            mv -Ti "$DEST" "$DEST-old"
        fi
        ln -sfnbT "$SRC" "$DEST"
    else
        echo "Source dir not found: $SRC"
        return 1
    fi
}

WAR_DIR=${WAR_DIR:-$(yad --file --directory --title="Choose Warhammer Online directory")}
REPO_DIR=$PWD

link "$REPO_DIR/Interface" "$WAR_DIR/Interface"
link "$REPO_DIR/user" "$WAR_DIR/user"
ln -sfnT "$REPO_DIR/watchsc" "$WAR_DIR/watchsc"
