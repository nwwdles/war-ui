Title: PotionBar v7.2

Author: Felyza and Sihirbaz - Original authors
				Alpha_Male (alpha_male@speakeasy.net) - Current Maintainer - New Features and Fixes
				Wothor - Optimizations and fixes

Description: Dedicated customizable actionbar for potions that automatically updates itself based on player inventory.

Features: * Support for all potion categories and types
					* Support for all liniments
					* Support for unknown and special potions
					* Automatic Updating based on current potion and liniment inventory in bag
					* Customizable PotionBar size and position
					* Configurable User Options
						* PotionBar display direction
						* Show/Hide activator nub
						* Autohide PotionBar
						* PotionBar Scale
						* PotionBar Opacity
						* Potion/Liniment Count display
					* Configurable Potion Options
						* Quick Actions configure potion and lininment settings
						* Potion and liniment types to display
						* Display position of potions and liniments
						* Stack by potion type
						* Potion usage and sort priority (strongest potion, weakest potion, etc...)
					* LibSlash support for chat line options
					* Multiple Language Support (currently includes English, German, Spanish, Italian, and French)

Files:	\language\DE.lua
				\language\EN.lua
				\language\FR.lua
				\language\IT.lua
				\language\SP.lua
				\settings\Settings.lua
				\settings\Settings.xml
				\source\Data.lua
				\source\Floating.lua
				\source\Floating.xml
				\source\Main.lua
 		 		\PotionBar.mod
 		 		\PotionBar_Install.txt 
 		 		\PotionBar_Readme.txt (the file you are reading)

Version History: 7.2 - Bug Fixes
										 - Fix for macros and script functionality
										 - Fix that allows potions and liniments being used via macro/script to properly send message to chat
										 - Liniment UniqueID range fix
								 7.1 - New Features and Bug Fixes
										 - Includes all of Wothor's v6.48 branch:
										 	 - Optimizations
										 	 - Fixes for RvR currency pots (now separate and configurable)
										 	 - Liniment localization fixes
										 - Added support for 1.3.1 WARInfo Categories and Careers (PotionBar.mod)
										 - Added support for version info (PotionBar.mod)
										 - Updated Addon version information for 1.4.5
										 - Directory structure and file reorganization - THIS REQUIRES DELETING OLD DIRECTORY WHEN INSTALLING!
										 - Updated Versioning scheme with initialization output to the debug and chat windows.
										 - Player battle level now taken into account, allowing use of higher level potions when players are bolstered in RvR
										 - Player renown rank now taken into account, allowing use proper filtering and use of RvR potions
										 - System event for PLAYER_BATTLE_LEVEL_UPDATED now taken into account to allow proper updating
										 - System event for PLAYER_RENOWN_RANK_UPDATED now taken into account to allow proper updating
										 - Created system event queue to prevent race conditions or multiple updates triggering
										 - Added UnRegisterEventHandlers to PotionBar.Shutdown
										 - Added the following potions categories and types:
										 	 - Elixir of Tahoth
										 	 - Elixir of Ptra
										 	 - Speed of the Blowing Sand
										 	 - Zephyr Tonic
										 - Added the following liniments:
										 	 - Liniment of Boundless Sight     
										 	 - Liniment of Immutable Defense   
										 	 - Liniment of the Inexorable Aegis    
										 	 - Liniment of Peerless Defense    
										 	 - Liniment of the Quickened Blades    
										 	 - Liniment of Swift Tergiversation
										 - PotionBar Config Window:
										   - Reorganized potions and liniments due to new additions
										   - Config Window can no longer be dragged off screen
										   - Config Window resized for new functionality and organization
										   - Added Horizontal and Vertical separators and adjusted spacing for all UI elements
										   - Added Quick Actions pull down, allows global settings changes and reset to default for all potions and liniments
										   - New Save settings button and functionality
										   - Repositioned Defaults settings button
										   - About button repositioned to window frame bottom with other buttons
										   - About window reworked to be a proper style window and includes previous information and new contributors information
										   - Settings window must be manually closed (allows for continued editing even after hitting Save)
										   - Fixed Floating Activator not hiding when related setting selected
										   - Scale Slider
										     - Added tick marks and associated number value labels
										   - Opacity Slider
										     - Added tick marks and associated number value labels
										   - Adjusted vertical spacing on potion and liniment display elements
										   - All options are now grayed out or subdued if potions or liniments are set to not display 
										   - General capitalization fixes for option labels (English)
										 - Localization files updated due to changes and additions
										 - Script cleanup and optimizations (.lua and .xml files)
										 - Script reorganization and additional comments (.lua and .xml files)
										 - Readme and install file additions
								 7.0 - Internal test build containing all the 7.1 features.
								 6.4 - New Features
										 - Slightly extended support for macros.
								 6.3 - New Features
								 		 - Fixed using potions within macros.
								 6.2 - New Features
								 		 - PotionBar can now handle different cooldown timers for same types of potions.
								 6.1 - New Features and Bug Fixes
								 		 - Fixed scaling of the button icons.
								 		 - Changed internal template name to support having two different named PotionBars (see little howto in the description of PotionBar).
								 6.0 - New Features and Bug Fixes
								 		 - Added support for 1.3.
										 - The small little circular button is better positionable and even hideable.
								 		 - PotionBar can now be configured that potions of the same kind but with different name can be added into the bar.
								 		 - Changed slash commands a little bit so they can be used with the new feature.
								 		 - A few bug fixes in regards to unknown potions.
								 		 - Adaptions for the new german names for the hybrid potions.


Supported Versions: Warhammer Online v1.4.5

Dependencies: PotionBar 5.3 and above require LibSlash (http://war.curseforge.com/addons/libslash/)

Addon Compatability: None

Future Features: Continued updates for multiple language support

Known Issues:  None

Additional Credits: None

Special Thanks:	EA/Mythic for a great game and for releasing the API specs
							  The War Wiki (www.thewarwiki.com) for being a great source of knowledge for WAR mod development
								www.curse.com and www.curseforge.com for hosting WAR mod files and projects
							  Trouble INC guild of Badlands for their support

License/Disclaimers: This addon and it's relevant parts and files are released under the 
										 GNU General Public License version 3 (GPLv3).

Additional Notes: For additional help and support with PotionBar please visit http://www.curse.com/addons/war/potionbar