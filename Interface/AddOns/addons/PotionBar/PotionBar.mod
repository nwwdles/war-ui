<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >

	<UiMod name="PotionBar" version="7.2" date="04/01/2012" autoenabled="true">

		<Author name=" Sihirbaz | Wothor (Optimizations) | Alpha_Male" email="alpha_male@speakeasy.net" />
    <Description text="Dedicated customizable actionbar for potions that automatically updates itself based on player inventory."/>
		<VersionSettings gameVersion="1.4.5" windowsVersion="1.0" savedVariablesVersion="1.0" />

		<WARInfo>
			<Categories>
				<Category name="ITEMS_INVENTORY" />
				<Category name="BUFFS_DEBUFFS" />
				<Category name="CAREER_SPECIFIC" />
				<Category name="OTHER" />
			</Categories>
			<Careers>
				<Career name="BLACKGUARD" />
				<Career name="WITCH_ELF" />
				<Career name="DISCIPLE" />
				<Career name="SORCERER" />
				<Career name="IRON_BREAKER" />
				<Career name="SLAYER" />
				<Career name="RUNE_PRIEST" />
				<Career name="ENGINEER" />
				<Career name="BLACK_ORC" />
				<Career name="CHOPPA" />
				<Career name="SHAMAN" />
				<Career name="SQUIG_HERDER" />
				<Career name="WITCH_HUNTER" />
				<Career name="KNIGHT" />
				<Career name="BRIGHT_WIZARD" />
				<Career name="WARRIOR_PRIEST" />
				<Career name="CHOSEN" />
				<Career name="MARAUDER" />
				<Career name="ZEALOT" />
				<Career name="MAGUS" />
				<Career name="SWORDMASTER" />
				<Career name="SHADOW_WARRIOR" />
				<Career name="WHITE_LION" />
				<Career name="ARCHMAGE" />
			</Careers>
		</WARInfo>

			<Dependencies>
      	<Dependency name="EASystem_Utils"/>
        <Dependency name="LibSlash"/>
      </Dependencies>

      <Files>
				<File name="source/Data.lua"/>
	      <File name="source/Floating.lua"/>
	      <File name="source/Floating.xml"/>
	      <File name="source/Main.lua"/>
	      <File name="settings/Settings.lua"/>
	      <File name="settings/Settings.xml"/>
	      <File name="language/DE.lua"/>
	      <File name="language/EN.lua"/>
	      <File name="language/FR.lua"/>
	      <File name="language/IT.lua"/>
	      <File name="language/SP.lua"/>
      </Files>

      <OnInitialize>
      	<CallFunction name="PotionBar.Initialize"/>
      </OnInitialize>

      <OnUpdate>
	      <CallFunction name="PotionBar.UpdateCooldowns"/>
	      <CallFunction name="PotionBarFloating.RefreshButtons"/>
      </OnUpdate>

      <OnShutdown>
				<CallFunction name="PotionBar.Shutdown"/>
      </OnShutdown>

      <SavedVariables>
	      <SavedVariable name="PotionBar.Settings"/>
			</SavedVariables>

	</UiMod>

</ModuleFile>