if not PotionBar then PotionBar = {} end
if not PotionBarData then PotionBarData = {} end
if not PotionBarSettings then PotionBarSettings = {} end

PotionBar.English = {}

function PotionBar.getValuesEN(name, description)
    local potionV, potionD, potionT, potionC, potionTy = 0, 0, 0, 0, 0
    if string.find(description, 'Instantly restores %d+') then
        potionV = PotionBar.getNumbers(description, 1)
        if string.find(description, 'Action Points') then
            potionTy = PotionBar.Type.AP
        elseif string.find(description, 'health') then
            potionTy = PotionBar.Type.HEAL
        end
    elseif string.find(description, 'Heals for %d+ every %d+ seconds, for %d+ seconds.') then
        potionV, potionT, potionD = PotionBar.getNumbers(description, 3)
        potionV = potionV * (potionD/potionT)
        potionTy = PotionBar.Type.REGEN
    elseif string.find(description, 'Gradually restores %d+ health over %d+ seconds.') then
        potionV, potionD = PotionBar.getNumbers(description, 2)
        potionTy = PotionBar.Type.REGEN
    elseif string.find(description, 'Absorbs %d+ damage over the next %d+ seconds.') then
        potionV, potionD = PotionBar.getNumbers(description, 2)
        potionTy = PotionBar.Type.SHIELD
    elseif string.find(description, 'Surrounds you with a magical barrier for up to %d+ seconds which will absorb up to %d+ damage.') then
        potionD, potionV = PotionBar.getNumbers(description, 2)
        potionTy = PotionBar.Type.SHIELD
    elseif string.find(description, 'Increases [%a%s]+ by %d+ for %d+ minutes.') then
        potionV, potionD = PotionBar.getNumbers(description, 2)
        potionD = potionD * 60
        if string.find(description, 'Strength') then
            potionTy = PotionBar.Type.STRENGTH
        elseif string.find(description, 'Intelligence') then
            potionTy = PotionBar.Type.INTELLIGENCE
        elseif string.find(description, 'Ballistic Skill') then
            potionTy = PotionBar.Type.BALLISTIC
        elseif string.find(description, 'Toughness') then
            potionTy = PotionBar.Type.TOUGHNESS
        elseif string.find(description, 'Spirit Resistance') then
            potionTy = PotionBar.Type.SPIRIT
        elseif string.find(description, 'Elemental Resistance') then
            potionTy = PotionBar.Type.ELEMENTAL
        elseif string.find(description, 'Corporeal Resistance') then
            potionTy = PotionBar.Type.CORPOREAL
        elseif string.find(description, 'Armor') then
            potionTy = PotionBar.Type.ARMOR
        elseif string.find(description, 'Willpower') then
            potionTy = PotionBar.Type.WILLPOWER
        end
    elseif string.find(description, 'pikes cover your') then
        potionD, potionV = PotionBar.getNumbers(description, 2)
        potionD = potionD * 60
        potionTy = PotionBar.Type.THORNS
    elseif string.find(description, 'A volatile brew that burns') then
        potionV, potionT, potionD = PotionBar.getNumbers(description, 3)
        potionV = potionV * (potionD/potionT)
        potionTy = PotionBar.Type.DOT
    elseif string.find(description, 'A burning mixture which') then
        potionV, potionD = PotionBar.getNumbers(description, 2)
        potionTy = PotionBar.Type.DOT
    elseif string.find(description, 'A vile mixture that will burn') then
        potionT, potionD, potionV = PotionBar.getNumbers(description, 3)
        potionV = potionV * potionD
        potionTy = PotionBar.Type.AOEDOT
    elseif string.find(description, 'A mixture which will burst') then
        potionD, potionV, potionT, potionC = PotionBar.getNumbers(description, 4)
        potionV = potionV * (potionD/potionT)
        potionTy = PotionBar.Type.AOEDOT
    elseif string.find(description, 'A viscous concoction that coats') then
        potionT, potionD, potionV = PotionBar.getNumbers(description, 3)
        potionTy = PotionBar.Type.SNARE
    elseif string.find(description, 'A thick mixture which will cover') then
        potionD, potionC, potionV = PotionBar.getNumbers(description, 3)
        potionTy = PotionBar.Type.SNARE
    elseif string.find(description, 'A fiery draught that can') then
        potionC, potionV, potionD = PotionBar.getNumbers(description, 3)
        potionTy = PotionBar.Type.FIREBREATH
--    elseif string.find(name, 'Liniment of Inspirational Winds') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.INSPIRATIONALWINDS
--    elseif string.find(name, 'Liniment of Savage Vigor') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.SAVAGEVIGOR
--    elseif string.find(name, 'Liniment of Unfettered Zeal') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.UNFETTEREDZEAL
--    elseif string.find(name, 'Liniment of War.*Blood') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.WARBLOOD
--    elseif string.find(name, 'Liniment of War.*Demise') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.WARDEMISE
--    elseif string.find(name, 'Liniment of War.*Fervor') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.WARFERVOR
--    elseif string.find(name, 'Liniment of War.*Genius') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.WARGENIUS
--    elseif string.find(name, 'Liniment of War.*Hunger') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.WARHUNGER
--    elseif string.find(name, 'Liniment of War.*Mercy') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.WARMERCY
--    elseif string.find(name, 'Liniment of the Eternal Hunt') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.ETERNALHUNT
--    elseif string.find(name, 'Liniment of the Inevitable Tempest') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.INEVITABLETEMPEST
--    elseif string.find(name, 'Liniment of the Tolling Bell') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.TOLLINGBELL
--    elseif string.find(name, 'Liniment of Boundless Sight') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.BOUNDLESSSIGHT
--    elseif string.find(name, 'Liniment of Immutable Defiance') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.IMMUTABLEDEFIANCE
--    elseif string.find(name, 'Liniment of the Inexorable Aegis') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.INEXORABLEAEGIS
--    elseif string.find(name, 'Liniment of Peerless Defense') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.PEERLESSDEFENSE
--    elseif string.find(name, 'Liniment of Quickened Blades') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.QUICKENEDBLADES
--    elseif string.find(name, 'Liniment of Swift Tergiversation') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.SWIFTTERGIVERSATION
    elseif string.find(name, 'Nepenthean Tonic') then
        potionV, potionD = 10000, 1
        potionTy = PotionBar.Type.NEPENTHEANTONIC
    elseif string.find(name, 'Zephyr Tonic') then
    		potionV, potionD = PotionBar.getNumbers(description, 2)
        potionD = potionD * 60
        potionTy = PotionBar.Type.ZEPHYRTONIC
    elseif string.find(name, 'Elixir of Tahoth') then
    		potionD = PotionBar.getNumbers(description, 1)
        potionD = potionD * 60
        potionTy = PotionBar.Type.TAHOTHELIXIR
    elseif string.find(name, 'Elixir of Ptra') then
    		potionV, potionD = PotionBar.getNumbers(description, 2)
        potionD = potionD * 60
        potionTy = PotionBar.Type.PTRAELIXIR
    elseif string.find(name, 'Speed of the Blowing Sand') then
    		potionD = PotionBar.getNumbers(description, 1)
        potionD = potionD * 60
        potionTy = PotionBar.Type.SPEEDBLOWINGSAND
    end
    return potionV, potionD, potionTy
end