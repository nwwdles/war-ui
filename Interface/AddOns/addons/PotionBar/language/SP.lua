if not PotionBar then PotionBar = {} end
if not PotionBarData then PotionBarData = {} end
if not PotionBarSettings then PotionBarSettings = {} end

PotionBar.Spanish = {}

function PotionBar.getValuesSP(name, description)
    local potionV, potionD, potionT, potionC, potionTy = 0, 0, 0, 0, 0
    if string.find(description, 'instant[^%d]+neamente') then
        potionV = PotionBar.getNumbers(description, 1)
        if string.find(description, 'Puntos de acc') then
            potionTy = PotionBar.Type.AP
        elseif string.find(description, 'salud') then
            potionTy = PotionBar.Type.HEAL
        end
    elseif string.find(description, 'Elixir energ[^%d]+tico') then
        potionV = 1
        potionTy = PotionBar.Type.AP
    elseif string.find(description, 'Cura %d+ cada %d+ segundos durante') then
        potionV, potionT, potionD = PotionBar.getNumbers(description, 3)
        potionV = potionV * (potionD/potionT)
        potionTy = PotionBar.Type.REGEN
    elseif string.find(description, 'Restaura gradualmente') then
        potionV, potionD = PotionBar.getNumbers(description, 2)
        potionTy = PotionBar.Type.REGEN
    elseif string.find(description, 'Absorbe [%d]+de da[^%d]+o durante los pr') then
        potionV, potionD = PotionBar.getNumbers(description, 2)
        potionTy = PotionBar.Type.SHIELD
    elseif string.find(description, 'Te rodeas de una barrera m[^%d]+gica que resiste durante') then
        potionD, potionV = PotionBar.getNumbers(description, 2)
        potionTy = PotionBar.Type.SHIELD
    elseif string.find(description, 'Incrementa[^%d]+%d+ durante') then
        potionV, potionD = PotionBar.getNumbers(description, 2)
        potionD = potionD * 60
        if string.find(description, 'Fuerza') then
            potionTy = PotionBar.Type.STRENGTH
        elseif string.find(description, 'Inteligencia') then
            potionTy = PotionBar.Type.INTELLIGENCE
        elseif string.find(description, 'Habilidad de Proyectiles') then
            potionTy = PotionBar.Type.BALLISTIC
        elseif string.find(description, 'Resistencia en') then
            potionTy = PotionBar.Type.TOUGHNESS
        elseif string.find(description, 'Fortaleza') then
            potionTy = PotionBar.Type.TOUGHNESS
        elseif string.find(description, 'Resistencia [%a]spirit') then
            potionTy = PotionBar.Type.SPIRIT
        elseif string.find(description, 'Resistencia [%a]lement') then
            potionTy = PotionBar.Type.ELEMENTAL
        elseif string.find(description, 'Resistencia [%a]orp') then
            potionTy = PotionBar.Type.CORPOREAL
        elseif string.find(description, 'armadura') then
            potionTy = PotionBar.Type.ARMOR
        elseif string.find(description, 'Voluntad') then
            potionTy = PotionBar.Type.WILLPOWER
        end
    elseif string.find(description, 'Una capa de p[^%d]+as recubre tu cuerpo durante') then
        potionD, potionV = PotionBar.getNumbers(description, 2)
        potionD = potionD * 60
        potionTy = PotionBar.Type.THORNS
    elseif string.find(description, 'Tu armadura se cubre de espinas m[^%d]+gicas y[^%d]+ durante los pr[^%d]+ximos') then
        potionD, potionV = PotionBar.getNumbers(description, 2)
        potionD = potionD * 60
        potionTy = PotionBar.Type.THORNS
    elseif string.find(description, 'Una mezcla explosiva que hace que el objetivo arda') then
        potionV, potionT, potionD = PotionBar.getNumbers(description, 3)
        potionV = potionV * (potionD/potionT)
        potionTy = PotionBar.Type.DOT
    elseif string.find(description, 'Una mezcla incendiaria que inflige') then
        potionV, potionD = PotionBar.getNumbers(description, 2)
        potionTy = PotionBar.Type.DOT
    elseif string.find(description, 'Mezcla infame que abrasar[^%d]+ el [^%d]+rea objetivo') then
        potionT, potionD, potionV = PotionBar.getNumbers(description, 3)
        potionV = potionV * potionD
        potionTy = PotionBar.Type.AOEDOT
    elseif string.find(description, 'Una mezcla que comenzar[^%d]+ a arder durante') then
        potionD, potionV, potionT, potionC = PotionBar.getNumbers(description, 4)
        potionV = potionV * (potionD/potionT)
        potionTy = PotionBar.Type.AOEDOT
    elseif string.find(description, 'Mejunje viscoso que cubre el [^%d]+rea objetivo de') then
        potionT, potionD, potionV = PotionBar.getNumbers(description, 3)
        potionTy = PotionBar.Type.SNARE
    elseif string.find(description, 'Una mezcla espesa que cubre el suelo durante') then
        potionD, potionC, potionV = PotionBar.getNumbers(description, 3)
        potionTy = PotionBar.Type.SNARE
    elseif string.find(description, 'reparado ardiente que puede') then
        potionC, potionV, potionD = PotionBar.getNumbers(description, 3)
        potionTy = PotionBar.Type.FIREBREATH
    end
    return potionV, potionD, potionTy
end
