if not PotionBar then PotionBar = {} end
if not PotionBarData then PotionBarData = {} end
if not PotionBarSettings then PotionBarSettings = {} end

PotionBar.German = {}

PotionBar.German.Build = {}
PotionBar.German.Build =
{
    [PotionBar.Build.Right] = { Desc = L"Rechts" },
    [PotionBar.Build.Left] = { Desc = L"Links" },
    [PotionBar.Build.Up] = { Desc = L"Aufw�rts" },
    [PotionBar.Build.Down] = { Desc = L"Abw�rts" },
}

PotionBar.German.Priorities = {}
PotionBar.German.Priorities =
{
    [PotionBar.Priorities.BigStack] = { Desc = L"Gr��ter Stapel" },
    [PotionBar.Priorities.LessStack] = { Desc = L"Kleinster Stapel" },
    [PotionBar.Priorities.Weakest] = { Desc = L"Schw�chster Trank" },
    [PotionBar.Priorities.Strongest] = { Desc = L"St�rkster Trank" },
}

PotionBar.German.StackCountInfoType = {}
PotionBar.German.StackCountInfoType =
{
    [PotionBar.StackCountInfoType.Hide] = { Desc = L"Nichts" },
    [PotionBar.StackCountInfoType.Single] = { Desc = L"Zeige Stapelanzahl an" },
    [PotionBar.StackCountInfoType.Total] = { Desc = L"Zeige Gesammtzahl an" },
}

PotionBar.German.ActivatorModes = 
{
    [PotionBar.ActivatorModes.LeftOrUpper] = { Desc = L"Normal" },
    [PotionBar.ActivatorModes.RightOrBottom] = { Desc = L"Auf der anderen Seite" },
    [PotionBar.ActivatorModes.Hide] = { Desc = L"Verstecke Anfasser" },
}

PotionBar.German.QuickActions = {}
PotionBar.German.QuickActions =
{
    SelectAll = 1,
    UnSelectAll = 2,
    SelectLiniments = 3,
    UnSelectLiniments = 4,
    SelectStackVariants = 5,
    UnSelectStackVariants = 6,
    ResetPotionDefaults = 7,

    [1] = { Desc = L"W�hlen Sie All For Anzeige" },
    [2] = { Desc = L"Alle abw�hlen F�r Display-" },
    [3] = { Desc = L"W�hlen Sie Alle Einreibemittel zur Anzeige" },
    [4] = { Desc = L"Auswahl aufheben Einreibemittel zur Anzeige" },
    [5] = { Desc = L"Alle ausw�hlen Alle Varianten f�r Stack" },
    [6] = { Desc = L"Alle abw�hlen f�r Stack Alle Varianten" },
    [7] = { Desc = L"Alle zur�cksetzen Tr�nke auf Standardeinstellungen" },
}

PotionBar.German.Type =
{
    [PotionBar.Type.AP] = { Desc = L"Aktionspunkte"},
    [PotionBar.Type.AP_RVR] = { Desc = L"Aktionspunkte (RvR)" },
    [PotionBar.Type.HEAL] = { Desc = L"Heilung" },
    [PotionBar.Type.HEAL_RVR] = { Desc = L"Heilung (RvR)" },
    [PotionBar.Type.REGEN] = { Desc = L"Regeneration" },
    [PotionBar.Type.SHIELD] = { Desc = L"Absorption" },
    [PotionBar.Type.STRENGTH] = { Desc = L"St�rke" },
    [PotionBar.Type.INTELLIGENCE] = { Desc = L"Intelligenz" },
    [PotionBar.Type.BALLISTIC] = { Desc = L"Ballistik" },
    [PotionBar.Type.TOUGHNESS] = { Desc = L"Widerstand" }, 
    [PotionBar.Type.SPIRIT] = { Desc = L"Geist" }, 
    [PotionBar.Type.ELEMENTAL] = { Desc = L"Elementar" },
    [PotionBar.Type.CORPOREAL] = { Desc = L"K�rper" },
    [PotionBar.Type.ARMOR] = { Desc = L"R�stung" },
    [PotionBar.Type.THORNS] = { Desc = L"Dornen" },
    [PotionBar.Type.WILLPOWER] = { Desc = L"Willenskraft" },
    [PotionBar.Type.DOT] = { Desc = L"Brennendes Ziel" },
    [PotionBar.Type.AOEDOT] = { Desc = L"Brennender Bereich" },
    [PotionBar.Type.SNARE] = { Desc = L"Verlangsamung"}, 
    [PotionBar.Type.FIREBREATH] = { Desc = L"Feueratem" },

    [PotionBar.Type.INSPIRATIONALWINDS] = { Desc = L"Willenskraft + Heilbonus" }, 												-- Salbung der inspirierenden Winde
    [PotionBar.Type.SAVAGEVIGOR] = { Desc = L"Widerstand + kritische Nahkampftreffer" }, 									-- Salbung der schonungslosen Schlagkraft
    [PotionBar.Type.UNFETTEREDZEAL] = { Desc = L"St�rke + Heilbonus" }, 																	-- Salbung des uneingeschr�nkten Eifers
    [PotionBar.Type.WARBLOOD] = { Desc = L"Leben + kritische Nahkampftreffer" }, 													-- Salbung des Krieges: Blut
    [PotionBar.Type.WARDEMISE] = { Desc = L"Leben + Nahkampfbonus" }, 																		-- Salbung des Krieges: Niedergang
    [PotionBar.Type.WARFERVOR] = { Desc = L"Leben + kritische Fernkampftreffer" }, 												-- Salbung des Krieges: Eifer
    [PotionBar.Type.WARGENIUS] = { Desc = L"Leben + kritische Magietreffer" }, 														-- Salbung des Krieges: Begabung
    [PotionBar.Type.WARHUNGER] = { Desc = L"Leben + St�rke" }, 																						-- Salbung des Krieges: Hunger
    [PotionBar.Type.WARMERCY] = { Desc = L"Leben + Heilbonus" }, 																					-- Salbung des Krieges: Gnade
    [PotionBar.Type.ETERNALHUNT] = { Desc = L"Ballistik + kritische Fernkampftreffer" },									-- Salbung der ewigen Jagd
    [PotionBar.Type.INEVITABLETEMPEST] = { Desc = L"St�rke + Nahkampfbonus" }, 														-- Salbung des unvermeindlichen Sturms
    [PotionBar.Type.TOLLINGBELL] = { Desc = L"Intelligenz + kritische Magietreffer" }, 										-- Salbung der zollenden Glocke
    [PotionBar.Type.BOUNDLESSSIGHT] = { Desc = L"Leben + Initiative" },																		-- Salbung der grenzenlosen Sicht
    [PotionBar.Type.IMMUTABLEDEFIANCE] = { Desc = L"Leben Regeneration + Reduzierten Krit Dmg" }, 				-- Salbung des unab�nderlichen Trotzes
    [PotionBar.Type.INEXORABLEAEGIS] = { Desc = L"Resistenzen +180" },																		-- Salbung der unerbitterlichen �gide
    [PotionBar.Type.PEERLESSDEFENSE] = { Desc = L"Z�higkeit + Reduzierten Krit Trefferchance" }, 					-- Salbung der unvergleichlichen Abwehr
    [PotionBar.Type.QUICKENEDBLADES] = { Desc = L"Waffenfertigkeit + Reduziert R�stung Pen" }, 						-- Salbung der beschleunigten Klingen
    [PotionBar.Type.SWIFTTERGIVERSATION] = { Desc = L"Waffenfertigkeit + Reduziert Crit Trefferchance" }, -- Salbung der flinken Ausfl�chte
    [PotionBar.Type.NEPENTHEANTONIC] = { Desc = L"Wiederbelebungstrank" }, 									-- Tonikum des G�ttertrunks
    [PotionBar.Type.ZEPHYRTONIC] = { Desc = L"Tonikum des Zephirs" }, 											-- Tonikum des Zephirs
    [PotionBar.Type.TAHOTHELIXIR] = { Desc = L"Elixier des Tahoth" }, 											-- Elixier der Tahoth
    [PotionBar.Type.PTRAELIXIR] = { Desc = L"Elixier des Ptra" }, 													-- Elixier der Ptra
    [PotionBar.Type.SPEEDBLOWINGSAND] = { Desc = L"Geschwindigkeit der Blasen von Sand" }, 	-- Geschwindigkeit der Blasen von Sand
    [PotionBar.Type.UNKNOWNS] = { Desc = L"Alle anderen" },
    [PotionBar.Type.SINGLES] = { Desc = L"Einzelne" },
}

PotionBar.German.Strings = {} 
PotionBar.German.Strings =
{
    Config = L"Einstellungen...",
    FloatHint1 = L"PotionBar ist mit der linken Maustaste verschiebbar.",
    FloatHint2 = L"Dr�cke die rechte Maustaste um PotionBar zu konfigurieren.",

    TitleBarText = L"PotionBar Konfiguration",
    RightPaneTitle = L"Reihenfolge und Priorit�t",
    QuickActionsLabel = L" -------------- Schnelle Aktionen -------------- ",
    LeftTopPaneTitle = L"Anzeigeoptionen",
    BuildLabel = L"Ausrichtung", 
    AutohideLabel = L"Ausblenden",
    ScaleLabel = L"Skalierung",
    OpacityLabel = L"Deckkraft",
    SaveButton = L"Sparen",
    OKButton = L"OK",
    ResetButton = L"Standard Einstellungen",
    AboutButton = L"�ber...",
    InfoTopRight = L"Anzahl oben rechts",
    InfoBottomRight = L"Anzahl unten rechts",
    Show1Label = L"Zeige Anzahl von 1 an",
    ShowLastLabel = L"Zeige Anzahl des letzten Stapels an",
    DontSplitNameLabel = L"Zeige alle Varianten als ein Stapel an",
    ActivatorLabel = L"Position des Anfassers",

    BuildHint = L"W�hle die Ausrichtung von PotionBar.",
    AutoHideHint = L"Mache ein H�kchen, damit PotionBar sich automatisch ausblendet.",
    MoveUpHint = L"Dr�cke hier, um Tr�nke dieser Art fr�her aufzuzeigen.",
    MoveDownHint = L"Dr�cke hier, um Tr�nke dieser Art sp�ter aufzuzeigen.",
    UseHint = L"Mache ein H�kchen, um Tr�nke dieser Art in PotionBar anzuzeigen.",
    MethodHint = L"W�hle, nach welcher Methode PotionBar Tr�nke priorisieren soll.",
    InfoTopRightHint = L"W�hle die Art der Anzahl, die in der oberen rechten Ecke des Trankes angezeigt werden soll.",
    InfoBottomRightHint = L"W�hle die Art der Anzahl, die in der unteren rechten Ecke des Trankes angezeigt werden soll.",
    Show1Hint = L"Mache ein H�kchen, falls PotionBar auch dann die Anzahl anzeigen soll, falls sie Eins entspricht.",
    ShowLastHint = L"Mache ein H�kchen, um auch dann die Gesammtzahl anzuzeigen, wenn es der letzte Stapel ist.",
    QuickActionsHint = L"Erm�glicht schnellen Versteck und Anzeige aller oder w�hlen Sie Gruppen von Tr�nke / Einreibungen.",
    DontSplitNameHint = L"Mache ein H�kchen, um unterschiedliche Namen von Tr�nken zu ignorieren.",
    ActivatorHint = L"Hiermit bestimmst du die Position des kleinen runden Anfassers von PotionBar",

    HelpTitle = L"PotionBar kennt folgende Kommandos:",
    HelpForUnknown = L"Um einen bestimmten Tank zu trinken, nutze dieses Kommando:",
    HelpNameOfPotion = "Name des Trankes",
    HelpForMacros = L"Wenn du deine Tr�nke aus einem Makro nutzen willst, muss dein Makro so aussehen:",

    MacroPotionFound1 = "PotionBar benutzt '",
    MacroPotionFound2 = "' aus dem Gep�cktaschenplatz ",
    MacroPotionFound3 = ".",

    MacroNoPotionFound1 = L"PotionBar 'use' Kommandofehler",
    MacroNoPotionFound2 = "Nicht gefunden: ",
    MacroNoPotionFound3 = "",

    MacroPotionOnCoolDown1 = "PotionBar kann '",
    MacroPotionOnCoolDown2 = "' nicht benutzen, weil es noch im CoolDown ist.",
}

function PotionBar.getValuesDE(name, description)
    local potionV, potionD, potionT, potionC, potionTy = 0, 0, 0, 0, 0
    if string.find(description, 'Stellt sofort[^%d]+%d+[^%d]+ wieder her.') then
        potionV = PotionBar.getNumbers(description, 1)
        if string.find(description, 'ktionspunk') then
            potionTy = PotionBar.Type.AP
        elseif string.find(description, 'ebenspunkt') then
            potionTy = PotionBar.Type.HEAL
        end
    elseif string.find(description, 'Heilt[^%d]+[%d]+[^%d]+ekund[^%d]+lang alle[^%d]+%d+[^%d]+[%d]+') then
        potionD, potionT, potionV = PotionBar.getNumbers(description, 3)
        potionV = potionV * (potionD/potionT)
        potionTy = PotionBar.Type.REGEN
    elseif string.find(description, 'Stellt[^%d]+[%d]+[^%d]+lang insgesamt[^%d]+%d+[^%d]+enspunk[^%d]+wieder her') then
        potionD, potionV = PotionBar.getNumbers(description, 2)
        potionTy = PotionBar.Type.REGEN
    elseif string.find(description, 'Absorbiert[^%d]+[%d]+[^%d]+ekund[^%d]+%d+[^%d]+chaden') then
        potionD, potionV = PotionBar.getNumbers(description, 2)
        potionTy = PotionBar.Type.SHIELD
    elseif string.find(description, 'Umgibt Euch bis zu[^%d]+[%d]+[^%d]+ng mit einer mag[^%d]+%d+[^%d]+absorbie') then
        potionD, potionV = PotionBar.getNumbers(description, 2)
        potionTy = PotionBar.Type.SHIELD
    elseif string.find(description, 'Erh[^%d]+re[^%d]+[%d]+[^%d]+inu[^%d]+lang um[^%d]+[%d]+') then
        potionD, potionV = PotionBar.getNumbers(description, 2)
        potionD = potionD * 60
        if string.find(description, 'St[^%d]+rke') then
            potionTy = PotionBar.Type.STRENGTH
        elseif string.find(description, 'Intelligenz') then
            potionTy = PotionBar.Type.INTELLIGENCE
        elseif string.find(description, 'Ballistische') then
            potionTy = PotionBar.Type.BALLISTIC
        elseif string.find(description, 'Widerstand') then
            potionTy = PotionBar.Type.TOUGHNESS
        elseif string.find(description, 'Geistresistenz') then
            potionTy = PotionBar.Type.SPIRIT
        elseif string.find(description, 'Elementarresistenz') then
            potionTy = PotionBar.Type.ELEMENTAL
        elseif string.find(description, 'K[^%d]+rperresistenz') then
            potionTy = PotionBar.Type.CORPOREAL
        elseif string.find(description, 'R[^%d]+stung') then
            potionTy = PotionBar.Type.ARMOR
        elseif string.find(description, 'Willenskraft') then
            potionTy = PotionBar.Type.WILLPOWER
        end
    elseif string.find(description, 'Dornen bedec') then
        potionD, potionV = PotionBar.getNumbers(description, 2) 
        potionD = potionD * 60
        potionTy = PotionBar.Type.THORNS
    elseif string.find(description, 'Ein unbest[^%d]+ndiges Gebr[^%d]+u, das Euer Ziel') then
        potionD, potionT, potionV = PotionBar.getNumbers(description, 3)
        potionV = potionV * (potionD/potionT)
        potionTy = PotionBar.Type.DOT
    elseif string.find(description, 'Eine brennende Mixtur, die an') then
        potionD, potionV = PotionBar.getNumbers(description, 2)
        potionTy = PotionBar.Type.DOT
    elseif string.find(description, 'Eine abscheuliche Mixtur, die ein') then
        potionT, potionD, potionV = PotionBar.getNumbers(description, 3)
        potionV = potionV * potionD
        potionTy = PotionBar.Type.AOEDOT
    elseif string.find(description, 'wenn sie aufschl[^%d]+gt und allen Feinden innerhalb von') then
        potionD, potionC, potionT, potionV = PotionBar.getNumbers(description, 4)
        potionV = potionV * (potionD/potionT)
        potionTy = PotionBar.Type.AOEDOT
    elseif string.find(description, 'dickfl[^%d]+ssige Mixtur, die das Zielgebiet') then
        potionT, potionD, potionV = PotionBar.getNumbers(description, 3)
        potionTy = PotionBar.Type.SNARE
    elseif string.find(description, 'dickfl[^%d]+ssige Mixtur, die den Boden') then
        potionD, potionC, potionV = PotionBar.getNumbers(description, 3)
        potionTy = PotionBar.Type.SNARE
    elseif string.find(description, 'Ein feuriger Schluck, der benutzt werden kann') then
        potionC, potionD, potionV = PotionBar.getNumbers(description, 3)
        potionTy = PotionBar.Type.FIREBREATH
--    elseif string.find(name, 'Salbung der inspirierenden Winde') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.INSPIRATIONALWINDS
--    elseif string.find(name, 'Salbung der schonungslosen Schlagkraft') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.SAVAGEVIGOR
--    elseif string.find(name, 'Salbung des uneingeschr.*nkten Eifers') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.UNFETTEREDZEAL
--    elseif string.find(name, 'Salbung des Krieges.*Blut') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.WARBLOOD
--    elseif string.find(name, 'Salbung des Krieges.*Niedergang') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.WARDEMISE
--    elseif string.find(name, 'Salbung des Krieges.*Eifer') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.WARFERVOR
--    elseif string.find(name, 'Salbung des Krieges.*Genialit.*t') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.WARGENIUS
--    elseif string.find(name, 'Salbung des Krieges.*Hunger') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.WARHUNGER
--    elseif string.find(name, 'Salbung des Krieges.*Gnade') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.WARMERCY
--    elseif string.find(name, 'Salbung der ewigen Jagd') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.ETERNALHUNT
--    elseif string.find(name, 'Salbung des unvermeidlichen Sturms') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.INEVITABLETEMPEST
--    elseif string.find(name, 'Salbung der l.*utenden Glocke') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.TOLLINGBELL
--    elseif string.find(name, 'Salbung der grenzenlosen Sicht') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.BOUNDLESSSIGHT
--    elseif string.find(name, 'Salbung des unab�nderlichen Trotzes') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.IMMUTABLEDEFIANCE
--    elseif string.find(name, 'Salbung der unerbitterlichen �gide') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.INEXORABLEAEGIS
--    elseif string.find(name, 'Salbung der unvergleichlichen Abwehr') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.PEERLESSDEFENSE
--    elseif string.find(name, 'Salbung der beschleunigten Klingen') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.QUICKENEDBLADES
--    elseif string.find(name, 'Salbung der flinken Ausfl�chte') then
--        potionV, potionD = 100, 1800
--        potionTy = PotionBar.Type.SWIFTTERGIVERSATION
    elseif string.find(name, 'Tonikum des G.*ttertrunks') or string.find(name, 'Nepenthean Tonic') then	-- lazy hack
        potionV, potionD = 10000, 1
        potionTy = PotionBar.Type.NEPENTHEANTONIC
    elseif string.find(name, 'Tonikum des Zephirs') then
    		potionV, potionD = PotionBar.getNumbers(description, 2)
        potionD = potionD * 60
        potionTy = PotionBar.Type.ZEPHYRTONIC
    elseif string.find(name, 'Elixier des Tahoth') then
    		potionD = PotionBar.getNumbers(description, 1)
        potionD = potionD * 60
        potionTy = PotionBar.Type.TAHOTHELIXIR
    elseif string.find(name, 'Elixier des Ptra') then
    		potionV, potionD = PotionBar.getNumbers(description, 2)
        potionD = potionD * 60
        potionTy = PotionBar.Type.PTRAELIXIR
    elseif string.find(name, '*.*Sand') then
    		potionD = PotionBar.getNumbers(description, 1)
        potionD = potionD * 60
        potionTy = PotionBar.Type.SPEEDBLOWINGSAND
    end
    return potionV, potionD, potionTy
end
