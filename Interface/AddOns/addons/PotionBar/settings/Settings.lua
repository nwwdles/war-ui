if not PotionBar then PotionBar = {} end
if not PotionBarSettings then PotionBarSettings = {} end

function PotionBarSettings.OnHidden()
    WindowSetShowing("PotionBarWindowConfig", false)
end

function PotionBarSettings.OnCancelButton()
    WindowSetShowing("PotionBarWindowConfig", false)
end

function PotionBarSettings.OnSaveButton()
    PotionBar.UpdateButtons()
end

function PotionBarSettings.OnResetButton()
    PotionBar.Defaults()
    WindowSetShowing("PotionBarWindowConfig", false)
    WindowSetShowing("PotionBarWindowConfig", true)
end

function PotionBarSettings.HelpTip(text)
    Tooltips.CreateTextOnlyTooltip(SystemData.ActiveWindow.name)
    Tooltips.SetTooltipText(1, 1, text)
    Tooltips.SetTooltipColorDef(1, 1, Tooltips.COLOR_HEADING)
    Tooltips.Finalize();
    Tooltips.AnchorTooltip( { Point = "topleft", RelativeTo = SystemData.ActiveWindow.name, RelativePoint = "bottomleft", XOffset = 0, YOffset = -10 } )
end

--Build Direction Combo
function PotionBarSettings.BuildComboSelChanged()
    local selectedcomboindex = ComboBoxGetSelectedMenuItem("PotionBarWindowConfigLeftTopPaneBuildCombo")
    if selectedcomboindex ~= nil and selectedcomboindex > 0 then
        PotionBar.Settings.Build = selectedcomboindex
    end
    PotionBar.UpdateButtons()
end

function PotionBarSettings.OnMouseoverBuild()
    PotionBarSettings.HelpTip(PotionBar.LocalizableStrings.BuildHint)
end

--Activator Mode Combo
function PotionBarSettings.ActivatorComboSelChanged()
    local selectedcomboindex = ComboBoxGetSelectedMenuItem("PotionBarWindowConfigLeftTopPaneActivatorCombo")
    if selectedcomboindex ~= nil and selectedcomboindex > 0 then
        PotionBar.Settings.Activator = selectedcomboindex
    end
    PotionBar.UpdateButtons()
end

function PotionBarSettings.OnMouseoverActivator()
    PotionBarSettings.HelpTip(PotionBar.LocalizableStrings.ActivatorHint)
end

--Autohide Check
function PotionBarSettings.AutohideCheck()
    PotionBar.Settings.Autohide = not PotionBar.Settings.Autohide
    ButtonSetPressedFlag("PotionBarWindowConfigLeftTopPaneAutohideCheck", PotionBar.Settings.Autohide)
    PotionBarFloating.Autoshow()
    PotionBarFloating.Autohide()
end

function PotionBarSettings.OnMouseoverAutohide()
    PotionBarSettings.HelpTip(PotionBar.LocalizableStrings.AutoHideHint)
end

--Scale Slide
function PotionBarSettings.OnScaleSliderChanged()
    PotionBar.Settings.Scale = SliderBarGetCurrentPosition("PotionBarWindowConfigLeftTopPaneScaleSlider")
    PotionBarFloating.Scale()
end

--Opacity Slide
function PotionBarSettings.OnAlphaSliderChanged()
    PotionBar.Settings.Opacity = SliderBarGetCurrentPosition("PotionBarWindowConfigLeftTopPaneOpacitySlider")
    PotionBarFloating.Alpha()
end

--Move up button
function PotionBarSettings.MoveUp()
    local index = PotionBar.getNumberFromWindowName()
    local tempHolder = {}
    tempHolder = PotionBar.Settings.Buttons[index]
    table.remove(PotionBar.Settings.Buttons, index)
    table.insert(PotionBar.Settings.Buttons, index - 1, tempHolder)
    PotionBarSettings.RefreshSingleRightLine(index)
    PotionBarSettings.RefreshSingleRightLine(index - 1)
    PotionBar.UpdateButtons()
end

function PotionBarSettings.OnMouseoverMoveUp()
    PotionBarSettings.HelpTip(PotionBar.LocalizableStrings.MoveUpHint)
end

--Move down button
function PotionBarSettings.MoveDown()
    if ButtonGetDisabledFlag(SystemData.MouseOverWindow.name) then return end
    local index = PotionBar.getNumberFromWindowName()
    local tempHolder = {}
    tempHolder = PotionBar.Settings.Buttons[index]
    table.remove(PotionBar.Settings.Buttons, index)
    table.insert(PotionBar.Settings.Buttons, index + 1, tempHolder)
    PotionBarSettings.RefreshSingleRightLine(index)
    PotionBarSettings.RefreshSingleRightLine(index + 1)
    PotionBar.UpdateButtons()
end

function PotionBarSettings.OnMouseoverMoveDown()
    PotionBarSettings.HelpTip(PotionBar.LocalizableStrings.MoveDownHint)
end

--Use Checkbox
function PotionBarSettings.OnUseCheck()
    if ButtonGetDisabledFlag(SystemData.MouseOverWindow.name) then return end
    local index = PotionBar.getNumberFromWindowName()
    local windowName = "PotionBarWindowConfigScrollRightPane" .. index

    PotionBar.Settings.Buttons[index].Use = not PotionBar.Settings.Buttons[index].Use
    ButtonSetPressedFlag(windowName.. "Check", PotionBar.Settings.Buttons[index].Use)
		
		PotionBarSettings.OnUseCheckUpdateHighlighting(windowName, index)

    PotionBar.UpdateButtons()
end

function PotionBarSettings.OnUseCheckUpdateHighlighting(winName, indx)
		-- Mute/Unmute the colors for the text
    if ButtonGetPressedFlag( winName.. "Check" ) then
    		LabelSetTextColor( winName .. "Title", DefaultColor.WHITE.r, DefaultColor.WHITE.g, DefaultColor.WHITE.b)
		else
				LabelSetTextColor( winName .. "Title", DefaultColor.MEDIUM_GRAY.r, DefaultColor.MEDIUM_GRAY.g, DefaultColor.MEDIUM_GRAY.b)
		end
    
    ComboBoxSetDisabledFlag( winName .. "Combo", not PotionBar.Settings.Buttons[indx].Use)
    ButtonSetDisabledFlag( winName .. "DontSplitNameCheck", not PotionBar.Settings.Buttons[indx].Use)

		-- Mute/Unmute the colors for the text
    if ButtonGetPressedFlag( winName.. "Check" ) then
    		LabelSetTextColor( winName .. "DontSplitNameTitle", DefaultColor.WHITE.r, DefaultColor.WHITE.g, DefaultColor.WHITE.b)
		else
				LabelSetTextColor( winName .. "DontSplitNameTitle", DefaultColor.MEDIUM_GRAY.r, DefaultColor.MEDIUM_GRAY.g, DefaultColor.MEDIUM_GRAY.b)
		end
end	

function PotionBarSettings.OnMouseoverUse()
    PotionBarSettings.HelpTip(PotionBar.LocalizableStrings.UseHint)
end

--Method Combo
function PotionBarSettings.ComboMethod()
    local index = PotionBar.getNumberFromWindowName()
    local windowName = "PotionBarWindowConfigScrollRightPane" .. index
    local selectedcombotxt = towstring(ComboBoxGetSelectedText(windowName .. "Combo")) -- Convert towstring to fix wstringcompare error
    if selectedcombotxt == nil then return end
    for mode = 1, #PotionBar.Priorities do
        if 0 == WStringsCompare(selectedcombotxt, PotionBar.Priorities[mode].Desc) then
            PotionBar.Settings.Buttons[index].Preference = mode
            PotionBar.UpdateButtons(true)
        end
    end
end

function PotionBarSettings.OnMouseoverMethod()
    PotionBarSettings.HelpTip(PotionBar.LocalizableStrings.MethodHint)
end

--Split Potions by Name checkbox
function PotionBarSettings.OnDontSplitNameCheck()
    if ButtonGetDisabledFlag(SystemData.MouseOverWindow.name) then return end
    local index = PotionBar.getNumberFromWindowName()
    local windowName = "PotionBarWindowConfigScrollRightPane" .. index

    PotionBar.Settings.Buttons[index].SplitName = not PotionBar.Settings.Buttons[index].SplitName
    ButtonSetPressedFlag(windowName.. "DontSplitNameCheck", not PotionBar.Settings.Buttons[index].SplitName)

    PotionBar.UpdateButtons()
end

function PotionBarSettings.OnMouseoverDontSplitName()
    PotionBarSettings.HelpTip(PotionBar.LocalizableStrings.DontSplitNameHint)
end

--Top Right Info
function PotionBarSettings.InfoTextTRComboSelChanged()
    local selectedcomboindex = ComboBoxGetSelectedMenuItem("PotionBarWindowConfigLeftTopPaneInfoTextTRCombo")
    if selectedcomboindex ~= nil and selectedcomboindex > 0 then
        PotionBar.Settings.TopRight = selectedcomboindex
    end
    PotionBarSettings.UpdateLastCheckBasedOnInfoText()
    PotionBar.UpdateButtons()
end

function PotionBarSettings.OnMouseoverInfoTextTR()
    PotionBarSettings.HelpTip(PotionBar.LocalizableStrings.InfoTopRightHint)
end

--Bottom Right Info
function PotionBarSettings.InfoTextBRComboSelChanged()
    local selectedcomboindex = ComboBoxGetSelectedMenuItem("PotionBarWindowConfigLeftTopPaneInfoTextBRCombo")
    if selectedcomboindex ~= nil and selectedcomboindex > 0 then
        PotionBar.Settings.BottomRight = selectedcomboindex
    end
    PotionBarSettings.UpdateLastCheckBasedOnInfoText()
    PotionBar.UpdateButtons()
end

function PotionBarSettings.OnMouseoverInfoTextBR()
    PotionBarSettings.HelpTip(PotionBar.LocalizableStrings.InfoBottomRightHint)
end

--Show Count of 1 Check
function PotionBarSettings.Show1Check()
    PotionBar.Settings.ShowStackSizeEvenIf1 = not PotionBar.Settings.ShowStackSizeEvenIf1
    ButtonSetPressedFlag("PotionBarWindowConfigLeftTopPaneShow1Check", PotionBar.Settings.ShowStackSizeEvenIf1)
    PotionBar.UpdateButtons()
end

function PotionBarSettings.OnMouseoverShow1()
    PotionBarSettings.HelpTip(PotionBar.LocalizableStrings.Show1Hint)
end

--Show Last Stack Check
function PotionBarSettings.ShowLastCheck()
    PotionBar.Settings.ShowStackSizeEvenIfLast = not PotionBar.Settings.ShowStackSizeEvenIfLast
    PotionBarSettings.UpdateLastCheckBasedOnInfoText()
    PotionBar.UpdateButtons()
end

function PotionBarSettings.OnMouseoverShowLast()
    PotionBarSettings.HelpTip(PotionBar.LocalizableStrings.ShowLastHint)
end

function PotionBarSettings.UpdateLastCheckBasedOnInfoText()
    local enable = PotionBar.Settings.TopRight ~= PotionBar.Settings.BottomRight and PotionBar.Settings.TopRight ~= PotionBar.StackCountInfoType.Hide and PotionBar.Settings.BottomRight ~= PotionBar.StackCountInfoType.Hide
    if not enable then
        PotionBar.Settings.ShowStackSizeEvenIfLast = PotionBar.Settings.TopRight == PotionBar.StackCountInfoType.Total or PotionBar.Settings.BottomRight == PotionBar.StackCountInfoType.Total
    end
    ButtonSetDisabledFlag("PotionBarWindowConfigLeftTopPaneShowLastCheck", not enable)
    if PotionBar.Settings.ShowStackSizeEvenIfLast ~= ButtonGetPressedFlag("PotionBarWindowConfigLeftTopPaneShowLastCheck") then
        ButtonSetPressedFlag("PotionBarWindowConfigLeftTopPaneShowLastCheck", PotionBar.Settings.ShowStackSizeEvenIfLast)
    end
end

function PotionBarSettings.SkipThisPriority(potionBarType, priorityType)
    local priorityIsSizeBased = (priorityType == PotionBar.Priorities.BigStack or priorityType == PotionBar.Priorities.LessStack)
    local priorityIsStrengthBased = (priorityType == PotionBar.Priorities.Weakest or priorityType == PotionBar.Priorities.Strongest)
    return (potionBarType == PotionBar.Type.SINGLES and priorityIsSizeBased)
            or (potionBarType == PotionBar.Type.UNKNOWNS and priorityIsStrengthBased)
            or (potionBarType == PotionBar.Type.INSPIRATIONALWINDS and priorityIsStrengthBased)
            or (potionBarType == PotionBar.Type.SAVAGEVIGOR and priorityIsStrengthBased)
            or (potionBarType == PotionBar.Type.UNFETTEREDZEAL and priorityIsStrengthBased)
            or (potionBarType == PotionBar.Type.WARBLOOD and priorityIsStrengthBased)
            or (potionBarType == PotionBar.Type.WARDEMISE and priorityIsStrengthBased)
            or (potionBarType == PotionBar.Type.WARFERVOR and priorityIsStrengthBased)
            or (potionBarType == PotionBar.Type.WARGENIUS and priorityIsStrengthBased)
            or (potionBarType == PotionBar.Type.WARHUNGER and priorityIsStrengthBased)
            or (potionBarType == PotionBar.Type.WARMERCY and priorityIsStrengthBased)
            or (potionBarType == PotionBar.Type.ETERNALHUNT and priorityIsStrengthBased)
            or (potionBarType == PotionBar.Type.INEVITABLETEMPEST and priorityIsStrengthBased)
            or (potionBarType == PotionBar.Type.TOLLINGBELL and priorityIsStrengthBased)
            or (potionBarType == PotionBar.Type.NEPENTHEANTONIC and priorityIsStrengthBased)
end

function PotionBarSettings.RefreshSingleRightLine(index)
    local currentPane = "PotionBarWindowConfigScrollRightPane"
    local windowName = currentPane .. index
    local windowLast = currentPane .. (index - 1)

    --Title
    LabelSetText(windowName .. "Title", PotionBar.Type[PotionBar.Settings.Buttons[index].Type].Desc)
    LabelSetFont(windowName .. "Title", "font_default_text", WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)

    --Method Combo
    local toselect = 1
    local addeditems = 0
    ComboBoxClearMenuItems(windowName .. "Combo")
    for mode = 1, #PotionBar.Priorities do
        local skipit = PotionBarSettings.SkipThisPriority(PotionBar.Settings.Buttons[index].Type, mode)
        if not skipit then
            ComboBoxAddMenuItem(windowName .. "Combo", PotionBar.Priorities[mode].Desc)
            addeditems = addeditems + 1
            if mode == PotionBar.Settings.Buttons[index].Preference then
                toselect = addeditems
            end
        end
    end
    ComboBoxSetSelectedMenuItem(windowName .. "Combo", toselect)
    WindowSetShowing(windowName .. "Combo", addeditems > 0)

    --Use Checkbox
    ButtonSetPressedFlag(windowName .. "Check", PotionBar.Settings.Buttons[index].Use)

		-- Mute/Unmute the colors for the text
    if ButtonGetPressedFlag( windowName.. "Check" ) then
    		LabelSetTextColor(windowName .. "Title", DefaultColor.WHITE.r, DefaultColor.WHITE.g, DefaultColor.WHITE.b)
		else
				LabelSetTextColor(windowName .. "Title", DefaultColor.MEDIUM_GRAY.r, DefaultColor.MEDIUM_GRAY.g, DefaultColor.MEDIUM_GRAY.b)
		end

    ComboBoxSetDisabledFlag(windowName .. "Combo", not(PotionBar.Settings.Buttons[index].Use))

    --Don't Split Names Checkbox
    ButtonSetPressedFlag(windowName .. "DontSplitNameCheck", PotionBar.Settings.Buttons[index].SplitName ~= true)
    ButtonSetDisabledFlag(windowName .. "DontSplitNameCheck", not PotionBar.Settings.Buttons[index].Use)
    LabelSetText(windowName .. "DontSplitNameTitle", PotionBar.LocalizableStrings.DontSplitNameLabel)
    LabelSetFont(windowName .. "DontSplitNameTitle", "font_default_text", WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)

		-- Mute/Unmute the colors for the text
    if ButtonGetPressedFlag( windowName.. "Check" ) then
    		LabelSetTextColor(windowName .. "DontSplitNameTitle", DefaultColor.WHITE.r, DefaultColor.WHITE.g, DefaultColor.WHITE.b)
		else
				LabelSetTextColor(windowName .. "DontSplitNameTitle", DefaultColor.MEDIUM_GRAY.r, DefaultColor.MEDIUM_GRAY.g, DefaultColor.MEDIUM_GRAY.b)
		end

    --Set Anchors (and disable moving first up and last down)
    if index == 1 then
        WindowClearAnchors(windowName)
        WindowAddAnchor(windowName, "topleft", currentPane, "topleft", 13, 10)
        ButtonSetDisabledFlag(windowName .. "UpButton", true)
        ButtonSetDisabledFlag(windowName .. "DownButton", false)
    else
        WindowClearAnchors(windowName)
        WindowAddAnchor(windowName, "bottomleft", windowLast, "topleft", 0, 2)
        ButtonSetDisabledFlag(windowName .. "UpButton", false)
        ButtonSetDisabledFlag(windowName .. "DownButton", false)
    end
    if index == #PotionBar.Settings.Buttons then
        ButtonSetDisabledFlag(windowName .. "UpButton", false)
        ButtonSetDisabledFlag(windowName .. "DownButton", true)
    end
end

--New Config Window
function PotionBarSettings.OnShown()
    local windowTemplate = "PotionBarTypeTemplate"

    LabelSetText("PotionBarWindowConfigTitleBarText", PotionBar.LocalizableStrings.TitleBarText)

    --Right Pane--
    LabelSetText("PotionBarWindowConfigRightPaneTitle", PotionBar.LocalizableStrings.RightPaneTitle)

		--Quick Actions--
    ComboBoxClearMenuItems("PotionBarWindowConfigRightPaneQuickActionsCombo")
    for mode = 1, #PotionBar.QuickActions do
        ComboBoxAddMenuItem("PotionBarWindowConfigRightPaneQuickActionsCombo", PotionBar.QuickActions[mode].Desc)
    end
    ButtonSetText( "PotionBarWindowConfigRightPaneQuickActionsComboSelectedButton", PotionBar.LocalizableStrings.QuickActionsLabel )
    
    -- Add horizontal seperators into the pull down window of the combobox
    local w,h = WindowGetDimensions("PotionBarWindowConfigRightPaneQuickActionsCombo")

		if DoesWindowExist( "PotionBarWindowConfigRightPaneQuickActionsComboMenuBackground" ) then
		    for i = 1, #PotionBar.QuickActionsSeperators do
 	
		    		local wndComboBox = "PotionBarWindowConfigRightPaneQuickActionsComboMenu"
		    		local wndComboBackground = wndComboBox.."Background"
		    		local wndSeperator = wndComboBackground.."Seperator"..i
		    		local wndComboButton = wndComboBox.."Button"..PotionBar.QuickActionsSeperators[i]
		    	
						if not DoesWindowExist( wndSeperator ) then
								CreateWindowFromTemplate( wndSeperator, "PotionBarConfigHorizontalSeparator", wndComboBackground )
								WindowSetShowing( wndSeperator, true )
								WindowClearAnchors( wndSeperator )
								WindowAddAnchor( wndSeperator, "bottomleft", wndComboButton, "topleft", PotionBar.QuickActionsSeperators.OffsetXPos, -(PotionBar.QuickActionsSeperators.OffsetYPos) )
								WindowSetDimensions(wndSeperator, w - PotionBar.QuickActionsSeperators.WidthAdjust, PotionBar.QuickActionsSeperators.Height)
								WindowSetAlpha(wndSeperator, PotionBar.QuickActionsSeperators.Alpha)
						end		        
		    end		
		end
    
		--Potion Scroll Window--
    local parentForPotionConfigs = "PotionBarWindowConfigScrollRightPane"
    WindowSetTintColor(parentForPotionConfigs .. "Background", 64, 44, 44)

    for index = 1, #PotionBar.Settings.Buttons do
        local windowName = parentForPotionConfigs .. index
        if DoesWindowExist(windowName) then
            DestroyWindow(windowName)
        end
    end
    for index = 1, #PotionBar.Settings.Buttons do
        local windowName = parentForPotionConfigs .. index
        CreateWindowFromTemplate(windowName, windowTemplate, parentForPotionConfigs)
        PotionBarSettings.RefreshSingleRightLine(index)
    end

    --Top Left Pane--
    LabelSetText("PotionBarWindowConfigLeftTopPaneTitle", PotionBar.LocalizableStrings.LeftTopPaneTitle)

    LabelSetText("PotionBarWindowConfigLeftTopPaneBuildTitle", PotionBar.LocalizableStrings.BuildLabel)
    ComboBoxClearMenuItems("PotionBarWindowConfigLeftTopPaneBuildCombo")
    for mode = 1, #PotionBar.Build do
        ComboBoxAddMenuItem("PotionBarWindowConfigLeftTopPaneBuildCombo", PotionBar.Build[mode].Desc)
    end
    ComboBoxSetSelectedMenuItem("PotionBarWindowConfigLeftTopPaneBuildCombo", PotionBar.Settings.Build)

    LabelSetText("PotionBarWindowConfigLeftTopPaneActivatorTitle", PotionBar.LocalizableStrings.ActivatorLabel)
    ComboBoxClearMenuItems("PotionBarWindowConfigLeftTopPaneActivatorCombo")
    for mode = 1, #PotionBar.ActivatorModes do
        ComboBoxAddMenuItem("PotionBarWindowConfigLeftTopPaneActivatorCombo", PotionBar.ActivatorModes[mode].Desc)
    end
    ComboBoxSetSelectedMenuItem("PotionBarWindowConfigLeftTopPaneActivatorCombo", PotionBar.Settings.Activator)

    LabelSetText("PotionBarWindowConfigLeftTopPaneAutohideTitle", PotionBar.LocalizableStrings.AutohideLabel)
    ButtonSetPressedFlag("PotionBarWindowConfigLeftTopPaneAutohideCheck", PotionBar.Settings.Autohide)

    LabelSetText("PotionBarWindowConfigLeftTopPaneScaleTitle", PotionBar.LocalizableStrings.ScaleLabel)
    SliderBarSetCurrentPosition("PotionBarWindowConfigLeftTopPaneScaleSlider", PotionBar.Settings.Scale)
		LabelSetText( "PotionBarWindowConfigLeftTopPaneScaleSliderMinLabel", L"0.0" )
		LabelSetText( "PotionBarWindowConfigLeftTopPaneScaleSliderMaxLabel", L"1.0" )
		LabelSetText( "PotionBarWindowConfigLeftTopPaneScaleSliderMidLabel", L"0.5" )

    LabelSetText("PotionBarWindowConfigLeftTopPaneOpacityTitle", PotionBar.LocalizableStrings.OpacityLabel)
    SliderBarSetCurrentPosition("PotionBarWindowConfigLeftTopPaneOpacitySlider", PotionBar.Settings.Opacity)
		LabelSetText( "PotionBarWindowConfigLeftTopPaneOpacitySliderMinLabel", L"0.0" )
		LabelSetText( "PotionBarWindowConfigLeftTopPaneOpacitySliderMaxLabel", L"1.0" )
		LabelSetText( "PotionBarWindowConfigLeftTopPaneOpacitySliderMidLabel", L"0.5" )

    LabelSetText("PotionBarWindowConfigLeftTopPaneInfoTextTRTitle", PotionBar.LocalizableStrings.InfoTopRight)
    ComboBoxClearMenuItems("PotionBarWindowConfigLeftTopPaneInfoTextTRCombo")
    for mode = 1, #PotionBar.StackCountInfoType do
        ComboBoxAddMenuItem("PotionBarWindowConfigLeftTopPaneInfoTextTRCombo", PotionBar.StackCountInfoType[mode].Desc)
    end
    ComboBoxSetSelectedMenuItem("PotionBarWindowConfigLeftTopPaneInfoTextTRCombo", PotionBar.Settings.TopRight)

    LabelSetText("PotionBarWindowConfigLeftTopPaneInfoTextBRTitle", PotionBar.LocalizableStrings.InfoBottomRight)
    ComboBoxClearMenuItems("PotionBarWindowConfigLeftTopPaneInfoTextBRCombo")
    for mode = 1, #PotionBar.StackCountInfoType do
        ComboBoxAddMenuItem("PotionBarWindowConfigLeftTopPaneInfoTextBRCombo", PotionBar.StackCountInfoType[mode].Desc)
    end
    ComboBoxSetSelectedMenuItem("PotionBarWindowConfigLeftTopPaneInfoTextBRCombo", PotionBar.Settings.BottomRight)

    LabelSetText("PotionBarWindowConfigLeftTopPaneShow1Title", PotionBar.LocalizableStrings.Show1Label)
    ButtonSetPressedFlag("PotionBarWindowConfigLeftTopPaneShow1Check", PotionBar.Settings.ShowStackSizeEvenIf1)

    LabelSetText("PotionBarWindowConfigLeftTopPaneShowLastTitle", PotionBar.LocalizableStrings.ShowLastLabel)
    ButtonSetPressedFlag("PotionBarWindowConfigLeftTopPaneShowLastCheck", PotionBar.Settings.ShowStackSizeEvenIfLast)
    PotionBarSettings.UpdateLastCheckBasedOnInfoText()

    --Bottom Left Pane--
    -- EXPANSION SPACE --

    --Bottom Panel
    ButtonSetText("PotionBarWindowConfigSaveButton", PotionBar.LocalizableStrings.SaveButton)
    ButtonSetText("PotionBarWindowConfigResetButton", PotionBar.LocalizableStrings.ResetButton)
    ButtonSetText("PotionBarWindowConfigCancelButton", GetString( StringTables.Default.LABEL_CLOSE ))
    ButtonSetText("PotionBarWindowConfigAboutButton", PotionBar.LocalizableStrings.AboutButton)

    WindowSetAlpha(parentForPotionConfigs .. "Background", 0)
end

function PotionBarSettings.QuickActionsSelChanged()

    local selectedcomboindex = ComboBoxGetSelectedMenuItem("PotionBarWindowConfigRightPaneQuickActionsCombo")

		if selectedcomboindex == 1 then
				for PotionBarType = 1, #PotionBar.Type do
						local windowName = "PotionBarWindowConfigScrollRightPane" .. PotionBarType
		        PotionBar.Settings.Buttons[PotionBarType].Use = true	-- Check All
		        ButtonSetPressedFlag(windowName.. "Check", PotionBar.Settings.Buttons[PotionBarType].Use)
		        PotionBarSettings.OnUseCheckUpdateHighlighting(windowName, PotionBarType)
		    end
		elseif selectedcomboindex == 2 then
				for PotionBarType = 1, #PotionBar.Type do
						local windowName = "PotionBarWindowConfigScrollRightPane" .. PotionBarType
		        PotionBar.Settings.Buttons[PotionBarType].Use = false	-- Uncheck All
		        ButtonSetPressedFlag(windowName.. "Check", PotionBar.Settings.Buttons[PotionBarType].Use)
		        PotionBarSettings.OnUseCheckUpdateHighlighting(windowName, PotionBarType)
		    end
		elseif selectedcomboindex == 3 then
				for PotionBarType = 19, 36 do -- 19-36 is the current Liniment Potion.Type range, update this if that changes
						local windowName = "PotionBarWindowConfigScrollRightPane" .. PotionBarType
		        PotionBar.Settings.Buttons[PotionBarType].Use = true	-- Check All Liniments
		        ButtonSetPressedFlag(windowName.. "Check", PotionBar.Settings.Buttons[PotionBarType].Use)
		        PotionBarSettings.OnUseCheckUpdateHighlighting(windowName, PotionBarType)
		    end			
		elseif selectedcomboindex == 4 then
				for PotionBarType = 19, 36 do -- 19-36 is the current Liniment Potion.Type range, update this if that changes
						local windowName = "PotionBarWindowConfigScrollRightPane" .. PotionBarType
		        PotionBar.Settings.Buttons[PotionBarType].Use = false	-- Uncheck All liniment
		        ButtonSetPressedFlag(windowName.. "Check", PotionBar.Settings.Buttons[PotionBarType].Use)
		        PotionBarSettings.OnUseCheckUpdateHighlighting(windowName, PotionBarType)
		    end
		elseif selectedcomboindex == 5 then
				for PotionBarType = 1, #PotionBar.Type do
						local windowName = "PotionBarWindowConfigScrollRightPane" .. PotionBarType
		        PotionBar.Settings.Buttons[PotionBarType].SplitName = false	-- Check All for Stack All Variants
		        ButtonSetPressedFlag(windowName.. "DontSplitNameCheck", not PotionBar.Settings.Buttons[PotionBarType].SplitName)
		        PotionBarSettings.OnUseCheckUpdateHighlighting(windowName, PotionBarType)
		    end
		elseif selectedcomboindex == 6 then
				for PotionBarType = 1, #PotionBar.Type do
						local windowName = "PotionBarWindowConfigScrollRightPane" .. PotionBarType
		        PotionBar.Settings.Buttons[PotionBarType].SplitName = true	-- Uncheck All for Stack All Variants
		        ButtonSetPressedFlag(windowName.. "DontSplitNameCheck", not PotionBar.Settings.Buttons[PotionBarType].SplitName)
		        PotionBarSettings.OnUseCheckUpdateHighlighting(windowName, PotionBarType)
		    end
		elseif selectedcomboindex == 7 then
				PotionBar.DefaultsPotions()
				WindowSetShowing("PotionBarWindowConfig", false)
   			WindowSetShowing("PotionBarWindowConfig", true)
		end

    PotionBar.UpdateButtons()
    ButtonSetText( "PotionBarWindowConfigRightPaneQuickActionsComboSelectedButton", PotionBar.LocalizableStrings.QuickActionsLabel )
end

function PotionBarSettings.OnMouseoverQuickActions()
		PotionBarSettings.HelpTip(PotionBar.LocalizableStrings.QuickActionsHint)
end

function PotionBarSettings.OnAboutShown()
    LabelSetText("PotionBarAboutTitleBarText", L"About PotionBar")
    local version = L"PotionBar v" .. PotionBar.CurrentMajorVersion .. L"." .. PotionBar.CurrentMinorVersion
    LabelSetText("PotionBarAboutVersionInfo", version)
    local authors = L"Written by Sihirbaz and Felyza"..
    								L"\n(PotionBar was formerly known as Autobar)"..
    								L"\n\nOptimizations and Features by Wothor"..
    								L"\n(PotionBar@nusi.de)"..
    								L"\n\nFixes, Updates, and New Features by Alpha_Male"..
    								L"\n(alpha_male@speakeasy.net)"
    LabelSetText("PotionBarAboutAuthorInfo", authors)
    ButtonSetText("PotionBarAboutCloseButton" , PotionBar.LocalizableStrings.OkButton)
    WindowClearAnchors("PotionBarAbout")
    WindowAddAnchor("PotionBarAbout", "center", "Root", "center", 0, 0)
end

function PotionBarSettings.OnAboutClose()
    WindowSetShowing("PotionBarAbout", false)
end

function PotionBarSettings.OnAboutButton()
    if not DoesWindowExist("PotionBarAbout") then
        CreateWindow("PotionBarAbout", false)
    end
    WindowSetShowing("PotionBarAbout", true)
    PotionBarSettings.OnAboutShown()
end
