if not WarBoard_TogglerWSCT then WarBoard_TogglerWSCT = {} end
local WarBoard_TogglerWSCT = WarBoard_TogglerWSCT
local modName = "WarBoard_TogglerWSCT"
local modLabel = "WSCT"

function WarBoard_TogglerWSCT.Initialize()
	if LibWBToggler.CreateToggler(modName, modLabel, "WSCTIcon", 0, 0) then
		WindowSetDimensions(modName, 90, 30)
		WindowSetDimensions(modName.."Label", 58, 30)
		LibWBToggler.RegisterEvent(modName, "OnLButtonUp", "WarBoard_TogglerWSCT.OptionsWindow")
		LibWBToggler.RegisterEvent(modName, "OnMouseOver", "WarBoard_TogglerWSCT.ShowStatus")
	end
end

function WarBoard_TogglerWSCT.OptionsWindow()
	WSCT:OpenMenu()
end

function WarBoard_TogglerWSCT.ShowStatus()
	LibWBToggler.DefaultTooltip(modName, modLabel)
end
