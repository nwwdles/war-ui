if not WarBoard_Clock then WarBoard_Clock = {} end
local WarBoard_Clock = WarBoard_Clock
local LabelSetText, LabelSetTextColor, GetComputerTime, format, elapsedTime =
	  LabelSetText, LabelSetTextColor, GetComputerTime, wstring.format, 0
local timer, Time = 0, GetComputerTime()
local dSec = Time % 60
Time = (Time - dSec) / 60
local dMin = Time % 60
Time = (Time - dMin) / 60
local dHour = Time

function WarBoard_Clock.OnOptionsButton()
	WindowSetShowing("WarBoard_ClockOptions", not WindowGetShowing("WarBoard_ClockOptions"))
end

function WarBoard_Clock.Initialize()
	if WarBoard.AddMod("WarBoard_Clock") then
		if not WarBoard_ClockSettings then
			WarBoard_ClockSettings = {  Hours = 24, AmPm = false,
										Format = L"%02d:%02d:%02d",
										R = 255, G = 255, B = 255,}
		end
		WarBoard_ClockOptions.Initialize()
		LabelSetTextColor("WarBoard_ClockText", WarBoard_ClockSettings.R, WarBoard_ClockSettings.G, WarBoard_ClockSettings.B)
	end
end

function WarBoard_Clock.OnUpdate(elapsedTime)
	if elapsedTime > 0 then
		timer = timer + elapsedTime
		if timer >= 1 then
			dSec = dSec + 1
			if dSec >= 60 then dMin = dMin + 1 dSec = 0 
				if dMin >= 60 then dHour = dHour + 1 dMin = 0 
					if dHour == 24 then dHour = 0 end
				end
			end

			local fmt = WarBoard_ClockSettings.Format
			if WarBoard_ClockSettings.AmPm then
				if dHour < 12 then fmt=fmt..L" AM" else fmt=fmt..L" PM" end
			end

			local tHour = dHour % WarBoard_ClockSettings.Hours
			if tHour == 0 and WarBoard_ClockSettings.Hours == 12 then tHour = 12 end 
			LabelSetText("WarBoard_ClockText", format(fmt, tHour, dMin, dSec)) 
			timer = 0
		end
	end
end
