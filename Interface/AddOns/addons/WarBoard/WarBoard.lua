if not WarBoard then WarBoard = {} end
local WarBoard = WarBoard
local WindowSetTintColor, WindowSetAlpha, WindowClearAnchors, WindowAddAnchor, CreateWindow, towstring, tostring, tinsert, tremove,
	  setmetatable, getmetatable, ipairs, type, WindowGetDimensions, WindowSetDimensions, gsub, sfind, WindowStartAlphaAnimation,
	  MakeOneButtonDialog, DestroyWindow, Tooltips
	  =
	  WindowSetTintColor, WindowSetAlpha, WindowClearAnchors, WindowAddAnchor, CreateWindow, towstring, tostring, table.insert, table.remove, 
	  setmetatable, getmetatable, ipairs, type, WindowGetDimensions, WindowSetDimensions, string.gsub, string.find, WindowStartAlphaAnimation,
	  DialogManager.MakeOneButtonDialog, DestroyWindow, Tooltips

local defaults = 
{
	Board = { Red = 0, Blue = 0, Green = 0, Alpha = 0.5, VertSpace = 5, HorizSpace = 10, },
	Mods = { Red = 0, Blue = 0, Green = 0, Alpha = 0.5, },
	RowAlign = { 2, 2, 2, 2, }
}
local Version = "0.5.2"
local UnloadedMods = {}
local LayOutMode = false
local ClickedWindow = nil
WarBoard.LoadedMods = {}

local function CopyTable(object)
	-- Code from http://lua-users.org/wiki/CopyTable
	local lookup_table = {}
	local function _copy(object)
		if type(object) ~= "table" then
			return object
		elseif lookup_table[object] then
			return lookup_table[object]
		end
		local new_table = {}
		lookup_table[object] = new_table
		for index, value in pairs(object) do
			new_table[_copy(index)] = _copy(value)
		end
		return setmetatable(new_table, getmetatable(object))
	end
	return _copy(object)
end

function WarBoard.Initialize()
	WarBoard.LoadGeneralSettings()
	WarBoard.Options.Initialize()
	if LibSlash then
		LibSlash.RegisterSlashCmd("warboard", WarBoard.SlashCommand)
	end
	RegisterEventHandler(SystemData.Events.LOADING_END, "WarBoard.SortMods")
	RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "WarBoard.SortMods")
	d("WarBoard Loaded")
end

function WarBoard.LoadGeneralSettings()
	if WarBoardSettings then
		if not WarBoardSettings.Version or WarBoardSettings.Version ~= Version then
			WarBoardSettings = nil
		end
	end
	if not WarBoardSettings then
		WarBoard.LoadDefaults()
	end
	-- Create windows
	local TopBoard, BottomBoard = WarBoardSettings.Top.Board, WarBoardSettings.Bottom.Board
	if TopBoard.Enabled then
		CreateWindow("WarBoard", true)
		WindowSetTintColor("WarBoardBackground", TopBoard.Red, TopBoard.Green, TopBoard.Blue) 
		WindowSetAlpha("WarBoardBackground", TopBoard.Alpha)
		if TopBoard.ButtonsOn then
			CreateWindow("WarBoardOptionsButtonWindow", true)
			CreateWindow("WarBoardLayoutModeButtonWindow", true)
			WindowSetAlpha("WarBoardOptionsButton", 0)
			WindowSetAlpha("WarBoardLayoutModeButton", 0)
		end
	end
	if BottomBoard.Enabled then
		CreateWindowFromTemplate("BottomBoard", "WarBoard", "Root")
		WindowClearAnchors("BottomBoard")
		WindowAddAnchor ("BottomBoard", "bottomleft", "Root", "bottomleft", -1, -1)
		WindowAddAnchor ("BottomBoard", "bottomright", "Root", "bottomright", 0, -1)
		WindowSetTintColor ("BottomBoardBackground", BottomBoard.Red, BottomBoard.Green, BottomBoard.Blue)
		WindowSetAlpha ("BottomBoardBackground", BottomBoard.Alpha)
		if BottomBoard.ButtonsOn then
			CreateWindow("WarBoardBottomOptionsButtonWindow", true)
			CreateWindow("WarBoardBottomLayoutModeButtonWindow", true)
			WindowSetAlpha("BottomBoardOptionsButton", 0)
			WindowSetAlpha("BottomBoardLayoutModeButton", 0)
		end
	end
end

function WarBoard.LoadDefaults()
	WarBoardSettings = {}
	WarBoardSettings.Version = Version
	WarBoardSettings.DefaultBoard = "Top"

	WarBoardSettings.Top = CopyTable(defaults)
	WarBoardSettings.Top.ModRows = {}
	tinsert(WarBoardSettings.Top.ModRows, {})
	WarBoardSettings.Top.Board.Enabled = true
	WarBoardSettings.Top.Board.ButtonsOn = true

	WarBoardSettings.Bottom = CopyTable(defaults)
	WarBoardSettings.Bottom.ModRows = {}
	tinsert(WarBoardSettings.Bottom.ModRows, {})
	WarBoardSettings.Bottom.Board.Enabled = false
	WarBoardSettings.Bottom.Board.ButtonsOn = false
end

function WarBoard.SlashCommand()
	WindowSetShowing("WarBoardOptions", not WindowGetShowing("WarBoardOptions"))
end

function WarBoard.SortMods()
	if #WarBoard.LoadedMods > 0 then
		if WarBoardSettings.Top.Board.Enabled then
			WarBoard.CheckSavedRow(WarBoardSettings.Top.ModRows)
		end
		if WarBoardSettings.Bottom.Board.Enabled then
			WarBoard.CheckSavedRow(WarBoardSettings.Bottom.ModRows)
		end

		for k, v in ipairs(UnloadedMods) do
			local thismod = UnloadedMods[ k ]
			local board = thismod[ "board" ]
			local row = thismod[ "row" ]
			local pos = thismod[ "pos" ]
			tremove(board[ row ], pos)
			if #board[ row ] == 0 then
				tremove(board, row)
			end
		end

		--After each saved row has been checked, add any remaining LoadedMods(These are new) to the last row, checking length each add
		while #WarBoard.LoadedMods > 0 do
			--dump loaded mods into the last row of the default board
			for i=#WarBoard.LoadedMods, 0, -1 do
				local DefaultBoard = WarBoardSettings.DefaultBoard
				tinsert(WarBoardSettings[ DefaultBoard ].ModRows[ #WarBoardSettings[ DefaultBoard ].ModRows ], WarBoard.LoadedMods [ i ])
				if WarBoard.CheckLength(WarBoardSettings[ DefaultBoard ], #WarBoardSettings[ DefaultBoard ].ModRows) then
					tremove(WarBoard.LoadedMods)
				else-- this row was too long with the new mod
					tremove(WarBoardSettings[ DefaultBoard ].ModRows[ #WarBoardSettings[ DefaultBoard ].ModRows ])
					WarBoard.AddRow(WarBoardSettings[ DefaultBoard ].ModRows)
					break
				end
			end
		end
	end
	WarBoard.AnchorMods()
end

function WarBoard.CheckSavedRow(board)
	for k, v in ipairs(board) do
		local CurrentRow = board[ k ]
		local CurrentRowNum = k
		if CurrentRow[1] then
			for k, v in ipairs(board[ k ]) do
				local savedMod = v
				local savedPos = k
				local loadedMod = nil
				local loadedPos = nil
				for k, v in ipairs(WarBoard.LoadedMods) do
					if v == savedMod then
						loadedMod = v
						loadedPos = k
						break
					end
				end
				if not loadedMod then
					tinsert(UnloadedMods, { [ "board" ] = board, [ "row" ] = CurrentRowNum, [ "pos" ] = savedPos })
				else
					tremove(WarBoard.LoadedMods, loadedPos)
				end
			end
		end
	end
end

function WarBoard.AddRow(board)
	tinsert(board, {})
end

function WarBoard.CheckLength(board, row)
	local windowName = nil
	if board == WarBoardSettings.Top then
		windowName = "WarBoard"
	elseif board == WarBoardSettings.Bottom then
		windowName = "BottomBoard"
	end
	local WBx, WBy = WindowGetDimensions(windowName)
	local TotalModsLength = WarBoard.GetModsLength(board, row)
	local rootX = WindowGetDimensions("Root")
	if WBx == 0 then
		WBx = rootX
	end
	if row == 1 and board.ButtonsOn then
		WBx = WBx - 60
	end
	if TotalModsLength < WBx then
		return true
	else
		return false
	end
end

function WarBoard.GetModsLength(board, row)
	local TotalModsLength = 0
	for k, v in ipairs(board.ModRows[ row ]) do
		local modX, modY = WindowGetDimensions(v)
		TotalModsLength = TotalModsLength + modX
	end
	if #board.ModRows[ row ] > 1 then
		local padding = (#board.ModRows[ row ] -1) * board.Board.HorizSpace
		TotalModsLength = TotalModsLength + padding
	end
	return TotalModsLength
end

function WarBoard.AnchorMods()
	local WBx, WBy = 0, 0
	local BBx, BBy = 0, 0
	local rootX = WindowGetDimensions("Root")

	if WarBoardSettings.Top.Board.Enabled then
		WBx, WBy = WindowGetDimensions("WarBoard")
		if WBx == 0 then
			WBx = rootX
		end
		if #WarBoardSettings.Top.ModRows > 1 then
			WindowSetDimensions("WarBoard", WBx, ((#WarBoardSettings.Top.ModRows) * 30) + ((#WarBoardSettings.Top.ModRows - 1) * WarBoardSettings.Top.Board.VertSpace) + 2)
		else
			WindowSetDimensions("WarBoard", WBx, 32)
		end
		for k, v in ipairs(WarBoardSettings.Top.ModRows) do
			WarBoard.AnchorModRow(WarBoardSettings.Top, k, WBx)
		end
	end

	if WarBoardSettings.Bottom.Board.Enabled then
		WBx, WBy = WindowGetDimensions("BottomBoard")
		if BBx == 0 then
			BBx = rootX
		end
		if #WarBoardSettings.Bottom.ModRows > 1 then
			WindowSetDimensions("BottomBoard", BBx, ((#WarBoardSettings.Bottom.ModRows) * 30) + ((#WarBoardSettings.Bottom.ModRows - 1) * WarBoardSettings.Bottom.Board.VertSpace) + 2)
		else
			WindowSetDimensions("BottomBoard", BBx, 32)
		end
		for k, v in ipairs(WarBoardSettings.Bottom.ModRows) do
			WarBoard.AnchorModRow(WarBoardSettings.Bottom, k, BBx)
		end
	end
end

function WarBoard.AnchorModRow(board, row, windowX)
	local windowName, point
	if board == WarBoardSettings.Top then
		windowName = "WarBoard"
		point = "top"
	elseif board == WarBoardSettings.Bottom then
		windowName = "BottomBoard"
		point = "bottom"
	end
	local TotalModsLength = WarBoard.GetModsLength(board, row)

	for k, v in ipairs(board.ModRows[ row ]) do
		local yOffset = 0
		if row > 1 then
			yOffset = ((row - 1) * 30) + ((row - 1) * board.Board.VertSpace)
			if board == WarBoardSettings.Bottom then
				yOffset = -yOffset
			end
		end

		if k == 1 and board.RowAlign[ row ] == 2 then
			local xOffset = (windowX - TotalModsLength) / 2
			WindowClearAnchors(v)
			WindowAddAnchor(v, point.."left", windowName, point.."left", xOffset, yOffset)
		elseif k == 1 and board.RowAlign[ row ] == 1 then
			local xOffset = 0
			if board.Board.ButtonsOn and row == 1 then
				xOffset = 30
			else
				xOffset = board.Board.HorizSpace
			end
			WindowClearAnchors(v)
			WindowAddAnchor(v, point.."left", windowName, point.."left", xOffset, yOffset)
		elseif k == 1 and board.RowAlign[ row ] == 3 then
			local xOffset = (windowX - TotalModsLength)
			if board.Board.ButtonsOn and row == 1 then
				xOffset = xOffset - 30
			else
				xOffset = xOffset - board.Board.HorizSpace
			end
			WindowClearAnchors(v)
			WindowAddAnchor(v, point.."left", windowName, point.."left", xOffset, yOffset)
		else
			local thisRow = board.ModRows[ row ]
			local prevWindow = thisRow[ k-1 ]
			WindowClearAnchors(v)
			WindowAddAnchor(v, point.."right", prevWindow, point.."left", board.Board.HorizSpace, 0)
		end
		local background = v.."Background"
		WindowSetTintColor(background, board.Mods.Red, board.Mods.Green, board.Mods.Blue)
		WindowSetAlpha(background, board.Mods.Alpha)
	end
end

function WarBoard.AddMod(modName)
	tinsert(WarBoard.LoadedMods, modName)
	CreateWindow(modName, true)
	return true
end

function WarBoard.GetModPosition(modName)
	if WarBoardSettings.Top.Board.Enabled then
		for k, v in ipairs(WarBoardSettings.Top.ModRows) do
			local row = k
			for k, v in ipairs(WarBoardSettings.Top.ModRows[ row ]) do
				if v == modName then
					return "Top", row, k
				end
			end
		end
	end
	if WarBoardSettings.Bottom.Board.Enabled then
		for k, v in ipairs(WarBoardSettings.Bottom.ModRows) do
			local row = k
			for k, v in ipairs(WarBoardSettings.Bottom.ModRows[ row ]) do
				if v == modName then
					return "Bottom", row, k
				end
			end
		end
	end
end

function WarBoard.MoveModLeft(modName)
	local windowName = SystemData.ActiveWindow.name
	local modName = gsub(windowName, "MoveLeft", "")
	local board, row, pos = WarBoard.GetModPosition(modName)
	local currentRow = WarBoardSettings[ board ].ModRows[ row ]

	tremove(currentRow, pos)
	if pos ~= 1 then
		tinsert(currentRow, pos - 1, modName)
	else
		tinsert(currentRow, modName)
	end
	WarBoard.AnchorMods()
end

function WarBoard.MoveModRight()
	local windowName = SystemData.ActiveWindow.name
	local modName = gsub(windowName, "MoveRight", "")
	local board, row, pos = WarBoard.GetModPosition(modName)
	local currentRow = WarBoardSettings[ board ].ModRows[ row ]
	local nextMod = currentRow[ pos + 1 ]

	if pos ~= #currentRow then
		tremove(currentRow, pos + 1)
		tinsert(currentRow, pos, nextMod)
	else
		tremove(currentRow, pos)
		tinsert(currentRow, 1, modName)
	end
	WarBoard.AnchorMods()
end

function WarBoard.MoveModUp()
	local modName = gsub(ClickedWindow, "Move", "")
	local board, row, pos = WarBoard.GetModPosition(modName)
	local currentRow = WarBoardSettings[ board ].ModRows[ row ]
	if row == 1 then
		tinsert(WarBoardSettings[ board ].ModRows[ #WarBoardSettings[ board ].ModRows ], modName)
		if WarBoard.CheckLength(WarBoardSettings[ board ], #WarBoardSettings[ board ].ModRows) then
			tremove(currentRow, pos)
			if #currentRow == 0 then
				tremove(WarBoardSettings[ board ].ModRows, row)
			end
		else
			tremove(WarBoardSettings[ board ].ModRows[ #WarBoardSettings[ board ].ModRows ])
			local errorMsg = "WarBoard cannot move "..modName.." up, there is not enough room"
			MakeOneButtonDialog(towstring(errorMsg), L"OK")
		end
	else
		tinsert(WarBoardSettings[ board ].ModRows[ row - 1 ], modName)
		if WarBoard.CheckLength(WarBoardSettings[ board ], row - 1) then
			tremove(currentRow, pos)
			if #currentRow == 0 then
				tremove(WarBoardSettings[ board ].ModRows, row)
			end
		else
			tremove(WarBoardSettings[ board ].ModRows[ row - 1 ])
			local errorMsg = "WarBoard cannot move "..modName.." up, there is not enough room"
			MakeOneButtonDialog(towstring(errorMsg), L"OK")
		end
	end
	WarBoard.AnchorMods()
end

function WarBoard.MoveModDown()
	local modName = gsub(ClickedWindow, "Move", "")
	local board, row, pos = WarBoard.GetModPosition(modName)
	local currentRow = WarBoardSettings[ board ].ModRows[ row ]

	if row == #WarBoardSettings[ board ].ModRows then
		tinsert(WarBoardSettings[ board ].ModRows[ 1 ], modName)
		if WarBoard.CheckLength(WarBoardSettings[ board ], 1) then
			tremove(currentRow, pos)
			if #currentRow == 0 then
				tremove(WarBoardSettings[ board ].ModRows)
			end
		else
			tremove(WarBoardSettings[ board ].ModRows[ 1 ])
			local errorMsg = "WarBoard cannot move "..modName.." down, there is not enough room"
			MakeOneButtonDialog(towstring(errorMsg), L"OK")
		end
	else
		tinsert(WarBoardSettings[ board ].ModRows[ row + 1 ], modName)
		if WarBoard.CheckLength(WarBoardSettings[ board ], row + 1) then
			tremove(currentRow, pos)
			if #currentRow == 0 then
				tremove(WarBoardSettings[ board ].ModRows, row)
			end
		else
			tremove(WarBoardSettings[ board ].ModRows[ row + 1 ])
			local errorMsg = "WarBoard cannot move "..modName.." down, there is not enough room"
			MakeOneButtonDialog(towstring(errorMsg), L"OK")
		end
	end
	WarBoard.AnchorMods()
end

function WarBoard.ChangeModBoard(modName)
	if not modName then
		modName = gsub(ClickedWindow, "Move", "")
	end
	local board, row, pos = WarBoard.GetModPosition(modName)
	if board == "Top" then
		tinsert(WarBoardSettings.Bottom.ModRows[ #WarBoardSettings.Bottom.ModRows ], modName)
		if WarBoard.CheckLength(WarBoardSettings.Bottom, #WarBoardSettings.Bottom.ModRows) then
			tremove(WarBoardSettings.Top.ModRows[ row ], pos)
			if #WarBoardSettings.Top.ModRows[ row ] == 0  and #WarBoardSettings.Top.ModRows > 1 then
				tremove(WarBoardSettings.Top.ModRows, row)
			end
			WarBoard.AnchorMods()
		else
			tremove(WarBoardSettings.Bottom.ModRows[ #WarBoardSettings.Bottom.ModRows ])
			WarBoard.AddRow(WarBoardSettings.Bottom.ModRows)
			tinsert(WarBoardSettings.Bottom.ModRows[ #WarBoardSettings.Bottom.ModRows ], modName)
			tremove(WarBoardSettings.Top.ModRows[ row ], pos)
			if #WarBoardSettings.Top.ModRows[ row ] == 0  and #WarBoardSettings.Top.ModRows > 1 then
				tremove(WarBoardSettings.Top.ModRows, row)
			end
			WarBoard.AnchorMods()
		end
	elseif board == "Bottom" then
		tinsert(WarBoardSettings.Top.ModRows[ #WarBoardSettings.Top.ModRows ], modName)
		if WarBoard.CheckLength(WarBoardSettings.Top, #WarBoardSettings.Top.ModRows) then
			tremove(WarBoardSettings.Bottom.ModRows[ row ], pos)
			if #WarBoardSettings.Bottom.ModRows[ row ] == 0 and #WarBoardSettings.Bottom.ModRows > 1 then
				tremove(WarBoardSettings.Bottom.ModRows, row)
			end
			WarBoard.AnchorMods()
		else
			tremove(WarBoardSettings.Top.ModRows[ #WarBoardSettings.Top.ModRows ])
			WarBoard.AddRow(WarBoardSettings.Top.ModRows)
			tinsert(WarBoardSettings.Top.ModRows[ #WarBoardSettings.Top.ModRows ], modName)
			tremove(WarBoardSettings.Bottom.ModRows[ row ], pos)
			if #WarBoardSettings.Bottom.ModRows[ row ] == 0 and #WarBoardSettings.Bottom.ModRows > 1 then
				tremove(WarBoardSettings.Bottom.ModRows, row)
			end
			WarBoard.AnchorMods()
		end
	end
end

function WarBoard.OpenLayoutMenu()
	local windowName = SystemData.ActiveWindow.name
	ClickedWindow = windowName
	local modName = gsub(ClickedWindow, "Move", "")
	local board = WarBoard.GetModPosition(modName)
	local changeRow = false
	local changeBoard = false
	local destBoard = nil

	if #WarBoardSettings[ board ].ModRows > 1 then
		changeRow = true
	end
	if board == "Top" and WarBoardSettings.Bottom.Board.Enabled then
		changeBoard = true
		destBoard = "Bottom"
	elseif board == "Bottom" and WarBoardSettings.Top.Board.Enabled then
		changeBoard = true
		destBoard = "Top"
	end

	if changeBoard or changeRow then
		EA_Window_ContextMenu.CreateContextMenu(windowName)
		if changeRow then
			EA_Window_ContextMenu.AddMenuItem(L"Move Up", WarBoard.MoveModUp, false, true)
			EA_Window_ContextMenu.AddMenuItem(L"Move Down", WarBoard.MoveModDown, false, true)
		end
		if changeBoard then
			EA_Window_ContextMenu.AddMenuItem(towstring("Move to "..destBoard.." Board"), WarBoard.ChangeModBoard, false, true)
		end
		EA_Window_ContextMenu.Finalize()
	end
end

------------BUTTONS-----------
function WarBoard.OnOptionsButton()
	WindowSetShowing("WarBoardOptions", not WindowGetShowing("WarBoardOptions"))
end

function WarBoard.OnLayoutModeButton()
	if not LayOutMode then
		if WarBoardSettings.Top.Board.Enabled then
			for k, v in ipairs(WarBoardSettings.Top.ModRows) do
				local row = k
				for k, v in ipairs(WarBoardSettings.Top.ModRows[ row ]) do
					local windowName = v.."Move"
					CreateWindowFromTemplate(windowName, "LayoutTemplate", v)
					WindowAddAnchor(windowName, "topleft", v, "topleft", 0, 0)
					WindowAddAnchor(windowName, "bottomright", v, "bottomright", 0, 0)
					WindowSetTintColor(windowName.."Background", 0, 0, 0)
					WindowSetAlpha(windowName.."Background", 0.5)
				end
			end
		end
		if WarBoardSettings.Bottom.Board.Enabled then
			for k, v in ipairs(WarBoardSettings.Bottom.ModRows) do
				local row = k
				for k, v in ipairs(WarBoardSettings.Bottom.ModRows[ row ]) do
					local windowName = v.."Move"
					CreateWindowFromTemplate(windowName, "LayoutTemplate", v)
					WindowAddAnchor(windowName, "topleft", v, "topleft", 0, 0)
					WindowAddAnchor(windowName, "bottomright", v, "bottomright", 0, 0)
					WindowSetTintColor(windowName.."Background", 0, 0, 0)
					WindowSetAlpha(windowName.."Background", 0.5)
				end
			end
		end
		LayOutMode = true
	else
		if WarBoardSettings.Top.Board.Enabled then
			for k, v in ipairs(WarBoardSettings.Top.ModRows) do
				local row = k
				for k, v in ipairs(WarBoardSettings.Top.ModRows[ row ]) do
					local windowName = v.."Move"
					DestroyWindow(windowName)
				end
			end
		end
		if WarBoardSettings.Bottom.Board.Enabled then
			for k, v in ipairs(WarBoardSettings.Bottom.ModRows) do
				local row = k
				for k, v in ipairs(WarBoardSettings.Bottom.ModRows[ row ]) do
					local windowName = v.."Move"
					DestroyWindow(windowName)
				end
			end
		end
		LayOutMode = false
	end
end

function WarBoard.OnMouseOver()
	WindowStartAlphaAnimation("WarBoardOptionsButton", Window.AnimationType.EASE_OUT, 0, 1, 0.5, true, 0, 1)
	WindowStartAlphaAnimation("WarBoardLayoutModeButton", Window.AnimationType.EASE_OUT, 0, 1, 0.5, true, 0, 1)
	local windowName = SystemData.ActiveWindow.name
	if sfind(windowName, "Layout") then
		Tooltips.CreateTextOnlyTooltip(windowName, nil)
		Tooltips.AnchorTooltip(Tooltips.ANCHOR_WINDOW_BOTTOM)
		Tooltips.SetTooltipText(1, 1, L"WarBoard Layout")
		Tooltips.SetTooltipText(2, 1, L"Click to begin/exit WarBoard Layout editing")
		Tooltips.Finalize()
	elseif sfind(windowName, "Options") then
		Tooltips.CreateTextOnlyTooltip(windowName, nil)
		Tooltips.AnchorTooltip(Tooltips.ANCHOR_WINDOW_BOTTOM)
		Tooltips.SetTooltipText(1, 1, L"WarBoard Options")
		Tooltips.SetTooltipText(2, 1, L"Click to open WarBoard Options window")
		Tooltips.Finalize()
	end
end

function WarBoard.OnMouseOverEnd()
	WindowStartAlphaAnimation("WarBoardOptionsButton", Window.AnimationType.EASE_OUT, 1, 0, 0.5, true, 0, 1)
	WindowStartAlphaAnimation("WarBoardLayoutModeButton", Window.AnimationType.EASE_OUT, 1, 0, 0.5, true, 0, 1)
end

function WarBoard.OnMouseOverBottom()
	WindowStartAlphaAnimation("BottomBoardOptionsButton", Window.AnimationType.EASE_OUT, 0, 1, 0.5, true, 0, 1)
	WindowStartAlphaAnimation("BottomBoardLayoutModeButton", Window.AnimationType.EASE_OUT, 0, 1, 0.5, true, 0, 1)
	local windowName = SystemData.ActiveWindow.name
	if sfind(windowName, "Layout") then
		Tooltips.CreateTextOnlyTooltip(windowName, nil)
		Tooltips.AnchorTooltip(Tooltips.ANCHOR_WINDOW_TOP)
		Tooltips.SetTooltipText(1, 1, L"WarBoard Layout")
		Tooltips.SetTooltipText(2, 1, L"Click to begin/exit WarBoard Layout editing")
		Tooltips.Finalize()
	elseif sfind(windowName, "Options") then
		Tooltips.CreateTextOnlyTooltip(windowName, nil)
		Tooltips.AnchorTooltip(Tooltips.ANCHOR_WINDOW_TOP)
		Tooltips.SetTooltipText(1, 1, L"WarBoard Options")
		Tooltips.SetTooltipText(2, 1, L"Click to open WarBoard Options window")
		Tooltips.Finalize()
	end
end

function WarBoard.OnMouseOverEndBottom()
	WindowStartAlphaAnimation("BottomBoardOptionsButton", Window.AnimationType.EASE_OUT, 1, 0, 0.5, true, 0, 1)
	WindowStartAlphaAnimation("BottomBoardLayoutModeButton", Window.AnimationType.EASE_OUT, 1, 0, 0.5, true, 0, 1)
end

function WarBoard.GetModToolTipAnchor(modName)
	local board = WarBoard.GetModPosition(modName)
	if board == "Top" then
		return Tooltips.ANCHOR_WINDOW_BOTTOM
	elseif board == "Bottom" then
		return Tooltips.ANCHOR_WINDOW_TOP
	end
end
