if not WarBoard_TogglerQQueuer then WarBoard_TogglerQQueuer = {} end
local WarBoard_TogglerQQueuer = WarBoard_TogglerQQueuer
local modName = "WarBoard_TogglerQQueuer"
local modLabel = "Queue"
local initialized, timer = false, 0

function WarBoard_TogglerQQueuer.Initialize()
	if LibWBToggler.CreateToggler(modName, modLabel, "EA_Training_Specialization", 0, 0, "tactic-white", 0.4) then
		WindowSetDimensions(modName, 110, 30)
		WindowSetDimensions(modName.."Label", 60, 30)
		LibWBToggler.RegisterEvent(modName, "OnLButtonUp", "QueueQueuer_GUI.MapButton_OnLButtonUp")
		LibWBToggler.RegisterEvent(modName, "OnRButtonUp", "WarBoard_TogglerQQueuer.MenuDropdown")
		LibWBToggler.RegisterEvent(modName, "OnMouseOver", "WarBoard_TogglerQQueuer.ShowStatus")
		LibWBToggler.RegisterEvent(modName, "OnUpdate", "WarBoard_TogglerQQueuer.OnUpdate")
	end
	WarBoard_TogglerQQueuer.DisableMain()
	initialized = true
end

function WarBoard_TogglerQQueuer.OnUpdate(elapsed)
	if not initialized then return end
	timer = timer + elapsed
	if timer < 1 then return end

	if QueueQueuer_SavedVariables["enabled"] then
		if QueueQueuer.QueueCooldown() then
			DynamicImageSetTextureSlice(modName.."Icon", "tactic-yellow")
			--LabelSetTextColor(modName.."Label", 255, 255, 0)
			LabelSetTextColor(modName.."Label", 255, 255, 255)
		elseif QueueQueuer.IsQueuer() then
			DynamicImageSetTextureSlice(modName.."Icon", "tactic-white")
			LabelSetTextColor(modName.."Label", 255, 255, 255)
		else
			DynamicImageSetTextureSlice(modName.."Icon", "tactic-black")
			--LabelSetTextColor(modName.."Label", 100, 100, 100)
			LabelSetTextColor(modName.."Label", 255, 255, 255)
		end
	else
		DynamicImageSetTextureSlice(modName.."Icon", "tactic-orange")
		LabelSetTextColor(modName.."Label", 255, 165, 0)
	end
	timer = 0
end

function WarBoard_TogglerQQueuer.DisableMain()
	LayoutEditor.UnregisterWindow("QueueQueuer_GUI_MapButtons")
	QueueQueuer_GUI.OnUpdate = nil
	QueueQueuer_GUI.MapButton_OnUpdate = nil
	WindowSetShowing("QueueQueuer_GUI_MapButton", false)
	WindowSetShowing("QueueQueuer_GUI_MapButton_DISABLED", false)
	WindowSetShowing("QueueQueuer_GUI_MapButton_QUEUER", false)
	WindowSetShowing("QueueQueuer_GUI_MapButton_COOLDOWN", false)
end

function WarBoard_TogglerQQueuer.ShowStatus()
	QueueQueuer_GUI.MapButton_OnMouseOver()
	Tooltips.AnchorTooltip(WarBoard.GetModToolTipAnchor(modName))
end

function WarBoard_TogglerQQueuer.MenuDropdown()
	EA_Window_ContextMenu.CreateContextMenu("QueueQueuer_GUI_ContextMenu", 1, L"Queue Queuer")
	EA_Window_ContextMenu.AddMenuDivider(1)
	EA_Window_ContextMenu.AddUserDefinedMenuItem("QueueQueuer_GUI_Context1", 1)
	EA_Window_ContextMenu.AddUserDefinedMenuItem("QueueQueuer_GUI_Context2", 1)
	EA_Window_ContextMenu.AddUserDefinedMenuItem("QueueQueuer_GUI_Context3", 1)
	EA_Window_ContextMenu.AddUserDefinedMenuItem("QueueQueuer_GUI_Context4", 1)
	EA_Window_ContextMenu.AddMenuDivider(1)
	EA_Window_ContextMenu.AddMenuItem(L"Blacklist", nil, true, false, 1)
	ButtonSetTextColor("EA_Window_ContextMenu1DefaultItem1", Button.ButtonState.DISABLED, 255, 255, 255)

	for id = 1, EA_Window_ScenarioLobby.MAX_SCENARIOS do
		if GameData.ScenarioQueueData[id].id ~= 0 then
			LabelSetText("QueueQueuer_GUI_ContextScenario" .. id .. "CheckBoxLabel", GetScenarioName(GameData.ScenarioQueueData[id].id))
			for key, value in pairs(QueueQueuer_SavedVariables["blacklist"]) do
				if QueueQueuer.ScenarioNames[key] == GetScenarioName(GameData.ScenarioQueueData[id].id) then
					ButtonSetPressedFlag("QueueQueuer_GUI_ContextScenario" .. id .. "CheckBox", value)
				end
			end
			EA_Window_ContextMenu.AddUserDefinedMenuItem("QueueQueuer_GUI_ContextScenario" .. id, 1)
		end
	end
	if GameData.ScenarioQueueData[1].id == 0 then
		EA_Window_ContextMenu.AddMenuItem(L"No Scenarios", nil, true, true, 1)
	end

	EA_Window_ContextMenu.AddCascadingMenuItem(L"Cooldowns", QueueQueuer_GUI.CreateCooldownsContextMenu, false, 1)
	EA_Window_ContextMenu.AddMenuDivider(1)
	EA_Window_ContextMenu.AddMenuItem(L"Queuer Check", QueueQueuer_GUI.QueuerCheckButton_OnLButtonUp, not QueueQueuer.IsQueuer(), true, 1)
	EA_Window_ContextMenu.AddMenuItem(L"Leave Queues", QueueQueuer_GUI.LeaveButton_OnLButtonUp, false, true, 1)
	EA_Window_ContextMenu.AddMenuItem(L"Join Queues", QueueQueuer_GUI.JoinButton_OnLButtonUp, false, true, 1)
	--EA_Window_ContextMenu.Finalize(1, { ["XOffset"] = 0, ["YOffset"] = 0, ["Point"] = "bottomleft", ["RelativePoint"] = "topleft", ["RelativeTo"] = modName,})
	EA_Window_ContextMenu.Finalize( 1, 
	{ 
		["XOffset"] = 0,
		["YOffset"] = -10,
		["Point"] = "top",
		["RelativePoint"] = "bottom",
		["RelativeTo"] = modName,
	} )
end
