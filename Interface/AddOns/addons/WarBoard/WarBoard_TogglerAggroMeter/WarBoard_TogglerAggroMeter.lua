if not WarBoard_TogglerAggroMeter then WarBoard_TogglerAggroMeter = {} end
local WarBoard_TogglerAggroMeter = WarBoard_TogglerAggroMeter
local modName = "WarBoard_TogglerAggroMeter"
local modLabel = "AggroMeter"

function WarBoard_TogglerAggroMeter.Initialize()
	if LibWBToggler.CreateToggler(modName, modLabel, "AggroMeterIconv4", 0, 0) then
		--WindowSetDimensions(modName, 70, 30)
		--WindowSetDimensions(modName.."Label", 70, 30)
		LibWBToggler.RegisterEvent(modName, "OnRButtonUp", "WarBoard_TogglerAggroMeter.Settings")
		LibWBToggler.RegisterEvent(modName, "OnMouseOver", "WarBoard_TogglerAggroMeter.ShowStatus")
	end
end

function WarBoard_TogglerAggroMeter.Settings()
	AggroMeter.OnTabRBU()
end

function WarBoard_TogglerAggroMeter.ShowStatus()
	LibWBToggler.DefaultTooltip(modName, "AggroMeter")
end
