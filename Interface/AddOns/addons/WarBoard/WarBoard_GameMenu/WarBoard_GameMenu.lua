WarBoard_GameMenu = {}

WarBoard_GameMenu_ItemTable = {}

local WINDOW_HEIGHT = 30
local ITEM_WIDTH = 30
local ITEM_HEIGHT = 30
local ITEM_SPACING = 0
local PRE_SPACING = 5
local POST_SPACING = 5


--[[--------------------------------------------------
	Initializes the mod
------------------------------------------------------]]
function WarBoard_GameMenu.Initialize()
	d("WarBoard_GameMenu initializing...")
	if (WarBoard.AddMod("WarBoard_GameMenu")) then
		-- Add all menu items to the table
		if not (WarBoard_GameMenu_ItemTable) or (table.getn(WarBoard_GameMenu_ItemTable) == 0) then
			WarBoard_GameMenu.AddItem(1, "Abilities", "Spellbook", "AbilitiesWindow", nil, "Abilities", true)
			WarBoard_GameMenu.AddItem(2, "BackPack", "Bag", "EA_Window_Backpack", nil, "Backpack", true)
			WarBoard_GameMenu.AddItem(3, "Character", "PaperDoll", "CharacterWindow", "PaperDoll-Button-Selected-Highlighted", "Character", true)
			WarBoard_GameMenu.AddItem(4, "ToK", "Tome", "TomeWindow", nil, "Tome of Knowledge", true)
			WarBoard_GameMenu.AddItem(5, "MainMenu", "MainMenu", "MainMenuWindow", nil, "Main Menu", true)
			WarBoard_GameMenu.AddItem(6, "OpenParty", "Scenario-Grouping", "EA_Window_OpenParty", nil, "Open Parties", true)
			WarBoard_GameMenu.AddItem(7, "CustomizeInterface", "CustomizeInterface", "CustomizeUiWindow", nil, "Customize UI", true)
			WarBoard_GameMenu.AddItem(8, "Guild", "Guild", "GuildWindow", nil, "Guild", true)
			WarBoard_GameMenu.AddItem(9, "HelpElement", "Help", "EA_Window_Help", nil, "Help", true)
		end
		
		-- Build the menu
		WarBoard_GameMenu.BuildMenu()
	end
end
	
--[[--------------------------------------------------
	Adds a menu item to the item table
------------------------------------------------------]]
function WarBoard_GameMenu.AddItem(position, name, template, openingwindow, highlighticon, friendlytext, visible)
	table.insert(WarBoard_GameMenu_ItemTable, {position = position, name = name, template = template, openingwindow = openingwindow, highlighticon = highlighticon, friendlytext = friendlytext, visible = visible})
end

--[[--------------------------------------------------
	Uses the item table to build the menu
------------------------------------------------------]]
function WarBoard_GameMenu.BuildMenu()
	-- Set the dimensions of the window
	WindowSetDimensions("WarBoard_GameMenu", WarBoard_GameMenu.CalculateWindowSize(), WINDOW_HEIGHT)
	
	-- Initialize the horizontal offset
	local xOffSet = PRE_SPACING
	
	-- Loop through each item in the item table
	for k, v in ipairs(WarBoard_GameMenu_ItemTable) do
		if (v.visible) then
			-- Create the menu item
			WarBoard_GameMenu.BuildItem(v.name, v.template, xOffSet)
			
			-- Increment the horizontal offset
			xOffSet = xOffSet + ITEM_WIDTH + ITEM_SPACING
		end
	end
end

--[[--------------------------------------------------
	Builds a specific menu item
------------------------------------------------------]]
function WarBoard_GameMenu.BuildItem(name, coreName, xOffSet)
	-- Create a window for the menu item
	CreateWindowFromTemplate(name, "WarBoard_GameMenu_MenuItem", "Root")
	WindowSetShowing(name, true)
	WindowSetParent(name, "WarBoard_GameMenu")
	WindowSetDimensions(name, ITEM_WIDTH, ITEM_HEIGHT)
	
	-- Create the icon for the menu item
	DynamicImageSetTexture(name.."Icon", "EA_Texture_Menu01", 0, 0)
	DynamicImageSetTextureSlice(name.."Icon", coreName.."-Button")
	DynamicImageSetTextureScale(name.."Icon", 0.5)
	
	-- Add event handlers to the menu item
	WindowRegisterCoreEventHandler(name, "OnMouseOver", "WarBoard_GameMenu.OnMouseOver")
	WindowRegisterCoreEventHandler(name, "OnMouseOverEnd", "WarBoard_GameMenu.OnMouseOverEnd")
	WindowRegisterCoreEventHandler(name, "OnLButtonUp", "WarBoard_GameMenu.OnLButtonUp")
	
	-- Anchor the menu item to the main WarBoard_GameMenu window
	WindowAddAnchor(name, "left", "WarBoard_GameMenu", "left", xOffSet, 0)
end

--[[--------------------------------------------------
	Counts the number of visible items in the item
	table
------------------------------------------------------]]
function WarBoard_GameMenu.CountVisibleItems()
	local count = 0
	for k, v in ipairs(WarBoard_GameMenu_ItemTable) do
		if (v.visible) then
			count = count + 1
		end
	end
	return count
end

--[[--------------------------------------------------
	Calculates the effective window size based on the
	number of visible items and the spacing between
	them
------------------------------------------------------]]
function WarBoard_GameMenu.CalculateWindowSize()
	local visibleItems = WarBoard_GameMenu.CountVisibleItems()
	local itemWidth = visibleItems * ITEM_WIDTH
	local itemSpacingWidth = (visibleItems - 1) * ITEM_SPACING

	return (PRE_SPACING + itemWidth + itemSpacingWidth + POST_SPACING)
end

--[[--------------------------------------------------
	Highlights the icon when the mouse hovers over it
------------------------------------------------------]]
function WarBoard_GameMenu.OnMouseOver()
	-- Loop through each item
	for k, v in ipairs( WarBoard_GameMenu_ItemTable ) do
		-- Check to see if the current item is the item being hovered over
		if (v.name == SystemData.ActiveWindow.name) then
			-- Create tooltip
			WarBoard_GameMenu.CreateTooltip(towstring(v.friendlytext))
			
			-- Highlight the icon
			if (v.highlighticon ~= nil) then
				DynamicImageSetTextureSlice(v.name.."Icon", v.highlighticon)
			else
				DynamicImageSetTextureSlice(v.name.."Icon", v.template.."-Button-Highlighted")
			end
			
			-- Enlarge the icon
			DynamicImageSetTextureScale(v.name.."Icon", 0.42)
			break
		end
	end	
end

--[[--------------------------------------------------
	Removes the highlighting of the icon when the
	mouse is no longer hovering over it
------------------------------------------------------]]
function WarBoard_GameMenu.OnMouseOverEnd()
	-- Loop through each item
	for k, v in ipairs(WarBoard_GameMenu_ItemTable) do
		-- Check to see if the current item is the target
		if (v.name == SystemData.ActiveWindow.name) then
			-- Remove the highlight
			DynamicImageSetTextureSlice(v.name.."Icon", v.template.."-Button")
			
			-- Shrink the icon back to normal size
			DynamicImageSetTextureScale(v.name.."Icon", 0.5)
			break
		end
	end	
end

--[[--------------------------------------------------
	Opens/closes the appropriate window based on
	which item was clicked
------------------------------------------------------]]
function WarBoard_GameMenu.OnLButtonUp()
	-- Loop through each item
	for k, v in ipairs( WarBoard_GameMenu_ItemTable ) do
		-- Check to see if the current item is the target
		if (v.name == SystemData.ActiveWindow.name) then
			-- Open/close the appropriate window
			WindowSetShowing(v.openingwindow, not WindowGetShowing(v.openingwindow))
			break
		end
	end	
end

--[[--------------------------------------------------
	Attaches a tooltip to target item
------------------------------------------------------]]
function WarBoard_GameMenu.CreateTooltip(text)
	Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name, nil )
	Tooltips.SetTooltipText(1, 1, text)
	Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_TOP)
	Tooltips.Finalize()
end


--[[--------------------------------------------------
	Layout Mode Functions
	NOTE: Argument is the name of the window.
------------------------------------------------------]]
function WarBoard_GameMenu.OnDisable()
	WarBoard.DisableMod("WarBoard_GameMenu")
end
function WarBoard_GameMenu.MoveRight()
	WarBoard.MoveModRight("WarBoard_GameMenu")
end
function WarBoard_GameMenu.MoveLeft()
	WarBoard.MoveModLeft("WarBoard_GameMenu")
end
