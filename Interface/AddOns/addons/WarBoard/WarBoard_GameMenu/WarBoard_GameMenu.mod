<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="WarBoard_GameMenu" version="1.0.2" date="04/16/2009" >
        
        <Author name="Dunnar" email="dunnar@gmail.com" />
        <Description text="A WarBoard plug-in that displays the game menu." />
		
		<VersionSettings gameVersion="1.2.1" windowsVersion="1.0" savedVariablesVersion="1.0" />
        
        <Dependencies>
	    	<Dependency name="WarBoard" optional="false" forceEnable="true" />
        </Dependencies>
        
		<SavedVariables>
            <SavedVariable name="WarBoard_GameMenu_ItemTable" />
        </SavedVariables>
		
        <Files>
            <File name="WarBoard_GameMenu.lua" />
			<File name="WarBoard_GameMenu.xml" />
        </Files>
        
        <OnInitialize>
			<CallFunction name="WarBoard_GameMenu.Initialize" />
        </OnInitialize>

        <OnUpdate/>

        <OnShutdown/>
    </UiMod>
</ModuleFile>
