-- god have mercy on my soul

if not ZonePOP_SearchFunctions then ZonePOP_SearchFunctions = {} end

-- Dwarf vs Greenskin
function ZonePOP_SearchFunctions.SearchZone6()
WarBoard_ZonePOP.SearchZone(6)
end
function ZonePOP_SearchFunctions.SearchZone11()
WarBoard_ZonePOP.SearchZone(11)
end
function ZonePOP_SearchFunctions.SearchZone7()
WarBoard_ZonePOP.SearchZone(7)
end
function ZonePOP_SearchFunctions.SearchZone1()
WarBoard_ZonePOP.SearchZone(1)
end
function ZonePOP_SearchFunctions.SearchZone2()
WarBoard_ZonePOP.SearchZone(2)
end
function ZonePOP_SearchFunctions.SearchZone8()
WarBoard_ZonePOP.SearchZone(8)
end
function ZonePOP_SearchFunctions.SearchZone9()
WarBoard_ZonePOP.SearchZone(9)
end
function ZonePOP_SearchFunctions.SearchZone5()
WarBoard_ZonePOP.SearchZone(5)
end
function ZonePOP_SearchFunctions.SearchZone3()
WarBoard_ZonePOP.SearchZone(3)
end
function ZonePOP_SearchFunctions.SearchZone4()
WarBoard_ZonePOP.SearchZone(4)
end
function ZonePOP_SearchFunctions.SearchZone10()
WarBoard_ZonePOP.SearchZone(10)
end

--Chaos vs Empire
function ZonePOP_SearchFunctions.SearchZone100()
WarBoard_ZonePOP.SearchZone(100)
end
function ZonePOP_SearchFunctions.SearchZone106()
WarBoard_ZonePOP.SearchZone(106)
end
function ZonePOP_SearchFunctions.SearchZone107()
WarBoard_ZonePOP.SearchZone(107)
end
function ZonePOP_SearchFunctions.SearchZone101()
WarBoard_ZonePOP.SearchZone(101)
end
function ZonePOP_SearchFunctions.SearchZone102()
WarBoard_ZonePOP.SearchZone(102)
end
function ZonePOP_SearchFunctions.SearchZone108()
WarBoard_ZonePOP.SearchZone(108)
end
function ZonePOP_SearchFunctions.SearchZone103()
WarBoard_ZonePOP.SearchZone(103)
end
function ZonePOP_SearchFunctions.SearchZone105()
WarBoard_ZonePOP.SearchZone(105)
end
function ZonePOP_SearchFunctions.SearchZone109()
WarBoard_ZonePOP.SearchZone(109)
end
function ZonePOP_SearchFunctions.SearchZone104()
WarBoard_ZonePOP.SearchZone(104)
end
function ZonePOP_SearchFunctions.SearchZone110()
WarBoard_ZonePOP.SearchZone(110)
end

--High Elves vs Dark Elves
function ZonePOP_SearchFunctions.SearchZone200()
WarBoard_ZonePOP.SearchZone(200)
end
function ZonePOP_SearchFunctions.SearchZone206()
WarBoard_ZonePOP.SearchZone(206)
end
function ZonePOP_SearchFunctions.SearchZone201()
WarBoard_ZonePOP.SearchZone(201)
end
function ZonePOP_SearchFunctions.SearchZone207()
WarBoard_ZonePOP.SearchZone(207)
end
function ZonePOP_SearchFunctions.SearchZone202()
WarBoard_ZonePOP.SearchZone(202)
end
function ZonePOP_SearchFunctions.SearchZone208()
WarBoard_ZonePOP.SearchZone(208)
end
function ZonePOP_SearchFunctions.SearchZone203()
WarBoard_ZonePOP.SearchZone(203)
end
function ZonePOP_SearchFunctions.SearchZone205()
WarBoard_ZonePOP.SearchZone(205)
end
function ZonePOP_SearchFunctions.SearchZone209()
WarBoard_ZonePOP.SearchZone(209)
end
function ZonePOP_SearchFunctions.SearchZone204()
WarBoard_ZonePOP.SearchZone(204)
end
function ZonePOP_SearchFunctions.SearchZone210()
WarBoard_ZonePOP.SearchZone(210)
end