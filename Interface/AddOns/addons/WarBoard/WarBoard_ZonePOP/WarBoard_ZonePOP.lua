--[[
Based on Sullemunks ZonePOP 1.3.1
https://www.returnofreckoning.com/forum/viewtopic.php?f=66&t=17913#p194449
]]--

if not WarBoard_ZonePOP then WarBoard_ZonePOP = {} end

local pairs = pairs
local ipairs = ipairs
local tinsert = table.insert
local sf = string.find
local ts = tostring
local tn = tonumber

local SendPlayerSearchRequest = SendPlayerSearchRequest
local TextLogGetEntry = TextLogGetEntry

local showTierOne = false
local showForts = false
local mergePairings = true

local ZonesToSearch = {}
local CurrentZoneName = nil

local isSearching = false
local ZonepopSearch = false

local CareerNamesDestro = {}
local CareerNamesOrder = {}
local playerCount = 1

local modName = "WarBoard_ZonePOP"
local modLabel = "ZonePOP"

function WarBoard_ZonePOP.Initialize()

	-- don't load lua debug library 
	--SetLoadLuaDebugLibrary(false)
	--ButtonSetPressedFlag("DebugWindowOptionsLuaDebugLibraryButton", false)
	
	-- turn off logging
	--DebugWindow.Settings.logsOn = false
	--CHAT_DEBUG( L" UI Logging OFF" )
    --DebugWindow.UpdateLog()

	-- registger this module with warboard manager
	if WarBoard.AddMod("WarBoard_ZonePOP") then
		WindowSetDimensions(modName, 120, 30)
		LabelSetText("WarBoard_ZonePOPDestro", (towstring("---")))
		LabelSetText("WarBoard_ZonePOPOrder", (towstring("---")))
		LabelSetText("WarBoard_ZonePOPAnon", (towstring("---")))
		LibWBToggler.RegisterEvent(modName, "OnMouseOver", "WarBoard_ZonePOP.ShowTooltip")
	end
	
	-- hide tier 1 zones if player outleveled them
	local playerLevel = GameData.Player.level
	if (playerLevel < 16) then
		showTierOne = true
	end
	
	WarBoard_ZonePOP.CreateMenuCheckboxes()
		
	WarBoard_ZonePOP.Index = 1
	WarBoard_ZonePOP.Timer = 0
	WarBoard_ZonePOP.Anon = 0
	WarBoard_ZonePOP.Destro = 0
	WarBoard_ZonePOP.Order = 0
	WarBoard_ZonePOP.Sort ={L"a",L"b",L"c",L"d",L"e",L"f",L"g",L"h",L"i",L"j",L"k",L"l",L"m",L"n",L"o",L"p",L"q",L"r",L"s",L"t",L"u",L"v",L"w",L"x",L"y",L"z"}

	-- turn class IDs in to language specific class names
	local CareerIdsDestro = {24,25,26,27,64,65,66,67,104,105,106,107}
	for i, id in ipairs(CareerIdsDestro) do
		tinsert(CareerNamesDestro, WarBoard_ZonePOP.FixString(towstring(GetStringFromTable("CareerNamesMale", id))))
	end
	
	local CareerIdsOrder = {20,21,22,23,61,60,63,62,100,102,103,101}	
	for i, id in ipairs(CareerIdsOrder) do
		tinsert(CareerNamesOrder, WarBoard_ZonePOP.FixString(towstring(GetStringFromTable("CareerNamesMale", id))))
	end
	
	if LibSlash then
		if not(LibSlash.IsSlashCmdRegistered("zonepop")) then
			LibSlash.RegisterSlashCmd("zonepop", WarBoard_ZonePOP.Search)
		end
		if not(LibSlash.IsSlashCmdRegistered("zonetell")) then
			LibSlash.RegisterSlashCmd("zonetell", WarBoard_ZonePOP.Tell)
		end
	end
	
	RegisterEventHandler(TextLogGetUpdateEventId("System"), "WarBoard_ZonePOP.OnChatLogUpdated")
	
end

function WarBoard_ZonePOP.LocalSearch()
	local zone = GameData.Player.zone
	WarBoard_ZonePOP.SearchZone(zone)
end

function WarBoard_ZonePOP.SearchZone(zone)

	if ((isSearching == false) and (ZonepopSearch == false)) then
	
		Tooltips.ClearTooltip(modName)
	
		if (mergePairings == false) then
			CurrentZoneName = GetZoneName(zone)
			ZonesToSearch = {zone}
		-- player is in tier 1
		elseif (zone == 100) or (zone == 106) then
			CurrentZoneName = "T1 Empire"
			ZonesToSearch = {100,106}
		elseif (zone == 200) or (zone == 206) then
			CurrentZoneName = "T1 Elf"
			ZonesToSearch = {200,206}
		elseif (zone == 6) or (zone == 11) then
			CurrentZoneName = "T1 Dwarf"
			ZonesToSearch = {6,11}
		-- player is in tier 2
		elseif (zone == 101) or (zone == 107) then
			CurrentZoneName = "T2 Empire"
			ZonesToSearch = {101,107}
		elseif (zone == 201) or (zone == 207) then
			CurrentZoneName = "T2 Elf"
			ZonesToSearch = {201,207}
		elseif (zone == 1) or (zone == 7) then
			CurrentZoneName = "T2 Dwarf"
			ZonesToSearch = {1,7}
		-- player is in tier 3
		elseif (zone == 108) or (zone == 102) then
			CurrentZoneName = "T3 Empire"
			ZonesToSearch = {108,102}
		elseif (zone == 202) or (zone == 208) then
			CurrentZoneName = "T3 Elf"
			ZonesToSearch = {202,208}
		elseif (zone == 2) or (zone == 8) then
			CurrentZoneName = "T3 Dwarf"
			ZonesToSearch = {2,8}			
		-- player is in a single instance zone
		else
			CurrentZoneName = GetZoneName(zone)
			ZonesToSearch = {zone}
		end
				
		playerCount = 1
		WarBoard_ZonePOP.Index = 1
		WarBoard_ZonePOP.Timer = 0 
		WarBoard_ZonePOP.Anon = 0
		WarBoard_ZonePOP.Destro = 0
		WarBoard_ZonePOP.Order = 0

		isSearching = true
		ZonepopSearch = true
		
	end

end


function WarBoard_ZonePOP.OnChatLogUpdated(updateType, filterType)
	
	-- idle while not searching
	if (isSearching == false) then
		return
	end

	if (updateType == SystemData.TextLogUpdate.ADDED) then
		local _, filterId, text = TextLogGetEntry( "System", TextLogGetNumEntries("System") - 1 )
		text = ts(text)
		if sf(text,"Found") then
			isSearching = false
			WarBoard_ZonePOP.ParseSearchResults()
		end
	end
	
end


function WarBoard_ZonePOP.Update(timeElapsed)

	if (isSearching == false) then
		return
	end
	
	if (ZonepopSearch == true) then

		-- set font to white while displaying progress text
		LabelSetTextColor("WarBoard_ZonePOPDestro",255,255,255)
		LabelSetTextColor("WarBoard_ZonePOPOrder",255,255,255)

		-- update progress text
		LabelSetText("WarBoard_ZonePOPDestro", (towstring("---")))
		local progress_percent = ((tn(WarBoard_ZonePOP.Index)-1)*4)
		if (progress_percent < 100) then
			LabelSetText("WarBoard_ZonePOPOrder", towstring(progress_percent..L"%"))
		end
		LabelSetText("WarBoard_ZonePOPAnon", (towstring("---")))
		
		-- send a search query
		if not(WarBoard_ZonePOP.Sort[WarBoard_ZonePOP.Index] == nil) then
			SendPlayerSearchRequest((towstring(WarBoard_ZonePOP.Sort[WarBoard_ZonePOP.Index])),L"",L"",{ZonesToSearch[1]},1,40,false)
		end
		ZonepopSearch = false
		
	end

	if (isSearching == true) then
		WarBoard_ZonePOP.Timer = WarBoard_ZonePOP.Timer + timeElapsed
	end

	if (WarBoard_ZonePOP.Timer > 0.6) then
		WarBoard_ZonePOP.Timer = 0 
		isSearching = false
		WarBoard_ZonePOP.ParseSearchResults()
	end
end

function WarBoard_ZonePOP.ParseSearchResults()
	
	local SearchList = GetSearchList()	
	for i, value in ipairs(SearchList) do
		
		playerCount = playerCount + 1
		
		local ClassName = WarBoard_ZonePOP.FixString(SearchList[i].career)
		--EA_ChatWindow.Print(playerCount..L" " ..WarBoard_ZonePOP.FixString(SearchList[i].name)..L" - "..ClassName)
		
		-- player is anonymos or invisible
		if ((ClassName == L"") or (ClassName == nil)) then
			WarBoard_ZonePOP.Anon = WarBoard_ZonePOP.Anon + 1
		else
			-- assign faction according to class
			for SearchIndex = 1,12 do
				if (ClassName == CareerNamesOrder[SearchIndex]) then
					WarBoard_ZonePOP.Order = WarBoard_ZonePOP.Order + 1 
					break
				elseif (ClassName == CareerNamesDestro[SearchIndex]) then
					WarBoard_ZonePOP.Destro = WarBoard_ZonePOP.Destro + 1 
					break
				end
			end
		end
		
	end
	
	WarBoard_ZonePOP.Index = WarBoard_ZonePOP.Index + 1	
	WarBoard_ZonePOP.Timer = 0
	ZonepopSearch = true
	isSearching = true
	
	if (WarBoard_ZonePOP.Index > 26) then
	
		WarBoard_ZonePOP.Index = 1
		table.remove(ZonesToSearch,1)

		if (#ZonesToSearch == 0) then
			LabelSetTextColor("WarBoard_ZonePOPDestro",255,69,0)
			LabelSetTextColor("WarBoard_ZonePOPOrder",0,205,255)
			LabelSetText("WarBoard_ZonePOPDestro", (towstring(WarBoard_ZonePOP.Destro)))
			LabelSetText("WarBoard_ZonePOPOrder", (towstring(WarBoard_ZonePOP.Order)))
			LabelSetText("WarBoard_ZonePOPAnon", (towstring(WarBoard_ZonePOP.Anon)))
			ZonepopSearch = false
			isSearching = false
			playerCount = 1
			--EA_ChatWindow.Print(towstring("-------------"))
		end		
	end
	
end

function WarBoard_ZonePOP.Tell()

	if (isSearching == true) or (CurrentZoneName == nil) then
		return
	end
	
	local popstring = towstring(CurrentZoneName)..L" Population: <LINK data=\"0\" text=\"Destro: "..towstring(WarBoard_ZonePOP.Destro)..L"\" color=\"255,25,25\"> "..L"  <LINK data=\"0\" text=\"Order: "..towstring(WarBoard_ZonePOP.Order)..L"\" color=\"75,75,255\"> "..L"  <LINK data=\"0\" text=\"Anon: "..towstring(WarBoard_ZonePOP.Anon)..L"\" color=\"255,255,55\">"
	EA_ChatWindow.InsertText(popstring)
	
end

function WarBoard_ZonePOP.CreateMenuCheckboxes()
	-- Show Tier 1
	CreateWindowFromTemplate("WarBoard_ZonePOP_GUI_Context1", "ChatContextMenuItemCheckBox", "Root")
		LabelSetText( "WarBoard_ZonePOP_GUI_Context1CheckBoxLabel", L"Show Tier 1 zones" )
		ButtonSetStayDownFlag( "WarBoard_ZonePOP_GUI_Context1CheckBox", showTierOne )
		ButtonSetPressedFlag( "WarBoard_ZonePOP_GUI_Context1CheckBox", showTierOne)
		WindowRegisterCoreEventHandler("WarBoard_ZonePOP_GUI_Context1", "OnLButtonUp", "WarBoard_ZonePOP.ContextCheckBox_OnLButtonUp")
		WindowRegisterCoreEventHandler("WarBoard_ZonePOP_GUI_Context1", "OnMouseOver", "WarBoard_ZonePOP.ContextCheckBox_OnMouseOver")
		WindowSetId("WarBoard_ZonePOP_GUI_Context1", 1)
		WindowSetShowing("WarBoard_ZonePOP_GUI_Context1", false)
	-- Show Forts
	CreateWindowFromTemplate("WarBoard_ZonePOP_GUI_Context2", "ChatContextMenuItemCheckBox", "Root")
		LabelSetText( "WarBoard_ZonePOP_GUI_Context2CheckBoxLabel", L"Show Forts" )
		ButtonSetStayDownFlag( "WarBoard_ZonePOP_GUI_Context2CheckBox", showForts )
		ButtonSetPressedFlag( "WarBoard_ZonePOP_GUI_Context2CheckBox", showForts)
		WindowRegisterCoreEventHandler("WarBoard_ZonePOP_GUI_Context2", "OnLButtonUp", "WarBoard_ZonePOP.ContextCheckBox_OnLButtonUp")
		WindowRegisterCoreEventHandler("WarBoard_ZonePOP_GUI_Context2", "OnMouseOver", "WarBoard_ZonePOP.ContextCheckBox_OnMouseOver")
		WindowSetId("WarBoard_ZonePOP_GUI_Context2", 2)
		WindowSetShowing("WarBoard_ZonePOP_GUI_Context2", false)
	-- Merge Pairings
	CreateWindowFromTemplate("WarBoard_ZonePOP_GUI_Context3", "ChatContextMenuItemCheckBox", "Root")
		LabelSetText( "WarBoard_ZonePOP_GUI_Context3CheckBoxLabel", L"Merge pairings" )
		ButtonSetStayDownFlag( "WarBoard_ZonePOP_GUI_Context3CheckBox", mergePairings )
		ButtonSetPressedFlag( "WarBoard_ZonePOP_GUI_Context3CheckBox", mergePairings)
		WindowRegisterCoreEventHandler("WarBoard_ZonePOP_GUI_Context3", "OnLButtonUp", "WarBoard_ZonePOP.ContextCheckBox_OnLButtonUp")
		WindowRegisterCoreEventHandler("WarBoard_ZonePOP_GUI_Context3", "OnMouseOver", "WarBoard_ZonePOP.ContextCheckBox_OnMouseOver")
		WindowSetId("WarBoard_ZonePOP_GUI_Context3", 3)
		WindowSetShowing("WarBoard_ZonePOP_GUI_Context3", false)
end

function WarBoard_ZonePOP.OpenMenu()

	-- title
	EA_Window_ContextMenu.CreateContextMenu( "WarBoard_ZonePOP_ContextMenu", 1, L"ZonePOP")
	EA_Window_ContextMenu.AddMenuDivider(1)

	-- Dwarfs vs Greenskins
	EA_Window_ContextMenu.AddCascadingMenuItem( L"Dwarfs vs Greenskins", WarBoard_ZonePOP.CreateDwarfvsGreenskinContextMenu, false, 1 )
	--EA_Window_ContextMenu.AddMenuDivider(1)
	
	-- Chaos vs Empire
	EA_Window_ContextMenu.AddCascadingMenuItem( L"Empire vs Chaos", WarBoard_ZonePOP.CreateChaosvsEmpireContextMenu, false, 1 )
	--EA_Window_ContextMenu.AddMenuDivider(1)

	-- High Elves vs Dark Elves
	EA_Window_ContextMenu.AddCascadingMenuItem( L"High Elves vs Dark Elves", WarBoard_ZonePOP.CreateHighvsDarkContextMenu, false, 1 )
	EA_Window_ContextMenu.AddMenuDivider(1)
		
	-- checkboxes
	EA_Window_ContextMenu.AddUserDefinedMenuItem( "WarBoard_ZonePOP_GUI_Context1", 1 ) -- show tier 1
	EA_Window_ContextMenu.AddUserDefinedMenuItem( "WarBoard_ZonePOP_GUI_Context2", 1 ) -- show forts
	EA_Window_ContextMenu.AddUserDefinedMenuItem( "WarBoard_ZonePOP_GUI_Context3", 1 ) -- show merge pairings	
	EA_Window_ContextMenu.AddMenuItem (L"Run .rvr command", WarBoard_ZonePOP.RunRvrCommand , false, false,1)
	--EA_Window_ContextMenu.AddMenuDivider(1)
	
	-- render the menu
	EA_Window_ContextMenu.Finalize( 1, 
		{ 
			["XOffset"] = 0,
			["YOffset"] = 0,
			["Point"] = "center",
			["RelativePoint"] = "center",
			["RelativeTo"] = "Root",
		} )
end

---------- Dwarfs vs Greenskins ----------
local Dwarf_vs_Greenskins = {
	t1zones = {
		id = {
			6, -- L"Ekrund",
			11, -- L"Mount Bloodhorn",
		},
		callback = {
			ZonePOP_SearchFunctions.SearchZone6,
			ZonePOP_SearchFunctions.SearchZone11,
		}
	},
	regularZones = {
		id = {
			7, -- L"Barak Varr",
			1, -- L"Marshes of Madness",
			2, -- L"The Badlands",
			8, -- L"Black Fire Pass",
			9, -- L"Kadrin Valley",
			5, -- L"Thunder Mountain",
			3, -- L"Black Crag",
		},
		callback = {
			ZonePOP_SearchFunctions.SearchZone7,
			ZonePOP_SearchFunctions.SearchZone1,
			ZonePOP_SearchFunctions.SearchZone2,
			ZonePOP_SearchFunctions.SearchZone8,
			ZonePOP_SearchFunctions.SearchZone9,
			ZonePOP_SearchFunctions.SearchZone5,
			ZonePOP_SearchFunctions.SearchZone3,
		}
	},
	forts = {
		id = {
			-- Forts
			4, -- L"Butcher's Pass",
			10, -- L"Stonewatch",	
		},
		callback = {
			ZonePOP_SearchFunctions.SearchZone4,
			ZonePOP_SearchFunctions.SearchZone10,
		}
	}
}

function WarBoard_ZonePOP.CreateDwarfvsGreenskinContextMenu()

	EA_Window_ContextMenu.CreateContextMenu( "WarBoard_ZonePOP_GUI_DwarfvsGreenskinContextMenu", 2, L"Dwarfs vs Greenskins" )
	EA_Window_ContextMenu.AddMenuDivider(2)
	
	-- tier 1
	if (showTierOne == true) then
		for i, id in ipairs(Dwarf_vs_Greenskins.t1zones.id) do
			local zoneName = GetZoneName(id)
			EA_Window_ContextMenu.AddMenuItem (L""..zoneName, Dwarf_vs_Greenskins.t1zones.callback[i], false, true,2)
		end
	end

	-- regular zones
	for i, id in ipairs(Dwarf_vs_Greenskins.regularZones.id) do
		local zoneName = GetZoneName(id)
		EA_Window_ContextMenu.AddMenuItem (L""..zoneName, Dwarf_vs_Greenskins.regularZones.callback[i], false, true,2)
	end
	
	-- forts
	if (showForts == true) then
		for i, id in ipairs(Dwarf_vs_Greenskins.forts.id) do
			local zoneName = GetZoneName(id)
			EA_Window_ContextMenu.AddMenuItem (L"[F] "..zoneName, Dwarf_vs_Greenskins.forts.callback[i], false, true,2)
		end
	end

	EA_Window_ContextMenu.Finalize( 2, nil)
	
end

---------- Chaos vs Empire ----------
local Chaos_vs_Empire = {
	t1zones = {
		id = {
			100, -- L"Norsca",
			106, -- L"Nordland",
		},
		callback = {
			ZonePOP_SearchFunctions.SearchZone100,
			ZonePOP_SearchFunctions.SearchZone106,
		}
	},
	regularZones = {
		id = {
			107, -- L"Ostland",
			101, -- L"Troll Country",
			102, -- L"High Pass",
			108, -- L"Talabecland",
			103, -- L"Chaos Wastes",
			105, -- L"Praag",
			109, -- L"Reikland",
		},
		callback = {
			ZonePOP_SearchFunctions.SearchZone107,
			ZonePOP_SearchFunctions.SearchZone101,
			ZonePOP_SearchFunctions.SearchZone102,
			ZonePOP_SearchFunctions.SearchZone108,
			ZonePOP_SearchFunctions.SearchZone103,
			ZonePOP_SearchFunctions.SearchZone105,
			ZonePOP_SearchFunctions.SearchZone109,

		}
	},
	forts = {
		id = {
			-- Forts
			104, -- L"The Maw",
			110, -- L"Reikwald",
		},
		callback = {
			ZonePOP_SearchFunctions.SearchZone104,
			ZonePOP_SearchFunctions.SearchZone110,
		}
	}
}

function WarBoard_ZonePOP.CreateChaosvsEmpireContextMenu()

	EA_Window_ContextMenu.CreateContextMenu( "WarBoard_ZonePOP_GUI_ChaosvsEmpireContextMenu", 2, L"Empire vs Chaos" )
	EA_Window_ContextMenu.AddMenuDivider(2)

	-- tier 1
	if (showTierOne == true) then
		for i, id in ipairs(Chaos_vs_Empire.t1zones.id) do
			local zoneName = GetZoneName(id)
			EA_Window_ContextMenu.AddMenuItem (L""..zoneName, Chaos_vs_Empire.t1zones.callback[i], false, true,2)
		end
	end

	-- regular zones
	for i, id in ipairs(Chaos_vs_Empire.regularZones.id) do
		local zoneName = GetZoneName(id)
		EA_Window_ContextMenu.AddMenuItem (L""..zoneName, Chaos_vs_Empire.regularZones.callback[i], false, true,2)
	end
	
	-- forts
	if (showForts == true) then
		for i, id in ipairs(Chaos_vs_Empire.forts.id) do
			local zoneName = GetZoneName(id)
			EA_Window_ContextMenu.AddMenuItem (L"[F] "..zoneName, Chaos_vs_Empire.forts.callback[i], false, true,2)
		end
	end

	EA_Window_ContextMenu.Finalize( 2, nil)

end

---------- High Elves vs Dark Elves ----------
local HighElves_vs_DarkElves = {
	t1zones = {
		id = {
			200, -- L"The Blighted Isle",
			206, -- L"Chrace",
		},
		callback = {
			ZonePOP_SearchFunctions.SearchZone200,
			ZonePOP_SearchFunctions.SearchZone206,
		}
	},
	regularZones = {
		id = {
			--High Elves vs Dark Elves
			201, -- L"The Shadowlands",
			207, -- L"Ellyrion",
			202, -- L"Avelorn",
			208, -- L"Saphery",
			203, -- L"Caledor",
			205, -- L"Dragonwake",
			209, -- L"Eataine"
		},
		callback = {
			ZonePOP_SearchFunctions.SearchZone201,
			ZonePOP_SearchFunctions.SearchZone207,
			ZonePOP_SearchFunctions.SearchZone202,
			ZonePOP_SearchFunctions.SearchZone208,
			ZonePOP_SearchFunctions.SearchZone203,
			ZonePOP_SearchFunctions.SearchZone205,
			ZonePOP_SearchFunctions.SearchZone209,

		}
	},
	forts = {
		id = {
			204, -- L"Fell Landing"
			210 -- L"Shining Way"
		},
		callback = {
			ZonePOP_SearchFunctions.SearchZone204,
			ZonePOP_SearchFunctions.SearchZone210,
		}	
	},
}

function WarBoard_ZonePOP.CreateHighvsDarkContextMenu()

	EA_Window_ContextMenu.CreateContextMenu( "WarBoard_ZonePOP_GUI_HighvsDarkContextMenu", 2, L"High Elves vs Dark Elves" )
	EA_Window_ContextMenu.AddMenuDivider(2)

	---------- High Elves vs Dark Elves ----------
	-- tier 1
	if (showTierOne == true) then
		for i, id in ipairs(HighElves_vs_DarkElves.t1zones.id) do
			local zoneName = GetZoneName(id)
			EA_Window_ContextMenu.AddMenuItem (L""..zoneName, HighElves_vs_DarkElves.t1zones.callback[i], false, true,2)
		end
	end
	
	-- regular zones
	for i, id in ipairs(HighElves_vs_DarkElves.regularZones.id) do
		local zoneName = GetZoneName(id)
		EA_Window_ContextMenu.AddMenuItem (L""..zoneName, HighElves_vs_DarkElves.regularZones.callback[i], false, true,2)
	end
	
	-- forts
	if (showForts == true) then
		for i, id in ipairs(HighElves_vs_DarkElves.forts.id) do
			local zoneName = GetZoneName(id)
			EA_Window_ContextMenu.AddMenuItem (L"[F] "..zoneName, HighElves_vs_DarkElves.forts.callback[i], false, true,2)
		end
	end

	EA_Window_ContextMenu.Finalize( 2, nil)
	
end

function WarBoard_ZonePOP.ContextCheckBox_OnMouseOver()
	EA_Window_ContextMenu.Hide(2)
end

function WarBoard_ZonePOP.ContextCheckBox_OnLButtonUp()

	local buttonId = WindowGetId(SystemData.ActiveWindow.name)
	local toggle = ButtonGetPressedFlag("WarBoard_ZonePOP_GUI_Context" .. buttonId .. "CheckBox") == false 
	
	ButtonSetPressedFlag( "WarBoard_ZonePOP_GUI_Context" .. buttonId .. "CheckBox", toggle )
	
	-- show tier 1
	if (buttonId == 1) then
		if (toggle == true) then
			showTierOne = true
		else
			showTierOne = false
		end
	-- show forts
	elseif (buttonId == 2) then
		if (toggle == true) then
			showForts = true
		else
			showForts = false
		end
	-- merge pairings
	elseif (buttonId == 3) then
		if (toggle == true) then
			mergePairings = true
		else
			mergePairings = false
		end
	end
	
	WarBoard_ZonePOP.OpenMenu()
end

function WarBoard_ZonePOP.RunRvrCommand()
	SendChatText(L".rvr", L"")
end

function WarBoard_ZonePOP.TogglePairingMerge()
	if (mergePairings == true) then
		mergePairings = false
	else
		mergePairings = true
	end
end

function WarBoard_ZonePOP.FixString(str)
	if (str == nil) then
		return nil
	end
	local str = str
	local pos = str:find(L"^", 1, true)
	if (pos) then
		str = str:sub (1, pos - 1)
	end
	return str
end

function WarBoard_ZonePOP.ShowTooltip()
	
	--[[
	Tooltips.CreateTextOnlyTooltip(modName,nil)
	
	if (CurrentZoneName == nil) then
		Tooltips.SetTooltipText(1, 1, L"ZonePOP")
		Tooltips.SetTooltipText(2, 1, L"Right Click: search local zone")
		Tooltips.SetTooltipText(3, 1, L"Middle Click: bring up menu")
	else
		local wstr = towstring(CurrentZoneName.." population")
		Tooltips.SetTooltipText(1, 1, wstr)
	end
	
	Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_TOP)
	Tooltips.Finalize()
	]]--
	
	if (CurrentZoneName == nil) then
		Tooltips.CreateTextOnlyTooltip(modName,nil)
		Tooltips.SetTooltipText(1, 1, L"ZonePOP")
		Tooltips.SetTooltipText(2, 1, L"Left Click: search local zone")
		Tooltips.SetTooltipText(3, 1, L"Middle Click: bring up menu")
		Tooltips.SetTooltipText(4, 1, L"Right Click: send to chat")
		Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_TOP)
		Tooltips.Finalize()
	end
	
	
end

function WarBoard_ZonePOP.OnMouseover()
end

function WarBoard_ZonePOP.OnMouseaway()
end