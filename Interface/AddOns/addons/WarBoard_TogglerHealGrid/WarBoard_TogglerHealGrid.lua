if not WarBoard_TogglerHealGrid then WarBoard_TogglerHealGrid = {} end
local WarBoard_TogglerHealGrid = WarBoard_TogglerHealGrid
local modName = "WarBoard_TogglerHealGrid"
local modLabel = "HealGrid" 

function WarBoard_TogglerHealGrid.Initialize()
	if LibWBToggler.CreateToggler(modName, modLabel, "hg_healgridicon", 0, 0) then
		WindowSetDimensions(modName, 113, 30)
		WindowSetDimensions(modName.."Label", 81, 30)
		LibWBToggler.RegisterEvent(modName, "OnLButtonUp", "WarBoard_TogglerHealGrid.ToggleActive")
		LibWBToggler.RegisterEvent(modName, "OnRButtonUp", "HealGridGui.ToggleGui")
		LibWBToggler.RegisterEvent(modName, "OnMouseOver", "WarBoard_TogglerHealGrid.ShowStatus")
	end
end

function WarBoard_TogglerHealGrid.ToggleActive()
	HealGridGuiTabHUD.StayHiddenClicked()
end

function WarBoard_TogglerHealGrid.ShowStatus()
	LibWBToggler.DefaultTooltip(modName, modLabel)
end
