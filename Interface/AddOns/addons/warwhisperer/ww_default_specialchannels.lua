--[[
	definiton of the default special channels
]]--

warwhisperer_defaultchannels = {}

--[[ PARTY ]]--
warwhisperer_defaultchannels.party = {}
warwhisperer_defaultchannels.party.name = "party"
warwhisperer_defaultchannels.party.id = 5900
warwhisperer_defaultchannels.party.flash = true
warwhisperer_defaultchannels.party.title = L"Party"
warwhisperer_defaultchannels.party.inputFilter = { SystemData.ChatLogFilters.GROUP }
warwhisperer_defaultchannels.party.outputFilter = SystemData.ChatLogFilters.GROUP

--[[ Guild/Alliance ]]--
warwhisperer_defaultchannels.guild = {}
warwhisperer_defaultchannels.guild.name = "guild"
warwhisperer_defaultchannels.guild.id = 5901
warwhisperer_defaultchannels.guild.flash = true
warwhisperer_defaultchannels.guild.title = L"Guild"
warwhisperer_defaultchannels.guild.inputFilter = { SystemData.ChatLogFilters.GUILD, SystemData.ChatLogFilters.GUILD_OFFICER, SystemData.ChatLogFilters.ALLIANCE, SystemData.ChatLogFilters.ALLIANCE_OFFICER}
warwhisperer_defaultchannels.guild.outputFilter = SystemData.ChatLogFilters.GUILD

--[[ SCENARIO ]]--
warwhisperer_defaultchannels.sc = {}
warwhisperer_defaultchannels.sc.name = "sc"
warwhisperer_defaultchannels.sc.id = 5902
warwhisperer_defaultchannels.sc.flash = true
warwhisperer_defaultchannels.sc.title = L"SC"
warwhisperer_defaultchannels.sc.inputFilter = { SystemData.ChatLogFilters.SCENARIO, SystemData.ChatLogFilters.SCENARIO_GROUPS }
warwhisperer_defaultchannels.sc.outputFilter = SystemData.ChatLogFilters.SCENARIO

--[[ BATTLEGROUP ]]--
warwhisperer_defaultchannels.wb = {}
warwhisperer_defaultchannels.wb.name = "wb"
warwhisperer_defaultchannels.wb.id = 5903
warwhisperer_defaultchannels.wb.flash = true
warwhisperer_defaultchannels.wb.title = L"WB"
warwhisperer_defaultchannels.wb.inputFilter = { SystemData.ChatLogFilters.BATTLEGROUP }
warwhisperer_defaultchannels.wb.outputFilter = SystemData.ChatLogFilters.BATTLEGROUP

--[[ CHAT CHANNELS ]]--
warwhisperer_defaultchannels.channels = {}
warwhisperer_defaultchannels.channels.name = "channels"
warwhisperer_defaultchannels.channels.id = 5905
warwhisperer_defaultchannels.channels.flash = false
warwhisperer_defaultchannels.channels.title = L"Chan"
warwhisperer_defaultchannels.channels.inputFilter = { SystemData.ChatLogFilters.SAY, SystemData.ChatLogFilters.CHANNEL_1, SystemData.ChatLogFilters.CHANNEL_2 }
warwhisperer_defaultchannels.channels.outputFilter = SystemData.ChatLogFilters.CHANNEL_1

--[[ ADVICE CHANNEL ]]--
warwhisperer_defaultchannels.advice = {}
warwhisperer_defaultchannels.advice.name = "advice"
warwhisperer_defaultchannels.advice.id = 5906
warwhisperer_defaultchannels.advice.flash = false
warwhisperer_defaultchannels.advice.title = L"Advice"
warwhisperer_defaultchannels.advice.inputFilter = { SystemData.ChatLogFilters.ADVICE }
warwhisperer_defaultchannels.advice.outputFilter = SystemData.ChatLogFilters.ADVICE

--[[ REALWAR CHANNEL ]]--
warwhisperer_defaultchannels.realmwar = {}
warwhisperer_defaultchannels.realmwar.name = "realmwar"
warwhisperer_defaultchannels.realmwar.id = 5907
warwhisperer_defaultchannels.realmwar.flash = false
warwhisperer_defaultchannels.realmwar.title = L"RW"
warwhisperer_defaultchannels.realmwar.inputFilter = { SystemData.ChatLogFilters.REALM_WAR_T1, SystemData.ChatLogFilters.REALM_WAR_T2, SystemData.ChatLogFilters.REALM_WAR_T3, SystemData.ChatLogFilters.REALM_WAR_T4  }
warwhisperer_defaultchannels.realmwar.outputFilter = SystemData.ChatLogFilters.REALM_WAR_T4

TextLogAddEntry("Chat", 0,L"warwhisperer default channels loaded")