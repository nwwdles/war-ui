<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="WarBoard_WarWhisperer" version="0.15" date="22/10/2009" >

		<Author name="smurfy" email="" />
		<Description text="simple whisper helper" />

		<VersionSettings gameVersion="1.4.0" windowsVersion="1.0" savedVariablesVersion="1.0" />    

		<Dependencies>
			<Dependency name="WarBoard" />
			<Dependency name="warwhisperer" />
		</Dependencies>

		<OnInitialize>
			<CallFunction name="warwhisperer.initWarBoard" />
		</OnInitialize>

		<OnUpdate/>

		<OnShutdown/>
	</UiMod>
</ModuleFile>