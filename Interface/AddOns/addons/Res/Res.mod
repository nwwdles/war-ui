<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="Res" version="1.3" date="17/08/2010" >
		<Author name="Medikage" email="medikage@gmail.com" />
		<Description text="The Ressurection Enhancement System (Res) addon, automatically sends a notification, via the chat channel of your choice, that you are casting a ressurection spell and who it is that you are ressurecting.  You can also add your own custom message."/>		
		<VersionSettings gameVersion="1.3.6" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<Dependencies>
			<Dependency name="LibSlash" />
			<Dependency name="EA_ChatWindow" />
			<Dependency name="EASystem_Utils"/>
			<Dependency name="EASystem_WindowUtils"/>
		</Dependencies>
		<Files>
			<File name="Libraries/LibStub.lua" />
			<File name="Libraries/AceLocale-3.0.lua" />
			<File name="Localisation/enUS.lua" />
			<File name="Localisation/deDE.lua" />
			<File name="Localisation/frFR.lua" />
			<File name="Res.xml" />
		</Files>
		<SavedVariables>
			<SavedVariable name="Res.Settings" />
		</SavedVariables>
		<OnInitialize>
			<CallFunction name="Res.Init" />
		</OnInitialize>
		<OnUpdate/>
		<OnShutdown>
			<CallFunction name="Res.Shutdown" />
		</OnShutdown>
		<WARInfo>
			<Categories>
			    <!-- Whilst I realise these are wrong, there is a current bug
					 within warhammer that puts them all out by 1, so in game, 
					 these categories actually align properly. -->
				<Category name="MAIL"/> 
				<Category name="COMBAT"/>
			</Categories>
			<Careers>
				<Career name="DISCIPLE" />
				<Career name="RUNE_PRIEST" />
				<Career name="SHAMAN" />
				<Career name="WARRIOR_PRIEST" />
				<Career name="ZEALOT" />
				<Career name="ARCHMAGE" />
			</Careers>
		</WARInfo>
	</UiMod>
</ModuleFile>


