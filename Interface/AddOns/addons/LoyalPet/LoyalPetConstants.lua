if not LPET_CONSTANTS then LPET_CONSTANTS = {} end
LPET_CONSTANTS.DEBUG_LEVEL = 0
LPET_CONSTANTS.DEBUG_LEVEL_OFF = 0
LPET_CONSTANTS.DEBUG_LEVEL_1 = 1
LPET_CONSTANTS.DEBUG_LEVEL_2 = 2
LPET_CONSTANTS.DEBUG_FUNC_FLAG = false
LPET_CONSTANTS.COMPATIBLE_VERSION = "2.9"
LPET_CONSTANTS.VERSION = "2.9.2"
LPET_CONSTANTS.PET_NO_COMMAND = 0
LPET_CONSTANTS.PET_FOLLOW = 1
LPET_CONSTANTS.PET_ATTACK = 2
LPET_CONSTANTS.ATTACK_OVERRIDE = false
LPET_CONSTANTS.FOLLOW_OVERRIDE = false
LPET_CONSTANTS.WAIT_FOR_PET_ATTACKS = false
LPET_CONSTANTS.WHITE_LION = 102
LPET_CONSTANTS.SQUIG_HERDER = 27
LPET_CONSTANTS.MAGUS = 67
LPET_CONSTANTS.ENGINEER = 23
LPET_CONSTANTS.ALL_CAREERS = 0
LPET_CONSTANTS.AUTO_ATTACK = 2490
LPET_CONSTANTS.NATURE_BOND = 9164
LPET_CONSTANTS.ABILITY_TIME_DELAY = 0.33
LPET_CONSTANTS.FOLLOW_TIME_DELAY = 1.0
LPET_CONSTANTS.ATTACK_TIME_DELAY = 1.0
LPET_CONSTANTS.STATE_TIME_DELAY = 1.0
LPET_CONSTANTS.MOUNT_TIME_DELAY = 0.50
LPET_CONSTANTS.NO_CAST = 1
LPET_CONSTANTS.AUTO_CAST = 2
LPET_CONSTANTS.MANUAL_CAST = 3
LPET_CONSTANTS.IGNORE_CAST = 4
LPET_CONSTANTS.OFF = 1
LPET_CONSTANTS.ON = 2
LPET_CONSTANTS.NEED_INIT_UPDATE = false
LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT = 5
LPET_CONSTANTS.DEFAULT_PRIORITY = 1
LPET_CONSTANTS.MAX_PROFILES = 4
--The following table is borrowed from the AutoDismount addon.
--Great Addon.  If you don't have it, get it at http://war.curse.com/downloads/war-addons/details/dismount.aspx
LPET_CONSTANTS.MountAbilities = 
{
-- this is every "Summon Mount" ability in the game
[14593]=true,--	; TEST Magus Basic mounts^n
[14594]=true,--	Summon Mount^n
[14595]=true,--	Summon Mount^n
[14596]=true,--	Summon Mount^n
[14597]=true,--	Summon Mount^n
[14598]=true,--	Summon Mount^n
[14599]=true,--	Summon Mount^n
[14600]=true,--	Summon Mount^n
[14601]=true,--	Summon Mount^n
[14602]=true,--	Summon Mount^n
[14603]=true,--	Summon Mount^n
[14604]=true,--	Summon Mount^n
[14605]=true,--	Summon Mount^n
[14606]=true,--	Summon Mount^n
[14607]=true,--	Summon Mount^n
[14608]=true,--	Summon Mount^n
[14609]=true,--	Summon Mount^n
[14610]=true,--	Summon Mount^n
[14611]=true,--	Summon Mount^n
[14612]=true,--	Summon Mount^n
[14613]=true,--	Summon Mount^n
[14614]=true,--	Summon Mount^n
[14615]=true,--	Summon Mount^n
[14616]=true,--	Summon Mount^n
[14617]=true,--	Summon Mount^n
[14618]=true,--	Summon Mount^n
[14619]=true,--	Summon Mount^n
[14620]=true,--	Summon Mount^n
[14621]=true,--	Summon Mount^n
[14622]=true,--	Summon Mount^n
[14623]=true,--	Summon Mount^n
[14624]=true,--	Summon Mount^n
[14625]=true,--	Summon Mount^n
[14626]=true,--	Summon Mount^n
[14627]=true,--	Summon Mount^n
[14628]=true,--	Summon Mount^n
[14629]=true,--	Summon Mount^n
[14630]=true,--	Summon Mount^n
[14631]=true,--	Summon Mount^n
[14632]=true,--	Summon Mount^n
[14633]=true,--	Summon Mount^n
[14634]=true,--	Summon Mount^n
[14635]=true,--	Summon Mount^n
[14636]=true,--	Summon Mount^n
[14637]=true,--	Summon Mount^n
[14638]=true,--	Summon Mount^n
[14639]=true,--	Summon Mount^n
[14640]=true,--	Summon Mount^n
[14641]=true,--	Summon Mount^n
[14669]=true,--	; TEST Magus Mounts^n
[14670]=true,--	Summon Mount^n
[14671]=true,--	; TEST Dwarf Mounts^n
[14672]=true,--	Summon Mount^n
[14673]=true,--	; TEST Goblin Mounts^n
[14674]=true,--	Summon Mount^n
[14675]=true,--	; TEST Orc Mounts^n
[14676]=true,--	Summon Mount^n
[14677]=true,--	; TEST Empire Mounts^n
[14678]=true,--	Summon Mount^n
[14679]=true,--	; TEST Chaos Mounts^n
[14680]=true,--	Summon Mount^n
[14681]=true,--	; TEST High Elf Mounts^n
[14682]=true,--	Summon Mount^n
[14683]=true,--	; TEST Dark Elf Mounts^n
[14684]=true,--	Summon Mount^n
[14782]=true,--	; TEST Dwarf Light mounts^n
[14783]=true,--	Summon Mount^n
[14784]=true,--	Summon Mount^n
[14785]=true,--	Summon Mount^n
[14786]=true,--	Summon Mount^n
[14787]=true,--	Summon Mount^n
[14788]=true,--	Summon Mount^n
[14789]=true,--	; TEST Goblin Light mounts^n
[14790]=true,--	Summon Mount^n
[14791]=true,--	Summon Mount^n
[14792]=true,--	Summon Mount^n
[14793]=true,--	Summon Mount^n
[14794]=true,--	Summon Mount^n
[14795]=true,--	Summon Mount^n
[14796]=true,--	; TEST Orc Light mounts^n
[14797]=true,--	Summon Mount^n
[14798]=true,--	Summon Mount^n
[14799]=true,--	Summon Mount^n
[14800]=true,--	Summon Mount^n
[14801]=true,--	Summon Mount^n
[14802]=true,--	Summon Mount^n
[14803]=true,--	; TEST Empire Light mounts^n
[14804]=true,--	Summon Mount^n
[14805]=true,--	Summon Mount^n
[14806]=true,--	Summon Mount^n
[14807]=true,--	Summon Mount^n
[14808]=true,--	Summon Mount^n
[14809]=true,--	Summon Mount^n
[14810]=true,--	; TEST Chaos Light mounts^n
[14811]=true,--	Summon Mount^n
[14812]=true,--	Summon Mount^n
[14813]=true,--	Summon Mount^n
[14814]=true,--	Summon Mount^n
[14815]=true,--	Summon Mount^n
[14816]=true,--	Summon Mount^n
[14817]=true,--	; TEST High Elf Light mounts^n
[14818]=true,--	Summon Mount^n
[14819]=true,--	Summon Mount^n
[14820]=true,--	Summon Mount^n
[14821]=true,--	Summon Mount^n
[14822]=true,--	Summon Mount^n
[14823]=true,--	Summon Mount^n
[14824]=true,--	; TEST Dark Elf Light mounts^n
[14825]=true,--	Summon Mount^n
[14826]=true,--	Summon Mount^n
[14827]=true,--	Summon Mount^n
[14828]=true,--	Summon Mount^n
[14829]=true,--	Summon Mount^n
[14830]=true,--	Summon Mount^n
[14831]=true,--	; TEST Dwarf Heavy mounts^n
[14832]=true,--	Summon Mount^n
[14833]=true,--	Summon Mount^n
[14834]=true,--	Summon Mount^n
[14835]=true,--	Summon Mount^n
[14836]=true,--	Summon Mount^n
[14837]=true,--	Summon Mount^n
[14838]=true,--	; TEST Goblin Heavy mounts^n
[14839]=true,--	Summon Mount^n
[14840]=true,--	Summon Mount^n
[14841]=true,--	Summon Mount^n
[14842]=true,--	Summon Mount^n
[14843]=true,--	Summon Mount^n
[14844]=true,--	Summon Mount^n
[14845]=true,--	; TEST Orc Heavy mounts^n
[14846]=true,--	Summon Mount^n
[14847]=true,--	Summon Mount^n
[14848]=true,--	Summon Mount^n
[14849]=true,--	Summon Mount^n
[14850]=true,--	Summon Mount^n
[14851]=true,--	Summon Mount^n
[14852]=true,--	; TEST Empire Heavy mounts^n
[14853]=true,--	Summon Mount^n
[14854]=true,--	Summon Mount^n
[14855]=true,--	Summon Mount^n
[14856]=true,--	Summon Mount^n
[14857]=true,--	Summon Mount^n
[14858]=true,--	Summon Mount^n
[14979]=true,--	; TEST Chaos Heavy mounts^n
[14980]=true,--	Summon Mount^n
[14981]=true,--	Summon Mount^n
[14982]=true,--	Summon Mount^n
[14983]=true,--	Summon Mount^n
[14984]=true,--	Summon Mount^n
[14985]=true,--	Summon Mount^n
[14986]=true,--	; TEST High Elf Heavy mounts^n
[14987]=true,--	Summon Mount^n
[14988]=true,--	Summon Mount^n
[14989]=true,--	Summon Mount^n
[14990]=true,--	Summon Mount^n
[14991]=true,--	Summon Mount^n
[14992]=true,--	Summon Mount^n
[14993]=true,--	; TEST Dark Elf Heavy mounts^n
[14994]=true,--	Summon Mount^n
[14995]=true,--	Summon Mount^n
[14996]=true,--	Summon Mount^n
[14997]=true,--	Summon Mount^n
[14998]=true,--	Summon Mount^n
[14999]=true,--	Summon Mount^n
[15601]=true,--	; TEST Magus Light mounts^n
[15602]=true,--	Summon Mount^n
[15603]=true,--	Summon Mount^n
[15604]=true,--	Summon Mount^n
[15605]=true,--	Summon Mount^n
[15606]=true,--	Summon Mount^n
[15607]=true,--	Summon Mount^n
[15608]=true,--	; TEST Magus Heavy mounts^n
[15609]=true,--	Summon Mount^n
[15610]=true,--	Summon Mount^n
[15611]=true,--	Summon Mount^n
[15612]=true,--	Summon Mount^n
[15613]=true,--	Summon Mount^n
[15614]=true,--	Summon Mount^n
[15051]=true,--   Summon Mount (?)
[14970]=true,--   Summon Manticore (Referral Mount?)
[14971]=true,--   Summon Griffon (Referral Mount)
[14972]=true,--   Summon Manticore (Referral Mount)
[14973]=true,--   Summon Manticore Bone Disc (Referral Mount)
[14974]=true,--   Summon Manticore (Referral Mount?)
}
----------------------------------------------------------------
--Constant Default Configuration
if not DEFAULT_CONFIG then DEFAULT_CONFIG = {} end
DEFAULT_CONFIG.name = "Default"
DEFAULT_CONFIG.version = "1.0.0"
DEFAULT_CONFIG.career = 0
DEFAULT_CONFIG.autofollow = LPET_CONSTANTS.OFF
DEFAULT_CONFIG.autoswitch = LPET_CONSTANTS.OFF
DEFAULT_CONFIG.switchrangecheck = LPET_CONSTANTS.OFF
DEFAULT_CONFIG.autoattack = LPET_CONSTANTS.OFF
DEFAULT_CONFIG.attackrangecheck = LPET_CONSTANTS.OFF
DEFAULT_CONFIG.autodefend = LPET_CONSTANTS.OFF
DEFAULT_CONFIG.defendrangecheck = LPET_CONSTANTS.OFF
DEFAULT_CONFIG.selftargetfollow = LPET_CONSTANTS.OFF
DEFAULT_CONFIG.attackbuttoncombo = LPET_CONSTANTS.OFF
DEFAULT_CONFIG.followbuttoncombo = LPET_CONSTANTS.OFF
-- White Lion Settings
DEFAULT_CONFIG.LionsRoar = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
DEFAULT_CONFIG.LegTear = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
DEFAULT_CONFIG.Shred = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
DEFAULT_CONFIG.GutRipper = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
DEFAULT_CONFIG.Maul = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
DEFAULT_CONFIG.FangAndClaw = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
DEFAULT_CONFIG.TerrifyingRoar = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
DEFAULT_CONFIG.Bite = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
DEFAULT_CONFIG.ClawSweep = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
-- Engineer Settings
DEFAULT_CONFIG.PenetratingRound = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
DEFAULT_CONFIG.Flamethrower = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
DEFAULT_CONFIG.ShockGrenade = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
DEFAULT_CONFIG.HighExplosiveGrenade = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
DEFAULT_CONFIG.MachineGun = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
DEFAULT_CONFIG.SteamVent = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
-- Magus Settings
DEFAULT_CONFIG.DaemonicFire = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
DEFAULT_CONFIG.WarpingEnergy = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
DEFAULT_CONFIG.FlameOfTzeentch = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
DEFAULT_CONFIG.FlamesOfChange = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
DEFAULT_CONFIG.CoruscatingEnergy = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
DEFAULT_CONFIG.DaemonicConsumption = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
-- Squig Herder Settings
DEFAULT_CONFIG.SquigSqueal = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
DEFAULT_CONFIG.Gore = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
DEFAULT_CONFIG.GoopShootin = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
DEFAULT_CONFIG.PoisonedSpine = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
DEFAULT_CONFIG.SpineFling = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
DEFAULT_CONFIG.SporeCloud = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
DEFAULT_CONFIG.HeadButt = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
DEFAULT_CONFIG.DeathFromAbove = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
----------------------------------------------------------------
if not LPET_CONFIG then LPET_CONFIG = {} end
LPET_CONFIG.name = "Custom"
LPET_CONFIG.version = "1.0.0"
LPET_CONFIG.career = 0
LPET_CONFIG.autofollow = LPET_CONSTANTS.OFF
LPET_CONFIG.autoswitch = LPET_CONSTANTS.OFF
LPET_CONFIG.switchrangecheck = LPET_CONSTANTS.OFF
LPET_CONFIG.autoattack = LPET_CONSTANTS.OFF
LPET_CONFIG.attackrangecheck = LPET_CONSTANTS.OFF
LPET_CONFIG.autodefend = LPET_CONSTANTS.OFF
LPET_CONFIG.defendrangecheck = LPET_CONSTANTS.OFF
LPET_CONFIG.selftargetfollow = LPET_CONSTANTS.OFF
LPET_CONFIG.attackbuttoncombo = LPET_CONSTANTS.OFF
LPET_CONFIG.followbuttoncombo = LPET_CONSTANTS.OFF
-- White Lion Settings
LPET_CONFIG.LionsRoar = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
LPET_CONFIG.LegTear = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
LPET_CONFIG.Shred = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
LPET_CONFIG.GutRipper = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
LPET_CONFIG.Maul = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
LPET_CONFIG.FangAndClaw = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
LPET_CONFIG.TerrifyingRoar = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
LPET_CONFIG.Bite = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
LPET_CONFIG.ClawSweep = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
-- Engineer Settings
LPET_CONFIG.PenetratingRound = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
LPET_CONFIG.Flamethrower = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
LPET_CONFIG.ShockGrenade = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
LPET_CONFIG.HighExplosiveGrenade = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
LPET_CONFIG.MachineGun = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
LPET_CONFIG.SteamVent = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
-- Magus Settings
LPET_CONFIG.DaemonicFire = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
LPET_CONFIG.WarpingEnergy = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
LPET_CONFIG.FlameOfTzeentch = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
LPET_CONFIG.FlamesOfChange = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
LPET_CONFIG.CoruscatingEnergy = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
LPET_CONFIG.DaemonicConsumption = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
-- Squig Herder Settings
LPET_CONFIG.SquigSqueal = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
LPET_CONFIG.Gore = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
LPET_CONFIG.GoopShootin = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
LPET_CONFIG.PoisonedSpine = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
LPET_CONFIG.SpineFling = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
LPET_CONFIG.SporeCloud = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
LPET_CONFIG.HeadButt = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}
LPET_CONFIG.DeathFromAbove = { mode = LPET_CONSTANTS.AUTO_CAST, priority = LPET_CONSTANTS.DEFAULT_PRIORITY, healthLimit = LPET_CONSTANTS.DEFAULT_HEALTH_LIMIT}