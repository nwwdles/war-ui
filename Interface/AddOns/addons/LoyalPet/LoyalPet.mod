﻿<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="LoyalPet" version="2.9.2" date="26/10/2009" >
    <VersionSettings gameVersion="1.3.2" windowsVersion="1.0" savedVariablesVersion="1.0"/>

        <Author name="AxeNarwhal" email="avalis@onmyinternet.info" />
        <Description text="All-In-One Addon For Your Pets" />
        
        <Files>
          <File name="TargetInfoFix.lua" />
          <File name="LoyalPetConstants.lua" />
          <File name="LoyalPetActions.lua" />
            <File name="LoyalPet.lua" />
            <File name="gui/locals/localization.lua" />
            <File name="gui/lpet_gui_setup.lua" />
            <File name="gui/whitelion_gui.lua" />
            <File name="gui/engineer_gui.lua" />
            <File name="gui/magus_gui.lua" />
            <File name="gui/squigherder_gui.lua" />
            <File name="gui/lpet_gui.xml" />
            <File name="gui/lpet_gui.lua" />
        </Files>
        <SavedVariables>
            <SavedVariable name="LPET_CONFIG" />
            <SavedVariable name="LPET_PROFILES" />
    </SavedVariables>
        <Dependencies>
            <Dependency name="EA_AlertTextWindow" />
            <Dependency name="EA_ChatWindow" />
            <Dependency name="EA_ActionBars" />
            <Dependency name="EA_MoraleWindow" />
            <Dependency name="EA_CareerResourcesWindow" />
            <Dependency name="EASystem_TargetInfo" />
            <Dependency name="EATemplate_DefaultWindowSkin" />
            <Dependency name="EASystem_WindowUtils" />
            <Dependency name="EASystem_Utils" />            
            <Dependency name="EA_LegacyTemplates" />
            <Dependency name="EASystem_Tooltips" />
            <Dependency name="LibTargetInfo" optional="true" forceEnable="true" />
         </Dependencies>
        <OnInitialize>
            <CallFunction name="LPET.Initialize" />
        </OnInitialize>
        <OnUpdate>
            <CallFunction name="LPET.OnUpdate" />
        </OnUpdate>
        <OnShutdown>
            <CallFunction name="LPET.Shutdown" />
        </OnShutdown>
    </UiMod>
</ModuleFile>