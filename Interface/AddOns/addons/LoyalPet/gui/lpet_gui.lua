local options = LPET.OPTIONS.Windows

local string_find = string.find
local floor = math.floor
local towstring = towstring

local selected_tab = "LPETOptionsTabAuto"

----------------------
--Called when option page loads
function LPET.OptionsOnInitialize()
  --Misc Options
  LPET.UpdateGUISettings()
  for k,v in pairs(options) do
    --titles
    if v.type == "title" then
      LabelSetText(k.."Text", v.label)
    elseif v.type == "label" then
      LabelSetText(k, v.label)
    elseif v.type == "text" then
      TextEditBoxSetText(k, v.value)
    --tabs
    elseif v.type == "button" then
      ButtonSetText(k, v.label)
    --comboboxes
    elseif v.type == "combo" then
      for id, data in ipairs(v.data) do
        if data.label then
          ComboBoxAddMenuItem(k, data.label)
        else
          ComboBoxAddMenuItem(k, data)
        end
      end
      ComboBoxSetSelectedMenuItem(k, v.value)
    end
    
    if v.career == GameData.Player.career.id or v.career == LPET_CONSTANTS.ALL_CAREERS then
      WindowSetShowing(k, true)
    else
      WindowSetShowing(k, false)
    end
  end

  --set tab
  selected_tab = "LPETOptionsTabAuto"
  LPET:SetTab(selected_tab)
end

----------------------
--reset options (not pretty but don't feel like writing something less lazy =) )
function LPET.ResetOptions()
  LPET.UpdateGUISettings()
  for k,v in pairs(options) do
    --titles
    --comboboxes
    if v.type == "combo" then
      ComboBoxSetSelectedMenuItem(k, v.value)
    end
    
    if v.career == GameData.Player.career.id or v.career == LPET_CONSTANTS.ALL_CAREERS then
      WindowSetShowing(k, true)
    else 
      WindowSetShowing(k, false)
    end
  end
  --set tab
  LPET:SetTab(selected_tab)
end

function LPET.UpdateGUISettings()
  options.LPETOptionsQuickSettingsCombo.value = LPET.quickSettings
  options.LPETOptionsAutoSwitchCombo.value = LPET_CONFIG.autoswitch
  options.LPETOptionsSwitchRangeCheckCombo.value = LPET_CONFIG.switchrangecheck
  options.LPETOptionsAutoAttackCombo.value = LPET_CONFIG.autoattack
  options.LPETOptionsAttackRangeCheckCombo.value = LPET_CONFIG.attackrangecheck
  options.LPETOptionsAutoDefendCombo.value = LPET_CONFIG.autodefend
  options.LPETOptionsDefendRangeCheckCombo.value = LPET_CONFIG.defendrangecheck
--  options.LPETOptionsAutoFollowCombo.value = LPET_CONFIG.autofollow
  options.LPETOptionsSelfFollowCombo.value = LPET_CONFIG.selftargetfollow
  options.LPETOptionsPetAttackCombo.value = LPET_CONFIG.attackbuttoncombo
  options.LPETOptionsPetFollowCombo.value = LPET_CONFIG.followbuttoncombo
  options.LPETOptionsProfilesCombo.value = LPET.currentProfileSelected
  --WhiteLion Abilities
  if GameData.Player.career.id == LPET_CONSTANTS.WHITE_LION then
    LPET.UpdateWhiteLionGUI()
  end
  --Engineer Abilities
  if GameData.Player.career.id == LPET_CONSTANTS.ENGINEER then
    LPET.UpdateEngineerGUI()
  end
  --Magus Abilities
  if GameData.Player.career.id == LPET_CONSTANTS.MAGUS then
    LPET.UpdateMagusGUI()
  end
  --SquigHerder Abilities
  if GameData.Player.career.id == LPET_CONSTANTS.SQUIG_HERDER then
    LPET.UpdateSquigHerderGUI()
  end
end

function LPET.ResetProfilesCombo()
  local formatString = ""
  for i = 1, #LPET_PROFILES do
    formatString = tostring(i) .. " - " .. LPET_PROFILES[i].name
    LPET.LOCALS.OPTION.PROFILECOMBO[i] = towstring(formatString)
  end
  
  --Reset combo box
  local oldSelectedItem = LPET.currentProfileSelected
  ComboBoxClearMenuItems("LPETOptionsProfilesCombo")
  for id, data in ipairs(options.LPETOptionsProfilesCombo.data) do
    if data.label then
      ComboBoxAddMenuItem("LPETOptionsProfilesCombo", data.label)
    else
      ComboBoxAddMenuItem("LPETOptionsProfilesCombo", data)
    end
  end
  LPET.currentProfileSelected = oldSelectedItem
  options.LPETOptionsProfilesCombo.value = LPET.currentProfileSelected
  ComboBoxSetSelectedMenuItem("LPETOptionsProfilesCombo", options.LPETOptionsProfilesCombo.value)
end
----------------------
--Called to open or create the option window
function LPET.OpenMenu()
  LPET.showGUI = true
  if not (DoesWindowExist("LPETOptions")) then
    CreateWindow("LPETOptions", false)
  end
  LPET.ResetProfilesCombo()
  WindowSetShowing("LPETOptions", true)
end

----------------------
--Called to close option window
function LPET.HideMenu()
  LPET.showGUI = false
  WindowSetShowing("LPETOptions", false)
end

----------------------
--Called when opened
function LPET.OnShown()
  LPET.showGUI = true
  WindowUtils.OnShown(LPET.HideMenu, WindowUtils.Cascade.MODE_NONE)
  --reset options
  LPET.ResetOptions()
end

----------------------
--Called when closed
function LPET.OnHidden()
  LPET.showGUI = false
  WindowUtils.OnHidden()
end

----------------------
--When Tab is clicked
function LPET.TabOnLButtonUp()
  local tab = SystemData.ActiveWindow.name
  selected_tab = tab
  LPET:SetTab(selected_tab)
end

----------------------
--Set the active tab
function LPET:SetTab(tab)
  for k,v in pairs(LPET.OPTIONS.TABS) do
    if (k ~= tab) then
      ButtonSetPressedFlag(k, false)
      WindowSetShowing(v, false)
    else
      ButtonSetPressedFlag(k, true)
      WindowSetShowing(v, true)
    end
  end
end

----------------------
--default tooltip style
function LPET:MakeTooltip(window, text)
  Tooltips.CreateTextOnlyTooltip (window, text)
  Tooltips.SetTooltipColorDef (1, 1, Tooltips.COLOR_HEADING)
  Tooltips.AnchorTooltip (Tooltips.ANCHOR_WINDOW_RIGHT)
  Tooltips.Finalize ()
end

----------------------
--When Mousing Over
function LPET.OnMouseOver()
  local window = SystemData.ActiveWindow.name
  local parent1 = WindowGetParent(window)
  local parent2 = WindowGetParent(parent1)
  if (options[window] and options[window].tooltiptext) then
    LPET:MakeTooltip(window, options[window].tooltiptext)
  elseif (options[parent1] and options[parent1].tooltiptext) then
    LPET:MakeTooltip(window, options[parent1].tooltiptext)
  elseif (options[parent2] and options[parent2].tooltiptext) then
    LPET:MakeTooltip(window, options[parent2].tooltiptext)
  end
end

----------------------
--When combo box selection
function LPET.QuickSettingsComboOnSelChanged(index)
  LPET_CONFIG.quickSettings = index
  options.LPETOptionsQuickSettingsCombo.value = index
  LPET.SetAllGeneralSettings(index)
  LPET_CONFIG.quickSettings = 0
  LPET.RefreshGUI()
end

function LPET.AutoSwitchComboOnSelChanged(index)
  LPET_CONFIG.autoswitch = index
  options.LPETOptionsAutoSwitchCombo.value = index

  if index == LPET_CONSTANTS.ON then
    LPET_CONFIG.autoattack = LPET_CONSTANTS.OFF
    options.LPETOptionsAutoAttackCombo.value = LPET_CONSTANTS.OFF
    LPET_CONFIG.autodefend = LPET_CONSTANTS.OFF
    options.LPETOptionsAutoDefendCombo.value = LPET_CONSTANTS.OFF
    LPET.RefreshGUI()
  end
end

function LPET.SwitchRangeCheckComboOnSelChanged(index)
  LPET_CONFIG.switchrangecheck = index
  options.LPETOptionsSwitchRangeCheckCombo.value = index
end

function LPET.AutoAttackComboOnSelChanged(index)
  LPET_CONFIG.autoattack = index
  options.LPETOptionsAutoAttackCombo.value = index

  if index == LPET_CONSTANTS.ON then
    LPET_CONFIG.autoswitch = LPET_CONSTANTS.OFF
    options.LPETOptionsAutoSwitchCombo.value = LPET_CONSTANTS.OFF
    LPET.RefreshGUI()
  end
end

function LPET.AttackRangeCheckComboOnSelChanged(index)
  LPET_CONFIG.attackrangecheck = index
  options.LPETOptionsAttackRangeCheckCombo.value = index
end

function LPET.AutoDefendComboOnSelChanged(index)
  LPET_CONFIG.autodefend = index
  options.LPETOptionsAutoDefendCombo.value = index
  
  if index == LPET_CONSTANTS.ON then
    LPET_CONFIG.autoswitch = LPET_CONSTANTS.OFF
    options.LPETOptionsAutoSwitchCombo.value = LPET_CONSTANTS.OFF
    LPET.RefreshGUI()
  end  
end

function LPET.DefendRangeCheckComboOnSelChanged(index)
  LPET_CONFIG.defendrangecheck = index
  options.LPETOptionsDefendRangeCheckCombo.value = index
end

function LPET.AutoFollowComboOnSelChanged(index)
  LPET_CONFIG.autofollow = index
  options.LPETOptionsAutoFollowCombo.value = index
end

function LPET.SelfFollowComboOnSelChanged(index)
  LPET_CONFIG.selftargetfollow = index
  options.LPETOptionsSelfFollowCombo.value = index
end

function LPET.PetAttackComboOnSelChanged(index)
  LPET_CONFIG.attackbuttoncombo = index
  options.LPETOptionsPetAttackCombo.value = index
end

function LPET.PetFollowComboOnSelChanged(index)
  LPET_CONFIG.followbuttoncombo = index
  options.LPETOptionsPetFollowCombo.value = index
end

function LPET.ProfilesComboOnSelChanged(index)
  LPET.currentProfileSelected = index
  options.LPETOptionsProfilesCombo.value = index
end

function LPET.QuickModeOnSelChanged(index)
  --WhiteLion Abilities
  if GameData.Player.career.id == LPET_CONSTANTS.WHITE_LION then
    LPET.LionComboOnSelChanged(index)
  end
  --Engineer Abilities
  if GameData.Player.career.id == LPET_CONSTANTS.ENGINEER then
    LPET.TurretComboOnSelChanged(index)
  end
  --Magus Abilities
  if GameData.Player.career.id == LPET_CONSTANTS.MAGUS then
    LPET.MagusComboOnSelChanged(index)
  end
  --SquigHerder Abilities
  if GameData.Player.career.id == LPET_CONSTANTS.SQUIG_HERDER then
    LPET.SquigComboOnSelChanged(index)
  end
  LPET.quickMode = 0
  LPET.RefreshGUI()
end

function LPET.QuickPriorityOnSelChanged(index)
  --WhiteLion Abilities
  if GameData.Player.career.id == LPET_CONSTANTS.WHITE_LION then
    LPET.LionPriorityComboOnSelChanged(index)
  end
  --Engineer Abilities
  if GameData.Player.career.id == LPET_CONSTANTS.ENGINEER then
    LPET.TurretPriorityComboOnSelChanged(index)
  end
  --Magus Abilities
  if GameData.Player.career.id == LPET_CONSTANTS.MAGUS then
    LPET.DemonPriorityComboOnSelChanged(index)
  end
  --SquigHerder Abilities
  if GameData.Player.career.id == LPET_CONSTANTS.SQUIG_HERDER then
    LPET.SquigPriorityComboOnSelChanged(index)
  end
  LPET.quickPriority = 0
  LPET.RefreshGUI()
end

function LPET.QuickHealthOnSelChanged(index)
  --WhiteLion Abilities
  if GameData.Player.career.id == LPET_CONSTANTS.WHITE_LION then
    LPET.LionHealthComboOnSelChanged(index)
  end
  --Engineer Abilities
  if GameData.Player.career.id == LPET_CONSTANTS.ENGINEER then
    LPET.TurretHealthComboOnSelChanged(index)
  end
  --Magus Abilities
  if GameData.Player.career.id == LPET_CONSTANTS.MAGUS then
    LPET.DemonHealthComboOnSelChanged(index)
  end
  --SquigHerder Abilities
  if GameData.Player.career.id == LPET_CONSTANTS.SQUIG_HERDER then
    LPET.SquigHealthComboOnSelChanged(index)
  end
  LPET.quickHealthLimit = -5
  LPET.RefreshGUI()
end
-----------------------
function LPET.RefreshGUI()
  LPET.HideMenu()
  LPET.OpenMenu()
end

function LPET.LoadProfileOnButtonUp()
  LPET.LoadProfile(LPET.currentProfileSelected)
  LPET.ResetOptions()
  LPET.RefreshGUI()
end

function LPET.SaveProfileOnButtonUp()
  LPET.SaveProfile(LPET.currentProfileSelected)  
  LPET.ResetOptions()
  options.LPETOptionsProfilesEdit.value = TextEditBoxGetText("LPETOptionsProfilesEdit")
  if options.LPETOptionsProfilesEdit.value == L""  or options.LPETOptionsProfilesEdit.value == nil then
    LPET.RefreshGUI()
    return
  end
  LPET.RenameProfile(LPET.currentProfileSelected, WStringToString(options.LPETOptionsProfilesEdit.value))
  TextEditBoxSetText("LPETOptionsProfilesEdit", L"")
  LPET.RefreshGUI()
end

function LPET.RenameProfileOnButtonUp()
  options.LPETOptionsProfilesEdit.value = TextEditBoxGetText("LPETOptionsProfilesEdit")
  if options.LPETOptionsProfilesEdit.value == L""  or options.LPETOptionsProfilesEdit.value == nil then
    return
  end
  LPET.RenameProfile(LPET.currentProfileSelected, WStringToString(options.LPETOptionsProfilesEdit.value))
  LPET.ResetOptions()
  TextEditBoxSetText("LPETOptionsProfilesEdit", L"")
  LPET.RefreshGUI()
end

function LPET.AddProfileOnButtonUp()
  LPET.AddProfile()
  LPET.ResetOptions()
  options.LPETOptionsProfilesEdit.value = TextEditBoxGetText("LPETOptionsProfilesEdit")
  if options.LPETOptionsProfilesEdit.value == L""  or options.LPETOptionsProfilesEdit.value == nil then
    LPET.RefreshGUI()
    return
  end
  LPET.RenameProfile(LPET.currentProfileSelected, WStringToString(options.LPETOptionsProfilesEdit.value))
  TextEditBoxSetText("LPETOptionsProfilesEdit", L"")
  LPET.RefreshGUI()
end

function LPET.RemoveProfileOnButtonUp()
  local removed = table.remove(LPET.LOCALS.OPTION.PROFILECOMBO, LPET.currentProfileSelected)
  LPET.RemoveProfile(LPET.currentProfileSelected)
  LPET.ResetOptions()
  LPET.RefreshGUI()
end