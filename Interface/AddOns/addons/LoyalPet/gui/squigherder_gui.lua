--Labels
LPET.OPTIONS.Windows.LPETOptionsSquigSquealLabel= {type="label", career=LPET_CONSTANTS.SQUIG_HERDER, label=LPET.LOCALS.OPTION.SQUIGSQUEALTITLE.label, tooltiptext=LPET.LOCALS.OPTION.SQUIGSQUEALTITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsGoreLabel= {type="label", career=LPET_CONSTANTS.SQUIG_HERDER, label=LPET.LOCALS.OPTION.GORETITLE.label, tooltiptext=LPET.LOCALS.OPTION.GORETITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsGoopShootinLabel= {type="label", career=LPET_CONSTANTS.SQUIG_HERDER, label=LPET.LOCALS.OPTION.GOOPSHOOTINTITLE.label, tooltiptext=LPET.LOCALS.OPTION.GOOPSHOOTINTITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsPoisonedSpineLabel= {type="label", career=LPET_CONSTANTS.SQUIG_HERDER, label=LPET.LOCALS.OPTION.POISONEDSPINETITLE.label, tooltiptext=LPET.LOCALS.OPTION.POISONEDSPINETITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsSpineFlingLabel= {type="label", career=LPET_CONSTANTS.SQUIG_HERDER, label=LPET.LOCALS.OPTION.SPINEFLINGTITLE.label, tooltiptext=LPET.LOCALS.OPTION.SPINEFLINGTITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsSporeCloudLabel= {type="label", career=LPET_CONSTANTS.SQUIG_HERDER, label=LPET.LOCALS.OPTION.SPORECLOUDTITLE.label, tooltiptext=LPET.LOCALS.OPTION.SPORECLOUDTITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsHeadButtLabel= {type="label", career=LPET_CONSTANTS.SQUIG_HERDER, label=LPET.LOCALS.OPTION.HEADBUTTTITLE.label, tooltiptext=LPET.LOCALS.OPTION.HEADBUTTTITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsDeathFromAboveLabel= {type="label", career=LPET_CONSTANTS.SQUIG_HERDER, label=LPET.LOCALS.OPTION.DEATHFROMABOVETITLE.label, tooltiptext=LPET.LOCALS.OPTION.DEATHFROMABOVETITLE.tooltiptext}

--Combos
LPET.OPTIONS.Windows.LPETOptionsSquigCombo = {type="combo", career=LPET_CONSTANTS.SQUIG_HERDER, label = L"", tooltiptext = L"Use this to set all modes to same value at once", value = LPET.quickMode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}
LPET.OPTIONS.Windows.LPETOptionsSquigSquealCombo = {type="combo", career=LPET_CONSTANTS.SQUIG_HERDER, label = LPET.LOCALS.OPTION.ABILITYUSE.label, tooltiptext = LPET.LOCALS.OPTION.ABILITYUSE.tooltiptext, value = LPET_CONFIG.SquigSqueal.mode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}
LPET.OPTIONS.Windows.LPETOptionsGoreCombo = {type="combo", career=LPET_CONSTANTS.SQUIG_HERDER, label = LPET.LOCALS.OPTION.ABILITYUSE.label, tooltiptext = LPET.LOCALS.OPTION.ABILITYUSE.tooltiptext, value = LPET_CONFIG.Gore.mode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}
LPET.OPTIONS.Windows.LPETOptionsGoopShootinCombo = {type="combo", career=LPET_CONSTANTS.SQUIG_HERDER, label = LPET.LOCALS.OPTION.ABILITYUSE.label, tooltiptext = LPET.LOCALS.OPTION.ABILITYUSE.tooltiptext, value = LPET_CONFIG.GoopShootin.mode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}
LPET.OPTIONS.Windows.LPETOptionsPoisonedSpineCombo = {type="combo", career=LPET_CONSTANTS.SQUIG_HERDER, label = LPET.LOCALS.OPTION.ABILITYUSE.label, tooltiptext = LPET.LOCALS.OPTION.ABILITYUSE.tooltiptext, value = LPET_CONFIG.PoisonedSpine.mode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}
LPET.OPTIONS.Windows.LPETOptionsSpineFlingCombo = {type="combo", career=LPET_CONSTANTS.SQUIG_HERDER, label = LPET.LOCALS.OPTION.ABILITYUSE.label, tooltiptext = LPET.LOCALS.OPTION.ABILITYUSE.tooltiptext, value = LPET_CONFIG.SpineFling.mode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}
LPET.OPTIONS.Windows.LPETOptionsSporeCloudCombo = {type="combo", career=LPET_CONSTANTS.SQUIG_HERDER, label = LPET.LOCALS.OPTION.ABILITYUSE.label, tooltiptext = LPET.LOCALS.OPTION.ABILITYUSE.tooltiptext, value = LPET_CONFIG.SporeCloud.mode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}
LPET.OPTIONS.Windows.LPETOptionsHeadButtCombo = {type="combo", career=LPET_CONSTANTS.SQUIG_HERDER, label = LPET.LOCALS.OPTION.ABILITYUSE.label, tooltiptext = LPET.LOCALS.OPTION.ABILITYUSE.tooltiptext, value = LPET_CONFIG.HeadButt.mode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}
LPET.OPTIONS.Windows.LPETOptionsDeathFromAboveCombo = {type="combo", career=LPET_CONSTANTS.SQUIG_HERDER, label = LPET.LOCALS.OPTION.ABILITYUSE.label, tooltiptext = LPET.LOCALS.OPTION.ABILITYUSE.tooltiptext, value = LPET_CONFIG.DeathFromAbove.mode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}

LPET.OPTIONS.Windows.LPETOptionsSquigPriorityCombo = {type="combo", career=LPET_CONSTANTS.SQUIG_HERDER, label = L"", tooltiptext = L"Use this to set all priorities to same value at once", value = LPET.quickPriority, data = LPET.LOCALS.OPTION.PRIORITY2.data}
LPET.OPTIONS.Windows.LPETOptionsSquigSquealPriorityCombo = {type="combo", career=LPET_CONSTANTS.SQUIG_HERDER, label = LPET.LOCALS.OPTION.PRIORITY2.label, tooltiptext = LPET.LOCALS.OPTION.PRIORITY2.tooltiptext, value = LPET_CONFIG.SquigSqueal.priority, data = LPET.LOCALS.OPTION.PRIORITY2.data}
LPET.OPTIONS.Windows.LPETOptionsGorePriorityCombo = {type="combo", career=LPET_CONSTANTS.SQUIG_HERDER, label = LPET.LOCALS.OPTION.PRIORITY2.label, tooltiptext = LPET.LOCALS.OPTION.PRIORITY2.tooltiptext, value = LPET_CONFIG.Gore.priority, data = LPET.LOCALS.OPTION.PRIORITY2.data}
LPET.OPTIONS.Windows.LPETOptionsGoopShootinPriorityCombo = {type="combo", career=LPET_CONSTANTS.SQUIG_HERDER, label = LPET.LOCALS.OPTION.PRIORITY2.label, tooltiptext = LPET.LOCALS.OPTION.PRIORITY2.tooltiptext, value = LPET_CONFIG.GoopShootin.priority, data = LPET.LOCALS.OPTION.PRIORITY2.data}
LPET.OPTIONS.Windows.LPETOptionsPoisonedSpinePriorityCombo = {type="combo", career=LPET_CONSTANTS.SQUIG_HERDER, label = LPET.LOCALS.OPTION.PRIORITY2.label, tooltiptext = LPET.LOCALS.OPTION.PRIORITY2.tooltiptext, value = LPET_CONFIG.PoisonedSpine.priority, data = LPET.LOCALS.OPTION.PRIORITY2.data}
LPET.OPTIONS.Windows.LPETOptionsSpineFlingPriorityCombo = {type="combo", career=LPET_CONSTANTS.SQUIG_HERDER, label = LPET.LOCALS.OPTION.PRIORITY2.label, tooltiptext = LPET.LOCALS.OPTION.PRIORITY2.tooltiptext, value = LPET_CONFIG.SpineFling.priority, data = LPET.LOCALS.OPTION.PRIORITY2.data}
LPET.OPTIONS.Windows.LPETOptionsSporeCloudPriorityCombo = {type="combo", career=LPET_CONSTANTS.SQUIG_HERDER, label = LPET.LOCALS.OPTION.PRIORITY2.label, tooltiptext = LPET.LOCALS.OPTION.PRIORITY2.tooltiptext, value = LPET_CONFIG.SporeCloud.priority, data = LPET.LOCALS.OPTION.PRIORITY2.data}
LPET.OPTIONS.Windows.LPETOptionsHeadButtPriorityCombo = {type="combo", career=LPET_CONSTANTS.SQUIG_HERDER, label = LPET.LOCALS.OPTION.PRIORITY2.label, tooltiptext = LPET.LOCALS.OPTION.PRIORITY2.tooltiptext, value = LPET_CONFIG.HeadButt.priority, data = LPET.LOCALS.OPTION.PRIORITY2.data}
LPET.OPTIONS.Windows.LPETOptionsDeathFromAbovePriorityCombo = {type="combo", career=LPET_CONSTANTS.SQUIG_HERDER, label = LPET.LOCALS.OPTION.PRIORITY2.label, tooltiptext = LPET.LOCALS.OPTION.PRIORITY2.tooltiptext, value = LPET_CONFIG.DeathFromAbove.priority, data = LPET.LOCALS.OPTION.PRIORITY2.data}

LPET.OPTIONS.Windows.LPETOptionsSquigHealthCombo = {type="combo", career=LPET_CONSTANTS.SQUIG_HERDER, label = L"", tooltiptext = L"Use this to set all health limits to same value at once", value = (LPET.quickHealthLimit / 5) + 1, data = LPET.LOCALS.OPTION.HEALTH.data}
LPET.OPTIONS.Windows.LPETOptionsSquigSquealHealthCombo = {type="combo", career=LPET_CONSTANTS.SQUIG_HERDER, label = LPET.LOCALS.OPTION.HEALTH.label, tooltiptext = LPET.LOCALS.OPTION.HEALTH.tooltiptext, value = LPET_CONFIG.SquigSqueal.healthLimit, data = LPET.LOCALS.OPTION.HEALTH.data}
LPET.OPTIONS.Windows.LPETOptionsGoreHealthCombo = {type="combo", career=LPET_CONSTANTS.SQUIG_HERDER, label = LPET.LOCALS.OPTION.HEALTH.label, tooltiptext = LPET.LOCALS.OPTION.HEALTH.tooltiptext, value = LPET_CONFIG.Gore.healthLimit, data = LPET.LOCALS.OPTION.HEALTH.data}
LPET.OPTIONS.Windows.LPETOptionsGoopShootinHealthCombo = {type="combo", career=LPET_CONSTANTS.SQUIG_HERDER, label = LPET.LOCALS.OPTION.HEALTH.label, tooltiptext = LPET.LOCALS.OPTION.HEALTH.tooltiptext, value = LPET_CONFIG.GoopShootin.healthLimit, data = LPET.LOCALS.OPTION.HEALTH.data}
LPET.OPTIONS.Windows.LPETOptionsPoisonedSpineHealthCombo = {type="combo", career=LPET_CONSTANTS.SQUIG_HERDER, label = LPET.LOCALS.OPTION.HEALTH.label, tooltiptext = LPET.LOCALS.OPTION.HEALTH.tooltiptext, value = LPET_CONFIG.PoisonedSpine.healthLimit, data = LPET.LOCALS.OPTION.HEALTH.data}
LPET.OPTIONS.Windows.LPETOptionsSpineFlingHealthCombo = {type="combo", career=LPET_CONSTANTS.SQUIG_HERDER, label = LPET.LOCALS.OPTION.HEALTH.label, tooltiptext = LPET.LOCALS.OPTION.HEALTH.tooltiptext, value = LPET_CONFIG.SpineFling.healthLimit, data = LPET.LOCALS.OPTION.HEALTH.data}
LPET.OPTIONS.Windows.LPETOptionsSporeCloudHealthCombo = {type="combo", career=LPET_CONSTANTS.SQUIG_HERDER, label = LPET.LOCALS.OPTION.HEALTH.label, tooltiptext = LPET.LOCALS.OPTION.HEALTH.tooltiptext, value = LPET_CONFIG.SporeCloud.healthLimit, data = LPET.LOCALS.OPTION.HEALTH.data}
LPET.OPTIONS.Windows.LPETOptionsHeadButtHealthCombo = {type="combo", career=LPET_CONSTANTS.SQUIG_HERDER, label = LPET.LOCALS.OPTION.HEALTH.label, tooltiptext = LPET.LOCALS.OPTION.HEALTH.tooltiptext, value = LPET_CONFIG.HeadButt.healthLimit, data = LPET.LOCALS.OPTION.HEALTH.data}
LPET.OPTIONS.Windows.LPETOptionsDeathFromAboveHealthCombo = {type="combo", career=LPET_CONSTANTS.SQUIG_HERDER, label = LPET.LOCALS.OPTION.HEALTH.label, tooltiptext = LPET.LOCALS.OPTION.HEALTH.tooltiptext, value = LPET_CONFIG.DeathFromAbove.healthLimit, data = LPET.LOCALS.OPTION.HEALTH.data}

-- Functions
function LPET.UpdateSquigHerderGUI()
  LPET.OPTIONS.Windows.LPETOptionsSquigCombo.value = LPET.quickMode
  LPET.OPTIONS.Windows.LPETOptionsSquigSquealCombo.value = LPET_CONFIG.SquigSqueal.mode
  LPET.OPTIONS.Windows.LPETOptionsGoreCombo.value = LPET_CONFIG.Gore.mode
  LPET.OPTIONS.Windows.LPETOptionsGoopShootinCombo.value = LPET_CONFIG.GoopShootin.mode
  LPET.OPTIONS.Windows.LPETOptionsPoisonedSpineCombo.value = LPET_CONFIG.PoisonedSpine.mode
  LPET.OPTIONS.Windows.LPETOptionsSpineFlingCombo.value = LPET_CONFIG.SpineFling.mode
  LPET.OPTIONS.Windows.LPETOptionsSporeCloudCombo.value = LPET_CONFIG.SporeCloud.mode
  LPET.OPTIONS.Windows.LPETOptionsHeadButtCombo.value = LPET_CONFIG.HeadButt.mode
  LPET.OPTIONS.Windows.LPETOptionsDeathFromAboveCombo.value = LPET_CONFIG.DeathFromAbove.mode
  
  --Priority
  LPET.OPTIONS.Windows.LPETOptionsSquigPriorityCombo.value = LPET.quickPriority
  LPET.OPTIONS.Windows.LPETOptionsSquigSquealPriorityCombo.value = LPET_CONFIG.SquigSqueal.priority
  LPET.OPTIONS.Windows.LPETOptionsGorePriorityCombo.value = LPET_CONFIG.Gore.priority
  LPET.OPTIONS.Windows.LPETOptionsGoopShootinPriorityCombo.value = LPET_CONFIG.GoopShootin.priority
  LPET.OPTIONS.Windows.LPETOptionsPoisonedSpinePriorityCombo.value = LPET_CONFIG.PoisonedSpine.priority
  LPET.OPTIONS.Windows.LPETOptionsSpineFlingPriorityCombo.value = LPET_CONFIG.SpineFling.priority
  LPET.OPTIONS.Windows.LPETOptionsSporeCloudPriorityCombo.value = LPET_CONFIG.SporeCloud.priority
  LPET.OPTIONS.Windows.LPETOptionsHeadButtPriorityCombo.value = LPET_CONFIG.HeadButt.priority
  LPET.OPTIONS.Windows.LPETOptionsDeathFromAbovePriorityCombo.value = LPET_CONFIG.DeathFromAbove.priority
  
  --Health Limit
  LPET.OPTIONS.Windows.LPETOptionsSquigHealthCombo.value = (LPET.quickHealthLimit / 5) + 1
  LPET.OPTIONS.Windows.LPETOptionsSquigSquealHealthCombo.value = (LPET_CONFIG.SquigSqueal.healthLimit / 5) + 1
  LPET.OPTIONS.Windows.LPETOptionsGoreHealthCombo.value = (LPET_CONFIG.Gore.healthLimit / 5) + 1
  LPET.OPTIONS.Windows.LPETOptionsGoopShootinHealthCombo.value = (LPET_CONFIG.GoopShootin.healthLimit / 5) + 1
  LPET.OPTIONS.Windows.LPETOptionsPoisonedSpineHealthCombo.value = (LPET_CONFIG.PoisonedSpine.healthLimit / 5) + 1
  LPET.OPTIONS.Windows.LPETOptionsSpineFlingHealthCombo.value = (LPET_CONFIG.SpineFling.healthLimit / 5) + 1
  LPET.OPTIONS.Windows.LPETOptionsSporeCloudHealthCombo.value = (LPET_CONFIG.SporeCloud.healthLimit / 5) + 1
  LPET.OPTIONS.Windows.LPETOptionsHeadButtHealthCombo.value = (LPET_CONFIG.HeadButt.healthLimit / 5) + 1
  LPET.OPTIONS.Windows.LPETOptionsDeathFromAboveHealthCombo.value = (LPET_CONFIG.DeathFromAbove.healthLimit / 5) + 1

end

function LPET.SquigComboOnSelChanged(index)
  LPET.quickMode = index
  LPET.OPTIONS.Windows.LPETOptionsSquigCombo.value = index
  LPET.SetAllCastModes(index)
end

function LPET.SquigSquealComboOnSelChanged(index)
  LPET_CONFIG.SquigSqueal.mode = index
  LPET.OPTIONS.Windows.LPETOptionsSquigSquealCombo.value = index
  LPET.UpdatePetAbilitySettings()
end

function LPET.GoreComboOnSelChanged(index)
  LPET_CONFIG.Gore.mode = index
  LPET.OPTIONS.Windows.LPETOptionsGoreCombo.value = index
  LPET.UpdatePetAbilitySettings()
end

function LPET.GoopShootinComboOnSelChanged(index)
  LPET_CONFIG.GoopShootin.mode = index
  LPET.OPTIONS.Windows.LPETOptionsGoopShootinCombo.value = index
  LPET.UpdatePetAbilitySettings()
end

function LPET.PoisonedSpineComboOnSelChanged(index)
  LPET_CONFIG.PoisonedSpine.mode = index
  LPET.OPTIONS.Windows.LPETOptionsPoisonedSpineCombo.value = index
  LPET.UpdatePetAbilitySettings()
end

function LPET.SpineFlingComboOnSelChanged(index)
  LPET_CONFIG.SpineFling.mode = index
  LPET.OPTIONS.Windows.LPETOptionsSpineFlingCombo.value = index
  LPET.UpdatePetAbilitySettings()
end

function LPET.SporeCloudComboOnSelChanged(index)
  LPET_CONFIG.SporeCloud.mode = index
  LPET.OPTIONS.Windows.LPETOptionsSporeCloudCombo.value = index
  LPET.UpdatePetAbilitySettings()
end

function LPET.HeadButtComboOnSelChanged(index)
  LPET_CONFIG.HeadButt.mode = index
  LPET.OPTIONS.Windows.LPETOptionsHeadButtCombo.value = index
  LPET.UpdatePetAbilitySettings()
end

function LPET.DeathFromAboveComboOnSelChanged(index)
  LPET_CONFIG.DeathFromAbove.mode = index
  LPET.OPTIONS.Windows.LPETOptionsDeathFromAboveCombo.value = index
  LPET.UpdatePetAbilitySettings()
end
--[[
  Priority Settings
--]]
function LPET.SquigPriorityComboOnSelChanged(index)
  LPET.quickPriority = index
  LPET.OPTIONS.Windows.LPETOptionsSquigPriorityCombo.value = index
  LPET.SetAllPriorities(index)
end

function LPET.SquigSquealPriorityComboOnSelChanged(index)
  LPET_CONFIG.SquigSqueal.priority = index
  LPET.OPTIONS.Windows.LPETOptionsSquigSquealPriorityCombo.value = index
  LPET.UpdatePetPrioritySettings()
end

function LPET.GorePriorityComboOnSelChanged(index)
  LPET_CONFIG.Gore.priority = index
  LPET.OPTIONS.Windows.LPETOptionsGorePriorityCombo.value = index
  LPET.UpdatePetPrioritySettings()
end

function LPET.GoopShootinPriorityComboOnSelChanged(index)
  LPET_CONFIG.GoopShootin.priority = index
  LPET.OPTIONS.Windows.LPETOptionsGoopShootinPriorityCombo.value = index
  LPET.UpdatePetPrioritySettings()
end

function LPET.PoisonedSpinePriorityComboOnSelChanged(index)
  LPET_CONFIG.PoisonedSpine.priority = index
  LPET.OPTIONS.Windows.LPETOptionsPoisonedSpinePriorityCombo.value = index
  LPET.UpdatePetPrioritySettings()
end

function LPET.SpineFlingPriorityComboOnSelChanged(index)
  LPET_CONFIG.SpineFling.priority = index
  LPET.OPTIONS.Windows.LPETOptionsSpineFlingPriorityCombo.value = index
  LPET.UpdatePetPrioritySettings()
end

function LPET.SporeCloudPriorityComboOnSelChanged(index)
  LPET_CONFIG.SporeCloud.priority = index
  LPET.OPTIONS.Windows.LPETOptionsSporeCloudPriorityCombo.value = index
  LPET.UpdatePetPrioritySettings()
end

function LPET.HeadButtPriorityComboOnSelChanged(index)
  LPET_CONFIG.HeadButt.priority = index
  LPET.OPTIONS.Windows.LPETOptionsHeadButtPriorityCombo.value = index
  LPET.UpdatePetPrioritySettings()
end

function LPET.DeathFromAbovePriorityComboOnSelChanged(index)
  LPET_CONFIG.DeathFromAbove.priority = index
  LPET.OPTIONS.Windows.LPETOptionsDeathFromAbovePriorityCombo.value = index
  LPET.UpdatePetPrioritySettings()
end
--[[
  Health Limit Settings
--]]
function LPET.SquigHealthComboOnSelChanged(index)
  LPET.quickHealthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsSquigHealthCombo.value = index
  LPET.SetAllHealthLimits(LPET.quickHealthLimit)
end

function LPET.SquigSquealHealthComboOnSelChanged(index)
  LPET_CONFIG.SquigSqueal.healthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsSquigSquealHealthCombo.value = index
  LPET.UpdatePetHealthLimitSettings()
end

function LPET.GoreHealthComboOnSelChanged(index)
  LPET_CONFIG.Gore.healthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsGoreHealthCombo.value = index
  LPET.UpdatePetHealthLimitSettings()
end

function LPET.GoopShootinHealthComboOnSelChanged(index)
  LPET_CONFIG.GoopShootin.healthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsGoopShootinHealthCombo.value = index
  LPET.UpdatePetHealthLimitSettings()
end

function LPET.PoisonedSpineHealthComboOnSelChanged(index)
  LPET_CONFIG.PoisonedSpine.healthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsPoisonedSpineHealthCombo.value = index
  LPET.UpdatePetHealthLimitSettings()
end

function LPET.SpineFlingHealthComboOnSelChanged(index)
  LPET_CONFIG.SpineFling.healthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsSpineFlingHealthCombo.value = index
  LPET.UpdatePetHealthLimitSettings()
end

function LPET.SporeCloudHealthComboOnSelChanged(index)
  LPET_CONFIG.SporeCloud.healthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsSporeCloudHealthCombo.value = index
  LPET.UpdatePetHealthLimitSettings()
end

function LPET.HeadButtHealthComboOnSelChanged(index)
  LPET_CONFIG.HeadButt.healthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsHeadButtHealthCombo.value = index
  LPET.UpdatePetHealthLimitSettings()
end

function LPET.DeathFromAboveHealthComboOnSelChanged(index)
  LPET_CONFIG.DeathFromAbove.healthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsDeathFromAboveHealthCombo.value = index
  LPET.UpdatePetHealthLimitSettings()
end