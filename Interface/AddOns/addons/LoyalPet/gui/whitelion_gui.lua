--Labels
LPET.OPTIONS.Windows.LPETOptionsLionsRoarLabel= {type="label", career=LPET_CONSTANTS.WHITE_LION, label=LPET.LOCALS.OPTION.LIONSROARTITLE.label, tooltiptext=LPET.LOCALS.OPTION.LIONSROARTITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsLegTearLabel= {type="label", career=LPET_CONSTANTS.WHITE_LION, label=LPET.LOCALS.OPTION.LEGTEARTITLE.label, tooltiptext=LPET.LOCALS.OPTION.LEGTEARTITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsShredLabel= {type="label", career=LPET_CONSTANTS.WHITE_LION, label=LPET.LOCALS.OPTION.SHREDTITLE.label, tooltiptext=LPET.LOCALS.OPTION.SHREDTITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsGutRipperLabel= {type="label", career=LPET_CONSTANTS.WHITE_LION, label=LPET.LOCALS.OPTION.GUTRIPPERTITLE.label, tooltiptext=LPET.LOCALS.OPTION.GUTRIPPERTITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsMaulLabel= {type="label", career=LPET_CONSTANTS.WHITE_LION, label=LPET.LOCALS.OPTION.MAULTITLE.label, tooltiptext=LPET.LOCALS.OPTION.MAULTITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsFangAndClawLabel= {type="label", career=LPET_CONSTANTS.WHITE_LION, label=LPET.LOCALS.OPTION.FANGANDCLAWTITLE.label, tooltiptext=LPET.LOCALS.OPTION.FANGANDCLAWTITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsTerrifyingRoarLabel= {type="label", career=LPET_CONSTANTS.WHITE_LION, label=LPET.LOCALS.OPTION.TERRIFYINGROARTITLE.label, tooltiptext=LPET.LOCALS.OPTION.TERRIFYINGROARTITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsBiteLabel= {type="label", career=LPET_CONSTANTS.WHITE_LION, label=LPET.LOCALS.OPTION.BITETITLE.label, tooltiptext=LPET.LOCALS.OPTION.BITETITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsClawSweepLabel= {type="label", career=LPET_CONSTANTS.WHITE_LION, label=LPET.LOCALS.OPTION.CLAWSWEEPTITLE.label, tooltiptext=LPET.LOCALS.OPTION.CLAWSWEEPTITLE.tooltiptext}

-- Combos
LPET.OPTIONS.Windows.LPETOptionsLionCombo = {type="combo", career=LPET_CONSTANTS.WHITE_LION, label = L"", tooltiptext = L"Use this to set all modes to same value at once", value = LPET.quickMode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}
LPET.OPTIONS.Windows.LPETOptionsLionsRoarCombo = {type="combo", career=LPET_CONSTANTS.WHITE_LION, label = LPET.LOCALS.OPTION.ABILITYUSE.label, tooltiptext = LPET.LOCALS.OPTION.ABILITYUSE.tooltiptext, value = LPET_CONFIG.LionsRoar.mode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}
LPET.OPTIONS.Windows.LPETOptionsLegTearCombo = {type="combo", career=LPET_CONSTANTS.WHITE_LION, label = LPET.LOCALS.OPTION.ABILITYUSE.label, tooltiptext = LPET.LOCALS.OPTION.ABILITYUSE.tooltiptext, value = LPET_CONFIG.LegTear.mode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}
LPET.OPTIONS.Windows.LPETOptionsShredCombo = {type="combo", career=LPET_CONSTANTS.WHITE_LION, label = LPET.LOCALS.OPTION.ABILITYUSE.label, tooltiptext = LPET.LOCALS.OPTION.ABILITYUSE.tooltiptext, value = LPET_CONFIG.Shred.mode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}
LPET.OPTIONS.Windows.LPETOptionsGutRipperCombo = {type="combo", career=LPET_CONSTANTS.WHITE_LION, label = LPET.LOCALS.OPTION.ABILITYUSE.label, tooltiptext = LPET.LOCALS.OPTION.ABILITYUSE.tooltiptext, value = LPET_CONFIG.GutRipper.mode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}
LPET.OPTIONS.Windows.LPETOptionsMaulCombo = {type="combo", career=LPET_CONSTANTS.WHITE_LION, label = LPET.LOCALS.OPTION.ABILITYUSE.label, tooltiptext = LPET.LOCALS.OPTION.ABILITYUSE.tooltiptext, value = LPET_CONFIG.Maul.mode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}
LPET.OPTIONS.Windows.LPETOptionsFangAndClawCombo = {type="combo", career=LPET_CONSTANTS.WHITE_LION, label = LPET.LOCALS.OPTION.ABILITYUSE.label, tooltiptext = LPET.LOCALS.OPTION.ABILITYUSE.tooltiptext, value = LPET_CONFIG.FangAndClaw.mode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}
LPET.OPTIONS.Windows.LPETOptionsTerrifyingRoarCombo = {type="combo", career=LPET_CONSTANTS.WHITE_LION, label = LPET.LOCALS.OPTION.ABILITYUSE.label, tooltiptext = LPET.LOCALS.OPTION.ABILITYUSE.tooltiptext, value = LPET_CONFIG.TerrifyingRoar.mode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}
LPET.OPTIONS.Windows.LPETOptionsBiteCombo = {type="combo", career=LPET_CONSTANTS.WHITE_LION, label = LPET.LOCALS.OPTION.ABILITYUSE.label, tooltiptext = LPET.LOCALS.OPTION.ABILITYUSE.tooltiptext, value = LPET_CONFIG.Bite.mode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}
LPET.OPTIONS.Windows.LPETOptionsClawSweepCombo = {type="combo", career=LPET_CONSTANTS.WHITE_LION, label = LPET.LOCALS.OPTION.ABILITYUSE.label, tooltiptext = LPET.LOCALS.OPTION.ABILITYUSE.tooltiptext, value = LPET_CONFIG.ClawSweep.mode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}

LPET.OPTIONS.Windows.LPETOptionsLionPriorityCombo = {type="combo", career=LPET_CONSTANTS.WHITE_LION, label = L"", tooltiptext = L"Use this to set all priorities to same value at once", value = LPET.quickPriority, data = LPET.LOCALS.OPTION.PRIORITY3.data}
LPET.OPTIONS.Windows.LPETOptionsLionsRoarPriorityCombo = {type="combo", career=LPET_CONSTANTS.WHITE_LION, label = LPET.LOCALS.OPTION.PRIORITY3.label, tooltiptext = LPET.LOCALS.OPTION.PRIORITY3.tooltiptext, value = LPET_CONFIG.LionsRoar.priority, data = LPET.LOCALS.OPTION.PRIORITY3.data}
LPET.OPTIONS.Windows.LPETOptionsLegTearPriorityCombo = {type="combo", career=LPET_CONSTANTS.WHITE_LION, label = LPET.LOCALS.OPTION.PRIORITY3.label, tooltiptext = LPET.LOCALS.OPTION.PRIORITY3.tooltiptext, value = LPET_CONFIG.LegTear.priority, data = LPET.LOCALS.OPTION.PRIORITY3.data}
LPET.OPTIONS.Windows.LPETOptionsShredPriorityCombo = {type="combo", career=LPET_CONSTANTS.WHITE_LION, label = LPET.LOCALS.OPTION.PRIORITY3.label, tooltiptext = LPET.LOCALS.OPTION.PRIORITY3.tooltiptext, value = LPET_CONFIG.Shred.priority, data = LPET.LOCALS.OPTION.PRIORITY3.data}
LPET.OPTIONS.Windows.LPETOptionsGutRipperPriorityCombo = {type="combo", career=LPET_CONSTANTS.WHITE_LION, label = LPET.LOCALS.OPTION.PRIORITY3.label, tooltiptext = LPET.LOCALS.OPTION.PRIORITY3.tooltiptext, value = LPET_CONFIG.GutRipper.priority, data = LPET.LOCALS.OPTION.PRIORITY3.data}
LPET.OPTIONS.Windows.LPETOptionsMaulPriorityCombo = {type="combo", career=LPET_CONSTANTS.WHITE_LION, label = LPET.LOCALS.OPTION.PRIORITY3.label, tooltiptext = LPET.LOCALS.OPTION.PRIORITY3.tooltiptext, value = LPET_CONFIG.Maul.priority, data = LPET.LOCALS.OPTION.PRIORITY3.data}
LPET.OPTIONS.Windows.LPETOptionsFangAndClawPriorityCombo = {type="combo", career=LPET_CONSTANTS.WHITE_LION, label = LPET.LOCALS.OPTION.PRIORITY3.label, tooltiptext = LPET.LOCALS.OPTION.PRIORITY3.tooltiptext, value = LPET_CONFIG.FangAndClaw.priority, data = LPET.LOCALS.OPTION.PRIORITY3.data}
LPET.OPTIONS.Windows.LPETOptionsTerrifyingRoarPriorityCombo = {type="combo", career=LPET_CONSTANTS.WHITE_LION, label = LPET.LOCALS.OPTION.PRIORITY3.label, tooltiptext = LPET.LOCALS.OPTION.PRIORITY3.tooltiptext, value = LPET_CONFIG.TerrifyingRoar.priority, data = LPET.LOCALS.OPTION.PRIORITY3.data}
LPET.OPTIONS.Windows.LPETOptionsBitePriorityCombo = {type="combo", career=LPET_CONSTANTS.WHITE_LION, label = LPET.LOCALS.OPTION.PRIORITY3.label, tooltiptext = LPET.LOCALS.OPTION.PRIORITY3.tooltiptext, value = LPET_CONFIG.Bite.priority, data = LPET.LOCALS.OPTION.PRIORITY3.data}
LPET.OPTIONS.Windows.LPETOptionsClawSweepPriorityCombo = {type="combo", career=LPET_CONSTANTS.WHITE_LION, label = LPET.LOCALS.OPTION.PRIORITY3.label, tooltiptext = LPET.LOCALS.OPTION.PRIORITY3.tooltiptext, value = LPET_CONFIG.ClawSweep.priority, data = LPET.LOCALS.OPTION.PRIORITY3.data}

LPET.OPTIONS.Windows.LPETOptionsLionHealthCombo = {type="combo", career=LPET_CONSTANTS.WHITE_LION, label = L"", tooltiptext = L"Use this to set all health limits to same value at once", value = (LPET.quickHealthLimit / 5) + 1, data = LPET.LOCALS.OPTION.HEALTH.data}
LPET.OPTIONS.Windows.LPETOptionsLionsRoarHealthCombo = {type="combo", career=LPET_CONSTANTS.WHITE_LION, label = LPET.LOCALS.OPTION.HEALTH.label, tooltiptext = LPET.LOCALS.OPTION.HEALTH.tooltiptext, value = (LPET_CONFIG.LionsRoar.healthLimit / 5) + 1, data = LPET.LOCALS.OPTION.HEALTH.data}
LPET.OPTIONS.Windows.LPETOptionsLegTearHealthCombo = {type="combo", career=LPET_CONSTANTS.WHITE_LION, label = LPET.LOCALS.OPTION.HEALTH.label, tooltiptext = LPET.LOCALS.OPTION.HEALTH.tooltiptext, value = (LPET_CONFIG.LegTear.healthLimit / 5) + 1, data = LPET.LOCALS.OPTION.HEALTH.data}
LPET.OPTIONS.Windows.LPETOptionsShredHealthCombo = {type="combo", career=LPET_CONSTANTS.WHITE_LION, label = LPET.LOCALS.OPTION.HEALTH.label, tooltiptext = LPET.LOCALS.OPTION.HEALTH.tooltiptext, value = (LPET_CONFIG.Shred.healthLimit / 5) + 1, data = LPET.LOCALS.OPTION.HEALTH.data}
LPET.OPTIONS.Windows.LPETOptionsGutRipperHealthCombo = {type="combo", career=LPET_CONSTANTS.WHITE_LION, label = LPET.LOCALS.OPTION.HEALTH.label, tooltiptext = LPET.LOCALS.OPTION.HEALTH.tooltiptext, value = (LPET_CONFIG.GutRipper.healthLimit / 5) + 1, data = LPET.LOCALS.OPTION.HEALTH.data}
LPET.OPTIONS.Windows.LPETOptionsMaulHealthCombo = {type="combo", career=LPET_CONSTANTS.WHITE_LION, label = LPET.LOCALS.OPTION.HEALTH.label, tooltiptext = LPET.LOCALS.OPTION.HEALTH.tooltiptext, value = (LPET_CONFIG.Maul.healthLimit / 5) + 1, data = LPET.LOCALS.OPTION.HEALTH.data}
LPET.OPTIONS.Windows.LPETOptionsFangAndClawHealthCombo = {type="combo", career=LPET_CONSTANTS.WHITE_LION, label = LPET.LOCALS.OPTION.HEALTH.label, tooltiptext = LPET.LOCALS.OPTION.HEALTH.tooltiptext, value = (LPET_CONFIG.FangAndClaw.healthLimit / 5) + 1, data = LPET.LOCALS.OPTION.HEALTH.data}
LPET.OPTIONS.Windows.LPETOptionsTerrifyingRoarHealthCombo = {type="combo", career=LPET_CONSTANTS.WHITE_LION, label = LPET.LOCALS.OPTION.HEALTH.label, tooltiptext = LPET.LOCALS.OPTION.HEALTH.tooltiptext, value = (LPET_CONFIG.TerrifyingRoar.healthLimit / 5) + 1, data = LPET.LOCALS.OPTION.HEALTH.data}
LPET.OPTIONS.Windows.LPETOptionsBiteHealthCombo = {type="combo", career=LPET_CONSTANTS.WHITE_LION, label = LPET.LOCALS.OPTION.HEALTH.label, tooltiptext = LPET.LOCALS.OPTION.HEALTH.tooltiptext, value = (LPET_CONFIG.Bite.healthLimit / 5) + 1, data = LPET.LOCALS.OPTION.HEALTH.data}
LPET.OPTIONS.Windows.LPETOptionsClawSweepHealthCombo = {type="combo", career=LPET_CONSTANTS.WHITE_LION, label = LPET.LOCALS.OPTION.HEALTH.label, tooltiptext = LPET.LOCALS.OPTION.HEALTH.tooltiptext, value = (LPET_CONFIG.ClawSweep.healthLimit / 5) + 1, data = LPET.LOCALS.OPTION.HEALTH.data}

-- Functions
function LPET.UpdateWhiteLionGUI()
  LPET.OPTIONS.Windows.LPETOptionsLionCombo.value = LPET.quickMode
  LPET.OPTIONS.Windows.LPETOptionsLionsRoarCombo.value = LPET_CONFIG.LionsRoar.mode
  LPET.OPTIONS.Windows.LPETOptionsLegTearCombo.value = LPET_CONFIG.LegTear.mode
  LPET.OPTIONS.Windows.LPETOptionsShredCombo.value = LPET_CONFIG.Shred.mode
  LPET.OPTIONS.Windows.LPETOptionsGutRipperCombo.value = LPET_CONFIG.GutRipper.mode
  LPET.OPTIONS.Windows.LPETOptionsMaulCombo.value = LPET_CONFIG.Maul.mode
  LPET.OPTIONS.Windows.LPETOptionsFangAndClawCombo.value = LPET_CONFIG.FangAndClaw.mode
  LPET.OPTIONS.Windows.LPETOptionsTerrifyingRoarCombo.value = LPET_CONFIG.TerrifyingRoar.mode
  LPET.OPTIONS.Windows.LPETOptionsBiteCombo.value = LPET_CONFIG.Bite.mode
  LPET.OPTIONS.Windows.LPETOptionsClawSweepCombo.value = LPET_CONFIG.ClawSweep.mode
  
  --Priority Settings
  LPET.OPTIONS.Windows.LPETOptionsLionPriorityCombo.value = LPET.quickPriority
  LPET.OPTIONS.Windows.LPETOptionsLionsRoarPriorityCombo.value = LPET_CONFIG.LionsRoar.priority
  LPET.OPTIONS.Windows.LPETOptionsLegTearPriorityCombo.value = LPET_CONFIG.LegTear.priority
  LPET.OPTIONS.Windows.LPETOptionsShredPriorityCombo.value = LPET_CONFIG.Shred.priority
  LPET.OPTIONS.Windows.LPETOptionsGutRipperPriorityCombo.value = LPET_CONFIG.GutRipper.priority
  LPET.OPTIONS.Windows.LPETOptionsMaulPriorityCombo.value = LPET_CONFIG.Maul.priority
  LPET.OPTIONS.Windows.LPETOptionsFangAndClawPriorityCombo.value = LPET_CONFIG.FangAndClaw.priority
  LPET.OPTIONS.Windows.LPETOptionsTerrifyingRoarPriorityCombo.value = LPET_CONFIG.TerrifyingRoar.priority
  LPET.OPTIONS.Windows.LPETOptionsBitePriorityCombo.value = LPET_CONFIG.Bite.priority
  LPET.OPTIONS.Windows.LPETOptionsClawSweepPriorityCombo.value = LPET_CONFIG.ClawSweep.priority
  
  --Health Limit Settings
  LPET.OPTIONS.Windows.LPETOptionsLionHealthCombo.value = (LPET.quickHealthLimit / 5) + 1
  LPET.OPTIONS.Windows.LPETOptionsLionsRoarHealthCombo.value = (LPET_CONFIG.LionsRoar.healthLimit / 5) + 1
  LPET.OPTIONS.Windows.LPETOptionsLegTearHealthCombo.value = (LPET_CONFIG.LegTear.healthLimit / 5) + 1
  LPET.OPTIONS.Windows.LPETOptionsShredHealthCombo.value = (LPET_CONFIG.Shred.healthLimit / 5) + 1
  LPET.OPTIONS.Windows.LPETOptionsGutRipperHealthCombo.value = (LPET_CONFIG.GutRipper.healthLimit / 5) + 1
  LPET.OPTIONS.Windows.LPETOptionsMaulHealthCombo.value = (LPET_CONFIG.Maul.healthLimit / 5) + 1
  LPET.OPTIONS.Windows.LPETOptionsFangAndClawHealthCombo.value = (LPET_CONFIG.FangAndClaw.healthLimit / 5) + 1
  LPET.OPTIONS.Windows.LPETOptionsTerrifyingRoarHealthCombo.value = (LPET_CONFIG.TerrifyingRoar.healthLimit / 5) + 1
  LPET.OPTIONS.Windows.LPETOptionsBiteHealthCombo.value = (LPET_CONFIG.Bite.healthLimit / 5) + 1
  LPET.OPTIONS.Windows.LPETOptionsClawSweepHealthCombo.value = (LPET_CONFIG.ClawSweep.healthLimit / 5) + 1
end

function LPET.LionComboOnSelChanged(index)
  LPET.quickMode = index
  LPET.OPTIONS.Windows.LPETOptionsLionCombo.value = index
  LPET.SetAllCastModes(index)
end

function LPET.LionsRoarComboOnSelChanged(index)
  LPET_CONFIG.LionsRoar.mode = index
  LPET.OPTIONS.Windows.LPETOptionsLionsRoarCombo.value = index
  LPET.UpdatePetAbilitySettings()
end

function LPET.LegTearComboOnSelChanged(index)
  LPET_CONFIG.LegTear.mode = index
  LPET.OPTIONS.Windows.LPETOptionsLegTearCombo.value = index
  LPET.UpdatePetAbilitySettings()
end

function LPET.ShredComboOnSelChanged(index)
  LPET_CONFIG.Shred.mode = index
  LPET.OPTIONS.Windows.LPETOptionsShredCombo.value = index
  LPET.UpdatePetAbilitySettings()
end

function LPET.GutRipperComboOnSelChanged(index)
  LPET_CONFIG.GutRipper.mode = index
  LPET.OPTIONS.Windows.LPETOptionsGutRipperCombo.value = index
  LPET.UpdatePetAbilitySettings()
end

function LPET.MaulComboOnSelChanged(index)
  LPET_CONFIG.Maul.mode = index
  LPET.OPTIONS.Windows.LPETOptionsMaulCombo.value = index
  LPET.UpdatePetAbilitySettings()
end

function LPET.FangAndClawComboOnSelChanged(index)
  LPET_CONFIG.FangAndClaw.mode = index
  LPET.OPTIONS.Windows.LPETOptionsFangAndClawCombo.value = index
  LPET.UpdatePetAbilitySettings()
end

function LPET.TerrifyingRoarComboOnSelChanged(index)
  LPET_CONFIG.TerrifyingRoar.mode = index
  LPET.OPTIONS.Windows.LPETOptionsTerrifyingRoarCombo.value = index
  LPET.UpdatePetAbilitySettings()
end

function LPET.BiteComboOnSelChanged(index)
  LPET_CONFIG.Bite.mode = index
  LPET.OPTIONS.Windows.LPETOptionsBiteCombo.value = index
  LPET.UpdatePetAbilitySettings()
end

function LPET.ClawSweepComboOnSelChanged(index)
  LPET_CONFIG.ClawSweep.mode = index
  LPET.OPTIONS.Windows.LPETOptionsClawSweepCombo.value = index
  LPET.UpdatePetAbilitySettings()
end
--[[
  Priority Settings
--]]
function LPET.LionPriorityComboOnSelChanged(index)
  LPET.quickPriority = index
  LPET.OPTIONS.Windows.LPETOptionsLionPriorityCombo.value = index
  LPET.SetAllPriorities(index)
end

function LPET.LionsRoarPriorityComboOnSelChanged(index)
  LPET_CONFIG.LionsRoar.priority = index
  LPET.OPTIONS.Windows.LPETOptionsLionsRoarPriorityCombo.value = index
  LPET.UpdatePetPrioritySettings()
end

function LPET.LegTearPriorityComboOnSelChanged(index)
  LPET_CONFIG.LegTear.priority = index
  LPET.OPTIONS.Windows.LPETOptionsLegTearPriorityCombo.value = index
  LPET.UpdatePetPrioritySettings()
end

function LPET.ShredPriorityComboOnSelChanged(index)
  LPET_CONFIG.Shred.priority = index
  LPET.OPTIONS.Windows.LPETOptionsShredPriorityCombo.value = index
  LPET.UpdatePetPrioritySettings()
end

function LPET.GutRipperPriorityComboOnSelChanged(index)
  LPET_CONFIG.GutRipper.priority = index
  LPET.OPTIONS.Windows.LPETOptionsGutRipperPriorityCombo.value = index
  LPET.UpdatePetPrioritySettings()
end

function LPET.MaulPriorityComboOnSelChanged(index)
  LPET_CONFIG.Maul.priority = index
  LPET.OPTIONS.Windows.LPETOptionsMaulPriorityCombo.value = index
  LPET.UpdatePetPrioritySettings()
end

function LPET.FangAndClawPriorityComboOnSelChanged(index)
  LPET_CONFIG.FangAndClaw.priority = index
  LPET.OPTIONS.Windows.LPETOptionsFangAndClawPriorityCombo.value = index
  LPET.UpdatePetPrioritySettings()
end

function LPET.TerrifyingRoarPriorityComboOnSelChanged(index)
  LPET_CONFIG.TerrifyingRoar.priority = index
  LPET.OPTIONS.Windows.LPETOptionsTerrifyingRoarPriorityCombo.value = index
  LPET.UpdatePetPrioritySettings()
end

function LPET.BitePriorityComboOnSelChanged(index)
  LPET_CONFIG.Bite.priority = index
  LPET.OPTIONS.Windows.LPETOptionsBitePriorityCombo.value = index
  LPET.UpdatePetPrioritySettings()
end

function LPET.ClawSweepPriorityComboOnSelChanged(index)
  LPET_CONFIG.ClawSweep.priority = index
  LPET.OPTIONS.Windows.LPETOptionsClawSweepPriorityCombo.value = index
  LPET.UpdatePetPrioritySettings()
end
--[[
  Health Limit Settings
--]]
function LPET.LionHealthComboOnSelChanged(index)
  LPET.quickHealthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsLionHealthCombo.value = index
  LPET.SetAllHealthLimits(LPET.quickHealthLimit)
end

function LPET.LionsRoarHealthComboOnSelChanged(index)
  LPET_CONFIG.LionsRoar.healthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsLionsRoarHealthCombo.value = index
  LPET.UpdatePetHealthLimitSettings()
end

function LPET.LegTearHealthComboOnSelChanged(index)
  LPET_CONFIG.LegTear.healthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsLegTearHealthCombo.value = index
  LPET.UpdatePetHealthLimitSettings()
end

function LPET.ShredHealthComboOnSelChanged(index)
  LPET_CONFIG.Shred.healthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsShredHealthCombo.value = index
  LPET.UpdatePetHealthLimitSettings()
end

function LPET.GutRipperHealthComboOnSelChanged(index)
  LPET_CONFIG.GutRipper.healthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsGutRipperHealthCombo.value = index
  LPET.UpdatePetHealthLimitSettings()
end

function LPET.MaulHealthComboOnSelChanged(index)
  LPET_CONFIG.Maul.healthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsMaulHealthCombo.value = index
  LPET.UpdatePetHealthLimitSettings()
end

function LPET.FangAndClawHealthComboOnSelChanged(index)
  LPET_CONFIG.FangAndClaw.healthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsFangAndClawHealthCombo.value = index
  LPET.UpdatePetHealthLimitSettings()
end

function LPET.TerrifyingRoarHealthComboOnSelChanged(index)
  LPET_CONFIG.TerrifyingRoar.healthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsTerrifyingRoarHealthCombo.value = index
  LPET.UpdatePetHealthLimitSettings()
end

function LPET.BiteHealthComboOnSelChanged(index)
  LPET_CONFIG.Bite.healthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsBiteHealthCombo.value = index
  LPET.UpdatePetHealthLimitSettings()
end

function LPET.ClawSweepHealthComboOnSelChanged(index)
  LPET_CONFIG.ClawSweep.healthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsClawSweepHealthCombo.value = index
  LPET.UpdatePetHealthLimitSettings()
end