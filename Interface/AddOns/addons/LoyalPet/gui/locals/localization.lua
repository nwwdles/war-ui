--Locals
LPET.LOCALS = {}
LPET.LOCALS.OPTION = {}

--text
LPET.LOCALS.OPTION.PROFILESEDITTITLE = {label=nil, tooltiptext = L"Enter a name for renaming/adding a profile"}

--titles
LPET.LOCALS.OPTION.TITLE = {label=L"LoyalPet Options", tooltiptext = L"Left Click to Drag"}
LPET.LOCALS.OPTION.AUTOSWITCHTITLE = {label=L"Auto-Switch To Player Target", tooltiptext = L"Set on to make pet always attack the same target as the player.If pet is attacking a different target than player when this activates, it will leave that target to attack the same target as the player"}
LPET.LOCALS.OPTION.SWITCHRANGECHECKTITLE = {label=L"Auto-Switch Close Targets Only", tooltiptext = L"Set on to make the auto-switch feature activate only when target is within melee range of player.Not recommended for ranged DPS"}
LPET.LOCALS.OPTION.AUTOATTACKTITLE = {label=L"Auto-Attack", tooltiptext = L"Set on to make pet attack target whenever the player attacks if it has no target.Pet will continue to attack until the target dies, or the player manually forces it to attack a new target(Only works with passive stance)"}
LPET.LOCALS.OPTION.ATTACKRANGECHECKTITLE = {label=L"Auto-Attack Close Targets Only", tooltiptext = L"Set on to make auto-attack feature activate only when target is within melee range of player.Not recommended for ranged DPS"}
LPET.LOCALS.OPTION.AUTODEFENDTITLE = {label=L"Auto-Defend", tooltiptext = L"Set on to make pet attack if the player receives any attack if it has no target.Pet will continue to attack until the target dies, or the player manually forces it to attack a new target(Only works with passive stance)"}
LPET.LOCALS.OPTION.DEFENDRANGECHECKTITLE = {label=L"Auto-Defend Close Targets Only", tooltiptext = L"Set on to make auto-defend feature activate only when target is within melee range of player.Not recommended for ranged DPS"}
LPET.LOCALS.OPTION.AUTOFOLLOWTITLE = {label=L"Auto-Follow", tooltiptext = L"Set on to make pet automatically switch to follow mode if pet's target exceeds range of player's longest attack ability"}
LPET.LOCALS.OPTION.SELFFOLLOWTITLE = {label=L"Self-Targeting Follow", tooltiptext = L"Set on to make pet follow player whenever player targets himself"}
LPET.LOCALS.OPTION.LIONSROARTITLE = {label=L"Lion's Roar", tooltiptext = nil}
LPET.LOCALS.OPTION.LEGTEARTITLE = {label=L"Leg Tear" , tooltiptext = nil}
LPET.LOCALS.OPTION.SHREDTITLE = {label=L"Shred" , tooltiptext = nil}
LPET.LOCALS.OPTION.GUTRIPPERTITLE = {label=L"Gut Ripper" , tooltiptext = nil}
LPET.LOCALS.OPTION.MAULTITLE = {label=L"Maul" , tooltiptext = nil}
LPET.LOCALS.OPTION.FANGANDCLAWTITLE = {label=L"Fang And Claw" , tooltiptext = nil}
LPET.LOCALS.OPTION.TERRIFYINGROARTITLE = {label=L"Terrifying Roar" , tooltiptext = nil}
LPET.LOCALS.OPTION.BITETITLE = {label=L"Bite" , tooltiptext = nil}
LPET.LOCALS.OPTION.CLAWSWEEPTITLE = {label=L"Claw Sweep" , tooltiptext = nil}
LPET.LOCALS.OPTION.PENETRATINGROUNDTITLE = {label=L"Penetrating Round" , tooltiptext = nil}
LPET.LOCALS.OPTION.FLAMETHROWERTITLE = {label=L"Flamethrower" , tooltiptext = nil}
LPET.LOCALS.OPTION.SHOCKGRENADETITLE = {label=L"Shock Grenade" , tooltiptext = nil}
LPET.LOCALS.OPTION.HIGHEXPLOSIVEGRENADETITLE = {label=L"High-Explosive Grenade" , tooltiptext = nil}
LPET.LOCALS.OPTION.MACHINEGUNTITLE = {label=L"Machine Gun" , tooltiptext = nil}
LPET.LOCALS.OPTION.STEAMVENTTITLE = {label=L"Steam Vent" , tooltiptext = nil}
LPET.LOCALS.OPTION.DAEMONICFIRETITLE = {label=L"Daemonic Fire" , tooltiptext = nil}
LPET.LOCALS.OPTION.WARPINGENERGYTITLE = {label=L"Warping Energy" , tooltiptext = nil}
LPET.LOCALS.OPTION.FLAMEOFTZEENTCHTITLE = {label=L"Flame Of Tzeentch" , tooltiptext = nil}
LPET.LOCALS.OPTION.FLAMESOFCHANGETITLE = {label=L"Flames Of Change" , tooltiptext = nil}
LPET.LOCALS.OPTION.CORUSCATINGENERGYTITLE = {label=L"Coruscating Energy" , tooltiptext = nil}
LPET.LOCALS.OPTION.DAEMONICCONSUMPTIONTITLE = {label=L"Daemonic Consumption" , tooltiptext = nil}
LPET.LOCALS.OPTION.SQUIGSQUEALTITLE = {label=L"Squig Squeal" , tooltiptext = nil}
LPET.LOCALS.OPTION.GORETITLE = {label=L"Gore" , tooltiptext = nil}
LPET.LOCALS.OPTION.GOOPSHOOTINTITLE = {label=L"Goop Shootin'" , tooltiptext = nil}
LPET.LOCALS.OPTION.POISONEDSPINETITLE = {label=L"Poisoned Spine" , tooltiptext = nil}
LPET.LOCALS.OPTION.SPINEFLINGTITLE = {label=L"Spine Fling" , tooltiptext = nil}
LPET.LOCALS.OPTION.SPORECLOUDTITLE = {label=L"Spore Cloud" , tooltiptext = nil}
LPET.LOCALS.OPTION.HEADBUTTTITLE = {label=L"Head Butt" , tooltiptext = nil}
LPET.LOCALS.OPTION.DEATHFROMABOVETITLE = {label=L"Death From Above" , tooltiptext = nil}
LPET.LOCALS.OPTION.PETATTACKTITLE = {label=L"Pet Attack Command", tooltiptext=L"Pet will attack selected target when selected combination is pressed"}
LPET.LOCALS.OPTION.PETFOLLOWTITLE = {label=L"Pet Follow Command", tooltiptext=L"Pet will follow player when selected combination is pressed"}
LPET.LOCALS.OPTION.ABILITYNAMETITLE = {label=L"Name", tooltiptext = "Ability Name"}
LPET.LOCALS.OPTION.ABILITYMODETITLE = {label=L"Mode", tooltiptext = L"Off:Ability will be removed from actionbar\nAuto:Addon will control when ability is used\nManual:Player will control when the ability is used\nIgnore:Game will control when ability is used"}
LPET.LOCALS.OPTION.ABILITYPRIORITYTITLE = {label=L"Priority", tooltiptext = L"Set the order of pet abilities to be used in combat"}
LPET.LOCALS.OPTION.ABILITYHEALTHTITLE = {label=L"Health Limit", tooltiptext = L"Set the target health point to stop using ability in combat"}

--Tabs
LPET.LOCALS.OPTION.AUTOTAB = {label=L"General", tooltiptext = L"Configure general settings"}
LPET.LOCALS.OPTION.MANUALTAB = {label=L"KeyBinds", tooltiptext = L"Configure additional mouse and keyboard bindings for pet attack and follow commands"}
LPET.LOCALS.OPTION.ABILITIESTAB = {label=L"Ability Control", tooltiptext = L"Configure control settings for pet abilities"}
LPET.LOCALS.OPTION.PROFILESTAB = {label=L"Profile", tooltiptext = L"Manage configuration profiles"}

--Buttons
LPET.LOCALS.OPTION.SAVEBUTTON = {label=L"Save", tooltiptext = L"Save current settings to selected profile, and renames it if textbox is not empty"}
LPET.LOCALS.OPTION.LOADBUTTON = {label=L"Load", tooltiptext = L"Load selected profile"}
LPET.LOCALS.OPTION.RENAMEBUTTON = {label=L"Rename", tooltiptext = L"Rename selected profile if textbox is not empty"}
LPET.LOCALS.OPTION.ADDBUTTON = {label=L"Add", tooltiptext = L"Add a profile, and rename it if textbox is not empty"}
LPET.LOCALS.OPTION.REMOVEBUTTON = {label=L"Remove", tooltiptext = L"Remove selected profile"}

--Defined ComboBoxes
--Enable
LPET.LOCALS.OPTION.ENABLECOMBO = {
  [1] = L"Off",
  [2] = L"On",
}
--Ability categories
LPET.LOCALS.OPTION.ABILITYCOMBO = {
  [1] = L"Off",
  [2] = L"Auto",
  [3] = L"Manual",
  [4] = L"Ignore",
}
--Mouse Buttons
LPET.LOCALS.OPTION.BUTTONCOMBO = {
  [1]  = L"Off",
  [2]  = L"Left Mouse",
  [3]  = L"Left Mouse + Shift",
  [4]  = L"Left Mouse + Ctrl",
  [5]  = L"Left Mouse + Alt",
  [6]  = L"Middle Mouse",
  [7]  = L"Middle Mouse + Shift",
  [8]  = L"Middle Mouse + Ctrl",
  [9]  = L"Middle Mouse + Alt",
  [10] = L"Right Mouse",
  [11] = L"Right Mouse + Shift",
  [12] = L"Right Mouse + Ctrl",
  [13] = L"Right Mouse + Alt",
}

LPET.LOCALS.OPTION.PRIORITYCOMBO2 = {
  [1] = L"1",
  [2] = L"2",
}

LPET.LOCALS.OPTION.PRIORITYCOMBO3 = {
  [1] = L"1",
  [2] = L"2",
  [3] = L"3",
}

LPET.LOCALS.OPTION.HEALTHCOMBO = {
  [1] = L" 0",
  [2] = L" 5",
  [3] = L"10",
  [4] = L"15",
  [5] = L"20",
  [6] = L"25",
  [7] = L"30",
}

--Default Profiles
LPET.LOCALS.OPTION.PROFILECOMBO = {
  [1] = L"Undefined",
}
--ComboBoxes
LPET.LOCALS.OPTION.ENABLE = { label=L"", tooltiptext = L"Turn Setting On or Off", data = LPET.LOCALS.OPTION.ENABLECOMBO}
LPET.LOCALS.OPTION.ABILITYUSE = { label=L"", tooltiptext = L"Choose how this ability is controlled. If the mode is set to Off, Auto or Manual, then the game's autoUse setting will be automatically disabled.", data = LPET.LOCALS.OPTION.ABILITYCOMBO}
LPET.LOCALS.OPTION.BUTTONS = { label=L"", tooltiptext = L"Choose a button combination.", data = LPET.LOCALS.OPTION.BUTTONCOMBO}
LPET.LOCALS.OPTION.PRIORITY2 = { label=L"", tooltiptext = L"Choose a priority.  2 = Highest Priority", data = LPET.LOCALS.OPTION.PRIORITYCOMBO2}
LPET.LOCALS.OPTION.PRIORITY3 = { label=L"", tooltiptext = L"Choose a priority.  3 = Highest Priority", data = LPET.LOCALS.OPTION.PRIORITYCOMBO3}
LPET.LOCALS.OPTION.HEALTH = { label=L"", tooltiptext = L"Choose when to stop using abilities based on pet target's health. 0 = Ability will be used whenever cooldown is ready", data = LPET.LOCALS.OPTION.HEALTHCOMBO}
LPET.LOCALS.OPTION.PROFILES = { label=L"", tooltiptext = L"Choose the profile you want to load, save, remove, or rename, then click the appropriate button", data = LPET.LOCALS.OPTION.PROFILECOMBO}