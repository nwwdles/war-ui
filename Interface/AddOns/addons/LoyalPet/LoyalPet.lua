--[[
  Hook function variables 
--]]
local alertWindow_SetAlertData
local chatWindow_Print
local chatWindow_OnKeyEnter
local textLogAddEntry
----------------------------------------------------------------
local prevHasPet = nil
local prevPetHasEnemyTarget = nil
local prevCombat = nil
local abilitiesFound = false
----------------------------------------------------------------
if not LPET then LPET = {} end
LPET.PlayerHasEnemyTarget = false
LPET.petTargetUpdated = false
LPET.petTargetProcessed = false
LPET.petMode = LPET_CONSTANTS.PET_NO_COMMAND
LPET.showGUI = false
LPET.abilityTimer = 0.0
LPET.followTimer = 0.0
LPET.stateTimer = 0.0
LPET.attackTimer = 0.0
LPET.mountTimer = 0.0
LPET.abilityCounter = 1
LPET.cHostileTarget = nil
LPET.pHostileTarget = nil
LPET.pPetTargetNum = nil
LPET.cPetTargetNum = nil
LPET.currentProfileSelected = 0
LPET.quickSettings = 0
LPET.quickMode = 0
LPET.quickPriority = 0
LPET.quickHealthLimit = -5
LPET.petCommands = 0
LPET.mountStance = nil
LPET.attackStance = nil
LPET.isMounted = false
LPET.AlertFilterList = {
  "ability not yet ready",
  "invalid target",
  "target is invalid",
  "pet is disabled",
  "not enough action points",
  "ability failed",
}

LPET.ChannelFilterList = {
  SystemData.ChatLogFilters.COMBAT_DEFAULT,
  SystemData.ChatLogFilters.RVR,
  SystemData.ChatLogFilters.SCENARIO,
  SystemData.ChatLogFilters.CHANNEL_6,
  SystemData.ChatLogFilters.CHANNEL_9,
}
----------------------------------------------------------------
if not LPET.actionOrder then LPET.actionOrder = {} end
----------------------------------------------------------------

----------------------------------------------------------------
if not LPET_PROFILES then LPET_PROFILES = {} end
----------------------------------------------------------------
--[[
  General Functions
--]]
----------------------------------------------------------------
-- Determine if target is valid Enemy
function LPET.VerifyEnemyTarget(targetClass, targetType)
  local correctClass = false
  if targetClass == "selfhostiletarget" then
    correctClass = true
  end --targetClass == "selfhostiletarget"

  local correctType = false
  if targetType == SystemData.TargetObjectType.ENEMY_PLAYER or
     targetType == SystemData.TargetObjectType.ENEMY_NON_PLAYER or 
     targetType == SystemData.TargetObjectType.STATIC_ATTACKABLE then
    correctType = true
  end --targetType == SystemData.TargetObjectType.ENEMY_PLAYER
  
  local health = TargetInfo:UnitHealth(targetClass)
  
  if correctClass and correctType and health > 0 then
    LPET.PlayerHasEnemyTarget = true    
  else
    LPET.PlayerHasEnemyTarget = false
  end --correctType and health > 0
end
----------------------------------------------------------------
-- Determine if pet target is valid enemy
function LPET.PetHasEnemyTarget()
  if GameData.Player.Pet.Target.name == L""  or GameData.Player.Pet.Target.name == nil then
    --LPET.PrintDebug("No Pet Target", LPET_CONSTANTS.DEBUG_LEVEL_1)
    return false
  end --GameData.Player.Pet.Target.name == L""
  
  if GameData.Player.Pet.Target.healthPercent <= 0 then
    --LPET.PrintDebug("No Pet Target", LPET_CONSTANTS.DEBUG_LEVEL_1)
    return false
  end --GameData.Player.Pet.Target.healthPercent <= 0
  
  return true
end
----------------------------------------------------------------
-- Determine if Player career can use pets
function LPET.IsPetCareer()
  if GameData.Player.career.id == LPET_CONSTANTS.WHITE_LION then
    return true
  end --GameData.Player.career.id

  if GameData.Player.career.id == LPET_CONSTANTS.SQUIG_HERDER then
    return true
  end --GameData.Player.career.id

  if GameData.Player.career.id == LPET_CONSTANTS.MAGUS then
    return true
  end --GameData.Player.career.id

  if GameData.Player.career.id == LPET_CONSTANTS.ENGINEER then
    return true
  end --GameData.Player.career.id
  
  return false
end
------------------------------------------------------------------
-- Determine if player's pet is available
function LPET.HasPet()
  if GameData.Player.Pet.name == L"" or GameData.Player.Pet.name == nil then
    return false
  end
  return true
end
----------------------------------------------------------------
-- Initialize Addon
function LPET.Initialize()
  if not LPET.IsPetCareer() then
    LPET.PrintDebug("LPET Not Initialized.  Career doesn't use pets", LPET_CONSTANTS.DEBUG_LEVEL_1)
    return
  end --not LPET.IsPetCareer()
  
  TextLogSetEnabled("Combat", true)
  
  LPET.SetAlertFilterHook()
  LPET.SetChatFilterHook()
  LPET.SetTextLogHook()
  LPET.SetSlashHandleHook()  

  RegisterEventHandler("LPET", SystemData.Events.PLAYER_TARGET_UPDATED, "LPET.UpdateTarget")
  RegisterEventHandler("LPET", SystemData.Events.PLAYER_PET_UPDATED, "LPET.UpdatePet")
  RegisterEventHandler("LPET", SystemData.Events.PLAYER_PET_STATE_UPDATED, "LPET.UpdatePet")
  RegisterEventHandler("LPET", SystemData.Events.PLAYER_PET_TARGET_UPDATED, "LPET.CheckPetTargetUpdate")
  RegisterEventHandler("LPET", SystemData.Events.L_BUTTON_DOWN_PROCESSED, "LPET.LButtonUpdate")
  RegisterEventHandler("LPET", SystemData.Events.R_BUTTON_DOWN_PROCESSED, "LPET.RButtonUpdate")
  RegisterEventHandler("LPET", SystemData.Events.M_BUTTON_DOWN_PROCESSED, "LPET.MButtonUpdate")
  RegisterEventHandler("LPET", SystemData.Events.WORLD_OBJ_COMBAT_EVENT, "LPET.UpdateCombatEvent")
  RegisterEventHandler("LPET", SystemData.Events.PLAYER_CAREER_RESOURCE_UPDATED, "LPET.UpdatePet")
  RegisterEventHandler("EA_CareerResourceWindow", SystemData.Events.R_BUTTON_DOWN_PROCESSED, "LPET.PetWindowRButtonUpdate")
  RegisterEventHandler("LPET", SystemData.Events.PLAYER_BEGIN_CAST, "LPET.CastAbilityUpdate")
  


  if not LPET_CONFIG then
    LPET.Print("Config Table Empty.  Resetting Saved Settings To Default Values")
    LPET.ResetSettings()
    LPET.ResetProfiles()
  elseif not string.find(LPET_CONFIG.version, LPET_CONSTANTS.COMPATIBLE_VERSION)  then
    LPET.Print("Version Compatibility Failed.  Resetting Saved Settings To Default Values")
    LPET.ResetSettings()
    LPET.ResetProfiles()
  end
  
  if not LPET_PROFILES then
    LPET.Print("Profiles Table Empty.  Resetting Saved Settings To Default Values")
    LPET.ResetProfiles()
  end
  
  LPET_CONFIG.version = LPET_CONSTANTS.VERSION
  LPET_CONFIG.career = GameData.Player.career.id
  LPET.UpdatePetAbilitySettings()
  LPET.UpdatePetPrioritySettings()
  LPET.UpdatePetHealthLimitSettings()
   
  LPET.PrintDebug("LoyalPet Version " .. LPET_CONFIG.version .. " Initialized Properly.", LPET_CONSTANTS.DEBUG_LEVEL_1)
  LPET.Print("LoyalPet Version " .. LPET_CONFIG.version .. " Initialized Properly.  Type /lpet to see supported command list.")
  
  LPET_CONSTANTS.NEED_INIT_UPDATE = true
end
----------------------------------------------------------------
--Print to screen
function LPET.Print(txt)
  EA_ChatWindow.Print(L"[LPET]:" .. towstring(txt))
end
----------------------------------------------------------------
-- Print debug information to channel 7 and/or channel 8
function LPET.PrintDebug(text, level)
  if level == LPET_CONSTANTS.DEBUG_LEVEL_1 and LPET_CONSTANTS.DEBUG_LEVEL > LPET_CONSTANTS.DEBUG_LEVEL_OFF then
      EA_ChatWindow.Print(L"[LPET]:" .. towstring(text), SystemData.ChatLogFilters.CHANNEL_7)
  elseif level == LPET_CONSTANTS.DEBUG_LEVEL_2 and LPET_CONSTANTS.DEBUG_LEVEL == LPET_CONSTANTS.DEBUG_LEVEL_2 then
      EA_ChatWindow.Print(L"[LPET]:" .. towstring(text), SystemData.ChatLogFilters.CHANNEL_8)
  end
end
----------------------------------------------------------------
-- Shutdown Addon
function LPET.Shutdown()
  UnregisterEventHandler("LPET", SystemData.Events.PLAYER_TARGET_UPDATED, "LPET.UpdateTarget")
  UnregisterEventHandler("LPET", SystemData.Events.PLAYER_PET_UPDATED, "LPET.UpdatePet")
  UnregisterEventHandler("LPET", SystemData.Events.PLAYER_PET_STATE_UPDATED, "LPET.UpdatePet")
  UnregisterEventHandler("LPET", SystemData.Events.PLAYER_PET_TARGET_UPDATED, "LPET.CheckPetTargetUpdate")
  UnregisterEventHandler("LPET", SystemData.Events.L_BUTTON_DOWN_PROCESSED, "LPET.LButtonUpdate")
  UnregisterEventHandler("LPET", SystemData.Events.R_BUTTON_DOWN_PROCESSED, "LPET.RButtonUpdate")
  UnregisterEventHandler("LPET", SystemData.Events.M_BUTTON_DOWN_PROCESSED, "LPET.MButtonUpdate")
  UnregisterEventHandler("LPET", SystemData.Events.WORLD_OBJ_COMBAT_EVENT, "LPET.UpdateCombatEvent")
  UnregisterEventHandler("LPET", SystemData.Events.PLAYER_CAREER_RESOURCE_UPDATED, "LPET.UpdatePet")
  UnregisterEventHandler("EA_CareerResourceWindow", SystemData.Events.R_BUTTON_DOWN_PROCESSED, "LPET.PetWindowRButtonUpdate")
  UnregisterEventHandler("LPET", SystemData.Events.PLAYER_BEGIN_CAST, "LPET.CastAbilityUpdate")  
end
----------------------------------------------------------------
-- Resets certains variables related to pet
function LPET.ResetPetOverrides()
  LPET.PrintDebug("Calling ResetPetOverrides" , LPET_CONSTANTS.DEBUG_LEVEL_1)
  if LPET.petMode == LPET_CONSTANTS.PET_ATTACK and LPET.HasPet() then
      CommandPet(LPET.attackStance)
  end
  LPET.petMode = LPET_CONSTANTS.PET_NO_COMMAND
  LPET_CONSTANTS.ATTACK_OVERRIDE = false
  LPET_CONSTANTS.FOLLOW_OVERRIDE = false
  LPET_CONSTANTS.WAIT_FOR_PET_ATTACKS = true
  LPET.abilityCounter = 1
end
----------------------------------------------------------------
-- Process Target Updates
function LPET.UpdateTarget(targetClass, targetId, targetType)
  -- Limit the amount of target updates processed
  if targetClass ~= "selffriendlytarget" and targetClass ~= "selfhostiletarget" then
    return
  end --targetClass ~= "selffriendlytarget" and targetClass ~= "selfhostiletarget"
  
  if targetId == nil or targetId == 0 then
    return
  end 
  
  if targetType == SystemData.TargetObjectType.SELF and LPET.Enabled(LPET_CONFIG.selftargetfollow) then
    LPET_CONSTANTS.FOLLOW_OVERRIDE = true
    LPET.PrintDebug("Self-Targeting Follow Activated", LPET_CONSTANTS.DEBUG_LEVEL_1)
    LPET.Follow()
  end --if targetType == SystemData.TargetObjectType.SELF and LPET.Enabled(LPET_CONFIG.selftargetfollow) then
  
  LPET.VerifyEnemyTarget(targetClass, targetType)
  
  if LPET.PlayerHasEnemyTarget then
    LPET.pHostileTarget = LPET.cHostileTarget
    LPET.cHostileTarget = TargetInfo:UnitEntityId("selfhostiletarget")
  end
end
----------------------------------------------------------------
function LPET.CheckPetTargetUpdate()
   LPET.petTargetUpdated = LPET.PetHasEnemyTarget()
   if LPET.PetHasEnemyTarget() then
      LPET.petTargetProcessed = false
   end
end
-- Process Pet Target Updates
function LPET.UpdatePetTarget()
  LPET.PrintDebug("LPET.UpdatePetTarget", LPET_CONSTANTS.DEBUG_LEVEL_2)
  if prevPetHasEnemyTarget ~= LPET.PetHasEnemyTarget() and prevPetHasEnemyTarget == LPET.petTargetUpdated then
    LPET.PrintDebug("LPET.UpdatePetTarget: Target Changed", LPET_CONSTANTS.DEBUG_LEVEL_1)
    BroadcastEvent(SystemData.Events.PLAYER_PET_TARGET_UPDATED)
  end --if prevPetHasEnemyTarget ~= LPET.PetHasEnemyTarget() then

  if not LPET.PetHasEnemyTarget() then
    LPET.PrintDebug("LPET.UpdatePetTarget: Pet Has No Target", LPET_CONSTANTS.DEBUG_LEVEL_2)
    for _,action in pairs(LPET_ACTIONS) do
      if action.available then
        if ActionBars:IsActionActive(action.actionId) == true and action.mode ~= LPET_CONSTANTS.IGNORE_CAST and abilitiesFound then
          CommandPetToggleAbility(action.actionId)          
        end
      end --if action.available then
    end --for _,action in pairs(LPET_ACTIONS) do
    
    --Check to see if overrides need to be reset
    if prevPetHasEnemyTarget == true then
      LPET.PrintDebug("LPET.UpdatePetTarget: Resetting Overrides", LPET_CONSTANTS.DEBUG_LEVEL_1)
      LPET.ResetPetOverrides()
      LPET.cPetTargetNum = nil
    end --if prevPetHasEnemyTarget == true or LPET.petMode == LPET_CONSTANTS.PET_ATTACK then
  end --if not LPET.PetHasEnemyTarget() then
  prevPetHasEnemyTarget = LPET.PetHasEnemyTarget()
end
----------------------------------------------------------------
----------------------------------------------------------------
-- Command Pet to perform an action
function LPET.CommandPet(petAction, targetId)
  if petAction == GameData.PetCommand.FOLLOW then
    LPET.petMode = LPET_CONSTANTS.PET_FOLLOW
  elseif petAction == GameData.PetCommand.ATTACK then
    LPET.petMode = LPET_CONSTANTS.PET_ATTACK
    LPET.pPetTargetNum = LPET.cPetTargetNum
    LPET.cPetTargetNum = targetId
    if LPET.pPetTargetNum ~= LPET.cPetTargetNum and LPET.pPetTargetNum ~= nil then 
      CommandPet(GameData.PetCommand.FOLLOW)     
      LPET_CONSTANTS.WAIT_FOR_PET_ATTACKS = true
      LPET.abilityCounter = 1
    end
    LPET.attackStance = GameData.Player.Pet.stance
  elseif petAction == GameData.PetCommand.STAY then
    LPET.petMode = LPET_CONSTANTS.PET_NO_COMMAND    
  end --petAction == GameData.PetCommand.ATTACK
  
  if LPET_CONSTANTS.WAIT_FOR_PET_ATTACKS == false and petAction == GameData.PetCommand.ATTACK then
    CommandPetDoAbility(petAction)
  else
    CommandPet(petAction)
  end
end
----------------------------------------------------------------
-- Periodically check if player's pet is available
function LPET.CheckPetState(elapsed)
  LPET.stateTimer = LPET.stateTimer + elapsed
  if LPET.stateTimer < LPET_CONSTANTS.STATE_TIME_DELAY then
    return
  end
  LPET.stateTimer = 0  
  
  -- If player doesn't have a pet then reset some settings
  if not LPET.HasPet() then
    if prevHasPet == true then
      LPET.PrintDebug("LPET.CheckPetState: Resetting", LPET_CONSTANTS.DEBUG_LEVEL_1)
      LPET.ResetPetOverrides()      
    end
    prevPetHasEnemyTarget = false
  else
    LPET.UpdatePetTarget()
  end
  prevHasPet = LPET.HasPet()
end
----------------------------------------------------------------

function LPET.CheckMountState(elapsed)
  LPET.mountTimer = LPET.mountTimer + elapsed
  if LPET.mountTimer < LPET_CONSTANTS.MOUNT_TIME_DELAY then
    return
  end
  LPET.mountTimer = 0
  
  prevMountState = LPET.isMounted
  --Check for mounted state
  LPET.isMounted = false  
  for k,v in pairs(GetBuffs(GameData.BuffTargetType.SELF)) do
--[[
		if ( LPET_CONSTANTS.MountAbilities[v.abilityId] ) then
			LPET.isMounted = true
			break
		end
--]]
		if ( GetAbilityName(v.abilityId) == L"Summon Mount" ) then
			LPET.isMounted = true
			break
		end
	end
	
	--Check to see if player just dismounted
	if prevMountState == true and LPET.isMounted == false and LPET.HasPet() then
	   CommandPet (LPET.mountStance) 
 	end
end
----------------------------------------------------------------
-- Periodic function
function LPET.OnUpdate(elapsed)
  if LPET_CONSTANTS.NEED_INIT_UPDATE == true then
    LPET_CONSTANTS.NEED_INIT_UPDATE = false
    BroadcastEvent(SystemData.Events.PLAYER_PET_UPDATED)
  end
  
  LPET.CheckPetState(elapsed)
  LPET.PetAbilityThrottle(elapsed)
  LPET.CheckMountState(elapsed)
  
  -- Update Pet Attack Timer
  LPET.attackTimer = LPET.attackTimer + elapsed
end
----------------------------------------------------------------
--- Used to filter certain alert messages
function LPET.SetAlertFilterHook()
  alertWindow_SetAlertData = AlertTextWindow.SetAlertData
  AlertTextWindow.SetAlertData = function(vecType, vecText)
    for k,v in ipairs(vecType) do
      --regex exclusions
      for _,regex in ipairs(LPET.AlertFilterList) do
        if (towstring(vecText[k]:lower())):find(towstring(regex)) then
          vecText[k] = L""
        end
      end --_,regex in ipairs
    end --k,v
    return alertWindow_SetAlertData(vecType, vecText)
  end --AlertTextWindow.SetAlertData = function(vecType, vecText)
end
----------------------------------------------------------------
--- Used to filter certain chat messages
function LPET.SetChatFilterHook()
  chatWindow_Print = EA_ChatWindow.Print
  EA_ChatWindow.Print = function(text, channelId)
    for k,v in ipairs(LPET.ChannelFilterList) do
      if v == channelId then
        for _,regex in ipairs(LPET.AlertFilterList) do
          if (towstring(text:lower())):find(towstring(regex)) then
            text = L""
          end --if (towstring(text:lower())):find(towstring(regex)) then
        end --_,regex in ipairs   
      end --v==channelId
    end --k,v in ipairs
    return chatWindow_Print(text, channelId)
  end --EA_ChatWindow.Print = function(text, channelId)
end

function LPET.SetTextLogHook()
  textLogAddEntry = EA_ChatWindow.OnTextLogUpdated
  EA_ChatWindow.OnTextLogUpdated = function(updateType, filterType)
    if filterType == ChatSettings.Channels[ SystemData.ChatLogFilters.PET_HITS ].id then
      if LPET_CONSTANTS.WAIT_FOR_PET_ATTACKS == true or LPET.petTargetProcessed == false then
        LPET.abilityCounter = 1
        LPET_CONSTANTS.ABILITY_TIME_DELAY = 0.25
      end
      LPET_CONSTANTS.WAIT_FOR_PET_ATTACKS = false
      LPET.petTargetProcessed = true
    end
    return textLogAddEntry(updateType, filterType)
  end
end
----------------------------------------------------------------
--[[
  Configuration and Setting Functions
--]]
----------------------------------------------------------------
-- Change Pet Ability Settings
function LPET.UpdatePetAbilitySettings()
  --White Lion
  if GameData.Player.career.id == LPET_CONSTANTS.WHITE_LION then
    LPET_ACTIONS.lionsRoar.mode = LPET_CONFIG.LionsRoar.mode
    LPET_ACTIONS.legTear.mode = LPET_CONFIG.LegTear.mode
    LPET_ACTIONS.shred.mode = LPET_CONFIG.Shred.mode
    LPET_ACTIONS.gutRipper.mode = LPET_CONFIG.GutRipper.mode
    LPET_ACTIONS.maul.mode = LPET_CONFIG.Maul.mode
    LPET_ACTIONS.fangAndClaw.mode = LPET_CONFIG.FangAndClaw.mode
    LPET_ACTIONS.terrifyingRoar.mode = LPET_CONFIG.TerrifyingRoar.mode
    LPET_ACTIONS.bite.mode = LPET_CONFIG.Bite.mode
    LPET_ACTIONS.clawSweep.mode = LPET_CONFIG.ClawSweep.mode
  end
  --Engineer
  if GameData.Player.career.id == LPET_CONSTANTS.ENGINEER then
    LPET_ACTIONS.penetratingRound.mode = LPET_CONFIG.PenetratingRound.mode
    LPET_ACTIONS.flamethrower.mode = LPET_CONFIG.Flamethrower.mode
    LPET_ACTIONS.shockGrenade.mode = LPET_CONFIG.ShockGrenade.mode
    LPET_ACTIONS.highExplosiveGrenade.mode = LPET_CONFIG.HighExplosiveGrenade.mode
    LPET_ACTIONS.machineGun.mode = LPET_CONFIG.MachineGun.mode
    LPET_ACTIONS.steamVent.mode = LPET_CONFIG.SteamVent.mode
  end
  --Magus
  if GameData.Player.career.id == LPET_CONSTANTS.MAGUS then
    LPET_ACTIONS.daemonicFire.mode = LPET_CONFIG.DaemonicFire.mode
    LPET_ACTIONS.warpingEnergy.mode = LPET_CONFIG.WarpingEnergy.mode
    LPET_ACTIONS.flameOfTzeentch.mode = LPET_CONFIG.FlameOfTzeentch.mode
    LPET_ACTIONS.flamesOfChange.mode = LPET_CONFIG.FlamesOfChange.mode
    LPET_ACTIONS.coruscatingEnergy.mode = LPET_CONFIG.CoruscatingEnergy.mode
    LPET_ACTIONS.daemonicConsumption.mode = LPET_CONFIG.DaemonicConsumption.mode
  end
  --Squig Herder
  if GameData.Player.career.id == LPET_CONSTANTS.MAGUS then
    LPET_ACTIONS.squigSqueal.mode = LPET_CONFIG.SquigSqueal.mode
    LPET_ACTIONS.gore.mode = LPET_CONFIG.Gore.mode
    LPET_ACTIONS.goopShootin.mode = LPET_CONFIG.GoopShootin.mode
    LPET_ACTIONS.poisonedSpine.mode = LPET_CONFIG.PoisonedSpine.mode
    LPET_ACTIONS.spineFling.mode = LPET_CONFIG.SpineFling.mode
    LPET_ACTIONS.sporeCloud.mode = LPET_CONFIG.SporeCloud.mode
    LPET_ACTIONS.headButt.mode = LPET_CONFIG.HeadButt.mode
    LPET_ACTIONS.deathFromAbove.mode = LPET_CONFIG.DeathFromAbove.mode
  end
  LPET.UpdatePet()
end
----------------------------------------------------------------
-- Change Pet Priority Settings
function LPET.UpdatePetPrioritySettings()
  --White Lion
  if GameData.Player.career.id == LPET_CONSTANTS.WHITE_LION then
    LPET_ACTIONS.lionsRoar.priority = LPET_CONFIG.LionsRoar.priority
    LPET_ACTIONS.legTear.priority = LPET_CONFIG.LegTear.priority
    LPET_ACTIONS.shred.priority = LPET_CONFIG.Shred.priority
    LPET_ACTIONS.gutRipper.priority = LPET_CONFIG.GutRipper.priority
    LPET_ACTIONS.maul.priority = LPET_CONFIG.Maul.priority
    LPET_ACTIONS.fangAndClaw.priority = LPET_CONFIG.FangAndClaw.priority
    LPET_ACTIONS.terrifyingRoar.priority = LPET_CONFIG.TerrifyingRoar.priority
    LPET_ACTIONS.bite.priority = LPET_CONFIG.Bite.priority
    LPET_ACTIONS.clawSweep.priority = LPET_CONFIG.ClawSweep.priority
  end
  --Engineer
  if GameData.Player.career.id == LPET_CONSTANTS.ENGINEER then
    LPET_ACTIONS.penetratingRound.priority = LPET_CONFIG.PenetratingRound.priority
    LPET_ACTIONS.flamethrower.priority = LPET_CONFIG.Flamethrower.priority
    LPET_ACTIONS.shockGrenade.priority = LPET_CONFIG.ShockGrenade.priority
    LPET_ACTIONS.highExplosiveGrenade.priority = LPET_CONFIG.HighExplosiveGrenade.priority
    LPET_ACTIONS.machineGun.priority = LPET_CONFIG.MachineGun.priority
    LPET_ACTIONS.steamVent.priority = LPET_CONFIG.SteamVent.priority
  end
  --Magus
  if GameData.Player.career.id == LPET_CONSTANTS.MAGUS then
    LPET_ACTIONS.daemonicFire.priority = LPET_CONFIG.DaemonicFire.priority
    LPET_ACTIONS.warpingEnergy.priority = LPET_CONFIG.WarpingEnergy.priority
    LPET_ACTIONS.flameOfTzeentch.priority = LPET_CONFIG.FlameOfTzeentch.priority
    LPET_ACTIONS.flamesOfChange.priority = LPET_CONFIG.FlamesOfChange.priority
    LPET_ACTIONS.coruscatingEnergy.priority = LPET_CONFIG.CoruscatingEnergy.priority
    LPET_ACTIONS.daemonicConsumption.priority = LPET_CONFIG.DaemonicConsumption.priority
  end
  --Squig Herder
  if GameData.Player.career.id == LPET_CONSTANTS.MAGUS then
    LPET_ACTIONS.squigSqueal.priority = LPET_CONFIG.SquigSqueal.priority
    LPET_ACTIONS.gore.priority = LPET_CONFIG.Gore.priority
    LPET_ACTIONS.goopShootin.priority = LPET_CONFIG.GoopShootin.priority
    LPET_ACTIONS.poisonedSpine.priority = LPET_CONFIG.PoisonedSpine.priority
    LPET_ACTIONS.spineFling.priority = LPET_CONFIG.SpineFling.priority
    LPET_ACTIONS.sporeCloud.priority = LPET_CONFIG.SporeCloud.priority
    LPET_ACTIONS.headButt.priority = LPET_CONFIG.HeadButt.priority
    LPET_ACTIONS.deathFromAbove.priority = LPET_CONFIG.DeathFromAbove.priority
  end
  LPET.UpdatePet()
end
----------------------------------------------------------------
-- Change Pet Target Health Settings
function LPET.UpdatePetHealthLimitSettings()
  --White Lion
  if GameData.Player.career.id == LPET_CONSTANTS.WHITE_LION then
    LPET_ACTIONS.lionsRoar.healthLimit = LPET_CONFIG.LionsRoar.healthLimit
    LPET_ACTIONS.legTear.healthLimit = LPET_CONFIG.LegTear.healthLimit
    LPET_ACTIONS.shred.healthLimit = LPET_CONFIG.Shred.healthLimit
    LPET_ACTIONS.gutRipper.healthLimit = LPET_CONFIG.GutRipper.healthLimit
    LPET_ACTIONS.maul.healthLimit = LPET_CONFIG.Maul.healthLimit
    LPET_ACTIONS.fangAndClaw.healthLimit = LPET_CONFIG.FangAndClaw.healthLimit
    LPET_ACTIONS.terrifyingRoar.healthLimit = LPET_CONFIG.TerrifyingRoar.healthLimit
    LPET_ACTIONS.bite.healthLimit = LPET_CONFIG.Bite.healthLimit
    LPET_ACTIONS.clawSweep.healthLimit = LPET_CONFIG.ClawSweep.healthLimit
  end
  --Engineer
  if GameData.Player.career.id == LPET_CONSTANTS.ENGINEER then
    LPET_ACTIONS.penetratingRound.healthLimit = LPET_CONFIG.PenetratingRound.healthLimit
    LPET_ACTIONS.flamethrower.healthLimit = LPET_CONFIG.Flamethrower.healthLimit
    LPET_ACTIONS.shockGrenade.healthLimit = LPET_CONFIG.ShockGrenade.healthLimit
    LPET_ACTIONS.highExplosiveGrenade.healthLimit = LPET_CONFIG.HighExplosiveGrenade.healthLimit
    LPET_ACTIONS.machineGun.healthLimit = LPET_CONFIG.MachineGun.healthLimit
    LPET_ACTIONS.steamVent.healthLimit = LPET_CONFIG.SteamVent.healthLimit
  end
  --Magus
  if GameData.Player.career.id == LPET_CONSTANTS.MAGUS then
    LPET_ACTIONS.daemonicFire.healthLimit = LPET_CONFIG.DaemonicFire.healthLimit
    LPET_ACTIONS.warpingEnergy.healthLimit = LPET_CONFIG.WarpingEnergy.healthLimit
    LPET_ACTIONS.flameOfTzeentch.healthLimit = LPET_CONFIG.FlameOfTzeentch.healthLimit
    LPET_ACTIONS.flamesOfChange.healthLimit = LPET_CONFIG.FlamesOfChange.healthLimit
    LPET_ACTIONS.coruscatingEnergy.healthLimit = LPET_CONFIG.CoruscatingEnergy.healthLimit
    LPET_ACTIONS.daemonicConsumption.healthLimit = LPET_CONFIG.DaemonicConsumption.healthLimit
  end
  --Squig Herder
  if GameData.Player.career.id == LPET_CONSTANTS.MAGUS then
    LPET_ACTIONS.squigSqueal.healthLimit = LPET_CONFIG.SquigSqueal.healthLimit
    LPET_ACTIONS.gore.healthLimit = LPET_CONFIG.Gore.healthLimit
    LPET_ACTIONS.goopShootin.healthLimit = LPET_CONFIG.GoopShootin.healthLimit
    LPET_ACTIONS.poisonedSpine.healthLimit = LPET_CONFIG.PoisonedSpine.healthLimit
    LPET_ACTIONS.spineFling.healthLimit = LPET_CONFIG.SpineFling.healthLimit
    LPET_ACTIONS.sporeCloud.healthLimit = LPET_CONFIG.SporeCloud.healthLimit
    LPET_ACTIONS.headButt.healthLimit = LPET_CONFIG.HeadButt.healthLimit
    LPET_ACTIONS.deathFromAbove.healthLimit = LPET_CONFIG.DeathFromAbove.healthLimit
  end
end
----------------------------------------------------------------
-- Relates gui value to a true/false setting
function LPET.Enabled(setting)
  if setting == LPET_CONSTANTS.ON then
    return true
  end
  return false
end
----------------------------------------------------------------
-- Set Cast Modes
function LPET.SetAllCastModes(setting)
  if GameData.Player.career.id == LPET_CONSTANTS.WHITE_LION then
    LPET_CONFIG.LionsRoar.mode = setting
    LPET_CONFIG.LegTear.mode = setting
    LPET_CONFIG.Shred.mode = setting
    LPET_CONFIG.GutRipper.mode = setting
    LPET_CONFIG.Maul.mode = setting
    LPET_CONFIG.FangAndClaw.mode = setting
    LPET_CONFIG.TerrifyingRoar.mode = setting
    LPET_CONFIG.Bite.mode = setting
    LPET_CONFIG.ClawSweep.mode = setting
  end

  if GameData.Player.career.id == LPET_CONSTANTS.ENGINEER then
    LPET_CONFIG.PenetratingRound.mode = setting
    LPET_CONFIG.Flamethrower.mode = setting
    LPET_CONFIG.ShockGrenade.mode = setting
    LPET_CONFIG.HighExplosiveGrenade.mode = setting
    LPET_CONFIG.MachineGun.mode = setting
    LPET_CONFIG.SteamVent.mode = setting
  end

  if GameData.Player.career.id == LPET_CONSTANTS.MAGUS then
    LPET_CONFIG.DaemonicFire.mode = setting
    LPET_CONFIG.WarpingEnergy.mode = setting
    LPET_CONFIG.FlameOfTzeentch.mode = setting
    LPET_CONFIG.FlamesOfChange.mode = setting
    LPET_CONFIG.CoruscatingEnergy.mode = setting
    LPET_CONFIG.DaemonicConsumption.mode = setting
  end

  if GameData.Player.career.id == LPET_CONSTANTS.SQUIG_HERDER then
    LPET_CONFIG.SquigSqueal.mode = setting
    LPET_CONFIG.Gore.mode = setting
    LPET_CONFIG.GoopShootin.mode = setting
    LPET_CONFIG.PoisonedSpine.mode = setting
    LPET_CONFIG.SpineFling.mode = setting
    LPET_CONFIG.SporeCloud.mode = setting
    LPET_CONFIG.HeadButt.mode = setting
    LPET_CONFIG.DeathFromAbove.mode = setting
  end
  LPET.UpdatePetAbilitySettings()
end
----------------------------------------------------------------
-- Set Priorities Modes
function LPET.SetAllPriorities(setting)
  if GameData.Player.career.id == LPET_CONSTANTS.WHITE_LION then
    LPET_CONFIG.LionsRoar.priority = setting
    LPET_CONFIG.LegTear.priority = setting
    LPET_CONFIG.Shred.priority = setting
    LPET_CONFIG.GutRipper.priority = setting
    LPET_CONFIG.Maul.priority = setting
    LPET_CONFIG.FangAndClaw.priority = setting
    LPET_CONFIG.TerrifyingRoar.priority = setting
    LPET_CONFIG.Bite.priority = setting
    LPET_CONFIG.ClawSweep.priority = setting
  end

  if GameData.Player.career.id == LPET_CONSTANTS.ENGINEER then
    LPET_CONFIG.PenetratingRound.priority = setting
    LPET_CONFIG.Flamethrower.priority = setting
    LPET_CONFIG.ShockGrenade.priority = setting
    LPET_CONFIG.HighExplosiveGrenade.priority = setting
    LPET_CONFIG.MachineGun.priority = setting
    LPET_CONFIG.SteamVent.priority = setting
  end

  if GameData.Player.career.id == LPET_CONSTANTS.MAGUS then
    LPET_CONFIG.DaemonicFire.priority = setting
    LPET_CONFIG.WarpingEnergy.priority = setting
    LPET_CONFIG.FlameOfTzeentch.priority = setting
    LPET_CONFIG.FlamesOfChange.priority = setting
    LPET_CONFIG.CoruscatingEnergy.priority = setting
    LPET_CONFIG.DaemonicConsumption.priority = setting
  end

  if GameData.Player.career.id == LPET_CONSTANTS.SQUIG_HERDER then
    LPET_CONFIG.SquigSqueal.priority = setting
    LPET_CONFIG.Gore.priority = setting
    LPET_CONFIG.GoopShootin.priority = setting
    LPET_CONFIG.PoisonedSpine.priority = setting
    LPET_CONFIG.SpineFling.priority = setting
    LPET_CONFIG.SporeCloud.priority = setting
    LPET_CONFIG.HeadButt.priority = setting
    LPET_CONFIG.DeathFromAbove.priority = setting
  end
  LPET.UpdatePetPrioritySettings()
end
----------------------------------------------------------------
-- Set Health Modes
function LPET.SetAllHealthLimits(setting)
  if GameData.Player.career.id == LPET_CONSTANTS.WHITE_LION then
    LPET_CONFIG.LionsRoar.healthLimit = setting
    LPET_CONFIG.LegTear.healthLimit = setting
    LPET_CONFIG.Shred.healthLimit = setting
    LPET_CONFIG.GutRipper.healthLimit = setting
    LPET_CONFIG.Maul.healthLimit = setting
    LPET_CONFIG.FangAndClaw.healthLimit = setting
    LPET_CONFIG.TerrifyingRoar.healthLimit = setting
    LPET_CONFIG.Bite.healthLimit = setting
    LPET_CONFIG.ClawSweep.healthLimit = setting
  end

  if GameData.Player.career.id == LPET_CONSTANTS.ENGINEER then
    LPET_CONFIG.PenetratingRound.healthLimit = setting
    LPET_CONFIG.Flamethrower.healthLimit = setting
    LPET_CONFIG.ShockGrenade.healthLimit = setting
    LPET_CONFIG.HighExplosiveGrenade.healthLimit = setting
    LPET_CONFIG.MachineGun.healthLimit = setting
    LPET_CONFIG.SteamVent.healthLimit = setting
  end

  if GameData.Player.career.id == LPET_CONSTANTS.MAGUS then
    LPET_CONFIG.DaemonicFire.healthLimit = setting
    LPET_CONFIG.WarpingEnergy.healthLimit = setting
    LPET_CONFIG.FlameOfTzeentch.healthLimit = setting
    LPET_CONFIG.FlamesOfChange.healthLimit = setting
    LPET_CONFIG.CoruscatingEnergy.healthLimit = setting
    LPET_CONFIG.DaemonicConsumption.healthLimit = setting
  end

  if GameData.Player.career.id == LPET_CONSTANTS.SQUIG_HERDER then
    LPET_CONFIG.SquigSqueal.healthLimit = setting
    LPET_CONFIG.Gore.healthLimit = setting
    LPET_CONFIG.GoopShootin.healthLimit = setting
    LPET_CONFIG.PoisonedSpine.healthLimit = setting
    LPET_CONFIG.SpineFling.healthLimit = setting
    LPET_CONFIG.SporeCloud.healthLimit = setting
    LPET_CONFIG.HeadButt.healthLimit = setting
    LPET_CONFIG.DeathFromAbove.healthLimit = setting
  end
  LPET.UpdatePetHealthLimitSettings()
end

function LPET.SetAllGeneralSettings(setting)
  LPET_CONFIG.selftargetfollow = setting
  LPET_CONFIG.autofollow = setting
  LPET_CONFIG.autoswitch = setting
  LPET_CONFIG.switchrangecheck = setting
  if setting == LPET_CONSTANTS.ON then
    LPET_CONFIG.autoattack = LPET_CONSTANTS.OFF
  else
    LPET_CONFIG.autoattack = setting
  end
  LPET_CONFIG.attackrangecheck = setting
  if setting == LPET_CONSTANTS.ON then
    LPET_CONFIG.autodefend = LPET_CONSTANTS.OFF
  else
    LPET_CONFIG.autodefend = setting
  end
  LPET_CONFIG.defendrangecheck = setting
end
----------------------------------------------------------------
-- Reset
function LPET.ResetSettings()
  LPET_CONFIG = LPET.CopyTable(DEFAULT_CONFIG)
  LPET_CONFIG.name = "Custom"
  LPET_CONFIG.version = LPET_CONSTANTS.VERSION
  LPET_CONFIG.career = GameData.Player.career.id
  LPET.Print("All Settings Reset")
  LPET.UpdatePetAbilitySettings()
  LPET.UpdatePetPrioritySettings()
  LPET.UpdatePetHealthLimitSettings()
end
----------------------------------------------------------------
--
function LPET.ResetProfiles()
  if #LPET_PROFILES <= 1  or not LPET_PROFILES then
    LPET_PROFILES = {}
    LPET_PROFILES[1] = LPET.CopyTable(DEFAULT_CONFIG)
    LPET_PROFILES[1].name = "Default Profile"
    LPET_PROFILES[1].version = LPET_CONSTANTS.VERSION
    LPET_PROFILES[1].career = GameData.Player.career.id
  else
    for i = 1, #LPET_PROFILES do
      LPET_PROFILES[i] = LPET.CopyTable(DEFAULT_CONFIG)
      LPET_PROFILES[i].name = "Default Profile " .. tostring(i)
      LPET_PROFILES[i].version = LPET_CONSTANTS.VERSION
      LPET_PROFILES[i].career = GameData.Player.career.id
    end
  end
end
----------------------------------------------------------------
--hooks chat window to look for lpet commands
function LPET.SetSlashHandleHook()
  chatWindow_OnKeyEnter = EA_ChatWindow.OnKeyEnter
  EA_ChatWindow.OnKeyEnter = function(...)
    local input = WStringToString(EA_TextEntryGroupEntryBoxTextInput.Text)
    local cmd, args = input:match("^/([a-zA-Z]+) ?(.*)")
    if cmd then
      if cmd:lower() == "lpet" then
        LPET.SlashHandler(args)
        EA_TextEntryGroupEntryBoxTextInput.Text = L""
      end --cmd:lower() == "lpet"
    end  --cmd
    return chatWindow_OnKeyEnter(...)
  end
end
----------------------------------------------------------------
function LPET.ToggleGUI()
  if not LPET.showGUI then
    LPET.OpenMenu()
  else
    LPET.HideMenu()
  end
end
----------------------------------------------------------------
-- Parse lpet command
function LPET.SlashHandler(args)
  local opt, val = args:match("([a-z0-9]+)[ ]?(.*)")
     
  if opt then opt = string.lower(opt) else opt = "" end
  if val then val = string.lower(val) else val = "" end
  if opt == "gui" then
    if val == "" or val == nil then
      LPET.ToggleGUI()
    else
      LPET.Print("Command Usage: /lpet gui")
    end
  elseif opt == "reset" then
    if val == "current" or val == nil then
      LPET.ResetSettings()      
    elseif val == "all" then
      LPET.ResetSettings()
      LPET.ResetProfiles()
    else
      LPET.Print("Command Usage: /lpet reset current|all")
    end --val
  elseif opt == "debug" then
    if val == "d1" then
      LPET_CONSTANTS.DEBUG_LEVEL = LPET_CONSTANTS.DEBUG_LEVEL_1
      LPET.Print("LPET Level 1 DEBUG Turned On")
    elseif val == "d2" then
      LPET_CONSTANTS.DEBUG_LEVEL = LPET_CONSTANTS.DEBUG_LEVEL_2
      LPET.Print("LPET Level 2 DEBUG Turned On")
    elseif val == "off" then
      LPET_CONSTANTS.DEBUG_LEVEL = LPET_CONSTANTS.DEBUG_LEVEL_OFF
      LPET.Print("LPET DEBUG Turned Off")      
    else
      LPET.Print("Command Usage: /lpet debug d1|d2|off")
    end --val
  elseif opt == "loadprofile" then
    if tonumber(val) ~= nil then 
      if tonumber(val) >= 1 and tonumber(val) <= #LPET_PROFILES then
        LPET.LoadProfile(tonumber(val))
      else
        LPET.Print("Command Usage: /lpet loadprofile 1-" .. tostring(#LPET_PROFILES) .. " - Load specified profile")
      end
    else
      LPET.Print("Command Usage: /lpet loadprofile 1-" .. tostring(#LPET_PROFILES))
    end --val
  elseif opt == "saveprofile" then
    if tonumber(val) ~= nil then 
      if tonumber(val) >= 1 and tonumber(val) <= #LPET_PROFILES then
        LPET.saveprofile(tonumber(val))
      else
        LPET.Print("Command Usage: /lpet saveprofile 1-" .. tostring(#LPET_PROFILES))
      end
    else
      LPET.Print("Command Usage: /lpet saveprofile 1-" .. tostring(#LPET_PROFILES))
    end --val
  else
    LPET.Print("Loyal Pet Version " .. LPET_CONFIG.version)
    LPET.Print("Command Usage: /lpet - Display this list")
    LPET.Print("Command Usage: /lpet gui - Toggles GUI.  GUI can also be toggled by pressing Shift+Right Mouse on pet career resource icon")
    LPET.Print("Command Usage: /lpet reset current|all - Reset configurations to default values.  Current will reset current configuration values.  All resets everything including profiles")
    LPET.Print("Command Usage: /lpet loadprofile 1-" .. tostring(#LPET_PROFILES) .. " - Loads specified profile into current configuration")
    LPET.Print("Command Usage: /lpet saveprofile 1-" .. tostring(#LPET_PROFILES) .. " - Saves current configuration into specified profile")
  end --opt
end
----------------------------------------------------------------
--[[
  Mouse Functions
--]]
----------------------------------------------------------------
-- Process Left Mouse Click
function LPET.LButtonUpdate(flags, x, y)
  if LPET_CONFIG.attackbuttoncombo == 2 then  
    if LPET.PlayerHasEnemyTarget then
      LPET_CONSTANTS.ATTACK_OVERRIDE = true
      LPET.Attack(LPET.cHostileTarget)
    end --LPET.PlayerHasEnemyTarget
  elseif LPET_CONFIG.attackbuttoncombo == 3 and flags == SystemData.ButtonFlags.SHIFT then
    if LPET.PlayerHasEnemyTarget then
      LPET_CONSTANTS.ATTACK_OVERRIDE = true
      LPET.Attack(LPET.cHostileTarget)
    end --LPET.PlayerHasEnemyTarget
  elseif LPET_CONFIG.attackbuttoncombo == 4 and flags == SystemData.ButtonFlags.CONTROL then
    if LPET.PlayerHasEnemyTarget then
      LPET_CONSTANTS.ATTACK_OVERRIDE = true
      LPET.Attack(LPET.cHostileTarget)
    end --LPET.PlayerHasEnemyTarget
  elseif LPET_CONFIG.attackbuttoncombo == 5 and flags == SystemData.ButtonFlags.ALT then
    if LPET.PlayerHasEnemyTarget then
      LPET_CONSTANTS.ATTACK_OVERRIDE = true
      LPET.Attack(LPET.cHostileTarget)
    end --LPET.PlayerHasEnemyTarget
  end 
  
  if LPET_CONFIG.followbuttoncombo == 2 then  
      LPET_CONSTANTS.FOLLOW_OVERRIDE = true
      LPET.Follow()
  elseif LPET_CONFIG.followbuttoncombo == 3 and flags == SystemData.ButtonFlags.SHIFT then
      LPET_CONSTANTS.FOLLOW_OVERRIDE = true
      LPET.Follow()
  elseif LPET_CONFIG.followbuttoncombo == 4 and flags == SystemData.ButtonFlags.CONTROL then
      LPET_CONSTANTS.FOLLOW_OVERRIDE = true
      LPET.Follow()
  elseif LPET_CONFIG.followbuttoncombo == 5 and flags == SystemData.ButtonFlags.ALT then
      LPET_CONSTANTS.FOLLOW_OVERRIDE = true
      LPET.Follow()
  end 
end
----------------------------------------------------------------
function LPET.MouseOverCareerResourceWindow()
  if string.find(SystemData.MouseOverWindow.name,"EA_CareerResourceWindow") then
    return true
  else    
    return false
  end
end

function LPET.PetWindowRButtonUpdate(flags, x, y)
  if not LPET.MouseOverCareerResourceWindow() then
    return
  end
  
  if flags == SystemData.ButtonFlags.SHIFT then
    LPET.ToggleGUI()
  end
end
----------------------------------------------------------------
-- Process Right Mouse Click
function LPET.RButtonUpdate(flags, x, y)
  if LPET_CONFIG.attackbuttoncombo == 10 then  
    if LPET.PlayerHasEnemyTarget then
      LPET_CONSTANTS.ATTACK_OVERRIDE = true
      LPET.Attack(LPET.cHostileTarget)
    end --LPET.PlayerHasEnemyTarget
  elseif LPET_CONFIG.attackbuttoncombo == 11 and flags == SystemData.ButtonFlags.SHIFT then
    if LPET.MouseOverCareerResourceWindow() then
      return
    end
    
    if LPET.PlayerHasEnemyTarget then
      LPET_CONSTANTS.ATTACK_OVERRIDE = true
      LPET.Attack(LPET.cHostileTarget)
    end --LPET.PlayerHasEnemyTarget
  elseif LPET_CONFIG.attackbuttoncombo == 12 and flags == SystemData.ButtonFlags.CONTROL then
    if LPET.PlayerHasEnemyTarget then
      LPET_CONSTANTS.ATTACK_OVERRIDE = true
      LPET.Attack(LPET.cHostileTarget)
    end --LPET.PlayerHasEnemyTarget
  elseif LPET_CONFIG.attackbuttoncombo == 13 and flags == SystemData.ButtonFlags.ALT then
    if LPET.PlayerHasEnemyTarget then
      LPET_CONSTANTS.ATTACK_OVERRIDE = true
      LPET.Attack(LPET.cHostileTarget)
    end --LPET.PlayerHasEnemyTarget
  end 
  
  if LPET_CONFIG.followbuttoncombo == 10 then  
    LPET_CONSTANTS.FOLLOW_OVERRIDE = true
    LPET.Follow()
  elseif LPET_CONFIG.followbuttoncombo == 11 and flags == SystemData.ButtonFlags.SHIFT then
    if LPET.MouseOverCareerResourceWindow() then
      return
    end
    
    LPET_CONSTANTS.FOLLOW_OVERRIDE = true
    LPET.Follow()
  elseif LPET_CONFIG.followbuttoncombo == 12 and flags == SystemData.ButtonFlags.CONTROL then
    LPET_CONSTANTS.FOLLOW_OVERRIDE = true
    LPET.Follow()
  elseif LPET_CONFIG.followbuttoncombo == 13 and flags == SystemData.ButtonFlags.ALT then
    LPET_CONSTANTS.FOLLOW_OVERRIDE = true
    LPET.Follow()
  end 
end
----------------------------------------------------------------
-- Process Middle Mouse Click
function LPET.MButtonUpdate(flags, x, y)
  if LPET_CONFIG.attackbuttoncombo == 6 then  
    if LPET.PlayerHasEnemyTarget then
      LPET_CONSTANTS.ATTACK_OVERRIDE = true
      LPET.Attack(LPET.cHostileTarget)
    end --LPET.PlayerHasEnemyTarget
  elseif LPET_CONFIG.attackbuttoncombo == 7 and flags == SystemData.ButtonFlags.SHIFT then
    if LPET.PlayerHasEnemyTarget then
      LPET_CONSTANTS.ATTACK_OVERRIDE = true
      LPET.Attack(LPET.cHostileTarget)
    end --LPET.PlayerHasEnemyTarget
  elseif LPET_CONFIG.attackbuttoncombo == 8 and flags == SystemData.ButtonFlags.CONTROL then
    if LPET.PlayerHasEnemyTarget then
      LPET_CONSTANTS.ATTACK_OVERRIDE = true
      LPET.Attack(LPET.cHostileTarget)
    end --LPET.PlayerHasEnemyTarget
  elseif LPET_CONFIG.attackbuttoncombo == 9 and flags == SystemData.ButtonFlags.ALT then
    if LPET.PlayerHasEnemyTarget then
      LPET_CONSTANTS.ATTACK_OVERRIDE = true
      LPET.Attack(LPET.cHostileTarget)
    end --LPET.PlayerHasEnemyTarget
  end 
  
  if LPET_CONFIG.followbuttoncombo == 6 then  
      LPET_CONSTANTS.FOLLOW_OVERRIDE = true
      LPET.Follow()
  elseif LPET_CONFIG.followbuttoncombo == 7 and flags == SystemData.ButtonFlags.SHIFT then
      LPET_CONSTANTS.FOLLOW_OVERRIDE = true
      LPET.Follow()
  elseif LPET_CONFIG.followbuttoncombo == 8 and flags == SystemData.ButtonFlags.CONTROL then
      LPET_CONSTANTS.FOLLOW_OVERRIDE = true
      LPET.Follow()
  elseif LPET_CONFIG.followbuttoncombo == 9 and flags == SystemData.ButtonFlags.ALT then
      LPET_CONSTANTS.FOLLOW_OVERRIDE = true
      LPET.Follow()
  end 
end
----------------------------------------------------------------
--[[
  Attacking Functions
--]]
----------------------------------------------------------------
-- Command Pet To Attack
function LPET.Attack(targetId)
  -- If player doesn't have a pet then nothing needs to be done
  if not LPET.HasPet() then
    LPET.PrintDebug("LPET.Attack", LPET_CONSTANTS.DEBUG_LEVEL_1)
    return
  end

  if LPET_CONSTANTS.ATTACK_OVERRIDE then
    LPET_CONSTANTS.ATTACK_OVERRIDE = false
    LPET.PrintDebug("Manual Attack Activated", LPET_CONSTANTS.DEBUG_LEVEL_1)
    LPET.CommandPet(GameData.PetCommand.ATTACK, targetId)
  else
  -- Throttle Auto Pet Attack Commands
    if LPET.attackTimer < LPET_CONSTANTS.ATTACK_TIME_DELAY then
      return
    end
    LPET.PrintDebug("Auto-Attack Activated", LPET_CONSTANTS.DEBUG_LEVEL_1)
    LPET.CommandPet(GameData.PetCommand.ATTACK, targetId)
    LPET.attackTimer = 0
  end --else LPET_CONSTANTS.ATTACK_OVERRIDE
end
----------------------------------------------------------------
-- Check to see if target is in acceptable range for auto-attack
function LPET.InSwitchRange()
  if not LPET.Enabled(LPET_CONFIG.switchrangecheck) then
    return true
  end
   
  local valid, selected = IsTargetValid(LPET.ShortRangeAbility())
  if (valid) then
    return true
  else
    return false
  end
end
----------------------------------------------------------------
-- Check to see if target is in acceptable range for auto-attack
function LPET.InAttackRange()
  if not LPET.Enabled(LPET_CONFIG.attackrangecheck) then
    return true
  end
   
  local valid, selected = IsTargetValid(LPET.ShortRangeAbility())
  if (valid) then
    return true
  else
    return false
  end
end
----------------------------------------------------------------
-- Check to see if target is in acceptable range for auto-defend
function LPET.InDefendRange()
  if not LPET.Enabled(LPET_CONFIG.defendrangecheck) then
    return true
  end
    
  local valid, selected = IsTargetValid(LPET.ShortRangeAbility())
  if (valid) then
    return true
  else
    return false
  end
end
----------------------------------------------------------------
-- Process Combat Events
function LPET.UpdateCombatEvent(target, amount, combatEvent, actionId)
  -- Not interest in incoming combat events
  if target == 0 or target == GameData.Player.Pet.objNum then
    return
  end
  
  local player = (target == GameData.Player.worldObjNum)
  local pTarget = (target == TargetInfo:UnitEntityId("selfhostiletarget"))
  
  -- Not interested in heal events
  if (amount > 0) and
    ((combatEvent == GameData.CombatEvent.HIT) or 
     (combatEvent == GameData.CombatEvent.ABILITY_HIT) or
     (combatEvent == GameData.CombatEvent.CRITICAL) or
     (combatEvent == GameData.CombatEvent.ABILITY_CRITICAL))
  then
    return
  end  

  local stance = GameData.Player.Pet.stance
  
  if actionId == 0 and pTarget then
    if LPET.Enabled(LPET_CONFIG.autoswitch) then
      LPET.Attack(target)
    elseif LPET.Enabled(LPET_CONFIG.autoattack) and stance == GameData.PetCommand.PASSIVE and not LPET.PetHasEnemyTarget() then 
      LPET.Attack(target)
    end
  end
  
  if player and LPET.Enabled(LPET_CONFIG.autodefend) and stance == GameData.PetCommand.PASSIVE and not LPET.PetHasEnemyTarget() and LPET.InDefendRange() then
    LPET.Attack(LPET.cHostileTarget)
  end
end

function LPET.CastAbilityUpdate(abilityId, isChannel, desiredCastTime, averageLatency)
  local stance = GameData.Player.Pet.stance

--[[  
  if (LPET_CONSTANTS.MountAbilities[abilityId]) and LPET.HasPet() then
	  LPET.mountStance = stance
	end
--]]
	
  if (GetAbilityName(abilityId) == L"Summon Mount") and LPET.HasPet() then
	  LPET.mountStance = stance
	end

  local abilityData = GetAbilityData(abilityId)
  if (abilityData.targetType == GameData.TargetTypes.TARGET_ENEMY) then
    if LPET.Enabled(LPET_CONFIG.autoswitch) and LPET.InSwitchRange() then
        LPET.Attack(LPET.cHostileTarget)
    elseif LPET.Enabled(LPET_CONFIG.autoattack) and stance == GameData.PetCommand.PASSIVE and not LPET.PetHasEnemyTarget() and LPET.InAttackRange() then 
       LPET.Attack(LPET.cHostileTarget)
    end  
  end
end
----------------------------------------------------------------
--[[
  Follow Functions
--]]
----------------------------------------------------------------
-- Command Pet To Follow
function LPET.Follow()
  -- If player doesn't have a pet then nothing needs to be done
  if not LPET.HasPet() then
    LPET.PrintDebug("LPET.Follow", LPET_CONSTANTS.DEBUG_LEVEL_1)
    return
  end
  
  -- Engineer and Magus have stationary pets
  if GameData.Player.career.id == LPET_CONSTANTS.ENGINEER or GameData.Player.career.id == LPET_CONSTANTS.MAGUS then
    LPET.petMode = LPET_CONSTANTS.PET_NO_COMMAND
    LPET_CONSTANTS.FOLLOW_OVERRIDE = false
    return
  end
  
  if LPET_CONSTANTS.FOLLOW_OVERRIDE then
    LPET_CONSTANTS.FOLLOW_OVERRIDE = false
    LPET.PrintDebug("Manual Follow Activated", LPET_CONSTANTS.DEBUG_LEVEL_1)
    LPET.CommandPet(GameData.PetCommand.FOLLOW)
  else
-- Remove the condition of only sending auto-folow once, hopefully this will make the client-server update go better
    LPET.PrintDebug("Auto-Follow Activated", LPET_CONSTANTS.DEBUG_LEVEL_1)
    LPET.CommandPet(GameData.PetCommand.FOLLOW)
  end -- else LPET_CONSTANTS.FOLLOW_OVERRIDE
end
----------------------------------------------------------------
-- Check for auto-follow state
function LPET.UpdateAutoFollow(elapsed)
  LPET.followTimer = LPET.followTimer + elapsed
  if LPET.followTimer < LPET_CONSTANTS.FOLLOW_TIME_DELAY then
    return
  end
  LPET.followTimer = 0
 
  if not LPET.HasPet() then
    return
  end
  
  local valid, selected = IsTargetValid (LPET.LongRangeAbility())
  
  --Only try to Auto-Follow if we just exited combat mode
  if LPET.PetHasEnemyTarget() and not valid and LPET.Enabled(LPET_CONFIG.autofollow) then
    LPET.Follow()
  end --LPET.PlayerHasEnemyTarget
end
----------------------------------------------------------------
--[[
  Ability Functions
--]]
------------------------------------------------------------------
-- Get Cooldown for an action
function LPET.GetPetCooldown(command)
  command.prevCooldown = command.cooldown
  command.cooldown, command.maxcooldown = GetHotbarCooldown( command.slot )
end
------------------------------------------------------------------
-- Move to next command in ability list
function LPET.NextCommand(delay)
  LPET_CONSTANTS.ABILITY_TIME_DELAY = delay
  LPET.abilityCounter = LPET.abilityCounter + 1
  if LPET.abilityCounter > #LPET.actionOrder then
    LPET.abilityCounter = 1
  end
end
----------------------------------------------------------------
function LPET.NeedExtendedTimer(command)
  if command.actionId == 432 then --Fang And Claw
    return true
  end
  
  if command.actionId == 421 then --Machine Gun
    return true
  end
  
  if command.actionId == 425 then --Steam Vent 
    return true
  end
  
  return false
end
----------------------------------------------------------------
function LPET.SameTarget()
  if not LPET.PlayerHasEnemyTarget or not LPET.PetHasEnemyTarget() then
    return false
  end
  
  if not string.find(WStringToString(GameData.Player.Pet.Target.name), WStringToString(TargetInfo:UnitName("selfhostiletarget"))) then
    LPET.PrintDebug("LPET.SameTarget: Different Names", LPET_CONSTANTS.DEBUG_LEVEL_1)
    return false
  end
  
  if GameData.Player.Pet.Target.healthPercent ~= TargetInfo:UnitHealth("selfhostiletarget") then
    LPET.PrintDebug("LPET.SameTarget: Different Health", LPET_CONSTANTS.DEBUG_LEVEL_1)
    return false
  end
  
  if GameData.Player.Pet.Target.level ~= TargetInfo:UnitLevel("selfhostiletarget") then
    LPET.PrintDebug("LPET.SameTarget: Different Level", LPET_CONSTANTS.DEBUG_LEVEL_1)
    return false
  end
  
--[[
-- Keep this code segment in case Mythic re-introduces door targeting bug for pet abilities
  local targetType =  TargetInfo:UnitType("selfhostiletarget")
  if targetType == SystemData.TargetObjectType.STATIC_ATTACKABLE then
    LPET.PrintDebug("LPET.SameTarget: Attacking Door/Gate(Workaround to use abilities)", LPET_CONSTANTS.DEBUG_LEVEL_1)
    return true
  end
--]]
  
  LPET.PrintDebug("LPET.SameTarget: Targets Are Same", LPET_CONSTANTS.DEBUG_LEVEL_1)
  return true
end
----------------------------------------------------------------
-- Try to command pet to use and ability
local prevCooldown = 9999
function LPET.HandleCmd(command)
  --Update ActionBars Cooldown Text
  ActionBars.SetCooldownFlag()
  
  --If mode setting for command is off, return
  if command.mode ~= LPET_CONSTANTS.AUTO_CAST then
    LPET.PrintDebug("LPET.HandleCmd: " .. command.name .. " Not Set to Auto", LPET_CONSTANTS.DEBUG_LEVEL_2)
    LPET.NextCommand(0.33)
    return
  end  
  
  local chargeActive = false
  for k,v in pairs(GetBuffs(GameData.BuffTargetType.SELF)) do
		if (v.abilityId == 9184) then  --Check for Charge Ability
			chargeActive = true
			break
		end
	end

  -- If player is a white lion, and charge! is active, return
  if GameData.Player.career.id == LPET_CONSTANTS.WHITE_LION and chargeActive then
    LPET.PrintDebug("LPET.HandleCmd: Career is White Lion's Charge Is Active", LPET_CONSTANTS.DEBUG_LEVEL_2)
    LPET_CONSTANTS.ABILITY_TIME_DELAY = 0.33
    return
  end    
  
  --Try to Ensure command is in correct toggled state
  LPET.PrintDebug("LPET.HandleCmd: CurrentTarget and PetTarget Equal", LPET_CONSTANTS.DEBUG_LEVEL_2)
  if ActionBars:IsActionActive(command.actionId) == true then
    LPET.PrintDebug("LPET.HandleCmd: Toggling Ability " .. command.name .. " Off", LPET_CONSTANTS.DEBUG_LEVEL_1)
    CommandPetToggleAbility(command.actionId)      
  end
  
  if GameData.Player.Pet.Target.healthPercent <= command.healthLimit then
    LPET.PrintDebug("LPET.HandleCmd: PetTarget Already At Low Health", LPET_CONSTANTS.DEBUG_LEVEL_2)
    LPET.NextCommand(0.33)
    return
  end
  
  --Wait until game uses first ability, then start throttling
  if LPET_CONSTANTS.WAIT_FOR_PET_ATTACKS then
    LPET.GetPetCooldown(command)
    if command.cooldown > 0 then
      LPET.PrintDebug("LPET.HandleCmd: New Target Flag Unset", LPET_CONSTANTS.DEBUG_LEVEL_2)
      LPET_CONSTANTS.ABILITY_TIME_DELAY = 0.33 --Don't need to wait
      LPET_CONSTANTS.WAIT_FOR_PET_ATTACKS = false      
    else
      LPET.PrintDebug("LPET.HandleCmd: New Target Flag Still Set", LPET_CONSTANTS.DEBUG_LEVEL_2)
      -- LPET.NextCommand(0.33)
    end
  else
    LPET.GetPetCooldown(command)
    if command.cooldown == 0 then
      LPET.PrintDebug("LPET.HandleCmd: Pet Is Casting " .. command.name, LPET_CONSTANTS.DEBUG_LEVEL_1)
      --Try to prevent as many multiple calls as possible
      CommandPetDoAbility(command.actionId)
      
      if LPET.NeedExtendedTimer(command) then
        LPET_CONSTANTS.ABILITY_TIME_DELAY = 2.00        
      else
        LPET_CONSTANTS.ABILITY_TIME_DELAY = 0.33
      end
    else --command.cooldown == 0
      LPET.PrintDebug("LPET.HandleCmd: Command Executed. Checking Next Command", LPET_CONSTANTS.DEBUG_LEVEL_2)
      LPET.NextCommand(0.33)
    end
  end --newPetTarget
end
----------------------------------------------------------------
-- Command Pet to Use Abilities
function LPET.PetAbilityThrottle(elapsed)
  LPET.abilityTimer = LPET.abilityTimer + elapsed
  if LPET.abilityTimer < LPET_CONSTANTS.ABILITY_TIME_DELAY then
    return
  end
  LPET.abilityTimer = 0
  
  if not LPET.HasPet() then
    return
  end
  
  if LPET.PetHasEnemyTarget() and #LPET.actionOrder > 0 then
    if not LPET.SameTarget() then
      for i = 1, #LPET.actionOrder do
        if ActionBars:IsActionActive(LPET.actionOrder[i].actionId) == false and LPET.actionOrder[i].mode == LPET_CONSTANTS.AUTO_CAST then
          CommandPetToggleAbility(LPET.actionOrder[i].actionId)          
        end
      end --for _,action in pairs(LPET_ACTIONS) do
      LPET.abilityCounter = 1
      return
    else
      LPET.PrintDebug("LPET.PetAbilityThrottle: Calling HandleCmd", LPET_CONSTANTS.DEBUG_LEVEL_2)
      LPET.HandleCmd(LPET.actionOrder[LPET.abilityCounter])      
    end
  end
end
----------------------------------------------------------------
-- Get Short Range Ability ID
function LPET.ShortRangeAbility()
  if GameData.Player.career.id == LPET_CONSTANTS.WHITE_LION then
    return 9160 --Hack
  end

  if GameData.Player.career.id == LPET_CONSTANTS.ENGINEER then
    return 1509 --Spanner Swipe
  end

  if GameData.Player.career.id == LPET_CONSTANTS.MAGUS then
    return 8484 --Daemonic Maw
  end

  if GameData.Player.career.id == LPET_CONSTANTS.SQUIG_HERDER then
    return 1820 --Stabbity
  end
end
----------------------------------------------------------------
-- Get Long Range Ability ID
function LPET.LongRangeAbility()
  if GameData.Player.career.id == LPET_CONSTANTS.WHITE_LION then
    return 9158 --Axe Toss
  end

  if GameData.Player.career.id == LPET_CONSTANTS.ENGINEER then
    return 1508 --Gun Blast
  end

  if GameData.Player.career.id == LPET_CONSTANTS.MAGUS then
    return 8470 --Flickering Red Fire
  end

  if GameData.Player.career.id == LPET_CONSTANTS.SQUIG_HERDER then
    return 1821 --Plink
  end
end
----------------------------------------------------------------
-- Temporary Function Until Target of Target is available
function LPET.AutoAttackRangeAbility()
  if GameData.Player.career.id == LPET_CONSTANTS.WHITE_LION then
    return 9160
  end

  if GameData.Player.career.id == LPET_CONSTANTS.ENGINEER then
    return 1508
  end

  if GameData.Player.career.id == LPET_CONSTANTS.MAGUS then
    return 8470
  end

  if GameData.Player.career.id == LPET_CONSTANTS.SQUIG_HERDER then
    return 1821
  end
end
----------------------------------------------------------------
-- Determine if pet needs its pet hotbar updated
function LPET.UpdatePet()
  if LPET.HasPet() then
    LPET.actionOrder = {}
    
    LPET.GetPetAbilitiesByName()
    
    if abilitiesFound == false then
      LPET.GetPetAbilitiesById()
    end
    
    if abilitiesFound == true then
      LPET.UpdatePetAbilityBar()
    end
  end
end
----------------------------------------------------------------
-- Determine which pet abilities are currently available
function LPET.GetPetAbilitiesByName()
  local abilityTable = GetAbilityTable(GameData.AbilityType.PET)
  local abilityCount = 0
  abilitiesFound = false
  if abilityTable ~= nil then
    for i, j in pairs (LPET_ACTIONS) do
      j.available = false
      for k, v in pairs (abilityTable) do
        local actionName = WStringToString(v.name)
        local start, finish = string.find(actionName, j.name)
        if start ~= nil then
          LPET.PrintDebug("Command Match Found", LPET_CONSTANTS.DEBUG_LEVEL_2)
          j.actionId = v.id
          --If pet has a target that's near death, then make ability unavailable
          LPET.PrintDebug(j.name .. " Available", LPET_CONSTANTS.DEBUG_LEVEL_2)
          j.available = true
          abilityCount = abilityCount + 1
          break
        end --j.actionId == v.id 
      end --for k, v in pairs 
    end --for i, j in pairs
    if abilityCount < #abilityTable then
      LPET.PrintDebug("GetPetAbilitiesByName: Abilities Missing", LPET_CONSTANTS.DEBUG_LEVEL_1)
      LPET.PrintDebug("AbilityCount = " .. tostring(abilityCount), LPET_CONSTANTS.DEBUG_LEVEL_1)
      LPET.PrintDebug("Table Size = " .. tostring(#abilityTable), LPET_CONSTANTS.DEBUG_LEVEL_1)
      abilitiesFound = false
    else
      LPET.PrintDebug("GetPetAbilitiesByName: Abilities Found", LPET_CONSTANTS.DEBUG_LEVEL_1)
      abilitiesFound = true
    end
  end --if abilityTable ~= nil then
end
----------------------------------------------------------------
function LPET.GetPetAbilitiesById()
  local abilityTable = GetAbilityTable(GameData.AbilityType.PET)
  local abilityCount = 0
  abilitiesFound = false
  if abilityTable ~= nil then
    for i, j in pairs (LPET_ACTIONS) do
      j.available = false
      tableCount = 0
      for k, v in pairs (abilityTable) do
        if j.actionId == v.id then
          LPET.PrintDebug("Command Match Found", LPET_CONSTANTS.DEBUG_LEVEL_2)
          j.name = WStringToString(v.name)
          --If pet has a target that's near death, then make ability unavailable
          LPET.PrintDebug(j.name .. " Available", LPET_CONSTANTS.DEBUG_LEVEL_2)
          j.available = true
          abilityCount = abilityCount + 1
          break
        end --j.actionId == v.id 
      end --for k, v in pairs 
    end --for i, j in pairs
    if abilityCount < #abilityTable then
      LPET.PrintDebug("GetPetAbilitiesById: Abilities Missing", LPET_CONSTANTS.DEBUG_LEVEL_1)
      LPET.PrintDebug("AbilityCount = " .. tostring(abilityCount), LPET_CONSTANTS.DEBUG_LEVEL_1)
      LPET.PrintDebug("Table Size = " .. tostring(#abilityTable), LPET_CONSTANTS.DEBUG_LEVEL_1)
      abilitiesFound = false
    else
      LPET.PrintDebug("GetPetAbilitiesById: Abilities Found", LPET_CONSTANTS.DEBUG_LEVEL_1)
      abilitiesFound = true
    end
  end --if abilityTable ~= nil then
end
----------------------------------------------------------------
-- Update the pet hotbar based on available abilities and ability settings of the addon
function LPET.UpdatePetAbilityBar()
  if not ActionBars.m_Bars.EA_CareerResourceWindowActionBar then
    LPET.PrintDebug("Critical Error: Pet Bar Not Found!!", LPET_CONSTANTS.DEBUG_LEVEL_1)
    return
  end --if not ActionBars.m_Bars.EA_CareerResourceWindowActionBar then
    
  --Step 1 - Fill the list
  local idx = 1
  for _,lpetAction in pairs(LPET_ACTIONS) do
    if lpetAction.available then
      if ActionBars:IsActionActive(lpetAction.actionId) == true and lpetAction.mode ~= LPET_CONSTANTS.IGNORE_CAST then
        CommandPetToggleAbility(lpetAction.actionId)
      end --if ActionBars:IsActionActive(lpetAction.actionId) == true then
      
      if lpetAction.mode ~= LPET_CONSTANTS.NO_CAST then
        LPET.actionOrder[idx] = LPET.CopyTable(lpetAction)
        idx = idx + 1 
      end --if lpetAction.mode ~= LPET_CONSTANTS.NO_CAST then
    end --lpetAction.available and lpetAction.mode ~= LPET_CONSTANTS.NO_CAST then
  end --for _,lpetAction in pairs(LPET_ACTIONS) do
  
  if #LPET.actionOrder == 0 then
    LPET.PrintDebug("LPET.UpdatePetAbilityBar: Command List Empty.  Exiting Function", LPET_CONSTANTS.DEBUG_LEVEL_1)
    return
  end
  
  --Step 2 - Sort the list
  local swp
  for i = 1, #LPET.actionOrder - 1 do
    for j = i + 1, #LPET.actionOrder do
      if LPET.actionOrder[i].priority < LPET.actionOrder[j].priority then
        swp = LPET.CopyTable(LPET.actionOrder[i])
        LPET.actionOrder[i] = LPET.CopyTable(LPET.actionOrder[j])
        LPET.actionOrder[j] = LPET.CopyTable(swp)
      elseif LPET.actionOrder[i].priority == LPET.actionOrder[j].priority then
        if LPET.actionOrder[i].actionId < LPET.actionOrder[j].actionId then
          swp = LPET.CopyTable(LPET.actionOrder[i])
          LPET.actionOrder[i] = LPET.CopyTable(LPET.actionOrder[j])
          LPET.actionOrder[j] = LPET.CopyTable(swp)
        end --if LPET.actionOrder[i].actionId > LPET.actionOrder[j].actionId then
      end --if LPET.actionOrder[i].priority < LPET.actionOrder[j].priority then
    end --for j = i + 1, #LPET.actionOrder do
  end --for i = 1, #LPET.actionOrder - 1 do
  
  --Step 3 - Count Number of Pet Commands
  LPET.petCommands = 0
  local bar = ActionBars.m_Bars.EA_CareerResourceWindowActionBar.m_Buttons
  for idx = 1, #bar do
    if bar[idx].m_ActionType == GameData.PlayerActions.COMMAND_PET then
      LPET.petCommands = LPET.petCommands + 1
    end
  end --idx = 1, #bar
 
  --Step 4 - Clear Current Abilities
  local currentSlot = (GameDefs.LAST_PET_ABILITY_SLOT - LPET.petCommands) - 1
  while currentSlot >= GameDefs.FIRST_PET_ABILITY_SLOT
  do
    SetHotbarData (currentSlot, GameData.PlayerActions.NONE, 0)
    currentSlot = currentSlot - 1
  end
  
  --Step 5 - Recreate Bar with Sorted Abilities
  for actionIdx = 1, #LPET.actionOrder do
    currentSlot = (GameDefs.LAST_PET_ABILITY_SLOT - LPET.petCommands) - 1
    while currentSlot >= GameDefs.FIRST_PET_ABILITY_SLOT
    do
      local actionType, actionId = GetHotbarData (currentSlot)
    
      if ((actionType == GameData.PlayerActions.NONE) and (actionId == 0))
      then
        SetHotbarData (currentSlot, GameData.PlayerActions.COMMAND_PET_DO_ABILITY, LPET.actionOrder[actionIdx].actionId)
        LPET.actionOrder[actionIdx].slot = currentSlot
        break
      end
      currentSlot = currentSlot - 1
    end
  end
  ActionBars.SetCooldownFlag()
end

----------------------------------------------------------------
--Copy Function from  EASystemUtils/DataUtils files
function LPET.CopyTable (source)
	if (source == nil) 
    then
        LPET.PrintDebug("LPET.CopyTable (source): Source table was nil.", DEBUG_LEVEL_1)
        return nil
    end
    
    if (type (source) ~= "table") 
    then        
        LPET.PrintDebug("LPET.CopyTable (source): Source is not a table, it was a ="..tostring(type(source)), DEBUG_LEVEL_1)
        return nil
    end

    local newTable = {}

    for k, v in pairs (source) 
    do
        if (type (v) == "table")
        then
            newTable[k] = LPET.CopyTable (v)
        else
            newTable[k] = v
        end
    end
    
    return newTable
end

function LPET.LoadProfile(index)
  if not LPET_PROFILES[index] then
    LPET.Print("LPET.LoadProfile: Selected Profile Does Not Exist! Please save a configuration there first.")
    return
  end
  
  if index > #LPET_PROFILES or index < 1 then
    LPET.Print("LPET.LoadProfile: Selected Index Does Not Exist!.")
    return
  end
  
  LPET.currentProfileSelected = index
  LPET_CONFIG = LPET.CopyTable(LPET_PROFILES[index])
  LPET.Print("LPET.LoadProfile: Profile #" .. tostring(index) .. " Successfully Loaded")
  LPET.UpdatePetAbilitySettings()
  LPET.UpdatePetPrioritySettings()
  LPET.UpdatePetHealthLimitSettings()
end

function LPET.SaveProfile(index)
  if not LPET_PROFILES[index] then
    LPET.Print("LPET.LoadProfile: Selected Profile Does Not Exist! Please save a configuration there first.")
    return
  end
  
  if index > #LPET_PROFILES or index < 1 then
    LPET.Print("LPET.SaveProfile: Selected Index Does Not Exist!")
    return
  end
  
  LPET.currentProfileSelected = index
  LPET_PROFILES[index] = LPET.CopyTable(LPET_CONFIG)
  LPET.Print("LPET.SaveProfile: Profile #" .. tostring(index) .. " Successfully Saved")
end

local function trim (s)
	return (string.gsub(s, "^%s*(.-)%s*$", "%1"))
end

local function isAlphaNumeric(s)
   return (#string.match(s, "%w+") == #s)
end

function LPET.RenameProfile(index, newName)
  if not LPET_PROFILES[index] then
    LPET.Print("LPET.RenameProfile: Selected Profile Does Not Exist! Please save a configuration there first.")
    return
  end
  
  if index > #LPET_PROFILES or index < 1 then
    LPET.Print("LPET.RenameProfile: Selected Index Does Not Exist!")
    return
  end
  
  trimmed = trim(newName)
    
  if trimmed == "" or trimmed == nil then
    LPET.Print("LPET.RenameProfile: Invalid Name Provided! Please enter a valid name")
    return
  end  
  
  LPET.currentProfileSelected = index
  LPET_PROFILES[index].name = newName
  LPET.Print("LPET.RenameProfile: Profile " .. tostring(index) .. " Successfully Renamed To " .. newName)
end

function LPET.AddProfile()
  local newProfile = LPET.CopyTable(LPET_CONFIG)
  newProfile.name = "New Profile"
  local added = table.insert(LPET_PROFILES, newProfile)
  LPET.currentProfileSelected = #LPET_PROFILES
  LPET.Print("LPET.AddProfile: Profile #" .. tostring(#LPET_PROFILES) .. " Sucessfully Added")
end

function LPET.RemoveProfile(index)
  if not LPET_PROFILES[index] then
    LPET.Print("LPET.RemoveProfile: Selected Profile Does Not Exist!")
    return
  end
  
  if index > #LPET_PROFILES or index < 1 then
    LPET.Print("LPET.RemoveProfile: Selected Index Does Not Exist!.")
    return
  end
  
  local removed = table.remove(LPET_PROFILES, index)
  LPET.currentProfileSelected = 0
  LPET.Print("LPET.RemoveProfile: Profile #" .. tostring(index) .. " Successfully Removed")
end

