--Locals
LPET.LOCALS = {}

--Everything From here on would need to be translated and put
--into if statements for each specific language.

--***********
--ENGLISH
--***********

LPET.LOCALS.NAME = L"LoyalPet"

--Option messages
LPET.LOCALS.STARTUP = LPET.LOCALS.NAME..L" loaded. Type /lpet for options.";

--Warnings