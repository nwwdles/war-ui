--------------------------------------------------------------------------------
-- Copyright (c) 2008 Flimgoblin <fingoniel@roguishness.com>
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
--
--------------------------------------------------------------------------------
-- File:      PointsOfInterest.lua
-- Date:      2009-03-29T23:35:00Z
-- Author:    Flimgoblin <fingoniel@roguishness.com>
-- Version:   Beta v0.3
-- Copyright: 2009
--------------------------------------------------------------------------------
-- Zone point of interest data for BOs, Keeps and warcamps
-- Thanks to Groham and FxG (Handojin) for helping me collect 
-- all the data and Jocelynn for helping test it's all accurate
--------------------------------------------------------------------------------

PointsOfInterest={
	DISTANCE_AT=1500,
	DISTANCE_NEAR=100000,
	points={},
	spelling={}
}
local round=function(num,idp) return tonumber(string.format("%." .. (idp or 0) .. "f", num)) end

PointsOfInterest.spelling[L"Garrisomn of Skulls"]=L"Garrison of Skulls"


function PointsOfInterest.FindNearbyPoint(pos,longname,brief)
	-- TODO: Deal with close things in nearby zones, probably want to use mapmonsterapi and world coords
	-- for now just a simple bit of trig
	
	if(PointsOfInterest.points[pos.zoneId]) then
		local deltas={}			
		local normal_points=PointsOfInterest.points[pos.zoneId]
		local points={}	
		if(PointsOfInterest_custompoints[pos.zoneId]) then
			local custom_points=PointsOfInterest_custompoints[pos.zoneId]

			for k,poi in normal_points do
				points[k]=poi
			end
			for k,poi in custom_points do
				points[k]=poi
			end
		else
			points=normal_points
		end
				
		local closest=-1
		for k,poi in pairs(points) do
			local deltaX = pos.zoneX - poi.zoneX
			local deltaY = pos.zoneY - poi.zoneY
			local distance = math.sqrt( (deltaX * deltaX) + (deltaY * deltaY) )
			if(distance<PointsOfInterest.DISTANCE_NEAR) then			
				deltas[distance]={name=poi.name,
									dx=deltaX,
									dy=deltaY}
				if(closest<0 or closest>distance) then
					closest=distance
				end
			end
		end		
		
		if(closest~=-1) then
			local poi={}
			local delta=deltas[closest]
			poi.distance=closest	
			
			local poiname=""
			
			if(longname and brief) then
				poiname=poiname..tostring(delta.name).." ["..tostring(delta.brief).."]";
			elseif(brief) then
				if(delta.brief) then			
					poiname=tostring(delta.brief)
				else
					poiname=tostring(delta.name)
				end
			else	
				poiname=tostring(delta.name)
			end
			
			
			if(closest<PointsOfInterest.DISTANCE_AT) then			
				poi.description=poiname
				poi.at=true
			else							
				poi.description=round(poi.distance/1000).."k "..PointsOfInterest.GetDirection(delta.dx,delta.dy).." of "..poiname;
				poi.at=false
			end
			return poi
		end
	end
	
	return nil
end

function PointsOfInterest.GetDirection(dx,dy)
	local r=math.sqrt(dx*dx+dy*dy)
	local theta=math.atan2(dy/r,dx/r);	
	theta=math.deg(theta)
	if(theta>=-202.5 and theta<-157.5) then
		return "west"
	elseif(theta>=-157.5 and theta<-112.5) then
		return "northwest"
	elseif(theta>=-112.5 and theta<-67.5) then
		return "north"
	elseif(theta>=-67.5 and theta<-22.5) then
		return "northeast"	
	elseif(theta>=-22.5 and theta<22.5) then 
		return "east"
	elseif(theta>=22.5 and theta<67.5) then
		return "southeast"
	elseif(theta>=67.5 and theta<112.5) then
		return "south"
	elseif(theta>=112.5 and theta<157.5) then
		return "southwest"		
	elseif(theta>=157.5 and theta <=202.5) then
		return "west"	
	else
		d({dx=dx,dy=dy,theta=theta,r=r,error="couldn't find a direction"})		
		return "maths fail"
	end
	
end
---- Tier 1
-- Ekrund
PointsOfInterest.points[6]={
	{name="Stonemine Tower",zoneX=50400,zoneY=5200},
	{name="Cannon Battery",zoneX=55900,zoneY=9700},
	{name="Order Warcamp",zoneX=56300,zoneY=16300}, -- Grudgekeg's Guard	
}

-- Bloodhorn
PointsOfInterest.points[11]={
	{name="Ironmane Outpost",zoneX=20900,zoneY=5800},
	{name="The Lookout",zoneX=16500,zoneY=13500},
	{name="Destruction Warcamp",zoneX=8000,zoneY=19400},
}

-- Norsca
PointsOfInterest.points[100]={
	{name="Lost Lagoon",zoneX=34500,zoneY=58300},	
}

-- Nordland
PointsOfInterest.points[106]={
	{name="The Harvest Shrine",zoneX=40400,zoneY=13300},
	{name="Festenplatz",zoneX=35100,zoneY=9900},
	{name="The Nordland XI",zoneX=25300,zoneY=4400},
	{name="Order Warcamp",zoneX=28400,zoneY=19200},
	{name="Destruction Warcamp",zoneX=40000,zoneY=4600},
}

-- Blighted Isle 
PointsOfInterest.points[200]={
	{name="Altar Of Khaine",zoneX=24000,zoneY=63000},
	{name="Destruction Warcamp",zoneX=26500,zoneY=57500}, -- Cynathai Span
	{name="House of Lorendyth",zoneX=45200,zoneY=54500},
	{name="Order Warcamp",zoneX=44900,zoneY=48900},	
}
-- Chrace
PointsOfInterest.points[206]={
	{name="Tower of Nightflame",zoneX=45000,zoneY=3500},
	{name="Shard of Grief",zoneX=29000,zoneY=11600}
}

---- 

--- Tier 2

-- Marshes of Madness

PointsOfInterest.points[1]={	
	{name="Destruction Warcamp",zoneX=26700,zoneY=26600}, -- Morth's Mire
	{name="Fangbreaka Swamp",zoneX=32200,zoneY=32400},
	{name="Alcadizaar's Tomb",zoneX=26500,zoneY=38500},
	{name="Goblin Armoury",zoneX=19200,zoneY=46300},
	{name="Order Warcamp",zoneX=40300,zoneY=42200}, -- Thurarikson's Warcamp
}

-- Barak Varr

PointsOfInterest.points[7]={
	{name="The Ironclad",zoneX=38300,zoneY=30800},
	{name="The Lighthouse",zoneX=15200,zoneY=33729},
	{name="Dok Karaz",zoneX=31200,zoneY=36200},
	{name="Order Warcamp",zoneX=22400,zoneY=44600}, -- Goldpeak's Warcamp
	{name="Destruction Warcamp",zoneX=41400,zoneY=43200}, -- Foultooth's Warcamp
}



-- Troll Country

PointsOfInterest.points[101]={
	{name="Ruins of Greystone Keep",zoneX=2900,zoneY=50900},
	{name="Stonetroll Kep",zoneX=2500,zoneY=56600},
	{name="Monastery of Morr",zoneX=33900,zoneY=59700},
	{name="Order Warcamp",zoneX=40700,zoneY=60500}, -- Blackbramble hollow
}

-- Ostland

PointsOfInterest.points[107]={
	{name="Destruction Warcamp",zoneX=28200,zoneY=4900}, -- Raven's Edge
	{name="Mandred's Hold",zoneX=39400,zoneY=3800},
	{name="Kinschel's Stronghold",zoneX=56300,zoneY=5800},
	{name="Crypt of Weapons",zoneX=25900,zoneY=9400},
}

-- The Shadowlands

PointsOfInterest.points[201]={
	{name="Destruction Warcamp",zoneX=24300,zoneY=51300}, -- Oath's end
	{name="Order Warcamp",zoneX=46300,zoneY=42400}, -- Bladewatch
	{name="The Shadow Spire",zoneX=28600,zoneY=52700},
	{name="The Unicorn Siege Camp",zoneX=41000,zoneY=48400},
	{name="Spite's Reach",zoneX=33000,zoneY=48700},
}

-- Ellyrion

PointsOfInterest.points[207]={
	{name="The Needle of Ellyrion",zoneX=26800,zoneY=9100},
	{name="The Reaver Stables",zoneX=39100,zoneY=14100},
	{name="Cascades of Thunder",zoneX=34500,zoneY=34500},
}



--- Tier 3

-- Black Fire Pass

PointsOfInterest.points[8]={
	{name="Gnol Baraz",zoneX=2900,zoneY=16400},
	{name="Furrig's Fall",zoneX=15900,zoneY=5300},
	{name="Bugman's Brewery",zoneX=25100,zoneY=18900},
	{name="Destruction Warcamp",zoneX=32600,zoneY=8800}, -- Torka's Boyz
	{name="Order Warcamp",zoneX=34800,zoneY=23400}, -- Odinadotr's Watch
}

-- The Badlands

PointsOfInterest.points[2]={
	{name="Destruction Warcamp",zoneX=46700,zoneY=45300}, -- Muggar's Choppaz
	{name="Order Warcamp",zoneX=16200,zoneY=33600}, -- Dour Guard
	{name="Thickmuck Pit",zoneX=20400,zoneY=41500},
	{name="Goblin Artillery Range",zoneX=27000,zoneY=60600},
	{name="Karagaz",zoneX=43400,zoneY=58900},
}


-- High Pass

PointsOfInterest.points[102]={
	{name="Stoneclaw Castle",zoneX=4600,zoneY=56000},
	{name="Feiten's Lock",zoneX=11300,zoneY=64400},
	{name="Ogrund's Tavern",zoneX=27900,zoneY=60000},
	{name="Hallenfurt Manor",zoneX=48500,zoneY=56600},	
}

-- Talabecland

PointsOfInterest.points[108]={
	{name="Destruction Warcamp",zoneX=21400,zoneY=6000}, -- Hellfang Ridge
	{name="Verentane's Tower",zoneX=29000,zoneY=8500},
	{name="Passwatch Castle",zoneX=37900,zoneY=7500},
}

-- Avelorn 

PointsOfInterest.points[202]={
	{name="Destruction Warcamp",zoneX=17200,zoneY=54500}, -- Isha's Fall
	{name="Order Warcamp",zoneX=41600,zoneY=55000}, -- Gaen Mere
	{name="Ghrond's Sacristy",zoneX=29200,zoneY=53900},
	{name="The Wood Choppaz",zoneX=20000,zoneY=62600},
	{name="Maiden's Landing",zoneX=38600,zoneY=63000},
}

-- Saphery

PointsOfInterest.points[208]={
	{name="Well of Qhaysh",zoneX=17400,zoneY=15900},
	{name="Sari' Daroir",zoneX=26300,zoneY=6100},
	{name="The Spire of Teclis",zoneX=39600,zoneY=10900},	
}

---- Tier 4

--- DvG

-- Stonewatch

PointsOfInterest.points[10]={
	{name="The Fortress",zoneX=30000,zoneY=55000},
}
	
-- Kadrin Valley
PointsOfInterest.points[9]={
	{name="Order Warcamp",zoneX=27600,zoneY=5800}, -- Gharvin's Brace
	{name="Hardwater Falls",zoneX=36900,zoneY=10300},
	{name="Icehearth Crossing",zoneX=29700,zoneY=19800},
	{name="Karaz Drengi",zoneX=45500,zoneY=18700},
	{name="Dolgrund's Cairn",zoneX=53000,zoneY=38500},	
	{name="Destruction Warcamp",zoneX=37400,zoneY=52400}, -- Krung's Scrappin' Spot	
	{name="Kazad Dammaz",zoneX=34200,zoneY=46700},	
	{name="Gromril Junction",zoneX=12100,zoneY=34500},	
}
	
-- Thunder Mountain

PointsOfInterest.points[5]={
	{name="Order Warcamp",zoneX=37200,zoneY=11400}, -- Hammerstriker Point
	{name="Thargrim's Headwall",zoneX=20700,zoneY=26300},
	{name="Doomstriker Vein",zoneX=20500,zoneY=33300},
	{name="Karak Karag",zoneX=2600,zoneY=35200},
	{name="Bloodfist Rock",zoneX=37600,zoneY=35400},
	{name="Gromril Kruk",zoneX=48500,zoneY=34600},
	{name="Karak Palik",zoneX=24100,zoneY=42700},
	{name="Destruction Warcamp",zoneX=37100,zoneY=57700}, -- Mudja's Warcamp
}

-- Black Crag

PointsOfInterest.points[3]={
	{name="Squiggly Beast Pens",zoneX=36800,zoneY=3000},
	{name="Order Warcamp",zoneX=46300,zoneY=6800}, -- Kagrund's Stand
	{name="Ironskin Skar",zoneX=27400,zoneY=20100},
	{name="Rottenpike Ravine",zoneX=23700,zoneY=30700},
	{name="Madcap Pickins",zoneX=45700,zoneY=30200},
	{name="Lobba Mill",zoneX=26300,zoneY=58000},
	{name="Destruction Warcamp",zoneX=17800,zoneY=60700}, -- Gudmud's Strong-Huts
	{name="Badmoon Hole",zoneX=46200,zoneY=53800},
}

-- Butcher's Pass

PointsOfInterest.points[4]={
	{name="The Fortress",zoneX=30800,zoneY=16000},
}

--- EvC

-- The Maw

PointsOfInterest.points[104]={
	{name="The Fortress",zoneX=20400,zoneY=52500},
}

-- Chaos Wastes

PointsOfInterest.points[103]={
	{name="Chokethorn Bramble",zoneX=33000,zoneY=5100},
	{name="Thaugamond Massif",zoneX=22300,zoneY=9700},
	{name="Zimmeron's Hold",zoneX=31700,zoneY=9400},
	{name="The Statue of Everchosen",zoneX=27500,zoneY=44200},
	{name="Charon's Keep",zoneX=38600,zoneY=43100},
	{name="The Shrine of Time",zoneX=45200,zoneY=45700},
	{name="Destruction Warcamp",zoneX=23700,zoneY=30300}, -- Seven Shades Creep
	{name="Order Warcamp",zoneX=48400,zoneY=37100}, -- Tannebach's Doom
}

-- Praag

PointsOfInterest.points[105]={
	{name="Garrison of Skulls",zoneX=35400,zoneY=11800},
	{name="Kurlov's Armory",zoneX=42100,zoneY=29300},
	{name="Martyr's Square",zoneX=30700,zoneY=34800},
	{name="Order Warcamp",zoneX=16200,zoneY=36500}, -- Westmark Barricade
	{name="Destruction Warcamp",zoneX=44100,zoneY=38300}, -- Ravensworn
	{name="Manor of Ortel von Zaris",zoneX=27000,zoneY=48100},
	{name="Russenscheller Graveyard",zoneX=24900,zoneY=58300},
	{name="Southern Garrison",zoneX=37100,zoneY=63400},
}

-- Reikland

PointsOfInterest.points[109]={
	{name="Frostbeard's Quarry",zoneX=37500,zoneY=7700},
	{name="Order Warcamp",zoneX=24900,zoneY=13300}, -- Deathwatch Landing
	{name="Wilhelm's Fist",zoneX=30600,zoneY=27000},
	{name="Destruction Warcamp",zoneX=41900,zoneY=30900}, -- Darkstone Vantage
	{name="Reikwatch",zoneX=31800,zoneY=39200},
	{name="Runehammer Gunworks",zoneX=22400,zoneY=45700},
	{name="Morr's Repose",zoneX=23100,zoneY=59500},
	{name="Schwenderhalle Manor",zoneX=25500,zoneY=64000},
}

-- Reikwald

PointsOfInterest.points[110]={
	{name="The Fortress",zoneX=21900,zoneY=16500},
}

--- HEvDE

-- Fell Landing

PointsOfInterest.points[204]={
	{name="The Fortress",zoneX=49800,zoneY=29600},
}

-- Caledor

PointsOfInterest.points[203]={
	{name="Hatred's Way",zoneX=2200,zoneY=31800},
	{name="Druchii Barracks",zoneX=17800,zoneY=29700},
	{name="Shrine of the Conqueror",zoneX=31400,zoneY=31600},
	{name="Destruction Warcamp",zoneX=29700,zoneY=24700}, -- Conqueror's Watch
	{name="Order Warcamp",zoneX=28800,zoneY=40500}, -- Conqueror's Descent
	{name="Sarathanan Vale",zoneX=39200,zoneY=30900},
	{name="Wrath's Resolve",zoneX=45000,zoneY=32100},
	{name="Senlathain Stand",zoneX=62200,zoneY=34500},
}

-- Dragonwake

PointsOfInterest.points[205]={
	{name="Fireguard Spire",zoneX=7600,zoneY=34500},
	{name="Drakebreaker's Scourge",zoneX=18700,zoneY=31400},
	{name="Mournfire's Approach",zoneX=21700,zoneY=29500},
	{name="Order Warcamp",zoneX=1100,zoneY=26100}, -- Drakewarden Keep
	{name="Pelgorath's Ember",zoneX=26100,zoneY=42600},
	{name="Covenant of Flame",zoneX=37500,zoneY=30500},
	{name="Milaith's Memory",zoneX=52000,zoneY=41100},
	{name="Destruction Warcamp",zoneX=52600,zoneY=45500}, -- Celrath Pass
}

-- Eataine 

PointsOfInterest.points[209]={
	{name="Chillwind Manor",zoneX=6300,zoneY=34500},
	{name="Pillars of Remembrence",zoneX=17100,zoneY=38000},
	{name="Bel-Korhadris' Solitude",zoneX=25200,zoneY=35600},
	{name="Destruction Warcamp",zoneX=28400,zoneY=43700}, -- Ebonhold Watch
	{name="Sanctuary of Dreams",zoneX=36000,zoneY=34700},
	{name="Order Warcamp",zoneX=36000,zoneY=26100}, -- Eataine Mustering
	{name="Arbor of Light",zoneX=54500,zoneY=34300},
	{name="Uthorin Siege Camp",zoneX=63400,zoneY=33000},
}

-- Shining Way

PointsOfInterest.points[210]={
	{name="The Fortress",zoneX=14300,zoneY=32500},
}



---- Scenarios 

--- Tier 1

-- Khaine's Embrace
PointsOfInterest.points[230]={
	{name="Death's Charge",zoneX=10000,zoneY=3600},
	{name="Dance of Swords",zoneX=4118,zoneY=3600},	
	{name="Order Spawn",zoneX=2800,zoneY=9700},	
	{name="Destruction Spawn",zoneX=11600,zoneY=9300},	
}
-- Nordenwatch
PointsOfInterest.points[130]={
	{name="Order Spawn",zoneX=13500,zoneY=11600},
	{name="Destruction Spawn",zoneX=12900,zoneY=3000},
	{name="The Barracks",zoneX=7900,zoneY=5300},
	{name="The Fortress",zoneX=3300,zoneY=8800},
	{name="The Lighthouse",zoneX=8720,zoneY=11900},
}

-- Gates of Ekrund
PointsOfInterest.points[30]={
	{name="Order Spawn",zoneX=7800,zoneY=1800},
	{name="Destruction Spawn",zoneX=6300,zoneY=11700},
	{name="Ammunitions Cache",zoneX=3600,zoneY=6500},
	{name="Gate Switch",zoneX=5800,zoneY=6200},
	{name="Supply Room",zoneX=7900,zoneY=5700},
}


--- Tier 2

-- Mourkain Temple
PointsOfInterest.points[31]={
	{name="Order Spawn",zoneX=10325,zoneY=8346},
	{name="Destruction Spawn",zoneX=4278,zoneY=1923},
	{name="Mourkain Artifact Spawn",zoneX=5253,zoneY=6995},	
}

-- Stonetroll Crossing

PointsOfInterest.points[131]={
	{name="Order Spawn",zoneX=15900,zoneY=8900},
	{name="Destruction Spawn",zoneX=2300,zoneY=7800},
	{name="Troll Pacifier Spawn",zoneX=9500,zoneY=8600},
	{name="Gravel Bottom Trolls",zoneX=7400,zoneY=6800},
	{name="Boulder Fist Trolls",zoneX=11400,zoneY=6100},
	{name="Chuck Rock Trolls",zoneX=9600,zoneY=12700},
}

-- Phoenix Gate

PointsOfInterest.points[231]={
	{name="Order Spawn",zoneX=12199,zoneY=16583},
	{name="Destruction Spawn",zoneX=5683,zoneY=2354},
	{name="Order Flag Spawn",zoneX=8119,zoneY=13924},
	{name="Destruction Flag Spawn",zoneX=9174,zoneY=3755},
}

--- Tier 3


-- Highpass Cemetery

PointsOfInterest.points[139]={
	{name="Destruction Spawn",zoneX=4600,zoneY=1100},
	{name="Order Spawn",zoneX=4500,zoneY=8200},
	{name="The Stag",zoneX=7600,zoneY=4700},
	{name="The Crypt",zoneX=1300,zoneY=4700}
}

-- Temple of Isha

PointsOfInterest.points[236]={
	{name="Destruction Spawn",zoneX=14600,zoneY=4400},
	{name="Order Spawn",zoneX=3800,zoneY=4400},	
	{name="Temple of Isha",zoneX=8780,zoneY=13900},
	{name="Isha's Will Spawn",zoneX=8500,zoneY=6200}
}

-- Tor Anroc

PointsOfInterest.points[232]={
	{name="Destruction Spawn",zoneX=8600,zoneY=2500},
	{name="Order Spawn",zoneX=13700,zoneY=18300},	
	{name="Brimstone Bauble Spawn",zoneX=10200,zoneY=10700}
}


-- Doomfist Crater

PointsOfInterest.points[33]={
	{name="Destruction Spawn",zoneX=7602,zoneY=2114},
	{name="Order Spawn",zoneX=1754,zoneY=8292},	
	{name="Doomfist Crater Flag",zoneX=6672,zoneY=6538},
	{name="NW Doomfist Ore Spawn",zoneX=4400,zoneY=5400},
	{name="NE Doomfist Ore Spawn",zoneX=9900,zoneY=5600},
	{name="South Doomfist Ore Spawn",zoneX=7000,zoneY=11100}
}

-- Black Fire Basin

PointsOfInterest.points[38]={
	{name="Destruction Spawn",zoneX=12600,zoneY=1600},
	{name="Order Spawn",zoneX=4600,zoneY=18000},	
	{name="Order Flag Spawn",zoneX=9600,zoneY=17600},
	{name="Destruction Flag Spawn",zoneX=9100,zoneY=1400},
	{name="Cetus's Bane",zoneX=9200,zoneY=9400}
}


-- Talabec Dam

PointsOfInterest.points[132]={
	{name="Destruction Spawn",zoneX=9042,zoneY=20215},
	{name="Order Spawn",zoneX=11688,zoneY=1838},
	{name="The Dam",zoneX=4533,zoneY=17201},
	{name="The Windmill",zoneX=16858,zoneY=5587},
	{name="Bomb Spawn",zoneX=10389,zoneY=11198}
}

--- Tier 4

--- DvG

-- Gromril Crossing --
PointsOfInterest.points[43]={
	{name="Da Base Camp",zoneX=6962,zoneY=7353},
	{name="Pile O' Warstuff",zoneX=13998,zoneY=4862},
	{name="The Bridge",zoneX=11384,zoneY=11604},
	{name="The Repair Depot",zoneX=9039,zoneY=17418},
	{name="Engine Number Nine",zoneX=16172,zoneY=16075},
	{name="Destruction Spawn",zoneX=1539,zoneY=10138},
	{name="Order Spawn",zoneX=19080,zoneY=14218},
}

-- Howling Gorge --  
PointsOfInterest.points[44]={	
	{name="Powder Keg Spawn",zoneX=9126,zoneY=9742},
	{name="Order Target",zoneX=11592,zoneY=4213},
	{name="Destruction Target",zoneX=6474,zoneY=14860},
	{name="Destruction Spawn",zoneX=9352,zoneY=2323},
	{name="Order Spawn",zoneX=9454,zoneY=16689},
}                           

-- Logrin's Forge --                            
PointsOfInterest.points[39]={
	{name="Logrin's Hammer",zoneX=5341,zoneY=9126},
	{name="Logrin's Anvil",zoneX=13122,zoneY=9084},
	{name="Destruction Spawn",zoneX=9211,zoneY=17538},
	{name="Order Spawn",zoneX=9168,zoneY=757},
}                           
                                                                                        
-- Thunder Valley --                             
PointsOfInterest.points[34]={
	{name="Gyrocopter Hangar",zoneX=10499,zoneY=11095},
	{name="Blasting Cart",zoneX=5714,zoneY=5690},
	{name="Firing Range",zoneX=14856,zoneY=5928},
	{name="Gugnir's Fist",zoneX=6642,zoneY=14737},
	{name="Fireball Mortar",zoneX=14690,zoneY=14928},
	{name="Order Spawn",zoneX=11333,zoneY=19332},
	{name="Destruction Spawn",zoneX=10214,zoneY=1595},
	
}                           

--- EvC 

-- Reikland Hills --
PointsOfInterest.points[134]={
	{name="Destruction Spawn",zoneX=9623,zoneY=3690},
	{name="Order Spawn",zoneX=8976,zoneY=14845},
	{name="The Mill",zoneX=4796,zoneY=7379},
	{name="The Factory",zoneX=14565,zoneY=12105},
	{name="Fallen Bridge",zoneX=9839,zoneY=9710},
}                           
                             
-- Battle for Praag -- 
PointsOfInterest.points[136]={
	{name="Southern Praag",zoneX=5258,zoneY=13539},
	{name="South Central Praag",zoneX=8417,zoneY=12884},
	{name="Central Praag",zoneX=9861,zoneY=9996},
	{name="North Central Praag",zoneX=11260,zoneY=7108},
	{name="Northern Praag",zoneX=14464,zoneY=6599},
	{name="Destruction Spawn",zoneX=9139,zoneY=18548},
	{name="Order Spawn",zoneX=10402,zoneY=2369},
}      

-- Maw of Madness --
PointsOfInterest.points[133]={
	{name="Glyph of Madness Spawn",zoneX=6643,zoneY=8360},
	{name="Warpstone Talisman Spawn",zoneX=8096,zoneY=9020},
	{name="Glyph of Fury Spawn",zoneX=9454,zoneY=8228},
	{name="Order Spawn",zoneX=14078,zoneY=9303},
	{name="Destruction Spawn",zoneX=1849,zoneY=7850},
}                           
                     
-- Grovod Caverns --                            
PointsOfInterest.points[137]={
	{name="Destruction Spawn",zoneX=753,zoneY=3849},
	{name="Order Spawn",zoneX=9847,zoneY=6945},
}                           

--- elf

-- Serpent's Passage --                             
PointsOfInterest.points[234]={
	{name="Order Spawn",zoneX=12886,zoneY=2883},
	{name="Destruction Spawn",zoneX=3226,zoneY=3245},
	{name="Salvage Part Spawn",zoneX=8056,zoneY=13115},
}                           

-- Dragon's Bane --
PointsOfInterest.points[235]={
	{name="Destruction Spawn",zoneX=21960,zoneY=11202},
	{name="Order Spawn",zoneX=1097,zoneY=11411},
	{name="The Commorancy",zoneX=11636,zoneY=5908},
	{name="The Summoning Tower",zoneX=11515,zoneY=16973},
	{name="The Academy",zoneX=11803,zoneY=11385},					
	{name="Sun Dragon Beacon Spawn",zoneX=7050,zoneY=11280},					
	{name="Black Dragon Beacon Spawn",zoneX=16868,zoneY=11385},					
}                           

-- Caledor Woods --
PointsOfInterest.points[237]={
	{name="Destruction Spawn",zoneX=8667,zoneY=19054},
	{name="Order Spawn",zoneX=11611,zoneY=1531},
	{name="Objective",zoneX=7042,zoneY=10174},
}                           


-- Blood of the Black Cairn --
PointsOfInterest.points[238]={
	{name="Destruction Spawn",zoneX=8906,zoneY=17748},
	{name="Order Spawn",zoneX=8714,zoneY=2606},
	{name="The Fountain",zoneX=11875,zoneY=5681},
	{name="The Orchard",zoneX=11576,zoneY=14310},
	{name="The Estate",zoneX=6685,zoneY=9526},
}                           

         
---- Cities

-- Contested Altdorf
PointsOfInterest.points[168]={
	{name="Order Spawn",zoneX=21524,zoneY=16895},
	{name="Destruction Spawn",zoneX=18899,zoneY=30640},
	{name="Temple of Sigmar",zoneX=9116,zoneY=24149},
	{name="The Docks",zoneX=27347,zoneY=17849},
} 
 
 
-- Altdorf WAR Quarters
PointsOfInterest.points[41]={
	{name="Order Spawn",zoneX=16103,zoneY=21694},
	{name="Destruction Spawn",zoneX=16328,zoneY=31178},
{name="Warmachine Quarters North",zoneX=21945,zoneY=22665},
{name="Warmachine Quarters South",zoneX=21983,zoneY=30058},
{name="Warmachine Quarters Bomb Target",zoneX=22469,zoneY=26250},
{name="Militia Quarters North",zoneX=10486,zoneY=22777},
{name="Militia Quarters South",zoneX=10532,zoneY=30096},
{name="Militia Quarters Bomb Target",zoneX=8763,zoneY=26175},
{name="The Courtyard",zoneX=16103,zoneY=26212},
} 
                       
                             
---- Cities

-- Contested IC
PointsOfInterest.points[167]={
	{name="Order Spawn",zoneX=19590,zoneY=31122},
	{name="Destruction Spawn",zoneX=16971,zoneY=15410},
	{name="The Monolith",zoneX=12388,zoneY=21302},
	{name="Temple of the Damned",zoneX=25835,zoneY=23367},
}                        

-- Undercroft
PointsOfInterest.points[42]={
	{name="Corrupters Crown",zoneX=20000,zoneY=32900},
	{name="Lower Temple",zoneX=25000,zoneY=33000},
	{name="Upper Temple",zoneX=27400,zoneY=33100},
	{name="Soul Blight Stone",zoneX=11700,zoneY=33200},
	{name="Soul Blight Court",zoneX=13600,zoneY=33200},
	{name="Soul Blight North",zoneX=11000,zoneY=30100},
	{name="Soul Blight Overlook",zoneX=11400,zoneY=35500},
	{name="Destruction Spawn",zoneX=19800,zoneY=28400},
	{name="Order Spawn",zoneX=20200,zoneY=37800}	
}


---- Land of the Dead

-- Necropolis of Zandri

PointsOfInterest.points[191]={
-- destro
	{name="Da Dusty Dry",brief="DWC",zoneX=56997,zoneY=7455},
	{name="Sedjhet Temple",zoneX=46287,zoneY=8239},
	{name="Obelisk of Judgement",zoneX=54601,zoneY=23967},
	{name="Aerie of Death",zoneX=43666,zoneY=20972},
	{name="Forbidden Vaults",zoneX=33779,zoneY=17826},
	{name="Hall of the Heavens",zoneX=23293,zoneY=12433},
	{name="Pit of Asaph",zoneX=13032,zoneY=14000},
-- center		
	{name="Temple of Ualatp",zoneX=4269,zoneY=26589},
	{name="The Quay of Seftu",zoneX=19848,zoneY=26439},
-- lairs	
	{name="Tomb of the Sun",zoneX=14000,zoneY=3000},
	{name="Tomb of the Stars",zoneX=59000,zoneY=25000},	
	{name="Tomb of the Sky",zoneX=60000,zoneY=32000},
	{name="Tomb of the Moon",zoneX=10400,zoneY=48700},
-- order	
	{name="Goldbarrow",brief="OWC",zoneX=61324,zoneY=45164},
	{name="Nikosi Temple",zoneX=50200,zoneY=49900},
	{name="The Carrion Nest",zoneX=54500,zoneY=40000},
	{name="The Quarry of Bone",zoneX=39300,zoneY=42700},
	{name="Tombs of the Bitter Winds",zoneX=31200,zoneY=34900},
	{name="The Library of Zandri",zoneX=21800,zoneY=46700},
	{name="Pit of Kem Semef",zoneX=13000,zoneY=37200},
		
	
}