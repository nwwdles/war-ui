local T = LibStub( "WAR-AceLocale-3.0" ):NewLocale( "WARCommander", "deDE")	-- both inherited from MapMonster, no need to include
if not T then return end
T[L"WARCommander Configuration"] = L"WARCommander Konfiguration"
T[L"Alerts Channel (RvR):"] = L"Alarm Kanal (RvR):"
T[L"Command Channel (RvR):"] = L"Kommando Kanal (RvR):"
T[L"Alerts Channel (Scen):"] = L"Alarm Kanal (Szen):"
T[L"Command Channel (Scen):"] = L"Kommando Kanal (Szen):"
T[L"Listen for WARCommander commands/alerts on:"] = L"Empfange Kommandos und Alarme auf diesen Kan�len:"
T[L"Other Options:"] = L"Andere Optionen"
T[L"Autocreate Waypoints"] = L"Automatische Wegpunkterstellung"
T[L"Show Updates Window"] = L"Zeige Logbuch"
T[L"Show Updates Title"] = L"Zeige Logbuch Titel"

-- valid channels
T[L"Warband"] = L"Kriegstrupp"
T[L"Party"] = L"Gruppe"
T[L"Region"] = L"Region"
T[L"RvR"] = L"RvR"
T[L"Scenario"] = L"Szenario"
T[L"Scenario Party"] = L"Szenariogruppe"
T[L"Say"] = L"Sagen"
T[L"Guild"] = L"Gilde"
T[L"Alliance"] = L"Allianz"

-- Update log
T[L"Updates"] = L"Logbuch"