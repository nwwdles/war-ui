local T = LibStub( "WAR-AceLocale-3.0" ):NewLocale( "WARCommander", "enUS", true)	-- both inherited from MapMonster, no need to include

T[L"WARCommander Configuration"] = true
T[L"Alerts Channel (RvR):"] = true
T[L"Command Channel (RvR):"] = true
T[L"Alerts Channel (Scen):"] = true
T[L"Command Channel (Scen):"] = true
T[L"Listen for WARCommander commands/alerts on:"] = true
T[L"Other Options:"] = true
T[L"Autocreate Waypoints"] = true
T[L"Show Updates Window"] = true
T[L"Show Updates Title"] = true

-- valid channels
T[L"Warband"] = true
T[L"Party"] = true
T[L"Region"] = true
T[L"RvR"] = true
T[L"Scenario"] = true
T[L"Scenario Party"] = true
T[L"Say"] = true
T[L"Guild"] = true
T[L"Alliance"] = true

-- Update log
T[L"Updates"] = true