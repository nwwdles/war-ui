--------------------------------------------------------------------------------
-- Copyright (c) 2009 Flimgoblin <fingoniel@roguishness.com>
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
--
--------------------------------------------------------------------------------
-- File:      WARCommander.lua
-- Date:      2009-03-28T15:01:00Z
-- Author:    Flimgoblin <fingoniel@roguishness.com>
-- Version:   Beta v0.6
-- Copyright: 2009
--------------------------------------------------------------------------------
-- Feel free to reuse/reproduce just credit me if you do
-- Requires LibSlash and MapMonster to run.
-- Uses the MapMonsterAPI from: Bloodwalker <metagamegeek@gmail.com>
--------------------------------------------------------------------------------
local T = LibStub("WAR-AceLocale-3.0"):GetLocale("WARCommander")

local pairs = pairs

local Player = GameData.Player

WARCommander = {			
	--lastProcessed=0,
	DECAY_TIME=30,
	MAIN_MAP_WINDOW="EA_Window_WorldMapZoneViewMapDisplay",
	MINI_MAP_WINDOW="EA_Window_OverheadMapMapDisplay",
	MINI_MAP_WINDOW_SCALE={40.93, 29,19.5},
	mapZoneId=0,
	contextCommands= {
		"Regroup",
		"Attack",
		"Defend",
		"Hold",
		"Group 1",
		"Group 2",
		"Group 3",
		"Group 4"
	},	
	mouse={
		x=0,
		y=0
	},	
	pinlookup={},
	labelColours={}
}
WARCommander.messageLog={}
WARCommander.default =
{
	version = 0.1,
	state = "on",
	channel="wb",
	alertchannel="channel2",
	scalertchannel="sc",
	scchannel="sp",
	delay = 2,
	hide=false,
	hideInfo=false,
	listening={},
	LONG_DURATION=300,
	SHORT_DURATION=180,
	customCommands={
		context={},
		alert={},
	},
	longpoiname=true,
	briefpoiname=false,
	briefzonename=false,
	chatColours=false,
	setWaypoints=true
}

WARCommander.listen_channels=
{
	wb=14,
	p=4,
	channel1=57,
	channel2=58,
	sc=15,
	sp=16,
	say=0,
	g=8,
	a=12
}
WARCommander.valid_channels = 
{
	wb = "Warband",	
	p = "Party",	
	channel1="Region",
	channel2="RvR",
	sc="Scenario",
	sp="Scenario Party",
	say = "Say",
	g="Guild",
	a="Alliance"
}

WARCommander.ignore = {}

local function pm(msg)
	if ( not msg ) then
		return
	else
		msg=tostring(msg)
		d(msg)
		EA_ChatWindow.Print(towstring("[WARCommander]: "..msg))
	end
end

local function getn(tab)
	if(type(tab)=='table') then
		for i,_ in ipairs(tab) do
		end
		return i
	else
		return 0
	end
end

local function slashcmds()
	pm("/warc ignore <name>");
	pm("Ignores any warband instructions from that person.");
	pm("/warc unignore <name>");
	pm("Stops ignoring any warband instructions from that person.");
	pm("/warc duration <alerts> <commands>")
	pm("Sets how many seconds it takes for the alerts and the commands to fade.")
	pm("/warc info")
	pm("Displays zone info") 	
	pm("/warc poi <name> <x> <y>")
	pm("Tool for collecting data - put in the name of the point of interest, the xcoord and the ycoord from mouseover on the map") 
	pm("/warc zonedata <x> <y>")
	pm("Tool for collecting data - mouse over your own arrow and type that") 
	pm("/warc custom <command>")
	pm("Creates a new custom command for the context window (UI pending)")
	pm("/warc custom delete <command>")
	pm("Deletes a command")
	pm("/warc chatcolour")
	pm("Toggles on or off the alert chat colours")
	pm("/warc waypoints")
	pm("Toggles on or off automatic generation of waypoints")
end	

local function CreatePinTypes()
	
--"target"
--"ress"
--	MapMonsterAPI.DeletePinType("WARCommanderress")
--	MapMonsterAPI.DeletePinType("WARCommandertarget")
	MapMonsterAPI.DeletePinType("WARCommander")
	if(not MapMonsterAPI.CreatePinType("WARCommander",{label=L"WARCommander"},true)) then 
		d(MapMonster.ERROR_CODE)
	end

	if(not MapMonsterAPI.CreatePinSubType("WARCommander","target",{label=L"Target Spotted",mapIcon={ texture = "map_markers01", scale = 0.9, slice = "Shout-Incoming"}},true)) then
		d(MapMonster.ERROR_CODE)
	end 
	if(not MapMonsterAPI.CreatePinSubType("WARCommander","targetwb",{label=L"Warband Spotted",mapIcon={ texture = "EA_HUD_01", scale = 0.65, slice = "RvR-Flag" }},true)) then
		d(MapMonster.ERROR_CODE)
	end 
	if(not MapMonsterAPI.CreatePinSubType("WARCommander","targetgroup",{label=L"Group Spotted",mapIcon={ texture = "map_markers01", scale = 1.2, slice = "Shout-Incoming"}},true)) then
		d(MapMonster.ERROR_CODE)
	end 
	
	if(not MapMonsterAPI.CreatePinSubType("WARCommander","killtarget",{label=L"Target Spotted",mapIcon={ texture = "EA_HUD_01", scale = 0.9, slice = "PartyMarker-Sword"}},true)) then
		d(MapMonster.ERROR_CODE)
	end 
	
	if(not MapMonsterAPI.CreatePinSubType("WARCommander","ress",{label=L"Need Ress",mapIcon={ texture = "EA_HUD_01", scale = 0.9, slice = "PartyMarker-Skull", tint=DefaultColor.GREEN}},true)) then
		d(MapMonster.ERROR_CODE)
	end 
		
	if(not MapMonsterAPI.CreatePinSubType("WARCommander","regroup",{label=L"Regroup",mapIcon={ texture = "map_markers01", scale =0.875, slice = "Waypoint-Large", tint=DefaultColor.GREEN}},true)) then
		d(MapMonster.ERROR_CODE)
	end 
	if(not MapMonsterAPI.CreatePinSubType("WARCommander","position",{label=L"Position",mapIcon={ texture = "map_markers01", scale =0.875, slice = "Waypoint-Large"}},true)) then
		d(MapMonster.ERROR_CODE)
	end 
	if(not MapMonsterAPI.CreatePinSubType("WARCommander","other",{label=L"Other",mapIcon={ texture = "map_markers01", scale =0.875, slice = "Waypoint-Large", tint=DefaultColor.BLUE}},true)) then
		d(MapMonster.ERROR_CODE)
	end 
	
	
	-- charge
	if(not MapMonsterAPI.CreatePinSubType("WARCommander","attack",{label=L"Attack",mapIcon={ texture = "map_markers01", scale =0.875, slice = "Waypoint-Large", tint=DefaultColor.RED}},true)) then
		d(MapMonster.ERROR_CODE)
	end 
	
	-- group1	
	if(not MapMonsterAPI.CreatePinSubType("WARCommander","group1",{label=L"Group 1 Target",mapIcon={ texture = "EA_HUD_01", scale = 0.9, slice = "PartyMarker-Fist"}},true)) then
		d(MapMonster.ERROR_CODE)
	end 
	
	-- group2
	if(not MapMonsterAPI.CreatePinSubType("WARCommander","group2",{label=L"Group 2 Target",mapIcon={ texture = "EA_HUD_01", scale = 0.9, slice = "PartyMarker-Moon"}},true)) then
		d(MapMonster.ERROR_CODE)
	end 
	
	-- group3
	if(not MapMonsterAPI.CreatePinSubType("WARCommander","group3",{label=L"Group 3 Target",mapIcon={ texture = "EA_HUD_01", scale = 0.9, slice = "PartyMarker-Shield"}},true)) then
		d(MapMonster.ERROR_CODE)
	end 
	
	-- group4
	if(not MapMonsterAPI.CreatePinSubType("WARCommander","group4",{label=L"Group 4 Target",mapIcon={ texture = "EA_HUD_01", scale = 0.9, slice = "PartyMarker-Sun"}},true)) then
		d(MapMonster.ERROR_CODE)
	end 
end

function WARCommander.Init()		
	for k,v in pairs(WARCommander.listen_channels) do
		WARCommander.default.listening[v] = 1		
	end
	WARCommander.SetUpVariables()
	
	if LibSlash then LibSlash.RegisterSlashCmd("warc", WARCommander.command) end	
	RegisterEventHandler( SystemData.Events.CHAT_TEXT_ARRIVED, "WARCommander.OnChatUpdate")

	-- hook EA_Window_WorldMap.ShowZone
	WARCommander.hookShowZone = EA_Window_WorldMap.ShowZone
	EA_Window_WorldMap.ShowZone = WARCommander.ShowZoneHooked

	WARCommander.hookHyperLinkLButtonUp=EA_ChatWindow.OnHyperLinkLButtonUp
	EA_ChatWindow.OnHyperLinkLButtonUp=WARCommander.OnHyperLinkLButtonUp
	
	WindowRegisterEventHandler( WARCommander.MAIN_MAP_WINDOW, SystemData.Events.R_BUTTON_DOWN_PROCESSED, "WARCommander.OnClickMap" )
	if(MiniMapMonster) then
		if MiniMapMonster.CurrentMiniMap then WARCommander.MINI_MAP_WINDOW = MiniMapMonster.CurrentMiniMap end
		WindowRegisterEventHandler( WARCommander.MINI_MAP_WINDOW, SystemData.Events.R_BUTTON_DOWN_PROCESSED, "WARCommander.OnClickMiniMap" )
		WARCommander.HookSetMiniMapWindow=MiniMapMonster.SetMiniMapWindow
		MiniMapMonster.SetMiniMapWindow=function(window)
			WindowUnregisterEventHandler( WARCommander.MINI_MAP_WINDOW, SystemData.Events.R_BUTTON_DOWN_PROCESSED)
			WARCommander.MINI_MAP_WINDOW=window
			WindowRegisterEventHandler( WARCommander.MINI_MAP_WINDOW, SystemData.Events.R_BUTTON_DOWN_PROCESSED, "WARCommander.OnClickMiniMap" )
			pm("Using "..tostring(window))
			WARCommander.HookSetMiniMapWindow(window)
		end
	end
	 
	CreateWindow("WARCommander", true)	
	LayoutEditor.RegisterWindow("WARCommander", L"WARCommander", L"WARCommander window.", false, false, true, nil)	
	
	CreateWindow("WARCommanderButton", true)	
	LayoutEditor.RegisterWindow("WARCommanderButton", L"WARCommanderButton", L"WARCommander show/hide button.", false, false, true, nil)		
	
	--LabelSetText("WARCommanderTitle",L"WARCommander")

	WindowSetTintColor("WARCommanderRess",0,255,0)	
		
	CreateWindow("WARCommanderInfo", true)	
	LayoutEditor.RegisterWindow("WARCommanderInfo", L"WARCommanderInfo", L"WARCommander info window.", false, false, true, nil)	
	WindowSetDimensions("WARCommanderInfo",420,200)		
	LabelSetText("WARCommanderInfoTitle",T[L"Updates"])
	LabelSetText("WARCommanderInfoLastMessage0",L"")		
	LabelSetText("WARCommanderInfoLastMessage1",L"")		
	LabelSetText("WARCommanderInfoLastMessage2",L"")		
	 
	LabelSetText("WARCommanderAssist",L"No target")	
	
	CreatePinTypes() 
	
	-- delete leftover pins
	if(WARCommander_pins) then		
		for k,pin in pairs(WARCommander_pins) do
			if (k ~= "length") then MapMonsterAPI.DeletePin(pin["pinId"]) end
		end
	end
	WARCommander_pins = { length = 0}
	
	if(WARCommander_config.hide) then WARCommander.Hide() end
	
	if(WARCommander_config.hideInfo) then
		WARCommander.HideInfo()		
	else
		WARCommander.ShowInfo()	
	end
	
	if(WARCommander_config.hideInfoTitle) then
		WARCommander.HideInfoTitle()		
	else
		WARCommander.ShowInfoTitle()	
	end
	
	WARCommanderConfig.Init()
	
	pm("WARCommander loaded - use /warc for options")
end

function WARCommander.OnHyperLinkLButtonUp(info,x,y,z)
	local zoneX,zoneY,zoneId=info:match(L"WARC:([0-9]+),([0-9]+),([0-9]+)")
	if(zoneX and zoneY and zoneId) then
		local lookup=zoneX..L"-"..zoneY..L"-"..zoneId
		local pnum=WARCommander.pinlookup[lookup]
		local found=false
		if(pnum) and (WARCommander_pins[pnum]) then
			local pin=WARCommander_pins[pnum]["pinId"]
			if(pin) then
				found=true				
				MapMonsterAPI.HighlightMapPin(pin)
			end
		end
		if(not found) then
			pm("Pin has expired.")
		end
	else
		WARCommander.hookHyperLinkLButtonUp(info,x,y,z)	
	end
end

function WARCommander.ShowZoneHooked(zoneId)
	WARCommander.mapZoneId=zoneId
	WARCommander.hookShowZone(zoneId)
end

function WARCommander.ChatButtonMouseOver(msg)

    local windowName	= SystemData.ActiveWindow.name
	local msg=nil
	if(windowName:match("SpotWarband")) then
		msg=L"Spotted Warband"	
	elseif(windowName:match("SpotGroup")) then
		msg=L"Spotted Group"
	elseif(windowName:match("Spot")) then
		msg=L"Spotted Enemy"
	elseif(windowName:match("Ress")) then
		msg=L"Ress request"
	elseif(windowName:match("Target")) then
		msg=L"Call target"
	elseif(windowName:match("Position")) then
		msg=L"Report position"
	elseif(windowName:match("Clear")) then
		msg=L"Report all clear"
	end	
	
	if(msg)	then
	    Tooltips.CreateTextOnlyTooltip (windowName, nil)
	    Tooltips.SetTooltipText (1, 1, msg)
	    --Tooltips.SetTooltipColorDef (1, 1, Tooltips.COLOR_BODY)
	    Tooltips.Finalize()
	    
	    local anchor = { Point="left", RelativeTo=windowName, RelativePoint="right", XOffset=0, YOffset=0 }
	    Tooltips.AnchorTooltip (anchor)
	    Tooltips.SetTooltipAlpha (1)
	end
end

function WARCommander.UpdateMessageLog(msg,pin)

	local msgstart,msgend,_=msg:match(L"(.*)<LINK data=\"WARC:[0-9]+,[0-9]+,[0-9]+\" text=\"([^\"]*)\"(.*)>")

	if(msgend) then
		msg=msgstart..L" "..msgend			
	end

	WARCommander.messageLog[2]=WARCommander.messageLog[1]
	WARCommander.messageLog[1]=WARCommander.messageLog[0]
	WARCommander.messageLog[0]={message = msg,
		pinId=pin	
	}
	for k=0,2 do	
		if(WARCommander.messageLog[k]) then 
			local lmsg=WARCommander.messageLog[k].message
			LabelSetText("WARCommanderInfoLastMessage"..k,lmsg)
			if(lmsg:match(L"Spotted")) then
				LabelSetTextColor("WARCommanderInfoLastMessage"..k, 250,30,30)
			elseif(lmsg:match(L"Ress")) then
				LabelSetTextColor("WARCommanderInfoLastMessage"..k, 30,250,30)
			elseif(lmsg:match(L"Attack")) then
				LabelSetTextColor("WARCommanderInfoLastMessage"..k, 200,120,30)
			elseif(lmsg:match(L"Target")) then
				LabelSetTextColor("WARCommanderInfoLastMessage"..k, 200,120,30)
			elseif(lmsg:match(L"Regroup")) then
				LabelSetTextColor("WARCommanderInfoLastMessage"..k, 100,200,100)
			elseif(lmsg:match(L"Hold")) then
				LabelSetTextColor("WARCommanderInfoLastMessage"..k, 100,200,100)
			elseif(lmsg:match(L"Defend")) then
				LabelSetTextColor("WARCommanderInfoLastMessage"..k, 100,200,100)
			elseif(lmsg:match(L"Group1") or lmsg:match(L"Group 1")) then
				LabelSetTextColor("WARCommanderInfoLastMessage"..k, 100,150,200)
			elseif(lmsg:match(L"Group2") or lmsg:match(L"Group 2")) then
				LabelSetTextColor("WARCommanderInfoLastMessage"..k, 150,30,200)
			elseif(lmsg:match(L"Group3") or lmsg:match(L"Group 3")) then
				LabelSetTextColor("WARCommanderInfoLastMessage"..k, 30,150,30)
			elseif(lmsg:match(L"Group4") or lmsg:match(L"Group 4")) then
				LabelSetTextColor("WARCommanderInfoLastMessage"..k, 200,200,30)						
			else
				LabelSetTextColor("WARCommanderInfoLastMessage"..k, 200,200,200)
			end
		end
	end	
end		
	    	
function WARCommander.SetUpVariables()
	if(not WARCommander_config) then WARCommander_config={} end
	if(not PointsOfInterest_custompoints) then PointsOfInterest_custompoints={} end
	--d("custom:", PointsOfInterest_custompoints)
	for k,v in pairs(WARCommander.default) do
		if(not WARCommander_config[k]) then
			WARCommander_config[k] = v
		end
	end					
	WARCommander_config["version"] = WARCommander.default["version"]	
end

local lastProcessed = 0
function WARCommander.OnUpdate(elapsed)
	lastProcessed = lastProcessed + elapsed
	if(lastProcessed > 2) then
		lastProcessed = 0
		-- update age on pins and remove old ones
		for i,pin in pairs(WARCommander_pins) do	
			if(i~="length") and (pin["age"] >= 0) then
				pin["age"]= pin["age"] + 2
				if(pin["age"] > pin["maxage"]) then
					for j=0,2 do
						if(WARCommander.messageLog[j] and WARCommander.messageLog[j]["pinId"]==WARCommander_pins[i]["pinId"]) then
							LabelSetText("WARCommanderInfoLastMessage"..j,L"")	
							WARCommander.messageLog[j].message=nil							
						end
					end
					MapMonsterAPI.DeletePin(pin["pinId"])
					WARCommander_pins[i]=nil
				else
					if(pin["pinId"]) then
						local win="MapMonster_MapPin" .. pin["pinId"]				
						if DoesWindowExist(win) then
							local alpha=(pin["maxage"]-pin["age"])/pin["maxage"]	
							if alpha < 0.5 then alpha = 0.5 end						
							WindowSetAlpha(win,alpha)
						end
					end
				end
			end
		end
	end
end

local ChatData = GameData.ChatData
function WARCommander.OnChatUpdate()

	if(WARCommander_config.listening[ChatData.type]) then  
		local chat=ChatData.text
		local action,x,y,zoneId,extra=chat:match(L"<icon[0-9]+> (.*) {([0-9]+),([0-9]+),([0-9]+)}(.*)")
		if(action and zoneName and zoneId and x and y) then        
			if(extra) then
				local assist=extra:match(L"%[A:([0-9a-zA-Z ]+)%]")
				if(assist) then       				
					WARCommander.SetAssistTarget(assist)
				end
			end		
			WARCommander.AlertReceived(ChatData.name,action,zoneId,x,y,assist);	    	 		    		
		else    		    		
			-- check for State of Realm broadcasts
			local bo_type,bo_name=chat:match(L"Moving on to (.*): (.*) next")
			if(not bo_type) then    				
				bo_name=chat:match(L"Heading to .* to take (.*) from .*")
				if(not bo_name) then
					bo_name=chat:match(L"Heading to .* to defend (.*) from .*")
				end    				    			
			end
			if(bo_name) then    	    			
				if(PointsOfInterest.spelling[bo_name]) then
					bo_name=PointsOfInterest.spelling[bo_name]
				end    				    				 
				for zoneId,points in pairs(PointsOfInterest.points) do     			 		
					if(WARCommander.BroadcastSoRCommand(ChatData.name,zoneId,bo_name)) then return end
				end
			end    			
		end    		
	end
end	

function WARCommander.BroadcastSoRCommand(who,zoneId,bo_name)
	local points=PointsOfInterest.points[tonumber(zoneId)]	
	local original_bo_name=towstring(bo_name)
	bo_name=towstring(string.lower(tostring(bo_name)))    			
	local bo_name2=L"the "..bo_name   	
	for k,poi in pairs(points) do
		local poi_name=towstring(string.lower(tostring(poi.name)))
		local poi_name2=L"the "..poi_name		
		if(poi_name==bo_name or poi_name==bo_name2 or poi_name2==bo_name or poi_name2==bo_name2) then
			  local zoneName=WARCommander.GetZoneName(zoneId)
			  local action=L"Regroup at "..original_bo_name..L" in "..zoneName
			  WARCommander.AlertReceived(who,action,zoneId,poi.zoneX,poi.zoneY);	
		 	  local linkstart=L"<LINK data=\"WARC:"..poi.zoneX..L","..poi.zoneY..L","..zoneId..L"\" text=\""
		      local linkend=L"\" >"		      
			  pm(tostring(linkstart..L"SOR Target: "..towstring(poi.name)..linkend));	
			  return true	  			  
		end    							
	end							
	return false
end

function WARCommander.Broadcast(isCommand,text,pos,at,colour,assist)
	local zone=MapMonsterAPI.GetZoneData(pos.zoneId)
	local zoneName=WARCommander.GetZoneName(pos.zoneId)
	zoneName=zoneName:match(L"([a-zA-Z0-9 ]*)")
		
	local cmd
	-- check if in scen
	if(Player.isInScenario) then
		if(isCommand) then
			cmd = towstring(WARCommander_config.scchannel)
		else 
			cmd = towstring(WARCommander_config.scalertchannel)
		end
	else
		if(isCommand) then
			cmd = towstring(WARCommander_config.channel)
		else 
			cmd = towstring(WARCommander_config.alertchannel)
		end
	end
	if cmd == L"channel1" then 
		cmd = L"1" 
	elseif cmd == L"channel2" then 
		cmd = L"2"	
	end
	cmd = L"/"..cmd
	
	-- find nearby objective	
	poi=PointsOfInterest.FindNearbyPoint(pos,WARCommander_config.longpoiname,WARCommander_config.briefpoiname)
	local poitext=L""	
	if(poi) then
		poitext=towstring(poi.description) 	
		if(poi.at and at) then					
			text=text..L" at"		
		end	
	end
	
	local icon,bits=text:match(L"(<icon[0-9]+>) (.*)")
	
	local linkstart=L"<LINK data=\"WARC:"..pos.zoneX..L","..pos.zoneY..L","..pos.zoneId..L"\" text=\""
	local linkend=L"\""
	if(colour and WARCommander_config.chatColours) then	linkend = linkend..L" color=\""..towstring(colour)..L"\"" end
	linkend=linkend..L" >"
	
	if(icon and bits) then
		text=icon..L" "..linkstart..bits..L" "..poitext..L" in "..zoneName..linkend
	else
		text=linkstart..text..L" "..poitext..L" in "..zoneName..linkend		
	end				

	text=cmd..L" "..text..L" {"..pos.zoneX..L","..pos.zoneY..L","..pos.zoneId..L"}"	
	if(assist) then
		assist=assist:match(L"([0-9a-zA-Z ]*)")
		text=text..L" [A:"..assist..L"]"
	end
	SendChatText(text, L"")

end

function WARCommander.CreatePin(who,type,description,pos,duration,selfWaypoint)
	
	local msgstart,msgend,_=description:match(L"(.*)<LINK data=\"[^\"]*\" text=\"([^\"]*)\"(.*)>")
	if(msgend) then 
		description=msgstart..msgend;
	end
	local pin=MapMonsterAPI.CreatePin(L"WARCommander",type,who..L": "..description,pos,{L"Set by WARCommander using MapMonsterAPI"},nil,false)
	if(pin == false) then
		d("Error creating pin: "..MapMonster.ERROR_CODE)
	else	
		if(duration==0) then
		 	duration=WARCommander_config.SHORT_DURATION
		end			
		WARCommander_pins.length=WARCommander_pins.length+1
		WARCommander_pins[WARCommander_pins.length]={pinId=pin,
													age=0,
													maxage=duration}	
		WARCommander.lastPin=pin
		local lookup=pos.zoneX..L"-"..pos.zoneY..L"-"..pos.zoneId
		WARCommander.pinlookup[lookup]=WARCommander_pins.length
		WARCommander.UpdateMessageLog(who..L": "..description,pin)
				
		if(WARCommander_config.setWaypoints and MMNavigator and MMNavigator.isWPEnabled) then
			if(selfWaypoint or not Player.name==who) then		
				MMNavigator.AddAsWaypoint(MapMonsterAPI.GetPin(pin))
			end
		end
    end			
end

function WARCommander.SetAssistTarget(target)
	WARCommander.assistTarget=target
	
	LabelSetText("WARCommanderAssist",target)
	LabelSetTextColor("WARCommanderAssist",200,120,30)
end

function WARCommander.Assist()
	if(WARCommander.assistTarget) then
		pm("Targetting "..tostring(WARCommander.assistTarget))
		SendChatText(L"/target " .. WARCommander.assistTarget, L"")
	end
end

function WARCommander.AssistLabelTint()
	if(WARCommander.assistTarget) then WARCommander.LabelTint() end
end
function WARCommander.AssistLabelTintRemove()
	if(WARCommander.assistTarget) then WARCommander.LabelTintRemove() end
end

function WARCommander.AlertReceived(who,action,zoneId,x,y,assist)	
	if(WARCommander.ignore[tostring(who)]) then return end		

	if assist then WARCommander.SetAssistTarget(assist) end
	
	local what,dir,where = action:match(L"(.*) [0-9]+k (.*) of (.*)")
	if (what and dir and where) then action = what..L" near "..where end
	local targets = action:match(L"Spotted (.*)")
	local pos = MapMonsterAPI.ZoneToWorld({zoneId=zoneId,zoneX=x,zoneY=y})
	if targets then	
		if targets:match(L"warband") then
			WARCommander.CreatePin(who,L"targetwb",action,pos,WARCommander_config.SHORT_DURATION,false)														
		elseif(targets:match(L"group")) then
			WARCommander.CreatePin(who,L"targetgroup",action,pos,WARCommander_config.SHORT_DURATION,false)														
		else
			WARCommander.CreatePin(who,L"target",action,pos,WARCommander_config.SHORT_DURATION,false)
		end														
	else		
		if(action:match(L"Please could I have a ress%?")) then
			local front,back=action:match(L"(.*)Please could I have a ress%?(.*)")
			action=front..L"Ress please?"..back			
			WARCommander.CreatePin(who,L"ress",action,pos,WARCommander_config.SHORT_DURATION,false)
		elseif(action:match(L"Regroup")) then
			WARCommander.CreatePin(who,L"regroup",action,pos,WARCommander_config.LONG_DURATION,true)
		elseif(action:match(L"Attack")) then
			WARCommander.CreatePin(who,L"attack",action,pos,WARCommander_config.LONG_DURATION,true)
		elseif(action:match(L"Defend")) then
			WARCommander.CreatePin(who,L"regroup",action,pos,WARCommander_config.LONG_DURATION,true)
		elseif(action:match(L"Hold")) then
			WARCommander.CreatePin(who,L"regroup",action,pos,WARCommander_config.LONG_DURATION,true)
		elseif(action:match(L"Target")) then
			WARCommander.CreatePin(who,L"killtarget",action,pos,WARCommander_config.SHORT_DURATION,false)
		elseif(action:match(L"All clear")) then
			WARCommander.CreatePin(who,L"position",action,pos,WARCommander_config.SHORT_DURATION,true)
		elseif(action:match(L"I'm")) then
			local front,back=action:match(L"(.*)I'm(.*)")
			WARCommander.CreatePin(who,L"position",front..who..L"'s location"..back,pos,WARCommander_config.SHORT_DURATION,false)
		elseif(action:match(L"Group 1 move to")) then
			WARCommander.CreatePin(who,L"group1",action,pos,WARCommander_config.LONG_DURATION,true)
		elseif(action:match(L"Group 2 move to")) then
			WARCommander.CreatePin(who,L"group2",action,pos,WARCommander_config.LONG_DURATION,true)
		elseif(action:match(L"Group 3 move to")) then
			WARCommander.CreatePin(who,L"group3",action,pos,WARCommander_config.LONG_DURATION,true)
		elseif(action:match(L"Group 4 move to")) then
			WARCommander.CreatePin(who,L"group4",action,pos,WARCommander_config.LONG_DURATION,true)	
		else
			WARCommander.CreatePin(who,L"other",action,pos,WARCommander_config.LONG_DURATION,true)
		end
	end	
end

local function HighlightLast(val)
	if (WARCommander.messageLog[val]) and (WARCommander.messageLog[val].pinId) then
		MapMonsterAPI.HighlightMapPin(WARCommander.messageLog[val].pinId)
	end
end
function WARCommander.HighlightLast0() HighlightLast(0) end
function WARCommander.HighlightLast1() HighlightLast(1) end
function WARCommander.HighlightLast2() HighlightLast(2) end

function WARCommander.CollectPOI(str)
	local name,x,y=str:match("^(.*) ([0-9]+) ([0-9]+)$")
	if(name) then
		PointsOfInterest.points[Player.zone][tostring(name)]={name=tostring(name), zoneX=tonumber(x), zoneY=tonumber(y)}
		pm("Point of interest: {name=\""..name.."\",zoneX=\""..x.."\",zoneY=\""..y.."\"}")		
	else
		name=str
		local pos=MapMonsterAPI.GetPlayerPosition()
		PointsOfInterest.points[Player.zone][tostring(name)]={name=tostring(name), zoneX=pos.zoneX, zoneY=pos.zoneY}
		pm("Point of interest: {name=\""..name.."\",zoneX=\""..pos.zoneX.."\",zoneY=\""..pos.zoneY.."\"}")			
	end
end

function WARCommander.CollectCustomPOI(str)
	local name,x,y=str:match("^(.*) ([0-9]+) ([0-9]+)$")
	local zoneId=Player.zone		
	if(not PointsOfInterest_custompoints[zoneId]) then
		PointsOfInterest_custompoints[zoneId]={}
	end
	
	local zoneName=tostring(GetZoneName(zoneId))
	
	if(name) then
		PointsOfInterest_custompoints[zoneId][tostring(name)]={name=tostring(name), zoneX=tonumber(x), zoneY=tonumber(y)}
		pm("Added Point of interest: \""..name.."\" in \"..zoneName..\" at ("..x..","..y..")")		
	else
		name=str
		local pos=MapMonsterAPI.GetPlayerPosition()
		if(not pos.zoneX) then 
			if(pos.worldX) then			
				pm("Sorry, no data for this zone.")
			else
				pm("You need to move before you can log a point of interest.")
			end
		else			
			PointsOfInterest_custompoints[zoneId][tostring(name)]={name=tostring(name), zoneX=pos.zoneX, zoneY=pos.zoneY}
			pm("Added Point of interest: \""..name.."\" in \"..zoneName..\" at ("..pos.zoneX..","..pos.zoneY..")")	
		end			
	end
end

function WARCommander.CollectZoneData(str)
	local x,y=str:match("^([0-9]+) ([0-9]+)$")
	local pos=MapMonsterAPI.GetPlayerPosition()		
	if(x and y and pos) then
		x=tonumber(x)
		y=tonumber(y)
		x=pos.worldX-x
		y=pos.worldY-y
		pm("WARCommander_ZoneInfo["..pos.zoneId.."]={offsetX=\""..x.."\",offsetY=\""..y.."\"}")
	else
		pm("/warc zonedata <x> <y>")
	end
end

function WARCommander.PrintZoneInfo()
	local pos=MapMonsterAPI.GetPlayerPosition()
	pm("World coords: "..pos.worldX..", "..pos.worldY.." zone "..pos.zoneId)
end

function WARCommander.Spot(what)
	local pos=MapMonsterAPI.GetPlayerPosition()	
	local tgt=TargetInfo.m_Units["selfhostiletarget"]				
	
	if(not pos.zoneX) then 
		if(pos.worldX) then			
			pm("Sorry, no data for this zone.")
		else
			pm("You need to move before you can spot for targets.")
		end
	elseif(tgt and not tgt.isNPC and tgt.career ~= 0) then
		if(what) then
			WARCommander.Broadcast(false,L"<icon51> Spotted enemy "..what..L" of "..tgt.name..L" the "..tgt.careerName,pos,true,L"200,30,30")						
		else
			WARCommander.Broadcast(false,L"<icon51> Spotted "..tgt.name..L" the "..tgt.careerName,pos,true,"200,30,30")
		end						
	else
		if(what) then
			WARCommander.Broadcast(false,L"<icon51> Spotted enemy "..what,pos,true,"200,30,30") 			
		else			
			WARCommander.Broadcast(false,L"<icon51> Spotted enemies",pos,true,"200,30,30")
		end
	end
end

function WARCommander.Target()
	local pos=MapMonsterAPI.GetPlayerPosition()	
	local tgt=TargetInfo.m_Units["selfhostiletarget"]				
	--local tgt=TargetInfo.m_Units["selffriendlytarget"]				
	if(not pos.zoneX) then 
		if(pos.worldX) then			
			pm("Sorry, no data for this zone.")
		else
			pm("You need to move before you can spot for targets.")
		end
	elseif(tgt) then
		if(not tgt.isNPC and tgt.career ~= 0) then
			WARCommander.Broadcast(true,L"<icon51> Target "..tgt.name..L" the "..tgt.careerName..L" ("..tgt.healthPercent..L"%)",pos,true,"200,120,30",tgt.name)					
		elseif(tgt.name:match(L"[A-Za-z0-9]+")) then
			WARCommander.Broadcast(true,L"<icon51> Target "..tgt.name..L" ("..tgt.healthPercent..L"%)",pos,true,"200,120,30",tgt.name)			
		else		
			pm("You need a hostile target to call it.")									
		end
	else
		pm("You need a hostile target to call it.")						
	end
end

function WARCommander.ReportPosition()
	local pos=MapMonsterAPI.GetPlayerPosition()		
	
	if(not pos.zoneX) then 
		if(pos.worldX) then			
			pm("Sorry, no data for this zone.")
		else
			pm("You need to move before you can report your position.")
		end
	else
		WARCommander.Broadcast(true,L"<icon52> I'm",pos,true)					
	end
end

function WARCommander.AllClear()
	local pos=MapMonsterAPI.GetPlayerPosition()		
	
	if(not pos.zoneX) then 
		if(pos.worldX) then			
			pm("Sorry, no data for this zone.")
		else
			pm("You need to move before you can report all clear.")
		end
	else
		WARCommander.Broadcast(false,L"<icon52> All clear",pos,true)					
	end
end

function WARCommander.ToggleVisibility()
	if(WARCommander_config.hide) then
		WARCommander.Show()
	else
		WARCommander.Hide()
	end
end

function WARCommander.Hide()
	WindowSetShowing("WARCommander", false)	
	WARCommander_config.hide=true
end

function WARCommander.Show()
	WindowSetShowing("WARCommander", true)		
	WARCommander_config.hide=false
end

function WARCommander.HideInfo()	
	WindowSetShowing("WARCommanderInfo", false)	
	WARCommander_config.hideInfo=true
end

function WARCommander.ShowInfo()
	WindowSetShowing("WARCommanderInfo", true)	
	WARCommander_config.hideInfo=false
end
function WARCommander.HideInfoTitle()	
	WindowSetShowing("WARCommanderInfoTitle", false)	
	WARCommander_config.hideInfoTitle=true
end

function WARCommander.ShowInfoTitle()
	WindowSetShowing("WARCommanderInfoTitle", true)	
	WARCommander_config.hideInfoTitle=false
end

function WARCommander.cmd_spot() WARCommander.Spot(nil)	end
function WARCommander.cmd_spotgroup() WARCommander.Spot(L"group") end
function WARCommander.cmd_spotwb() WARCommander.Spot(L"warband") end

function WARCommander.cmd_ress()

	local pos=MapMonsterAPI.GetPlayerPosition()	
	
	if(not pos.zoneX) then 
		pm("Sorry, not got your position, no ress for you :(")
	else
		--if(Player.hitPoints.current==0) then
			WARCommander.Broadcast(false,L"<icon52> Please could I have a ress?",pos,true)
--		else
			--pm("You don't appear to be dead. Use the Position button to report your position.")
		--end	
	end
end

local round=function(num,idp) return tonumber(string.format("%." .. (idp or 0) .. "f", num)) end

function WARCommander.MouseCoordsToZone(mouseX,mouseY,zoneId,minimap)
	local win = WARCommander.MAIN_MAP_WINDOW 	
	if (minimap) then win = WARCommander.MINI_MAP_WINDOW end
	local windowX, windowY = WindowGetDimensions(win) -- 1000, 1000
	local windowScale = WindowGetScale(win)
		
	windowX = windowX * windowScale
	windowY = windowY * windowScale
 	local screenX, screenY = WindowGetScreenPosition(win) 	

	local anchorX = mouseX - screenX
	local anchorY = mouseY - screenY
		
	local pin = {}
	
	if(minimap) then
		local pos = MapMonsterAPI.GetPlayerPosition()
		if(not pos or not pos.worldX) then
			pm("Sorry, you have to move before you can order via the minimap.")
			return false
		end
		local dx = anchorX - windowX / 2
		local dy = anchorY - windowY / 2
		
		local zoom = GetOverheadMapZoomLevel()
		dx = dx / windowScale
		dy = dy / windowScale
		dx = dx * WARCommander.MINI_MAP_WINDOW_SCALE[zoom+1]
		dy = dy * WARCommander.MINI_MAP_WINDOW_SCALE[zoom+1]
		
		pin.worldX = math.floor(pos.worldX+dx)
		pin.worldY = math.floor(pos.worldY+dy)	
		pin.zoneId = pos.zoneId
		pin = MapMonsterAPI.WorldToZone(pin)			
	else	
		-- get the mapsize in coords
		local zoneInfo = MapMonsterAPI.GetZoneData(zoneId)
		if not zoneInfo or zoneInfo.noMap then return false end
		pin.zoneX = anchorX * zoneInfo.mapsizeX / windowX
		pin.zoneY = anchorY * zoneInfo.mapsizeY / windowY
		pin.zoneId = zoneId
		pin.zoneX = math.floor(pin.zoneX)
		pin.zoneY = math.floor(pin.zoneY)
	end
	
	return pin
end

function WARCommander.cmd_maporder(args,custom)
	local pos=WARCommander.MouseCoordsToZone(WARCommander.mouse.x,WARCommander.mouse.y,WARCommander.mapZoneId,false)
	if pos then WARCommander.CreateMapOrder(pos,args,custom) end
end

function WARCommander.cmd_minimaporder(args,custom)
	local mouseX=WARCommander.mouse.x;
	local mouseY=WARCommander.mouse.y;
	local pos=WARCommander.MouseCoordsToZone(mouseX,mouseY,0,true)
	
	if(pos) then
		WARCommander.CreateMapOrder(pos,args,custom)
	end	
end

function WARCommander.CreateMapOrder(pos,args,custom)
	if(args=='Regroup') then
		WARCommander.Broadcast(true,L"<icon45> Regroup",pos,true,L"100,200,100")
	elseif(args=='Attack') then
		WARCommander.Broadcast(true,L"<icon41> Attack",pos,false,L"200,120,30")
	elseif(args=='Defend') then
		WARCommander.Broadcast(true,L"<icon45> Defend",pos,false,L"100,200,100")
	elseif(args=='Hold') then
		WARCommander.Broadcast(true,L"<icon45> Hold",pos,true,L"100,200,100")
	elseif(args=='Group 1') then				
		WARCommander.Broadcast(true,L"<icon440> Group 1 move to",pos,false,L"100,150,200")	
	elseif(args=='Group 2') then
		WARCommander.Broadcast(true,L"<icon428> Group 2 move to",pos,false,L"150,30,200")	
	elseif(args=='Group 3') then
		WARCommander.Broadcast(true,L"<icon456> Group 3 move to",pos,false,L"30,150,30")	
	elseif(args=='Group 4') then
		WARCommander.Broadcast(true,L"<icon512> Group 4 move to",pos,false,L"200,200,30")
	elseif(custom) then
		WARCommander.Broadcast(true,L"<icon43> "..towstring(args),pos,true)			
	end
end

function WARCommander.cmd_ignore(name)
	WARCommander.ignore[name] = 1
	pm("Ignoring "..name)
end

function WARCommander.cmd_unignore(name)
	if(WARCommander.ignore[name]) then
		WARCommander.ignore[name] = nil
		pm("No longer ignoring "..name)
	else
		pm("You weren't ignoring "..name.." anyway")
	end
end

function WARCommander.DoClickMap(mapDisplay, zoneId, flags, x, y)	-- third party map handler

	-- Get the human readbale map coords of the mouse Click
	if not (x and y) then
		local mapPositionX, mapPositionY = WindowGetScreenPosition( mapDisplay )
		x = SystemData.MousePosition.x - mapPositionX
		y = SystemData.MousePosition.y - mapPositionY
	end
	local resolutionScale = InterfaceCore.GetResolutionScale() * WindowGetScale(mapDisplay)
	
	local mapX, mapY = MapGetCoordinatesForPoint( mapDisplay, x / resolutionScale, y / resolutionScale )
	local clickPos = { zoneId = zoneId, zoneX = mapX or 0, zoneY = mapY or 0, }

	local cleanPos = MapMonsterAPI.ValidateZonePos(clickPos)
	if cleanPos then
		EA_Window_ContextMenu.CreateContextMenu("WARCommander_contextMenu")	
		for k,v in pairs(WARCommander.contextCommands) do
			EA_Window_ContextMenu.AddMenuItem(towstring(v),function() WARCommander.CreateMapOrder(cleanPos,v,false) end, false, true)		
		end
		for k,v in pairs(WARCommander_config.customCommands.context) do
			EA_Window_ContextMenu.AddMenuItem(towstring(v.name),function() WARCommander.CreateMapOrder(cleanPos,v.name,true) end, false, true)		
		end
		EA_Window_ContextMenu.Finalize()
		
		return true	-- "has handled Map Click"
	else
		MapMonster.print("Cannot create pin in a zone with no map")
		return false
	end
	
end

function WARCommander.OnClickMap()									-- Mythic Main Map
	if(SystemData.MouseOverWindow.name==WARCommander.MAIN_MAP_WINDOW) then		
		WARCommander.mouse.x=SystemData.MousePosition.x
		WARCommander.mouse.y=SystemData.MousePosition.y
		EA_Window_ContextMenu.CreateContextMenu("WARCommander_contextMenu")	
		for k,v in pairs(WARCommander.contextCommands) do
			EA_Window_ContextMenu.AddMenuItem(towstring(v),function() WARCommander.cmd_maporder(v,false) end, false, true)		
		end
		for k,v in pairs(WARCommander_config.customCommands.context) do
			EA_Window_ContextMenu.AddMenuItem(towstring(v.name),function() WARCommander.cmd_maporder(v.name,true) end, false, true)		
		end
		EA_Window_ContextMenu.Finalize()
	end		
end

function WARCommander.OnClickMiniMap()	-- MiniMapMonster dependend; right click
	if (SystemData.MouseOverWindow.name == WARCommander.MINI_MAP_WINDOW) then		
		WARCommander.mouse.x = SystemData.MousePosition.x
		WARCommander.mouse.y = SystemData.MousePosition.y
		EA_Window_ContextMenu.CreateContextMenu("WARCommander_contextMenu")	
		for k,v in pairs(WARCommander.contextCommands) do
			EA_Window_ContextMenu.AddMenuItem(towstring(v),function() WARCommander.cmd_minimaporder(v,false) end, false, true)		
		end
		for k,v in pairs(WARCommander_config.customCommands.context) do
			EA_Window_ContextMenu.AddMenuItem(towstring(v.name),function() WARCommander.cmd_minimaporder(v.name,true) end, false, true)		
		end
		EA_Window_ContextMenu.Finalize()
	end		
end

function WARCommander.command(cmd)
	local opt, val = cmd:match("(%w+)[ ]?(.*)")
	if ( not opt or not val ) then
		slashcmds()
	else							
		if(opt=="ignore") then
			WARCommander.cmd_ignore(val)	
		elseif(opt=="unignore") then
			WARCommander.cmd_unignore(val)	
		elseif(opt=="info") then
			WARCommander.PrintZoneInfo()
		elseif(opt=="poi") then
			WARCommander.CollectPOI(val)
		elseif(opt=="custompoi") then
			WARCommander.CollectCustomPOI(val)
       elseif(opt=="zonedata") then
       	   WARCommander.CollectZoneData(val)						
		elseif(opt=="duration") then		
			local short,long=val:match("([0-9]+) ([0-9]+)")
			if(short and long) then
				WARCommander_config.LONG_DURATION=tonumber(long)
				WARCommander_config.SHORT_DURATION=tonumber(short)	
				pm("Alert duration now "..short.." seconds")
				pm("Command duration now "..long.." seconds")				
			else
				pm("/warc duration <alerts> <commands>")
				pm("Sets how many seconds it takes for the alerts and the commands to fade.") 
			end
		elseif(opt=="custom") then
			local cmd=val:match("delete ([a-zA-Z0-9 ]*)")						
			if(cmd) then
				if(WARCommander_config.customCommands.context[cmd]) then
					WARCommander_config.customCommands.context[cmd]=nil
					pm("Deleted command '"..cmd.."'")
				else
					pm("Could not find command '"..cmd.."'")
				end
			else
				local cmd=val:match("([a-zA-Z0-9 ]+)")			
				if(cmd) then
					WARCommander_config.customCommands.context[cmd]={name=cmd}
					pm("Added command '"..cmd.."' to right click map menu")
				else
					pm("/warc custom <command>")
					pm("Creates a new custom command for the context window (UI pending)")
					pm("/warc custom delete <command>")
					pm("Creates a new custom command for the context window (UI pending)")
				end
			end		
		elseif(opt=="colour" or opt=="color") then
			if(WARCommander_config.chatColours) then 
				WARCommander_config.chatColours=false
				pm("Chat colours switched off")
			else
				WARCommander_config.chatColours=true
				pm("Chat colours switched on")
			end	
		elseif(opt=="waypoints") then
			if(WARCommander_config.setWaypoints) then 
				WARCommander_config.setWaypoints=false
				pm("No longer creating waypoints")
			else
				WARCommander_config.chatColours=true
				pm("Now creating all pins as waypoints for map monster navigator")
			end			
		else
			slashcmds()
		end
	end
end

--[[function WARCommander.CreateChannelSelectWindow()
	local window = SystemData.ActiveWindow.name

	EA_Window_ContextMenu.CreateContextMenu("WARCommander_contextMenu")	
	for k,v in pairs(WARCommander.valid_channels) do
		EA_Window_ContextMenu.AddMenuItem(L"Use "..towstring(v),function() WARCommander.changeChannel(k) end, false, true)		
	end
	EA_Window_ContextMenu.Finalize()		
end

function WARCommander.CreateListenSelectWindow()
	local window = SystemData.ActiveWindow.name	

	EA_Window_ContextMenu.CreateContextMenu("WARCommander_contextMenu")	
	for k,v in pairs(WARCommander.listen_channels) do
		local name=WARCommander.valid_channels[k];
		if(WARCommander_config.listening[v]) then
			EA_Window_ContextMenu.AddMenuItem(L"Listening on "..towstring(name),function() WARCommander.BlockChannel(v) end, false, true)
		else
			EA_Window_ContextMenu.AddMenuItem(L"Blocking "..towstring(name),function() WARCommander.ListenChannel(v) end, false, true)
		end		
	end
	EA_Window_ContextMenu.Finalize()		
end

function WARCommander.changeChannel(chan)
	if(WARCommander.valid_channels[chan]) then
		if(chan=="channel1") then 
			chan="1"		
		elseif(chan=="channel2") then 
			chan="2"
		end
		WARCommander_config.channel=chan		
		pm("Changed broadcast channel to: "..chan)
		LabelSetText("WARCommanderChannel",L"Channel: "..towstring(WARCommander_config.channel))		
	else
		pm("Invalid channel: "..chan)
	end
end]]--

function WARCommander.ListenChannel(v) WARCommander_config.listening[v] = 1 end
function WARCommander.BlockChannel(v) WARCommander_config.listening[v] = 0 end

function WARCommander.LabelTint()
	if DoesWindowExist(SystemData.MouseOverWindow.name) then
		WARCommander.labelColours[SystemData.MouseOverWindow.name]={LabelGetTextColor(SystemData.MouseOverWindow.name)}		
		LabelSetTextColor(SystemData.MouseOverWindow.name,215,215,30)
	end
end

function WARCommander.LabelTintRemove()	
	if DoesWindowExist(SystemData.MouseOverWindow.name) then
		local col=WARCommander.labelColours[SystemData.MouseOverWindow.name]
		if(col) then		
			LabelSetTextColor(SystemData.MouseOverWindow.name,col[1],col[2],col[3])
		end
					
	end
end

WARCommander.BriefZoneNames={
	Dragonwake="DW",	
}

function WARCommander.GetZoneName(zoneId)
	local zname=GetZoneName(zoneId)
	if(WARCommander_config.briefzonename) then
		if(WARCommander.BriefZoneNames[zname]) then
			zname=WARCommander.BriefZoneNames
		end		
	end
	return zname
end
