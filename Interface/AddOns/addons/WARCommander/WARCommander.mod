<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="WARCommander" version="0.91" date="28/02/2011" >
		<VersionSettings gameVersion="1.4.0" windowsVersion="0.4" savedVariablesVersion="0.4" />
		<Author name="Wothor; Previously Flimgoblin" email="fingonielweb@roguishness.com" />
		<Description text="Addon to help organise a warband without voicecomms" />
	    <Dependencies>          
             <Dependency name="LibSlash" optional="false" forceEnable="true" /> 
             <Dependency name="MapMonster" optional="false" forceEnable="true"/> 
             <Dependency name="MiniMapMonster" optional="true" forceEnable="false"/> 
        </Dependencies>

		<Files>
			<File name="localization/enUS.lua" />
			<File name="localization/deDE.lua" />
			<File name="WARCommanderConfig.lua" />
			<File name="WARCommander.lua" />
			<File name="WARCommanderConfig.xml" />
			<File name="WARCommander.xml" />
			<File name="PointsOfInterest.lua" />
		</Files>
		<OnInitialize>
			<CallFunction name="WARCommander.Init" />
		</OnInitialize>
		<OnUpdate>
			<CallFunction name="WARCommander.OnUpdate" />
		</OnUpdate>
		<SavedVariables>
			<SavedVariable name="WARCommander_config" />
			<SavedVariable name="WARCommander_pins" />
			<SavedVariable name="PointsOfInterest_custompoints" />
		</SavedVariables>
		
		<WARInfo>
			<Categories>
				<Category name="RVR" />
				<Category name="GROUPING" />     
				<Category name="COMBAT" />
				<Category name="MAP" />
			</Categories>
			<Careers>
				<Career name="BLACKGUARD" />
				<Career name="WITCH_ELF" />
				<Career name="DISCIPLE" />
				<Career name="SORCERER" />
				<Career name="IRON_BREAKER" />
				<Career name="SLAYER" />
				<Career name="RUNE_PRIEST" />
				<Career name="ENGINEER" />
				<Career name="BLACK_ORC" />
				<Career name="CHOPPA" />
				<Career name="SHAMAN" />
				<Career name="SQUIG_HERDER" />
				<Career name="WITCH_HUNTER" />
				<Career name="KNIGHT" />
				<Career name="BRIGHT_WIZARD" />
				<Career name="WARRIOR_PRIEST" />
				<Career name="CHOSEN" />
				<Career name= "MARAUDER" />
				<Career name="ZEALOT" />
				<Career name="MAGUS" />
				<Career name="SWORDMASTER" />
				<Career name="SHADOW_WARRIOR" />
				<Career name="WHITE_LION" />
				<Career name="ARCHMAGE" />
			</Careers>
		</WARInfo>
		
			
	</UiMod>
</ModuleFile>    