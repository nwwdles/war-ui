--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

if Motion == nil then Motion = {} end 

local Motion 		= Motion
local LibGUI 		= LibStub( "LibGUI" )

local pairs 		= pairs
local ipairs		= ipairs
local tonumber 		= tonumber
local tostring 		= tostring
local towstring 	= towstring
local tinsert		= table.insert
local tsort			= table.sort
local tconcat		= table.concat
local tremove		= table.remove
local string_format = string.format

local initializeUI, retrieveItemList, getPlayerCraftType, updateContainerIcon, clearClientSlottedCraftingInformation
local resourceMenuSelect, retrieveTotalItemCount, performCraft, determineCraftSubKey, upgradeProfile
local createContextMenu_ItemWindow, createContextMenu_Recipeindow, fulfilledRequirements, processItemMove, orderingFunction 
local sortItems, sortRecipes, createContextMenu_RecipeWindow, continueCrafting, displayMenu_Item, displayMenu_Recipe
local initializeCraftInterfaces, updateCraftingSuccessDisplay, clearAllItemData, retrieveItemInventoryCount, updateUIPosition
local resetWindows

local debug = false
--[===[@debug@
debug = true
--@end-debug@]===]

local alpha = false
--[===[@alpha@
alpha = true
--@end-alpha@]===]

local T 				= LibStub( "WAR-AceLocale-3.0" ) : GetLocale( "Motion", debug )

local VERSION 			= { MAJOR = 1, MINOR = 2, REV = 6 }
local DisplayVersion 	= string.format( "%d.%d.%d", VERSION.MAJOR, VERSION.MINOR, VERSION.REV )

if( debug ) then
	DisplayVersion 		= DisplayVersion .. " Dev"
else
	DisplayVersion 		= DisplayVersion .. " r" .. ( tonumber( "56" ) or "0" )
	
	if( alpha ) then
		DisplayVersion = DisplayVersion .. "-alpha"
	end
end

do
	function Motion.wipe(t)
		for k,_ in pairs( t )
		do
			t[k] = nil
		end 	
	end

	local cache = setmetatable({}, {__mode='k'})
	
	--- Return a table
	-- @usage local t = Motion.new()
	-- @return a blank table
	function Motion.new()
		local t = next(cache)
		if t then
			cache[t] = nil
			return t
		end
		
		return {}
	end
	
	local wipe = Motion.wipe
	
	--- Delete a table, clearing it and putting it back into the queue
	-- @usage local t = Motion.new()
	-- t = del(t)
	-- @return nil
	function Motion.del(t)
		wipe(t)
		cache[t] = true
		return nil
	end
end

local new, del, wipe = Motion.new, Motion.del, Motion.wipe

local db 				= nil
Motion.db 				= nil
Motion.DefaultSettings 	=
{
	icon = 
	{
		point					= "center",
		relpoint				= "center",
		relwin					= "Root",
		x						= 0,
		y						= 0,
		scale					= .75,
		visible					= true,
	},

	anchor =
	{
		point					= "center",
		relpoint				= "center",
		relwin					= "Root",
		x						= 0,
		y						= 0,
		scale					= 1.0,
	},
	
	general =
	{
		debug					= false,
		version					= VERSION,
		show					= true,
		makes					= 1,
		enforceoptimalresources	= true,
		stopincombat			= true,
		stoponspellcast			= true,
		throttleitemqueue		= true,
	},
	
	recipes = {},
}
Motion.lastRecipe = nil

local windowId					= "MotionWindow"
local configWindowId			= "MotionConfig"
local iconWindowId				= "MotionIcon"
local twoLineToolTipId			= "Motion_Tooltip_TwoLine"
local baseContextMenu_ItemId	= "Motion_ContextMenu_Item"
local baseContextMenu_RecipeId	= "Motion_ContextMenu_Recipe"

local firstLoad					= true
local defaultSlice				= "White-Orb"

local goColor					= { 0, 200, 0 }
local stopColor					= { 200, 0, 0 }

local MAX_CRAFT_SLOTS			= 4   -- This is actually 5, however, this is 0 based

local MOUSEOVER_DELETE_COLOR	= { 0, 0, 255 }
local MOUSEOVEREND_DELETE_COLOR	= { 255, 255, 255 }

local craftingTypes =
{
	[GameData.TradeSkills.APOTHECARY] =
	{
		[1] = 		-- Potions
		{	
			containers 		= { GameData.CraftingItemType.CONTAINER },
			determinants 	= { GameData.CraftingItemType.MAIN_INGREDIENT },
			resources		= 
			{ 
				[1] = { GameData.CraftingItemType.STABILIZER, GameData.CraftingItemType.EXTENDER, GameData.CraftingItemType.MULTIPLIER, GameData.CraftingItemType.STIMULANT },
				[2] = { GameData.CraftingItemType.STABILIZER, GameData.CraftingItemType.EXTENDER, GameData.CraftingItemType.MULTIPLIER, GameData.CraftingItemType.STIMULANT },
				[3] = { GameData.CraftingItemType.STABILIZER, GameData.CraftingItemType.EXTENDER, GameData.CraftingItemType.MULTIPLIER, GameData.CraftingItemType.STIMULANT },
			},
			requirements	= 
			{
				[1] = { resource=GameData.CraftingItemType.CONTAINER, 			minCount = 1, optimalCount = 1, },
				[2] = { resource=GameData.CraftingItemType.MAIN_INGREDIENT, 	minCount = 1, optimalCount = 1, },
			},
			success =
			{
				[GameData.CraftingSuccessChance.HIGH] 		= { slice = "Green-Orb", },
				[GameData.CraftingSuccessChance.MEDIUM] 	= { slice = "Orange-Orb", },
				[GameData.CraftingSuccessChance.LOW] 		= { slice = "Red-Orb", },
				[GameData.CraftingSuccessChance.INVALID] 	= { slice = "Red-Orb", },	
			},
			cascaderesourceremoval = false,
		},
		[2] =		-- Dyes 
		{
			containers 		= { GameData.CraftingItemType.CONTAINER_DYE },
			determinants 	= { GameData.CraftingItemType.PIGMENT },
			resources		= 
			{ 
				[1] = { GameData.CraftingItemType.FIXER, }, 
			},
			requirements	= 
			{
				[1] = { resource=GameData.CraftingItemType.CONTAINER_DYE, 		minCount = 1, optimalCount = 1, },
				[2] = { resource=GameData.CraftingItemType.PIGMENT, 			minCount = 1, optimalCount = 1, },
				[3] = { resource=GameData.CraftingItemType.FIXER, 				minCount = 1, optimalCount = 1, },
			},
			success =
			{
				[GameData.CraftingSuccessChance.HIGH] 		= { slice = "Green-Orb", 	},
				[GameData.CraftingSuccessChance.MEDIUM] 	= { slice = "Orange-Orb", 	},
				[GameData.CraftingSuccessChance.LOW] 		= { slice = "Orange-Orb", 	},
				[GameData.CraftingSuccessChance.INVALID] 	= { slice = "Red-Orb", 		},	
			},
		},
	},
	[GameData.TradeSkills.TALISMAN] =
	{
		[1] =
		{	
			containers 		= { GameData.CraftingItemType.TALISMAN_CONTAINER },
			determinants 	= { GameData.CraftingItemType.FRAGMENT },
			resources		= 
			{ 
				[1]	= { GameData.CraftingItemType.GOLD_ESSENCE },
				[2]	= { GameData.CraftingItemType.CURIOS },
				[3]	= { GameData.CraftingItemType.MAGIC_ESSENCE },
			},
			requirements	=
			{
				[1] = { resource=GameData.CraftingItemType.TALISMAN_CONTAINER, 		minCount = 1, optimalCount = 1, },
				[2] = { resource=GameData.CraftingItemType.FRAGMENT, 				minCount = 1, optimalCount = 1, },
				[3] = { resource=GameData.CraftingItemType.GOLD_ESSENCE, 			minCount = 0, optimalCount = 1, },
				[4] = { resource=GameData.CraftingItemType.CURIOS, 					minCount = 0, optimalCount = 1, },
				[5] = { resource=GameData.CraftingItemType.MAGIC_ESSENCE, 			minCount = 0, optimalCount = 1, },
			},
			success =
			{
				[GameData.CraftingSuccessChance.MEDIUM] 	= { slice = "Green-Orb", 	},
				[GameData.CraftingSuccessChance.LOW] 		= { slice = "Orange-Orb", 	},
				[GameData.CraftingSuccessChance.INVALID] 	= { slice = "Red-Orb", 		},	
			},
		},
	},
}

--
-- As containers currently only apply to a single craft and craft sub type
-- this table provides a quick lookup to the crafting subtype
--
local containerLookup =
{
	[GameData.TradeSkills.APOTHECARY] =
	{
		[GameData.CraftingItemType.CONTAINER] 			= 1,
		[GameData.CraftingItemType.CONTAINER_DYE] 		= 2,

	},
	[GameData.TradeSkills.TALISMAN] =
	{
		[GameData.CraftingItemType.TALISMAN_CONTAINER] 	= 1,
	},
}

--
-- This is the big ole type to interface index table.  This table is used when the data
-- is received from the server, as it is assumed that the data is valid and minimal checking
-- is required. 
--
local typeToInterfaceIndex =
{
	--
	-- Apothecary - Potions
	--
	[GameData.CraftingItemType.CONTAINER]			= 0,
	[GameData.CraftingItemType.MAIN_INGREDIENT]		= 1,
	[GameData.CraftingItemType.STABILIZER]			= { 2, 3, 4, },
	[GameData.CraftingItemType.EXTENDER]			= { 2, 3, 4, },
	[GameData.CraftingItemType.MULTIPLIER]			= { 2, 3, 4, },
	[GameData.CraftingItemType.STIMULANT]			= { 2, 3, 4, },
	
	--
	-- Apothecary - Dyes
	--
	[GameData.CraftingItemType.CONTAINER_DYE]		= 0,
	[GameData.CraftingItemType.PIGMENT]				= 1,
	[GameData.CraftingItemType.FIXER]				= 2,
	
	--
	-- Talisman Making
	--
	[GameData.CraftingItemType.TALISMAN_CONTAINER]	= 0,
	[GameData.CraftingItemType.FRAGMENT]			= 1,
	[GameData.CraftingItemType.GOLD_ESSENCE]		= 2,
	[GameData.CraftingItemType.CURIOS]				= 3,
	[GameData.CraftingItemType.MAGIC_ESSENCE]		= 4,
}

local powerLookup =
{
	[GameData.TradeSkills.APOTHECARY] =
	{
		func = 
			function()
				if( GameData.CraftingStatus == GameData.CraftingSuccessChance.HIGH ) then
					return 100, 100
				elseif( GameData.CraftingStatus == GameData.CraftingSuccessChance.MEDIUM ) then
					return 50, 100
				elseif( GameData.CraftingStatus == GameData.CraftingSuccessChance.LOW ) then
					return 0, 100
				end
				
				return 0
			end
	},
	[GameData.TradeSkills.TALISMAN] =
	{
		func = 
			function()
				return GameData.CraftingStatus.OmeterValue, 49 
			end
	},
}

Motion.backpackTypes = 
{
	[1] = { loc = GameData.ItemLocs.INVENTORY, data=DataUtils.GetItems },
	[2] = { loc = GameData.ItemLocs.CRAFTING_ITEM, data=DataUtils.GetCraftingItems },
}

Motion.itemLocToBackpackType =
{
	[GameData.ItemLocs.INVENTORY] 		= EA_Window_Backpack.TYPE_INVENTORY,
	[GameData.ItemLocs.CRAFTING_ITEM]	= EA_Window_Backpack.TYPE_CRAFTING,
}

local sortOrder = 
{
    INCREASING  = 1,
    DECREASING 	= 2,
}

local itemSortKeys =
{
    ["craftingSkillRequirement"] 	= { fallback = "rarity", sortOrder = sortOrder.INCREASING },
    ["rarity"] 						= { fallback = "name", sortOrder = sortOrder.INCREASING },
    ["name"]                    	= { sortOrder = sortOrder.INCREASING }
}

local recipeSortKeys =
{
	["name"]                    	= { sortOrder = sortOrder.INCREASING }
}

local validOrderingValueTypes =
{
    ["number"]  = true, 
    ["string"]  = true, 
    ["wstring"] = true, 
    ["boolean"] = true,
}

------------------------------------------------
-- Runtime Variables
------------------------------------------------

local window	= {
	W			= new(),
	I			= new(),
}
local displayWindows 				= new()
local resourceWindows				= new()

-- This stores the information required when the user right clicks one of the item boxes
-- and subsequent selection handling.
local craftContext = 
{
	data 		= new(),
	interface	= nil,
}

local allCraftInterfaces = new()
local craftInterfaces = 
{
	container 	= nil,
	determinant = nil,
	resources = { [1] = nil, [2] = nil, [3] = nil, }, 
}

local playerCraftType				= nil
local deleteRecipeIndex				= nil
local queuedCraftingItemAdditions	= new()
local makeCount						= 0
local isRunning						= false
local playerInventoryUpdateReceived	= false
local inventoryUpdateThrottle		= 0

function Motion.GetVersion() return DisplayVersion end
function Motion.GetPlayerCraftType() return playerCraftType end

function Motion.OnInitialize()
	RegisterEventHandler( SystemData.Events.RELOAD_INTERFACE, "Motion.OnLoad" )
	RegisterEventHandler( SystemData.Events.LOADING_END, "Motion.OnLoad" )
end

function Motion.OnLoad()
	Motion.Debug( "Motion.OnLoad" )
	--
	-- Update the players craft type on zone/load 
	--
	playerCraftType = getPlayerCraftType()
	
	if( playerCraftType == nil ) then return end
	
	if( firstLoad ) then
		-- Create our configuration objects
		Motion.InitProfiles()
		
		local profileIdx, _ = Motion.GetCurrentProfile()
		
		-- Update our local db with configuration information
		Motion.db = Motion.GetProfileDataForUse( profileIdx )
		db = Motion.db
		
		-- Upgrade our profile
		upgradeProfile()
		
		-- Update our UIs position
		updateUIPosition()

		-- Register our slash commands with LibSlash
		Motion.Debug( "Registering slash commands" )
		
		-- Initialize our UI
		initializeUI()
		
		-- Initialize our craft interfaces (These require the UI, so this must be after initializeUI )
		initializeCraftInterfaces()
		
		-- Load our configuration window
		MotionConfig.OnLoad()
		
		-- Catch the layout editor events
		LayoutEditor.RegisterEditCallback( Motion.OnLayoutEditorEvent )
		
		-- Register the UI with the layout editor
		LayoutEditor.RegisterWindow( windowId, L"Motion", L"Motion", false, false, false, nil )
		LayoutEditor.RegisterWindow( iconWindowId, T["Motion Toggle"], T["Motion Toggle"], false, false, true, nil )
		
		-- Print our initialization usage
		local init = WStringToString( T["Motion %s initialized."] )
		Motion.Print( init:format( DisplayVersion ) )
		
		-- Attempt to register one of our handlers
		if( LibSlash ~= nil and type( LibSlash.RegisterSlashCmd ) == "function" ) then
			LibSlash.RegisterSlashCmd( "motion", Motion.Slash )
			Motion.Print( T["(Motion) use /motion to bring up configuration window."] )
		end
		
		RegisterEventHandler( SystemData.Events.PLAYER_CRAFTING_UPDATED, 		"Motion.OnPlayerCraftingUpdated" )
		RegisterEventHandler( SystemData.Events.PLAYER_INVENTORY_SLOT_UPDATED, 	"Motion.OnPlayerInventorySlotUpdated" )
		RegisterEventHandler( SystemData.Events.PLAYER_CRAFTING_SLOT_UPDATED, 	"Motion.OnPlayerCraftingSlotUpdated" )
		RegisterEventHandler( SystemData.Events.CRAFTING_SHOW_WINDOW,			"Motion.OnCraftingShowWindow" )
		RegisterEventHandler( SystemData.Events.PLAYER_COMBAT_FLAG_UPDATED,		"Motion.OnPlayerCombatFlagUpdated" )
		RegisterEventHandler( SystemData.Events.PLAYER_BEGIN_CAST, 				"Motion.OnPlayerBeginCast" )
		
		Motion.OnPlayerCraftingUpdated( new() )
		
		d( "Motion loaded." )
		
		firstLoad = false
	end
	
	--
	-- If the user zoned in the middle of crafting, stop crafting
	--
	if( isRunning ) then
		Motion.StopCrafting()
	end
	
	-- Clear our crafting data
	clearClientSlottedCraftingInformation()
end

function Motion.AddRecipe( name, containerData, determinantData, resourcesData, craftType )
	-- Make sure the minimal amount of info
	if( containerData == nil or determinantData == nil or resourcesData == nil or name:len() == 0 ) then return false end 

	local recipe = new()
	recipe.name 		= name
	recipe.type			= craftType
	recipe.container 	= DataUtils.CopyTable( containerData )
	recipe.determinant	= DataUtils.CopyTable( determinantData )
	recipe.resources	= DataUtils.CopyTable( resourcesData )
	
	tinsert( db.recipes, recipe )
	
	-- Sort the recipes
	tsort( db.recipes, sortRecipes )
	
	return true
end

function Motion.RememberRecipe( )
	local resourceData = new()
	for k, v in ipairs( craftInterfaces.resources )
	do
		resourceData[k] = v:GetItemData()
	end

	local containerData = craftInterfaces.container:GetItemData()
	local determinantData = craftInterfaces.determinant:GetItemData()
	local resourcesData = resourceData
	local craftType =playerCraftType 

	-- Make sure the minimal amount of info
	if( containerData == nil or determinantData == nil or resourcesData == nil) then return false end 

	local recipe = new()
	recipe.type			= craftType
	recipe.container 	= DataUtils.CopyTable( containerData )
	recipe.determinant	= DataUtils.CopyTable( determinantData )
	recipe.resources	= DataUtils.CopyTable( resourcesData )
	
	Motion.lastRecipe = recipe

	return true
end

function Motion.Debug(str)	
	if( db ~= nil and db.general.debug ) then 
		DEBUG(towstring(str)) 
	end 
end

function Motion.DeleteRecipe( id )
	if( db.recipes[id] ~= nil ) then
		del(db.recipes[id])
		tremove( db.recipes, id )
	end
end

function Motion.DisplayTwoLineTooltip( window, text1, text2 )
	LabelSetText( twoLineToolTipId .. "Line1", text1 )
	LabelSetText( twoLineToolTipId .. "Line2", text2 )
	Tooltips.CreateCustomTooltip( window, twoLineToolTipId )
    Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_VARIABLE )
end

function Motion.LoadRecipe( recipe )
	Motion.Debug( "Entering Motion.LoadRecipe" )

	if( isRunning ) then return end
	
	--
	-- Clear our existing display
	--
	clearAllItemData()
	
	--
	-- Sanity check after clearing data
	--
	if( recipe.container == nil or recipe.determinant == nil or recipe.resources == nil ) then return end
	
	craftInterfaces.container:SetItemData( recipe.container )
	craftInterfaces.determinant:SetItemData( recipe.determinant )
	
	for k, v in pairs( recipe.resources )
	do
		craftInterfaces.resources[k]:SetItemData( v )
	end
end

function Motion.OnCraftingShowWindow( tradeSkill )
	if( playerCraftType ~= tradeSkill ) then return end
	Motion.Debug( "Motion.OnCraftingShowWindow" )
	
	--
	-- When the user displays the crafting window, it clears all currently slotted crafting materials.
	-- As such, if we are running we will immediately stop crafting.
	--
	if( isRunning ) then
		Motion.StopCrafting()
	end 
end

function Motion.OnHidden()
	if( firstLoad ) then return end
	db.general.show = false
end

function Motion.OnLayoutEditorEvent( code )
	if( code == LayoutEditor.EDITING_END ) then
		-- Save our UI location
		db.anchor.point, db.anchor.relpoint, db.anchor.relwin, db.anchor.x, db.anchor.y = WindowGetAnchor( windowId, 1 )
		db.anchor.scale = WindowGetScale( windowId )
		
		-- Save our icon location
		db.icon.point, db.icon.relpoint, db.icon.relwin, db.icon.x, db.icon.y = WindowGetAnchor( iconWindowId, 1 )
		db.icon.scale = WindowGetScale( iconWindowId )
		db.icon.visible = WindowGetShowing( iconWindowId )
	end
end

function Motion.OnLButtonUp_ContextMenu_Item()
	Motion.Debug( "Entering Motion.OnLButtonUp_ContextMenu_Item" )
	local id = WindowGetId( SystemData.MouseOverWindow.name )
	if( craftContext.data[id] ~= nil ) then
		craftContext.interface:SetItemData( craftContext.data[id] )
	end
	
	EA_Window_ContextMenu.HideAll()
end

function Motion.OnLButtonUp_ContextMenu_Recipe()
	Motion.Debug( "Entering Motion.OnLButtonUp_ContextMenu_Recipe" )
	local id = WindowGetId( SystemData.MouseOverWindow.name )
	if( db.recipes[id] ~= nil ) then
		Motion.LoadRecipe( db.recipes[id] )
	end
	
	EA_Window_ContextMenu.HideAll()
end

function Motion.OnLButtonUp_ContextMenu_Recipe_Delete()
	Motion.Debug( "Entering Motion.OnLButtonUp_ContextMenu_Recipe_Delete" )
	
	local id = WindowGetId( WindowGetParent( SystemData.MouseOverWindow.name ) )
	
	if( db.recipes[id] ~= nil ) then
		-- Store the index for the callback to use as needed
		deleteRecipeIndex = id
	
		DialogManager.MakeTwoButtonDialog( T["Do you wish to delete the following recipe?"] .. L"  " .. db.recipes[id].name, 
			T["Yes"], Motion.OnRecipeDeleteSuccess, 
			T["No"], Motion.OnRecipeDeleteCancel )
	end
end

function Motion.OnMouseOver_ContextMenu_Item()
	local id = WindowGetId( SystemData.MouseOverWindow.name )
	
	if( craftContext.data[id] ~= nil ) then
		Tooltips.CreateItemTooltip( craftContext.data[id], 
                    SystemData.MouseOverWindow.name,
                    Tooltips.ANCHOR_WINDOW_VARIABLE, 
                    true, 
                    nil, nil, nil )
	end
end

function Motion.OnMouseOver_ContextMenu_Recipe()
	local id		= WindowGetId( WindowGetParent( SystemData.MouseOverWindow.name ) )
	local itemId 	= WindowGetId( SystemData.MouseOverWindow.name )	
	
	if( id ~= nil and itemId ~= nil and db.recipes[id] ~= nil ) then
		local item
		
		if( itemId == 1 ) then 
			item = db.recipes[id].container 
		elseif( itemId == 2 ) then 
			item = db.recipes[id].determinant 
		elseif( itemId >= 3 and itemId <= 5 ) then 
			item = db.recipes[id].resources[itemId-2] 
		end
	
		if( item ~= nil ) then
			Tooltips.CreateItemTooltip( item, 
	                    SystemData.MouseOverWindow.name,
	                    Tooltips.ANCHOR_WINDOW_VARIABLE, 
	                    true, 
	                    nil, nil, nil )
	    end
	end
end

function Motion.OnMouseOver_ContextMenu_Recipe_Delete()
	LabelSetTextColor( SystemData.ActiveWindow.name, unpack( MOUSEOVER_DELETE_COLOR ) )
end

function Motion.OnMouseOverEnd_ContextMenu_Recipe_Delete()
	LabelSetTextColor( SystemData.ActiveWindow.name, unpack( MOUSEOVEREND_DELETE_COLOR ) )
end

function Motion.OnMouseOver_Window_Hint()
	local power = powerLookup[playerCraftType]
	if( power ~= nil ) then
		if( power.func ~= nil and type( power.func ) == "function" ) then
			local current, max = power.func()
		
			if( current ~= nil and max ~= nil ) then
				local display = towstring( current ) .. L" / " .. towstring( max )
				Tooltips.CreateTextOnlyTooltip( SystemData.MouseOverWindow.name, display )
				Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_VARIABLE )
			end	
		end     			
	end
end

function Motion.OnMouseOver_Icon()
	Tooltips.CreateTextOnlyTooltip( SystemData.MouseOverWindow.name, T["Show/Hide Motion"] )
    Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_VARIABLE )
end

function Motion.OnPlayerBeginCast( abilityId, isChannel, desiredCastTime, averageLatency )
	if( isRunning and abilityId ~= 0 and db.general.stoponspellcast ) then
		Motion.StopCrafting()
	end
end

function Motion.OnPlayerCombatFlagUpdated()
	if( isRunning and db.general.stopincombat and GameData.Player.inCombat ) then
		Motion.Debug( "Motion.OnPlayerCombatFlagUpdated" )
		Motion.StopCrafting()
	end
end

function Motion.OnPlayerCraftingSlotUpdated( updatedSlots )
 	playerInventoryUpdateReceived = true
end

function Motion.OnPlayerCraftingUpdated( updatedIngredients )
	Motion.Debug( "Motion.OnPlayerCraftingUpdated:  isRunning: " .. tostring( isRunning ) .. "   State: " .. tostring( GameData.CraftingStatus.State  ))
	
	-- If the player is not an apothecary or a talisman maker 
	if( playerCraftType == nil ) then return end

	-- If we are not running, process our enable/disabling of buttons, icon display, count, etc
	if( not isRunning ) then
		
		local index
		local updatedInterfaceIndices = new()
		local resourceType
		local itemData 
		
		if( playerCraftType == nil ) then return end
		
		--
		-- Get the current backpack slots for the slotted crafting items
		--
		local backpackSlots = GetCraftingBackPackSlots( playerCraftType )
		
		-- Check if the player has any backpack slots
		if( backpackSlots == nil ) then return end
		
		--
		-- Retrieve the item and resource type for each slot
		--
		for idx, slot in pairs( backpackSlots )
		do
			itemData = EA_Window_Backpack.GetItemsFromBackpack( slot.backpack )[slot.slot]
			
			if( itemData ~= nil ) then
				_, resourceType, _ = CraftingSystem.GetCraftingData( itemData )
				
				index = typeToInterfaceIndex[resourceType]
				
				if( index ~= nil ) then
					-- If index is a table, there are multiple possibilities, so look
					-- for a free index
					if( type(index) == "table" ) then
						for k, v in ipairs( index )
						do
							if( updatedInterfaceIndices[v] == nil ) then
								index = v
								break
							end
						end
					end
					
					--
					-- Sanity check to make sure this index is not already used
					--
					if( updatedInterfaceIndices[index] == nil ) then
						allCraftInterfaces[index]:UpdateItem( itemData, slot.slot, slot.backpack )
						updatedInterfaceIndices[index] = true
					end
				end
			end
		end
		
		--
		-- Clear the indices we did not use
		--
		for idx = 0, MAX_CRAFT_SLOTS
		do
			if( updatedInterfaceIndices[idx] == nil ) then
				allCraftInterfaces[idx]:UpdateItem( nil, nil, nil )
			end
		end
		
		--
		-- Update our crafting success display
		--
		updateCraftingSuccessDisplay()

		--
		-- Show/Hide the appropriate buttons
		--
		local hiddenWindows = new()
		if( GameData.CraftingStatus.State == GameData.CraftingStates.ADDCONTAINER ) then
			-- Enable the container window
			window.W.General.Container_Button:Show()
			tinsert( hiddenWindows, window.W.General.Determinant_Button )
			tinsert( hiddenWindows, window.W.General.Resource1_Button )
			tinsert( hiddenWindows, window.W.General.Resource2_Button )
			tinsert( hiddenWindows, window.W.General.Resource3_Button )
		elseif( GameData.CraftingStatus.State == GameData.CraftingStates.ADDDETERMINENT ) then
			-- Enable the determinant window
			window.W.General.Container_Button:Show()
			window.W.General.Determinant_Button:Show()
			tinsert( hiddenWindows, window.W.General.Resource1_Button )
			tinsert( hiddenWindows, window.W.General.Resource2_Button )
			tinsert( hiddenWindows, window.W.General.Resource3_Button )
		elseif( GameData.CraftingStatus.State == GameData.CraftingStates.ADDINGREDIENT or
				GameData.CraftingStatus.State == GameData.CraftingStates.VALID_RECIPE ) then
				
			window.W.General.Container_Button:Show()
			window.W.General.Determinant_Button:Show()
			
			-- Enable the appropriate number of resource windows
			local craftSubKey = determineCraftSubKey()
			if( craftSubKey ~= nil ) then 
				local craftType 	= craftingTypes[playerCraftType]
				local data 			= craftType[craftSubKey]
				if( data ~= nil ) then 
					if( #data.resources > 0 ) then
						for idx, window in ipairs( resourceWindows )
						do
							if( idx <= #data.resources ) then
								window:Show()
							else
								tinsert( hiddenWindows, window )
							end
						end
					else
						d( "Motion.OnPlayerCraftingUpdated: #data.resources == 0" )
					end
				else
					d( "Motion.OnPlayerCraftingUpdated: data == nil" )
				end
			else
				d( "Motion.OnPlayerCraftingUpdated: craftSubKey == nil" )
			end
		end
		
		for _, window in ipairs( hiddenWindows )
		do
			window:Hide()
		end
		
		del(updatedInterfaceIndices)
		del(hiddenWindows)
	else
		--
		-- We are currently running here
		--
		if( GameData.CraftingStatus.State == GameData.CraftingStates.ADDDETERMINENT or 
			GameData.CraftingStatus.State == GameData.CraftingStates.ADDINGREDIENT ) then
			local idx, item = next( queuedCraftingItemAdditions )
			if( item ~= nil ) then
				AddCraftingItem( playerCraftType, idx ,item.slot, item.type )
				queuedCraftingItemAdditions[idx] = nil
			else
				-- We are still being prompted for ingredients however we do not have any
				-- left to add, so stop the crafting process
				Motion.StopCrafting()	
			end
		elseif( GameData.CraftingStatus.State == GameData.CraftingStates.VALID_RECIPE ) then
			local idx, item = next( queuedCraftingItemAdditions )
			if( idx ~= nil and item ~= nil ) then
				AddCraftingItem( playerCraftType, idx ,item.slot, item.type )
				queuedCraftingItemAdditions[idx] = nil

			else
				-- There are no queued additions left, so perform the craft
				PerformCrafting( playerCraftType, 1 )
			end	
		elseif( GameData.CraftingStatus.State == GameData.CraftingStates.SUCCESS or 
				GameData.CraftingStatus.State == GameData.CraftingStates.SUCCESS_REPEAT ) then
			continueCrafting()
		elseif( GameData.CraftingStatus.State == GameData.CraftingStates.FAIL ) then
			Motion.StopCrafting()
		end 
	end
end

function Motion.OnPlayerInventorySlotUpdated( updatedSlots )
	playerInventoryUpdateReceived = true
end

function Motion.OnUpdate( elapsedTime )
	inventoryUpdateThrottle = inventoryUpdateThrottle + elapsedTime
	if( inventoryUpdateThrottle > 2 ) then
		if( playerInventoryUpdateReceived ) then
			processItemMove( new(), GameData.ItemLocs.INVENTORY )
			playerInventoryUpdateReceived = false
		end
		inventoryUpdateThrottle = 0
	end
end

function Motion.Print( wstr )
	EA_ChatWindow.Print( towstring( wstr ), ChatSettings.Channels[SystemData.ChatLogFilters.SAY].id )
end

function Motion.OnRecipeAddCancel() 
	-- noop 
end

function Motion.OnRecipeAddSuccess( name )
	local resourceData = new()
	for k, v in ipairs( craftInterfaces.resources )
	do
		resourceData[k] = v:GetItemData()
	end
	Motion.AddRecipe( name, 
			craftInterfaces.container:GetItemData(), 
			craftInterfaces.determinant:GetItemData(), 
			resourceData, playerCraftType )
	del(resourceData)
end

function Motion.OnRecipeDeleteCancel()
	deleteRecipeIndex = nil
end

function Motion.OnRecipeDeleteSuccess()
	if( deleteRecipeIndex ~= nil ) then
		Motion.DeleteRecipe( deleteRecipeIndex )
		deleteRecipeIndex = nil
	end
end

function Motion.OnShown()
	if( firstLoad ) then return end
	db.general.show = true
end

function Motion.OnProfileChanged()
	local profileIdx, currentProfileName = Motion.GetCurrentProfile()
	
	-- Update our local db with configuration information
	Motion.db = Motion.GetProfileDataForUse( profileIdx )
	db = Motion.db
	
	-- Upgrade our profile
	upgradeProfile()
	
	-- Update our UIs position
	updateUIPosition()
	
	Motion.Print( L"(Motion) " .. T["Active Profile:"] .. L" '" .. currentProfileName .. L"'" )
end

function Motion.RetrieveFirstAvailableItemSlot( itemId, allocatedSlots )
	--
	-- Build a crafting allocation map
	--
	for _, set in ipairs( Motion.backpackTypes )
	do
		for slot, item in ipairs( set.data() )
		do
			if( itemId == item.uniqueID ) then
				local count 		= item.stackCount
				local backpackType 	= Motion.itemLocToBackpackType[set.loc]
			
				-- Reduce the stackCount by the currently allocated amount(if available)
				if( allocatedSlots[backpackType] ~= nil and allocatedSlots[backpackType][slot] ~= nil ) then
					count = count - allocatedSlots[backpackType][slot]
				end
				
				-- If there is any stackcount remaining add it to our list
				if( count > 0 ) then
					return slot, backpackType
				end
			end
		end
	end

	return nil, nil
end

function Motion.Slash( input )
	if( not isRunning ) then Motion.Toggle() end
end

--
function Motion.StartCrafting()
	Motion.Debug( "Entering Motion.StartCrafting" )
	--[[ 

	-- Retrieve the make count	
	makeCount = tonumber( window.W.General.MakeCount_Textbox:GetText() ) or 0
	
	-- Make sure we have some work to do
	if( makeCount == 0 ) then return end

	isRunning = true
	
	-- Start doing our work, if we suceed flag we are running and return
	if( performCraft() ) then
		-- Identify this button now as the stop button
		window.W.General.Make_Button:Tint( unpack( stopColor ) )
		return 
	end
	
	isRunning = false
	
	-- If we made it here, stop crafting
	Motion.StopCrafting()
	-]]
	
	PerformCrafting( playerCraftType, 1 )
	Motion.RememberRecipe( )
end

function Motion.StopCrafting()
	Motion.Debug( "Entering Motion.StopCrafting" )
	
	-- Identify this button now as the go button
	window.W.General.Make_Button:Tint( unpack( goColor ) )
	
	-- Release our locks
	EA_BackpackUtilsMediator.ReleaseAllLocksForWindow( windowId )
	
	-- Set the make count to 1
	makeCount = db.general.makes
	
	-- Update our make count display
	window.W.General.MakeCount_Textbox:SetText( towstring( makeCount ) )
	
	isRunning = false
end

function Motion.Toggle()
	local show = not WindowGetShowing( windowId )
	WindowSetShowing( windowId, show ) 
end

function Motion.ToggleConfig()
	if( MotionConfig ~= nil and DoesWindowExist( MotionConfig.GetWindowName() ) ) then
		WindowSetShowing( MotionConfig.GetWindowName(), not WindowGetShowing( MotionConfig.GetWindowName() ) )
	end
end

------------------------------------------------------------
-- LOCAL FUNCTIONS
------------------------------------------------------------
function continueCrafting()
	Motion.Debug( "Entering continueCrafting" )
	
	-- Release our locks
	EA_BackpackUtilsMediator.ReleaseAllLocksForWindow( windowId )
	
	-- Reduce the make count	
	makeCount = makeCount - 1
	
	-- Update our count display
	window.W.General.MakeCount_Textbox:SetText( towstring( makeCount ) )
	
	-- If we have more work to do attempt to do it	
	if( makeCount > 0 ) then
		if( performCraft() ) then
			return 
		end
	end
	
	-- If we made it here, stop crafting
	Motion.StopCrafting()
end

function clearAllItemData()
	Motion.Debug( "clearAllItemData" )
	for _, interface in pairs( allCraftInterfaces )
	do
		interface:SetItemData( nil )
	end
	
	resetWindows()
end

function clearClientSlottedCraftingInformation()
    if( playerCraftType == nil ) then return end

	local craftingSlots = GetCraftingBackPackSlots( playerCraftType )
	if( craftingSlots ~= nil and craftingSlots[0] ~= nil ) then
		RemoveCraftingItem( playerCraftType, craftingSlots[0].slot, craftingSlots[0].backpack )
	end
	
	resetWindows()
end

function createContextMenu_ItemWindow( index, itemData )
	local windowId = baseContextMenu_ItemId .. index
	
	-- Check if we need to create the window, or reuse the old
	if( not DoesWindowExist( windowId ) ) then
		CreateWindowFromTemplate( windowId, baseContextMenu_ItemId, "Root" )
	end
	
	-- Populate the items
	LabelSetText( windowId .. "Count", 	towstring( itemData.stackCount ) .. L"x" )
	LabelSetText( windowId .. "Level", 	towstring( itemData.craftingSkillRequirement ) )
	LabelSetText( windowId .. "Name", 	towstring( itemData.name ) )
	
	-- Apply the appropriate colors
	LabelSetTextColor( windowId .. "Level", 	GameDefs.ItemRarity[itemData.rarity].color.r, GameDefs.ItemRarity[itemData.rarity].color.g, GameDefs.ItemRarity[itemData.rarity].color.b )	
	LabelSetTextColor( windowId .. "Name", 		GameDefs.ItemRarity[itemData.rarity].color.r, GameDefs.ItemRarity[itemData.rarity].color.g, GameDefs.ItemRarity[itemData.rarity].color.b )
  
	return windowId
end

function createContextMenu_RecipeWindow( index, recipe )
	local windowId = baseContextMenu_RecipeId .. index
	local texture, dx, dy = "", 0, 0
	
	-- Check if we need to create the window, or reuse the old
	if( not DoesWindowExist( windowId ) ) then
		CreateWindowFromTemplate( windowId, baseContextMenu_RecipeId, "Root" )
	end
	
	-- Populate the items
	LabelSetText( windowId .. "Name", 	towstring( recipe.name ) )
	LabelSetText( windowId .. "Delete", T["DEL"] )
	LabelSetTextColor( windowId .. "Delete", unpack( MOUSEOVEREND_DELETE_COLOR ) )
	
	texture, dx, dy = "", 0, 0
	if( recipe.container ~= nil ) then
		texture, textureDx, textureDy = GetIconData( recipe.container.iconNum )
	end
	DynamicImageSetTexture( windowId .. "Container", texture, dx, dy )
	WindowSetShowing( windowId .. "Container", recipe.container ~= nil )
	
	texture, dx, dy = "", 0, 0
	if( recipe.determinant ~= nil ) then
		texture, textureDx, textureDy = GetIconData( recipe.determinant.iconNum )
	end
	DynamicImageSetTexture( windowId .. "Determinant", texture, dx, dy )
	WindowSetShowing( windowId .. "Determinant", recipe.determinant ~= nil )
	
	texture, dx, dy = "", 0, 0
	if( recipe.resources ~= nil and recipe.resources[1] ~= nil ) then
		texture, textureDx, textureDy = GetIconData( recipe.resources[1].iconNum )
	end
	DynamicImageSetTexture( windowId .. "Resource1", texture, dx, dy )
	WindowSetShowing( windowId .. "Resource1", recipe.resources ~= nil and recipe.resources[1] ~= nil )
	
	texture, dx, dy = "", 0, 0
	if( recipe.resources ~= nil and recipe.resources[2] ~= nil ) then
		texture, textureDx, textureDy = GetIconData( recipe.resources[2].iconNum )
	end
	DynamicImageSetTexture( windowId .. "Resource2", texture, dx, dy )
	WindowSetShowing( windowId .. "Resource2", recipe.resources ~= nil and recipe.resources[2] ~= nil )
	
	texture, dx, dy = "", 0, 0
	if( recipe.resources ~= nil and recipe.resources[3] ~= nil ) then
		texture, textureDx, textureDy = GetIconData( recipe.resources[3].iconNum )
	end
	DynamicImageSetTexture( windowId .. "Resource3", texture, dx, dy )
	WindowSetShowing( windowId .. "Resource3", recipe.resources ~= nil and recipe.resources[3] ~= nil )
	
	return windowId
end

function determineCraftSubKey()
	--
	-- As the container type determines the difference for apothecary at this point, it will
	-- be used to determine the craft sub key.  This function assumes that all data
	-- retrieved is valid and no checking is required.
	--
	local itemData = craftInterfaces.container:GetItemData()
	
	if( itemData ~= nil ) then
		-- Retrieve the information for the container object
		local _, itemResourceType, _ =  CraftingSystem.GetCraftingData( itemData )
		
		-- Get the resource type of the item
		return containerLookup[playerCraftType][itemResourceType]
	end
	
	return nil
end

function displayMenu_Item( menuType, interface, resourceId, subKeyData )
	local craftType = craftingTypes[playerCraftType]
	
	if( craftType == nil ) then return end
	
	local types = new()
	
	-- If we are the container menu, we iterate all of our craft types
	-- if not we use the sub type and use those items
	if( subKeyData == false ) then
		for idx, type in pairs( craftType )
		do
			local baseData
			
			if( resourceId == nil ) then
				baseData = type[menuType]
			else
				baseData = type[menuType][resourceId]
			end
			
			if( baseData ~= nil ) then
				for _, iType in pairs( baseData )
				do
					types[iType] = true
				end
			end
		end
	else
		local craftSubKey = determineCraftSubKey()
		if( craftSubKey == nil ) then return false end
		local craftType 	= craftingTypes[playerCraftType]
		local type 			= craftType[craftSubKey]
		if( type == nil ) then return false end
		
		local baseData
			
		if( resourceId == nil ) then
			baseData = type[menuType]
		else
			baseData = type[menuType][resourceId]
		end
		
		if( baseData ~= nil ) then
			for _, iType in pairs( baseData )
			do
				types[iType] = true
			end
		end
	end
	
	-- Store our context information
	craftContext.interface = interface
	-- Retrieve a list of viable containers from our inventory
	craftContext.data = retrieveItemList( playerCraftType, types, math.max( GameData.CraftingStatus.SkillLevel, GameData.TradeSkillLevels[playerCraftType] ) )
		
	-- Sort our data
	tsort( craftContext.data, sortItems )
	
	-- Display the context menu
	EA_Window_ContextMenu.CreateContextMenu()

	if( #craftContext.data > 0 ) then
		for idx, data in ipairs( craftContext.data )
		do
			EA_Window_ContextMenu.AddUserDefinedMenuItem( createContextMenu_ItemWindow( idx, data ) )				
		end 	
	else
		EA_Window_ContextMenu.AddMenuItem( T["No items found."], function() end, false, true )		
	end
	
	del(types)
	
	EA_Window_ContextMenu.Finalize()
end

function displayMenu_Recipe()
	-- Display the context menu
	EA_Window_ContextMenu.CreateContextMenu()

	if( #db.recipes > 0 ) then
		for idx, data in ipairs( db.recipes )
		do
			EA_Window_ContextMenu.AddUserDefinedMenuItem( createContextMenu_RecipeWindow( idx, data ) )				
		end 	
	else
		EA_Window_ContextMenu.AddMenuItem( T["No recipes found."], function() end, false, true )		
	end
	
	EA_Window_ContextMenu.Finalize()
end

function getPlayerCraftType()
	for skillId, _ in pairs( craftingTypes )
	do
		if( GameData.TradeSkillLevels[skillId] ~= nil and GameData.TradeSkillLevels[skillId] > 0 ) then
			return skillId
		end
	end 
	return nil
end

--[[
	-- Just found this function here, not sure what it was used for at some point
	-- but has no local definition
function getSlotLockCount( slotNum, backpackType )
	local count = 0
	local backpack = EA_BackpackUtilsMediator.GetBackpack()
	
	if( EA_Window_Backpack.lockedSlot[backpackType] and  EA_Window_Backpack.lockedSlot[backpackType][slotNum] ) then
		count = #EA_Window_Backpack.lockedSlot[backpackType][slotNum]
	end
	return 0
end
--]]

function initializeCraftInterfaces()
	-- Create a craft interface for our container
	craftInterfaces.container = Motion.CraftInterface:Create( Motion.CraftInterface.TYPE_CONTAINER, window.W.General.Container_Button, 0 )
	window.W.General.Container_Button.SetInterface( craftInterfaces.container )
	tinsert( allCraftInterfaces, 0, craftInterfaces.container )
	
	craftInterfaces.determinant = Motion.CraftInterface:Create( Motion.CraftInterface.TYPE_DETERMINANT, window.W.General.Determinant_Button, 1 )
	window.W.General.Determinant_Button.SetInterface( craftInterfaces.determinant )
	tinsert( allCraftInterfaces, 1, craftInterfaces.determinant )
	
	craftInterfaces.resources[1] = Motion.CraftInterface:Create( Motion.CraftInterface.TYPE_RESOURCE, window.W.General.Resource1_Button, 2 )
	window.W.General.Resource1_Button.SetInterface( craftInterfaces.resources[1] )
	tinsert( allCraftInterfaces, 2, craftInterfaces.resources[1] )
	
	craftInterfaces.resources[2] = Motion.CraftInterface:Create( Motion.CraftInterface.TYPE_RESOURCE, window.W.General.Resource2_Button, 3 )
	window.W.General.Resource2_Button.SetInterface( craftInterfaces.resources[2] )
	tinsert( allCraftInterfaces, 3, craftInterfaces.resources[2] )
	
	craftInterfaces.resources[3] = Motion.CraftInterface:Create( Motion.CraftInterface.TYPE_RESOURCE, window.W.General.Resource3_Button, 4 )
	window.W.General.Resource3_Button.SetInterface( craftInterfaces.resources[3] )
	tinsert( allCraftInterfaces, 4, craftInterfaces.resources[3] )
end

function initializeUI()
	local w
	local e
	
	------------------------------
	-- ICON
	------------------------------
	
	-- General Window
	w = LibGUI( "window", iconWindowId, "MotionIconBase" )
	w:ClearAnchors()
	w:Resize( 38, 38 )
	w:Show()
	w.OnMouseOver = Motion.OnMouseOver_Icon
	window.I.General = w
	
	--[[
	e = window.I.General( "Image", iconWindowId .. "LogoImage" )
	e:Resize( 32, 32 )
    e:AnchorTo( window.I.General, "center", "center", 0, 0 )
    e:Layer( "secondary" )
    e:IgnoreInput()
    --e:Texture( "MotionLogo", 48, 48 )
    window.I.General.Logo_Image = e
    --]]
	
	------------------------------
	-- MAIN UI
	------------------------------
	
	-- General Window
	w = LibGUI( "window", windowId, "MotionConfigBase" )
	w:Resize( 232, 80 )
	if( db.general.show ) then
		w:Show()
	else
		w:Hide()
	end
	window.W.General = w
	
	-- General - Container Button
    e = window.W.General( "Button", windowId .. "ContainterButton", "Motion_Button_DefaultIconFrame" )
    e:Resize( 32, 32 )
    e:AnchorTo( window.W.General, "topleft", "topleft", 6, 5 )
    e.OnRButtonUp =
		function()
			if( isRunning ) then return end
			displayMenu_Item( "containers", window.W.General.Container_Button.interface, nil, false )
		end
	tinsert( displayWindows, e )
    window.W.General.Container_Button = e
    
    -- General - Container Overlay Button
    e = window.W.General( "Button", windowId .. "ContainterButtonOverlay", "Motion_Button_DefaultIconFrame_Overlay" )
    e:Resize( WindowGetDimensions( window.W.General.Container_Button.name ) )
    e:AnchorTo( window.W.General.Container_Button, "center", "center", 0, 0 )
    e:Layer( "popup" )
    e:Tint( 222, 192, 50 )
    window.W.General.Container_Button.overlay = e
    
    -- General - Determinant Button
    e = window.W.General( "Button", windowId .. "DeterminantButton", "Motion_Button_DefaultIconFrame" )
    e:Resize( 32, 32 )
    e:AnchorTo( window.W.General.Container_Button, "right", "left", 5, 0 )
    e.OnRButtonUp =
		function()
			if( isRunning ) then return end
			displayMenu_Item( "determinants", window.W.General.Determinant_Button.interface, nil, true )
		end
	tinsert( displayWindows, e )
    window.W.General.Determinant_Button = e
    
    -- General - Determinant Overlay Button
    e = window.W.General( "Button", windowId .. "DeterminantButtonOverlay", "Motion_Button_DefaultIconFrame_Overlay" )
    e:Resize( WindowGetDimensions( window.W.General.Determinant_Button.name ) )
    e:AnchorTo( window.W.General.Determinant_Button, "center", "center", 0, 0 )
    e:Layer( "popup" )
    e:Tint( 222, 192, 50 )
    window.W.General.Determinant_Button.overlay = e
    
    -- General - Resource 1 Button
    e = window.W.General( "Button", windowId .. "Resource1Button", "Motion_Button_DefaultIconFrame" )
    e:Resize( 32, 32 )
    e:AnchorTo( window.W.General.Determinant_Button, "right", "left", 5, 0 )
    e.OnRButtonUp =
		function()
			if( isRunning ) then return end
			displayMenu_Item( "resources", window.W.General.Resource1_Button.interface, 1, true )
		end
	tinsert( displayWindows, e )
	tinsert( resourceWindows, e )
	window.W.General.Resource1_Button = e
	
	-- General - Resource 1 Overlay Button
    e = window.W.General( "Button", windowId .. "Resource1ButtonOverlay", "Motion_Button_DefaultIconFrame_Overlay" )
    e:Resize( WindowGetDimensions( window.W.General.Resource1_Button.name ) )
    e:AnchorTo( window.W.General.Resource1_Button, "center", "center", 0, 0 )
    e:Layer( "popup" )
    e:Tint( 222, 192, 50 )
    window.W.General.Resource1_Button.overlay = e
    
    -- General - Resource 2 Button
    e = window.W.General( "Button", windowId .. "Resource2Button", "Motion_Button_DefaultIconFrame" )
    e:Resize( 32, 32 )
    e:AnchorTo( window.W.General.Resource1_Button, "right", "left", 5, 0 )
    e.OnRButtonUp =
		function()
			if( isRunning ) then return end
			displayMenu_Item( "resources", window.W.General.Resource2_Button.interface, 2, true )
		end
	tinsert( displayWindows, e )
	tinsert( resourceWindows, e )
    window.W.General.Resource2_Button = e
    
    -- General - Resource 2 Overlay Button
    e = window.W.General( "Button", windowId .. "Resource2ButtonOverlay", "Motion_Button_DefaultIconFrame_Overlay" )
    e:Resize( WindowGetDimensions( window.W.General.Resource2_Button.name ) )
    e:AnchorTo( window.W.General.Resource2_Button, "center", "center", 0, 0 )
    e:Layer( "popup" )
    e:Tint( 222, 192, 50 )
    window.W.General.Resource2_Button.overlay = e
    
    -- General - Resource 3 Button
    e = window.W.General( "Button", windowId .. "Resource3Button", "Motion_Button_DefaultIconFrame" )
    e:Resize( 32, 32 )
    e:AnchorTo( window.W.General.Resource2_Button, "right", "left", 5, 0 )
    e.OnRButtonUp =
		function()
			if( isRunning ) then return end
			displayMenu_Item( "resources", window.W.General.Resource3_Button.interface, 3, true )
		end
	tinsert( displayWindows, e )
	tinsert( resourceWindows, e )
    window.W.General.Resource3_Button = e
    
    -- General - Resource 3 Overlay Button
    e = window.W.General( "Button", windowId .. "Resource3ButtonOverlay", "Motion_Button_DefaultIconFrame_Overlay" )
    e:Resize( WindowGetDimensions( window.W.General.Resource3_Button.name ) )
    e:AnchorTo( window.W.General.Resource3_Button, "center", "center", 0, 0 )
    e:Layer( "popup" )
    e:Tint( 222, 192, 50 )
    window.W.General.Resource3_Button.overlay = e
    
    -- General - Hint Window
    e = window.W.General( "Window", windowId .. "Status", "Motion_Window_Hint" )
    e:Resize( 32, 32 )
    e:Layer( "secondary" )
    e:AnchorTo( window.W.General.Resource3_Button, "right", "left", 5, 0 )
    window.W.General.Hint_Window = e
    DynamicImageSetTexture( window.W.General.Hint_Window.name .. "Image", "EA_Apothecary01_d5", 21, 20 )
    DynamicImageSetTextureSlice( window.W.General.Hint_Window.name .. "Image", defaultSlice )
    
    
    
    --[[
	e = window.W.General( "Image", windowId .. "HintImage", "Motion_DynamicImage_StatusOrb" )
	e:Resize( 21, 20 )
    e:AnchorTo( window.W.General.Resource3_Button, "right", "left", 5, 0 )
    e:Layer( "secondary" )
    e:Texture( "EA_Apothecary01_d5", 21, 20 )
    e:TexSlice( defaultSlice )
    window.W.General.Hint_Image = e
    --]]
    
    
    -- General - Make Count Textbox
    e = window.W.General( "Textbox", windowId .. "MakeCountTextBox", "Motion_EditBox_DefaultFrame" )
    e:Resize( 65, 32 )
    e:Show()
    e:ClearAnchors()
    e:AddAnchor( window.W.General.Container_Button.name, "bottomleft", "topleft", 0, 4 )
    e:AddAnchor( window.W.General.Determinant_Button.name, "bottomright", "topright", 0, 4 )
    e:Layer( "secondary" )
    e:SetText( towstring( db.general.makes ) )
    window.W.General.MakeCount_Textbox = e
    
    -- General - Make Button
    e = window.W.General( "Button", windowId .. "MakeButton", "Motion_Button_Make" )
    e:Resize( 25, 25 )
    e:Show()
    e:AnchorTo( window.W.General.Resource1_Button, "bottom", "top", 0, 7 )
    e:Layer( "secondary" )
    e.OnMouseOver =
    	function()
    		local display
    		if( isRunning ) then
    			display = T["Stop Making Item(s)"]
    		else
    			display = T["Start Making Item(s)"]
    		end
    	
    		Tooltips.CreateTextOnlyTooltip( SystemData.MouseOverWindow.name, display )
    		Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_VARIABLE )
    	end
    e.OnLButtonUp = 
    	function()
    		if( isRunning ) then
    			Motion.StopCrafting()
    		else
    			Motion.StartCrafting()
    		end
    	end
    e.OnRButtonUp =
    	function()
    		if( isRunning or GameData.Player.inCombat) then return end
    		Motion.LoadRecipe( Motion.lastRecipe )
			
		end
    e:Tint( unpack( goColor ) )
    window.W.General.Make_Button = e
    
    -- General - Clear Button
    e = window.W.General( "Button", windowId .. "ClearButton", "Motion_Button_Clear" )
    e:Resize( 25, 25 )
    e:Show()
    e:AnchorTo( window.W.General.Resource2_Button, "bottom", "top", 0, 7 )
    e:Layer( "secondary" )
    e.OnMouseOver =
    	function()
    		Tooltips.CreateTextOnlyTooltip( SystemData.MouseOverWindow.name, T["Clear Ingredients"] )
	        Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_VARIABLE )
    	end
    e.OnLButtonUp = 
    	function()
    		if( isRunning ) then return end
    		clearAllItemData()
    	end
    window.W.General.Clear_Button = e
    
    -- General - Recipe Button
    e = window.W.General( "Button", windowId .. "RecipeButton", "Motion_Button_Recipe" )
    e:Resize( 25, 25 )
    e:Show()
    e:AnchorTo( window.W.General.Resource3_Button, "bottom", "top", 0, 8 )
    e:Layer( "secondary" )
    e.OnMouseOver =
    	function()
    		Motion.DisplayTwoLineTooltip( SystemData.MouseOverWindow.name,
					T["L-Click: List the saved recipes"], 
					T["R-Click: Save the current recipe"] )
	    end
    e.OnRButtonUp =
    	function()
    		if( isRunning or GameData.Player.inCombat) then return end
    		
			DialogManager.MakeTextEntryDialog( 
				T["Motion - Save Recipe"], 
				T["Please enter a name for the new recipe."], 
				T["New Recipe"], 
				Motion.OnRecipeAddSuccess,
				Motion.OnRecipeAddCancel,
				32,
				false )
		end
    e.OnLButtonUp = 
    	function()
    		if( isRunning ) then return end
    		displayMenu_Recipe()
    	end
    window.W.General.Recipe_Button = e


    -- General - Config Button
    e = window.W.General( "Button", windowId .. "ConfigButton", "Motion_Button_Config" )
    e:Resize( 25, 25 )
    e:Show()
    e:AnchorTo( window.W.General.Hint_Window, "bottom", "top", 0, 8 )
    e:Layer( "secondary" )
    e.OnMouseOver =
    	function()
    		Tooltips.CreateTextOnlyTooltip( SystemData.MouseOverWindow.name, T["Toggle Configuration"] )
    		Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_VARIABLE )
    	end
    e.OnLButtonUp = 
    	function()
    		Motion.ToggleConfig()
    	end
    window.W.General.Config_Button = e
    
    --
    -- Initialize our common display window settings/events
    --
    for _, frame in pairs( displayWindows )
	do
		frame:Layer( "secondary" )
		
		frame:Hide()
	
		frame.SetInterface =
			function( interface )
				frame.interface = interface
			end
			
		frame.GetInterface =
			function()
				return frame.interface
			end
			
		frame.OnLButtonUp = 
			function()
				if( isRunning ) then return end
				frame.interface:ClearItemData()
			end

		frame.OnMouseOver = 
			function()
				if( frame.interface:GetItemData() ~= nil ) then
					Tooltips.CreateItemTooltip( frame.interface:GetItemData(), 
	                    frame.name,
	                    Tooltips.ANCHOR_WINDOW_VARIABLE, 
	                    true, 
	                    nil, nil, nil )
	            else
	            	Tooltips.CreateTextOnlyTooltip( SystemData.MouseOverWindow.name, T["Right click to display menu."] )
	            	Tooltips.AnchorTooltip(Tooltips.ANCHOR_WINDOW_VARIABLE)
				end
			end
	end
    
	updateUIPosition()
end

function orderingFunction( table1, table2, sortKey, sortKeys )
    local value1        = table1[sortKey]
    local value2        = table2[sortKey]
    local value1Type    = type (value1)
    
    if( value1Type ~= type (value2) or not validOrderingValueTypes[value1Type] ) then return false end
    
    if( value1Type == "boolean" )then
    	local function NumberFromBoolean (b) 
            if (b) then return 1 end 
            return 0 
        end
        value1 = NumberFromBoolean (value1)
        value2 = NumberFromBoolean (value2)
    end
    
    assert( type (sortKeys[sortKey]) == "table" )
    
    if( sortKeys[sortKey].isNumeric ) then
        value1 = tonumber (value1)
        value2 = tonumber (value2)
    end
    
    if( value1 == value2 ) then
        if( sortKeys[sortKey].fallback ) then return orderingFunction( table1, table2, sortKeys[sortKey].fallback, sortKeys ) end
    else
    	if( sortKeys[sortKey].sortOrder == sortOrder.INCREASING ) then return value1 < value2 end
		return value1 > value2
    end
    return false
end

function performCraft()
	Motion.Debug( "Entering performCraft" )
	local craftSubKey = determineCraftSubKey()
	if( craftSubKey == nil ) then
		Motion.Debug( "performCraft: craftSubKey == nil" ) 
		return false 
	end
	local craftType 	= craftingTypes[playerCraftType]
	local data 			= craftType[craftSubKey]
	if( data == nil ) then
		Motion.Debug( "performCraft: data == nil" ) 
		return false 
	end
	
	--
	-- Make a list of all of our slotted items
	--
	local slottedItems 			= new()
	local item
	for idx = 0, MAX_CRAFT_SLOTS
	do
		local interface = allCraftInterfaces[idx]
		if( interface ~=  nil ) then
			item = interface:GetItemData()
			if( item ~= nil ) then
				tinsert( slottedItems, item )
			end
		end	
	end 
	
	-- 
	-- Retrieve a count of the item types, only processing if they pass our crafting level
	--
	local slottedItemTypeCount 	= new()
	for _, item in pairs( slottedItems )
	do
		local _, itemResourceType, itemCraftingLevel = CraftingSystem.GetCraftingData( item )
		if( itemCraftingLevel <= math.max( GameData.CraftingStatus.SkillLevel, GameData.TradeSkillLevels[playerCraftType] ) ) then
			if( slottedItemTypeCount[itemResourceType] == nil ) then slottedItemTypeCount[itemResourceType] = 0
				slottedItemTypeCount[itemResourceType] = slottedItemTypeCount[itemResourceType] + 1
			end
		end
	end
	
	--
	-- Iterate our requirements and check against the slotted item type counts
	--
	local countKey = "minCount"
	if( db.general.enforceoptimalresources ) then countKey = "optimalCount" end
	for idx, req in ipairs( data.requirements )
	do
		-- If the item type count doesnt have an entry for this resource
		if( slottedItemTypeCount[req.resource] == nil ) then
			-- and the requirement count says it should have a count, validation fails
			if( req[countKey] > 0 ) then
				Motion.Debug( "performCraft: slottedItemTypeCount[req.resource] == nil and req[countKey] > 0" ) 
				return false
			end 
		elseif( slottedItemTypeCount[req.resource] < req[countKey] ) then
			-- The item type count does not meet the required count
			Motion.Debug( "performCraft: slottedItemTypeCount[req.resource] < req[countKey]" ) 
			return false 
		end	
	end
	  
	--
	-- Generate a count of the required items that make up the slotted recipe
	--
	local itemReqCount		= new()
	for _, item in pairs( slottedItems )
	do
		if( itemReqCount[item.uniqueID] == nil ) then
			itemReqCount[item.uniqueID] = 0
		end
		itemReqCount[item.uniqueID] = itemReqCount[item.uniqueID] + 1
	end
	
	--
	-- Check to see if we have enough in inventory of the items in inventory
	--
	for id, count in pairs( itemReqCount )
	do
		local invCount = retrieveItemInventoryCount( id )
		-- If the required count exceeds what is in our inventory fail
		if( count > invCount ) then
			Motion.Debug( "performCraft: count > invCount" ) 
			return false 
		end
	end
	
	-------------------------------------------------------------
	-- If we have made it this far we have passed all of the validation
	-------------------------------------------------------------
	
	--
	-- Check to see if the items we need for crafting are already slotted,
	-- if so we can bypass the slotting of items and proceed directly to crafting
	--
	local backpackSlots = GetCraftingBackPackSlots( playerCraftType )
	local backpackSlotCount = 0
	for _, _ in pairs( backpackSlots )
	do
		backpackSlotCount = backpackSlotCount + 1
	end
	
	-- Quick check of the counts to see if they are the same to save some processing
	if( backpackSlotCount == #slottedItems ) then
		local slottedItemsCheck = DataUtils.CopyTable( slottedItems )
		local itemData
		
		for idx, slot in pairs( backpackSlots )
		do
			itemData = EA_Window_Backpack.GetItemsFromBackpack( slot.backpack )[slot.slot]
			
			if( itemData ~= nil ) then
				for idx, slotItem in pairs( slottedItemsCheck )
				do
					if( itemData.uniqueID == slotItem.uniqueID ) then
						tremove( slottedItemsCheck, idx )
						break
					end		
				end
			end
		end
		
		if( #slottedItemsCheck == 0 ) then
			PerformCrafting( playerCraftType, 1 )
			return true	
		end
	end
	
	-- Let the crafting system know to clear itself
	CraftingSystem.Clear()
	
	--
	-- Remove the current container to clear any slotted items
	--
	clearClientSlottedCraftingInformation()
	
	local allocatedSlots = new()
	
	--
	-- Get the inventory slots for the items
	--
	local containerSlot, containerType = Motion.RetrieveFirstAvailableItemSlot( craftInterfaces.container:GetItemData().uniqueID, allocatedSlots )
	if( containerSlot == nil or containerType == nil ) then
		Motion.Debug( "performCraft: containerSlot == nil or containerType == nil" ) 
		return false 
	end
	
	if( allocatedSlots[containerType] == nil ) then allocatedSlots[containerType] = new() end
	if( allocatedSlots[containerType][containerSlot] == nil ) then allocatedSlots[containerType][containerSlot] = 0 end
	allocatedSlots[containerType][containerSlot] = allocatedSlots[containerType][containerSlot] + 1
	
	local determinantSlot, determinantType 	= Motion.RetrieveFirstAvailableItemSlot( craftInterfaces.determinant:GetItemData().uniqueID, allocatedSlots )
	if( determinantSlot == nil or determinantType == nil ) then
		Motion.Debug( "performCraft: determinantSlot == nil or determinantType == nil" ) 
		return false 
	end
	
	if( allocatedSlots[containerType] == nil ) then allocatedSlots[containerType] = new() end
	if( allocatedSlots[containerType][containerSlot] == nil ) then allocatedSlots[containerType][containerSlot] = 0 end
	allocatedSlots[containerType][containerSlot] = allocatedSlots[containerType][containerSlot] + 1

	local resourceSlots 	= new()
	for k, v in ipairs( craftInterfaces.resources )
	do
		if( v:GetItemData() ~= nil ) then
			local slot, type 			= Motion.RetrieveFirstAvailableItemSlot( v:GetItemData().uniqueID, allocatedSlots )
			
			if( slot ~= nil and type ~= nil ) then
				resourceSlots[k]		= { slot = slot, type = type }
				
				if( allocatedSlots[type] == nil ) then allocatedSlots[type] = new() end
				if( allocatedSlots[type][slot] == nil ) then allocatedSlots[type][slot] = 0 end
				allocatedSlots[type][slot] = allocatedSlots[type][slot] + 1
			end
			
			-- If for some reason we no longer have any of this item available, fail
			if( resourceSlots[k] == nil ) then
				Motion.Debug( "performCraft: resourceSlots[k] == nil" ) 
				return false 
			end
		end 
	end

	-- Prepare our container information
	EA_BackpackUtilsMediator.RequestLockForSlot( containerSlot, containerType, windowId, {r=0,g=255,b=0} )
	
	-- Add the determimant
	tinsert( queuedCraftingItemAdditions, 1, { slot=determinantSlot, type=determinantType } )
	EA_BackpackUtilsMediator.RequestLockForSlot( determinantSlot, determinantType, windowId, {r=0,g=255,b=0} )
	
	-- Add the resources if needed
	if( resourceSlots[1] ~= nil ) then
		tinsert( queuedCraftingItemAdditions, 2, { slot=resourceSlots[1].slot, type=resourceSlots[1].type } )
		EA_BackpackUtilsMediator.RequestLockForSlot( resourceSlots[1].slot, resourceSlots[1].type, windowId, {r=0,g=255,b=0} )
	end
	if( resourceSlots[2] ~= nil ) then
		tinsert( queuedCraftingItemAdditions, 3, { slot=resourceSlots[2].slot, type=resourceSlots[2].type } )
		EA_BackpackUtilsMediator.RequestLockForSlot( resourceSlots[2].slot, resourceSlots[2].type, windowId, {r=0,g=255,b=0} )
	end
	if( resourceSlots[3] ~= nil ) then
		tinsert( queuedCraftingItemAdditions, 4, { slot=resourceSlots[3].slot, type=resourceSlots[3].type } )
		EA_BackpackUtilsMediator.RequestLockForSlot( resourceSlots[3].slot, resourceSlots[3].type, windowId, {r=0,g=255,b=0} )
	end
	
	-- Add our crafting container, to start the event chain
	AddCraftingContainer( playerCraftType, containerSlot, containerType )
	
	del(slottedItems) 
	del(slottedItemTypeCount)  
	del(itemReqCount)
	del(allocatedSlots) 
	del(resourceSlots)
	
	return true
end

function processItemMove( updatedSlots, type )
	--
	-- TODO: We have 2 methods we can use here
	-- 1) We blindly update all of our item counts
	-- 2) We maintain a list of slots used to make up a count of an item
	--    and as the slots get updated, we manipulate the count accordingly
	--
	-- For now, we will use #1
	--
	for _, interface in pairs( allCraftInterfaces )
	do
		interface:UpdateItemCount()
	end
end

function resetWindows()
	window.W.General.Container_Button:Show()
	window.W.General.Determinant_Button:Hide()
	window.W.General.Resource1_Button:Hide()
	window.W.General.Resource2_Button:Hide()
	window.W.General.Resource3_Button:Hide()
	window.W.General.Determinant_Button:Hide()
end

function retrieveItemInventoryCount( itemId )
	local count	= 0
	
	if( itemId ~= nil ) then
		for _, set in ipairs( Motion.backpackTypes )
		do
			local data = set.data()
			for slot, item in ipairs( data )
			do
				if( itemId == item.uniqueID ) then
					count = count + item.stackCount
				end
			end
		end
	end
	
	return count
end

function retrieveItemList( desiredCraftType, desiredItemType, minLevelReq )
	local checkList = new()
	local itemList = new()
	
	for _, set in ipairs( Motion.backpackTypes )
	do
		for slot, item in ipairs( set.data() )
		do
			local craftingTypes, resourceType, craftingLevel =  CraftingSystem.GetCraftingData( item )
			
			-- Verify we have been given a crafting type
			if( craftingTypes ~= nil ) then
				local found = false
				for k, v in pairs( craftingTypes )
				do
					if( v == desiredCraftType ) then
						found = true
						break
					end
				end
			
				-- If we found the desired craft type
				if( found and desiredItemType[resourceType] ~= nil and craftingLevel <= minLevelReq  ) then
					if( checkList[item.uniqueID] == nil ) then
						checkList[item.uniqueID] = item.stackCount
						table.insert( itemList, DataUtils.CopyTable( item ) )
					else
						checkList[item.uniqueID] = checkList[item.uniqueID] + item.stackCount
					end
				end
			end
		end
	end
	
	-- Copy our stack counts into the item list
	for _, item in pairs( itemList )
	do
		if( checkList[item.uniqueID] ~= nil ) then
			item.stackCount = checkList[item.uniqueID]
		end
	end
	
	del(checkList)
	
	return itemList
end

function sortItems( item1, item2 )
    return orderingFunction( item1, item2, "craftingSkillRequirement", itemSortKeys )
end

function sortRecipes( item1, item2 )
	return orderingFunction( item1, item2, "name", recipeSortKeys )
end

function updateCraftingSuccessDisplay()
	local slice = "White-Orb"
	
	-- If we do not have an item slotted in the container use our default
	if( craftInterfaces.container:GetItemData() ~= nil ) then 
		local craftSubKey = determineCraftSubKey()
		
		if( craftSubKey ~= nil ) then 
			local craftType 	= craftingTypes[playerCraftType]
			local data 			= craftType[craftSubKey]
			if( data ~= nil ) then 
			
				local success = data.success[GameData.CraftingStatus.SuccessChance]
				if( success ~= nil ) then
					slice = success.slice
				end
			else
				d( "updateCraftingSuccessDisplay: data == nil" )
			end
		else
			d( "updateCraftingSuccessDisplay: craftSubKey == nil" )
		end
	end
	
	DynamicImageSetTextureSlice( window.W.General.Hint_Window.name .. "Image", slice )
end

function updateUIPosition()
	if( DoesWindowExist( windowId ) ) then
		WindowClearAnchors( windowId )
    	WindowAddAnchor( windowId, db.anchor.point, db.anchor.relwin, db.anchor.relpoint, db.anchor.x, db.anchor.y )
    	WindowSetScale( windowId, db.anchor.scale )
    end
    
    if( DoesWindowExist( iconWindowId ) ) then
    	WindowClearAnchors( iconWindowId )
    	WindowAddAnchor( iconWindowId, db.icon.point, db.icon.relwin, db.icon.relpoint, db.icon.x, db.icon.y )
    	WindowSetScale( iconWindowId, db.icon.scale )
    	WindowSetShowing( iconWindowId, db.icon.visible )
    end
end

function upgradeProfile()
	local oldVersion = db.general.version
	db.general.version = VERSION
	
	-- add any processing here on version change
end
