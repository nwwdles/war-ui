--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

--
-- The localization information present within this file is automatically
-- packaged by the CurseForge packager.
--
-- Incorrect or missing translations can be updated here:
-- http://war.curseforge.com/projects/Motion/localization/
--

local T = LibStub("WAR-AceLocale-3.0"):NewLocale("Motion", "itIT")
if not T then return end

-- T["(Motion) use /motion to bring up configuration window."] = L""
T["About Motion"] = L"Informazioni su Motion"
T["Active Profile:"] = L"Profilo attivo"
T["Apply"] = L"Applica"
-- T["Are you sure?"] = L""
T["Clear Ingredients"] = L"Pulisci Ingredienti"
T["Close"] = L"Chiudi"
T["Configuration"] = L"Configurazione"
T["Copy Settings From:"] = L"Copia settaggi da:"
-- T["Create"] = L""
-- T["Create New Profile"] = L""
-- T["Create New Profile:"] = L""
T["Current Crafting Status"] = L"Stato attuale della creazione"
T["Current Profile:"] = L"Profilo attuale"
T["Default Makes:"] = L"Ripetizioni Predefinite:"
-- T["Delete"] = L""
T["Do you wish to delete the following recipe?"] = L"Vuoi cancellare la seguente combinazione?"
T["Enable Debugging"] = L"Abilita Debugging"
T["Enforce Optimal Resource Counts"] = L"Forza i conteggi delle risorse ottimali"
T["General"] = L"Generale"
-- T["L-Click: List the saved recipes"] = L""
T["L-Click: Save the current recipe"] = L"Click Sinistro: Salva la combinazione attuale"
T["Motion %s initialized."] = L"Motion % inizializzato\
% � la versione di Motion"
T["Motion - Configuration"] = L"Motion - Configurazione"
T["Motion - Save Recipe"] = L"Motion - Salva Combinazione"
-- T["Motion Toggle"] = L""
T["NOTE: Profile changes take effect immediately!"] = L"NOTA: I cambiamenti al profilo avranno effetto immediatamente!"
T["New Recipe"] = L"Nuova Combinazione"
T["No"] = L"No"
T["No items found."] = L"Nessun Oggetto trovato"
T["No recipes found."] = L"Nessuna combinazione trovata"
T["Please enter a name for the new recipe."] = L"Inserire un nome per la nuova combinazione"
T["Profiles"] = L"Profili"
T["R-Click: List the saved recipes"] = L"Click Destro: Lista delle combinazioni salvate"
-- T["R-Click: Save the current recipe"] = L""
T["Reset Profile"] = L"Azzera Profilo"
T["Revert"] = L"Annulla"
T["Right click to display menu."] = L"Click Destro per aprire il menu"
T["Saved Recipe(s)"] = L"Salva Combinazione(i)"
T["Set Active Profile:"] = L"Seleziona profilo attivo"
-- T["Show/Hide Motion"] = L""
T["Start Making Item(s)"] = L"Inizia a creare oggetto(i)"
T["Stop Crafting On Combat"] = L"Ferma la creazione in combattimento"
T["Stop Crafting On Spellcast"] = L"Ferma la creazione quando si laciano incantesimi"
T["Stop Making Item(s)"] = L"Smetti di creare oggetto(i)"
T["This is the default number of makes Motion will make."] = L"Questo � il numero predefinito di ripetizioni che Motion far�"
T["Toggle Configuration"] = L"Modifica Configurazione"
T["WARNING: Clicking this button will restore the current profile back to default values!"] = L"ATTENZIONE: Cliccando su questo bottone il profilo attuale verr� riportato ai valori predefiniti!"
T["With this enabled Motion will output debugging information to the Debug Log.  This information can be provided to the developer to help diagnose problems."] = L"Con questo attivato Motion inserir� le informazioni di debug nel Registro Debug. Queste informazioni possono essere fornite allo sviluppatore per aiutare nella diagnosi dei problemi."
T["With this option enabled you will stop crafting if you cast a spell."] = L"Con questa opzione attiva fermerai la creazione quando lanci un incatesimo."
T["With this option enabled you will stop crafting if you go into combat."] = L"Quando questa opzione � attiva fermerai la creazione quando entri in combattimento"
T["With this setting enabled, Motion will enforce a minimum amount of resources to be present before it will create an item.  This helps to prevent making items that are not to their full potenital."] = L"Abilitando questo settaggio, Motion imporr� un quantitativo minimo di risorse che devono essere presenti prima di creare un oggetto. Questo aiuta ad evitare di creare oggetti che non sono alla massima potenza."
T["Yes"] = L"S�"

