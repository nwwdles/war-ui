--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

--
-- The localization information present within this file is automatically
-- packaged by the CurseForge packager.
--
-- Incorrect or missing translations can be updated here:
-- http://war.curseforge.com/projects/Motion/localization/
--

local T = LibStub("WAR-AceLocale-3.0"):NewLocale("Motion", "deDE")
if not T then return end

-- T["(Motion) use /motion to bring up configuration window."] = L""
T["About Motion"] = L"�ber Motion"
T["Active Profile:"] = L"Aktives Profil:"
T["Apply"] = L"�bernehmen"
-- T["Are you sure?"] = L""
T["Clear Ingredients"] = L"Zutaten l�schen"
T["Close"] = L"Schlie�en"
T["Configuration"] = L"Konfiguration"
T["Copy Settings From:"] = L"Einstellungen kopieren von:"
-- T["Create"] = L""
-- T["Create New Profile"] = L""
-- T["Create New Profile:"] = L""
T["Current Crafting Status"] = L"Aktueller Herstellungsstatus"
T["Current Profile:"] = L"Aktuelles Profil:"
T["Default Makes:"] = L"Standardanzahl:"
-- T["Delete"] = L""
T["Do you wish to delete the following recipe?"] = L"Das folgende Rezept l�schen?"
T["Enable Debugging"] = L"Debugging anschalten"
T["Enforce Optimal Resource Counts"] = L"Optimale Ressourcen erzwingen"
T["General"] = L"Allgemein"
T["L-Click: List the saved recipes"] = L"L-Klick: Gespeicherte Rezepte anzeigen"
T["L-Click: Save the current recipe"] = L"L-Klick: Aktuelles Rezept speichern"
T["Motion %s initialized."] = L"Motion %s initialisiert"
T["Motion - Configuration"] = L"Motion - Konfiguration"
T["Motion - Save Recipe"] = L"Motion - Rezept speichern"
-- T["Motion Toggle"] = L""
T["NOTE: Profile changes take effect immediately!"] = L"HINWEIS: Profil�nderungen werden sofort ausgef�hrt!"
T["New Recipe"] = L"Neues Rezept"
T["No"] = L"Nein"
T["No items found."] = L"Keine Gegenst�nde gefunden"
T["No recipes found."] = L"Keine Rezepte gefunden."
T["Please enter a name for the new recipe."] = L"Bitte einen Namen f�r das neue Rezept angeben."
T["Profiles"] = L"Profile"
T["R-Click: List the saved recipes"] = L"R-Klick: Gespeicherte Rezepte anzeigen"
T["R-Click: Save the current recipe"] = L"R-Klick: Aktuelles Rezept speichern"
T["Reset Profile"] = L"Profil zur�cksetzen"
T["Revert"] = L"R�ckg�ngig"
T["Right click to display menu."] = L"Rechtsklick f�r Men�"
T["Saved Recipe(s)"] = L"Gespeicherte Rezepte"
T["Set Active Profile:"] = L"Aktives Profil setzen:"
T["Show/Hide Motion"] = L"zeige/verstecke Motion"
T["Start Making Item(s)"] = L"Beginne Herstellung"
T["Stop Crafting On Combat"] = L"Herstellung bei Kampf beenden"
T["Stop Crafting On Spellcast"] = L"Herstellung beim Zaubern beenden"
T["Stop Making Item(s)"] = L"Stoppe Herstellung"
T["This is the default number of makes Motion will make."] = L"Dies ist die Standardanzahl an die Motion herstellen wird."
T["Toggle Configuration"] = L"Konfiguration anzeigen"
T["WARNING: Clicking this button will restore the current profile back to default values!"] = L"WARNUNG: Ein Klick auf diese Schaltfl�che setzt das aktuelle Profil auf die Standardwerte zur�ck!"
T["With this enabled Motion will output debugging information to the Debug Log.  This information can be provided to the developer to help diagnose problems."] = L"Ist diese Option aktiviert, wird Motion Debugging-Informationen in das Debug-Log schreiben. Diese Informationen k�nnen dem Entwickler von Motion mitgeteilt werden, um Probleme zu finden."
T["With this option enabled you will stop crafting if you cast a spell."] = L"Diese Option stoppt den Herstellungsvorgang sobald man einen Zauber wirkt."
T["With this option enabled you will stop crafting if you go into combat."] = L"Diese Option stoppt den Herstellungsvorgang wenn man k�mpft."
T["With this setting enabled, Motion will enforce a minimum amount of resources to be present before it will create an item.  This helps to prevent making items that are not to their full potenital."] = L"Mit dieser Einstellung, erzwingt Motion das Vorhandensein der Mindestmenge an Ressourcen bevor ein Gegenstand hergestellt wird. Diese Einstellung hilft dabei, die Herstellung von Gegenst�nden zu unterbinden, die nicht ihr bestm�gliches Potential aussch�pfen."
T["Yes"] = L"Ja"

