if not WarBoard_TogglerTidyRoll then WarBoard_TogglerTidyRoll = {} end
local WarBoard_TogglerTidyRoll = WarBoard_TogglerTidyRoll
local modName = "WarBoard_TogglerTidyRoll"
local modLabel = "TidyRoll"

function WarBoard_TogglerTidyRoll.Initialize()
	if LibWBToggler.CreateToggler(modName, modLabel, "TidyRollIcon", 0, 0) then
		WindowSetDimensions(modName, 113, 30)
		WindowSetDimensions(modName.."Label", 81, 30)
		LibWBToggler.RegisterEvent(modName, "OnLButtonUp", "WarBoard_TogglerTidyRoll.OptionsWindow")
		LibWBToggler.RegisterEvent(modName, "OnMouseOver", "WarBoard_TogglerTidyRoll.ShowStatus")
	end
end

function WarBoard_TogglerTidyRoll.OptionsWindow()
	TidyRoll.ToggleOptions()
end

function WarBoard_TogglerTidyRoll.ShowStatus()
	LibWBToggler.DefaultTooltip(modName, modLabel)
end
