if not WarBoard_TogglerBagOMatic then WarBoard_TogglerBagOMatic = {} end
local WarBoard_TogglerBagOMatic = WarBoard_TogglerBagOMatic
local modName = "WarBoard_TogglerBagOMatic"
local modLabel = "BagOMatic"
local BOMTable = { {"sort", "sortbank", "sortcraft", "sortcurrency", count = 4}, {"pack", "packbank", count = 2}, {"bank", count = 1} } 
local Selection = {1, 1}

local function STTT(str) -- String To Tooltip Text
	return string.gsub(str, '(.)(.?.?.?)(.?)(.*)', function(a, b, c, d) return string.upper(a)..b..' '..string.upper(c)..d end)
end

function WarBoard_TogglerBagOMatic.Initialize()
	if LibWBToggler.CreateToggler(modName, modLabel, "BagOMaticIcon", 0, 0) then
		LibWBToggler.RegisterEvent(modName, "OnLButtonUp", "WarBoard_TogglerBagOMatic.CurrentOption")
		LibWBToggler.RegisterEvent(modName, "OnMButtonUp", "WarBoard_TogglerBagOMatic.ChangeSelectionList")
		LibWBToggler.RegisterEvent(modName, "OnRButtonUp", "WarBoard_TogglerBagOMatic.ChangeSelection")
		LibWBToggler.RegisterEvent(modName, "OnMouseOver", "WarBoard_TogglerBagOMatic.ShowStatus")
	end
end

function WarBoard_TogglerBagOMatic.CurrentOption()
	BagOMatic.cmd[BOMTable[ Selection[1] ][ Selection[2] ]].f()
end

function WarBoard_TogglerBagOMatic.ChangeSelectionList()
	Selection[2] = 1
	Selection[1] = Selection[1] + 1
	if Selection[1] > 3 then Selection[1] = 1 end
	WarBoard_TogglerBagOMatic.ShowStatus()
end

function WarBoard_TogglerBagOMatic.ChangeSelection()
	Selection[2] = Selection[2] + 1
	if Selection[2] > BOMTable[ Selection[1] ].count then Selection[2] = 1 end
	WarBoard_TogglerBagOMatic.ShowStatus()
end

function WarBoard_TogglerBagOMatic.ShowStatus()
	Tooltips.CreateTextOnlyTooltip(modName, nil)
	Tooltips.AnchorTooltip(WarBoard.GetModToolTipAnchor(modName))
	Tooltips.SetTooltipText(1, 1, L"BagOMatic")

	Tooltips.SetTooltipText(2, 1, L"Left Click:")
	Tooltips.SetTooltipText(2, 3, L"Use Option")
	Tooltips.SetTooltipText(3, 1, L"Middle Click:")
	Tooltips.SetTooltipText(3, 3, L"Change List:")
	Tooltips.SetTooltipText(4, 1, L"Right Click:")
	Tooltips.SetTooltipText(4, 3, L"Change Selection:")

	Tooltips.SetTooltipText(5, 1, L"Current List:")
	local row = 6
	for _,v in ipairs(BOMTable[Selection[1]]) do
		v = STTT(v)
		Tooltips.SetTooltipText(row, 1, towstring(v))
		row = row + 1
	end
	Tooltips.SetTooltipText(row, 1, L"Current Selection:")
	Tooltips.SetTooltipText(row, 3, towstring(STTT(BOMTable[ Selection[1] ] [ Selection[2] ])))

	Tooltips.Finalize()
end
