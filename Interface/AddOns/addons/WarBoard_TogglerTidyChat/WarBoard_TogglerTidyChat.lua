if not WarBoard_TogglerTidyChat then WarBoard_TogglerTidyChat = {} end
local WarBoard_TogglerTidyChat = WarBoard_TogglerTidyChat
local modName = "WarBoard_TogglerTidyChat"
local modLabel = "TidyChat"

function WarBoard_TogglerTidyChat.Initialize()
	if LibWBToggler.CreateToggler(modName, modLabel, "TidyChatIcon", 0, 0) then
		WindowSetDimensions(modName, 113, 30)
		WindowSetDimensions(modName.."Label", 81, 30)
		LibWBToggler.RegisterEvent(modName, "OnLButtonUp", "WarBoard_TogglerTidyChat.OptionsWindow")
		LibWBToggler.RegisterEvent(modName, "OnMouseOver", "WarBoard_TogglerTidyChat.ShowStatus")
	end
end

function WarBoard_TogglerTidyChat.OptionsWindow()
	TidyChat.ToggleOptions()
end

function WarBoard_TogglerTidyChat.ShowStatus()
	LibWBToggler.DefaultTooltip(modName, modLabel)
end
