if not WarBoard_TogglerSoR then WarBoard_TogglerSoR = {} end
local WarBoard_TogglerSoR = WarBoard_TogglerSoR
local modName = "WarBoard_TogglerSoR"
local modLabel = "SoR" 

function WarBoard_TogglerSoR.Initialize()
	if LibWBToggler.CreateToggler(modName, modLabel, "SoRicon", 0, 0, nil, 0.5) then
		WindowSetDimensions(modName, 80, 30)
		WindowSetDimensions(modName.."Label", 48, 30)
		LibWBToggler.RegisterEvent(modName, "OnLButtonUp", "WarBoard_TogglerSoR.ToggleActive")
		LibWBToggler.RegisterEvent(modName, "OnRButtonUp", "SOR.OpenOptions")
		LibWBToggler.RegisterEvent(modName, "OnMouseOver", "WarBoard_TogglerSoR.ShowStatus")
		if SOR_SavedSettings.HideWindow then LabelSetTextColor(modName.."Label", 255, 0, 0) else LabelSetTextColor(modName.."Label", 0, 255, 0) end
		LayoutEditor.UnregisterWindow("SOR.Button.Btn")
	end
end

function WarBoard_TogglerSoR.ToggleActive()
	SOR.Toggle()
	if SOR_SavedSettings.HideWindow then LabelSetTextColor(modName.."Label", 255, 0, 0) else LabelSetTextColor(modName.."Label", 0, 255, 0) end
	WarBoard_TogglerSoR.ShowStatus()
end

function WarBoard_TogglerSoR.ShowStatus()
	Tooltips.CreateTextOnlyTooltip(modName, nil)
	Tooltips.AnchorTooltip(WarBoard.GetModToolTipAnchor(modName))
	Tooltips.SetTooltipText(1, 1, L"SoR Toggler")
	if SOR_SavedSettings.HideWindow then Tooltips.SetTooltipColor(1, 1, 255, 0, 0) else Tooltips.SetTooltipColor(1, 1, 0, 255, 0) end
	Tooltips.SetTooltipText(2, 1, L"Left Click:")
	Tooltips.SetTooltipText(2, 3, L"Toggle Active")
	Tooltips.SetTooltipText(3, 1, L"Right Click:")
	Tooltips.SetTooltipText(3, 3, L"Toggle GUI")
	Tooltips.Finalize()
end
