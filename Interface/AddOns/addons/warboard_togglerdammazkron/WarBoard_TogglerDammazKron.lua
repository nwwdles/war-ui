if not WarBoard_TogglerDammazKron then WarBoard_TogglerDammazKron = {} end
local WarBoard_TogglerDammazKron = WarBoard_TogglerDammazKron
local modName = "WarBoard_TogglerDammazKron"
local modLabel = "DammazKron"

function WarBoard_TogglerDammazKron.Initialize()
	if LibWBToggler.CreateToggler(modName, modLabel, "DammazKronIcon", 0, 0) then
		WindowSetDimensions(modName, 142, 30)
		WindowSetDimensions(modName.."Label", 110, 30)
		LibWBToggler.RegisterEvent(modName, "OnLButtonUp", "WarBoard_TogglerDammazKron.ToggleTome")
		LibWBToggler.RegisterEvent(modName, "OnRButtonUp", "WarBoard_TogglerDammazKron.OptionsWindow")
		LibWBToggler.RegisterEvent(modName, "OnMouseOver", "WarBoard_TogglerDammazKron.ShowStatus")
	end
end

function WarBoard_TogglerDammazKron.ToggleTome()
	TomeWindow.ToggleShowing()
	if WindowGetShowing("TomeWindow") then
		TomeWindow.SetState(TomeWindow.PAGE_DAMMAZ_KRON_TOK , {})
	end
end

function WarBoard_TogglerDammazKron.OptionsWindow()
	DK_Config.ToggleMenu()
end

function WarBoard_TogglerDammazKron.ShowStatus()
	LibWBToggler.DefaultTooltip(modName, modLabel)
end
