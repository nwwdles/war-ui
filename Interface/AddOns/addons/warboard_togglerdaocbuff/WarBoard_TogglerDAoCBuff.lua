if not WarBoard_TogglerDAoCBuff then WarBoard_TogglerDAoCBuff = {} end
local WarBoard_TogglerDAoCBuff = WarBoard_TogglerDAoCBuff
local modName = "WarBoard_TogglerDAoCBuff"
local modLabel = "DAoCBuff"

function WarBoard_TogglerDAoCBuff.Initialize()
	if LibWBToggler.CreateToggler(modName, modLabel, "DAoCBuffIcon", 0, 0) then
		WindowSetDimensions(modName, 120, 30)
		WindowSetDimensions(modName.."Label", 88, 30)
		LibWBToggler.RegisterEvent(modName, "OnLButtonUp", "WarBoard_TogglerDAoCBuff.OptionsWindow")
		LibWBToggler.RegisterEvent(modName, "OnMouseOver", "WarBoard_TogglerDAoCBuff.ShowStatus")
	end
end

function WarBoard_TogglerDAoCBuff.OptionsWindow()
	DAoCBuffSettings.OpenOptionswindow()
end

function WarBoard_TogglerDAoCBuff.ShowStatus()
	LibWBToggler.DefaultTooltip(modName, modLabel)
end
