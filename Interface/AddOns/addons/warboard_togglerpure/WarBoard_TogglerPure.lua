if not WarBoard_TogglerPure then WarBoard_TogglerPure = {} end
local WarBoard_TogglerPure = WarBoard_TogglerPure
local modName = "WarBoard_TogglerPure"
local modLabel = "Pure"

function WarBoard_TogglerPure.Initialize()
	if LibWBToggler.CreateToggler(modName, modLabel, "PureIcon", 0, 0) then
		WindowSetDimensions(modName, 90, 30)
		WindowSetDimensions(modName.."Label", 58, 30)
		LibWBToggler.RegisterEvent(modName, "OnLButtonUp", "WarBoard_TogglerPure.OptionsWindow")
		LibWBToggler.RegisterEvent(modName, "OnMouseOver", "WarBoard_TogglerPure.ShowStatus")
	end
end

function WarBoard_TogglerPure.OptionsWindow()
	Pure.ToggleConfig()
end

function WarBoard_TogglerPure.ShowStatus()
	LibWBToggler.DefaultTooltip(modName, modLabel)
end
