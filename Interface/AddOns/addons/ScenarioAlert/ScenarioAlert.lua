ScenarioAlert = {}
ScenarioAlert.logFileName = "scenarioAlert"

function ScenarioAlert.OnInitialize()
	
	-- scenario log file
	TextLogCreate(ScenarioAlert.logFileName, 18000)
	TextLogSetEnabled(ScenarioAlert.logFileName, true)
	TextLogSetIncrementalSaving(ScenarioAlert.logFileName, true, StringToWString("logs/"..ScenarioAlert.logFileName..".log"))	

	-- coin log file
	TextLogCreate("coinLog", 18000)
	TextLogSetEnabled("coinLog", true)
	TextLogSetIncrementalSaving("coinLog", true, StringToWString("logs/coinLog.log"))
	
	RegisterEventHandler(SystemData.Events.SCENARIO_SHOW_JOIN_PROMPT, "ScenarioAlert.recordScPop")
	RegisterEventHandler(SystemData.Events.PLAYER_MONEY_UPDATED, "ScenarioAlert.OnPlayerCoinUpdate")
	--WindowRegisterCoreEventHandler("TwoButtonDlg1BoxText", "OnShown", "ScenarioAlert.recordScPop")

end

-- /script ScenarioAlert.recordScPop()
function ScenarioAlert.recordScPop()
	TextLogAddEntry(ScenarioAlert.logFileName, 0, towstring("pop"))
	TextLogSaveLog(ScenarioAlert.logFileName,towstring(""))
end

function ScenarioAlert.clearLogFile()
	-- sc log
	TextLogDestroy("scenarioAlert")
	TextLogCreate(ScenarioAlert.logFileName, 18000)
	TextLogSetEnabled(ScenarioAlert.logFileName, true)
	TextLogSetIncrementalSaving(ScenarioAlert.logFileName, true, StringToWString("logs/"..ScenarioAlert.logFileName..".log"))
	
	-- coin log
	TextLogDestroy("coinLog")
	TextLogCreate("coinLog", 18000)
	TextLogSetEnabled("coinLog", true)
	TextLogSetIncrementalSaving("coinLog", true, StringToWString("logs/coinLog.log"))
	
end

function ScenarioAlert.OnPlayerCoinUpdate()
	TextLogAddEntry("coinLog", 0, towstring("coin"))
	TextLogSaveLog("coinLog",towstring(""))
end