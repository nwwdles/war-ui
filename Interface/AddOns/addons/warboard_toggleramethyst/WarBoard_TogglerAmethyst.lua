if not WarBoard_TogglerAmethyst then WarBoard_TogglerAmethyst = {} end
local WarBoard_TogglerAmethyst = WarBoard_TogglerAmethyst
local modName = "WarBoard_TogglerAmethyst"
local modLabel = "Amethyst"

function WarBoard_TogglerAmethyst.Initialize()
	if LibWBToggler.CreateToggler(modName, modLabel, "AmethystIcon", 0, 0) then
		WindowSetDimensions(modName, 111, 30)
		WindowSetDimensions(modName.."Label", 79, 30)
		LibWBToggler.RegisterEvent(modName, "OnLButtonUp", "WarBoard_TogglerAmethyst.OptionsWindow")
		LibWBToggler.RegisterEvent(modName, "OnMouseOver", "WarBoard_TogglerAmethyst.ShowStatus")
		LabelSetTextColor(modName.."Label", 157, 107, 132)
	end
end

function WarBoard_TogglerAmethyst.OptionsWindow()
	Amethyst.Slash()
end

function WarBoard_TogglerAmethyst.ShowStatus()
	LibWBToggler.DefaultTooltip(modName, modLabel)
end
