QTS = {}

local function print(txt) EA_ChatWindow.Print(towstring(txt)) end
local function sortByName(a, b) return(WStringsCompare(a.Name, b.Name) == -1) end
local function sortByEnemy(a, b) return(WStringsCompare(a.Enemy, b.Enemy) == -1) end

local function ClearSortArrows(window)
	WindowSetShowing(window .. "DownArrow", false)
	WindowSetShowing(window .. "UpArrow", false)
end

local function ShowSortArrow(window)
	WindowSetShowing(window .. "DownArrow", not QTS.reverseDisplayOrder)
	WindowSetShowing(window .. "UpArrow", QTS.reverseDisplayOrder)
end

local function grep(grepTable, value)
	local found = false
	if grepTable ~= nil and grepTable ~= {} then
		for _, v in pairs(grepTable) do
			if (v == value) then
				found = true
				break
			end
		end
	end
	return found
end

function QTS.Initialize()
	QTS.TacticData = {}
	QTS.listDisplayData = {}
	QTS.Window = "QuickTacticSwitchWindow"
	QTS.TacticButtonTypes = {}
	QTS.CurrentTactic = nil
	QTS.sortColumnNum = 2
	QTS.sortColumnName = QTS.Window .. "NameSort"
	QTS.reverseDisplayOrder = false
	QTS.visibleRows = 9

	QTS.sortHeaderData =
	{
		{ column = "TextureSort", text = L"" },
		{ column = "NameSort", text = L"Name", sortFunc = sortByName },
		{ column = "EnemySort", text = L"Enemy", sortFunc = sortByEnemy }
	}

	for i, data in pairs (QTS.sortHeaderData) do
		local buttonName = QTS.Window .. data.column
		ButtonSetText(buttonName, data.text)
		if (buttonName == QTS.sortColumnName) then
			ShowSortArrow(buttonName)
		else
			ClearSortArrows(buttonName)
		end
	end

	-- Hooks
	QTS.RemoveTacticHook = TacticsEditor.OnActiveTacticRButtonDown
	QTS.SetChangedHook = TacticsEditor.OnSetMenuSelectionChanged
	TacticsEditor.OnActiveTacticRButtonDown = QTS.TacticRemoved
	TacticsEditor.OnSetMenuSelectionChanged = QTS.SetChanged

	-- Event Handlers
	RegisterEventHandler(SystemData.Events.PLAYER_NUM_TACTIC_SLOTS_UPDATED, "QTS.CountTactics")
	RegisterEventHandler(SystemData.Events.PLAYER_ACTIVE_TACTICS_UPDATED, "QTS.TacticsUpdated")

	-- Slash commands
	LibSlash.RegisterSlashCmd("QTS", function(args) QTS.SlashHandler(args) end)

	-- Default to NOT trying to maintain the tome tactic
	if (QTS.KeepTactic == nil) then QTS.KeepTactic = false end
	
	-- Make sure the window isn't showing initially
	WindowSetShowing(QTS.Window, false)

	-- How many tactic slots do we have now?
	QTS.CountTactics()
	QTS.getCurrentTomeTactic()

	-- Pretty
	DataUtils.SetListRowAlternatingTints(QTS.Window .. "List", QTS.visibleRows)
end

function QTS.ChangeSorting()
	local newSortColumn = WindowGetId(SystemData.ActiveWindow.name)
	-- Ignore the first column (it's just the icon)
	if (newSortColumn > 1) then
		-- Clear the old column sort direction
		ClearSortArrows(QTS.sortColumnName)
		-- If we clicked the current column, reverse order
		if (newSortColumn == QTS.sortColumnNum) then QTS.reverseDisplayOrder = not QTS.reverseDisplayOrder
		else QTS.reverseDisplayOrder = false end
		-- Store the new column name and number
		QTS.sortColumnName = SystemData.ActiveWindow.name
		QTS.sortColumnNum = newSortColumn
		-- Display our new sort direction
		ShowSortArrow(QTS.sortColumnName)
		-- Redraw it
		QTS.DisplayData()
	end
end

function QTS.CreateDisplayData()
	-- Populate the display table
	--/script DEBUG(towstring(tostring(GetActiveTactics()))
	QTS.listDisplayData = {}
	local currentTactics = GetActiveTactics()
	local myCarreerSlots = GetNumTacticsSlots()[GameData.TacticType.CAREER]
	for slotNum, tactic in pairs (QTS.TacticData) do
		-- if (not grep(currentTactics, tactic.id)) or myCarreerSlots == 1 then
			-- TextLogAddEntry("Chat", SystemData.ChatLogFilters.SCENARIO, tactic.name)
			local item = QTS.TacticData[slotNum]
			item.slotNum = slotNum
			table.insert(QTS.listDisplayData, item)
		-- end
	end
end

function QTS.DisplayData()
	-- Sort the data
	--TextLogAddEntry("Chat", SystemData.ChatLogFilters.SCENARIO, L"Displaying data")
	local comparator = QTS.sortHeaderData[QTS.sortColumnNum].sortFunc
	table.sort(QTS.listDisplayData, comparator)
	local sortOrder = {}
	for index,_ in pairs(QTS.listDisplayData) do
		if (QTS.reverseDisplayOrder) then
			table.insert(sortOrder, 1, index)
		else
			table.insert(sortOrder, index)
		end
		--TextLogAddEntry("Chat", SystemData.ChatLogFilters.SCENARIO, L"adding a tactic")
	end
	-- Note that this function MUST be called for it to show anything at all
	ListBoxSetDisplayOrder(QTS.Window .. "List", sortOrder)
end

function QTS.SlashHandler(args)
	local opt, val = args:match("([a-z0-9]+)[ ]?(.*)")
	if opt then opt = string.lower(opt) end

	if (opt == "keeptactic") then
		if (val == "") then
			if QTS.KeepTactic then print ("keeptactic is currently on.")
			else print ("keeptactic is currently off.") end
		elseif (val == "on") then
			QTS.KeepTactic = true
			QTS.getCurrentTomeTactic()
			print("keeptactic set to on.")
		elseif (val == "off") then
			QTS.KeepTactic = false
			print("keeptactic set to off.")
		else
			print("Valid options for keeptactic are on or off.")
		end
	else
		print("Valid options for /qts:")
		print("keeptactic [on|off] - sets whether QuickTacticSwitch will attempt to keep the same tome tactic on all tactic sets.")
	end
end

-- Calculate how many tactic buttons this character has
function QTS.CountTactics()
	QTS.TacticButtonTypes = {}
	local mySlots = GetNumTacticsSlots()
	for n = GameData.TacticType.FIRST, GameData.TacticType.NUM_TYPES, 1 do
		local count = mySlots[n]
		for b = 1, count, 1 do
			table.insert(QTS.TacticButtonTypes, n)
		end
	end
end

function QTS.TacticsUpdated()
	-- Save new tome tome tactic
	QTS.getCurrentTomeTactic()
	-- Update the tactics showing in our window
	if (WindowGetShowing(QTS.Window)) then
		QTS.CreateDisplayData()
		QTS.DisplayData()
	end
end

-- Get what the current tome tactic is
function QTS.getCurrentTomeTactic()
	local tactics = GetActiveTactics()
	for _, tacticId in pairs(tactics) do
		local abilityData = GetAbilityData(tacticId)
		if (abilityData.tacticType == GameData.TacticType.TOME) then
			QTS.CurrentTactic = tacticId
		end
	end
end

function QTS.Refresh(tacticType)
	-- Adjust some window stuff depending on the tactic type
	if (tacticType == GameData.TacticType.CAREER) then
		LabelSetText(QTS.Window .. "TitleBarText", L"Career Tactics")
		ButtonSetText(QTS.Window .. "EnemySort", L"Type")
	elseif (tacticType == GameData.TacticType.RENOWN) then 
		LabelSetText(QTS.Window .. "TitleBarText", L"Renown Tactics")
		ButtonSetText(QTS.Window .. "EnemySort", L"Type")
	elseif (tacticType == GameData.TacticType.TOME) then 
		LabelSetText(QTS.Window .. "TitleBarText", L"Tome Tactics")
		ButtonSetText(QTS.Window .. "EnemySort", L"Enemy")
	end
	-- Gather information about what tactics we have and prepare it for display
	QTS.TacticData = {}
	local myTactics = GetAbilityTable(GameData.AbilityType.TACTIC)
	for n, tactic in pairs(myTactics) do
		-- Add it to the list if it's the right tactic type
		if (tactic.tacticType == tacticType) then
			local item = {}
			--TextLogAddEntry("Chat", SystemData.ChatLogFilters.SCENARIO, tactic.name)
			if (tacticType == GameData.TacticType.TOME) then
				-- For tome tactics display the monster it affects
				local description = GetAbilityDesc(n)
				item.Enemy = wstring.match(description, L"enemy (.+) monster")
			else
				-- For other tactic types display the spec line
				item.Enemy = DataUtils.GetAbilitySpecLine(tactic)
			end
			item.Name = tactic.name
			item.id = n
			item.icon = tactic.iconNum
			table.insert(QTS.TacticData, item)
		end
	end
	QTS.CreateDisplayData()
	QTS.DisplayData()
end

function QTS.PopulateDisplay()
	-- Draw the icons
	for row, index in pairs (QuickTacticSwitchWindowList.PopulatorIndices) do
		local rowName = QTS.Window .. "ListRow" .. row
		local texture,x,y = GetIconData(QTS.listDisplayData[index].icon)
		DynamicImageSetTexture(rowName .. "TacticImage", texture, x, y)
	end
end

function GetTacticIndex(window)
	local rownumber = WindowGetId(window)
	local dataIndex = ListBoxGetDataIndex(QTS.Window .. "List", rownumber)
	if (dataIndex ~= nil) then
		local tacticIndex = QTS.listDisplayData[dataIndex].slotNum
		return tacticIndex
	end
end

function QTS.MouseOver()
	-- Tooltip!
	local tacticIndex = GetTacticIndex(SystemData.ActiveWindow.name)
	local abilityData = GetAbilityData(QTS.TacticData[tacticIndex].id)
	Tooltips.CreateAbilityTooltip(abilityData, SystemData.ActiveWindow.name, nil, L"Left click to set tactic.")
end

function QTS.OnClose()
	WindowSetShowing(QTS.Window, false)
end

function QTS.SetTactic()
	-- A tactic has been selected, load it up.
	local tacticIndex = GetTacticIndex(SystemData.ActiveWindow.name)
	local newTactic = QTS.TacticData[tacticIndex].id
	TacticsEditor.ExternalAddTactic(newTactic)
	local abilityData = GetAbilityData(newTactic)
	if (abilityData.tacticType == GameData.TacticType.TOME) then QTS.CurrentTactic = newTactic end
	WindowSetShowing(QTS.Window, false)
end

-- Hooks
function QTS.TacticRemoved(flags, x, y)
	local button = WindowGetId(SystemData.ActiveWindow.name)
	local tacticType = QTS.TacticButtonTypes[button]
	if (tacticType == GameData.TacticType.TOME) then QTS.CurrentTactic = nil end

	-- pass it on
	QTS.RemoveTacticHook(flags, x, y)

	QTS.Refresh(tacticType)
	-- Only draw the window if we have tactics
	if (table.getn(QTS.TacticData) > 0) then
		WindowSetShowing(QTS.Window, true)
	end
end

function QTS.SetChanged(currentSelection)
	-- Pass it on
	QTS.SetChangedHook(currentSelection)
	-- If we are maintaining the tome tactic and one is set
	if (QTS.KeepTactic and QTS.CurrentTactic ~= nil) then
		-- Get the new set, note that GetActiveTactics does not work in this situation
		local newSet = GetTacticsSet(currentSelection - 1)
		local tomePresent = false
		for index, tacticId in pairs (newSet) do
			if (tacticId) then
				-- Get data on the tactic
				local abilityData = GetAbilityData(tacticId)
				if (abilityData.tacticType == GameData.TacticType.TOME) then
					-- It's a tome tactic!
					tomePresent = true
					if (tacticId ~= QTS.CurrentTactic) then
						-- but not the one we want, swap it.
						SwapActiveTactic(tacticId, QTS.CurrentTactic)
						newSet[index] = QTS.CurrentTactic
					end
				end
			end
		end
		-- No tome tactics, load it up.
		if (not tomePresent) then
			TacticsEditor.ExternalAddTactic(QTS.CurrentTactic)
			table.insert(newSet, QTS.CurrentTactic)
		end
		SaveTacticsSet(currentSelection - 1, newSet)
	end
end