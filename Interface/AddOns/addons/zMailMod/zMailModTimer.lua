--[[
    --> file: zMailModTimer.lua
    
    --> description: managing time, queue, sending mails
    
    --> author: zoog
    --> email: ZooGTheOrc@gmail.com
]]--

MassMailTimer = {}

mmt = MassMailTimer

--------------------[VARIABLES]-------------------------------------------------

mmt.errors = 0
mmt.counter = 0
mmt.totalCounter = 0
mmt.resultCounter = 0
mmt.secondPassed = false
mmt.delay = 0
mmt.OPEN_PAUSE = 0

--------------------[INIT]------------------------------------------------------

-- registering a system event
function mmt.Initialize()
    RegisterEventHandler(SystemData.Events.MAILBOX_RESULTS_UPDATED, "MassMailTimer.CheckMailResult")
end

--------------------[SENDING MAILS]---------------------------------------------

-- initiates sending
function mmt.Send()
    local slotM
    local slotB
    local backpack
    local mail = {}

    if mmt.CanSend() then
        while #zmmd.CURRENT_ITEMS < zmmd.ITEMS_MAX do
            slotM = mmt.GetNextItem()

            if slotM == 0 then
                if #zmmd.CURRENT_ITEMS == 0 then
                    zmmd.JOB = zmmd.IDLE
                    return
                else
                    break
                end
            end

            mmw.QueueScrollToSlot(slotM)
            slotB = zmmd.QUEUE[slotM].slotB
            backpack = zmmd.QUEUE[slotM].backpack
            table.insert(zmmd.CURRENT_ITEMS, {SLOTM = slotM, BACKPACK = backpack, SLOTB = slotB})
        end
        
        if #zmmd.CURRENT_ITEMS < zmmd.ITEMS_MAX then
            for i = #zmmd.CURRENT_ITEMS + 1, zmmd.ITEMS_MAX, 1 do
                table.insert(zmmd.CURRENT_ITEMS, {SLOTM = 0, BACKPACK = 0, SLOTB = 0})
            end
        end

        if mmt.IsRecipientValid(zMailModMassMailToEditBox.Text) then
            mail = mmt.CreateMail()
            mmt.PauseSending()
            mmt.StartCounting()
            mmt.SendMail(mail)
        end
    end
end


-- creates table with all data needed to send a mail
function mmt.CreateMail()
    local items = {}
    local slots = {}
    local backpacks = {}
    local subject = mmt.SetMailSubject(zmmd.CURRENT_ITEMS[1].SLOTB, zmmd.CURRENT_ITEMS[1].BACKPACK)

    for i = 1, zmmd.ITEMS_MAX, 1 do
        if zmmd.CURRENT_ITEMS[i].SLOTB ~= 0 then
            table.insert(items, zmmd.CURRENT_ITEMS[i].SLOTB)
            table.insert(slots, zmmd.CURRENT_ITEMS[i].SLOTM)
            table.insert(backpacks, zmmd.CURRENT_ITEMS[i].BACKPACK)
        end
    end

    local mail =
    {
        id        = 0,
        to        = L"" .. zMailModMassMailToEditBox.Text,
        subject   = subject,
        body      = L"" .. zMailModMassMailMessageBodyEditBox.Text,
        items     = items,
        money     = 0,
        cod       = false,
        slots     = slots,
        backpacks = backpacks,
    }
    
    return mail
end


-- sends mail
function mmt.SendMail (mail)
    mmw.SaveRecipient(mail.to)
    mmw.QueueRemoveItems(mail.slots)
    SendMailboxCommand(MailWindow.MAILBOX_SEND, GameData.MailboxType.PLAYER, mail.id, mail.to, mail.subject, mail.body, mail.money, mail.items, mail.backpacks, mail.cod)
    PlaySound(GameData.Sound.TOME_CLOSE)
end


-- pauses sending
function mmt.PauseSending()
    zmmd.STATE = zmmd.PAUSED
end


-- resumes sending
function mmt.ResumeSending()
    if zmmd.TIME_TO_WAIT > 0 then
        zmmd.STATE = zmmd.WAITING
    else
        zmmd.STATE = zmmd.READY
    end
end


-- checks if sending is paused
function mmt.IsSendingPaused()
    if zmmd.STATE == zmmd.PAUSED then
        return true
    else
        return false
    end
end


-- checks the result of sending mail
function mmt.CheckMailResult (result, type)
    if zmmd.JOB == zmmd.OPENING and zmmd.STATE == zmmd.INBOX then
        -- ok: mail deleted --> call post-delete function
        if result == 12 then
            mmt.errors = 0
            mmi.MailDeleted(mmi.current.messageID, GameData.MailboxType.PLAYER)
        -- error: server busy --> wait and retry
        elseif result == 1 or result == 13 or result == 14 or result == 15 then
            if mmt.OPEN_PAUSE == 0 then mmt.OPEN_PAUSE = 3 end
--         -- error: server busy --> retry
--         elseif result == 13 or result == 14 or result == 15 then
--             mmt.errors = mmt.errors + 1
--             mmt.CheckErrors()
--             mmi.Open()
        -- error: can't take attachment or there's no attachment to take --> delete mail (check for attachment's presence later)
        elseif result == 16 then
            if EA_Window_Backpack.IsBackpackFull() then
                mmi.InboxStop()
                mmi.MailUpdated()
                zmm.UpdateResults(88)
            else
                mmi.DeleteMail(mmi.current, type)
            end
        else
            zmm.UpdateResults(result)
        end
        
        return
    end
    
    
    if zmmd.JOB == zmmd.OPENING and zmmd.STATE == zmmd.AUCTION then
        -- ok: mail deleted --> call post-delete function
        if result == 12 then
            mmt.errors = 0
            mma.MailDeleted(mma.current.messageID, GameData.MailboxType.AUCTION)
        -- error: server busy --> wait and retry
        elseif result == 1 or result == 13 or result == 14 or result == 15 then
            if mmt.OPEN_PAUSE == 0 then mmt.OPEN_PAUSE = 3 end
--         -- error: server busy --> retry
--         elseif result == 13 or result == 14 or result == 15 then
--             mmt.errors = mmt.errors + 1
--             mmt.CheckErrors()
--             mma.Open()
        -- error: can't take attachment or there's no attachment to take --> delete mail (check for attachment's presence later)
        elseif result == 16 then
            if EA_Window_Backpack.IsBackpackFull() then
                mma.InboxStop()
                mma.MailUpdated()
                zmm.UpdateResults(88)
            else
                mma.DeleteMail(mma.current, type)
            end
        else
            zmm.UpdateResults(result)
        end

        return
    end

    if zmmd.JOB == zmmd.SENDING then
        -- ok: mail sent --> unpause sending if it's paused
        if result == 4 then
            zmmd.ITEMS_SENT = zmmd.ITEMS_SENT + 1
            mmt.errors = 0
            zmmd.CURRENT_ITEMS = {}
            if mmt.IsSendingPaused() then mmt.ResumeSending() end
        -- error: server busy --> pause sending
        elseif result == 1 or result == 87 then
            mmt.QueueReaddItem()
            mmt.CheckErrors(result)
            if zmmd.JOB == zmmd.SENDING then mmt.PauseSending() end
        -- error: too far from the mailbox --> stop sending (actually resume after pause, it will be stopped automatically)
        elseif result == 2 then
            zmmd.JOB = zmmd.IDLE
            if mmt.IsSendingPaused() then mmt.ResumeSending() end
        -- error: server error --> readd item to queue, check number of errors, resume sending
        elseif result == 3 or result == 5 then
            mmt.QueueReaddItem()
            mmt.errors = mmt.errors + 1
            mmt.CheckErrors (result)
            if mmt.IsSendingPaused() then mmt.ResumeSending() end
        -- error: cooldown error (this should never happen) --> readd item to queue, resume sending
        elseif result == 6 then
            mmt.QueueReaddItem()
            if mmt.IsSendingPaused() then mmt.ResumeSending() end
        -- error: bad recipient or not enough money --> stop sending, readd item to queue
        elseif result == 7 or result == 8 then
            zmmd.JOB = zmmd.IDLE
            mmt.QueueReaddItem()
            zmm.UpdateResults(result)
            if mmt.IsSendingPaused() then mmt.ResumeSending() end
        -- local error: no recipient given or recipient is the player
        elseif result == 90 or result == 91 then
            zmmd.JOB = zmmd.IDLE
            zmm.UpdateResults(result)
        elseif result == 98 or result == 99 then
            mmw.QueueDisableScrollUpButton()
            mmw.QueueDisableScrollDownButton()
            zmm.UpdateResults(result)
        else
            zmm.UpdateResults(result)
        end
        
        return
    end
    
    zmm.UpdateResults(result)
end

--------------------[DATA VALIDATION]-------------------------------------------

-- checks if next mail can be sent
function mmt.CanSend()
    if zmmd.JOB == zmmd.SENDING and zmmd.STATE == zmmd.READY then
        return true
    else
        return false
    end
end


-- checks if recipient is valid
function mmt.IsRecipientValid (recipient)
    local result = 0

    if recipient == nil or recipient == "" or recipient == L"" then
        result = 90
    elseif WStringToString(recipient:lower()) == zmmd.PLAYER_NAME:lower() then
        result = 91
    end

    if result ~= 0 then
        mmt.CheckMailResult(result)
        zmmd.JOB = zmmd.IDLE
        return false
    end

    return true
end

--------------------[TIME]------------------------------------------------------

-- starts counting
function mmt.StartCounting()
    zmmd.TIME_TO_WAIT = zmmd.COOLDOWN
    mmt.counter       = zmmd.COOLDOWN
    mmt.totalCounter  = zmmd.TOTAL_TIME
    mmt.resultCounter = zmmd.PAUSE_TIME
end


-- handles passing time
function mmt.OnUpdate (elapsed)
    if zmmd.JOB == zmmd.OPENING then
        if mmt.OPEN_PAUSE == 0 then
            if zmmd.STATE == zmmd.INBOX then
                mmi.Open()
            elseif zmmd.STATE == zmmd.AUCTION then
                mma.Open()
            end
        else
            mmt.secondPassed = mmt.SecondPassed(elapsed)
            
            if mmt.secondPassed then mmt.OPEN_PAUSE = mmt.OPEN_PAUSE - 1 end
        end
    end
    
    if zmmd.STATE == zmmd.WAITING then
        mmt.secondPassed = mmt.SecondPassed(elapsed)
        
        if mmt.secondPassed then
            if mmt.totalCounter > 0 then mmt.totalCounter = mmt.totalCounter - 1 end
        
            if zmmd.JOB == zmmd.SENDING and zmmd.ITEMS_SENT > 0 then
                mmt.CheckMailResult(97)
            end
        
            mmw.UpdateTimeToWait()
        end
        
        if zmmd.TIME_TO_WAIT <= 0 then zmmd.STATE = zmmd.READY end
        if mmt.SentAll() then mmt.FinishSending() end
        
        mmt.Send()
    elseif zmmd.STATE == zmmd.PAUSED then
        mmt.secondPassed = mmt.SecondPassed(elapsed)
        
        if mmt.secondPassed then
            if mmt.counter > 0 then
                mmt.totalCounter = mmt.totalCounter - 1
                mmw.UpdateTimeToWait()
                mmt.CheckMailResult(97)
            elseif mmt.counter == 0 then
                if zmmd.JOB == zmmd.SENDING then
                    mmt.CheckMailResult(87)
                    mmt.UpdatePauseCounter()
                elseif zmmd.JOB == zmmd.IDLE then
                    zmmd.STATE = zmmd.READY
                    mmt.CheckMailResult(96)
                end
            end
        end
    elseif zmmd.STATE == zmmd.READY then
        if zmmd.JOB == zmmd.IDLE and mmt.SentAll() then mmt.FinishSending() end
        mmt.Send()
    end
end


-- emulates passing the time equals to one second
function mmt.SecondPassed(elapsed)
    zmmd.TIME_TO_WAIT = zmmd.TIME_TO_WAIT - elapsed

    if math.ceil(zmmd.TIME_TO_WAIT) ~= mmt.counter then
        if zmmd.TIME_TO_WAIT <= 0 then
            zmmd.TIME_TO_WAIT = 0
            mmt.counter = 0
        else
            mmt.counter = math.ceil(zmmd.TIME_TO_WAIT)
        end
        
        return true
    end
    
    return false
end


-- checks if all items have been sent
function mmt.SentAll()
    if zmmd.ITEMS_TO_SEND == 0 and zmmd.ITEMS_SENT > 0 then
        return true
    else
        return false
    end
end


-- finishes sending
function mmt.FinishSending()
    zmmd.JOB = zmmd.IDLE
    if zmmd.ITEMS_SENT == 1 then
        mmt.CheckMailResult(98)
    else
        mmt.CheckMailResult(99)
    end
    mmw.LoadDefaultSettings()
    mmw.FixSlotsId()
end

--------------------[MISCELLANEOUS]---------------------------------------------

-- finds first item in queue
function mmt.GetNextItem()
    for i=1, zmmd.ITEM_SLOTS, 1 do
        if zmmd.QUEUE[i].slotB ~= 0 and zmmd.QUEUE[i].backpack ~= 0 and zmmd.QUEUE[i].itemData ~= nil and not mmt.IsInCurrent(i) then
            return i
        end
    end

    return 0
end


-- adds item name and item quantity to the subject
function mmt.SetMailSubject (slotB, backpack)
    local subject = L"" .. zMailModMassMailSubjectEditBox.Text
    local data = EA_Window_Backpack.GetItemsFromBackpack(backpack)[slotB]
    
    if subject:len() == 0 then
        subject = data.name
    end
    
    if data.stackCount > 1 then
        subject = data.stackCount .. L"x " .. subject
    end

    return subject
end


-- checks if zMailMod should stop trying to send a mail
function mmt.CheckErrors (result)
    if mmt.errors >= zmmd.ERRORS and zmmd.ERRORS > 0 then
        if zmmd.JOB == zmmd.SENDING then
            zmmd.JOB = zmmd.IDLE
            zmm.UpdateResults(96)
        elseif zmmd.JOB == zmmd.OPENING then
            zmmd.JOB = zmmd.IDLE
            zmmd.STATE = zmmd.READY
            MailWindowTabMessage.OnClose()
            zmm.UpdateResults(95)
        end
        
        return false
    else
        zmm.UpdateResults(result)
        return true
    end
    
    return true
end


-- manages the pause time
function mmt.UpdatePauseCounter()
    if mmt.resultCounter > 0 then
        mmt.resultCounter = mmt.resultCounter - 1
    else
        mmt.errors = mmt.errors + 1
        zmmd.STATE = zmmd.READY
        mmt.Send()
    end
end


-- adds item that's currently being sent back to queue
function mmt.QueueReaddItem()
    if #zmmd.CURRENT_ITEMS == 0 then return end
d(zmmd.CURRENT_ITEMS)
    for i = 1, #zmmd.CURRENT_ITEMS, 1 do
        if zmmd.CURRENT_ITEMS[i].SLOTB ~= 0 and zmmd.CURRENT_ITEMS[i].SLOTM ~= 0 then
            if zmmd.QUEUE[zmmd.CURRENT_ITEMS[i].SLOTM].slotB == 0 then
                d("aaa1")
                mmw.QueueAddItem(zmmd.CURRENT_ITEMS[i].SLOTM, zmmd.CURRENT_ITEMS[i].SLOTB, zmmd.CURRENT_ITEMS[i].BACKPACK)
                d("aaa2")
            end
        end
    end
end


-- checks if given element has been already selected
function mmt.IsInCurrent(x)
    if #zmmd.CURRENT_ITEMS == 0 then return false end

    for k, v in pairs(zmmd.CURRENT_ITEMS) do
        if x == v.SLOTM then return true end
    end

    return false
end