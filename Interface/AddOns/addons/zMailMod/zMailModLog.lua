--[[
    --> file: zMailModLog.lua
    --> file: zMailModLog.xml

    --> description: creating and managing the log window

    --> author: zoog
    --> email: ZooGTheOrc@gmail.com
]]--
--------------------------------------------------------------------------------

zMailModLog = {}

mml = zMailModLog

mml.listData = {}

-- initialization
function mml.Initialize()
    CreateWindow("zMailModLog", false)
    LabelSetText("zMailModLogTitleBarText",  zL["TITLE_LOG"])

    LabelSetText("zMailModLogHeaderSender",  zL["LOG_HEADER_SENDER"])
    LabelSetText("zMailModLogHeaderSubject", GetMailString(StringTables.Mail.LABEL_MAIL_HEADER_SUBJECT):sub(1,-2))
    LabelSetText("zMailModLogHeaderType",    zL["LOG_HEADER_TYPE"])
    LabelSetText("zMailModLogHeaderMoney",   zL["LOG_HEADER_MONEY"])
    LabelSetText("zMailModLogHeaderItem",    zL["LOG_HEADER_ITEM"])
    
    ButtonSetText     ("zMailModLogClearActive", L"R")
    ButtonSetTextColor("zMailModLogClearActive", Button.ButtonState.NORMAL, 84, 0, 6)
    ButtonSetTextColor("zMailModLogClearActive", Button.ButtonState.HIGHLIGHTED, 84, 0, 6)
    ButtonSetTextColor("zMailModLogClearActive", Button.ButtonState.PRESSED, 255, 137, 19)
    ButtonSetTextColor("zMailModLogClearActive", Button.ButtonState.PRESSED_HIGHLIGHTED, 255, 137, 19)
    mml.UpdateList()
end


-- shows and hides the log window
function mml.ToggleLogWindow()
    WindowSetShowing("zMailModLog", not WindowGetShowing("zMailModLog"))
end


-- hides the log window
function mml.Hide()
    WindowSetShowing("zMailModLog", false)
end


-- refreshes the log
function mml.UpdateList()
    local order = mml.CreateListDataOrder()

    ListBoxSetDisplayOrder("zMailModLogList", order)
    mml.Populate()
    mml.SetListRowTints()
end


-- creates sorted list for the log
function mml.CreateListDataOrder()
    local listDataOrder = {}

    for row, _ in ipairs(mml.listData) do
        table.insert(listDataOrder, row)
    end

    return listDataOrder
end


-- adds tints for every second row
function mml.SetListRowTints()
	local num = zMailModLogList.numVisibleRows

	local row_mod = 1
	local color = DataUtils.GetAlternatingRowColor(row_mod)
	local targetRowWindow = ""

	for i = 1, num, 1 do
        row_mod = math.mod(i, 2)
        color = DataUtils.GetAlternatingRowColor(row_mod)
        row = "zMailModLogListRow" .. i
        WindowSetTintColor(row .. "Background", color.r, color.g, color.b )
        WindowSetAlpha(row .. "Background", color.a)
    end
end


-- adds entry to the log
function mml.AddEntry(data)
    table.insert(mml.listData, data)
    --mmi.ResetAttachmentsInfo()
    mma.ResetAttachmentsInfo()
    mml.UpdateList()
end


-- populating function
function mml.Populate()
    if zMailModLogList.PopulatorIndices ~= nil then
        for win, id in ipairs(zMailModLogList.PopulatorIndices) do
            local money   = zMailModLog.listData[id].money
            local item    = zMailModLog.listData[id].item
            local icon    = {}
            local data    = {}

            MoneyFrame.FormatMoney ("zMailModLogListRow" .. win .. "Money", money, MoneyFrame.HIDE_EMPTY_WINDOWS)

            LabelSetFont("zMailModLogListRow" .. win .. "MoneyGoldText",   "font_clear_medium", WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)
            LabelSetFont("zMailModLogListRow" .. win .. "MoneySilverText", "font_clear_medium", WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)
            LabelSetFont("zMailModLogListRow" .. win .. "MoneyBrassText",  "font_clear_medium", WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)
            WindowSetDimensions("zMailModLogListRow" .. win .. "Money", 175, 20)
            WindowSetShowing("zMailModLogListRow" .. win .. "ItemData", false)

            data = WStringSplit(towstring(item), L"#")

            if zMailModLog.listData[id] == nil or tonumber(data[1]) == 0 then
                WindowSetShowing("zMailModLogListRow" .. win .. "ItemIcon", false)
                WindowSetShowing("zMailModLogListRow" .. win .. "ItemCount", false)
            else
                icon.texture, icon.x, icon.y = GetIconData(tonumber(data[1]))
                DynamicImageSetTexture("zMailModLogListRow" .. win .. "ItemIconIconBase", icon.texture, icon.x, icon.y)
                WindowSetDimensions("zMailModLogListRow" .. win .. "ItemIconIconBase",18,18)
                WindowSetShowing("zMailModLogListRow" .. win .. "ItemIcon", true)
                if tonumber(data[2]) > 1 then
                    LabelSetText("zMailModLogListRow" .. win .. "ItemCount", L"x " .. data[2])
                    WindowSetShowing("zMailModLogListRow" .. win .. "ItemCount", true)
                else
                    WindowSetShowing("zMailModLogListRow" .. win .. "ItemCount", false)
                end
                
                LabelSetText("zMailModLogListRow" .. win .. "ItemData", data[3] .. L"#" .. data[4])
            end
        end
    end
end


-- creates small tooltip for item icon
function mml.OnItemMouseOver()
    local window  = SystemData.MouseOverWindow.name
    local parent  = WindowGetParent(window)
    local idp     = WindowGetId(parent)
    local id      = ListBoxGetDataIndex("zMailModLogList", idp)
    local text    = LabelGetText("zMailModLogListRow".. id .. "ItemData")
    local data    = WStringSplit(text, L"#")
    local color   = zmm.GetRarityColor(tonumber(data[1]))
    
    Tooltips.CreateCustomTooltip(SystemData.MouseOverWindow.name, "DefaultTooltip")
    Tooltips.SetTooltipText(1, 1, data[2])
    Tooltips.SetTooltipColor(1, 1, color.r, color.g, color.b)
    Tooltips.Finalize()
    Tooltips.AnchorTooltip (Tooltips.ANCHOR_WINDOW_TOP)
end


-- clears whole log
function mml.ClearLog()
    zMailModLog.listData = {}
	mml.UpdateList()
end