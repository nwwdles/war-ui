--[[
    --> file: zMailModSend.lua
    --> file: zMailModSend.xml

    --> description: redrawing and managing Send window

    --> author: zoog
    --> email: ZooGTheOrc@gmail.com
]]--
--------------------------------------------------------------------------------

zMailModSend = {}

mms = zMailModSend

local win = zmmd.WINDOW_SEND

--------------------[INITIALIZATION]--------------------------------------------

-- initialization
function mms.Initialize()
    mms.UpdateLayout()
end


function mms.UpdateLayout()
    if zmmd.LAYOUT_SEND == 1 then
        mms.SetNewLayout()
    else
        mms.SetDefaultLayout()
    end
end

-- changes the appearance of the Send tab
function mms.SetNewLayout()
    local parent   = win
    local sepBot   = parent .. "DisplaySeperatorBottom"
    local postage  = parent .. "Postage"
    local headTo   = parent .. "ToHeader"
    local headSub  = parent .. "SubjectHeader"
    local boxTo    = parent .. "ToEditBox"
    local boxSub   = parent .. "SubjectEditBox"
    local boxMsg   = parent .. "MessageBodyEditBox"
    local boxG     = parent .. "EditBoxGold"
    local boxS     = parent .. "EditBoxSilver"
    local boxB     = parent .. "EditBoxBrass"
    local coinG    = parent .. "GoldCoin"
    local coinS    = parent .. "SilverCoin"
    local coinB    = parent .. "BrassCoin"
    local headAtt  = parent .. "AttachmentHeader"
    local items    = parent .. "AttachmentSlots"
    local headCOD  = parent .. "CODCheckBoxButtonHeader"
    local cod      = parent .. "CODCheckBoxButton"
    local bg       = "zMailModSendBackground"
    local topbg    = "zMailModSendTopBackground"
    local space    = "zMailModSendSpaceAbove"
    local topsep   = "zMailModSendTopSeparator"
    local uTo      = "zMailModSendUnderlineTo"
    local uSub     = "zMailModSendUnderlineSubject"
    local boxGT    = "zMailModSendEditBoxGold"
    local boxST    = "zMailModSendEditBoxSilver"
    local boxBT    = "zMailModSendEditBoxBrass"
    local bgAC     = "zMailModSendBackgroundAutoComplete"
    local boxAC    = "zMailModSendToAutoComplete"
    
    WindowClearAnchors(postage)
    WindowAddAnchor   (postage, "topright", parent, "topright", -10, 15)

    if not DoesWindowExist(topsep) then
        CreateWindowFromTemplate(topsep, "EA_Window_DefaultSeparator", parent)
        WindowAddAnchor         (topsep, "bottomleft", sepBot, "topleft", 0, -165)
        WindowAddAnchor         (topsep, "bottomright", sepBot, "topright", 0, -165)
        WindowSetLayer          (topsep, 1)
    end
    WindowSetShowing(topsep, true)

    if not DoesWindowExist(bg) then
        CreateWindowFromTemplate(bg, "zMailModBackgroundShort", parent)
        WindowAddAnchor         (bg, "topleft", parent, "topleft", 7, 50)
        WindowAddAnchor         (bg, "topright", topsep, "topright", -7, 0)
        WindowSetTintColor      (bg, 217, 191, 157)
    end
    WindowSetShowing(bg, true)

    WindowClearAnchors      (headTo)
    WindowAddAnchor         (headTo, "topleft", parent, "topleft", 15, 65)
    WindowSetDimensions     (headTo, 103, 32)
    mmu.SetLabelFont        (headTo, "font_journal_small_heading")
    LabelSetTextColor       (headTo, 50, 37, 22)

    WindowClearAnchors      (headSub)
    WindowAddAnchor         (headSub, "bottomright", headTo, "topright", 0, 15)
    WindowSetDimensions     (headSub, 103, 32)
    mmu.SetLabelFont        (headSub, "font_journal_small_heading")
    LabelSetTextColor       (headSub, 50, 37, 22)

    if DoesWindowExist(boxAC) then DestroyWindow(boxAC) end
    CreateWindowFromTemplate(boxAC, boxAC .. "New", parent)
    TextEditBoxSetTextColor (boxAC, 255, 255, 255)

    if DoesWindowExist(bgAC) then DestroyWindow(bgAC) end
    CreateWindowFromTemplate(bgAC, bgAC .. "New", parent)
    WindowSetShowing(bgAC, false)

    if DoesWindowExist(boxTo) then DestroyWindow(boxTo) end
    CreateWindowFromTemplate(boxTo, "zMailModSendToEditBox", parent)
    TextEditBoxSetTextColor (boxTo, 0, 16, 32)

    if DoesWindowExist(boxSub) then DestroyWindow(boxSub) end
    CreateWindowFromTemplate(boxSub, "zMailModSendSubjectEditBox", parent)
    TextEditBoxSetTextColor (boxSub, 0, 16, 32)

    if DoesWindowExist(boxMsg) then DestroyWindow(boxMsg) end
    CreateWindowFromTemplate(boxMsg, "zMailModSendMessageEditBox", parent)
    TextEditBoxSetTextColor (boxMsg, 0, 16, 32)

    if not DoesWindowExist(uTo) then
        CreateWindowFromTemplate(uTo, "zMailModUnderline", parent)
        WindowAddAnchor         (uTo, "bottomleft", boxTo, "topleft", -15, -8)
    end
    WindowSetShowing(uTo, true)

    if not DoesWindowExist(uSub) then
        CreateWindowFromTemplate(uSub, "zMailModUnderline", parent)
        WindowAddAnchor         (uSub, "bottomleft", boxSub, "topleft", -15, -8)
    end
    WindowSetShowing(uSub, true)

    if not DoesWindowExist(topbg) then
        CreateWindowFromTemplate(topbg, "EA_FullResizeImage_MetalFill", parent)
        WindowAddAnchor         (topbg, "topleft", topsep, "topleft", 7, 12)
        WindowAddAnchor         (topbg, "topright", sepBot, "bottomright", -7, 12)
        WindowSetLayer          (topbg, 0)
    end
    WindowSetShowing(topbg, true)
    
    if DoesWindowExist(boxG) then DestroyWindow(boxG) end
    CreateWindowFromTemplate(boxG, boxGT, parent)
    TextEditBoxSetText(boxG, L"0")

    if DoesWindowExist(boxS) then DestroyWindow(boxS) end
    CreateWindowFromTemplate(boxS, boxST, parent)
    TextEditBoxSetText(boxS, L"0")

    if DoesWindowExist(boxB) then DestroyWindow(boxB) end
    CreateWindowFromTemplate(boxB, boxBT, parent)
    TextEditBoxSetText(boxB, L"0")

    WindowClearAnchors      (headAtt)
    WindowAddAnchor         (headAtt, "bottomright", topsep, "topright", -238, -6)
    LabelSetTextAlign       (headAtt, "leftcenter")

    WindowClearAnchors      (coinG)
    WindowAddAnchor         (coinG, "topright", headAtt, "topleft", 35, 2)
    WindowSetLayer          (coinG, 1)

    WindowClearAnchors      (coinS)
    WindowAddAnchor         (coinS, "topright", headAtt, "topleft", 107, 2)
    WindowSetLayer          (coinS, 1)

    WindowClearAnchors      (coinB)
    WindowAddAnchor         (coinB, "topright", headAtt, "topleft", 181, 2)
    WindowSetLayer          (coinB, 1)

    WindowClearAnchors      (items)
    WindowAddAnchor         (items, "top", sepBot, "bottom", 0, 7)
    WindowSetScale          (items, 0.75 * InterfaceCore.GetScale())

    WindowClearAnchors      (cod)
    WindowAddAnchor         (cod, "bottomright", items, "bottomleft", 8, -20)

    WindowClearAnchors      (headCOD)
    WindowSetDimensions     (headCOD, 45, 30)
    WindowAddAnchor         (headCOD, "topright", cod, "topleft", 0, -5)

    WindowClearAnchors(boxG)
    WindowAddAnchor   (boxG, "topright", headAtt, "topleft", -20, 0)

    WindowClearAnchors(boxS)
    WindowAddAnchor   (boxS, "topright", headAtt, "topleft", 64, 0)

    WindowClearAnchors(boxB)
    WindowAddAnchor   (boxB, "topright", headAtt, "topleft", 138, 0)

    WindowSetShowing(parent .. "MessageBodyHeader", false)
    WindowSetShowing(parent .. "MoneyInBackpack",   false)
    WindowSetShowing(parent .. "ButtonPlusGold",    false)
    WindowSetShowing(parent .. "ButtonPlusSilver",  false)
    WindowSetShowing(parent .. "ButtonPlusBrass",   false)
    WindowSetShowing(parent .. "ButtonMinusGold",   false)
    WindowSetShowing(parent .. "ButtonMinusSilver", false)
    WindowSetShowing(parent .. "ButtonMinusBrass",  false)
    WindowSetLayer  (parent .. "Background",        0)
end


-- sets back the default looks
function mms.SetDefaultLayout()
    local parent   = win
    local sep      = parent .. "DisplaySeperator"
    local sepMid   = parent .. "DisplaySeperatorMiddle"
    local postage  = parent .. "Postage"
    local headTo   = parent .. "ToHeader"
    local headSub  = parent .. "SubjectHeader"
    local boxTo    = parent .. "ToEditBox"
    local boxSub   = parent .. "SubjectEditBox"
    local boxMsg   = parent .. "MessageBodyEditBox"
    local boxG     = parent .. "EditBoxGold"
    local boxS     = parent .. "EditBoxSilver"
    local boxB     = parent .. "EditBoxBrass"
    local coinG    = parent .. "GoldCoin"
    local coinS    = parent .. "SilverCoin"
    local coinB    = parent .. "BrassCoin"
    local headAtt  = parent .. "AttachmentHeader"
    local bag      = parent .. "MoneyInBackpack"
    local items    = parent .. "AttachmentSlots"
    local headCOD  = parent .. "CODCheckBoxButtonHeader"
    local cod      = parent .. "CODCheckBoxButton"
    local plusG    = parent .. "ButtonPlusGold"
    local plusS    = parent .. "ButtonPlusSilver"
    local plusB    = parent .. "ButtonPlusBrass"
    local minusG   = parent .. "ButtonMinusGold"
    local minusS   = parent .. "ButtonMinusSilver"
    local minusB   = parent .. "ButtonMinusBrass"
    local bg       = "zMailModSendBackground"
    local topbg    = "zMailModSendTopBackground"
    local space    = "zMailModSendSpaceAbove"
    local topsep   = "zMailModSendTopSeparator"
    local uTo      = "zMailModSendUnderlineTo"
    local uSub     = "zMailModSendUnderlineSubject"
    local boxGT    = "zMailModSendEditBoxGoldDefault"
    local boxST    = "zMailModSendEditBoxSilverDefault"
    local boxBT    = "zMailModSendEditBoxBrassDefault"
    local bgAC     = "zMailModSendBackgroundAutoComplete"
    local boxAC    = "zMailModSendToAutoComplete"

    if DoesWindowExist(topsep) then WindowSetShowing(topsep, false) end
    if DoesWindowExist(bg)     then WindowSetShowing(bg    , false) end
    if DoesWindowExist(uTo)    then WindowSetShowing(uTo   , false) end
    if DoesWindowExist(uSub)   then WindowSetShowing(uSub  , false) end
    if DoesWindowExist(topbg)  then WindowSetShowing(topbg , false) end

    WindowClearAnchors (headTo)
    WindowAddAnchor    (headTo, "bottomleft", boxTo, "bottomright", -15, 0)
    WindowSetDimensions(headTo, 150, 32)
    mmu.SetLabelFont   (headTo, "font_journal_text")
    LabelSetTextColor  (headTo, 255, 255, 255)

    WindowClearAnchors (headSub)
    WindowAddAnchor    (headSub, "left", boxSub, "right", -10, 0)
    WindowSetDimensions(headSub, 150, 32)
    mmu.SetLabelFont   (headSub, "font_journal_text")
    LabelSetTextColor  (headSub, 255, 255, 255)

    if DoesWindowExist(boxAC) then DestroyWindow(boxAC) end
    CreateWindowFromTemplate(boxAC, boxAC .. "Default", parent)
    TextEditBoxSetTextColor (boxAC, 255, 255, 255)

    if DoesWindowExist(bgAC) then DestroyWindow(bgAC) end
    CreateWindowFromTemplate(bgAC, bgAC .. "Default", parent)
    WindowSetShowing(bgAC, false)

    if DoesWindowExist(boxTo) then DestroyWindow(boxTo) end
    CreateWindowFromTemplate(boxTo, "zMailModSendToEditBoxDefault", parent)

    if DoesWindowExist(boxSub) then DestroyWindow(boxSub) end
    CreateWindowFromTemplate(boxSub, "zMailModSendSubjectEditBoxDefault", parent)

    if DoesWindowExist(boxMsg) then DestroyWindow(boxMsg) end
    CreateWindowFromTemplate(boxMsg, "zMailModSendMessageEditBoxDefault", parent)

    if DoesWindowExist(boxG) then DestroyWindow(boxG) end
    CreateWindowFromTemplate(boxG, boxGT, parent)
    TextEditBoxSetText(boxG, L"0")

    if DoesWindowExist(boxS) then DestroyWindow(boxS) end
    CreateWindowFromTemplate(boxS, boxST, parent)
    TextEditBoxSetText(boxS, L"0")

    if DoesWindowExist(boxB) then DestroyWindow(boxB) end
    CreateWindowFromTemplate(boxB, boxBT, parent)
    TextEditBoxSetText(boxB, L"0")

    WindowClearAnchors(coinG)
    WindowAddAnchor   (coinG, "bottomright", plusG, "left", 5, -2)
    WindowSetLayer    (coinG, 1)

    WindowClearAnchors(coinS)
    WindowAddAnchor   (coinS, "bottomright", plusS, "left", 5, -2)
    WindowSetLayer    (coinS, 1)

    WindowClearAnchors(coinB)
    WindowAddAnchor   (coinB, "topright", items, "bottomright", 0, -14)
    WindowSetLayer    (coinB, 1)

    WindowClearAnchors(headAtt)
    WindowAddAnchor   (headAtt, "bottomleft", boxMsg, "topright", 0, 24)
    LabelSetTextAlign (headAtt, "rightcenter")

    WindowClearAnchors(postage)
    WindowAddAnchor   (postage, "bottom", sepMid, "top", 0, -2)

    WindowClearAnchors(bag)
    WindowAddAnchor   (bag, "bottom", postage, "top", 0, -2)

    WindowClearAnchors(items)
    WindowAddAnchor   (items, "top", sepMid, "bottom", 0, 4)
    WindowSetScale    (items, 0.85 * InterfaceCore.GetScale())

    WindowClearAnchors(cod)
    WindowAddAnchor   (cod, "bottomright", boxMsg, "topright", -2, 25)

    WindowClearAnchors (headCOD)
    WindowSetDimensions(headCOD, 60, 30)
    WindowAddAnchor    (headCOD, "left", cod, "right", -8, 0)

    WindowClearAnchors(plusG)
    WindowAddAnchor   (plusG, "topright", boxG, "toplfet", 4, 0)
    WindowSetShowing  (plusG, true)

    WindowClearAnchors(plusS)
    WindowAddAnchor   (plusS, "topright", boxS, "toplfet", 4, 0)
    WindowSetShowing  (plusS, true)

    WindowClearAnchors(plusB)
    WindowAddAnchor   (plusB, "topright", boxB, "toplfet", 4, 0)
    WindowSetShowing  (plusB, true)
    
    WindowClearAnchors(minusG)
    WindowAddAnchor   (minusG, "bottomright", boxG, "bottomleft", 4, 0)
    WindowSetShowing  (minusG, true)

    WindowClearAnchors(minusS)
    WindowAddAnchor   (minusS, "bottomright", boxS, "bottomleft", 4, 0)
    WindowSetShowing  (minusS, true)

    WindowClearAnchors(minusB)
    WindowAddAnchor   (minusB, "bottomright", boxB, "bottomleft", 4, 0)
    WindowSetShowing  (minusB, true)

    WindowClearAnchors(boxG)
    WindowAddAnchor   (boxG, "topleft", items, "bottomleft", 0, -5)

    WindowClearAnchors(boxS)
    WindowAddAnchor   (boxS, "top", items, "bottomright", 0, -5)

    WindowClearAnchors(boxB)
    WindowAddAnchor   (boxB, "topleft", coinB, "topright", -28, -6)

    WindowSetShowing(parent .. "MessageBodyHeader", true)
    WindowSetShowing(parent .. "MoneyInBackpack",   true)
end


-- changes the default text for the money header
function mms.OnLButtonUpCOD()
    zmm.hooked.MailWindowSend_LButtonUpCOD()
    local bIsCOD = ButtonGetPressedFlag(win .. "CODCheckBoxButton")
    if not bIsCOD then LabelSetText(win .. "AttachmentHeader", zL["LABEL_MONEY"]) end
end


-- automatically completes recipient's name
-- #TODO: this is just a workaround until there will be a function to select only part of the text in text box
function mms.AutoComplete()
    if zmmd.AUTOCOMPLETE == 1 then
        local text = MailWindowTabSendToEditBox.Text
        local length = text:len()
        local names = mmw.GetNameList(GetFriendsList(), GetGuildMemberData(), GameData.Account.CharacterSlot)
        local num = #names

        LabelSetFont("zMailModDummy", TextEditBoxGetFont(win .. "ToEditBox"), WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)

        if length == 0 then
            TextEditBoxSetText("zMailModSendToAutoComplete", L"")
            WindowSetShowing("zMailModSendBackgroundAutoComplete", false)
            return
        end

        for i, name in ipairs(names) do
            if text:lower() == name:lower():sub(1,length) then
                TextEditBoxSetText("zMailModSendToAutoComplete", text .. name:lower():sub(length + 1))

                if text:lower() == name:lower() then
                    WindowSetShowing("zMailModSendBackgroundAutoComplete", false)
                else
                    local width

                    LabelSetText("zMailModDummy", name:lower():sub(length + 1))

                    width = LabelGetTextDimensions("zMailModDummy")

                    WindowSetDimensions("zMailModSendBackgroundAutoComplete", width + 1, 22)
                    LabelSetText("zMailModDummy", text)

                    width = LabelGetTextDimensions("zMailModDummy") + 7
                    if zmmd.LAYOUT_SEND == 0 then width = width + 1 end

                    WindowClearAnchors("zMailModSendBackgroundAutoComplete")
                    WindowAddAnchor("zMailModSendBackgroundAutoComplete", "topleft", win .. "ToEditBox", "topleft", width, 0)
                    WindowSetShowing("zMailModSendBackgroundAutoComplete", true)
                end

                return
            end
        end

        WindowSetShowing("zMailModSendBackgroundAutoComplete", false)
        TextEditBoxSetText("zMailModSendToAutoComplete", text)
    else
        WindowSetShowing("zMailModSendBackgroundAutoComplete", false)
        TextEditBoxSetText("zMailModSendToAutoComplete", L"")
    end
end


-- shortens longer name (autocompleted) to the length specified by user
function mms.FixComplete()
    if zmmd.AUTOCOMPLETE == 1 then
        TextEditBoxSetText("zMailModSendToAutoComplete", MailWindowTabSendToEditBox.Text)
        WindowSetShowing("zMailModSendBackgroundAutoComplete", false)
    end
end


-- copies autocompleted recipient name to 'To' box
-- #TODO: this function should be really called automatically, when textbox 'to' has no longer focus (but there's no such event yet)
function mms.FixRecipient()
    if zmmd.AUTOCOMPLETE == 1 then
        if TextEditBoxGetText(win .. "ToEditBox") ~= TextEditBoxGetText("zMailModSendToAutoComplete") then
            TextEditBoxSetText(win .. "ToEditBox", TextEditBoxGetText("zMailModSendToAutoComplete"))
            WindowSetShowing("zMailModSendBackgroundAutoComplete", false)
        end
    end
end


-- handles keyboard keys pressed
-- #TODO: this should be improved as Mythic's function is buggy and CursorPos is sometimes incorrect
function mms.OnRawDeviceInput(deviceId, itemId, itemDown)
    if zmmd.AUTOCOMPLETE == 1 then
        if deviceId == zmmd.KEYBOARD_DEVICE_ID and itemDown == 1 then
            local pos = MailWindowTabSendToEditBox.CursorPos
            local len = MailWindowTabSendToEditBox.Text:len()
            if itemId == zmmd.KEY_DELETE then
                if pos == len then
                    mms.FixComplete()
                else
                    mms.AutoComplete()
                end
            elseif itemId == zmmd.KEY_END then
                MailWindowTabSendToEditBox.CursorPos = len
                mms.FixRecipient()
            elseif itemId == zmmd.KEY_HOME then
                MailWindowTabSendToEditBox.CursorPos = 0
            elseif itemId == zmmd.KEY_RIGHT_ARROW then
                if pos == len then
                    local text = zMailModSendToAutoComplete.Text:sub(1, len + 1)
                    TextEditBoxSetText(win .. "ToEditBox", text)
                else
                    MailWindowTabSendToEditBox.CursorPos = MailWindowTabSendToEditBox.CursorPos + 1
                end
            elseif itemId == zmmd.KEY_LEFT_ARROW then
                if pos > 0 then
                    MailWindowTabSendToEditBox.CursorPos = MailWindowTabSendToEditBox.CursorPos - 1
                end
            end
        end
    end
end


-- emulates taborder xml entity
function mms.OnKeyTab()
    mms.FixRecipient()
    WindowAssignFocus(win .. "SubjectEditBox", true)
    TextEditBoxSelectAll(win .. "SubjectEditBox")
end


-- sends the message
function mms.OnLButtonUpSend()
    mms.FixRecipient()
    mms.SaveRecipient()
    mms.SetSubject()
    zmm.hooked.MailWindowSend_LButtonUpSend()
end


-- handles changes of the text in the 'To' edit box
function mms.OnTextChanged()
    mms.FormatRecipient()
    mms.AutoComplete()
    zmm.RolodexCheckInput()
end


-- sets the first letter of the recipient as capital letter and rest as small letters
function mms.FormatRecipient()
    local to    = TextEditBoxGetText(win .. "ToEditBox")
    local len   = to:len()
    if len > 1 then
        local first = to:sub(1,1):upper()
        local rest  = to:sub(2, len):lower()
        TextEditBoxSetText(win .. "ToEditBox", first .. rest)
    elseif len == 1 then
        TextEditBoxSetText(win .. "ToEditBox", to:upper())
    end
end


function mms.SaveRecipient()
    local recipient = MailWindowTabSendToEditBox.Text

    if zmmd.LAST_RECIPIENT == 1 and not mmu.IsEmpty(recipient) then
        zMailModSavedSettings[zmmd.PLAYER_NAME].lastSend = recipient
    end
end


function mms.LoadRecipient()
    if zmmd.LAST_RECIPIENT == 1 and zMailModSavedSettings[zmmd.PLAYER_NAME].lastSend ~= nil then
        TextEditBoxSetText(win .. "ToEditBox", zMailModSavedSettings[zmmd.PLAYER_NAME].lastSend)
    end
end


function mms.SetSubject()
    local subject = L"" .. MailWindowTabSendSubjectEditBox.Text
    local slotB = MailWindowTabSend.attachedInventorySlotID
    local gold = tonumber(MailWindowTabSendEditBoxGold.Text)
    local silver = tonumber(MailWindowTabSendEditBoxSilver.Text)
    local brass = tonumber(MailWindowTabSendEditBoxBrass.Text)
    local money = gold * 10000 + silver * 100 + brass
    local data

    if subject:len() == 0 then
        if slotB ~= nil and slotB > 0 then
            data = DataUtils.GetItems()[slotB]
            subject = data.name

            if data.stackCount > 1 then
                subject = data.stackCount .. L"x " .. subject
            end
        elseif money > 0 and not ButtonGetPressedFlag(win .. "CODCheckBoxButton") then
            if gold > 0 then
                subject = gold .. zL["GOLD"]
            end
            
            if silver > 0 then
                if gold > 0 then subject = subject .. L" " end
                subject = subject .. silver .. zL["SILVER"]
            end
            
            if brass > 0 then
                if gold > 0 or silver > 0 then subject = subject .. L" " end
                subject = subject .. brass .. zL["BRASS"]
            end
        end
    end

    TextEditBoxSetText(win .. "SubjectEditBox", subject)
end


function mms.ClearEntries()
    zmm.hooked.MailWindowSend_ClearEntries()
    mms.LoadRecipient()
end