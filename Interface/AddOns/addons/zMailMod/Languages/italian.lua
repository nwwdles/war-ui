-- Translation by: mufla
zMailModData.LANGUAGES[SystemData.Settings.Language.ITALIAN] =
{
    TITLE_LOG             = L"zMailMod Log",
    TITLE_OPTIONS         = L"zMailMod Opzioni",

    INIT_MESSAGE          = L"zMailMod caricato.",

    LABEL_POSTAGE         = L"Spedizione:",
    LABEL_TIME_TO_WAIT    = L"Tempo d'Attesa:",
    LABEL_MONEY           = L"Quantitativo da Spedire:",

    BUTTON_SELECT_MONEY   = L"Seleziona Monete",
    BUTTON_SELECT_ALL     = L"Seleziona Tutto",
    BUTTON_OPEN_SELECTED  = L"Apri Selezionati",

    MESSAGE               = L"messaggio",
    MESSAGES              = L"messaggi",

    TIME_MINUTE           = L"min",
    TIME_MINUTES          = L"min",
    TIME_HOUR             = L"h",
    TIME_HOURS            = L"h",
    TIME_DAY              = L"giorno",
    TIME_DAYS             = L"giorni",

    GOLD                  = L"o",
    SILVER                = L"a",
    BRASS                 = L"b",
    
    AND                   = L"e",
    BLACK                 = L"nero",

    GENERAL               = L"Generale",
    QUEUE                 = L"Coda",
    LAYOUT                = L"Layout",
    AUTOCOMPLETE          = L"Autocompleta",
    AUTOCOMPLETE_ENABLE   = L"Abilita autocompletamento nomi",
    BACKPACK              = L"Inventario",
    BACKPACK_OPEN         = L"Apri inventario con la finestra di posta",
    BACKPACK_CLOSE        = L"Chiudi inventario con la finestra di posta",
    EXPIRE_TIME           = L"Tempo d'attesa",
    EXPIRE_WARNING        = L"Avvertimento Scadenza (in giorni)",
    LAST_RECIPIENT        = L"Ultimo destinatario",
    LAST_RECIPIENT_SAVE   = L"Memorizza ultimo destinatario",
    INPUT_COMBINATION     = L"Combinazione Input",
    INPUT_KEY             = L"Pulsanti tastiera",
    KEY_CTRL              = L"Ctrl",
    KEY_ALT               = L"Alt",
    KEY_SHIFT             = L"Shift",
    NONE                  = L"nessuno",
    INPUT_MOUSE           = L"Pulsanti mouse",
    LEFT                  = L"Sinistra",
    RIGHT                 = L"Destra",
    QUEUE_TIMERS          = L"Timer",
    QUEUE_COOLDOWN        = L"Tempo aggiuntivo tra le spedizioni",
    QUEUE_RETRIES         = L"Numero di tentativi",
    QUEUE_DELAY           = L"intervallo tra i tentativi",
    DEFAULT               = L"Default",
    MASSMAIL              = L"Invio Multiplo",
    MASSADD_ENABLE        = L"Mostra Bottoni Aggiunta Multipla",

    LOG_HEADER_SENDER     = L"Mittente",
    LOG_HEADER_TYPE       = L"Testo",
    LOG_HEADER_MONEY      = L"Monete",
    LOG_HEADER_ITEM       = L"Oggetto",
    
    TOOLTIP_MONEY         = L"Questo messaggio contiene:",

    TRY_LATER             = L"Riprova pi� tardi.",
    RETRY                 = L"Nuovo tentativo in <<1>> sec",

    AUCTION_COMPLETE      = L"Asta Completa!",

    TEXT_MAIL_RESULT87    = L"Il server di posta non risponde.",
    TEXT_MAIL_RESULT88    = L"Il tuo inventario � pieno.",
    TEXT_MAIL_RESULT89    = L"Ritiro finito.",
    TEXT_MAIL_RESULT90    = L"Devi inserire il destinatario.",
    TEXT_MAIL_RESULT91    = L"Non puoi mandare un messaggio a te stesso.",
    TEXT_MAIL_RESULT92    = L"Non puoi spedire tutti i messaggi.",
    TEXT_MAIL_RESULT93    = L"Ritiro... 1 messaggio mancante.",
    TEXT_MAIL_RESULT94    = L"Ritiro... <<2>> messaggi mancanti.",
    TEXT_MAIL_RESULT95    = L"Ritiro fallito.",
    TEXT_MAIL_RESULT96    = L"Spedizione fallita.",
    TEXT_MAIL_RESULT97    = L"<<1>> di <<2>> oggetti spediti. Tempo d'attesa rimasto: <<3>> sec",
    TEXT_MAIL_RESULT98    = L"1 oggetto spedito con successo.",
    TEXT_MAIL_RESULT99    = L"<<2>> oggetti spediti con successo.",
}