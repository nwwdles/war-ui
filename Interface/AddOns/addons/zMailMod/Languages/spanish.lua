-- Translation by: Arhyslan
zMailModData.LANGUAGES[SystemData.Settings.Language.SPANISH] =
{
    TITLE_LOG             = L"Log zMailMod",
    TITLE_OPTIONS         = L"Opciones zMailMod",

    INIT_MESSAGE          = L"zMailMod cargado.",

    LABEL_POSTAGE         = L"Envio:",
    LABEL_TIME_TO_WAIT    = L"Tiempo de espera:",
    LABEL_MONEY           = L"Cantidad a enviar:",

    BUTTON_SELECT_MONEY   = L"Seleccionar dinero",
    BUTTON_SELECT_ALL     = L"Seleccionar todo",
    BUTTON_OPEN_SELECTED  = L"Abrir seleccionados",

    MESSAGE               = L"mensaje",
    MESSAGES              = L"mensajes",

    TIME_MINUTE           = L"min",
    TIME_MINUTES          = L"min",
    TIME_HOUR             = L"h",
    TIME_HOURS            = L"h",
    TIME_DAY              = L"dia",
    TIME_DAYS             = L"dias",

    GOLD                  = L"o",
    SILVER                = L"p",
    BRASS                 = L"l",

    AND                   = L"y",
    BLACK                 = L"negro",

    GENERAL               = L"General",
    QUEUE                 = L"Cola",
    LAYOUT                = L"Dise�o",
    AUTOCOMPLETE          = L"Autocompletar",
    AUTOCOMPLETE_ENABLE   = L"Activar autocompletar nombres",
    BACKPACK              = L"Inventario",
    BACKPACK_OPEN         = L"Abrir inventario con ventana de correo",
    BACKPACK_CLOSE        = L"Cerrar inventario con ventana de correo",
    EXPIRE_TIME           = L"Expiracion",
    EXPIRE_WARNING        = L"Umbral de aviso (en dias)",
    LAST_RECIPIENT        = L"Ultimo destinatario",
    LAST_RECIPIENT_SAVE   = L"Recordar ultimo destinatario",
    INPUT_COMBINATION     = L"Combinacion",
    INPUT_KEY             = L"Tecla",
    KEY_CTRL              = L"Ctrl",
    KEY_ALT               = L"Alt",
    KEY_SHIFT             = L"Shift",
    NONE                  = L"Nada",
    INPUT_MOUSE           = L"Boton del raton",
    LEFT                  = L"Izquierda",
    RIGHT                 = L"Derecha",
    QUEUE_TIMERS          = L"Temporizadores",
    QUEUE_COOLDOWN        = L"Pausa extra entre envios",
    QUEUE_RETRIES         = L"Numero de reintentos",
    QUEUE_DELAY           = L"Retardo del reintento",
    DEFAULT               = L"Defecto",
    MASSMAIL              = L"MassMail",
    MASSADD_ENABLE        = L"Mostrar botones MassAdd",

    LOG_HEADER_SENDER     = L"Remitente",
    LOG_HEADER_TYPE       = L"Tipo",
    LOG_HEADER_MONEY      = L"Dinero",
    LOG_HEADER_ITEM       = L"Objeto",
    
    TOOLTIP_MONEY         = L"Este mensaje contiene:",

    TRY_LATER             = L"Intentalo mas tarde.",
    RETRY                 = L"Reintentado en <<1>> seg",

    AUCTION_COMPLETE      = L"Subasta Completada!",

    TEXT_MAIL_RESULT87    = L"El servidor de correo no responde.",
    TEXT_MAIL_RESULT88    = L"Tu inventario esta lleno.",
    TEXT_MAIL_RESULT89    = L"Apertura finalizada.",
    TEXT_MAIL_RESULT90    = L"Especifica un destinatario.",
    TEXT_MAIL_RESULT91    = L"No puedes enviarte correo a ti mismo.",
    TEXT_MAIL_RESULT92    = L"No puedes pagar todos los envios.",
    TEXT_MAIL_RESULT93    = L"Abriendo... falta 1 mensaje.",
    TEXT_MAIL_RESULT94    = L"Abriendo... faltan <<2>> mensajes.",
    TEXT_MAIL_RESULT95    = L"Apertura fallida.",
    TEXT_MAIL_RESULT96    = L"Envio fallido.",
    TEXT_MAIL_RESULT97    = L"<<1>> de <<2>> objetos enviados. Esperando: <<3>> seg",
    TEXT_MAIL_RESULT98    = L"1 objeto enviado satisfactoriamente.",
    TEXT_MAIL_RESULT99    = L"<<2>> objetos enviados satisfactoriamente.",
}