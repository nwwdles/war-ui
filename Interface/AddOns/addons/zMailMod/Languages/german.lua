-- Translation by: Ksetrak [Bewacher des Kaos] on Egrimm
zMailModData.LANGUAGES[SystemData.Settings.Language.GERMAN] =
{
    TITLE_LOG             = L"zMailMod Log",
    TITLE_OPTIONS         = L"zMailMod Optionen",

    INIT_MESSAGE          = L"zMailMod geladen.",

    LABEL_POSTAGE         = L"Kosten:",
    LABEL_TIME_TO_WAIT    = L"Wartezeit:",
    LABEL_MONEY           = L"Anhang:",

    BUTTON_SELECT_MONEY   = L"Auswahl Geld",
    BUTTON_SELECT_ALL     = L"Auswahl Alle",
    BUTTON_OPEN_SELECTED  = L"�ffne Auswahl",

    MESSAGE               = L"Brief",
    MESSAGES              = L"Briefe",

    TIME_MINUTE           = L"min",
    TIME_MINUTES          = L"min",
    TIME_HOUR             = L"std",
    TIME_HOURS            = L"std",
    TIME_DAY              = L"Tag",
    TIME_DAYS             = L"Tage",

    GOLD                  = L"g",
    SILVER                = L"s",
    BRASS                 = L"k",

    AND                   = L"und",
    BLACK                 = L"leer",

    GENERAL               = L"General",
    QUEUE                 = L"Warteschlange",
    LAYOUT                = L"Layout",
    AUTOCOMPLETE          = L"Autovervollst�ndigung",
    AUTOCOMPLETE_ENABLE   = L"Autovervollst�ndigung AN",
    BACKPACK              = L"Rucksack",
    BACKPACK_OPEN         = L"�ffne Rucksack mit Postfach",
    BACKPACK_CLOSE        = L"Schlie�e Rucksack mit Postfach",
    EXPIRE_TIME           = L"Zeit l�uft",
    EXPIRE_WARNING        = L"Warnung dauer (in Tagen)",
    LAST_RECIPIENT        = L"Letzter Empf�nger",
    LAST_RECIPIENT_SAVE   = L"Merke den letzten Empf�nger",
    INPUT_COMBINATION     = L"Tasten zum Einf�gen",
    INPUT_KEY             = L"Keyboard",
    KEY_CTRL              = L"Ctrl",
    KEY_ALT               = L"Alt",
    KEY_SHIFT             = L"Shift",
    NONE                  = L"Keiner",
    INPUT_MOUSE           = L"Mausknopf",
    LEFT                  = L"Links",
    RIGHT                 = L"Rechts",
    QUEUE_TIMERS          = L"Zeiten",
    QUEUE_COOLDOWN        = L"Extra Wartezeit zwischen Sendungen",
    QUEUE_RETRIES         = L"Nummer der Wiederholungen",
    QUEUE_DELAY           = L"Wiederholungsverz�gerung",
    DEFAULT               = L"Standard",
    MASSMAIL              = L"MassMail",
    MASSADD_ENABLE        = L"Zeig MassAdd Kn�pfe",

    LOG_HEADER_SENDER     = L"Absender",
    LOG_HEADER_TYPE       = L"Art",
    LOG_HEADER_MONEY      = L"Geld",
    LOG_HEADER_ITEM       = L"Item",

    TOOLTIP_MONEY         = L"Dieser Brief enth�lt:",

    TRY_LATER             = L"Versuch es sp�ter.",
    RETRY                 = L"Neuversuch in <<1>> sek",

    AUCTION_COMPLETE      = L"Auction Complete!",                               -- still to do

    TEXT_MAIL_RESULT87    = L"Mail server antwortet nicht.",
    TEXT_MAIL_RESULT88    = L"Rucksack ist voll.",
    TEXT_MAIL_RESULT89    = L"�ffnung fertig.",
    TEXT_MAIL_RESULT90    = L"Empf�nger angeben.",
    TEXT_MAIL_RESULT91    = L"Brief nicht an sich selbst.",
    TEXT_MAIL_RESULT92    = L"Zuwenig Geld f�r alle Briefe.",
    TEXT_MAIL_RESULT93    = L"�ffne... 1 Brief �brig.",
    TEXT_MAIL_RESULT94    = L"�ffne... <<2>> Briefe �brig.",
    TEXT_MAIL_RESULT95    = L"�ffnung fehlgeschlagen.",
    TEXT_MAIL_RESULT96    = L"Versand fehlgeschlagen.",
    TEXT_MAIL_RESULT97    = L"<<1>> von <<2>> items Versand. Wartezeit : <<3>> sek",
    TEXT_MAIL_RESULT98    = L"1 item erfolgreich Versand.",
    TEXT_MAIL_RESULT99    = L"<<2>> items erfolgreich Versand.",
}