zMailModData.LANGUAGES[SystemData.Settings.Language.ENGLISH] =
{
    TITLE_LOG             = L"zMailMod Log",                                    -- title of the log window
    TITLE_OPTIONS         = L"zMailMod Options",                                -- title of the options window

    INIT_MESSAGE          = L"zMailMod loaded.",

    LABEL_POSTAGE         = L"Postage:",
    LABEL_TIME_TO_WAIT    = L"Time to wait:",
    LABEL_MONEY           = L"Amount to send:",

    BUTTON_SELECT_MONEY   = L"Select money",
    BUTTON_SELECT_ALL     = L"Select all",
    BUTTON_OPEN_SELECTED  = L"Open selected",

    MESSAGE               = L"message",                                         -- 1 message
    MESSAGES              = L"messages",                                        -- 2 messages

    TIME_MINUTE           = L"min",                                             -- 1 miunute
    TIME_MINUTES          = L"min",                                             -- 2 minutes
    TIME_HOUR             = L"h",                                               -- 1 hour
    TIME_HOURS            = L"h",                                               -- 2 hours
    TIME_DAY              = L"day",                                             -- 1 day
    TIME_DAYS             = L"days",                                            -- 2 days

    GOLD                  = L"g",                                               -- abbreviation of 'gold'
    SILVER                = L"s",                                               -- abbreviation of 'silver'
    BRASS                 = L"b",                                               -- abbreviation of 'brass'
    
    AND                   = L"and",
    BLACK                 = L"black",

    GENERAL               = L"General",
    QUEUE                 = L"Queue",
    LAYOUT                = L"Layout",
    AUTOCOMPLETE          = L"Autocomplete",
    AUTOCOMPLETE_ENABLE   = L"Enable names autocompletion",
    BACKPACK              = L"Backpack",
    BACKPACK_OPEN         = L"Open backpack with mail window",
    BACKPACK_CLOSE        = L"Close backpack with mail window",
    EXPIRE_TIME           = L"Expire time",
    EXPIRE_WARNING        = L"Warning treshold (in days)",
    LAST_RECIPIENT        = L"Last recipient",
    LAST_RECIPIENT_SAVE   = L"Remember the last recipient",
    INPUT_COMBINATION     = L"Input combination",
    INPUT_KEY             = L"Keyboard key",
    KEY_CTRL              = L"Ctrl",
    KEY_ALT               = L"Alt",
    KEY_SHIFT             = L"Shift",
    NONE                  = L"None",
    INPUT_MOUSE           = L"Mouse button",
    LEFT                  = L"Left",
    RIGHT                 = L"Right",
    QUEUE_TIMERS          = L"Timers",
    QUEUE_COOLDOWN        = L"Extra cooldown between sending",
    QUEUE_RETRIES         = L"Number of retries",
    QUEUE_DELAY           = L"Retry delay time",
    DEFAULT               = L"Default",
    MASSMAIL              = L"MassMail",
    MASSADD_ENABLE        = L"Show MassAdd buttons",

    LOG_HEADER_SENDER     = L"Sender",
    LOG_HEADER_TYPE       = L"Type",
    LOG_HEADER_MONEY      = L"Money",
    LOG_HEADER_ITEM       = L"Item",
    
    TOOLTIP_MONEY         = L"This message contains:",

    TRY_LATER             = L"Try again later.",                                -- task definitely failed
    RETRY                 = L"Trying again in <<1>> sec",                       -- task failed, but will try again

    AUCTION_COMPLETE      = L"Auction Complete!",                               -- subject of the mail with money from the auction house (not used currently)

    TEXT_MAIL_RESULT87    = L"Mail server is not responding.",
    TEXT_MAIL_RESULT88    = L"Your backpack is full.",
    TEXT_MAIL_RESULT89    = L"Collecting finished.",
    TEXT_MAIL_RESULT90    = L"You need to enter the recipient.",
    TEXT_MAIL_RESULT91    = L"You cannot send a mail to yourself.",
    TEXT_MAIL_RESULT92    = L"You cannot afford to send all mails.",
    TEXT_MAIL_RESULT93    = L"Collecting... 1 message left.",
    TEXT_MAIL_RESULT94    = L"Collecting... <<2>> messages left.",
    TEXT_MAIL_RESULT95    = L"Collecting failed.",
    TEXT_MAIL_RESULT96    = L"Sending failed.",
    TEXT_MAIL_RESULT97    = L"<<1>> of <<2>> items sent. Waiting for cooldown to fade: <<3>> sec",
    TEXT_MAIL_RESULT98    = L"1 item sent succesfully.",
    TEXT_MAIL_RESULT99    = L"<<2>> items sent succesfully.",
}