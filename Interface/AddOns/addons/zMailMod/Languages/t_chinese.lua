-- Translation by: pjebuzpuokprejccakai
zMailModData.LANGUAGES[SystemData.Settings.Language.T_CHINESE] =
{
    TITLE_LOG             = L"zMailMod 日誌",
    TITLE_OPTIONS         = L"zMailMod 選項",

    INIT_MESSAGE          = L"zMailMod 已載入.",

    LABEL_POSTAGE         = L"郵資:",
    LABEL_TIME_TO_WAIT    = L"等候時間:",
    LABEL_MONEY           = L"寄送數量:",

    BUTTON_SELECT_MONEY   = L"選擇金錢",
    BUTTON_SELECT_ALL     = L"全選",
    BUTTON_OPEN_SELECTED  = L"打開已選",

    MESSAGE               = L"信件",
    MESSAGES              = L"信件",

    TIME_MINUTE           = L"分鐘",
    TIME_MINUTES          = L"分鐘",
    TIME_HOUR             = L"小時",
    TIME_HOURS            = L"小時",
    TIME_DAY              = L"天",
    TIME_DAYS             = L"天",

    GOLD                  = L"金",
    SILVER                = L"銀",
    BRASS                 = L"銅",

    AND                   = L"和",
    BLACK                 = L"black",

    GENERAL               = L"常規",
    QUEUE                 = L"佇列",
    LAYOUT                = L"佈局",
    AUTOCOMPLETE          = L"自動完成",
    AUTOCOMPLETE_ENABLE   = L"啟用人名自動完成",
    BACKPACK              = L"背包",
    BACKPACK_OPEN         = L"和郵件窗口一同打開背包",
    BACKPACK_CLOSE        = L"和郵件窗口一同關閉背包",
    EXPIRE_TIME           = L"過期時間",
    EXPIRE_WARNING        = L"警告閾值 (以天數形式)",
    LAST_RECIPIENT        = L"上次的收件人",
    LAST_RECIPIENT_SAVE   = L"記住上次的收件人",
    INPUT_COMBINATION     = L"輸入組合",
    INPUT_KEY             = L"鍵盤按鍵",
    KEY_CTRL              = L"Ctrl鍵",
    KEY_ALT               = L"Alt鍵",
    KEY_SHIFT             = L"Shift鍵",
    NONE                  = L"無",
    INPUT_MOUSE           = L"滑鼠按鍵",
    LEFT                  = L"左鍵",
    RIGHT                 = L"右鍵",
    QUEUE_TIMERS          = L"計時器",
    QUEUE_COOLDOWN        = L"多次發送間的額外冷卻時間",
    QUEUE_RETRIES         = L"重試次數",
    QUEUE_DELAY           = L"重試延遲時間",
    DEFAULT               = L"默認",
    MASSMAIL              = L"群發郵件",
    MASSADD_ENABLE        = L"顯示 批量添加 按鈕",

    LOG_HEADER_SENDER     = L"發件人",
    LOG_HEADER_TYPE       = L"類型",
    LOG_HEADER_MONEY      = L"金錢",
    LOG_HEADER_ITEM       = L"物品",
    
    TOOLTIP_MONEY         = L"此信件包含:",

    TRY_LATER             = L"請稍後再試.",
    RETRY                 = L"<<1>>秒後再次嘗試",

    AUCTION_COMPLETE      = L"拍賣結束!",

    TEXT_MAIL_RESULT87    = L"郵件伺服器沒有回應.",
    TEXT_MAIL_RESULT88    = L"你的背包已滿.",
    TEXT_MAIL_RESULT89    = L"領取完畢.",
    TEXT_MAIL_RESULT90    = L"你必須輸入收件人.",
    TEXT_MAIL_RESULT91    = L"你不能發送郵件給自己.",
    TEXT_MAIL_RESULT92    = L"你負擔不起發送全部郵件的費用.",
    TEXT_MAIL_RESULT93    = L"領取中... 1 信件剩餘.",
    TEXT_MAIL_RESULT94    = L"領取中... <<2>> 信件剩餘.",
    TEXT_MAIL_RESULT95    = L"領取失敗.",
    TEXT_MAIL_RESULT96    = L"發送失敗.",
    TEXT_MAIL_RESULT97    = L"<<1>> 之于 <<2>> 物品寄出. 等待冷卻時間結束: <<3>>秒",
    TEXT_MAIL_RESULT98    = L"1 物品成功寄出.",
    TEXT_MAIL_RESULT99    = L"<<2>> 物品成功寄出.",
}