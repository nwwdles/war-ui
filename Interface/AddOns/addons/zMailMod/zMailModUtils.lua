zMailModUtils = {}

mmu = zMailModUtils

-- returns expire time
function mmu.FormatExpireTime(secs)
    local mins
    local hours
    local days

    if secs < 60 then return L"< 1 " .. zL["TIME_MINUTE"] end

    mins = math.floor(secs / 60)

    if mins == 1 then return mins .. L" " .. zL["TIME_MINUTE"] end
    if mins < 60 then return mins .. L" " .. zL["TIME_MINUTES"] end

    hours = math.floor(secs / 3600)
    mins  = math.floor((secs % 3600) / 60)

    if hours == 1 then
        if mins == 0 then return hours .. L" " .. zL["TIME_HOUR"] end
        if mins == 1 then return hours .. L" " .. zL["TIME_HOUR"] .. L" " .. mins .. L" " .. zL["TIME_MINUTE"] end
        return hours .. L" " .. zL["TIME_HOUR"] .. L" " .. mins .. L" " .. zL["TIME_MINUTES"]
    end

    if hours < 24 then
        if mins == 0 then return hours .. L" " .. zL["TIME_HOURS"] end
        if mins == 1 then return hours .. L" " .. zL["TIME_HOURS"] .. L" " .. mins .. L" " .. zL["TIME_MINUTE"] end
        return hours .. L" " .. zL["TIME_HOURS"] .. L" " .. mins .. L" " .. zL["TIME_MINUTES"]
    end

    days  = math.floor(secs / 86400)
    hours = math.floor((secs % 86400) / 3600)

    if days == 1 then
        if hours == 0 then return days .. L" " .. zL["TIME_DAY"] end
        if hours == 1 then return days .. L" " .. zL["TIME_DAY"] .. L" " .. hours .. L" " .. zL["TIME_HOUR"] end
        return days .. L" " .. zL["TIME_DAY"] .. L" " .. hours .. L" " .. zL["TIME_HOURS"]
    end

    return days .. L" " .. zL["TIME_DAYS"]
end


-- shortcut for setting label text color
function mmu.SetLabelColor(label, color)
    LabelSetTextColor(label, color.r, color.g, color.b)
end


-- shortcut for setting tint and alpha for item icon
function mmu.SetIconTint(icon, color)
    WindowSetTintColor(icon, color.r, color.g, color.b)
    WindowSetAlpha    (icon, color.a)
end


-- shortcut for setting label's font
function mmu.SetLabelFont(label, font)
    LabelSetFont(label, font, WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)
end


-- checks if string or wstring is empty
function mmu.IsEmpty(s)
    if s == nil or s == "" or s == L"" then
        return true
    else
        return false
    end
end


-- function used for debugging
function mmu.debug(v)
    if zmmd.DEBUG == 1 then d(v) end
end