--[[
    --> zMailModOptions.lua
    --> zMailModOptions.xml
    
    --> managing options window
    
    author: zoog
]]--
--------------------------------------------------------------------------------

zMailModOptions = {}

mmo = zMailModOptions

mmo.AUTOCOMPLETE   = zmmd.DEFAULT.AUTOCOMPLETE
mmo.BAGS_OPEN      = zmmd.DEFAULT.BAGS_OPEN
mmo.BAGS_CLOSE     = zmmd.DEFAULT.BAGS_CLOSE
mmo.EXPIRE_WARNING = zmmd.DEFAULT.EXPIRE_WARNING
mmo.KEY            = zmmd.DEFAULT.KEY
mmo.MOUSE          = zmmd.DEFAULT.MOUSE
mmo.ERRORS         = zmmd.DEFAULT.ERRORS
mmo.PAUSE_TIME     = zmmd.DEFAULT.PAUSE_TIME
mmo.EXTRA_COOLDOWN = zmmd.DEFAULT.EXTRA_COOLDOWN
mmo.LAYOUT_INBOX   = zmmd.DEFAULT.LAYOUT_INBOX
mmo.LAYOUT_SEND    = zmmd.DEFAULT.LAYOUT_SEND
mmo.LAYOUT_MESSAGE = zmmd.DEFAULT.LAYOUT_MESSAGE
mmo.MASSADD_ENABLE = zmmd.DEFAULT.MASSADD_ENABLE
mmo.LAST_RECIPIENT = zmmd.DEFAULT.LAST_RECIPIENT

local win = zmmd.WINDOW_OPTIONS

local lstGen = win .. "ListGeneral"
local lstQue = win .. "ListQueue"
local lstLay = win .. "ListLayout"

local winGen = win .. "SettingsGeneral"
local winQue = win .. "SettingsQueue"
local winLay = win .. "SettingsLayout"

local abs = math.abs

--------------------[INITIALIZATION]--------------------------------------------

-- initialization
function mmo.Initialize()
    CreateWindow(win, false)

    LabelSetText (win .. "TitleBarText", zL["TITLE_OPTIONS"])
    ButtonSetText(win .. "SaveButton",   GetString(StringTables.Default.LABEL_SAVE))
    
    LabelSetText(winGen .. "Autocomplete"           , zL["AUTOCOMPLETE"])
    LabelSetText(winGen .. "AutocompleteEnableLabel", zL["AUTOCOMPLETE_ENABLE"])
    LabelSetText(winGen .. "Backpack"               , zL["BACKPACK"])
    LabelSetText(winGen .. "BackpackOpenLabel"      , zL["BACKPACK_OPEN"])
    LabelSetText(winGen .. "BackpackCloseLabel"     , zL["BACKPACK_CLOSE"])
    LabelSetText(winGen .. "Expire"                 , zL["EXPIRE_TIME"])
    LabelSetText(winGen .. "ExpireWarningLabel"     , zL["EXPIRE_WARNING"])
    LabelSetText(winGen .. "Recipient"              , zL["LAST_RECIPIENT"])
    LabelSetText(winGen .. "RecipientSaveLabel"     , zL["LAST_RECIPIENT_SAVE"])

    LabelSetText(winQue .. "Input"                  , zL["INPUT_COMBINATION"])
    LabelSetText(winQue .. "InputKey"               , zL["INPUT_KEY"])
    LabelSetText(winQue .. "InputKeyCtrlLabel"      , zL["KEY_CTRL"])
    LabelSetText(winQue .. "InputKeyAltLabel"       , zL["KEY_ALT"])
    LabelSetText(winQue .. "InputKeyShiftLabel"     , zL["KEY_SHIFT"])
    LabelSetText(winQue .. "InputKeyNoneLabel"      , zL["NONE"])
    LabelSetText(winQue .. "InputMouse"             , zL["INPUT_MOUSE"])
    LabelSetText(winQue .. "InputMouseLeftLabel"    , zL["LEFT"])
    LabelSetText(winQue .. "InputMouseRightLabel"   , zL["RIGHT"])
    LabelSetText(winQue .. "Timers"                 , zL["QUEUE_TIMERS"])
    LabelSetText(winQue .. "TimersCooldownLabel"    , zL["QUEUE_COOLDOWN"])
    LabelSetText(winQue .. "TimersRetriesLabel"     , zL["QUEUE_RETRIES"])
    LabelSetText(winQue .. "TimersDelayLabel"       , zL["QUEUE_DELAY"])

    LabelSetText(winLay .. "Inbox"                  , GetMailString(MailWindow.Tabs[MailWindow.TABS_INBOX].label) .. L" " .. zL["AND"] .. L" " .. GetMailString(MailWindow.Tabs[MailWindow.TABS_AUCTION].label))
    LabelSetText(winLay .. "Inbox1Label"            , towstring(zmmd.NAME))
    LabelSetText(winLay .. "Inbox2Label"            , towstring(zmmd.NAME) .. L" " .. zL["BLACK"])
    LabelSetText(winLay .. "Inbox0Label"            , zL["DEFAULT"])
    LabelSetText(winLay .. "Send"                   , GetMailString(MailWindow.Tabs[MailWindow.TABS_SEND].label))
    LabelSetText(winLay .. "Send1Label"             , towstring(zmmd.NAME))
    LabelSetText(winLay .. "Send0Label"             , zL["DEFAULT"])
    LabelSetText(winLay .. "Message"                , GetMailString(StringTables.Mail.LABEL_MAIL_MESSAGE_WINDOW))
    LabelSetText(winLay .. "Message1Label"          , towstring(zmmd.NAME))
    LabelSetText(winLay .. "Message0Label"          , zL["DEFAULT"])
    LabelSetText(winLay .. "Massmail"               , zL["MASSMAIL"])
    LabelSetText(winLay .. "MassmailMassaddLabel"   , zL["MASSADD_ENABLE"])
    
    mmo.ShowOptions(1)
end


-- hides options window
function mmo.Hide()
    WindowSetShowing(win, false)
end


-- shows and hides options window
function mmo.ToggleOptionsWindow()
    mmo.UpdateSettings()
    WindowSetShowing(win, not WindowGetShowing(win))
end


-- loads settings to the options window
function mmo.UpdateSettings()
    mmo.CopySettings()
    mmo.UpdateAutocomplete()
    mmo.UpdateBackpack()
    mmo.UpdateExpire()
    mmo.UpdateRecipient()
    mmo.UpdateInput()
    mmo.UpdateQueue()
    mmo.UpdateInbox()
    mmo.UpdateSend()
    mmo.UpdateMessage()
    mmo.UpdateMassMail()
end


-- copies global variables to local
function mmo.CopySettings()
    mmo.AUTOCOMPLETE   = zmmd.AUTOCOMPLETE
    mmo.BAGS_OPEN      = zmmd.BAGS_OPEN
    mmo.BAGS_CLOSE     = zmmd.BAGS_CLOSE
    mmo.EXPIRE_WARNING = zmmd.EXPIRE_WARNING
    mmo.KEY            = zmmd.KEY
    mmo.MOUSE          = zmmd.MOUSE
    mmo.ERRORS         = zmmd.ERRORS
    mmo.PAUSE_TIME     = zmmd.PAUSE_TIME
    mmo.EXTRA_COOLDOWN = zmmd.EXTRA_COOLDOWN
    mmo.LAYOUT_INBOX   = zmmd.LAYOUT_INBOX
    mmo.LAYOUT_SEND    = zmmd.LAYOUT_SEND
    mmo.LAYOUT_MESSAGE = zmmd.LAYOUT_MESSAGE
    mmo.MASSADD_ENABLE = zmmd.MASSADD_ENABLE
    mmo.LAST_RECIPIENT = zmmd.LAST_RECIPIENT
end


-- changes color of the 'mouseovered' category
function mmo.MouseOverListLabel()
    LabelSetTextColor(SystemData.MouseOverWindow.name, 255, 204, 102)
end


-- changes color of the 'mouseovered' category back to original
function mmo.MouseOverEndListLabel()
    if LabelGetText(SystemData.MouseOverWindow.name):find(L">") == nil then
        LabelSetTextColor(SystemData.MouseOverWindow.name, 255, 255, 255)
    end
end


-- shows options from the selected category
function mmo.ShowOptions(id)
    if id ~= 1 and id ~= 2 and id ~= 3 then id = WindowGetId(SystemData.ActiveWindow.name) end
    
    if id == 1 then
        LabelSetText     (lstGen, zL["GENERAL"] .. L"  >")
        LabelSetText     (lstQue, zL["QUEUE"])
        LabelSetText     (lstLay, zL["LAYOUT"])
        LabelSetTextColor(lstGen, 255, 204, 102)
        LabelSetTextColor(lstQue, 255, 255, 255)
        LabelSetTextColor(lstLay, 255, 255, 255)
        WindowSetShowing (winGen, true)
        WindowSetShowing (winQue, false)
        WindowSetShowing (winLay, false)
    elseif id == 2 then
        LabelSetText     (lstGen, zL["GENERAL"])
        LabelSetText     (lstQue, zL["QUEUE"] .. L"  >")
        LabelSetText     (lstLay, zL["LAYOUT"])
        LabelSetTextColor(lstGen, 255, 255, 255)
        LabelSetTextColor(lstQue, 255, 204, 102)
        LabelSetTextColor(lstLay, 255, 255, 255)
        WindowSetShowing (winGen, false)
        WindowSetShowing (winQue, true)
        WindowSetShowing (winLay, false)
    elseif id == 3 then
        LabelSetText     (lstGen, zL["GENERAL"])
        LabelSetText     (lstQue, zL["QUEUE"])
        LabelSetText     (lstLay, zL["LAYOUT"] .. L"  >")
        LabelSetTextColor(lstGen, 255, 255, 255)
        LabelSetTextColor(lstQue, 255, 255, 255)
        LabelSetTextColor(lstLay, 255, 204, 102)
        WindowSetShowing (winGen, false)
        WindowSetShowing (winQue, false)
        WindowSetShowing (winLay, true)
    end
end


--------------------[SELECTING OPTIONS BUTTONS]---------------------------------

-- checks/unchecks autocomplete buttons
function mmo.UpdateAutocomplete()
    ButtonSetPressedFlag(winGen .. "AutocompleteEnableButton",  mmo.AUTOCOMPLETE == 1)
end


-- checks/unchecks backpack button
function mmo.UpdateBackpack()
    ButtonSetPressedFlag(winGen .. "BackpackOpenButton", mmo.BAGS_OPEN == 1)
    ButtonSetPressedFlag(winGen .. "BackpackCloseButton", mmo.BAGS_CLOSE == 1)
end


function mmo.UpdateRecipient()
    ButtonSetPressedFlag(winGen .. "RecipientSaveButton", mmo.LAST_RECIPIENT == 1)
end


function mmo.UpdateInput()
    ButtonSetPressedFlag(winQue .. "InputKeyCtrlButton",  mmo.KEY == 8)
    ButtonSetPressedFlag(winQue .. "InputKeyAltButton",   mmo.KEY == 32)
    ButtonSetPressedFlag(winQue .. "InputKeyShiftButton", mmo.KEY == 4)
    ButtonSetPressedFlag(winQue .. "InputKeyNoneButton",  mmo.KEY == 0)
    ButtonSetPressedFlag(winQue .. "InputMouseLeftButton",  mmo.MOUSE == 1)
    ButtonSetPressedFlag(winQue .. "InputMouseRightButton", mmo.MOUSE == 2)
end


function mmo.UpdateInbox()
    ButtonSetPressedFlag(winLay .. "Inbox1Button", mmo.LAYOUT_INBOX == 1)
    ButtonSetPressedFlag(winLay .. "Inbox2Button", mmo.LAYOUT_INBOX == 2)
    ButtonSetPressedFlag(winLay .. "Inbox0Button", mmo.LAYOUT_INBOX == 0)
end


function mmo.UpdateSend()
    ButtonSetPressedFlag(winLay .. "Send1Button", mmo.LAYOUT_SEND == 1)
    ButtonSetPressedFlag(winLay .. "Send0Button", mmo.LAYOUT_SEND == 0)
end


function mmo.UpdateMessage()
    ButtonSetPressedFlag(winLay .. "Message1Button", mmo.LAYOUT_MESSAGE == 1)
    ButtonSetPressedFlag(winLay .. "Message0Button", mmo.LAYOUT_MESSAGE == 0)
end


function mmo.UpdateMassMail()
    ButtonSetPressedFlag(winLay .. "MassmailMassaddButton", mmo.MASSADD_ENABLE == 1)
end


function mmo.UpdateExpire()
    TextEditBoxSetText(winGen .. "ExpireWarningButton", L"" .. mmo.EXPIRE_WARNING)
end


function mmo.UpdateQueue()
    TextEditBoxSetText(winQue .. "TimersCooldownButton", L"" .. mmo.EXTRA_COOLDOWN)
    TextEditBoxSetText(winQue .. "TimersRetriesButton", L"" .. mmo.ERRORS)
    TextEditBoxSetText(winQue .. "TimersDelayButton", L"" .. mmo.PAUSE_TIME)
end

--------------------[INPUT]-----------------------------------------------------

function mmo.SelectOption()
    local parent = WindowGetParent(SystemData.ActiveWindow.name)
    local id = WindowGetId(parent)
    local _, _, sec  = parent:find("^zMailModOptionsSettings%u%l+(%u%l+)")
    local cat = WindowGetId(WindowGetParent(parent) .. sec)

    if cat == 11 then mmo.SelectAutocomplete(id)
    elseif cat == 12 then mmo.SelectBackpack(id)
    elseif cat == 13 then mmo.SetFocus(parent .. "Button")
    elseif cat == 14 then mmo.SelectRecipient(id)
    elseif cat == 21 then mmo.SelectInput(id)
    elseif cat == 22 then mmo.SetFocus(parent .. "Button")
    elseif cat == 31 then mmo.SelectInbox(id)
    elseif cat == 32 then mmo.SelectSend(id)
    elseif cat == 33 then mmo.SelectMessage(id)
    elseif cat == 34 then mmo.SelectMassMail(id)
    end
end


function mmo.OnTextChanged()
    local parent = WindowGetParent(SystemData.ActiveWindow.name)
    local id = WindowGetId(parent)
    local _, _, sec  = parent:find("^zMailModOptionsSettings%u%l+(%u%l+)")
    local cat = WindowGetId(WindowGetParent(parent) .. sec)

    if cat == 13 then mmo.SelectExpire(id)
    elseif cat == 22 then mmo.SelectQueue(id)
    end
end


function mmo.SetFocus(box)
    WindowAssignFocus(box, true)
    TextEditBoxSelectAll(box)
end


function mmo.SelectAutocomplete(id)
    mmo.AUTOCOMPLETE = abs(mmo.AUTOCOMPLETE - 1)
    mmo.UpdateAutocomplete()
end


function mmo.SelectBackpack(id)
    if id == 1 then
        mmo.BAGS_OPEN = abs(mmo.BAGS_OPEN - 1)
    elseif id == 2 then
        mmo.BAGS_CLOSE = abs(mmo.BAGS_CLOSE - 1)
    end
    mmo.UpdateBackpack()
end


function mmo.SelectRecipient(id)
    mmo.LAST_RECIPIENT = abs(mmo.LAST_RECIPIENT - 1)
    mmo.UpdateRecipient()
end


function mmo.SelectInput(id)
    if id == 8 or id == 32 or id == 4 or id == 0 then
        mmo.KEY = id
    elseif id == 1 or id == 2 then
        mmo.MOUSE = id
    end
    mmo.UpdateInput()
end


function mmo.SelectInbox(id)
    mmo.LAYOUT_INBOX = id
    mmo.UpdateInbox()
end


function mmo.SelectSend(id)
    mmo.LAYOUT_SEND = id
    mmo.UpdateSend()
end


function mmo.SelectMessage(id)
    mmo.LAYOUT_MESSAGE = id
    mmo.UpdateMessage()
end


function mmo.SelectMassMail(id)
    mmo.MASSADD_ENABLE = abs(mmo.MASSADD_ENABLE - 1)
    mmo.UpdateMassMail()
end


function mmo.SelectExpire(id)
    mmo.EXPIRE_WARNING = TextEditBoxGetText(SystemData.ActiveWindow.name)
    mmo.UpdateExpire()
end


function mmo.SelectQueue(id)
    local value = TextEditBoxGetText(SystemData.ActiveWindow.name)
    
    if value:len() == 2 and value:sub(1,1) == L"0" then
        value = value:sub(2)
    end
    
    if id ~= 2 and value == L"0" then
        value = 1
    end
    
    if id == 1 then
        mmo.EXTRA_COOLDOWN = value
    elseif id == 2 then
        mmo.ERRORS = value
    elseif id == 3 then
        mmo.PAUSE_TIME = value
    end

    mmo.UpdateQueue()
end

--------------------[SAVE]------------------------------------------------------

-- saves and updates the settings
function mmo.Save()
    mmo.ValidateSettings()
    
    zmmd.AUTOCOMPLETE   = mmo.AUTOCOMPLETE
    zmmd.BAGS_OPEN      = mmo.BAGS_OPEN
    zmmd.BAGS_CLOSE     = mmo.BAGS_CLOSE
    zmmd.EXPIRE_WARNING = mmo.EXPIRE_WARNING
    zmmd.KEY            = mmo.KEY
    zmmd.MOUSE          = mmo.MOUSE
    zmmd.ERRORS         = mmo.ERRORS
    zmmd.PAUSE_TIME     = mmo.PAUSE_TIME
    zmmd.EXTRA_COOLDOWN = mmo.EXTRA_COOLDOWN
    zmmd.LAYOUT_INBOX   = mmo.LAYOUT_INBOX
    zmmd.LAYOUT_SEND    = mmo.LAYOUT_SEND
    zmmd.LAYOUT_MESSAGE = mmo.LAYOUT_MESSAGE
    zmmd.MASSADD_ENABLE = mmo.MASSADD_ENABLE
    zmmd.LAST_RECIPIENT = mmo.LAST_RECIPIENT

    zmm.SaveSettings()
    zmm.UpdateLayout("Inbox")
    zmm.UpdateLayout("Auction")
    mms.UpdateLayout()
    mmv.UpdateLayout()
    mmi.Populate()
    mma.Populate()
    mmw.ToggleMassAddButtons()
    mmo.Hide()
end


-- checks types and values of certain variables
function mmo.ValidateSettings()
    mmo.ERRORS          = tonumber(mmo.ERRORS)
    mmo.PAUSE_TIME      = tonumber(mmo.PAUSE_TIME)
    mmo.EXTRA_COOLDOWN  = tonumber(mmo.EXTRA_COOLDOWN)
    mmo.EXPIRE_WARNING  = tonumber(mmo.EXPIRE_WARNING)
    
    if mmo.PAUSE_TIME == 0 then mmo.PAUSE_TIME = zmmd.DEFAULT.PAUSE_TIME end
    if mmo.EXTRA_COOLDOWN == 0 then mmo.EXTRA_COOLDOWN = zmmd.DEFAULT.EXTRA_COOLDOWN end
    
    if mmo.LAST_RECIPIENT == 0 then
        zMailModSavedSettings[zmmd.PLAYER_NAME].lastMassMail = nil
        zMailModSavedSettings[zmmd.PLAYER_NAME].lastSend = nil
        TextEditBoxSetText("MailWindowTabSendToEditBox", L"")
        TextEditBoxSetText("zMailModMassMailToEditBox", L"")
    end
end