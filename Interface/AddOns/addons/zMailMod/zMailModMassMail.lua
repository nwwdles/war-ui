--[[
    --> file: zMailModWindow.lua
    --> file: zMailModWindow.xml
    
    --> description: drawing and managing MassMail window
    
    --> author: zoog
    --> email: ZoogTheOrc@gmail.com
]]--
--------------------------------------------------------------------------------

MassMailWindow = {}

mmw = MassMailWindow

--------------------[VARIABLES]-------------------------------------------------

local tab  = zmmd.TAB_MASSMAIL
local win  = zmmd.WINDOW_MASSMAIL

--------------------[INITIALIZATION]--------------------------------------------

-- creates MassMail window
function mmw.Initialize()
     mmw.HideRightSeparator()
     mmw.CreateMassMailTab()
     mmw.CreateMassMailWin()
end


-- creates MassMailTab
function mmw.CreateMassMailTab()
    if not DoesWindowExist(tab) then
        CreateWindow(tab, true)
        WindowSetParent(tab, "MailWindow")
        ButtonSetText(tab, L"MassMail")
    end
end


--create MassMailWin
function mmw.CreateMassMailWin()
    if not DoesWindowExist(win) then
        CreateWindow   (win, false)
        WindowSetParent(win, "MailWindow")
        LabelSetText   (win .. "PostageHeader",     zL["LABEL_POSTAGE"])
        LabelSetText   (win .. "ToHeader",          GetMailString(StringTables.Mail.LABEL_MAIL_HEADER_TO))
        LabelSetText   (win .. "SubjectHeader",     GetMailString(StringTables.Mail.LABEL_MAIL_HEADER_SUBJECT))
        LabelSetText   (win .. "MessageBodyHeader", GetMailString(StringTables.Mail.LABEL_MAIL_HEADER_MESSAGE))
        LabelSetText   (win .. "TimeToWaitHeader",  zL["LABEL_TIME_TO_WAIT"])
        ButtonSetText  (win .. "CommandSendButton", GetMailString(StringTables.Mail.BUTTON_MAIL_SEND))
        WindowSetLayer (win .. "ToEditBox", Window.Layers.SECONDARY)
        WindowSetShowing(win .. "BackgroundAutoComplete", false)
        mmw.CreateItemSlots()
        mmw.CreateCraftingList()
        mmw.CreateRarityList()
        mmw.QueueInitialize()
        mmw.ToggleMassAddButtons()
    end

    WindowSetShowing(win, false)
end


-- hides separator bar below MassMail tab
function mmw.HideRightSeparator()
    if DoesWindowExist("MailWindowTabsSeparatorRight") then WindowSetShowing("MailWindowTabsSeparatorRight", false) end
end


-- creates buttons for items
function mmw.CreateItemSlots()
    local parent = win
    local slot   = nil
    local icon   = {texture = "", x = "0", y = "0"}

    for i=1, zmmd.DEFAULT_SLOTS, 1 do
        slot = parent .. "Slot" .. i
        local anchor = mmw.CalculateSlotAnchorPoints(i)

        CreateWindowFromTemplate(slot, win .. "ItemSlotTemplate", parent)
        WindowSetId(slot, i)
        WindowAddAnchor(slot, anchor.point, anchor.relTo, anchor.relPoint, anchor.offx, anchor.offy)
        icon.texture, icon.x, icon.y = GetIconData(zmmd.EMPTY_SLOT)
        DynamicImageSetTexture(parent .. "Slot" .. i .. "IconBase", icon.texture, icon.x, icon.y)
    end
end


-- shows and hides small buttons
function mmw.ToggleMassAddButtons()
    if zmmd.MASSADD_ENABLE == 1 then
        WindowSetShowing(win .. "RarityButton", true)
        WindowSetShowing(win .. "CraftingButton", true)
    else
        WindowSetShowing(win .. "RarityButton", false)
        WindowSetShowing(win .. "CraftingButton", false)
    end
end


-- creates list of crafting skills
function mmw.CreateCraftingList()
    CreateWindow  (win .. "CraftingList", false)
    ButtonSetText (win .. "CraftingButton", L"+")

    LabelSetText(win .. "CraftingListApothecary",     GetStringFromTable("BackpackStrings", StringTables.Backpack.FILTERS_CRAFTING_APOTHECARY))
    LabelSetText(win .. "CraftingListCultivating",    GetStringFromTable("BackpackStrings", StringTables.Backpack.FILTERS_CRAFTING_CULTIVATING))
    LabelSetText(win .. "CraftingListSalvaging",      GetStringFromTable("BackpackStrings", StringTables.Backpack.FILTERS_CRAFTING_SALVAGING))
    LabelSetText(win .. "CraftingListTalismanMaking", GetStringFromTable("BackpackStrings", StringTables.Backpack.FILTERS_CRAFTING_TALISMAN_MAKING))
    
    WindowSetShowing(win .. "CraftingList", false)
end


-- creates list of items rarities
function mmw.CreateRarityList()
    local rarity
    local color = {}
    
    CreateWindow      (win .. "RarityList", false)
    ButtonSetText     (win .. "RarityButton", L"+")

    rarity = SystemData.ItemRarity.UNCOMMON
    color  = GameDefs.ItemRarity[rarity].color
    LabelSetText(win .. "RarityListUncommon", GameDefs.ItemRarity[rarity].desc)
    LabelSetTextColor(win .. "RarityListUncommon", color.r, color.g, color.b)
    
    rarity = SystemData.ItemRarity.RARE
    color = GameDefs.ItemRarity[rarity].color
    LabelSetText(win .. "RarityListRare", GameDefs.ItemRarity[rarity].desc)
    LabelSetTextColor(win .. "RarityListRare", color.r, color.g, color.b)
    
    rarity = SystemData.ItemRarity.VERY_RARE
    color = GameDefs.ItemRarity[rarity].color
    LabelSetText(win .. "RarityListVeryRare", GameDefs.ItemRarity[rarity].desc)
    LabelSetTextColor(win .. "RarityListVeryRare", color.r, color.g, color.b)
    
    rarity = SystemData.ItemRarity.ARTIFACT
    color = GameDefs.ItemRarity[rarity].color
    LabelSetText(win .. "RarityListArtifact", GameDefs.ItemRarity[rarity].desc)
    LabelSetTextColor(win .. "RarityListArtifact", color.r, color.g, color.b)

    WindowSetShowing(win .. "RarityList", false)
end


-- calculates anchor data for a button with given id
function mmw.CalculateSlotAnchorPoints (id)
    local slot = win .. "Slot"
    local row  = zmmd.SLOTS_IN_ROW
    local anchor = {}

        if id == 1 then
            anchor.offx     = -35
            anchor.offy     = 10
            anchor.point    = "bottomleft"
            anchor.relPoint = "topleft"
            anchor.relTo    = win .. "MessageBodyEditBox"
        elseif math.mod(id - 1, row) == 0 and id > 1 then
            anchor.offx     = 0
            anchor.offy     = 63
            anchor.point    = "topleft"
            anchor.relPoint = "topleft"
            anchor.relTo    = slot .. (id - row)
        else
            anchor.offx     = 8
            anchor.offy     = 0
            anchor.point    = "right"
            anchor.relPoint = "left"
            anchor.relTo    = slot .. (id - 1)
        end

    return anchor
end

--------------------[INPUT HANDLING]--------------------------------------------

-- LMB
function mmw.OnSlotLButtonUp()
    local win = SystemData.ActiveWindow.name
    local slotM = WindowGetId(win)

    mmw.FixRecipient()

    if Cursor.IconOnCursor() then
        local slotB = Cursor.Data.SourceSlot
        if zmmd.QUEUE[slotM].slotB ~= 0 then mmw.QueueRemoveItem(slotM) end
        mmw.QueueAddItem(slotM, slotB, EA_BackpackUtilsMediator.GetCurrentBackpackType())
        Cursor.Clear()
    elseif zmmd.QUEUE[slotM].slotB ~= 0 and EA_Window_Backpack.currentMode == EA_Window_Backpack.VIEW_MODE_INVENTORY then
        local slotB = zmmd.QUEUE[slotM].slotB
        local itemData = zmmd.QUEUE[slotM].itemData
        Cursor.PickUp(Cursor.SOURCE_INVENTORY, slotB, itemData.uniqueID, itemData.iconNum, true)
        mmw.QueueRemoveItem(slotM)
    end
end


-- RMB
function mmw.OnSlotRButtonDown()
    local win   = SystemData.ActiveWindow.name
    local slotM = WindowGetId(win)
    mmw.QueueRemoveItem(slotM)
    mmw.FixRecipient()
end


-- Mouse Wheel
function mmw.OnMouseWheel(x, y, delta, flags)
    if delta > 0 and not mmw.QueueIsScrollUpButtonDisabled() then
        mmw.QueueScrollUp()
    elseif delta < 0 and not mmw.QueueIsScrollDownButtonDisabled() then
        mmw.QueueScrollDown()
    end
end


-- LMB on Send Button
function mmw.OnLButtonUpSendButton()
    if zmmd.JOB == zmmd.IDLE then
        mmt.errors = 0
        mmw.FixRecipient()
        zmmd.JOB = zmmd.SENDING
        zmm.UpdateResults(result)
    end
end

-- Mouse Over an item slot
function mmw.OnSlotMouseOver()
    local slotM = WindowGetId(SystemData.MouseOverWindow.name)
    mmw.ShowItemTooltip(slotM)
end


--------------------[QUEUE]-----------------------------------------------------

-- initializes table for queue
function mmw.QueueInitialize()
    zmmd.QUEUE = {}
    mmw.QueueDisableScrollUpButton()
    mmw.QueueDisableScrollDownButton()
    for i = 1, zmmd.NUM_ROWS, 1 do
        mmw.QueueRowInitialize(i)
    end
end


-- initializes one queue row
function mmw.QueueRowInitialize(num)
    local min = (num - 1) * zmmd.SLOTS_IN_ROW + 1
    local max = num * zmmd.SLOTS_IN_ROW
    
    for i = min, max, 1 do
        zmmd.QUEUE[i] = {slotB = 0, backpack = 0, itemData = nil}
    end
end


--adds item from slotB in backpack to slotM in MassMail queue
function mmw.QueueAddItem(slotM, slotB, backpack)
    if mmw.IsBackpackSlotLocked(slotB, backpack) then return end

    if slotM == 0 then slotM = mmw.QueueGetEmptySlot() end
    if slotM == 0 then mmw.ExpandQueue() end
    if slotM == 0 then slotM = mmw.QueueGetEmptySlot() end

    local itemData = EA_Window_Backpack.GetItemsFromBackpack(backpack)[slotB]

    if mmw.IsValidAttachment(itemData) then
        mmw.QueueScrollToSlot(slotM)
        mmw.LockBackpackSlot(slotB, backpack)
        mmw.SetIconForSlot (slotM, itemData.iconNum)
        mmw.SetSlotStackCount(slotM, itemData.stackCount)
        zmmd.QUEUE[slotM] = {slotB = slotB, backpack = backpack, itemData = itemData}
        zmmd.ITEMS_TO_SEND = zmmd.ITEMS_TO_SEND + 1
    else
        mmw.SetIconForSlot (slotM, zmmd.EMPTY_SLOT)
    end

    mmw.UpdatePostageCost()
    mmw.UpdateTimeToWait()
end


-- removes item from MassMail queue
function mmw.QueueRemoveItem (slotM)
    local slotB = zmmd.QUEUE[slotM].slotB
    local backpack = zmmd.QUEUE[slotM].backpack

    if slotB ~= 0 then
        zmmd.ITEMS_TO_SEND = zmmd.ITEMS_TO_SEND - 1
    end

    zmmd.QUEUE[slotM] = {slotB = 0, itemData = nil}

    if mmw.GetSlotPosition(slotM) == 0 then
        mmw.SetIconForSlot (slotM, zmmd.EMPTY_SLOT)
        mmw.SetSlotStackCount(slotM, 0)
    end

    mmw.UnlockBackpackSlot(slotB, backpack)
    mmw.UpdatePostageCost()
    mmw.UpdateTimeToWait()
end


-- removes items from MassMail queue
function mmw.QueueRemoveItems (slots)
    if #slots == 0 then return end

    for i = 1, #slots, 1 do
        mmw.QueueRemoveItem(slots[i])
    end
end


-- adds one row of slots to the queue
function mmw.ExpandQueue()
    zmmd.ITEM_SLOTS = zmmd.ITEM_SLOTS + zmmd.SLOTS_IN_ROW
    zmmd.NUM_ROWS = zmmd.NUM_ROWS + 1
    mmw.QueueRowInitialize(zmmd.NUM_ROWS)
    mmw.QueueScrollDown()
    if mmw.QueueIsScrollUpButtonDisabled() then mmw.QueueEnableScrollUpButton() end
end


-- reloads all slots
function mmw.ReloadQueue()
    mmw.FixSlotsId()
    mmw.FixSlotsIcons()
end


-- scrolls slots one row up
function mmw.QueueScrollUp()
    if zmmd.OFFSET == 0 then return end
    zmmd.OFFSET = zmmd.OFFSET - 1
    if zmmd.OFFSET == 0 then mmw.QueueDisableScrollUpButton() end
    if zmmd.NUM_ROWS > zmmd.DEFAULT_ROWS then mmw.QueueEnableScrollDownButton() end
    mmw.ReloadQueue()
end


-- scrolls slots one button down
function mmw.QueueScrollDown()
    if zmmd.OFFSET == zmmd.NUM_ROWS - zmmd.DEFAULT_ROWS then return end
    zmmd.OFFSET = zmmd.OFFSET + 1
    if zmmd.OFFSET == zmmd.NUM_ROWS - zmmd.DEFAULT_ROWS then mmw.QueueDisableScrollDownButton() end
    if zmmd.OFFSET > 0 then mmw.QueueEnableScrollUpButton() end
    mmw.ReloadQueue()
end


-- enables UP button
function mmw.QueueEnableScrollUpButton()
    ButtonSetDisabledFlag(win .. "UpArrowButton", false)
    WindowRegisterCoreEventHandler(win .. "UpArrowButton", "OnLButtonUp", "MassMailWindow.QueueScrollUp")
end


-- disables UP button
function mmw.QueueDisableScrollUpButton()
    ButtonSetDisabledFlag(win .. "UpArrowButton", true)
    WindowUnregisterCoreEventHandler(win .. "UpArrowButton", "OnLButtonUp")
end


-- checks if UP button is disabled
function mmw.QueueIsScrollUpButtonDisabled()
    if ButtonGetDisabledFlag(win .. "UpArrowButton") then
        return true
    else
        return false
    end
end

-- enables DOWN button
function mmw.QueueEnableScrollDownButton()
    ButtonSetDisabledFlag(win .. "DownArrowButton", false)
    WindowRegisterCoreEventHandler(win .. "DownArrowButton", "OnLButtonUp", "MassMailWindow.QueueScrollDown")
end


-- disables DOWN button
function mmw.QueueDisableScrollDownButton()
    ButtonSetDisabledFlag(win .. "DownArrowButton", true)
    WindowUnregisterCoreEventHandler(win .. "DownArrowButton", "OnLButtonUp")
end


-- checks if DOWN button is disabled
function mmw.QueueIsScrollDownButtonDisabled()
    if ButtonGetDisabledFlag(win .. "DownArrowButton") then
        return true
    else
        return false
    end
end


-- scrolls buttons until slot given is visible
-- #TODO: add checks of OFFSET value to prevent scrolling too far
function mmw.QueueScrollToSlot(slotM)
    local pos = mmw.GetSlotPosition(slotM)
    while pos ~= 0 do
        if pos == -1 then mmw.QueueScrollUp()
        elseif pos == 1 then mmw.QueueScrollDown()
        end
        pos = mmw.GetSlotPosition(slotM)
    end
end


-- returns first available slot in MassMail queue
function mmw.QueueGetEmptySlot()
    for i=1, zmmd.ITEM_SLOTS, 1 do
        if zmmd.QUEUE[i].itemData == nil then
            return i
        end
    end

    return 0
end


-- adds items of specified crafting skill to the queue
function mmw.AddCraftingItems()
    local id = WindowGetId(SystemData.ActiveWindow.name)
    local data

    for _, backpack in pairs{EA_Window_Backpack.TYPE_INVENTORY, EA_Window_Backpack.TYPE_CRAFTING} do
        for slot = 1, EA_Window_Backpack.numberOfSlots[backpack], 1 do
            data = EA_Window_Backpack.GetItemsFromBackpack(backpack)[slot]

            if not mmw.IsBackpackSlotLocked(slot, backpack) then
                if data ~= nil and data.uniqueID ~= 0 then
                    if DataUtils.IsTradeSkillItem(data, id) then
                        mmw.QueueAddItem(0, slot, backpack)
                    end
                end
            end
        end
    end
end


-- add items of selected rarity to the queue
function mmw.AddRarityItems()
    local rarity = WindowGetId(SystemData.ActiveWindow.name)
    local backpack = EA_Window_Backpack.TYPE_INVENTORY
    local data

    for slot = 1, EA_Window_Backpack.numberOfSlots[backpack], 1 do
		data = EA_Window_Backpack.GetItemsFromBackpack(backpack)[slot]

		if not mmw.IsBackpackSlotLocked(slot, backpack) then
            if data ~= nil and data.uniqueID ~= 0 then
                if data.equipSlot > 0 and data.rarity == rarity then
                    mmw.QueueAddItem(0, slot, backpack)
                end
			end
		end
	end
end

--------------------[LOCKING FUNCTIONS]-----------------------------------------

-- locks a slot
function mmw.LockBackpackSlot (slotB, backpack)
    local bag = math.ceil(slotB/16)
    return EA_Window_Backpack.RequestLockForSlot(slotB, backpack, "EA_Window_BackpackIconViewSection" .. bag .. "Buttons",  {r=0, g=255, b=0})
end


-- unlocks a slot
function mmw.UnlockBackpackSlot (slotB, backpack)
    local bag = math.ceil(slotB/16)
    return EA_Window_Backpack.ReleaseLockForSlot(slotB, backpack, "EA_Window_BackpackIconViewSection" .. bag .. "Buttons")
end


-- checks if a slot is locked
function mmw.IsBackpackSlotLocked (slot, backpack)
    return EA_Window_Backpack.IsSlotLocked(slot, backpack)
end

--------------------[MISCELLANEOUS]---------------------------------------------

-- calculates the button number (1-28) based on given id
function mmw.GetNumberFromId(id)
    return id - zmmd.OFFSET * zmmd.SLOTS_IN_ROW
end


-- calculates id of the button based on its number (1-28)
function mmw.GetIdFromNumber(num)
    return num + zmmd.OFFSET * zmmd.SLOTS_IN_ROW
end


-- show tooltip
function mmw.ShowItemTooltip (slotM)
    if zmmd.QUEUE ~= nil and zmmd.QUEUE[slotM] ~= nil then
        local itemData = zmmd.QUEUE[slotM].itemData
        local slotB = zmmd.QUEUE[slotM].slotB
        local window = SystemData.MouseOverWindow.name

        if itemData ~= nil and slotB ~= 0 then
            Tooltips.CreateItemTooltip(itemData, window, Tooltips.ANCHOR_WINDOW_BOTTOM)
        end
    end
end


-- adds number of items in the stack to the MassMail slot
function mmw.SetSlotStackCount (slotM, stackCount)
    local slot = mmw.GetNumberFromId(slotM)

    if stackCount == 0 or stackCount == 1 then
        ButtonSetText(win .. "Slot" .. slot, L"")
    elseif (stackCount > 1) then
        ButtonSetText(win .. "Slot" .. slot, towstring(stackCount))
    end
end


-- sets an icon for a slot
function mmw.SetIconForSlot (slotM, iconNumber)
    local slot = mmw.GetNumberFromId(slotM)
    local texture, width, height = GetIconData(iconNumber)
    if DoesWindowExist(win .. "Slot" .. slot .. "IconBase") then
        DynamicImageSetTexture(win .. "Slot" .. slot .. "IconBase", texture, width, height)
    end
end


-- sets proper id (depending on current offset) for each button
function mmw.FixSlotsId()
    for i=1, zmmd.DEFAULT_SLOTS, 1 do
        WindowSetId(win .. "Slot" .. i, mmw.GetIdFromNumber(i))
    end
end


-- sets proper (depending on current offset) icon for each button
function mmw.FixSlotsIcons()
    local slotM
    local slotB
    local backpack
    local item

    for i=1, zmmd.DEFAULT_SLOTS, 1 do
        slotM = mmw.GetIdFromNumber(i)
        slotB = zmmd.QUEUE[slotM].slotB
        backpack = zmmd.QUEUE[slotM].backpack

        if slotB > 0 then
            item  = EA_Window_Backpack.GetItemsFromBackpack(backpack)[slotB]
            mmw.SetIconForSlot (slotM, item.iconNum)
            mmw.SetSlotStackCount(slotM, item.stackCount)
        else
            mmw.SetIconForSlot (slotM, zmmd.EMPTY_SLOT)
            mmw.SetSlotStackCount(slotM, 0)
        end
    end

end


-- checks the position of slot (visible, above, below)
function mmw.GetSlotPosition(slotM)
    local row = math.ceil(slotM/zmmd.SLOTS_IN_ROW)
    local min = zmmd.OFFSET + 1
    local max = zmmd.OFFSET + zmmd.DEFAULT_ROWS
    
    if row < min then return -1
    elseif row > max then return 1
    else return 0
    end
end


-- checks if item can be sent
function mmw.IsValidAttachment (itemData)
    if itemData == nil then
        return false
    end

    if itemData.uniqueID == 0 or itemData.boundToPlayer == true then
        if itemData.boundToPlayer == true then mmt.CheckMailResult(9) end
        return false
    else
        mmt.CheckMailResult()
    end

    return true
end


-- updates the postage cost for all items in queue
function mmw.UpdatePostageCost()
    local cost = 0
    local items = 0

    for i=1, zmmd.ITEM_SLOTS, 1 do
        if zmmd.QUEUE[i].slotB ~= 0 and zmmd.QUEUE[i].itemData ~= nil and zmmd.QUEUE[i].itemData.uniqueID ~= 0 then
            cost = cost + zmmd.QUEUE[i].itemData.sellPrice
            items = items + 1
        end
    end
    
    cost = math.ceil(items/zmmd.ITEMS_MAX)*MailWindow.PostageCostBase + math.floor(cost * MailWindow.PostageCostItemMultiplier)
    MoneyFrame.FormatMoney (win .. "PostageFrame", cost, MoneyFrame.SHOW_EMPTY_WINDOWS)
end


-- updates the waiting time
function mmw.UpdateTimeToWait()
    local items = 0
    local secToWait = 0

    for i=1, zmmd.ITEM_SLOTS, 1 do
        if zmmd.QUEUE[i].slotB ~= 0 and zmmd.QUEUE[i].slotB ~= nil and zmmd.QUEUE[i].itemData ~= nil then
            items = items + 1
        end
    end
    
    if items == 0 then
        zmmd.TOTAL_TIME = 0
        LabelSetText(win .. "TimeToWait", TimeUtils.FormatClock(secToWait))
        return
    end
    
    secToWait = (math.ceil(items/zmmd.ITEMS_MAX) - 1)* zmmd.COOLDOWN
    
    if zmmd.JOB == zmmd.IDLE then
        zmmd.TOTAL_TIME = secToWait
        secToWait = TimeUtils.FormatClock(secToWait)
        
        if zmmd.STATE == zmmd.WAITING and zmmd.TIME_TO_WAIT > 0 and zmmd.ITEMS_TO_SEND > 0 then
            secToWait = secToWait .. L" (" .. TimeUtils.FormatClock(zmmd.TIME_TO_WAIT) .. L")"
        end
    elseif zmmd.JOB == zmmd.SENDING then
        if zmmd.ITEMS_TO_SEND > 0 then
            secToWait = secToWait + zmmd.TIME_TO_WAIT
        end
        
        zmmd.TOTAL_TIME = secToWait
        secToWait = TimeUtils.FormatClock(secToWait)
    end
    LabelSetText(win .. "TimeToWait", secToWait)
end


-- clears MassMail window
function mmw.ClearEntries()
    mmw.ClearEditBoxes()
    mmw.ClearQueue()
    mmw.LoadDefaultSettings()
    mmw.QueueInitialize()
    mmw.UpdatePostageCost()
    mmw.UpdateTimeToWait()
    WindowSetShowing(MailWindow.Tabs[MailWindow.TABS_MASSMAIL].window .. "ResultText", false)
    zmmd.JOB = zmmd.IDLE
    zmmd.STATE = zmmd.READY
end


-- sets global variables to default values
function mmw.LoadDefaultSettings()
    zmmd.ITEMS_TO_SEND = 0
    zmmd.ITEMS_SENT    = 0
    zmmd.ITEM_SLOTS    = zmmd.DEFAULT_SLOTS
    zmmd.NUM_ROWS      = zmmd.DEFAULT_ROWS
    zmmd.OFFSET        = 0
    zmmd.COOLDOWN      = zmmd.CD_LONG + zmmd.EXTRA_COOLDOWN
    zmmd.TIME_TO_WAIT  = 0
    zmmd.TOTAL_TIME    = 0
end


-- clears edit boxes
function mmw.ClearEditBoxes()
    TextEditBoxSetText(win .. "ToEditBox", L"")
    TextEditBoxSetText(win .. "ToAutoComplete", L"")
    TextEditBoxSetText(win .. "SubjectEditBox", L"")
    TextEditBoxSetText(win .. "MessageBodyEditBox", L"")
end


-- clears queue
function mmw.ClearQueue()
    local icon = {}

    for i = 1, zmmd.ITEM_SLOTS, 1 do
        local slotB = zmmd.QUEUE[i].slotB
        if slotB ~= nil and slotB > 0 then mmw.UnlockBackpackSlot(slotB, zmmd.QUEUE[i].backpack) end
        zmmd.QUEUE[i] = {slotB = 0, backpack = 0, itemData = nil}
    end
    
    mmw.FixSlotsIcons()
end


-- automatically completes recipient's name
-- #TODO: this is just a workaround until there is a function to select only part of the text in text box
function mmw.AutoComplete()
    if zmmd.AUTOCOMPLETE == 1 then
        local text = zMailModMassMailToEditBox.Text
        local length = text:len()
        local names = mmw.GetNameList(GetFriendsList(), GetGuildMemberData(), GameData.Account.CharacterSlot)
        local num = #names
        
        LabelSetFont("zMailModDummy", TextEditBoxGetFont("zMailModMassMailToEditBox"), WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)

        if length == 0 then
            TextEditBoxSetText("zMailModMassMailToAutoComplete", L"")
            WindowSetShowing(win .. "BackgroundAutoComplete", false)
            return
        end
        
        for i, name in ipairs(names) do
            if text:lower() == name:lower():sub(1,length) then
                TextEditBoxSetText("zMailModMassMailToAutoComplete", text .. name:lower():sub(length + 1))
                
                if text:lower() == name:lower() then
                    WindowSetShowing(win .. "BackgroundAutoComplete", false)
                else
                    local width
                    
                    LabelSetText("zMailModDummy", name:lower():sub(length + 1))
                    
                    width = LabelGetTextDimensions("zMailModDummy")
                    
                    WindowSetDimensions(win .. "BackgroundAutoComplete", width, 22)
                    LabelSetText("zMailModDummy",text)
                    
                    width = LabelGetTextDimensions("zMailModDummy")
                    
                    WindowClearAnchors(win .. "BackgroundAutoComplete")
                    WindowAddAnchor(win .. "BackgroundAutoComplete", "topleft", win .. "ToEditBox", "topleft", width + 8, 0)
                    WindowSetShowing(win .. "BackgroundAutoComplete", true)

                    mmw.SetCooldown()
                end
                
                return
            end
        end

        WindowSetShowing(win .. "BackgroundAutoComplete", false)
        TextEditBoxSetText("zMailModMassMailToAutoComplete", text)
    else
        WindowSetShowing(win .. "BackgroundAutoComplete", false)
        TextEditBoxSetText("zMailModMassMailToAutoComplete", L"")
    end

    mmw.SetCooldown()
end


-- handles keyboard keys pressed
-- #TODO: this should be improved as Mythic's function is buggy and CursorPos is sometimes incorrect
function mmw.OnRawDeviceInput(deviceId, itemId, itemDown)
    if zmmd.AUTOCOMPLETE == 1 then
        if deviceId == zmmd.KEYBOARD_DEVICE_ID and itemDown == 1 then
            local pos = zMailModMassMailToEditBox.CursorPos
            local len = zMailModMassMailToEditBox.Text:len()
            if itemId == zmmd.KEY_DELETE then
                if pos == len then
                    mmw.FixComplete()
                else
                    mmw.AutoComplete()
                end
            elseif itemId == zmmd.KEY_END then
                zMailModMassMailToEditBox.CursorPos = len
                mmw.FixRecipient()
            elseif itemId == zmmd.KEY_HOME then
                zMailModMassMailToEditBox.CursorPos = 0
            elseif itemId == zmmd.KEY_RIGHT_ARROW then
                if pos == len then
                    local text = zMailModMassMailToAutoComplete.Text:sub(1, len + 1)
                    TextEditBoxSetText("zMailModMassMailToEditBox", text)
                else
                    zMailModMassMailToEditBox.CursorPos = zMailModMassMailToEditBox.CursorPos + 1
                end
            elseif itemId == zmmd.KEY_LEFT_ARROW then
                if pos > 0 then
                    zMailModMassMailToEditBox.CursorPos = zMailModMassMailToEditBox.CursorPos - 1
                end
            end
        end
    end
end


-- shortens longer name (autocompleted) to the length specified by user
function mmw.FixComplete()
    if zmmd.AUTOCOMPLETE == 1 then
        TextEditBoxSetText(win .. "ToAutoComplete", zMailModMassMailToEditBox.Text)
        WindowSetShowing(win .. "BackgroundAutoComplete", false)
    end

    mmw.SetCooldown()
end


-- copies autocompleted recipient name to 'To' box
-- #TODO: this function should be really called automatically, when textbox 'to' has no longer focus (but there's no such event yet)
function mmw.FixRecipient()
    if zmmd.AUTOCOMPLETE == 1 then
        if TextEditBoxGetText(win .. "ToEditBox") ~= TextEditBoxGetText(win .. "ToAutoComplete") then
            TextEditBoxSetText(win .. "ToEditBox", TextEditBoxGetText(win .. "ToAutoComplete"))
            WindowSetShowing(win .. "BackgroundAutoComplete", false)
        end
    end

    mmw.SetCooldown()
end


-- sets the delay time (1s for alts and guildmates, 5s for others)
function mmw.SetCooldown()
    local recipient = zMailModMassMailToAutoComplete.Text
    local length = recipient:len()

    local names = mmw.GetNameList(nil, GetGuildMemberData(), GameData.Account.CharacterSlot)
    local num = #names

    for i, name in ipairs(names) do
        if recipient:lower() == name:lower() then
            zmmd.COOLDOWN = zmmd.CD_SHORT + zmmd.EXTRA_COOLDOWN
            mmw.UpdateTimeToWait()
            return
        end
    end

    zmmd.COOLDOWN = zmmd.CD_LONG + zmmd.EXTRA_COOLDOWN
    mmw.UpdateTimeToWait()
end


-- gathers names from various lists into one table
-- #TODO: remove doubled names
function mmw.GetNameList(friends, guild, alts)
    local names = {}

    if friends ~= nil then names = mmw.GetNames(friends, names) end
    if guild   ~= nil and GuildWindow.IsPlayerInAGuild() then names = mmw.GetNames(guild  , names) end
    if alts    ~= nil then names = mmw.GetNames(alts   , names) end
    
    table.sort(names)

    return names
end


-- returns a table with names from a list given
function mmw.GetNames(list, names)
    local length = #list
    local name
    
    for i = 1, length, 1 do
        name = list[i].name or list[i].Name
        name = name:sub(1,-3)
        if name:len() > 0 and name:lower() ~= towstring(zmmd.PLAYER_NAME:lower()) then
            table.insert(names, name)
        end
    end
    
    return names
end


-- emulates taborder xml entity
function mmw.OnKeyTab()
    mmw.FixRecipient()
    WindowAssignFocus("zMailModMassMailSubjectEditBox", true)
    TextEditBoxSelectAll("zMailModMassMailSubjectEditBox")
end


-- enables and disables list of crafting skills
function mmw.ToggleCraftingList()
    mmw.HideRarityList()
    WindowSetShowing(win .. "CraftingList", not WindowGetShowing(win .. "CraftingList"))
end


-- hides crafting skills list
function mmw.HideCraftingList()
    WindowSetShowing(win .. "CraftingList", false)
end


-- sets text color of selected crafting skill to 'yellow'
function mmw.MouseOverCraftingSkill()
    LabelSetTextColor(SystemData.MouseOverWindow.name, 255, 204, 102)
end


-- sets text color of selected crafting skill back to white
function mmw.MouseOverEndCraftingSkill()
    LabelSetTextColor(SystemData.MouseOverWindow.name, 255, 255, 255)
end


-- shows and hides item rarities list
function mmw.ToggleRarityList()
    mmw.HideCraftingList()
    WindowSetShowing(win .. "RarityList", not WindowGetShowing(win .. "RarityList"))
end


-- hides items rarities list
function mmw.HideRarityList()
    WindowSetShowing(win .. "RarityList", false)
end


-- changes color of selected item rarity to white
function mmw.MouseOverRarity()
    LabelSetTextColor(SystemData.MouseOverWindow.name, 255, 255, 255)
end


-- changes color of selected item rarity back to its original color
function mmw.MouseOverEndRarity()
    local rarity = WindowGetId(SystemData.MouseOverWindow.name)
    local color = GameDefs.ItemRarity[rarity].color
    LabelSetTextColor(SystemData.MouseOverWindow.name, color.r, color.g, color.b)
end


-- handles changes of the text in the 'To' edit box
function mmw.OnTextChanged()
    mmw.FormatRecipient()
    mmw.AutoComplete()
    zmm.RolodexCheckInput()
end


-- sets the first letter of the recipient as capital letter and rest as small letters
function mmw.FormatRecipient()
    local to  = TextEditBoxGetText("zMailModMassMailToEditBox")
    local len = to:len()
    if len > 1 then
        local first = to:sub(1,1):upper()
        local rest  = to:sub(2,len):lower()
        TextEditBoxSetText("zMailModMassMailToEditBox", first .. rest)
    elseif len == 1 then
        TextEditBoxSetText("zMailModMassMailToEditBox", to:upper())
    end
end


function mmw.SaveRecipient()
    local recipient = zMailModMassMailToEditBox.Text
    
    if zmmd.LAST_RECIPIENT == 1 and not mmu.IsEmpty(recipient) then
        zMailModSavedSettings[zmmd.PLAYER_NAME].lastMassMail = recipient
    end
end


function mmw.LoadRecipient()
    if zmmd.LAST_RECIPIENT == 1 and zMailModSavedSettings[zmmd.PLAYER_NAME].lastMassMail ~= nil then
        TextEditBoxSetText("zMailModMassMailToEditBox", zMailModSavedSettings[zmmd.PLAYER_NAME].lastMassMail)
    end
end