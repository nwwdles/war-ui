if not WarBoard_TogglerTokenMachine then WarBoard_TogglerTokenMachine = {} end
local WarBoard_TogglerTokenMachine = WarBoard_TogglerTokenMachine
local modName = "WarBoard_TogglerTokenMachine"
local modLabel = "Token"

function WarBoard_TogglerTokenMachine.Initialize()
	if LibWBToggler.CreateToggler(modName, modLabel, "TokenMachineIcon", 0, 0, nil, 0.9) then
		WindowSetDimensions(modName, 96, 30)
		WindowSetDimensions(modName.."Label", 64, 30)
		LibWBToggler.RegisterEvent(modName, "OnLButtonUp", "WarBoard_TogglerTokenMachine.OptionsWindow")
		LibWBToggler.RegisterEvent(modName, "OnMouseOver", "WarBoard_TogglerTokenMachine.ShowStatus")
	end
end

function WarBoard_TogglerTokenMachine.OptionsWindow()
	TokenMachine.ToggleSettings()
end

function WarBoard_TogglerTokenMachine.ShowStatus()
	LibWBToggler.DefaultTooltip(modName, modLabel)
end
