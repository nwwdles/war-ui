LibAddonButton = LibAddonButton or {};
LibAddonButton.Manager = 
{
	WindowName = "LibAddonButtonManagerWindow",
	Entries = {},
};
	
local windowName = LibAddonButton.Manager.WindowName;
local dragWindow = "LibAddonButtonManagerDragWindow";
local localization = LibAddonButton.Localization.GetMapping();

local DROP_DURATION = 1;
local animationTime = 0;

local NAVIGATE_LABELS = 4;

local activeItem = nil;
local activeRow = nil;

local activeButton = nil;
local activeMenu = nil;

local navigation = {};
local activeNavigation = nil;

local currentDraggableEntry = nil;
local isDragging = false;
local isDropping = nil;
local defaultPosition = nil;

local function CopyTable(table)

	local newTable = {};

	for k, v in pairs(table) do
		if (type(v) == "table") then
			newTable[k] = CopyTable(v);
		else
			newTable[k] = v;
		end
	end
	
	return newTable;

end

local function ShowTooltip(text)

	local windowName = SystemData.ActiveWindow.name;
	Tooltips.CreateTextOnlyTooltip(windowName, nil);
	Tooltips.SetTooltipText(1, 1, text);
	Tooltips.Finalize();
	
	local anchor = { Point = "top", RelativeTo = windowName, RelativePoint = "bottom", XOffset = 0, YOffset = 10 };
	Tooltips.AnchorTooltip(anchor);
	Tooltips.SetTooltipAlpha(1);
			
end

local function SetNavigationText(label, text)

	if (not text or text:len() == 0) then
		text = L"?";
	end

	LabelSetText(label, text);
	WindowSetDimensions(label, 150, 20);
	local width, _ = LabelGetTextDimensions(label);
	WindowSetDimensions(label, math.min(150, width), 20);
	
	return math.min(150, width)
	
end

local function ShowNavigation()
	
	local NAVIGATE_WIDTH = WindowGetDimensions(windowName .. "Navigate");
	local LABEL_SPACING = 30;
	
	local width = 0;
	local startIndex = 2;
	width = width + SetNavigationText(windowName .. "NavigateButton1", localization["Manager.Navigate.Main"]) + LABEL_SPACING;

	if (#navigation > 4) then
		startIndex = 3;
		width = width + SetNavigationText(windowName .. "NavigateButton2", L"...") + LABEL_SPACING;
		WindowSetShowing(windowName .. "NavigateButton2", true);
		WindowSetShowing(windowName .. "NavigateButton2Sep", true);
	end

	local rowStart = windowName .. "NavigateButton1";

	for index = startIndex, NAVIGATE_LABELS do
	
		local item = navigation[math.max(#navigation - (NAVIGATE_LABELS - startIndex + 1), startIndex - 1) + (index - startIndex) + 1]
		local labelName = windowName .. "NavigateButton" .. index;
		local isVisible = (item ~= nil);
		
		WindowSetShowing(labelName, isVisible);
		if (index > 1) then
			WindowSetShowing(windowName .. "NavigateButton" .. (index - 1) .. "Sep", isVisible);
		end
		
		if (isVisible) then
			local w = SetNavigationText(labelName, towstring(item.Text)) + LABEL_SPACING;
			width = width + w;
			WindowClearAnchors(labelName);
			if (width > NAVIGATE_WIDTH) then
				width = w;
				WindowAddAnchor(labelName, "bottomleft", rowStart, "topleft", 0, 0);
				rowStart = labelName;
			else
				WindowAddAnchor(labelName, "right", windowName .. "NavigateButton" .. (index - 1) .. "Sep", "left", 5, 2);
			end
		end
	end

end

local function IsDefaultPosition()

	if (not defaultPosition) then
		return true;
	end
		
	local x, y = WindowGetScreenPosition(windowName);
	return (x == defaultPosition.X and y == defaultPosition.Y);

end

local function CompareEntry(entryA, entryB)

	if (entryB == nil) then
		return false;
	end
	
	return (WStringsCompare(entryA.Text or L"", entryB.Text or L"") < 0);

end

local function SetupEntries()

	local entriesOrdered = {};
	LibAddonButton.Manager.Entries = {};

	-- note that only copied menu items have a "Name"
	for index, menuItem in ipairs(activeMenu.Subitems) do
		local displayText = towstring(menuItem.Text);
		if (menuItem.Divider) then
			displayText = localization["Manager.Divider"];
		end
		if (menuItem.Subitems) then
			displayText = displayText .. L" >";
		end
		table.insert(LibAddonButton.Manager.Entries, 
			{
				Text = displayText, 
				Name = menuItem.Name, 
				Index = index, 
				Subitems = menuItem.Subitems, 
				Divider = menuItem.Divider, 
				UserDefined = menuItem.UserDefined, 
				Created = menuItem.Created, 
				Source = menuItem 
			});
		table.insert(entriesOrdered, #LibAddonButton.Manager.Entries);
	end
	
	if (activeMenu.AutoSort) then
		table.sort(LibAddonButton.Manager.Entries, CompareEntry);
	end
	
	ListBoxSetDisplayOrder(windowName .. "List", entriesOrdered);
	
end

local function BeginDrag()
	
	animationTime = 0;
	isDropping = nil;
	
	LabelSetText(dragWindow .. "Text", currentDraggableEntry.Text);
	
	WindowSetDimensions(dragWindow .. "Text", 500, 30);
	
	local width, _ = LabelGetTextDimensions(dragWindow .. "Text");
	width = math.min(width, 500);
	
	WindowSetDimensions(dragWindow, width + 10, 30);
	WindowSetDimensions(dragWindow .. "Background", width + 10, 30);
	WindowSetDimensions(dragWindow .. "Text", width, 30);
	
	WindowClearAnchors(dragWindow);
	WindowAddAnchor(dragWindow, "right", "CursorWindow", "left", 0, 0);
	
	WindowStopAlphaAnimation(dragWindow);
	WindowStartAlphaAnimation(dragWindow, Window.AnimationType.SINGLE_NO_RESET, 1, 1, 1, false, 0, 0);
	
	WindowSetShowing(dragWindow, true);
	
	isDragging = true;
	Sound.Play(Sound.ICON_PICKUP);

end

local function EndDrag(failed)

	isDragging = false;
	
	if (failed or not currentDraggableEntry) then
		isDropping = nil;
		WindowClearAnchors(dragWindow);
		WindowSetShowing(dragWindow, false);
		return;
	end
	
	local activeWindow = SystemData.MouseOverWindow.name;
	local entry = currentDraggableEntry;
	local menu = entry.Source;
	
	currentDraggableEntry = nil;
		
	if (activeWindow:match("LibAddonButton") and activeWindow ~= windowName) then
	
		for name, createdButton in pairs(LibAddonButton.CreatedButtons) do
			if (createdButton:GetName() == activeWindow) then
			
				Sound.Play(Sound.ICON_DROP);
				
				isDropping = activeWindow;
				WindowStopAlphaAnimation(dragWindow);
				WindowStartAlphaAnimation(dragWindow, Window.AnimationType.EASE_OUT, 1, 0.1, 1, false, 0, 0);
				
				local buttonSettings = LibAddonButton.Settings.CreatedButtons[createdButton.Name];
				if (buttonSettings) then
					if (entry.Created) then
						-- created item, do a deep copy
						local item = CopyTable(entry.Source);
						table.insert(buttonSettings.Menu.Subitems, item);
					else
						-- copying a pre-defined button, check for duplicates
						local menuSource = activeButton.Name;
						if (activeButton.CreatedByUser) then
							menuSource = menu.Name; -- item is a copied item
						end
						for index, menuItem in ipairs(buttonSettings.Menu.Subitems) do
							if (menuItem.Name == menuSource and menuItem.Text == menu.Text) then
								return; -- item is already in the menu
							end
						end
						table.insert(buttonSettings.Menu.Subitems, { Name = menuSource, Text = menu.Text });
					end
				end
			
				if (activeWindow == activeButton:GetName()) then
					-- the copy to button is this button
					SetupEntries();
				end
			
				return;
			
			end
		end
	
	end
	
	Sound.Play(Sound.ICON_CLEAR);
	
	WindowClearAnchors(dragWindow);
	WindowSetShowing(dragWindow, false);

end

local function TintRow(rowName, isActive)

	local id = WindowGetId(rowName);
	local entry = LibAddonButton.Manager.Entries[id];
		
	if (isActive) then
		WindowSetTintColor(rowName .. "Background", 12, 47, 158);
		WindowSetAlpha(rowName .. "Background", 0.4);
	else
		WindowSetAlpha(rowName .. "Background", 0);
	end

end

local function TintRows()
	indexToRow = {};
	for rowIndex, dataIndex in ipairs(LibAddonButtonManagerWindowList.PopulatorIndices) do
	
		local rowName = windowName .. "ListRow" .. rowIndex;
		WindowSetId(rowName, dataIndex);
		indexToRow[dataIndex] = rowIndex;
		
		local entry = LibAddonButton.Manager.Entries[dataIndex];
		local r, g, b = 255, 255, 255;
		
		if (entry.Subitems) then
			r, g, b = 235, 213, 26;
		elseif (entry.Name) then
			r, g, b = 160, 160, 160; -- copied item
		elseif (entry.Divider) then
			r, g, b = 0, 213, 0;
		elseif (entry.UserDefined) then
			r, g, b = 160, 160, 255;
		end
		
		LabelSetTextColor(rowName .. "Text", r, g, b);
		
		TintRow(rowName, (rowName == activeRow));
	
	end
end

function LibAddonButton.Manager.Initialize()
	
	LibAddonButton.Manager.Advanced.Initialize();
	LibAddonButton.Manager.CustomItem.Initialize();
	LibAddonButton.Manager.MacroHelp.Initialize();
	
	CreateWindow(dragWindow, false);
	WindowSetTintColor(dragWindow, 0, 0, 0);
	WindowSetAlpha(dragWindow, 0.8);
	
	LabelSetText(windowName .. "TitleLabel", localization["Manager.Title"]);
	LabelSetText(windowName .. "ShowButtonCheckboxLabel", localization["Manager.Show"]);
	LabelSetText(windowName .. "AutoSortCheckboxLabel", localization["Manager.AutoSort"]);
	
	ButtonSetText(windowName .. "CreateButton", localization["Manager.Create"]);
	ButtonSetText(windowName .. "OptionButton", localization["Manager.Options"]);
	ButtonSetText(windowName .. "NewButton", localization["Manager.New"]);
	
	for index = 1, NAVIGATE_LABELS - 1 do
		LabelSetText(windowName .. "NavigateButton" .. index .. "Sep", L">");
		LabelSetTextColor(windowName .. "NavigateButton" .. index .. "Sep", 160, 160, 160);
	end
	
end

function LibAddonButton.Manager.Show()

	if (isDragging) then EndDrag(true) end

	if (WindowGetShowing(windowName)) then return end
	
	WindowClearAnchors(windowName);
	WindowAddAnchor(windowName, "center", "Root", "center", 0, 0);
	local x, y = WindowGetScreenPosition(windowName);
	defaultPosition = { X = x, Y = y };
	
	WindowSetShowing(windowName, true);
	
	WindowUtils.AddToOpenList(windowName, LibAddonButton.Manager.OnCloseLUp, WindowUtils.Cascade.MODE_NONE);
	
	Sound.Play(Sound.WINDOW_OPEN);

end

function LibAddonButton.Manager.Hide()

	if (not WindowGetShowing(windowName)) then return end
	
	WindowSetShowing(windowName, false);
	
	Sound.Play(Sound.WINDOW_CLOSE);
	
	WindowUtils.RemoveFromOpenList(windowName);

end

function LibAddonButton.Manager.OnHidden()

	if (isDragging) then
		EndDrag(true)
	end
	
	LibAddonButton.Manager.CustomItem.Hide();
	LibAddonButton.Manager.Advanced.Hide();

end

function LibAddonButton.Manager.OnCloseLUp()

	LibAddonButton.Manager.Hide();

end

local function MoveTo(menu)

	table.insert(menu.Subitems, table.remove(activeMenu.Subitems, activeItem.Entry.Index));
	LibAddonButton.Manager.CustomItem.Hide();
	SetupEntries();

end

local function ShowMoveTo(moveTo)
	
	local menuId = 2;
	EA_Window_ContextMenu.CreateContextMenu(activeRow, menuId);
	
	if (#navigation > 1) then -- #1 will always be the button itself and have no value
		local parentItem = nil;
		if (#navigation <= 2) then
			local createdButton = LibAddonButton.Settings.CreatedButtons[activeButton.Name];
			if (createdButton) then
				parentItem = createdButton.Menu;
			end
		else
			parentItem = navigation[#navigation - 1];
		end
		if (parentItem) then
			EA_Window_ContextMenu.AddMenuItem(localization["Manager.Context.MoveTo.Parent"], function() MoveTo(parentItem) end, false, true, menuId);
			
			if (#moveTo > 0) then
				EA_Window_ContextMenu.AddMenuDivider(menuId);
			end
		end
	end
	
	for index, menuItem in ipairs(moveTo) do
		EA_Window_ContextMenu.AddMenuItem(menuItem.Text, function() MoveTo(menuItem) end, false, true, menuId);
	end
	
	EA_Window_ContextMenu.Finalize(menuId);

end

local function CreateNewButton()
		
	if (IsDefaultPosition()) then
		WindowClearAnchors(windowName);
		WindowAddAnchor(windowName, "center", "Root", "center", -320, 0);
	end
	
	LibAddonButton.CreateFromTemplate(activeButton);
		
end

local function DestroyButton()

	LibAddonButton.Remove(activeButton.Name);
	LibAddonButton.Manager.Hide();

end

local function ShowAdvanced()

	LibAddonButton.Manager.Advanced.Show();

end

local function ShowOptions()
	
	EA_Window_ContextMenu.CreateContextMenu(SystemData.ActiveWindow.name);
	
	EA_Window_ContextMenu.AddMenuItem(localization["Manager.New"], function() CreateNewButton() end, false, true);
	EA_Window_ContextMenu.AddMenuItem(localization["Manager.Options.Advanced"], function() ShowAdvanced() end, false, true);
	EA_Window_ContextMenu.AddMenuDivider();
	
	local deleteIt = function()
		local menuId = 2;
		EA_Window_ContextMenu.CreateContextMenu(SystemData.ActiveWindow.name, menuId);
		EA_Window_ContextMenu.AddMenuItem(localization["Manager.Options.Delete.Delete"], function() DestroyButton() end, false, true, menuId);
		EA_Window_ContextMenu.Finalize(menuId);
	end
	
	EA_Window_ContextMenu.AddCascadingMenuItem(localization["Manager.Options.Delete"], deleteIt, false)
	
	
	EA_Window_ContextMenu.Finalize();

end

local function CreateRemoveContextMenu()
	
	if (not activeItem) then return end
	
	EA_Window_ContextMenu.CreateContextMenu(activeRow);
	
	local autoSort = ButtonGetPressedFlag(windowName .. "AutoSort" .. "Button");
	local moveTo = {};
	
	for index, menuItem in ipairs(activeMenu.Subitems) do
		if (index ~= activeItem.Entry.Index and menuItem.Created and menuItem.Subitems) then
			table.insert(moveTo, menuItem);
		end
	end
	
	if (activeItem.Entry.Subitems) then
		EA_Window_ContextMenu.AddMenuItem(localization["Manager.Context.ManageSubmenu"], LibAddonButton.Manager.OnContextMenuManageSubmenuLUp, false, true);
		EA_Window_ContextMenu.AddMenuDivider();
	end
	
	if (not autoSort) then	
		EA_Window_ContextMenu.AddMenuItem(localization["Manager.Context.MoveToTop"], LibAddonButton.Manager.OnContextMenuMoveToTopLUp, (activeItem.Entry.Index == 1), true);
		EA_Window_ContextMenu.AddMenuItem(localization["Manager.Context.MoveUp"], LibAddonButton.Manager.OnContextMenuMoveUpLUp, (activeItem.Entry.Index == 1), true);
		EA_Window_ContextMenu.AddMenuItem(localization["Manager.Context.MoveDown"], LibAddonButton.Manager.OnContextMenuMoveDownLUp, (activeItem.Entry.Index == #activeMenu.Subitems), true);
		EA_Window_ContextMenu.AddMenuItem(localization["Manager.Context.MoveToBottom"], LibAddonButton.Manager.OnContextMenuMoveToBottomLUp, (activeItem.Entry.Index == #activeMenu.Subitems), true);
		EA_Window_ContextMenu.AddMenuDivider();
	end
	
	if (#moveTo > 0 or #navigation > 1) then
		table.sort(moveTo, CompareEntry);
		EA_Window_ContextMenu.AddCascadingMenuItem(localization["Manager.Context.MoveTo"], function() ShowMoveTo(moveTo) end, false)
		EA_Window_ContextMenu.AddMenuDivider();
	end
	
	EA_Window_ContextMenu.AddMenuItem(localization["Manager.Context.Remove"], LibAddonButton.Manager.OnContextMenuRemoveLUp, false, true);
	EA_Window_ContextMenu.Finalize();
	
end

function LibAddonButton.Manager.OnContextMenuManageSubmenuLUp()

	LibAddonButton.Manager.LoadMenu(activeMenu.Subitems[activeItem.Entry.Index]);

end

local function ShiftItem(t, index, position)

	if (position < 1 or position > #t) then return end

	table.insert(t, position, table.remove(t, index));
	
	LibAddonButton.Manager.CustomItem.Hide();
	SetupEntries();

end

function LibAddonButton.Manager.OnContextMenuMoveToTopLUp()

	if (not activeItem) then return end
	local index = activeItem.Entry.Index;
	
	ShiftItem(activeMenu.Subitems, index, 1);
	
end

function LibAddonButton.Manager.OnContextMenuMoveUpLUp()

	if (not activeItem) then return end
	local index = activeItem.Entry.Index;
	
	ShiftItem(activeMenu.Subitems, index, index - 1);
	
end

function LibAddonButton.Manager.OnContextMenuMoveDownLUp()

	if (not activeItem) then return end
	local index = activeItem.Entry.Index;
	
	ShiftItem(activeMenu.Subitems, index, index + 1);

end

function LibAddonButton.Manager.OnContextMenuMoveToBottomLUp()

	if (not activeItem) then return end
	local index = activeItem.Entry.Index;
	
	ShiftItem(activeMenu.Subitems, index, #activeMenu.Subitems);

end

function LibAddonButton.Manager.OnContextMenuRemoveLUp()

	if (activeItem) then
		table.remove(activeMenu.Subitems, activeItem.Entry.Index);
		LibAddonButton.Manager.CustomItem.Hide();

		activeItem = nil;
		SetupEntries();
	end
	
end

function LibAddonButton.Manager.OnRowMouseOver()
	
	if (activeRow) then
		TintRow(activeRow, false);
	end
	
	activeRow = SystemData.MouseOverWindow.name;
	TintRow(activeRow, true);
		
end

function LibAddonButton.Manager.OnRowMouseOut()

	if (activeRow) then
		TintRow(activeRow, false);
		activeRow = nil;
	end
	
	if (currentDraggableEntry and not isDragging) then
		BeginDrag();
	end
		
end

function LibAddonButton.Manager.OnRowLDown()

	local id = WindowGetId(SystemData.MouseOverWindow.name);
	local entry = LibAddonButton.Manager.Entries[id];
	if (not entry) then return end
	
	currentDraggableEntry = entry;
	if (entry.Divider) then
		currentDraggableEntry = nil;
	end
	
	if (activeRow) then
		WindowSetTintColor(activeRow .. "Background", 12, 47, 198);
	end
	
end

function LibAddonButton.Manager.OnRowLUp()

	currentDraggableEntry = nil;
	local id = WindowGetId(SystemData.MouseOverWindow.name);
	local entry = LibAddonButton.Manager.Entries[id];
	if (not entry) then return end
	
	local item = activeMenu.Subitems[entry.Index];
	
	if (activeButton.CreatedByUser and item.Created) then
		LibAddonButton.Manager.CustomItem.ShowEdit(activeMenu, entry.Index);
	else
		ShowTooltip(localization["Manager.Error.CannotEdit"]);
	end
	
	if (activeRow) then
		TintRow(activeRow, true);
	end
	
end

function LibAddonButton.Manager.OnRowRDown()

	local id = WindowGetId(SystemData.MouseOverWindow.name);
	local entry = LibAddonButton.Manager.Entries[id];
	if (not entry) then return end
	
	if (activeRow) then
		WindowSetTintColor(activeRow .. "Background", 12, 47, 198);
	end
	
end

function LibAddonButton.Manager.OnRowRUp()

	local id = WindowGetId(SystemData.MouseOverWindow.name);
	local entry = LibAddonButton.Manager.Entries[id];
	if (not entry) then return end
	
	if (activeButton.CreatedByUser) then
		activeItem = { Text = entry.Text, Entry = entry };
		CreateRemoveContextMenu();
	end
	
	if (activeRow) then
		TintRow(activeRow, true);
	end
		
end

function LibAddonButton.Manager.OnCreateLClick()

	LibAddonButton.Manager.CustomItem.ShowEdit(activeMenu);

end

function LibAddonButton.Manager.OnOptionLClick()

	ShowOptions();

end

function LibAddonButton.Manager.OnNewLClick()

	CreateNewButton();

end

function LibAddonButton.Manager.OnNewMouseOver()

	local windowName = SystemData.ActiveWindow.name;
	
	Tooltips.CreateTextOnlyTooltip(windowName, nil);
	Tooltips.SetTooltipText(1, 1, localization["Manager.New.Tooltip"]);
	Tooltips.Finalize();
	
	local anchor = { Point = "top", RelativeTo = windowName, RelativePoint = "center", XOffset = 0, YOffset = -32 };
	Tooltips.AnchorTooltip(anchor);
	Tooltips.SetTooltipAlpha(1);
	
end

function LibAddonButton.Manager.OnShowButtonLUp()

	local isChecked = ButtonGetPressedFlag(windowName .. "ShowButton" .. "Button") == false;
	ButtonSetPressedFlag(windowName .. "ShowButton" .. "Button", isChecked);
	
	activeButton:Show(isChecked);
	
	local windowData = LayoutEditor.windowsList[activeButton:GetName()];
	if (windowData) then
		windowData.isHidden = (not isChecked);
	end

end

function LibAddonButton.Manager.OnShowButtonMouseOver()

	local windowName = SystemData.ActiveWindow.name;
	
	Tooltips.CreateTextOnlyTooltip(windowName, nil);
	Tooltips.SetTooltipText(1, 1, localization["Manager.Show.Tooltip"]);
	Tooltips.Finalize();
	
	local anchor = { Point = "right", RelativeTo = windowName, RelativePoint = "left", XOffset = 5, YOffset = 0 };
	Tooltips.AnchorTooltip(anchor);
	Tooltips.SetTooltipAlpha(1);
	
end

function LibAddonButton.Manager.OnAutoSortLUp()

	local isChecked = ButtonGetPressedFlag(windowName .. "AutoSort" .. "Button") == false;
	ButtonSetPressedFlag(windowName .. "AutoSort" .. "Button", isChecked);
	
	activeMenu.AutoSort = isChecked
	SetupEntries();

end

local function NavigateTo(index)

	if (not navigation[index]) then return end
	
	local item = navigation[index];
	
	while (#navigation >= index) do
		table.remove(navigation);
	end
	
	LibAddonButton.Manager.LoadMenu(item);

end

function LibAddonButton.Manager.OnNavigateLUp()

	local index = WindowGetId(SystemData.ActiveWindow.name);
	if (index == 2 and #navigation > NAVIGATE_LABELS) then
		-- show context
		local menuId = 1;
		EA_Window_ContextMenu.CreateContextMenu(SystemData.ActiveWindow.name, menuId);
		
		for i = 2, #navigation - 2 do
			EA_Window_ContextMenu.AddMenuItem(navigation[i].Text, function() NavigateTo(i) end, false, true, menuId);
		end
		
		EA_Window_ContextMenu.Finalize(menuId);
	elseif (index == 1) then
		LibAddonButton.Manager.Load(activeButton.Name);
	else
		NavigateTo(math.max(0, #navigation - NAVIGATE_LABELS) + index);
	end

end

function LibAddonButton.Manager.OnNavigateMouseOver()

	local index = WindowGetId(SystemData.ActiveWindow.name);
	activeNavigation = { Name = SystemData.ActiveWindow.name, Index = index };
	
	LabelSetTextColor(activeNavigation.Name, 255, 110, 10);

end

function LibAddonButton.Manager.OnNavigateMouseOut()

	if (activeNavigation) then
		LabelSetTextColor(activeNavigation.Name, 255, 255, 255);
	end

end

function LibAddonButton.Manager.OnPopulate()

	if (not LibAddonButtonManagerWindowList.PopulatorIndices) then
		return;
	end

	TintRows();
	
end

function LibAddonButton.Manager.LoadMenu(menu)

	LibAddonButton.Manager.CustomItem.Hide();
	
	WindowSetShowing(windowName .. "ShowButton", false);
	WindowSetShowing(windowName .. "ShowButtonCheckboxLabel", false);
	WindowSetShowing(windowName .. "AutoSort", true);
	WindowSetShowing(windowName .. "AutoSortCheckboxLabel", true);
	
	WindowSetShowing(windowName .. "Navigate", true);
	WindowSetShowing(windowName .. "InfoLabel", false);
	
	activeMenu = menu;
	
	ButtonSetPressedFlag(windowName .. "AutoSort" .. "Button", (menu.AutoSort == true));
	
	table.insert(navigation, menu);
	ShowNavigation();
	
	SetupEntries();

end

function LibAddonButton.Manager.Load(name)

	LibAddonButton.Manager.CustomItem.Hide();
	LibAddonButton.Manager.Advanced.Hide();
	local registeredButton = LibAddonButton.RegisteredButtons[name];
	if (not registeredButton) then return end
	
	local autoSort = true;
	
	activeButton = registeredButton.Button;
	if (activeButton.CreatedByUser) then
		local createdButton = LibAddonButton.Settings.CreatedButtons[activeButton.Name];
		activeMenu = createdButton.Menu;
	else
		activeMenu = registeredButton.Menu;
	end
	
	autoSort = activeMenu.AutoSort;	
	if (autoSort == nil) then
		autoSort = true;
	end
	
	navigation = {};
	table.insert(navigation, { }); -- first item will always revert to a reload

	if (activeButton.CreatedByUser) then
		LabelSetText(windowName .. "InfoLabel", localization["Manager.RemoveInfo"]);
		WindowSetShowing(windowName .. "OptionButton", true);
		WindowSetShowing(windowName .. "NewButton", false);
		WindowSetShowing(windowName .. "CreateButton", true);
		WindowSetShowing(windowName .. "AutoSort", true);
		WindowSetShowing(windowName .. "AutoSortCheckboxLabel", true);
	else
		LabelSetText(windowName .. "InfoLabel", localization["Manager.DragInfo"]);
		WindowSetShowing(windowName .. "OptionButton", false);
		WindowSetShowing(windowName .. "NewButton", true);
		WindowSetShowing(windowName .. "CreateButton", false);
		WindowSetShowing(windowName .. "AutoSort", false);
		WindowSetShowing(windowName .. "AutoSortCheckboxLabel", false);
	end
	
	WindowSetShowing(windowName .. "Navigate", false);
	WindowSetShowing(windowName .. "InfoLabel", true);
	WindowSetShowing(windowName .. "ShowButton", true);
	WindowSetShowing(windowName .. "ShowButtonCheckboxLabel", true);
	
	ButtonSetPressedFlag(windowName .. "AutoSort" .. "Button", autoSort);
	ButtonSetPressedFlag(windowName .. "ShowButton" .. "Button", WindowGetShowing(activeButton:GetName()));
	
	SetupEntries();

end

function LibAddonButton.Manager.Update()

	SetupEntries();
	
end

function LibAddonButton.Manager.GetActiveButton()
	return activeButton;
end

function LibAddonButton.Manager.OnUpdate(elapsed)

	if (isDragging) then	
		if (not Cursor.LButtonDown) then
			EndDrag();
		end
	end
	
	if (isDropping) then
		animationTime = animationTime + elapsed;
		if (animationTime >= DROP_DURATION) then
			animationTime = 0;
			isDropping = nil;
			WindowSetShowing(dragWindow, false);
		else
			local timeLeft = animationTime / DROP_DURATION;
			local buttonX, buttonY = WindowGetScreenPosition(isDropping);
			local dragX, dragY = WindowGetScreenPosition(dragWindow);
			local x, y = (buttonX - dragX) * timeLeft, (buttonY - dragY) * timeLeft;
		
			WindowClearAnchors(dragWindow);
			WindowAddAnchor(dragWindow, "topleft", "Root", "topleft", (dragX + x) / InterfaceCore.GetScale(), (dragY + y) / InterfaceCore.GetScale());
		end
	end

end