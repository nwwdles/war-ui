LibAddonButton = LibAddonButton or {};
LibAddonButton.Manager.MacroHelp = 
{
	WindowName = "LibAddonButtonManagerMacroHelpWindow",
};
	
local windowName = LibAddonButton.Manager.MacroHelp.WindowName;
local localization = LibAddonButton.Localization.GetMapping();

function LibAddonButton.Manager.MacroHelp.Initialize()
	
	LabelSetText(windowName .. "TitleLabel", localization["Manager.MacroHelp.Title"]);
	LabelSetText(windowName .. "ScriptLabel", localization["Manager.MacroHelp.Script"]);
	LabelSetText(windowName .. "ScriptInfoLabel", localization["Manager.MacroHelp.Script.Info"]);
	LabelSetText(windowName .. "MultipleLinesLabel", localization["Manager.MacroHelp.MultipleLines"]);
	LabelSetText(windowName .. "MultipleLinesInfoLabel", localization["Manager.MacroHelp.MultipleLines.Info"]);
	LabelSetText(windowName .. "CommandLabel", localization["Manager.MacroHelp.Command"]);
	LabelSetText(windowName .. "CommandDelayLabel", localization["Manager.MacroHelp.Command.Delay"]);
	LabelSetText(windowName .. "CommandDelayInfoLabel", localization["Manager.MacroHelp.Command.Delay.Info"]);
	
end

function LibAddonButton.Manager.MacroHelp.Show()

	if (WindowGetShowing(windowName)) then return end
	
	local x, y = WindowGetScreenPosition(LibAddonButton.Manager.CustomItem.WindowName .. "MacroHelpLabel");
	local interfaceScale = InterfaceCore.GetScale();
	
	WindowClearAnchors(windowName);
	WindowAddAnchor(windowName, "topleft", "Root", "center", x / interfaceScale, y / interfaceScale);

	WindowSetShowing(windowName, true);
	
	WindowUtils.AddToOpenList(windowName, LibAddonButton.Manager.MacroHelp.OnCloseLUp, WindowUtils.Cascade.MODE_NONE);
	
	Sound.Play(Sound.WINDOW_OPEN);

end

function LibAddonButton.Manager.MacroHelp.Hide()

	if (not WindowGetShowing(windowName)) then return end
	
	WindowSetShowing(windowName, false);
	
	Sound.Play(Sound.WINDOW_CLOSE);
	
	WindowUtils.RemoveFromOpenList(windowName);

end

function LibAddonButton.Manager.MacroHelp.OnHidden()

end

function LibAddonButton.Manager.MacroHelp.OnCloseLUp()

	LibAddonButton.Manager.MacroHelp.Hide();

end