LibAddonButton = LibAddonButton or {};
LibAddonButton.Manager.Advanced = 
{
	WindowName = "LibAddonButtonManagerAdvancedWindow",
	Frames = {},
};

local DEFAULT_HEIGHT = 200;
	
local windowName = LibAddonButton.Manager.Advanced.WindowName;
local localization = LibAddonButton.Localization.GetMapping();

local windowDimensions = {};
local Textures = { Default = 1, Highlight = 2, Pressed = 3 };
local TextureType = { Simple = 1, Animation = 2 };

local items = 
{
	{ Name = localization["Manager.Advanced.Size"], Window = "ItemSize" },
	{ Name = localization["Manager.Advanced.Default"], Window = "ItemTexture", Texture = Textures.Default },
	{ Name = localization["Manager.Advanced.Highlight"], Window = "ItemTexture", Texture = Textures.Highlight },
	{ Name = localization["Manager.Advanced.Pressed"], Window = "ItemTexture", Texture = Textures.Pressed },
};
local activeItem = nil;
local buttonSettings = {};
local textureSettings = {};
local activeRow = nil;
local activeRowItem = nil;
local lockSettings = false;

local function SetupFrames()

	local entriesOrdered = {};
	LibAddonButton.Manager.Advanced.Frames = {};

	for index, frame in ipairs(textureSettings.Animation.Frames) do
		table.insert(LibAddonButton.Manager.Advanced.Frames, { Texture = towstring(frame), Index = index });
		table.insert(entriesOrdered, #LibAddonButton.Manager.Advanced.Frames);
	end
	
	ListBoxSetDisplayOrder(windowName .. "ItemTextureItemAnimationFrames", entriesOrdered);

end

local function TintRow(rowName, isActive)

	local id = WindowGetId(rowName);
	local entry = LibAddonButton.Manager.Advanced.Frames[id];
		
	if (isActive) then
		WindowSetTintColor(rowName .. "Background", 12, 47, 158);
		WindowSetAlpha(rowName .. "Background", 0.4);
	else
		WindowSetAlpha(rowName .. "Background", 0);
	end

end

local function TintRows()

	for rowIndex, dataIndex in ipairs(LibAddonButtonManagerAdvancedWindowItemTextureItemAnimationFrames.PopulatorIndices) do
	
		local rowName = windowName .. "ItemTextureItemAnimationFramesRow" .. rowIndex;
		WindowSetId(rowName, dataIndex);
		
		local entry = LibAddonButton.Manager.Advanced.Frames[dataIndex];
		
		TintRow(rowName, (rowName == activeRow));
	
	end
end


local function CopyTable(table)

	local newTable = {};

	for k, v in pairs(table) do
		if (type(v) == "table") then
			newTable[k] = CopyTable(v);
		else
			newTable[k] = v;
		end
	end
	
	return newTable;

end

local function CopyTexture(texture)	
	local textureSettings = { Simple = "", Animation = { Frames = {}, FPS = 1 } };
	if (type(texture) == "table") then
		textureSettings.Animation = CopyTable(texture);
		textureSettings.Active = TextureType.Animation;
	else
		textureSettings.Simple = texture;
		textureSettings.Active = TextureType.Simple;
	end
	return textureSettings;
end

function LoadTexture(texture)

	lockSettings = true;
	textureSettings = texture;

	ButtonSetPressedFlag(windowName .. "ItemTextureSimple" .. "Button", (texture.Active == TextureType.Simple));
	ButtonSetPressedFlag(windowName .. "ItemTextureAnimation" .. "Button", (texture.Active == TextureType.Animation));
	
	WindowSetShowing(windowName .. "ItemTextureItemSimple", (texture.Active == TextureType.Simple));
	WindowSetShowing(windowName .. "ItemTextureItemAnimation", (texture.Active == TextureType.Animation));

	TextEditBoxSetText(windowName .. "ItemTextureItemSimpleTextureEditBox", towstring(texture.Simple or ""));

	SetupFrames();
	
	local animationWindow = windowName .. "ItemTextureItemAnimation";
	
	TextEditBoxSetText(animationWindow .. "FpsEditBox", towstring(texture.Animation.FPS or "1"));
	TextEditBoxSetText(animationWindow .. "TextureEditBox", L"");
	lockSettings = false;
	
end

local function GetWindowDimension(windowName)
	local dimensions = windowDimensions[windowName];
	if (not dimensions) then
		local w, h = WindowGetDimensions(windowName);
		dimensions = { Width = w, Height = h };
		windowDimensions[windowName] = dimensions;
	end
	return dimensions.Width, dimensions.Height;
end

local function ShowItem(item)

	if (activeItem and activeItem ~= item) then
		WindowSetShowing(windowName .. activeItem.Window, false);
	end
	
	local width, _ = GetWindowDimension(windowName);
	local _, height = GetWindowDimension(windowName .. item.Window);
	
	WindowSetDimensions(windowName, width, height + DEFAULT_HEIGHT);
	
	if (item.Window == "ItemTexture") then
		local texture = {};
		if (item.Texture == Textures.Default) then
			texture = buttonSettings.Default;
		elseif (item.Texture == Textures.Highlight) then
			texture = buttonSettings.Highlight;
		elseif (item.Texture == Textures.Pressed) then
			texture = buttonSettings.Pressed;
		end
		LoadTexture(texture);
	end
	
	WindowSetShowing(windowName .. item.Window, true);
	activeItem = item;

end

function LibAddonButton.Manager.Advanced.Initialize()

	ComboBoxClearMenuItems(windowName .. "ItemComboBox");
	for index, item in ipairs(items) do
		WindowSetShowing(windowName .. item.Window, false);
		ComboBoxAddMenuItem(windowName .. "ItemComboBox", item.Name);
	end
	
	WindowSetShowing(windowName .. "ItemTextureItemSimple", false);
	WindowSetShowing(windowName .. "ItemTextureItemAnimation", false);
	
	LabelSetText(windowName .. "TitleLabel", localization["Manager.Advanced.Title"]);
	
	LabelSetText(windowName .. "ItemSizeLabel", localization["Manager.Advanced.Size"]);
	LabelSetText(windowName .. "ItemSizeWidthLabel", localization["Manager.Advanced.Size.Width"]);
	LabelSetText(windowName .. "ItemSizeHeightLabel", localization["Manager.Advanced.Size.Height"]);
	
	LabelSetText(windowName .. "ItemTextureSimpleCheckboxLabel", localization["Manager.Advanced.Texture.Simple"]);
	LabelSetText(windowName .. "ItemTextureAnimationCheckboxLabel", localization["Manager.Advanced.Texture.Animation"]);
	
	LabelSetText(windowName .. "ItemTextureItemSimpleLabel", localization["Manager.Advanced.Texture"]);
	
	LabelSetText(windowName .. "ItemTextureItemAnimationFpsLabel", localization["Manager.Advanced.Texture.Animation.FPS"]);
	LabelSetText(windowName .. "ItemTextureItemAnimationFramesLabel", localization["Manager.Advanced.Texture.Animation.Frames"]);
	LabelSetText(windowName .. "ItemTextureItemAnimationTextureLabel", localization["Manager.Advanced.Texture"]);
	ButtonSetText(windowName .. "ItemTextureItemAnimationAddButton", localization["Manager.Advanced.Texture.Animation.Add"]);
	
	ButtonSetText(windowName .. "ApplyButton", localization["Manager.Advanced.Apply"]);
	
end

function LibAddonButton.Manager.Advanced.LoadSettings()

	lockSettings = true;
	local button = LibAddonButton.Manager.GetActiveButton();
	
	buttonSettings = {};
	buttonSettings.Size = { Width = button.Size.Width, Height = button.Size.Height };
	buttonSettings.Default = CopyTexture(button.Icons.Default);
	buttonSettings.Pressed = CopyTexture(button.Icons.Pressed);
	buttonSettings.Highlight = CopyTexture(button.Icons.Highlight);
	
	TextEditBoxSetText(windowName .. "ItemSizeWidthEditBox", towstring(buttonSettings.Size.Width));
	TextEditBoxSetText(windowName .. "ItemSizeHeightEditBox", towstring(buttonSettings.Size.Height));
	lockSettings = false;
	
end

function LibAddonButton.Manager.Advanced.Show()

	if (WindowGetShowing(windowName)) then return end
	
	ComboBoxSetSelectedMenuItem(windowName .. "ItemComboBox", 1);
	ShowItem(items[1]);
	LibAddonButton.Manager.Advanced.LoadSettings();
	
	WindowClearAnchors(windowName);
	WindowAddAnchor(windowName, "topleft", LibAddonButton.Manager.WindowName, "topright", 0, 0);
	
	WindowSetShowing(windowName, true);
	
	WindowUtils.AddToOpenList(windowName, LibAddonButton.Manager.Advanced.OnCloseLUp, WindowUtils.Cascade.MODE_NONE);
	
	Sound.Play(Sound.WINDOW_OPEN);

end

function LibAddonButton.Manager.Advanced.Hide()

	if (not WindowGetShowing(windowName)) then return end
	
	WindowSetShowing(windowName, false);
	
	Sound.Play(Sound.WINDOW_CLOSE);
	
	WindowUtils.RemoveFromOpenList(windowName);

end

function LibAddonButton.Manager.Advanced.OnHidden()

end

function LibAddonButton.Manager.Advanced.OnCloseLUp()

	LibAddonButton.Manager.Advanced.Hide();

end

function LibAddonButton.Manager.Advanced.OnItemChanged()

	local value = ComboBoxGetSelectedMenuItem(windowName .. "ItemComboBox");
	if (items[value]) then
		ShowItem(items[value]);
	end
	
end


local function CreateAnimationContextMenu()
	
	if (not activeRowItem) then return end
	
	EA_Window_ContextMenu.CreateContextMenu(activeRow);
		
	EA_Window_ContextMenu.AddMenuItem(localization["Manager.Context.MoveToTop"], LibAddonButton.Manager.Advanced.OnContextMenuMoveToTopLUp, (activeRowItem.Index == 1), true);
	EA_Window_ContextMenu.AddMenuItem(localization["Manager.Context.MoveUp"], LibAddonButton.Manager.Advanced.OnContextMenuMoveUpLUp, (activeRowItem.Index == 1), true);
	EA_Window_ContextMenu.AddMenuItem(localization["Manager.Context.MoveDown"], LibAddonButton.Manager.Advanced.OnContextMenuMoveDownLUp, (activeRowItem.Index == #textureSettings.Animation.Frames), true);
	EA_Window_ContextMenu.AddMenuItem(localization["Manager.Context.MoveToBottom"], LibAddonButton.Manager.Advanced.OnContextMenuMoveToBottomLUp, (activeRowItem.Index == #textureSettings.Animation.Frames), true);
	EA_Window_ContextMenu.AddMenuDivider();
	
	EA_Window_ContextMenu.AddMenuItem(localization["Manager.Context.Remove"], LibAddonButton.Manager.Advanced.OnContextMenuRemoveLUp, false, true);
	EA_Window_ContextMenu.Finalize();
	
end

local function ShiftItem(t, index, position)

	if (position < 1 or position > #t) then return end

	table.insert(t, position, table.remove(t, index));
	
	SetupFrames();

end

function LibAddonButton.Manager.Advanced.OnContextMenuMoveToTopLUp()

	if (not activeRowItem) then return end
	local index = activeRowItem.Index;
	
	ShiftItem(textureSettings.Animation.Frames, index, 1);
	
end

function LibAddonButton.Manager.Advanced.OnContextMenuMoveUpLUp()

	if (not activeRowItem) then return end
	local index = activeRowItem.Index;
	
	ShiftItem(textureSettings.Animation.Frames, index, index - 1);
	
end

function LibAddonButton.Manager.Advanced.OnContextMenuMoveDownLUp()

	if (not activeRowItem) then return end
	local index = activeRowItem.Index;
	
	ShiftItem(textureSettings.Animation.Frames, index, index + 1);

end

function LibAddonButton.Manager.Advanced.OnContextMenuMoveToBottomLUp()

	if (not activeRowItem) then return end
	local index = activeRowItem.Index;
	
	ShiftItem(textureSettings.Animation.Frames, index, #textureSettings.Animation.Frames);

end

function LibAddonButton.Manager.Advanced.OnContextMenuRemoveLUp()

	if (activeRowItem) then
		table.remove(textureSettings.Animation.Frames, activeRowItem.Index);
		activeRowItem = nil;
		SetupFrames();
	end
	
end

function LibAddonButton.Manager.Advanced.OnRowMouseOver()
	
	if (activeRow) then
		TintRow(activeRow, false);
	end
	
	activeRow = SystemData.MouseOverWindow.name;
	TintRow(activeRow, true);
		
end

function LibAddonButton.Manager.Advanced.OnRowMouseOut()

	if (activeRow) then
		TintRow(activeRow, false);
		activeRow = nil;
	end
		
end

function LibAddonButton.Manager.Advanced.OnRowLDown()

	local id = WindowGetId(SystemData.MouseOverWindow.name);
	local entry = LibAddonButton.Manager.Advanced.Frames[id];
	if (not entry) then return end
	
	
	if (activeRow) then
		WindowSetTintColor(activeRow .. "Background", 12, 47, 198);
	end
	
end

function LibAddonButton.Manager.Advanced.OnRowLUp()

	local id = WindowGetId(SystemData.MouseOverWindow.name);
	local entry = LibAddonButton.Manager.Advanced.Frames[id];
	if (not entry) then return end
	
	TextEditBoxSetText(windowName .. "ItemTextureItemAnimationTextureEditBox", towstring(entry.Texture));
	
	if (activeRow) then
		TintRow(activeRow, true);
	end
	
end

function LibAddonButton.Manager.Advanced.OnRowRDown()

	local id = WindowGetId(SystemData.MouseOverWindow.name);
	local entry = LibAddonButton.Manager.Advanced.Frames[id];
	if (not entry) then return end
	
	if (activeRow) then
		WindowSetTintColor(activeRow .. "Background", 12, 47, 198);
	end
	
end

function LibAddonButton.Manager.Advanced.OnRowRUp()

	local id = WindowGetId(SystemData.MouseOverWindow.name);
	local entry = LibAddonButton.Manager.Advanced.Frames[id];
	if (not entry) then return end
	
	activeRowItem = entry;
	CreateAnimationContextMenu();
	
	if (activeRow) then
		TintRow(activeRow, true);
	end
		
end

function LibAddonButton.Manager.Advanced.OnFramesPopulate()

	if (not LibAddonButtonManagerAdvancedWindowItemTextureItemAnimationFrames.PopulatorIndices) then
		return;
	end

	TintRows();

end

function LibAddonButton.Manager.Advanced.OnTextureSimpleLUp()

	local isChecked = ButtonGetPressedFlag(windowName .. "ItemTextureSimple" .. "Button") == false;
	if (not isChecked) then return end
	ButtonSetPressedFlag(windowName .. "ItemTextureSimple" .. "Button", isChecked);
	ButtonSetPressedFlag(windowName .. "ItemTextureAnimation" .. "Button", not isChecked);

	WindowSetShowing(windowName .. "ItemTextureItemSimple", (isChecked == true));
	WindowSetShowing(windowName .. "ItemTextureItemAnimation", (isChecked == false));
	
	textureSettings.Active = TextureType.Simple;
	
end

function LibAddonButton.Manager.Advanced.OnTextureAnimationLUp()

	local isChecked = ButtonGetPressedFlag(windowName .. "ItemTextureAnimation" .. "Button") == false;
	if (not isChecked) then return end
	ButtonSetPressedFlag(windowName .. "ItemTextureAnimation" .. "Button", isChecked);
	ButtonSetPressedFlag(windowName .. "ItemTextureSimple" .. "Button", not isChecked);
	
	WindowSetShowing(windowName .. "ItemTextureItemSimple", (isChecked == false));
	WindowSetShowing(windowName .. "ItemTextureItemAnimation", (isChecked == true));
	
	textureSettings.Active = TextureType.Animation;
	
end

function LibAddonButton.Manager.Advanced.OnWidthChanged()

	if (lockSettings) then return end
	local width = tonumber(TextEditBoxGetText(windowName .. "ItemSizeWidthEditBox"));
	buttonSettings.Size.Width = width;

end

function LibAddonButton.Manager.Advanced.OnHeightChanged()

	if (lockSettings) then return end
	local height = tonumber(TextEditBoxGetText(windowName .. "ItemSizeHeightEditBox"));
	buttonSettings.Size.Height = height;

end

function LibAddonButton.Manager.Advanced.OnSimpleTextureChanged()

	if (lockSettings) then return end
	local value = tostring(TextEditBoxGetText(windowName .. "ItemTextureItemSimpleTextureEditBox"));
	textureSettings.Simple = value;

end

function LibAddonButton.Manager.Advanced.OnAnimationFpsChanged()

	if (lockSettings) then return end
	local value = tonumber(TextEditBoxGetText(windowName .. "ItemTextureItemAnimationFpsEditBox"));
	textureSettings.Animation.FPS = value;

end

function LibAddonButton.Manager.Advanced.OnAnimationAddFrameLClick()

	local texture = tostring(TextEditBoxGetText(windowName .. "ItemTextureItemAnimationTextureEditBox"));
	if (texture:len() == 0) then return end
	
	TextEditBoxSetText(windowName .. "ItemTextureItemAnimationTextureEditBox", L"");
	
	table.insert(textureSettings.Animation.Frames, texture);
	SetupFrames();

end

local function GetTexture(settings)

	if (settings.Active == TextureType.Simple) then
		return settings.Simple;
	else
		return settings.Animation;
	end

end

function LibAddonButton.Manager.Advanced.OnApplyLClick()

	local button = LibAddonButton.Manager.GetActiveButton();
	
	local width, height = buttonSettings.Size.Width, buttonSettings.Size.Height;
	local defaultIcon = GetTexture(buttonSettings.Default);
	local highlightIcon = GetTexture(buttonSettings.Highlight);
	local pressedIcon = GetTexture(buttonSettings.Pressed);
	
	button:Modify(width, height, defaultIcon, highlightIcon, pressedIcon);
	
	local settings = LibAddonButton.Settings.CreatedButtons[button.Name];
	settings.Size = button.Size;
	settings.Icons = button.Icons;
	
	local isAnimated = (type(defaultIcon) == "table" or type(highlightIcon) == "table" or type(pressedIcon) == "table");
	LibAddonButton.UpdateButtonIsAnimated(button.Name, isAnimated);

	LibAddonButton.Manager.Advanced.Hide();

end