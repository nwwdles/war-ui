LibAddonButton = LibAddonButton or {};
LibAddonButton.Manager.CustomItem = 
{
	WindowName = "LibAddonButtonManagerCustomItemWindow",
};
	
local windowName = LibAddonButton.Manager.CustomItem.WindowName;
local localization = LibAddonButton.Localization.GetMapping();

local DEFAULT_HEIGHT = 200;

local windowDimensions = {};
local existingItem = {};
local activeMenuItem = nil;
local activeMenu = nil;
local activeType = nil;
local activeWindow = nil;

local ItemType = { Macro = 1, Menu = 2, Divider = 3 };
local typeWindows = { "Macro", "Menu", "Divider" };

local function CopyTable(table)

	local newTable = {};

	for k, v in pairs(table) do
		if (type(v) == "table") then
			newTable[k] = CopyTable(v);
		else
			newTable[k] = v;
		end
	end
	
	return newTable;

end

local function GetWindowDimension(windowName)
	local dimensions = windowDimensions[windowName];
	if (not dimensions) then
		local w, h = WindowGetDimensions(windowName);
		dimensions = { Width = w, Height = h };
		windowDimensions[windowName] = dimensions;
	end
	return dimensions.Width, dimensions.Height;
end

local function ShowTooltip(text)

	local windowName = SystemData.ActiveWindow.name;
	Tooltips.CreateTextOnlyTooltip(windowName, nil);
	Tooltips.SetTooltipText(1, 1, text);
	Tooltips.Finalize();
	
	local anchor = { Point = "top", RelativeTo = windowName, RelativePoint = "bottom", XOffset = 0, YOffset = -10 };
	Tooltips.AnchorTooltip(anchor);
	Tooltips.SetTooltipAlpha(1);
			
end

local function ShowType(type)

	local typeWindowName = typeWindows[type];

	if (activeWindow and activeWindow ~= typeWindowName) then
		WindowSetShowing(windowName .. activeWindow, false);
	end
	
	local width, _ = GetWindowDimension(windowName);
	local _, height = GetWindowDimension(windowName .. typeWindowName);
	
	WindowSetDimensions(windowName, width, height + DEFAULT_HEIGHT);

	local isMacro = (type == ItemType.Macro);
	local isMenu = (type == ItemType.Menu);
	local isDivider = (type == ItemType.Divider);

	-- copy the name from one type to the other
	if (activeType) then
		if (activeType == ItemType.Macro) then
			local text = TextEditBoxGetText(windowName .. "MacroTextEditBox");
			TextEditBoxSetText(windowName .. "MenuTextEditBox", text);
		elseif (activeType == ItemType.Menu) then
			local text = TextEditBoxGetText(windowName .. "MenuTextEditBox");
			TextEditBoxSetText(windowName .. "MacroTextEditBox", text);
		end
	end

	activeType = type;
	
	if (isMenu) then
		local isNew = true;
		if (existingItem.Index) then
			isNew = (activeMenuItem[existingItem.Index].Subitems == nil);
		end
		
		local infoText = localization["Manager.CustomItem.Menu.Info"];
		if (not isNew) then
			infoText = localization["Manager.CustomItem.Menu.Manage.Info"];
		end
		LabelSetText(windowName .. "MenuInfoLabel", infoText);
		
		WindowSetShowing(windowName .. "MenuManageButton", not isNew);
	end
	
	WindowSetShowing(windowName .. typeWindowName, true);
	activeWindow = typeWindowName;

end

local function ShowButtons(showApply, showCreate)

	if (WindowGetShowing(windowName .. "ApplyButton") == showApply and WindowGetShowing(windowName .. "CreateButton") == showCreate) then return end

	WindowSetShowing(windowName .. "ApplyButton", showApply);
	WindowSetShowing(windowName .. "CreateButton", showCreate);
	
	if (showCreate) then
		WindowClearAnchors(windowName .. "CreateButton");
		if (showApply) then
			WindowAddAnchor(windowName .. "CreateButton", "topleft", windowName .. "ApplyButton", "topright", 0, 0);
		else
			WindowAddAnchor(windowName .. "CreateButton", "bottomright", windowName, "bottomright", -20, -20);
		end
	end

end

local function LoadItem(index)

	local item = nil;
	existingItem = { Index = index };
	
	if (index) then
		item = activeMenuItem[index];
	end
	
	local text = L"";
	local macro = L"";
	local tooltip = L"";
	
	if (item) then
		--existing
		text = item.Text or text;
		macro = item.Macro or macro;
		tooltip = item.Tooltip or tooltip;
	else
		--new
	end
	
	TextEditBoxSetText(windowName .. "MacroTextEditBox", towstring(text));
	TextEditBoxSetText(windowName .. "MacroTooltipEditBox", towstring(tooltip));
	TextEditBoxSetText(windowName .. "MenuTextEditBox", towstring(text));
	TextEditBoxSetText(windowName .. "MacroEditBox", towstring(macro));
	
	local itemType = ItemType.Macro;
	
	if (item) then
		ShowButtons(true, true);
		if (item.Subitems) then
			itemType = ItemType.Menu;
		elseif (item.Divider) then
			itemType = ItemType.Divider;
		end
	else
		ShowButtons(false, true);
	end

	ShowType(itemType);
	ComboBoxSetSelectedMenuItem(windowName .. "TypeComboBox", itemType);
	
end

function LibAddonButton.Manager.CustomItem.Initialize()

	for index, typeWindowName in ipairs(typeWindows) do
		WindowSetShowing(windowName .. typeWindowName, false);
	end
	
	LabelSetText(windowName .. "TitleLabel", localization["Manager.CustomItem.Title"]);
	LabelSetText(windowName .. "TypeLabel", localization["Manager.CustomItem.Type"]);
	LabelSetText(windowName .. "MacroLabel", localization["Manager.CustomItem.Macro"]);
	LabelSetText(windowName .. "MacroTooltipLabel", localization["Manager.CustomItem.Tooltip"]);
	LabelSetText(windowName .. "MacroTextLabel", localization["Manager.CustomItem.Text"]);
	LabelSetText(windowName .. "MacroHelpLabel", L"?");
	LabelSetText(windowName .. "MenuLabel", localization["Manager.CustomItem.Menu"]);
	LabelSetText(windowName .. "MenuTextLabel", localization["Manager.CustomItem.Text"]);
	LabelSetText(windowName .. "DividerLabel", localization["Manager.CustomItem.Divider"]);
	LabelSetText(windowName .. "DividerWarningLabel", localization["Manager.CustomItem.Divider.Warning"]);
	
	LabelSetTextColor(windowName .. "DividerWarningLabel", 240, 10, 10);
	
	ComboBoxClearMenuItems(windowName .. "TypeComboBox");
	ComboBoxAddMenuItem(windowName .. "TypeComboBox", localization["Manager.CustomItem.Type.Macro"]);
	ComboBoxAddMenuItem(windowName .. "TypeComboBox", localization["Manager.CustomItem.Type.Menu"]);
	ComboBoxAddMenuItem(windowName .. "TypeComboBox", localization["Manager.CustomItem.Type.Divider"]);
	
	ButtonSetText(windowName .. "CreateButton", localization["Manager.CustomItem.Create"]);
	ButtonSetText(windowName .. "ApplyButton", localization["Manager.CustomItem.Apply"]);
	ButtonSetText(windowName .. "MenuManageButton", localization["Manager.CustomItem.Menu.Manage"]);
	
	ShowButtons(false, true);
	
end

function LibAddonButton.Manager.CustomItem.Show()

	if (WindowGetShowing(windowName)) then return end
	
	WindowClearAnchors(windowName);
	WindowAddAnchor(windowName, "topright", LibAddonButton.Manager.WindowName, "topleft", 0, 0);
	
	WindowSetShowing(windowName, true);
	
	WindowUtils.AddToOpenList(windowName, LibAddonButton.Manager.CustomItem.OnCloseLUp, WindowUtils.Cascade.MODE_NONE);
	
	Sound.Play(Sound.WINDOW_OPEN);

end

function LibAddonButton.Manager.CustomItem.Hide()

	if (not WindowGetShowing(windowName)) then return end
	
	WindowSetShowing(windowName, false);
	
	Sound.Play(Sound.WINDOW_CLOSE);
	
	WindowUtils.RemoveFromOpenList(windowName);

end

function LibAddonButton.Manager.CustomItem.OnHidden()

end

function LibAddonButton.Manager.CustomItem.OnCloseLUp()

	LibAddonButton.Manager.CustomItem.Hide();

end

function LibAddonButton.Manager.CustomItem.ShowNew(menu)

	activeMenu = menu;
	activeMenuItem = menu.Subitems;
	LoadItem(nil);
	LibAddonButton.Manager.CustomItem.Show();

end

function LibAddonButton.Manager.CustomItem.ShowEdit(menu, index)

	activeMenu = menu;
	activeMenuItem = menu.Subitems;
	LoadItem(index);
	LibAddonButton.Manager.CustomItem.Show();

end

function LibAddonButton.Manager.CustomItem.GetActiveItem()
	return existingItem or {};
end

local function SaveItem()

	local macro = TextEditBoxGetText(windowName .. "MacroEditBox");
	local text = L"";
	local itemType = ComboBoxGetSelectedMenuItem(windowName .. "TypeComboBox");
	local tooltip = nil;
	
	local isMacro = (itemType == ItemType.Macro);
	local isMenu = (itemType == ItemType.Menu);
	local isDivider = (itemType == ItemType.Divider);

	if (isMenu) then
		text = TextEditBoxGetText(windowName .. "MenuTextEditBox");
	elseif (isMacro) then
		text = TextEditBoxGetText(windowName .. "MacroTextEditBox");
		tooltip = TextEditBoxGetText(windowName .. "MacroTooltipEditBox");
		if (tooltip:len() == 0) then
			tooltip = nil; -- no point saving nothing
		end
	end

	if (isDivider) then
		text = nil;
	elseif (text:len() == 0) then
		ShowTooltip(localization["Manager.CustomItem.Error.TextRequired"]);
		return;
	end
	
	if (isMacro and macro:len() == 0) then
		ShowTooltip(localization["Manager.CustomItem.Error.MacroRequired"]);
		return;
	end

	local item =
	{
		Created = true,
		Text = text,
	}
	
	if (isMacro) then
		item.Macro = macro;
		item.Tooltip = tooltip;
	elseif (isDivider) then
		item.Divider = true;
	elseif (isMenu) then
		item.Subitems = {};
	end
	
	return item;

end

function LibAddonButton.Manager.CustomItem.OnMacroHelpLUp()

	LibAddonButton.Manager.MacroHelp.Show();

end

function LibAddonButton.Manager.CustomItem.OnMacroHelpMouseOver()
	
	LabelSetTextColor(windowName .. "MacroHelpLabel", 255, 110, 10);

end

function LibAddonButton.Manager.CustomItem.OnMacroHelpMouseOut()

	LabelSetTextColor(windowName .. "MacroHelpLabel", 255, 255, 255);
	
end

function LibAddonButton.Manager.CustomItem.OnTypeChanged()

	local type = ComboBoxGetSelectedMenuItem(windowName .. "TypeComboBox");
	ShowType(type);

end

function LibAddonButton.Manager.CustomItem.OnMenuManageLClick()

	LibAddonButton.Manager.LoadMenu(activeMenuItem[existingItem.Index]);

end

function LibAddonButton.Manager.CustomItem.OnCreateLClick()
	
	local item = SaveItem();
	if (not item) then return end
	
	if (existingItem.Index and activeMenuItem[existingItem.Index] and activeMenuItem[existingItem.Index].Subitems and item.Subitems) then
		item.Subitems = CopyTable(activeMenuItem[existingItem.Index].Subitems); -- creating using a template, copy all subitems
	end
	
	table.insert(activeMenuItem, item);
	
	LibAddonButton.Manager.Update();
	LibAddonButton.Manager.CustomItem.Hide();

end

function LibAddonButton.Manager.CustomItem.OnApplyLClick()
	
	local item = SaveItem();
	if (not item) then return end
	
	if (activeMenuItem[existingItem.Index] and activeMenuItem[existingItem.Index].Subitems and item.Subitems) then
		item.Subitems = activeMenuItem[existingItem.Index].Subitems; -- move over the subitems
	end
	
	activeMenuItem[existingItem.Index] = item;
	
	LibAddonButton.Manager.Update();
	LibAddonButton.Manager.CustomItem.Hide();

end