<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="LibAddonButton" version="1.0.6" date="11/17/2010" >
		<VersionSettings gameVersion="1.4.0" windowsVersion="1.0" savedVariablesVersion="1.1" />

		<Author name="Healix" email="" />
		<Description text="Creates icon buttons for addons" />
       
		<Dependencies>
		</Dependencies>

		<Files>
			<File name="Localization.lua" />
			<File name="Localization/enUS.lua" />
			<File name="Core.xml" />
			<File name="Core.lua" />
			<File name="Menu.lua" />
			<File name="Button.lua" />
			<File name="Manager/Manager.xml" />
			<File name="Manager/Manager.lua" />
			<File name="Manager/Advanced.xml" />
			<File name="Manager/Advanced.lua" />
			<File name="Manager/CustomItem.xml" />
			<File name="Manager/CustomItem.lua" />
			<File name="Manager/MacroHelp.xml" />
			<File name="Manager/MacroHelp.lua" />
		</Files>
		
		<SavedVariables>
			<SavedVariable name="LibAddonButton.Settings" />		  
		</SavedVariables>
		
		<OnInitialize>
			<CreateWindow name="LibAddonButtonManagerWindow" show="false" />
			<CreateWindow name="LibAddonButtonManagerAdvancedWindow" show="false" />
			<CreateWindow name="LibAddonButtonManagerCustomItemWindow" show="false" />
			<CreateWindow name="LibAddonButtonManagerMacroHelpWindow" show="false" />
			<CallFunction name="LibAddonButton.Initialize" />
		</OnInitialize>
		<OnUpdate>
			<CallFunction name="LibAddonButton.OnUpdate" />
		</OnUpdate>
		<OnShutdown>
			<CallFunction name="LibAddonButton.Unload" />
		</OnShutdown>
		<WARInfo>
		</WARInfo>
	</UiMod>
</ModuleFile>
