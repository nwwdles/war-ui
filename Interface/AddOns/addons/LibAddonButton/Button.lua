LibAddonButton = LibAddonButton or {};
LibAddonButton.Button = Frame:Subclass("LibAddonButtonTemplate");

local localization = LibAddonButton.Localization.GetMapping();
local DEFAULT_ICON = "LibAddonButton_Default";
local DEFAULT_ANIMATION =
{
	FPS = 10,
	Frames = { "LibAddonButton_Gear1", "LibAddonButton_Gear2", "LibAddonButton_Gear3", "LibAddonButton_Gear4", "LibAddonButton_Gear5", "LibAddonButton_Gear6" },
};
local DEFAULT_PRESSED_ICON = "LibAddonButton_Pressed";

local function UpdateIcon(self)

	self.Animation.Frame = 0;
	self.Animation.Duration = 0;

	if (self.Pressed) then
		if (not self.Animation.IsPressedAnimated) then
			self:DisplayIcon(self.Icons.Pressed);
		end
	elseif (self.Highlighted) then
		if (not self.Animation.IsHighlightAnimated) then
			self:DisplayIcon(self.Icons.Highlight);
		end
	else
		if (not self.Animation.IsDefaultAnimated) then
			self:DisplayIcon(self.Icons.Default);
		end
	end
	
end

local function SetLayout(button, width, height, defaultIcon, highlightIcon, pressedIcon)

	button.Icons =
	{
		Default = defaultIcon or DEFAULT_ICON,
		Highlight = highlightIcon or defaultIcon or DEFAULT_ICON,
		Pressed = pressedIcon or defaultIcon or DEFAULT_ICON,
	}
	if (not (defaultIcon and highlightIcon and pressedIcon)) then
		button.Icons.Highlight = DEFAULT_ANIMATION;
		button.Icons.Pressed = DEFAULT_PRESSED_ICON;
	end
	
	button.Animation = 
	{
		Frame = 0,
		Duration = 0,
		IsDefaultAnimated = (type(button.Icons.Default) == "table"),
		IsHighlightAnimated = (type(button.Icons.Highlight) == "table"),
		IsPressedAnimated = (type(button.Icons.Pressed) == "table"),
	};
	button.Size = { Width = width or 40, Height = height or 40 };
	button:SetDimensions(button.Size.Width, button.Size.Height);
	
	if (button.Animation.IsDefaultAnimated) then
		button:DisplayIcon(button.Icons.Default[1]);
	else
		button:DisplayIcon(button.Icons.Default);
	end
	
end

function LibAddonButton.Button:Create(name, width, height, defaultIcon, highlightIcon, pressedIcon)

	local windowName = "LibAddonButton" .. name;
	local button = self:CreateFromTemplate(windowName, "Root");
	button.Name = name;
	
	SetLayout(button, width, height, defaultIcon, highlightIcon, pressedIcon);
	
	button.MenuItems = 0;
	button.CreatedByUser = false;
	button.Override = { LUp = nil, RUp = nil };
	
	return button;
	
end

function LibAddonButton.Button:Modify(width, height, defaultIcon, highlightIcon, pressedIcon)

	SetLayout(self, width, height, defaultIcon, highlightIcon, pressedIcon);
	
end

-- Used to override the default LUp/RUp events
function LibAddonButton.Button:SetLUpOverride(callbackFunction)
	self.Override.LUp = callbackFunction;
end

function LibAddonButton.Button:SetRUpOverride(callbackFunction)
	self.Override.RUp = callbackFunction;
end

function LibAddonButton.Button:SetFinalizeMenuOverride(callbackFunction)
	self.Override.FinalizeMenu = callbackFunction;
end

function LibAddonButton.Button:DisplayIcon(icon)

	if (self.Icons.Current ~= icon) then
		DynamicImageSetTexture(self:GetName(), icon or DEFAULT_ICON, 0, 0);
		self.Icons.Current = icon;
	end

end

function LibAddonButton.Button:SetPosition(x, y)

	self:SetAnchor({ XOffset = x, YOffset = y });

end

local function RunMacro(macro)

	if (type(macro) ~= "wstring") then
		macro = towstring(macro);
	end
	
	LibAddonButton.RunMacro(macro);

end

local function AnchorContext(self)

	local interfaceScale = InterfaceCore.GetScale();
	local contextMenu = EA_Window_ContextMenu.contextMenus[1];
	local contextWidth, contextHeight = WindowGetDimensions(contextMenu.name);
	local buttonX, buttonY = WindowGetScreenPosition(self:GetName());
	local buttonWidth, buttonHeight = self:GetDimensions();
	local screenWidth, screenHeight = GetScreenResolution();

	local pointX, pointY, relativePointX, relativePointY, x, y = "right", "top", "left", "top", 10, 0;
	local anchorRight = true;
	local anchorTop = true;
	
	if (buttonX + (contextWidth + buttonWidth + 10) * interfaceScale > screenWidth) then
		relativePointX = "right";
		pointX = "left";
		x = -10;
	end
	
	if (buttonY + (contextHeight * interfaceScale) > screenHeight) then
		relativePointY = "bottom";
		pointY = "bottom"
	end
	
	WindowClearAnchors(contextMenu.name);
	WindowAddAnchor(contextMenu.name, pointY .. pointX, self:GetName(), relativePointY .. relativePointX, x, y);
	
end

function LibAddonButton.Button:OnLButtonDown(flags, mouseX, mouseY)

	self.Pressed = true;
	UpdateIcon(self);

end

local function GetMenuItem(name, menuText)

	local registeredButton = LibAddonButton.RegisteredButtons[name];
	if (not registeredButton) then return end
	
	for index, menuItem in ipairs(registeredButton.Menu.Subitems) do
		if (menuItem.Text == menuText) then
			return menuItem;
		end
	end

end

local function GetMenu(self)

	if (self.CreatedByUser) then
		return LibAddonButton.Settings.CreatedButtons[self.Name].Menu;
	else
		return LibAddonButton.RegisteredButtons[self.Name].Menu;
	end

end

local function CompareMenuItem(itemA, itemB)
	if (itemB == nil) then
		return false;
	end

	return (WStringsCompare(itemA.Text or L"", itemB.Text or L"") < 0);
	
end

local function GetMenuItems(self, menu)
	
	local menuItems = {};

	if (self.CreatedByUser) then
		
		for index, item in ipairs(menu.Subitems) do
			local menu = nil;
			if (item.Created) then -- item was created by the user
				--[[ There's really no need to make a copy anymore since both registered and created buttons use the same format
				menu = 
				{
					Text = item.Text,
					Macro = item.Macro,
					Subitems = item.Subitems,
					AutoSort = item.AutoSort,
				};
				--]]
				menu = item;
			elseif (item.Name) then -- item is a copy
				menu = GetMenuItem(item.Name, item.Text);
			else -- item must be registered and from a copy
				menu = item;
			end
			if (menu) then
				table.insert(menuItems, menu);
			end
		end
		
	else
	
		-- menu is registered, no checks are needed
		menuItems = menu.Subitems;
		
	end

	if (menu.AutoSort) then
		table.sort(menuItems, CompareMenuItem);
	end
	
	return menuItems;

end

local function HideContext(menuId)

	-- hides any showing cascading contexts that go beyond the selected menu
	local max = EA_Window_ContextMenu.CONTEXT_MENU_3;
	if (menuId > max) then return end
	
	for index = menuId, max do
		EA_Window_ContextMenu.Hide(index);
	end

end

local function ShowContext(self, menu, menuId)

	menuId = menuId or EA_Window_ContextMenu.CONTEXT_MENU_1;
	
	HideContext(menuId + 1);
	
	EA_Window_ContextMenu.CreateContextMenu(SystemData.ActiveWindow.name, menuId);
	LibAddonButton.Menu.Create(menuId);
	
	for index, menuItem in ipairs(menu) do
		if (menuItem.Subitems) then
			EA_Window_ContextMenu.AddCascadingMenuItem(menuItem.Text, function() ShowContext(self, GetMenuItems(self, menuItem), menuId + 1) end, false, menuId);
		elseif (menuItem.Divider) then
			EA_Window_ContextMenu.AddMenuDivider(menuId);
		elseif (menuItem.UserDefined) then
			EA_Window_ContextMenu.AddUserDefinedMenuItem(menuItem.UserDefined, menuId);
		else
			local f = menuItem.Callback;
			if (menuItem.Macro) then
				f = function() RunMacro(menuItem.Macro) end
			end
			LibAddonButton.Menu.AddMenuItem(menuId, menuItem.Text, f, menuItem.Tooltip);
		end
	end
	
	LibAddonButton.Menu.Finalize(menuId);
	
	if (self.Override.FinalizeMenu ~= nil and type(self.Override.FinalizeMenu) == "function") then
		local abortMenu = self.Override.FinalizeMenu(menuId);
		if (abortMenu) then
			return;
		end
	end
	
    EA_Window_ContextMenu.Finalize(menuId);

end


function LibAddonButton.Button:ProcessLButtonUp(flags, mouseX, mouseY)

	local menu = GetMenu(self);
	local menuItems = GetMenuItems(self, menu);
	local itemCount = #menuItems;
	self.MenuItems = itemCount;
	
	-- A user created menu with default to being a 1 click button when it has only 1 item
	if (self.CreatedByUser and itemCount == 1) then
		local menuItem = menuItems[1];
		if (menuItem.Subitems and #menuItem.Subitems > 0) then
			-- Note a submenu item is not sorted
			ShowContext(self, GetMenuItems(self, menuItem));
			AnchorContext(self);
			return;
		else
			if (type(menuItem.Callback) == "function") then
				menuItem.Callback();		
				return;
			elseif (menuItem.Macro) then
				RunMacro(menuItem.Macro);
				return;
			end
		end
		-- non standard button, display as normal
	end
	
	ShowContext(self, menuItems);
	AnchorContext(self);
	
end

function LibAddonButton.Button:OnLButtonUp(flags, mouseX, mouseY)

	self.Pressed = false;
	UpdateIcon(self);
	
	if (self.Override.LUp ~= nil) then
		local success, errmsg = pcall(self.Override.LUp, flags, mouseX, mouseY);
		return;
	end
	
	self:ProcessLButtonUp(flags, mouseX, mouseY);

end

function LibAddonButton.Button:ToggleLock(isMovable)

	if (isMovable == nil) then
		isMovable = not WindowGetMovable(self:GetName());
	end
	
	self:SetMovable(isMovable);

end

function LibAddonButton.Button:Manage()

	LibAddonButton.Manager.Load(self.Name);
	LibAddonButton.Manager.Show();

end

function LibAddonButton.Button:OnRButtonUp(flags, mouseX, mouseY)
	
	if (self.Override.RUp ~= nil) then
		local success, errmsg = pcall(self.Override.RUp, flags, mouseX, mouseY);
		return;
	end

	local contextMenuID = EA_Window_ContextMenu.CONTEXT_MENU_1;
	local text = GetString(StringTables.Default.LABEL_TO_LOCK); -- "Lock"
	
	if (not WindowGetMovable(self:GetName())) then
		text = GetString(StringTables.Default.LABEL_TO_UNLOCK); -- "Unlock"
	end

	EA_Window_ContextMenu.CreateContextMenu(SystemData.ActiveWindow.name, contextMenuID);
	EA_Window_ContextMenu.AddMenuItem(text, function() self:ToggleLock() end, false, true, contextMenuID);
	EA_Window_ContextMenu.AddMenuItem(localization["Button.Manage"], function() self:Manage() end, false, true, contextMenuID);
	
	-- modify the context creation so the minimum menu width is smaller
	local contextMenu = EA_Window_ContextMenu.contextMenus[contextMenuID];	
	local minWidth = EA_Window_ContextMenu.MIN_WIDTH;
	EA_Window_ContextMenu.MIN_WIDTH = contextMenu.greatestWidth + 10;
    EA_Window_ContextMenu.Finalize(contextMenuID);
	EA_Window_ContextMenu.MIN_WIDTH = minWidth;
	
	-- manually anchor the context menu item so that it won't break when the menu is going offscreen
	WindowClearAnchors(contextMenu.name .. "DefaultItem1");
	WindowAddAnchor(contextMenu.name .. "DefaultItem1", "topleft", contextMenu.name, "topleft", 5, 5);
	AnchorContext(self);
	
end

function LibAddonButton.Button:OnMouseOver(flags, mouseX, mouseY)

	self.Highlighted = true;
	UpdateIcon(self);

end

function LibAddonButton.Button:OnMouseOverEnd(flags, mouseX, mouseY)

	self.Pressed = false;
	self.Highlighted = false;
	UpdateIcon(self);
	
end

local function RunAnimation(self, elapsed, animation)

	self.Animation.Duration = self.Animation.Duration - elapsed;
	if (self.Animation.Duration <= 0) then
		self.Animation.Frame = (self.Animation.Frame % #animation.Frames) + 1;
		self.Animation.Duration = 1 / math.max(animation.FPS or 1, 1);
		self:DisplayIcon(animation.Frames[self.Animation.Frame]);
	end
			
end

function LibAddonButton.Button:Update(elapsed)

	if (self.Pressed) then
		if (self.Animation.IsPressedAnimated) then
			RunAnimation(self, elapsed, self.Icons.Pressed);
		end
	elseif (self.Highlighted) then
		if (self.Animation.IsHighlightAnimated) then
			RunAnimation(self, elapsed, self.Icons.Highlight);
		end
	else
		if (self.Animation.IsDefaultAnimated) then
			RunAnimation(self, elapsed, self.Icons.Default);
		end
	end

end