LibAddonButton = LibAddonButton or {};
if (not LibAddonButton.Localization) then
	LibAddonButton.Localization = {};
	LibAddonButton.Localization.Language = {};
end

local localizationWarning = false;

function LibAddonButton.Localization.GetMapping()

	local lang = LibAddonButton.Localization.Language[SystemData.Settings.Language.active];
	
	if (not lang) then
		if (not localizationWarning) then
			d("Your current language is not supported. English will be used instead.");
			localizationWarning = true;
		end
		lang = LibAddonButton.Localization.Language[SystemData.Settings.Language.ENGLISH];
	end
	
	return lang;
	
end