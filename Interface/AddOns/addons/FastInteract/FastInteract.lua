FastInteract={shift=false}

FastInteract.Options={
	heal="FastInteractWindowCheckHealer",
	ralley="FastInteractWindowCheckRalleyMaster",
	q_complete="FastInteractWindowCheckQuestComplete",
	q_accept="FastInteractWindowCheckQuestAccept",
	vendor_skip="FastInteractWindowCheckVendorSkip",
	vendor_jd="FastInteractWindowCheckVendorJD"
}


if FastInteractOptions==nil then
	FastInteractOptions={}
end

function FastInteract.ProcessRightClick(modifier)
	FastInteract.shift=(modifier==4)
end
function FastInteract.ProcessQuest()
	if FastInteract.shift then
		if (GameData.InteractQuestData.status == "offer") and (FastInteractOptions["q_accept"])then
			BroadcastEvent( SystemData.Events.INTERACT_ACCEPT_QUEST ) 
		elseif (GameData.InteractQuestData.status == "complete") and FastInteractOptions["q_complete"] then
			BroadcastEvent( SystemData.Events.INTERACT_COMPLETE_QUEST ) 
		end
		FastInteract.shift=false
	end
end
function FastInteract.HealAll()
	if FastInteractOptions["heal"]==true then
		if FastInteract.shift then
			EA_Window_InteractionHealer.HealAllPenalties()
			FastInteract.shift=false
		end
	end
end
function FastInteract.Interact()
	if FastInteract.shift then
		if  (#EA_Window_InteractionBase.questListData>0) then
			for k,v in pairs(EA_Window_InteractionBase.questListData) do
				if  (((v.completion==GameData.QuestCompletion.OFFER) and (DataUtils.GetQuestDataFromName( v.name ) == nil)) and FastInteractOptions["q_accept"]) or  --accept quest
					((v.completion==GameData.QuestCompletion.PENDING) and (DataUtils.IsQuestComplete( v.name )) and FastInteractOptions["q_complete"])  --complete quest
					--or (#EA_Window_InteractionBase.questListData==1) 
				then
					InteractSelect(EA_Window_InteractionBase.tempTarget, v.type, v.slot) -- Select quest
					break
				end
			end
		end
		
		if (#EA_Window_InteractionBase.optionListData>0) then
			for k,optionData in pairs(EA_Window_InteractionBase.optionListData) do
				if ((optionData.type == GameData.InteractType.STORE) and FastInteractOptions["vendor_jd"] and (JunkDump~=nil)) then
					BroadcastEvent(SystemData.Events.INTERACT_SHOW_STORE)
					JunkDump.processBackpack()
					JunkDump.JunkBackpack()
				end
				if ((optionData.type == GameData.InteractType.STORE) and FastInteractOptions["vendor_skip"])or 
				   ((optionData.type == GameData.InteractType.BINDER) and FastInteractOptions["ralley"])
				then
					InteractSelect(EA_Window_InteractionBase.tempTarget, optionData.type, k) --Ralley Master, bind to location
					--EA_Window_InteractionBase.Done() -- close interaction window
					break
				end
			end
		end
	else
		FastInteract.shift=false
	end
end

function FastInteract.OptionsShow()
	if DoesWindowExist("FastInteractWindow") then
		FastInteract.OptionsClose()
	else
		CreateWindow("FastInteractWindow",true)
	end
end
function FastInteract.OptionsSetup()
	LabelSetText("FastInteractWindowTitleBarText",L"FastInteract - Shortcuts")
	LabelSetText("FastInteractWindowDescription",L"Shortcuts are accessed\nwith Shift+Rightclick")
	LabelSetText(FastInteract.Options["heal"].."Label",			L"Healer:        heal all penalties")
	LabelSetText(FastInteract.Options["ralley"].."Label",		L"Ralley Master: set ralley point")
	LabelSetText(FastInteract.Options["q_complete"].."Label",	L"Questgiver:    complete quest")
	LabelSetText(FastInteract.Options["q_accept"].."Label",		L"Questgiver:    accept quest")
	LabelSetText(FastInteract.Options["vendor_skip"].."Label",		L"Merchant:      skip to items")
	if JunkDump ~= nil then
		LabelSetText(FastInteract.Options["vendor_jd"].."Label",		L"Merchant:      use JunkDump")
	else
		WindowSetShowing(FastInteract.Options["vendor_jd"],false)
	end
	
	
	for k,v in pairs(FastInteract.Options) do
		if FastInteractOptions[k]==nil then FastInteractOptions[k]=false end
	end
	
	--[[if FastInteractOptions["heal"]   == nil then   FastInteractOptions["heal"]   = false end
	if FastInteractOptions["ralley"] == nil then   FastInteractOptions["ralley"] = false end
	if FastInteractOptions["q_complete"] == nil then   FastInteractOptions["q_complete"] = false end
	if FastInteractOptions["q_accept"] == nil then   FastInteractOptions["q_accept"] = false end]]--
	FastInteract.OptionsLoad()
end
function FastInteract.OptionsClose()
	FastInteract.OptionsSave()
	WindowSetShowing("FastInteractWindow",false)
	DestroyWindow("FastInteractWindow")
end
function FastInteract.OptionsSave()
	for k, v in pairs(FastInteract.Options) do
			FastInteractOptions[k]=ButtonGetPressedFlag(v.."Button")
	end

--[[	if ButtonGetPressedFlag("FastInteractWindowCheckHealerButton") then
		FastInteractOptions["heal"]=true
	else
		FastInteractOptions["heal"]=false
	end
	
	if ButtonGetPressedFlag("FastInteractWindowCheckRalleyMasterButton") then
		FastInteractOptions["ralley"]=true
	else
		FastInteractOptions["ralley"]=false
	end]]--
end

function FastInteract.OptionsLoad()
	for k, v in pairs(FastInteract.Options) do
			ButtonSetPressedFlag(v.."Button" ,FastInteractOptions[k])
	end
end

function FastInteract.Initialize()
    LibSlash.RegisterSlashCmd("FastInteract", FastInteract.OptionsShow)
    LibSlash.RegisterSlashCmd("fi", FastInteract.OptionsShow)
	
	RegisterEventHandler(SystemData.Events.R_BUTTON_DOWN_PROCESSED,"FastInteract.ProcessRightClick")
	RegisterEventHandler(SystemData.Events.INTERACT_DEFAULT,"FastInteract.Interact")
	RegisterEventHandler(SystemData.Events.INTERACT_SHOW_QUEST,"FastInteract.ProcessQuest")
	RegisterEventHandler(SystemData.Events.INTERACT_SHOW_HEALER,"FastInteract.HealAll")
end

function FastInteract.OnShutdown()
	UnregisterEventHandler(SystemData.Events.R_BUTTON_DOWN_PROCESSED,"FastInteract.ProcessRightClick")
	UnregisterEventHandler(SystemData.Events.INTERACT_DEFAULT,"FastInteract.Interact")
	UnregisterEventHandler(SystemData.Events.INTERACT_SHOW_QUEST,"FastInteract.ProcessQuest")
	UnregisterEventHandler(SystemData.Events.INTERACT_SHOW_HEALER,"FastInteract.HealAll")
end