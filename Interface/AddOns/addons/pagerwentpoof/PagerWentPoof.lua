if not PagerWentPoof then PagerWentPoof = {} end
local pairs = pairs

function PagerWentPoof.Initialize()
	PagerWentPoof.HideThem()
end

function PagerWentPoof.HideThem()
	ActionBarConstants.SHOW_PAGE_SELECTOR_RIGHT = 44
	ActionBarConstants.SHOW_PAGE_SELECTOR_LEFT = 44
	for k1,v1 in pairs(ActionBarClusterSettings) do
		if k1 ~= "layoutMode" then
			v1["selector"] = 44
		end
	end
	ActionBarClusterManager.ReanchorCluster()
end
