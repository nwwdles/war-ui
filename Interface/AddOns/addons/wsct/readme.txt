********************************************
Warhammer Scrolling Combat Text Versions 1.3
********************************************

Website - http://war.curse.com/downloads/war-addons/details/wsct.aspx

What is it? - The official port of SCT from WoW to WAR

What can it do?

- Damage for incoming and outgoing with filtering for small damage.
- Heals for incoming and outgoing with filtering for small heals.
- All "Miss" events (dodge, block, immune, etc...)
- Custom Colors for all text events
- Low Health with values, and optional sounds
- Enter and Leave Combat Messages
- Renown, Experience, Influence Gain
- Morale Ability Alerts
- Buff/Debuff Gains and Fades
- Action Point Gains
- Career Resource Changes
- Skill names
- You skills icons
- Eight Animation Types (Vertical, Rainbow, Horizontal, Angled Down, Angled Up, Sprinkler, Curved HUD, Angled HUD)
- Five Fonts
- Three seperate Animation frames, each with their own settings. Assign any Event to each.
- Ability to flag any event as critical or as a text messages
- Options for text size, opacity, animation speed, movement speed, and on screen placement
- 5 built in profiles for easy experimentation.

How do I use it? - First unzip it into your interface\addons directory. For more info on installing, please read install.txt. Now just run WAR and once logged in, type /wsct to get the options list.

Slash Commands
--------------
Basics
/wsct menu        - shows the option menu
/wsct reset       - resets settings back to defaults


Support
Please post all errors and suggestions on http://war.curse.com/downloads/war-addons/details/wsct.aspx

Version History
1.3 Added skill icons. Options for icons are available for each frame. Icons will mostly only show for your skills, and buffs, but not always. Added Action Point event. Added Career Resource Event. Truncate will now also affect Buffs/Debuffs if on.
1.22 Added name truncation options. Seperated out Parry, Dodge, and Block for individual options. Fixed various minor bugs from 1.1.
1.21 Added ability and buff name options. Fixed alignment and sticky for labels (you may need to adjust your settings). Fixed Buff Type display. Fixed Alpha for large/smaller texts.
1.2 Added GUI options menu, most slash commands removed. Your options will reset, but should be much easier to configure now. Fixed Damage with Siege Weapons. Fixed some Morale Alert issues (still some remain). Low HP warning will fire only once every 5 seconds. Tweaked animations a bit.
1.1 Added Morale ABility Alerts. Added Buff/Debuff Gains and Fades. Use LibSlash if available. Filter out random high number events. Lots of animation tweaks, offscreen attached damage will now appear at its default location.
1.01 Created own slash handler, so LibSlash not required Tweaked animation when on targets. Misc clean up. 
1.0 Initial release