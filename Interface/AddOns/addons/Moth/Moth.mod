<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >	
  <UiMod name="Moth" version="1.3" date="02APR16" >		
    <Author name="Astika" email="xmtrcv+war@gmail.com" />		
    <Description text="Mouse Over Target Hover" /> 
    <VersionSettings gameVersion="1.9.9" windowsVersion="1.0" savedVariablesVersion="1.0" />		
    <Dependencies>
      <Dependency name="EA_MouseOverTargetWindow" /> 
      <Dependency name="EASystem_Tooltips" />  <!-- Coutersy of Aiiane and beatrix_kiddo <3 -->
    </Dependencies>
    <Files>			
      <File name="MothHelpers.lua" />			
      <File name="MothProfiles.lua" />			
      <File name="MothSettings.lua" />			
      <File name="MothPlugins.lua" />			
      <File name="Moth.lua" />			
      <File name="GuidanceCounselor.lua" />			
      <File name="Moth.xml" />		
    </Files>		
    <OnInitialize>			
      <CallFunction name="Moth.Initialize" />		
    </OnInitialize>		
    <OnUpdate>			
      <CallFunction name="Moth.Update" />		
    </OnUpdate>  		
    <OnShutdown />
    <WARInfo>    
      <Categories>
        <Category name="DEVELOPMENT" />
      </Categories>    
      <Careers>
        <Career name="BLACKGUARD" />
        <Career name="WITCH_ELF" />
        <Career name="DISCIPLE" />
        <Career name="SORCERER" />
        <Career name="IRON_BREAKER" />
        <Career name="SLAYER" />
        <Career name="RUNE_PRIEST" />
        <Career name="ENGINEER" />
        <Career name="BLACK_ORC" />
        <Career name="CHOPPA" />
        <Career name="SHAMAN" />
        <Career name="SQUIG_HERDER" />
        <Career name="WITCH_HUNTER" />
        <Career name="KNIGHT" />
        <Career name="BRIGHT_WIZARD" />
        <Career name="WARRIOR_PRIEST" />
        <Career name="CHOSEN" />
        <Career name="MARAUDER" />
        <Career name="ZEALOT" />
        <Career name="MAGUS" />
        <Career name="SWORDMASTER" />
        <Career name="SHADOW_WARRIOR" />
        <Career name="WHITE_LION" />
        <Career name="ARCHMAGE" />    
      </Careers>
    </WARInfo>
  </UiMod>
</ModuleFile>