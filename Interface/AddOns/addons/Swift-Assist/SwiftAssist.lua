-- credits for the original idea and icons to Yak, where the hell are you ;P
SwiftAssist = {}
local Addon = SwiftAssist
Addon.Settings = {}
Addon.target = L""

local Events = {MYASSIST = 1}--, SCENARIO_MODE_CHANGED = 2}
Addon.Events = Events
Addon.EventCallbacks = Addon.EventCallbacks or {}
local EventCallbacks = Addon.EventCallbacks

local tonumber = tonumber
local string_find = string.find

local SetMacroData = SetMacroData
local SendChatText = SendChatText
local WindowSetShowing = WindowSetShowing
local GetIconData = GetIconData
local LabelSetText = LabelSetText
local DynamicImageSetTexture = DynamicImageSetTexture

local playersName = nil
local function GetPlayersName()
	if (playersName == nil) then
	    playersName = (GameData.Player.name)
	end
	return playersName
end

local playersFormatedName = nil
local function GetPlayersFormatedName()
	if playersFormatedName == nil then
		playersFormatedName = GetPlayersName():match(L"([^^]+)^?([^^]*)")
	end
	return playersFormatedName
end

local function AnnounceSetAssist()
	local serverCmd
	if GameData.Player.isInScenario then
		serverCmd = Addon.Settings.AnnounceScenario
	elseif IsWarBandActive() then
		serverCmd = Addon.Settings.AnnounceWorld
	elseif PartyUtils.IsPartyActive() then
		if Addon.Settings.AnnounceWorld then serverCmd = L"/p" end	-- when set to warband, but in party: fallback to party
	end
	if serverCmd then SendChatText(L"<icon193> "..Addon.target,serverCmd) end
end

local function BroadcastSwiftAssistEvent(Event)
	if EventCallbacks[Event] == nil then return end
	if Event == Events.MYASSIST then
		for _, CallbackFunction in ipairs(EventCallbacks[Event]) do
			CallbackFunction((Addon.target):match(L"([^^]+)^?([^^]*)"))
		end
	end
end
function Addon.RegisterEventHandler(Event, CallbackFunction)
	if EventCallbacks[Event] == nil then EventCallbacks[Event] = {} end
	local isInStock
	for _, callbck in ipairs(EventCallbacks[Event]) do
		if callbck == CallbackFunction then
			isInStock = true
			break
		end
	end
	if not isInStock then
		table.insert(EventCallbacks[Event], CallbackFunction)
	end
	if Event == Events.MYASSIST and Addon.target ~= L"" then	-- Broadcast current immediately
		CallbackFunction(Addon.target)
	end
end
function Addon.UnregisterEventHandler(Event, CallbackFunction)
	for k, callbck in ipairs(EventCallbacks[Event]) do
		if callbck == CallbackFunction then
			table.remove(EventCallbacks[Event], k)
			break
		end
	end
end

local redirect = 0
local assistMacroId = -1
local function WriteLabels(text)
	LabelSetText("SwiftAssistLabel", text)
	LabelSetText("SwiftAssistShadow", text)
end

local function SetTexLabel()
	WriteLabels(towstring(Addon.target))
	local tex,x,y=GetIconData(Icons.GetCareerIconIDFromCareerLine(Addon.career))
	DynamicImageSetTexture("SwiftAssistCareerIcon",tex,x,y)
	WindowSetShowing("SwiftAssistCareerIcon", true)
	WindowSetShowing("SwiftAssistCareerIconFX", true)
end

local function GetMacroId(aWString)
	local macros = DataUtils.GetMacros()
	for slot = 1, EA_Window_Macro.NUM_MACROS do
		if macros[slot] and (macros[slot].text == aWString or macros[slot].name == aWString) then return slot end
	end
end

local function CreateMacro(name, text, iconId)
	local slot = GetMacroId (text)
	if (slot) then return slot end
	local macros = DataUtils.GetMacros()
	for slot = 1, EA_Window_Macro.NUM_MACROS
	do
		if (macros[slot].text == L"")
		then
			SetMacroData (name, text, iconId, slot)
 			EA_Window_Macro.UpdateMacros ()
			return slot
		end
	end
end

local function SlashHandler(cmd)
    local opt, args = cmd:match("([a-zA-Z0-9]+)[ ]?(.*)")
	if opt then opt = opt:lower() end
	if args then args = args:lower() end

	if opt == "announcesetassist" then
		if args == "party" then
			Addon.Settings.AnnounceWorld = L"/p"
			EA_ChatWindow.Print(L"Swiftassist <icon193> Announce in World: Party")
		elseif args == "warband" then
			Addon.Settings.AnnounceWorld = L"/wb"
			EA_ChatWindow.Print(L"Swiftassist <icon193> Announce in World: Warband (Fallback:Party)")
		elseif args == "scenario" then
			Addon.Settings.AnnounceScenario = L"/sc"
			EA_ChatWindow.Print(L"Swiftassist <icon193> Announce in Scenario: Scenario")
		elseif args == "scenariogroup" then
			Addon.Settings.AnnounceScenario = L"/sp"
			EA_ChatWindow.Print(L"Swiftassist <icon193> Announce in Scenario: Scenariogroup")
		elseif args == "worldoff" then
			Addon.Settings.AnnounceWorld = nil
			EA_ChatWindow.Print(L"Swiftassist <icon193> Announce in World: Off")
		elseif args == "scenariooff" then
			Addon.Settings.AnnounceScenario = nil
			EA_ChatWindow.Print(L"Swiftassist <icon193> Announce in Scenario: Off")
		else
			EA_ChatWindow.Print(L"Secondary Arguments to announcesetassist:<br>party, warband, worldoff,<br>scenario, scenariogroup, scenariooff")
		end
	else
		EA_ChatWindow.Print(L"Argument needed! Possible Arguments:<br>announcesetassist")
		EA_ChatWindow.Print(L"Secondary Arguments to announcesetassist:<br>party, warband, worldoff,<br>scenario, scenariogroup, scenariooff")
		
		if assistMacroId ~= -1 then
			EA_ChatWindow.Print(L"Assist Macro found: "..assistMacroId)
		else
			EA_ChatWindow.Print(L"Assist Macro not found!")
		end
	end
end

function Addon.Initialize()
	CreateWindow("SwiftAssist", true)
	WindowSetShowing("SwiftAssistCareerIcon", false)
	WindowSetShowing("SwiftAssistCareerIconFX", false)
	LayoutEditor.RegisterWindow( "SwiftAssist", L"SwiftAssist", L"SwiftAssist", false, false, true, nil )
	WriteLabels(L"")
	
	RegisterEventHandler(SystemData.Events.MACROS_LOADED, "SwiftAssist.OnMacrosLoaded")
	RegisterEventHandler(SystemData.Events.MACRO_UPDATED, "SwiftAssist.OnMacroUpdated")
	
	Addon.OnMacrosLoaded()
	
	if LibSlash then
		LibSlash.RegisterSlashCmd("swiftassist", SlashHandler)
	else
		local orig_ChatWindow_OnKeyEnter = EA_ChatWindow.OnKeyEnter
		EA_ChatWindow.OnKeyEnter = function(...)
				local input = WStringToString(EA_TextEntryGroupEntryBoxTextInput.Text)
				local cmd, args = input:match("^/([a-zA-Z]+)[ ]?(.*)")
				if cmd then
					cmd = cmd:lower()
					if cmd == "swiftassist" then
						SlashHandler(args)
						EA_TextEntryGroupEntryBoxTextInput.Text = L""
					end
				end
				return orig_ChatWindow_OnKeyEnter(...)
			end
	end
end

function Addon.OnMacrosLoaded()
	if ( not GetMacroId(L"/script SwiftAssist.SetAssist()") ) then CreateMacro(L"SetAssist", L"/script SwiftAssist.SetAssist()", 193) end
	--if ( not GetMacroId(L"/script SwiftAssist.SetMark()") ) then CreateMacro(L"ySetMark", L"/script SwiftAssist.SetMark()", 190) end
	assistMacroId = GetMacroId(L"Assist")
	if ( not assistMacroId) then assistMacroId = CreateMacro(L"Assist", L"/assist", 177) end
end

function Addon.OnMacroUpdated(macroId)
	if macroId == assistMacroId then
		if Addon.target ~= L"" then
			SetTexLabel()
		else
			WriteLabels(L"")
			WindowSetShowing("SwiftAssistCareerIcon", false)
			WindowSetShowing("SwiftAssistCareerIconFX", false)
		end
		BroadcastSwiftAssistEvent(Events.MYASSIST)
		AnnounceSetAssist()
	end
end

--[[function Addon.Mark()
	if redirect == 0 then 
		-- trigger main assist
	elseif redirect == 1 then
		Enemy.Mark()
	end
end]]--

function Addon.SetAssist()
	local actwnd = SystemData.MouseOverWindow.name
	local needsUpdate
	if actwnd == "EnemyIcon" or actwnd == "WarBoard_TogglerEnemy" then
		--[[redirect = 1
		DynamicImageSetTexture("SwiftAssistCareerIcon","SwiftAssistRedirect",0,0)
		WriteLabels(L"Enemy")
		WindowSetShowing("SwiftAssistCareerIcon", true)
		WindowSetShowing("SwiftAssistCareerIconFX", true)]]--
	else
		local isSetter = false
		if Effigy and Effigy.GetMouseOverFormationIndexes then
			local groupIndex,memberIndex=Effigy.GetMouseOverFormationIndexes()
			if memberIndex then
				isSetter = true
				local State = (groupIndex and EffigyState:GetState("Formation"):GetChildState(memberIndex,groupIndex)) or EffigyState:GetState("Party"):GetChildState(memberIndex)
				local name = State:GetExtraInfo("rawname")
				if name ~= GetPlayersName() and name ~= Addon.target then
					Addon.target = name
					Addon.career = State:GetExtraInfo("careerLine")
					redirect = 0
					needsUpdate = true
				end
			end
		end
		if isSetter == false then
			if Squared then
				local _,_,squaredgrp,squaredmember=string_find(actwnd,"^SquaredUnit_(%d+)_(%d+)Action")
				squaredgrp = tonumber(squaredgrp)
				squaredmember = tonumber(squaredmember)
				if squaredgrp ~= nil and squaredmember ~= nil then
					isSetter = true
					local squaredtarget = Squared.GetUnit(squaredgrp,squaredmember)
					local name = squaredtarget.rawname
					if name ~= GetPlayersName() and name ~= Addon.target then
						Addon.target = name
						Addon.career = squaredtarget.career
						redirect = 0
						needsUpdate = true
					end
				end
			end
			if isSetter == false then
				if Enemy then
					local group_index, index, frame = Enemy._GetUnitFrameFromWindowName (SystemData.MouseOverWindow.name)
					if frame then
						isSetter = true
						local name = frame.playerName
						if name ~= GetPlayersFormatedName() and name ~= Addon.target then						
							Addon.target = name
							Addon.career = frame.playerCareer
							redirect = 0
							needsUpdate = true
						end
					end
				end
				if isSetter == false then
					TargetInfo:UpdateFromClient()
					local name = TargetInfo:UnitName ("selffriendlytarget")
					if name ~= Addon.target then
						if name ~= L"" and name ~= GetPlayersName() then
							local ftCareer = TargetInfo:UnitCareer("selffriendlytarget")
							if ftCareer ~= 0 then
								Addon.target = name
								Addon.career = ftCareer
								redirect = 0
								needsUpdate = true
							else
								EA_ChatWindow.Print(L"You cannot assist NPCs or pets!")
								return
							end
						else
							d(L"Resetting Assist")
							Addon.target = L""
							Addon.career = 0
							redirect = 0
							needsUpdate = true
						end
					end
				end
			end
		end
	end
	if needsUpdate then
		SetMacroData (L"Assist", L"/assist"..(((Addon.target ~= L"") and L" "..Addon.target) or L""), 177, assistMacroId)	-- rewrite assist macro
	end
end
