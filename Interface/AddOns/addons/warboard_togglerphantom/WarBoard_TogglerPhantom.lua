if not WarBoard_TogglerPhantom then WarBoard_TogglerPhantom = {} end
local WarBoard_TogglerPhantom = WarBoard_TogglerPhantom
local modName = "WarBoard_TogglerPhantom"
local modLabel = "Phantom"

function WarBoard_TogglerPhantom.Initialize()
	if LibWBToggler.CreateToggler(modName, modLabel, "PhantomIcon", 0, 0) then
		WindowSetDimensions(modName, 110, 30)
		WindowSetDimensions(modName.."Label", 78, 30)
		LibWBToggler.RegisterEvent(modName, "OnLButtonUp", "WarBoard_TogglerPhantom.OptionsWindow")
		LibWBToggler.RegisterEvent(modName, "OnMouseOver", "WarBoard_TogglerPhantom.ShowStatus")
	end
end

function WarBoard_TogglerPhantom.OptionsWindow()
	WindowSetShowing("PhantomSettings", not WindowGetShowing("PhantomSettings"))
end

function WarBoard_TogglerPhantom.ShowStatus()
	LibWBToggler.DefaultTooltip(modName, modLabel)
end
