WarBoard_TogglerWARCommander = WarBoard_TogglerWARCommander or {}
local WarBoard_TogglerWARCommander = WarBoard_TogglerWARCommander
local modName = "WarBoard_TogglerWARCommander"
local modTitle = "Toggler WARCommander"
local modLabel = "WAR"

function WarBoard_TogglerWARCommander.Initialize()
	if LibWBToggler.CreateToggler( modName, modLabel, "map_markers01", 28, 33, "FlagDestruction", 0.8 ) then
		local textWidth, _ = LabelGetTextDimensions(modName.."Label")
		WindowSetDimensions(modName, textWidth + 37, 30)
		WindowSetDimensions(modName.."Label", textWidth + 2, 30)
		LibWBToggler.RegisterEvent( modName, "OnLButtonUp", "WarBoard_TogglerWARCommander.ToggleVisibility" )
		LibWBToggler.RegisterEvent( modName, "OnRButtonUp", "WarBoard_TogglerWARCommander.ToggleConfig" )
		LibWBToggler.RegisterEvent( modName, "OnMouseOver", "WarBoard_TogglerWARCommander.ShowStatus" )
		if WARCommander_config.hide then
            LabelSetTextColor(modName.."Label", 255, 0, 0)
            DynamicImageSetTextureSlice(modName.."Icon", "FlagDestruction")
        else
            LabelSetTextColor(modName.."Label", 0, 255, 0)
            DynamicImageSetTextureSlice(modName.."Icon", "FlagOrder")
        end
	end
end

function WarBoard_TogglerWARCommander.ToggleVisibility()
	WARCommander.ToggleVisibility()
	if WARCommander_config.hide then
		LabelSetTextColor(modName.."Label", 255, 0, 0)
		DynamicImageSetTextureSlice(modName.."Icon", "FlagDestruction")
	else
		LabelSetTextColor(modName.."Label", 0, 255, 0)
		DynamicImageSetTextureSlice(modName.."Icon", "FlagOrder")
	end
end

function WarBoard_TogglerWARCommander.ToggleConfig()
	WindowSetShowing("WARCommander_ConfigWindow", not WindowGetShowing("WARCommander_ConfigWindow"))
end

function WarBoard_TogglerWARCommander.ShowStatus()
	LibWBToggler.DefaultTooltip(modName, modTitle)
end
