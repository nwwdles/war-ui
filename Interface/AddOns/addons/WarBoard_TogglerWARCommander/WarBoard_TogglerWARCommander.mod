<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="WarBoard_TogglerWARCommander" version="0.2" date="08/20/2009" >

		<Author name="Garkin" email="garkin@bigboss.cz" />
		<Description text="WARCommander Toggler module created with LibWBToggler." />

		<VersionSettings gameVersion="1.3.1" />

		<WARInfo>
			<Categories>
				<Category name="OTHER" />
			</Categories>
			<Careers>
				<Career name="BLACKGUARD" />
				<Career name="WITCH_ELF" />
				<Career name="DISCIPLE" />
				<Career name="SORCERER" />
				<Career name="IRON_BREAKER" />
				<Career name="SLAYER" />
				<Career name="RUNE_PRIEST" />
				<Career name="ENGINEER" />
				<Career name="BLACK_ORC" />
				<Career name="CHOPPA" />
				<Career name="SHAMAN" />
				<Career name="SQUIG_HERDER" />
				<Career name="WITCH_HUNTER" />
				<Career name="KNIGHT" />
				<Career name="BRIGHT_WIZARD" />
				<Career name="WARRIOR_PRIEST" />
				<Career name="CHOSEN" />
				<Career name="MARAUDER" />
				<Career name="ZEALOT" />
				<Career name="MAGUS" />
				<Career name="SWORDMASTER" />
				<Career name="SHADOW_WARRIOR" />
				<Career name="WHITE_LION" />
				<Career name="ARCHMAGE" />
			</Careers>
		</WARInfo>


		<Dependencies>
			<Dependency name="LibWBToggler" />
			<Dependency name="WARCommander" forceEnable="false" />
		</Dependencies>

		<Files>
			<File name="WarBoard_TogglerWARCommander.lua" />
		</Files>

		<OnInitialize>
			<CallFunction name="WarBoard_TogglerWARCommander.Initialize" />
		</OnInitialize>
		
	</UiMod>
</ModuleFile>