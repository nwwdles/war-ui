<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="NPC Item Sale Price" version="0.3.3" date="25/02/2011" >
        <VersionSettings gameVersion="1.4.0" windowsVersion="1.0" savedVariablesVersion="1.0" />
        <Author name="Sniperumm" email="sniperumm@hotmail.com" />
        <Description text="Adds the npc item sale price into tooltip" />
        
        <Dependencies>
			    <Dependency name="EASystem_Tooltips"/>
			    <Dependency name="EA_BackpackWindow"/>
          <Dependency name="LibSlash" />
          <Dependency name="EA_ChatWindow"/>
        </Dependencies>
        
        <Files>
            <File name="Source/Nisp.lua" />
        </Files>
        
        <OnInitialize>
            <CallFunction name="Nisp.Init" />
        </OnInitialize>
        <OnUpdate/>
        <OnShutdown/>
        <SavedVariables>
          <SavedVariable name="Nisp.Enabled" global="true" />
          <SavedVariable name="Nisp.DebugEnabled" global="true" />
          <SavedVariable name="Nisp.DumpItemsTable" global="true" />
        </SavedVariables>
    </UiMod>
</ModuleFile>
