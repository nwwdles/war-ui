--------------------------------------------------------------------------------
-- Copyright (c) 2008 Bloodwalker <metagamegeek@gmail.com>
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
--
--------------------------------------------------------------------------------
--
-- Starting points for zone offsets and mapsize taken from Nevir27's work on
-- LibSurveyor, all other data and code is original
--
--------------------------------------------------------------------------------
-- File:      Source/MapMonster_PinTypeEditorWindow.lua
-- Date:      2012-08-17T12:40:42Z
-- Author:    Philosound
-- Version:   v0.96
-- Revision:  123
-- File Rev:  123
-- Copyright: 2008
--------------------------------------------------------------------------------
local T = LibStub("WAR-AceLocale-3.0"):GetLocale("MapMonster-Editor")
local LibToolkit = LibStub("LibToolkit-0.1")

local CLEAN_MODE = 1
local VIEW_MODE = 2
local PINTYPE_EDIT = 3
local SUBTYPE_EDIT = 4

local ERROR_COLOR = DefaultColor.RED
local MESSAGE_COLOR = DefaultColor.GREEN

local chooserWindow = "MapMonster_IconChooserWindow"
local chooserOrder = {}
-- Table of icons definitions for the chooser window
local TextureList = {
	{ texture = "MMNav_WaypointPinTintable", scale = 0.438 },
	{ texture = "map_markers01", scale = 0.875, slice = "Waypoint-Large" },
	{ texture = "icon023000", scale = 0.87 },
	{ texture = "icon023002", scale = 0.87 },
	{ texture = "icon023005", scale = 0.87 },
	{ texture = "icon023007", scale = 0.87 },
	{ texture = "icon023008", scale = 0.87 },
	{ texture = "icon023011", scale = 0.87 },
	{ texture = "EA_Apothecary01_d5", scale = 1.3, slice = "White-Orb" }, -- White Orb 21 x 20
	{ texture = "EA_Texture_Menu01", scale = 0.39, slice = "Grouping-Button-Highlighted" }, -- 73 x 71
	{ texture = "EA_Texture_Menu01", scale = 0.39, slice = "AccentGlow" },
	{ texture = "EA_ScenarioSummary01_d5", scale = 0.49, slice = "order-symbol" }, -- 59 x 56
	{ texture = "EA_ScenarioSummary01_d5", scale = 0.49, slice = "destruction-symbol" }, -- 59 x 56
	{ texture = "EA_HUD_01", scale = 0.65, slice = "RvR-Flag" },
	{ texture = "EA_Tome", scale = 1.08, slice = "MiniSection-Achievements" },
	{ texture = "EA_Tome", scale = 1.08, slice = "MiniSection-Armory" },
	{ texture = "EA_Tome", scale = 1.08, slice = "MiniSection-Bestiary" },
	{ texture = "EA_Tome", scale = 1.08, slice = "MiniSection-Chapters" },
	{ texture = "EA_Tome", scale = 1.08, slice = "MiniSection-Lore" },
	{ texture = "EA_Tome", scale = 1.08, slice = "MiniSection-Map" },
	{ texture = "EA_Tome", scale = 1.08, slice = "MiniSection-Quests" },
	{ texture = "EA_Tome", scale = 1.08, slice = "MiniSection-Rewards" },
	{ texture = "map_markers01", scale = 0.875, slice = "Influence-Teal-ROLLOVER" },
	{ texture = "map_markers01", scale = 0.875, slice = "QuestAvailable-Green" },
	{ texture = "map_markers01", scale = 0.875, slice = "QuestActive-ROLLOVER" },
	{ texture = "map_markers01", scale = 0.875, slice = "QuestCompleted-Gold-ROLLOVER" },
	{ texture = "map_markers01", scale = 0.875, slice = "QuestEpic-ROLLOVER" },
	{ texture = "map_markers01", scale = 0.875, slice = "QuestGroup-ROLLOVER" },
	{ texture = "map_markers01", scale = 0.875, slice = "QuestLocal-White-ROLLOVER" },
	{ texture = "map_markers01", scale = 0.875, slice = "QuestMovement-ROLLOVER" },
	{ texture = "map_markers01", scale = 0.875, slice = "QuestRVRGroup-ROLLOVER" },
	{ texture = "map_markers01", scale = 0.875, slice = "QuestRVRkill-ROLLOVER" },
	{ texture = "map_markers01", scale = 0.875, slice = "QuestRVR-ROLLOVER" },
	{ texture = "map_markers01", scale = 0.875, slice = "QuestTome-ROLLOVER" },
	{ texture = "map_markers01", scale = 0.94, slice = "NPC-Scenario" },
	{ texture = "map_markers01", scale = 0.94, slice = "Player-LFG" },
	{ texture = "map_markers01", scale = 0.9, slice = "Shout-Attack" },
	{ texture = "map_markers01", scale = 1.5, slice = "Shout-DefendMe" },
	{ texture = "map_markers01", scale = 1.25, slice = "Shout-HealMe" },
	{ texture = "map_markers01", scale = 1.15, slice = "Shout-Incoming" },
	{ texture = "map_markers01", scale = 0.875, slice = "GuildStandard" },
	{ texture = "map_markers01", scale = 1, slice = "FlagNeutral" },
	{ texture = "map_markers01", scale = 1, slice = "FlagNeutral-Burning" },
	{ texture = "map_markers01", scale = 1, slice = "FlagDestruction" },
	{ texture = "map_markers01", scale = 1, slice = "FlagDestruction-Burning" },
	{ texture = "map_markers01", scale = 1, slice = "FlagOrder" },
	{ texture = "map_markers01", scale = 1, slice = "FlagOrder-Burning" },
	{ texture = "map_markers01", scale = 1, slice = "MurderballDestruction" },
	{ texture = "map_markers01", scale = 1, slice = "MurderballOrder" },
	{ texture = "map_markers01", scale = 1, slice = "MurderballNeutral" },
	{ texture = "map_markers01", scale = 0.9, slice = "BombDestruction" },
	{ texture = "map_markers01", scale = 0.9, slice = "BombOrder" },
	{ texture = "map_markers01", scale = 0.9, slice = "BombNeutral" },
	{ texture = "map_markers01", scale = 1, slice = "Small-RVR-Hot-Spot" },
	{ texture = "map_markers01", scale = 0.9, slice = "RvR-Hotspot" },
	{ texture = "map_markers01", scale = 0.725, slice = "BIG-RVR-Hot-Spot" },
	{ texture = "map_markers01", scale = 0.875, slice = "GuildStandard" },
	{ texture = "map_markers01", scale = 0.9, slice = "Librarian" },
	{ texture = "map_markers01", scale = 0.875, slice = "PQ-Large" },
	{ texture = "map_markers01", scale = 1, slice = "KillCollector" },
	{ texture = "map_markers01", scale = 0.9, slice = "NPC-Merchant" },
	{ texture = "map_markers01", scale = 0.875, slice = "NPC-TrainerActive" },
	{ texture = "map_markers01", scale = 1, slice = "NPC-Binder" },
	{ texture = "map_markers01", scale = 0.875, slice = "Mail-Large" },
	{ texture = "map_markers01", scale = 0.875, slice = "NPC-Travel" },
	{ texture = "map_markers01", scale = 0.875, slice = "NPC-Healer-Large" },
	{ texture = "map_markers01", scale = 0.875, slice = "Auctioneer" },
	{ texture = "map_markers01", scale = 0.875, slice = "NPC-GuildRegistrar-Large" },
	{ texture = "EA_HUD_01", scale = 1, slice = "City-Rating-BIG-Star" }, -- 28 x 30
	{ texture = "EA_HUD_01", scale = 0.9, slice = "PartyMarker-Fist" }, -- 30 x 33
	{ texture = "EA_HUD_01", scale = 0.9, slice = "PartyMarker-Moon" },
	{ texture = "EA_HUD_01", scale = 0.9, slice = "PartyMarker-Shield" },
	{ texture = "EA_HUD_01", scale = 0.9, slice = "PartyMarker-Skull" },
	{ texture = "EA_HUD_01", scale = 0.9, slice = "PartyMarker-Sun" },
	{ texture = "EA_HUD_01", scale = 0.9, slice = "PartyMarker-Sword" },
	{ texture = "EA_HUD_01", scale = 1.25, slice = "Clock" }, -- 22 x 23
	{ texture = "EA_Backpack01_d5", scale = 0.6, slice = "Bag1-Closed" }, -- 46 x 50
	{ texture = "EA_Backpack01_d5", scale = 0.6, slice = "Bag2-Closed" }, -- 46 x 50
	{ texture = "EA_Backpack01_d5", scale = 0.6, slice = "Bag3-Closed" }, -- 46 x 50
	{ texture = "EA_Backpack01_d5", scale = 0.6, slice = "Bag4-Closed" }, -- 46 x 50
	{ texture = "EA_Backpack01_d5", scale = 0.6, slice = "Bag5-Closed" }, -- 46 x 50
	{ texture = "EA_MailWindow01_32b", scale = 0.43, slice = "mail-unread" }, -- 65 x 62
	{ texture = "EA_MailWindow01_32b", scale = 0.45, slice = "package-unread" }, --65 x 62
	{ texture = "EA_Texture_Menu01", scale = 0.39, slice = "Scenario-Grouping-Button-Highlighted" }, -- 73 x 71
	{ texture = "EA_Texture_Menu01", scale = 0.39, slice = "PaperDoll-Button-Selected-Highlighted" },
	{ texture = "EA_Texture_Menu01", scale = 0.39, slice = "Guild-Button-Highlighted" },
	{ texture = "EA_Texture_Menu01", scale = 0.39, slice = "Bag-Button-Highlighted" },
	{ texture = "EA_Texture_Menu01", scale = 0.39, slice = "Tome-Button-Selected-NEW-ENTRIES" },
	{ texture = "EA_PQLoot", scale = 0.95, slice = "PQHeaderIconLoot" },
	{ texture = "EA_PQLoot", scale = 1.2, slice = "PQSackBlue" },
	{ texture = "EA_PQLoot", scale = 1.2, slice = "PQSackGold" },
	{ texture = "EA_PQLoot", scale = 1.2, slice = "PQSackGreen" },
	{ texture = "EA_PQLoot", scale = 1.2, slice = "PQSackPurple" },
	{ texture = "EA_PQLoot", scale = 1.2, slice = "PQSackSilver" },
	{ texture = "EA_ScenarioSummary01_d5", scale = 0.87, slice = "multi-skulls-grey" }, -- 30 x 33
	{ texture = "EA_ScenarioSummary01_d5", scale = 0.8, slice = "crown-grey" }, -- 35 x 23
	{ texture = "EA_Campaign01_d5", scale = 0.46, slice = "City-CONTESTED-MINI" }, -- 59 x 61
	{ texture = "EA_Campaign01_d5", scale = 0.46, slice = "City-Dest-Chaos-MINI" },
	{ texture = "EA_Campaign01_d5", scale = 0.46, slice = "City-Dest-DarkElf-MINI" },
	{ texture = "EA_Campaign01_d5", scale = 0.46, slice = "City-Dest-Greenskin-MINI" },
	{ texture = "EA_Campaign01_d5", scale = 0.46, slice = "City-Order-Dwarf-MINI" },
	{ texture = "EA_Campaign01_d5", scale = 0.46, slice = "City-Order-Empire-MINI" },
	{ texture = "EA_Campaign01_d5", scale = 0.46, slice = "City-Order-HighElf-MINI" },
	{ texture = "EA_Campaign01_d5", scale = 0.32, slice = "City-Travel-Available" }, -- 87 x 88
	{ texture = "EA_Campaign01_d5", scale = 0.32, slice = "City-Travel-Current" },
	{ texture = "EA_Campaign01_d5", scale = 0.32, slice = "City-Travel-Unavailable" },
	{ texture = "EA_Campaign01_d5", scale = 0.52, slice = "Wing-CONTESTED" }, -- 54 x 54
	{ texture = "EA_Campaign01_d5", scale = 0.52, slice = "Wing-Dest-Chaos" },
	{ texture = "EA_Campaign01_d5", scale = 0.52, slice = "Wing-Dest-DarkElf" },
	{ texture = "EA_Campaign01_d5", scale = 0.52, slice = "Wing-Dest-Greenskin" },
	{ texture = "EA_Campaign01_d5", scale = 0.52, slice = "Wing-Order-Dwarf" },
	{ texture = "EA_Campaign01_d5", scale = 0.52, slice = "Wing-Order-Empire" },
	{ texture = "EA_Campaign01_d5", scale = 0.52, slice = "Wing-Order-HighElf" },
	{ texture = "EA_Campaign01_d5", scale = 0.51, slice = "Wing-Travel-Available" }, -- 55 x 54
	{ texture = "EA_Campaign01_d5", scale = 0.51, slice = "Wing-Travel-Current" },
	{ texture = "EA_Campaign01_d5", scale = 0.51, slice = "Wing-Travel-Unavailable" },
	-- Career Icons from icon.xml
	{ texture = "icon020180", scale = 0.87 },
	{ texture = "icon020181", scale = 0.87 },
	{ texture = "icon020182", scale = 0.87 },
	{ texture = "icon020183", scale = 0.87 },
	{ texture = "icon020184", scale = 0.87 },
	{ texture = "icon020185", scale = 0.87 },
	{ texture = "icon020186", scale = 0.87 },
	{ texture = "icon020187", scale = 0.87 },
	{ texture = "icon020188", scale = 0.87 },
	{ texture = "icon020189", scale = 0.87 },
	{ texture = "icon020190", scale = 0.87 },
	{ texture = "icon020191", scale = 0.87 },
	{ texture = "icon020192", scale = 0.87 },
	{ texture = "icon020193", scale = 0.87 },
	{ texture = "icon020194", scale = 0.87 },
	{ texture = "icon020195", scale = 0.87 },
	{ texture = "icon020196", scale = 0.87 },
	{ texture = "icon020197", scale = 0.87 },
	{ texture = "icon020198", scale = 0.87 },
	{ texture = "icon020199", scale = 0.87 },
	{ texture = "icon020200", scale = 0.87 },
	{ texture = "icon020201", scale = 0.87 },
	{ texture = "icon020202", scale = 0.87 },
	{ texture = "icon020203", scale = 0.87 },
}

-- Creates the multi dimensional table to use in the chooser window
local MapIconList = {}
local rowCount = 1
local iconCount = 0
for k, v in pairs(TextureList) do
	iconCount = iconCount + 1
	if iconCount == 6 then
		iconCount = 1
		rowCount = rowCount + 1
	end
	if not MapIconList[rowCount] then
		MapIconList[rowCount] = {}
	end
	MapIconList[rowCount][iconCount] = v
end

local editorWindow = "MapMonster_PinTypeEditorWindow"
local editorMode = CLEAN_MODE
local editorData = {}
local iconData = {}
local EDITOR_ICONSCALE = 2.3

local mouseOverDesc = {}
mouseOverDesc[1] = T["PinTypeEditDesc"]
mouseOverDesc[2] = T["SubTypeComboDesc"]
mouseOverDesc[3] = T["SubTypeEditDesc"]
mouseOverDesc[4] = T["MapIconDesc"]
mouseOverDesc[5] = T["RadiusEditDesc"]
mouseOverDesc[6] = T["LockedDesc"]
mouseOverDesc[7] = T["PrivateDesc"]

local subTypeList = {}
local subTypeCombo = {}
local subTypeComboRev = {}

local allEditorFields = { ["PinTypeEditBox"] 		 = false,
						  ["PinTypeLabel"] 			 = false,
						  ["SubTypeComboBox"] 		 = false,
						  ["SubTypeEditBox"] 		 = false,
						  ["SubTypeLabel"] 			 = false,
						  ["MapPinIcon"] 			 = true,
						  ["CustomColorRedSlider"] 	 = false,
						  ["CustomColorGreenSlider"] = false,
						  ["CustomColorBlueSlider"]  = false,
						  ["RadiusEditBox"] 		 = false,
						  ["RadiusLabel"] 			 = false,
						  ["IsLockedCheckBox"] 		 = true,
						  ["IsPrivateCheckBox"] 	 = true,
						  ["LeftButton"]             = false,
						  ["MiddleButton"]           = false,
						  ["RightButton"]            = false, }

local pinTypeEditFields = { "PinTypeEditBox",
						  "SubTypeComboBox",
						  "CustomColorRedSlider",
						  "CustomColorGreenSlider",
						  "CustomColorBlueSlider",
						  "RadiusEditBox",
						  "IsLockedCheckBox",
						  "IsPrivateCheckBox",
						  "LeftButton",
						  "MiddleButton",
						  "RightButton", }

local subTypeEditFields = { "PinTypeLabel",
						  "SubTypeEditBox",
						  "CustomColorRedSlider",
						  "CustomColorGreenSlider",
						  "CustomColorBlueSlider",
						  "RadiusEditBox",
						  "IsLockedCheckBox",
						  "IsPrivateCheckBox",
						  "LeftButton",
						  "MiddleButton",
						  "RightButton", }

local pinTypeViewFields = { "PinTypeLabel",
						  "SubTypeLabel",
						  "CustomColorRedSlider",
						  "CustomColorGreenSlider",
						  "CustomColorBlueSlider",
						  "RadiusLabel",
						  "IsLockedCheckBox",
						  "IsPrivateCheckBox", 
						  "MiddleButton", }

--------------------------------------------------------------------------------
--#
--#			Local Window Handler Functions
--#
--------------------------------------------------------------------------------
local function GetPinDataFromEditor()

	if editorMode == VIEW_MODE or editorMode == CLEAN_MODE then
		return false
	end
	
	local editorFields = { mapIcon = {} }
	local newOptions = {}
	local tint = { r = 0, g = 0, b = 0 }
	
	for windowPiece, _ in pairs(allEditorFields) do
		if string.match(windowPiece, "Label$") then
			editorFields[windowPiece] = LabelGetText(editorWindow .. windowPiece)
		elseif string.match(windowPiece, "EditBox$") then
			editorFields[windowPiece] = TextEditBoxGetText(editorWindow .. windowPiece)
		elseif string.match(windowPiece, "CheckBox$") then
			editorFields[windowPiece] = ButtonGetPressedFlag(editorWindow .. windowPiece)
		elseif string.match(windowPiece, "ComboBox$") then
			local selected = ComboBoxGetSelectedMenuItem(editorWindow .. windowPiece)
			local selectedValue
			if windowPiece == "SubTypeComboBox" then
				selectedValue = subTypeCombo[selected]
			end
			editorFields[windowPiece] = selectedValue
		elseif string.match(windowPiece, "Icon$") then
			editorFields.mapIcon.texture = iconData.texture
			editorFields.mapIcon.scale = iconData.scale
			editorFields.mapIcon.slice = iconData.slice
		elseif string.match(windowPiece, "Slider$") then
			local sliderValue = SliderBarGetCurrentPosition(editorWindow .. windowPiece)
			if windowPiece == "CustomColorRedSlider" then
				tint.r = sliderValue * 255
			elseif windowPiece == "CustomColorGreenSlider" then
				tint.g = sliderValue * 255
			elseif windowPiece == "CustomColorBlueSlider" then
				tint.b = sliderValue * 255			
			end
		end
	end

	if tint.r ~= 255 or tint.g ~= 255 or tint.b ~= 255 then
		editorFields.mapIcon.tint = tint
	end

	if editorMode == PINTYPE_EDIT then
		newOptions.label = editorFields.PinTypeLabel
		newOptions.defaultSubType = WStringToString(editorFields.SubTypeLabel)
	elseif editorMode == SUBTYPE_EDIT then
		newOptions.label = editorFields.SubTypeLabel
	end
	newOptions.id = editorData.id
	newOptions.radius = tonumber(editorFields.RadiusLabel)
	newOptions.isLocked = editorFields.IsLockedCheckBox
	newOptions.isPrivate = editorFields.IsPrivateCheckBox
	newOptions.mapIcon = editorFields.mapIcon

	return newOptions

end

local function PopulateSubTypeCombo( subTypesList )
	
	subTypeList = {}
	subTypeCombo = {}
	subTypeComboRev = {}
	
	for k, subData in pairs(subTypesList) do
		table.insert(subTypeList, { name = subData.label, id = subData.id })
	end
	table.sort(subTypeList, MapMonster.CustomSortByName)

	ComboBoxClearMenuItems(editorWindow .. "SubTypeComboBox")
	local counter = 0
	for i, v in pairs(subTypeList) do
		counter = counter + 1
		ComboBoxAddMenuItem(editorWindow .. "SubTypeComboBox", v.name)
		subTypeComboRev[v.id] = counter
		table.insert(subTypeCombo, v.id)
	end
	
end

local function SetMapIcon(mapIcon)

	iconData = {}
	iconData.texture = mapIcon.texture
	iconData.scale = mapIcon.scale
	
	DynamicImageSetTexture(editorWindow .. "MapPinIcon", mapIcon.texture, mapIcon.x or 0, mapIcon.y or 0)
	DynamicImageSetTextureScale(editorWindow .. "MapPinIcon", mapIcon.scale * EDITOR_ICONSCALE)
	if mapIcon.slice then
		DynamicImageSetTextureSlice(editorWindow .. "MapPinIcon", mapIcon.slice)
		iconData.slice = mapIcon.slice
	end
	local tint
	if mapIcon.tint then
		tint = mapIcon.tint
	else
		tint = DefaultColor.ZERO_TINT
	end
	SliderBarSetCurrentPosition( editorWindow .. "CustomColorRedSlider", tint.r / 255 )
	SliderBarSetCurrentPosition( editorWindow .. "CustomColorGreenSlider", tint.g / 255 )
	SliderBarSetCurrentPosition( editorWindow .. "CustomColorBlueSlider", tint.b / 255 )
	WindowSetTintColor(editorWindow .. "MapPinIcon", tint.r, tint.g, tint.b)

end

local function SetComboBox( comboBox, selection )

	if comboBox == "SubType" then
		ComboBoxSetSelectedMenuItem(editorWindow .. comboBox .. "ComboBox", selection)
		LabelSetText(editorWindow .. comboBox .. "Label", towstring(subTypeList[selection].id) )
	end
	
end

local function SetEditorMessage(msgString, errorFlag)
	
	--[===[@alpha@
	d("Set Editor Window Message")
	--@end-alpha@]===]
	local message = towstring(msgString)
	local color
	if errorFlag then
		color = ERROR_COLOR
	else
		color = MESSAGE_COLOR
	end
	LabelSetText(editorWindow .. "MessageBox", message)
	LabelSetTextColor(editorWindow .. "MessageBox", color.r, color.g, color.b)
	
end

local function ShowEditorFields()

	--[===[@alpha@
	d("Show Editor Fields")
	--@end-alpha@]===]
	local filedList
	if editorMode == VIEW_MODE then
		fieldList = pinTypeViewFields
	elseif editorMode == PINTYPE_EDIT then
		fieldList = pinTypeEditFields
	elseif editorMode == SUBTYPE_EDIT then
		fieldList = subTypeEditFields
	end

	for _, windowPiece in ipairs(fieldList) do
		WindowSetShowing(editorWindow .. windowPiece, true)
	end
	
end

local function CleanEditorWindow()

	MapMonster.PinTypeEditor.CloseChooser()
	editorMode = CLEAN_MODE
	editorData = {}

	LabelSetText(editorWindow .. "TitleBarText", 	T["Map Monster PinType Editor"])
	LabelSetText(editorWindow .. "SubTypeHeader", 	T["Default Sub Type:"])
	
	-- Empty and hide all editor labels and boxes 
	for windowPiece, showing in pairs(allEditorFields) do
		if ( string.match(windowPiece, "Label$") ) then
			LabelSetText(editorWindow .. windowPiece, L"")
		elseif ( string.match(windowPiece, "EditBox$") ) then
			TextEditBoxSetText(editorWindow .. windowPiece, L"")
		elseif ( string.match(windowPiece, "CheckBox$") ) then
			ButtonSetPressedFlag(editorWindow .. windowPiece, false)
			ButtonSetDisabledFlag(editorWindow .. windowPiece, false)
		elseif ( string.match(windowPiece, "ComboBox$") ) then
			ComboBoxClearMenuItems(editorWindow .. windowPiece)
		elseif ( string.match(windowPiece, "Slider$") ) then
			SliderBarSetCurrentPosition( editorWindow .. windowPiece, 0 )
		elseif ( string.match(windowPiece, "Icon$") ) then
			DynamicImageSetTexture( editorWindow .. windowPiece, "EA_HUD_01", 0, 0 )
			DynamicImageSetTextureScale( editorWindow .. windowPiece, 1.6 )
			DynamicImageSetTextureSlice( editorWindow .. windowPiece, "RvR-Flag" )
		end
		WindowSetShowing(editorWindow .. windowPiece, showing)
	end
	
	ButtonSetDisabledFlag(editorWindow .. "MapPinButton", false)
	
	SetEditorMessage("")
	-- set button labels		
	ButtonSetText(editorWindow .. "LeftButton",   T["Save"])
	ButtonSetText(editorWindow .. "MiddleButton", T["Reset"])
	ButtonSetText(editorWindow .. "RightButton",  T["Cancel"])
	
end

local function OpenTypeEditorWindow( pinType, subType )

	CleanEditorWindow()
	--add error checking and error message here
	if subType then
		editorMode = SUBTYPE_EDIT
		editorData = MapMonsterAPI.GetPinTypeOptions( pinType, subType )
		LabelSetText(editorWindow .. "TitleBarText", 	T["Map Monster SubType Editor"])
		LabelSetText(editorWindow .. "SubTypeHeader", 	T["Sub Type:"])
		-- Populate boxes
		LabelSetText(editorWindow .. "PinTypeLabel", editorData.parent.label)
		TextEditBoxSetText(editorWindow .. "SubTypeEditBox", editorData.label)
	else
		editorMode = PINTYPE_EDIT
		editorData = MapMonsterAPI.GetPinTypeOptions( pinType )
		-- Populate boxes
		TextEditBoxSetText(editorWindow .. "PinTypeEditBox", editorData.label)
		PopulateSubTypeCombo( editorData.subTypes )
		--ComboBoxSetSelectedMenuItem(editorWindow .. "SubTypeComboBox", subTypeComboRev[editorData.defaultSubType])		
		SetComboBox( "SubType", subTypeComboRev[editorData.defaultSubType] )
	end

	SetMapIcon(editorData.mapIcon)
	
	TextEditBoxSetText(editorWindow .. "RadiusEditBox", towstring(editorData.radius))
	ButtonSetPressedFlag(editorWindow .. "IsLockedCheckBox", editorData.isLocked)
	ButtonSetPressedFlag(editorWindow .. "IsPrivateCheckBox", editorData.isPrivate)

	ShowEditorFields()
	MapMonster.PinTypeEditor.Open()
	
end

local function OpenTypeViewerWindow( pinType, subType )

	CleanEditorWindow()
	--add error checking and error message here
	editorMode = VIEW_MODE
	if subType then
		editorData = MapMonsterAPI.GetPinTypeOptions( pinType, subType )
		LabelSetText(editorWindow .. "TitleBarText", 	T["Map Monster SubType Viewer"])
		LabelSetText(editorWindow .. "SubTypeHeader", 	T["Sub Type:"])
		LabelSetText(editorWindow .. "PinTypeLabel", editorData.parent.label)
		LabelSetText(editorWindow .. "SubTypeLabel", editorData.label)
	else
		editorData = MapMonsterAPI.GetPinTypeOptions( pinType )
		LabelSetText(editorWindow .. "TitleBarText", 	T["Map Monster PinType Viewer"])
		LabelSetText(editorWindow .. "SubTypeHeader", 	T["Default Sub Type:"])	
		LabelSetText(editorWindow .. "PinTypeLabel", editorData.label)
		LabelSetText(editorWindow .. "SubTypeLabel", editorData.subTypes[editorData.defaultSubType].label)
	end

	SetMapIcon(editorData.mapIcon)
	ButtonSetDisabledFlag(editorWindow .. "MapPinButton", true)
	
	LabelSetText(editorWindow .. "RadiusLabel", towstring(editorData.radius))
	
	-- set islocked and isprivate checkboxes
	ButtonSetPressedFlag( editorWindow .. "IsLockedCheckBox", editorData.isLocked )
	ButtonSetDisabledFlag( editorWindow .. "IsLockedCheckBox", true)
	
	ButtonSetPressedFlag( editorWindow .. "IsPrivateCheckBox", editorData.isPrivate )
	ButtonSetDisabledFlag( editorWindow .. "IsPrivateCheckBox", true)

	ButtonSetText(editorWindow .. "MiddleButton", T["Close"])
	
	ShowEditorFields()
	MapMonster.PinTypeEditor.Open()
end

MapMonster.PinTypeEditor = {}
--------------------------------------------------------------------------------
--#
--#			Global Window Handler Functions
--#
--------------------------------------------------------------------------------
function MapMonster.PinTypeEditor.OpenTypeEditor( pinType, subType )
	OpenTypeEditorWindow( pinType, subType )
end

function MapMonster.PinTypeEditor.OpenTypeViewer( pinType, subType )
	OpenTypeViewerWindow( pinType, subType )
end

function MapMonster.PinTypeEditor.Close()
	--[===[@alpha@
	d("PinType Editor Window Close")
	--@end-alpha@]===]
	CleanEditorWindow()
	WindowSetShowing(editorWindow, false)
end

function MapMonster.PinTypeEditor.Open()
	--[===[@alpha@
	d("PinType Editor Window Open")
	--@end-alpha@]===]
	if editorMode ~= CLEAN_MODE then
		WindowSetShowing(editorWindow, true)
	end
end

function MapMonster.PinTypeEditor.OnHidden()
	--[===[@alpha@
	d("PinType Editor On Hidden")
	--@end-alpha@]===]
	MapMonster.PinTypeEditor.CloseChooser()
	if editorMode ~= CLEAN_MODE then
		CleanEditorWindow()
	end
	WindowUtils.RemoveFromOpenList(editorWindow)
end

function MapMonster.PinTypeEditor.OnShown()
	--[===[@alpha@
	d("PinType Editor On Shown")
	--@end-alpha@]===]
	WindowUtils.AddToOpenList(editorWindow, MapMonster.PinTypeEditor.Close, WindowUtils.Cascade.MODE_NONE)
end

function MapMonster.PinTypeEditor.OnSetCustomColor()

	if editorMode == VIEW_MODE then
		local tint
		if editorData.mapIcon.tint then
			tint = editorData.mapIcon.tint
		else
			tint = DefaultColor.ZERO_TINT
		end
		SliderBarSetCurrentPosition( editorWindow .. "CustomColorRedSlider", tint.r / 255 )
		SliderBarSetCurrentPosition( editorWindow .. "CustomColorGreenSlider", tint.g / 255 )
		SliderBarSetCurrentPosition( editorWindow .. "CustomColorBlueSlider", tint.b / 255 )	
	else
		local colorRatio = 0
		local color = {
			r = 0,
			b = 0,
			g = 0,
		}
		colorRatio = SliderBarGetCurrentPosition(editorWindow .. "CustomColorRedSlider")
		color.r = colorRatio * 255
		colorRatio = SliderBarGetCurrentPosition(editorWindow .. "CustomColorGreenSlider")
		color.g = colorRatio * 255
		colorRatio = SliderBarGetCurrentPosition(editorWindow .. "CustomColorBlueSlider")
		color.b = colorRatio * 255
    
		-- Update the color swatch
		WindowSetTintColor(editorWindow .. "MapPinIcon", color.r, color.g, color.b)
	end

end

function MapMonster.PinTypeEditor.OnPinTypeTextChange()
	LabelSetText(editorWindow .. "PinTypeLabel", TextEditBoxGetText(editorWindow .. "PinTypeEditBox"))
end

function MapMonster.PinTypeEditor.OnSubTypeTextChange()
	LabelSetText(editorWindow .. "SubTypeLabel", TextEditBoxGetText(editorWindow .. "SubTypeEditBox"))
end

function MapMonster.PinTypeEditor.OnSubTypeChange( selectedSubType )
	-- clearing the select box triggers this with 0
	if selectedSubType == 0 then
		LabelSetText(editorWindow .. "SubTypeLabel", L"")
		return
	end
	LabelSetText(editorWindow .. "SubTypeLabel", towstring( subTypeList[selectedSubType].id ) )
end

function MapMonster.PinTypeEditor.OnClickChangeButton()
	if WindowGetShowing( chooserWindow ) then
		MapMonster.PinTypeEditor.CloseChooser()
	else
		MapMonster.PinTypeEditor.OpenChooser()
	end
end

function MapMonster.PinTypeEditor.OnRadiusTextChange()
	LabelSetText(editorWindow .. "RadiusLabel", TextEditBoxGetText(editorWindow .. "RadiusEditBox"))
end

function MapMonster.PinTypeEditor.OnLockChanged()

end

function MapMonster.PinTypeEditor.OnPrivateChanged()

end

function MapMonster.PinTypeEditor.MouseOverDescription()
	local windowName = SystemData.MouseOverWindow.name
	local descriptionId = WindowGetId(windowName)

    Tooltips.CreateTextOnlyTooltip (windowName, nil)
    Tooltips.SetTooltipText (1, 1, mouseOverDesc[descriptionId] )
    Tooltips.Finalize()
    
    local anchor = { Point="bottom", RelativeTo="CursorWindow", RelativePoint="topleft", XOffset=0, YOffset=0 }
    Tooltips.AnchorTooltip (anchor)
    Tooltips.SetTooltipAlpha (1)
	
end
--------------------------------------------------------------------------------
--#			Editor Window Button Handler Functions
--------------------------------------------------------------------------------
function MapMonster.PinTypeEditor.LeftButton() -- Save
	
	local newOptions = LibToolkit.TableDiff( GetPinDataFromEditor(), editorData )
	
	local pinType
	local subType
	if editorMode == PINTYPE_EDIT then
		pinType = editorData.id
		subType = nil
	elseif editorMode == SUBTYPE_EDIT then
		pinType = editorData.parent.id
		subType = editorData.id
	end

	if not LibToolkit.IsTableEmpty(newOptions) then
		-- we got something to change
		local success = MapMonsterAPI.SetPinTypeOption( pinType, subType, newOptions )
		if success then
			AlertTextWindow.AddLine (MapMonster.AlertTypes.SUCCESS, T["PinTypeSuccessAlert"])
			MapMonster.PinTypeEditor.Close()
		else
			local code, message = MapMonsterAPI.GetError()
			SetEditorMessage(message, true)
		end
	end
end

function MapMonster.PinTypeEditor.MiddleButton() -- Reset

	if editorMode == VIEW_MODE then
		MapMonster.PinTypeEditor.Close()
	else
		if editorData.defaultSubType then
			MapMonster.PinTypeEditor.OpenTypeEditor( editorData.id )
		else
			MapMonster.PinTypeEditor.OpenTypeEditor( editorData.parent.id, editorData.id )
		end
	end
	
end

function MapMonster.PinTypeEditor.RightButton() -- Cancel
	MapMonster.PinTypeEditor.Close()
end

--------------------------------------------------------------------------------
--#			Chooser Window Button Handler Functions
--------------------------------------------------------------------------------
function MapMonster.PinTypeEditor.CloseChooser()
	--[===[@alpha@
	d("Icon Chooser Window Close")
	--@end-alpha@]===]
	if WindowGetShowing( chooserWindow ) then
		WindowSetShowing(chooserWindow, false)
	end
end

function MapMonster.PinTypeEditor.OpenChooser()
	--[===[@alpha@
	d("Icon Chooser Window Open")
	--@end-alpha@]===]
	WindowSetShowing(chooserWindow, true)
end

function MapMonster.PinTypeEditor.OnHiddenChooser()
	--[===[@alpha@
	d("Icon Chooser On Hidden")
	--@end-alpha@]===]
	WindowUtils.RemoveFromOpenList(chooserWindow)
end

function MapMonster.PinTypeEditor.OnShownChooser()
	--[===[@alpha@
	d("Icon Chooser On Shown")
	--@end-alpha@]===]
	WindowUtils.AddToOpenList(chooserWindow, MapMonster.PinTypeEditor.CloseChooser, WindowUtils.Cascade.MODE_NONE)
end

function MapMonster.PinTypeEditor.OnClickChooseIcon()
	--[===[@alpha@
	d("Icon Chooser Click")
	--@end-alpha@]===]
	local windowName = SystemData.MouseOverWindow.name
	local parentName = WindowGetParent(windowName)
	local iconId = WindowGetId(windowName)
	local rowId = WindowGetId(parentName)
	
	if MapIconList[rowId][iconId] then
		SetMapIcon(MapIconList[rowId][iconId])
	end
	MapMonster.PinTypeEditor.CloseChooser()
	
end

--------------------------------------------------------------------------------
--#			Chooser List Functions
--------------------------------------------------------------------------------
function MapMonster.PinTypeEditor.OrderIcons()

	chooserOrder = {}
	
	for k, v in pairs(MapIconList) do
		table.insert(chooserOrder, k)
	end
	
	ListBoxSetDisplayOrder( chooserWindow .. "List", chooserOrder )
end

function MapMonster.PinTypeEditor.PopulateChooser()
	--[===[@alpha@
	d("Populate Icon Chooser")
	--@end-alpha@]===]
	if not MapMonster_IconChooserWindowList.PopulatorIndices then
		return
	end
	
	for k, row in ipairs(MapMonster_IconChooserWindowList.PopulatorIndices) do
		local rowName = "MapMonster_IconChooserWindowListRow" .. k
		WindowSetId( rowName, row)
		for icon = 1, 5 do
			local iconName = rowName .. "Icon" .. icon
			if MapIconList[row][icon] then
				DynamicImageSetTexture( iconName, MapIconList[row][icon].texture, 0, 0)
				DynamicImageSetTextureScale( iconName, MapIconList[row][icon].scale * EDITOR_ICONSCALE)
				if MapIconList[row][icon].slice then
					DynamicImageSetTextureSlice( iconName, MapIconList[row][icon].slice)
				end
				WindowSetId( iconName, icon)
				WindowSetShowing( iconName, true)
			else
				WindowSetShowing( iconName, false)
			end
		end
	end
end

--------------------------------------------------------------------------------
--#
--#			Global Functions
--#
--------------------------------------------------------------------------------
function MapMonster.PinTypeEditor.Initialize()

	-- Pin Type Editor Window
	CreateWindow( editorWindow, false )

	-- set labels
	LabelSetText(editorWindow .. "TitleBarText", 	T["Map Monster PinType Editor"])

	LabelSetText(editorWindow .. "PinTypeHeader", 	T["Pin Type:"])
	LabelSetText(editorWindow .. "SubTypeHeader", 	T["Default Sub Type:"])

	LabelSetText(editorWindow .. "MapPinIconHeader",T["Map Pin Icon:"])
	ButtonSetText(editorWindow .. "MapPinButton",   T["Change"])
	LabelSetText(editorWindow .. "CustomColorRedText", 	 T["Red"])
	LabelSetText(editorWindow .. "CustomColorGreenText", T["Green"])
	LabelSetText(editorWindow .. "CustomColorBlueText",  T["Blue"])
		
	LabelSetText(editorWindow .. "OptionsHeader",   T["Default Options:"])
	LabelSetText(editorWindow .. "RadiusHeader", 	T["Merge Radius"])
	-- set checkboxes
	LabelSetText(editorWindow .. "IsLockedHeader",  T["Locked"])
	ButtonSetCheckButtonFlag( editorWindow .. "IsLockedCheckBox", true)
	LabelSetText(editorWindow .. "IsPrivateHeader", T["Private"])
	ButtonSetCheckButtonFlag( editorWindow .. "IsPrivateCheckBox", true)
	
	-- set button labels		
	ButtonSetText(editorWindow .. "LeftButton",   T["Save"])
	ButtonSetText(editorWindow .. "MiddleButton", T["Reset"])
	ButtonSetText(editorWindow .. "RightButton",  T["Cancel"])
	
	LabelSetText(editorWindow .. "MessageBox", L"")
		
	-- Icon Chooser Window
	CreateWindow( chooserWindow, false )

	LabelSetText( chooserWindow .. "TitleBarText", 	T["Choose Icon..."])
	
	MapMonster.PinTypeEditor.OrderIcons()
	MapMonster.PinTypeEditor.PopulateChooser()
	
	CleanEditorWindow()

end

function MapMonster.PinTypeEditor.Shutdown()
	if DoesWindowExist(editorWindow) then
		DestroyWindow(editorWindow)
	end
	if DoesWindowExist(chooserWindow) then
		DestroyWindow(chooserWindow)
	end
end


function MapMonster.PinTypeEditor.Dump()
	d(GetPinDataFromEditor())
end
--------------------------------------------------------------------------------
--#
--#			MapMonster API Functions
--#
--------------------------------------------------------------------------------
