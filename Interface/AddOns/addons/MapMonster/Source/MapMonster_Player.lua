--------------------------------------------------------------------------------
-- Copyright (c) 2008 Bloodwalker <metagamegeek@gmail.com>
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
--
--------------------------------------------------------------------------------
--
-- Starting points for zone offsets and mapsize taken from Nevir27's work on
-- LibSurveyor, all other data and code is original
--
--------------------------------------------------------------------------------
-- File:      Source/MapMonster_Player.lua
-- Date:      2012-08-17T12:40:42Z
-- Author:    Philosound
-- Version:   v0.96
-- Revision:  123
-- File Rev:  123
-- Copyright: 2008
--------------------------------------------------------------------------------
if not MapMonster or not MapMonster.CoreLoaded then return end --shortcut loading the file if the core didnt load
local LibToolkit = LibStub("LibToolkit-0.1")

local string_format = string.format
local tonumber = tonumber
local math_atan2 = math.atan2
local math_deg = math.deg

local Player = GameData.Player
local MapGetPointForCoordinates = MapGetPointForCoordinates
local MapGetCoordinatesForPoint = MapGetCoordinatesForPoint
--------------------------------------------------------------------------------
--#
--#			Local Definitions
--#
--------------------------------------------------------------------------------
local isLoading = true
local playerPosition = {}
local worldX = 0
local worldY = 0
local lastWorldX = 0
local lastWorldY = 0
--local zoneX = 0
--local zoneY = 0
local zoneId = 0
local playerDirection = 0
local isPositionDirty = true
local isPlayerDirectionDirty = true

--------------------------------------------------------------------------------
--#
--#			Global MapMonster Player Event Handlers and Functions
--#
--------------------------------------------------------------------------------
MapMonster.PlayerFileLoaded = true

function MapMonster.InitializePlayer()
	RegisterEventHandler(SystemData.Events.LOADING_BEGIN, "MapMonster.OnLoadingBegin")
	RegisterEventHandler(SystemData.Events.LOADING_END, "MapMonster.OnLoadingEnd")
	RegisterEventHandler(SystemData.Events.INTERFACE_RELOADED, "MapMonster.OnLoadingEnd")
	RegisterEventHandler(SystemData.Events.PLAYER_ZONE_CHANGED, "MapMonster.OnPlayerZoneChanged")
	RegisterEventHandler(SystemData.Events.PLAYER_POSITION_UPDATED, "MapMonster.OnPlayerPositionUpdated")

	--CreateWindow("InvisiblePlayerZoneMap", false)
	--CreateMapInstance("InvisiblePlayerZoneMapDisplay", SystemData.MapTypes.NORMAL)
end

function MapMonster.ShutdownPlayer()
	UnregisterEventHandler(SystemData.Events.LOADING_BEGIN, "MapMonster.OnLoadingBegin")
	UnregisterEventHandler(SystemData.Events.LOADING_END, "MapMonster.OnLoadingEnd")
	UnregisterEventHandler(SystemData.Events.INTERFACE_RELOADED, "MapMonster.OnLoadingEnd")
	UnregisterEventHandler(SystemData.Events.PLAYER_ZONE_CHANGED, "MapMonster.OnPlayerZoneChanged")
	UnregisterEventHandler(SystemData.Events.PLAYER_POSITION_UPDATED, "MapMonster.OnPlayerPositionUpdated")
	--RemoveMapInstance("InvisiblePlayerZoneMapDisplay")
end

function MapMonster.OnPlayerPositionUpdated(newWorldX, newWorldY)	-- this is spammed as hell, so do the least possible
	lastWorldX = worldX
	lastWorldY = worldY
    worldX = newWorldX
	worldY = newWorldY
	isPositionDirty = true
	isPlayerDirectionDirty = true
end

function MapMonster.OnPlayerZoneChanged(newZoneId)

	if (newZoneId > 0 and not isLoading) then
		--MapSetMapView( "InvisiblePlayerZoneMapDisplay", GameDefs.MapLevel.ZONE_MAP, newZoneId)
		-- make sure we at least have a zoneId for wherever we are, even if we haven't moved yet
		
		-- make sure to reset X,Y since there's no guarantee we're in the same part of the world
		playerPosition.worldX = nil
		playerPosition.worldY = nil
		worldX = 0
		worldY = 0

		local zoneInfo = MapMonsterAPI.GetZoneData(playerPosition.zoneId)
		if not zoneInfo.sharedWorld or not zoneInfo.sharedWorld[newZoneId] then playerDirection = 0 end
		-- post error msg if we dont have offset data for this zone
		if not MapMonsterAPI.GetZoneData(newZoneId) then
			MapMonster.ERROR_CODE = 101
			local errorCode, errorMsg = MapMonsterAPI.GetError()
			d("Zone error for zone = " .. newZoneId)
			MapMonster.print(errorMsg)
		end
		playerPosition.zoneId = newZoneId
	end
end

function MapMonster.OnLoadingBegin()
	isLoading = true
	worldX = 0
	worldY = 0
end
function MapMonster.OnLoadingEnd()
	isLoading = false
	-- PLAYER_ZONE_CHANGED not fired when its the first login of your session 
	-- so we make sure to have a good zone id by the time loading ends
	worldX = 0
	worldY = 0
	playerPosition.zoneId = Player.zone
	--MapSetMapView( "InvisiblePlayerZoneMapDisplay", GameDefs.MapLevel.ZONE_MAP, Player.zone)
end

-- new attempt; so much for the need to gather ZoneData and Map Sizes
--[[function MapMonster.GetPlayerPointMapOffset()
	local zoneInfo = MapMonsterAPI.GetZoneData(Player.zone)
	if zoneInfo.chunkX then
		local function _trimWorldPos(oldPos, maxPos)
			local newPos = oldPos
			while newPos > maxPos do
				newPos = newPos - maxPos
			end
			return newPos
		end
		return MapGetPointForCoordinates("InvisiblePlayerZoneMapDisplay", _trimWorldPos(worldX, zoneInfo.chunkX), _trimWorldPos(worldY, zoneInfo.chunkY))
	else
		return MapGetPointForCoordinates("InvisiblePlayerZoneMapDisplay", worldX, worldY)
	end
end
function MapMonster.GetPlayerZoneCoordinates()
	local xOffset, yOffset = MapMonster.GetPlayerPointMapOffset()
	return MapGetCoordinatesForPoint("InvisiblePlayerZoneMapDisplay", xOffset, yOffset)
end]]--
--------------------------------------------------------------------------------
--# Do not use this, only for map calibration functions
--#
--------------------------------------------------------------------------------
function MapMonster.GetTruePosition() return playerPosition end
--------------------------------------------------------------------------------
--#
--#			MapMonster API Functions
--#
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.GetPlayerPosition()
--#		Returns the players current position in the world
--#
--#	Parameters:
--#		none
--#	Returns:
--#		table -(table) Returns a table containing the players most recent
--#						validated position
--#	Notes:
--#		Table includes, zoneId, world, and zone position if available
--#
--------------------------------------------------------------------------------
function MapMonsterAPI.GetPlayerPosition()

	--[===[@alpha@
	--d("Get Player Position")
	--@end-alpha@]===]
	if isLoading then
		MapMonster.ERROR_CODE = 102
		return false -- calling too early we have nothing
	end
	-- check for world pos data
	if ((worldX == 0 and worldY == 0) or worldX == nil or worldY == nil) then return { zoneId = playerPosition.zoneId } end	-- no world pos return just the zoneId

	if isPositionDirty then
		-- get the current position in the world
		--[[local playerWorldPos = {zoneId = Player.zone,
								worldX = LibToolkit.roundNum(worldX),
								worldY = LibToolkit.roundNum(worldY),}]]--
		local worldXrounded = LibToolkit.roundNum(worldX)
		local worldYrounded = LibToolkit.roundNum(worldY)
		if playerPosition.worldX == worldXrounded and playerPosition.worldY == worldYrounded then return playerPosition end

		local zoneInfo = MapMonsterAPI.GetZoneData(playerPosition.zoneId)
		if zoneInfo.chunkX then
			local function _trimWorldPos(oldPos, maxPos)
				local newPos = oldPos
				while newPos > maxPos do
					newPos = newPos - maxPos
				end
				return newPos
			end
			worldXrounded = _trimWorldPos(worldXrounded, zoneInfo.chunkX)
			worldYrounded = _trimWorldPos(worldYrounded, zoneInfo.chunkY)
			if playerPosition.worldX == worldXrounded and playerPosition.worldY == worldYrounded then return playerPosition end
		end
		playerPosition.worldX = worldXrounded
		playerPosition.worldY = worldYrounded
		
		-- get the zone position
		local playerZonePos = MapMonsterAPI.WorldToZone(playerPosition)
			
		-- if we have zone position cache it and return it
		-- otherwise just return the world pos
		if playerZonePos then
			playerPosition.zoneX = playerZonePos.zoneX
			playerPosition.zoneY = playerZonePos.zoneY
			return playerZonePos
		else
			--playerPosition.zoneX, playerPosition.zoneY = MapMonster.GetPlayerZoneCoordinates()
			return playerPosition
		end
		isPositionDirty = false
	else
		return playerPosition
	end
end

--  mapGet style on hold
--[[function MapMonsterAPI.GetPlayerPosition()
	if isLoading then
		MapMonster.ERROR_CODE = 102
		return false -- calling too early we have nothing
	end
	-- check for world pos data
	if ((worldX == 0 and worldY == 0) or worldX == nil or worldY == nil) then return { zoneId = Player.zone } end	-- no world pos return just the zoneId

	if isPositionDirty then
		zoneX, zoneY = MapMonster.GetPlayerZoneCoordinates()
		isPositionDirty = false
	end
	
	--EA_ChatWindow.Print(L"Player Zone Coordinates: "..zoneX..L", "..zoneY)
	return {
        zoneId = Player.zone,
        worldX = worldX,
        worldY = worldY,
		zoneX = zoneX,
		zoneY = zoneY,
    }
end]]--
--[[function MapMonsterAPI.GetPlayerPosition()

	--[===[@alpha@
	--d("Get Player Position")
	--@end-alpha@]===]
	if isLoading then
		MapMonster.ERROR_CODE = 102
		return false -- calling too early we have nothing
	end

	-- check for world pos data
	if (playerPosition.worldX == nil or playerPosition.worldY == nil ) then
		return { zoneId = playerPosition.zoneId } -- no world pos return just the zoneId
	end

	-- get the current position in the world
	local playerWorldPos = {zoneId = playerPosition.zoneId,
							worldX = LibToolkit.roundNum(playerPosition.worldX),
							worldY = LibToolkit.roundNum(playerPosition.worldY),}
	
	-- use the cached playerpos if we have it
	if playerPosition.zoneX and playerPosition.zoneY then
		playerWorldPos.zoneX = playerPosition.zoneX
		playerWorldPos.zoneY = playerPosition.zoneY
		return playerWorldPos
	end

	local zoneInfo = MapMonsterAPI.GetZoneData(playerPosition.zoneId)
	if zoneInfo.chunkX then
		local function _trimWorldPos(oldPos, maxPos)
			local newPos = oldPos
			while newPos > maxPos do
				newPos = newPos - maxPos
			end
			return newPos
		end
		playerWorldPos.worldX = _trimWorldPos(playerWorldPos.worldX, zoneInfo.chunkX)
		playerWorldPos.worldY = _trimWorldPos(playerWorldPos.worldY, zoneInfo.chunkY)
	end

	-- get the zone position
	local playerZonePos = MapMonsterAPI.WorldToZone(playerWorldPos)
		
	-- if we have zone position cache it and return it
	-- otherwise just return the world pos
	if playerZonePos then
		playerPosition.zoneX = playerZonePos.zoneX
		playerPosition.zoneY = playerZonePos.zoneY
		return playerZonePos
	else
		return playerWorldPos
	end

end]]--

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.GetPlayerDirection()
--#		Returns the direction the player is moving as compass direction in degrees
--#
--#	Parameters:
--#		none
--#	Returns:
--#		direction -(number) Compass direction the player last moved
--#	Notes:
--#		
--------------------------------------------------------------------------------
function MapMonsterAPI.GetPlayerDirection()
	if isPlayerDirectionDirty then
		local angle = math_deg( math_atan2( ((worldY or 0) - lastWorldY), ((worldX or 0) - lastWorldX) ) ) + 90
		if angle < 0 then angle = angle + 360 end
		playerDirection = tonumber(string_format("%.0f", angle))
		isPlayerDirectionDirty = false
	end
	return playerDirection
end
