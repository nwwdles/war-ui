--------------------------------------------------------------------------------
-- Copyright (c) 2008 Bloodwalker <metagamegeek@gmail.com>
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
--
--------------------------------------------------------------------------------
-- File:      Source/MapMonster_ZoneData.lua
-- Date:      2012-08-17T12:40:42Z
-- Author:    Philosound
-- Version:   v0.96
-- Revision:  123
-- File Rev:  123
-- Copyright: 2008
--------------------------------------------------------------------------------
--
-- Starting points for zone offsets and mapsize taken from Nevir27's work on
-- LibSurveyor, all other data is original
--
--------------------------------------------------------------------------------

MapMonster.ZoneData_VERSION = 1

MapMonster.ZoneInfo = {}

-- Dwarves vs Greenskins --
---------------------------
MapMonster.ZoneInfo[6]  = { offsetX  = 737280,  offsetY  = 843776,  -- Ekrund
                            mapsizeX = 65536,   mapsizeY = 65536,
                            sharedWorld  = { [11] = true,
											 [6]  = true, }, }
MapMonster.ZoneInfo[11] = { offsetX  = 802816,  offsetY  = 843776, -- Mount Bloodhorn
                            mapsizeX = 65536,   mapsizeY = 65536,
							sharedWorld  = { [11] = true, 
											 [6]  = true, }, }
MapMonster.ZoneInfo[7]  = { offsetX  = 1032192, offsetY  = 794624,  -- Barak Varr 
                            mapsizeX = 65536,   mapsizeY = 65536,
							sharedWorld  = { [1] = true,
											 [7] = true, }, }
MapMonster.ZoneInfo[1]  = { offsetX  = 1032192, offsetY  = 860160,  -- Marshes of Madness
                            mapsizeX = 65536,   mapsizeY = 65536,
							sharedWorld  = { [1] = true,
											 [7] = true, }, }
MapMonster.ZoneInfo[8]  = { offsetX  = 1236992,  offsetY  = 827392,  -- Black Fire Pass
                            mapsizeX = 65536,   mapsizeY = 65536,
							sharedWorld  = { [2] = true,
											 [8] = true, }, }
MapMonster.ZoneInfo[2]  = { offsetX  = 1236992,  offsetY  = 892928,  -- The Badlands
                            mapsizeX = 65536,   mapsizeY = 65536,
							sharedWorld  = { [2] = true,
											 [8] = true, }, }
MapMonster.ZoneInfo[10] = { offsetX  = 1368064, offsetY  = 761856,  -- Stonewatch
                            mapsizeX = 65536,   mapsizeY = 65536,
							sharedWorld  = { [10] = true,
											[9]  = true,
											[5]  = true,
											[26] = true,
											[27] = true,
											[3]  = true,
											[4]  = true, }, }
MapMonster.ZoneInfo[9]  = { offsetX = 1368064,  offsetY  = 827392,  -- Kadrin Valley
                            mapsizeX = 65536,   mapsizeY = 65536,
							sharedWorld  = { [10] = true,
											[9]  = true,
											[5]  = true,
											[26] = true,
											[27] = true,
											[3]  = true,
											[4]  = true, }, }
MapMonster.ZoneInfo[5]  = { offsetX  = 1368064, offsetY  = 892928,  -- Thunder Mountain
                            mapsizeX = 65536,   mapsizeY = 65536,
							sharedWorld  = { [10] = true,
											[9]  = true,
											[5]  = true,
											[26] = true,
											[27] = true,
											[3]  = true,
											[4]  = true, }, }
MapMonster.ZoneInfo[26] = { offsetX = 1302528,  offsetY  = 892928,  -- Cinderfall
                            mapsizeX = 65536,   mapsizeY = 65536,
							sharedWorld  = { [10] = true,
											[9]  = true,
											[5]  = true,
											[26] = true,
											[27] = true,
											[3]  = true,
											[4]  = true, }, }
MapMonster.ZoneInfo[27] = { offsetX = 1433600,  offsetY  = 892928,  -- Death Peak
                            mapsizeX = 65536,   mapsizeY = 65536,
							sharedWorld  = { [10] = true,
											[9]  = true,
											[5]  = true,
											[26] = true,
											[27] = true,
											[3]  = true,
											[4]  = true, }, }
MapMonster.ZoneInfo[3]  = { offsetX = 1368064,  offsetY  = 958464,  -- Black Crag
                            mapsizeX = 65536,   mapsizeY = 65536,
							sharedWorld  = { [10] = true,
											[9]  = true,
											[5]  = true,
											[26] = true,
											[27] = true,
											[3]  = true,
											[4]  = true, }, }
MapMonster.ZoneInfo[4]  = { offsetX = 1368064,  offsetY  = 1024000, -- Butchers Pass
                            mapsizeX = 65536,   mapsizeY = 65536,
							sharedWorld  = { [10] = true,
											[9]  = true,
											[5]  = true,
											[26] = true,
											[27] = true,
											[3]  = true,
											[4]  = true, }, }

-- Empire vs Chaos --
---------------------
MapMonster.ZoneInfo[100] = { offsetX  = 819200,  offsetY  = 819200, -- Norsca
                             mapsizeX = 65536,   mapsizeY = 65536,
							sharedWorld  = { [100] = true,
											 [106] = true, }, }
MapMonster.ZoneInfo[106] = { offsetX  = 819200,  offsetY  = 884736, -- Nordland
                             mapsizeX = 65536,   mapsizeY = 65536,
							sharedWorld  = { [100] = true,
											 [106] = true, }, }
MapMonster.ZoneInfo[101] = { offsetX  = 1015808, offsetY  = 819200, -- Troll Country
                             mapsizeX = 65536,   mapsizeY = 65536,
							sharedWorld  = { [101] = true,
											 [107] = true, }, }
MapMonster.ZoneInfo[107] = { offsetX  = 1015808, offsetY  = 884736, -- Ostland
                             mapsizeX = 65536,   mapsizeY = 65536,
							sharedWorld  = { [101] = true,
											 [107] = true, }, }
MapMonster.ZoneInfo[102] = { offsetX  = 1212416, offsetY  = 819200, -- High Pass
                             mapsizeX = 65536,   mapsizeY = 65536,
							sharedWorld  = { [102] = true,
											 [108] = true, }, }
MapMonster.ZoneInfo[108] = { offsetX  = 1212416, offsetY  = 884736, -- Talabecland
                             mapsizeX = 65536,   mapsizeY = 65536,
							sharedWorld  = { [102] = true,
											 [108] = true, }, }
MapMonster.ZoneInfo[104] = { offsetX  = 1409024, offsetY  = 688128, -- The Maw
                             mapsizeX = 65536,   mapsizeY = 65536,
                             sharedWorld  = { [103] = true,
											 [104] = true,
											 [105] = true,
											 [120] = true,
											 [109] = true,
											 [110] = true, }, }
MapMonster.ZoneInfo[103] = { offsetX  = 1409024, offsetY  = 753664, -- Chaos Wastes
                             mapsizeX = 65536,   mapsizeY = 65536,
                             sharedWorld  = { [103] = true,
											 [104] = true,
											 [105] = true,
											 [120] = true,
											 [109] = true,
											 [110] = true, }, }
MapMonster.ZoneInfo[105] = { offsetX  = 1409024, offsetY  = 819200, -- Praag
                             mapsizeX = 65536,   mapsizeY = 65536,
                             sharedWorld  = { [103] = true,
											 [104] = true,
											 [105] = true,
											 [120] = true,
											 [109] = true,
											 [110] = true, }, }
MapMonster.ZoneInfo[120] = { offsetX  = 1343488, offsetY  = 819200, -- West Praag
                             mapsizeX = 65536,   mapsizeY = 65536,
                             sharedWorld  = { [103] = true,
											 [104] = true,
											 [105] = true,
											 [120] = true,
											 [109] = true,
											 [110] = true, }, }
MapMonster.ZoneInfo[109] = { offsetX  = 1409024, offsetY  = 884736, -- Reikland
                             mapsizeX = 65536,   mapsizeY = 65536,
                             sharedWorld  = { [103] = true,
											 [104] = true,
											 [105] = true,
											 [120] = true,
											 [109] = true,
											 [110] = true, }, }
MapMonster.ZoneInfo[110] = { offsetX  = 1409024, offsetY  = 950272, -- Reikwald
                             mapsizeX = 65536,   mapsizeY = 65536,
                             sharedWorld  = { [103] = true,
											 [104] = true,
											 [105] = true,
											 [120] = true,
											 [109] = true,
											 [110] = true, }, }

-- High Elf vs Dark Elf --
--------------------------
MapMonster.ZoneInfo[200] = { offsetX  = 1015808, offsetY  = 1015808, -- The Blighted Isle
                             mapsizeX = 65536,   mapsizeY = 65536,
							sharedWorld  = { [200] = true,
											 [206] = true, }, }
MapMonster.ZoneInfo[206] = { offsetX  = 1015808, offsetY  = 1081344, -- Chrace
                             mapsizeX = 65536,   mapsizeY = 65536,
							sharedWorld  = { [200] = true,
											 [206] = true, }, }
MapMonster.ZoneInfo[201] = { offsetX  = 819200,  offsetY  = 1212416, -- The Shadowlands
                             mapsizeX = 65536,   mapsizeY = 65536,
							sharedWorld  = { [201] = true,
											 [207] = true, }, }
MapMonster.ZoneInfo[207] = { offsetX  = 819200,  offsetY  = 1277952, -- Ellyrion
                             mapsizeX = 65536,   mapsizeY = 65536,
							sharedWorld  = { [201] = true,
											 [207] = true, }, }
MapMonster.ZoneInfo[202] = { offsetX  = 1409024, offsetY  = 1409024, -- Avelorn
                             mapsizeX = 65536,   mapsizeY = 65536,
							sharedWorld  = { [202] = true,
											 [208] = true, }, }
MapMonster.ZoneInfo[208] = { offsetX  = 1409024, offsetY  = 1474560, -- Saphery
                             mapsizeX = 65536,   mapsizeY = 65536,
							sharedWorld  = { [202] = true,
											 [208] = true, }, }
MapMonster.ZoneInfo[204] = { offsetX  = 819200,  offsetY  = 1605632, -- Fell Landing
                             mapsizeX = 65536,   mapsizeY = 65536,
                             sharedWorld  = { [203] = true,
											 [204] = true,
											 [205] = true,
											 [220] = true,
											 [209] = true,
											 [210] = true, }, }
MapMonster.ZoneInfo[203] = { offsetX  = 884736,  offsetY  = 1605632, -- Caledor
                             mapsizeX = 65536,   mapsizeY = 65536,
                             sharedWorld  = { [203] = true,
											 [204] = true,
											 [205] = true,
											 [220] = true,
											 [209] = true,
											 [210] = true, }, }

MapMonster.ZoneInfo[191] = { offsetX  = 196532,  offsetY  = 1490883, -- Land of the dead --offsetX  = 196866,  offsetY  = 1490741, WarCommander data
                             mapsizeX = 65536,   mapsizeY = 65536,
                             sharedWorld  = { [203] = true,
											 [204] = true,
											 [205] = true,
											 [220] = true,
											 [209] = true,
											 [210] = true, }, }

MapMonster.ZoneInfo[205] = { offsetX  = 950272,  offsetY  = 1605632, -- Dragonwake
                             mapsizeX = 65536,   mapsizeY = 65536,
                             sharedWorld  = { [203] = true,
											 [204] = true,
											 [205] = true,
											 [220] = true,
											 [209] = true,
											 [210] = true, }, }
MapMonster.ZoneInfo[220] = { offsetX  = 950272,  offsetY  = 1474560, -- Isle of the Dead
                             mapsizeX = 65536,   mapsizeY = 65536,
                             sharedWorld  = { [203] = true,
											 [204] = true,
											 [205] = true,
											 [220] = true,
											 [209] = true,
											 [210] = true, }, }
MapMonster.ZoneInfo[209] = { offsetX  = 1015808, offsetY  = 1605632, -- Eataine
                             mapsizeX = 65536,   mapsizeY = 65536,
                             sharedWorld  = { [203] = true,
											 [204] = true,
											 [205] = true,
											 [220] = true,
											 [209] = true,
											 [210] = true, }, }
MapMonster.ZoneInfo[210] = { offsetX  = 1081344, offsetY  = 1605632, -- Shining Way
                             mapsizeX = 65536,   mapsizeY = 65536,
                             sharedWorld  = { [203] = true,
											 [204] = true,
											 [205] = true,
											 [220] = true,
											 [209] = true,
											 [210] = true, }, }

-- Capital City --
------------------
MapMonster.ZoneInfo[161] = { offsetX  = 439365,  offsetY  = 130714,	--offsetX  = 420300,  offsetY  = 114032, -- Inevitable City
                             mapsizeX = 44064,   mapsizeY = 44064, 	-- mapsizeX = 54880,   mapsizeY = 59776,}
							 mapStartOffsetX = 10816, mapStartOffsetY = 15712}
MapMonster.ZoneInfo[162] = { offsetX  = 104857,  offsetY  = 112264, -- Altdorf
                             mapsizeX = 41760,   mapsizeY = 41760, }
-- Capital City At War --
--[[MapMonster.ZoneInfo[167] = { offsetX  = 420300,  offsetY  = 114032, -- The Inevitable City
                             mapsizeX = 44064,   mapsizeY = 44064, }]]--
MapMonster.ZoneInfo[167] = { offsetX  = 19103,  offsetY  = 220590, -- IC	-- from WARCommander
                             mapsizeX = 44000,   mapsizeY = 44000, }
--[[MapMonster.ZoneInfo[168] = { offsetX  = 104857,  offsetY  = 112264, -- Altdorf
                             mapsizeX = 41760,   mapsizeY = 41760, }]]--
MapMonster.ZoneInfo[168] = { offsetX  = 14439,  offsetY  = 743177, -- Altdorf	-- from WARCommander
                             mapsizeX = 41760,   mapsizeY = 41760, }
-- Capital City Scenario --
MapMonster.ZoneInfo[42]  = { offsetX  = 420300,  offsetY  = 114032, -- The Undercroft
                             mapsizeX = 44064,   mapsizeY = 44064, }
--[[WARCommander_ZoneInfo[42] = { offsetX  = 19198,  offsetY  = 28296, -- Undercroft
                             mapsizeX = 40178,   mapsizeY = 40178, }]]--
MapMonster.ZoneInfo[41]  = { offsetX  = 104857,  offsetY  = 112264, -- Altdorf War Quarter
                             mapsizeX = 41760,   mapsizeY = 41760, }
--[[WARCommander_ZoneInfo[41]  = { offsetX  = 17253,  offsetY  = 293432, -- Altdorf War Quarter
                            mapsizeX = 32620,   mapsizeY = 32620, }]]--
-- Zones in the capital --
MapMonster.ZoneInfo[178] = { noMap = true } -- The Viper Pit
MapMonster.ZoneInfo[198] = { noMap = true } -- Sigmar's Hammer
MapMonster.ZoneInfo[172] = { noMap = true } -- The Eternal Citadel
MapMonster.ZoneInfo[170] = { noMap = true } -- Altdorf Palace


-- Scenarios --
----------------
-- Tier 1 --
MapMonster.ZoneInfo[30]  = { offsetX  = 32346,   offsetY  = 32346, -- Gates of Ekrund
                             mapsizeX = 13952,   mapsizeY = 13952,
                             chunkX   = 65536,   chunkY   = 65536 }
MapMonster.ZoneInfo[130] = { offsetX  = 30811,   offsetY  = 29564, -- Nordenwatch
                             mapsizeX = 16032,   mapsizeY = 16032,
                             chunkX   = 65536,   chunkY   = 65536 }
MapMonster.ZoneInfo[230] = { offsetX  = 37863,   offsetY  = 38574, -- Khaine's Embrace
                             mapsizeX = 14528,   mapsizeY = 14528,
                             chunkX   = 65536,   chunkY   = 65536 }
-- Tier 2 --
MapMonster.ZoneInfo[31]  = { offsetX  = 39025,   offsetY  = 30143, -- Mourkain Temple
                             mapsizeX = 12192,   mapsizeY = 12192,
                             chunkX   = 65536,   chunkY   = 65536 }
MapMonster.ZoneInfo[131] = { offsetX  = 28507,   offsetY  = 31902, -- Stonetroll Crossing ??
                             mapsizeX = 17856,   mapsizeY = 17856,
                             chunkX   = 65536,   chunkY   = 65536 }
MapMonster.ZoneInfo[231] = { offsetX  = 28239,   offsetY  = 32041, -- Phoenix Gate
                             mapsizeX = 17760,   mapsizeY = 17760,
                             chunkX   = 65536,   chunkY   = 65536 }
-- Tier 3 --
MapMonster.ZoneInfo[33]  = { offsetX  = 32652,   offsetY  = 34113, -- Doomfist Crater
                             mapsizeX = 13119,   mapsizeY = 13119,
                             chunkX   = 65536,   chunkY   = 65536 }
MapMonster.ZoneInfo[38]  = { offsetX  = 33244,   offsetY  = 28354, -- Black Fire Basin
                             mapsizeX = 19552,   mapsizeY = 19552,
                             chunkX   = 65536,   chunkY   = 65536 }
MapMonster.ZoneInfo[132] = { offsetX  = 28487,   offsetY  = 29289, -- Talabec Dam
                             mapsizeX = 21440,   mapsizeY = 21440,
                             chunkX   = 65536,   chunkY   = 65536 }
MapMonster.ZoneInfo[139] = { offsetX  = 32730,   offsetY  = 33701, -- High Pass Cemetery
                             mapsizeX = 9216,   mapsizeY = 9216,
                             chunkX   = 65536,   chunkY   = 65536 }
MapMonster.ZoneInfo[232] = { offsetX  = 26146,   offsetY  = 34407, -- Tor Anroc
                             mapsizeX = 20448,   mapsizeY = 20448,
                             chunkX   = 65536,   chunkY   = 65536 }
MapMonster.ZoneInfo[236] = { offsetX  = 32324,   offsetY  = 32859, -- Temple of Isha
                             mapsizeX = 18336,   mapsizeY = 18336,
                             chunkX   = 65536,   chunkY   = 65536 }
-- Tire 4 --
MapMonster.ZoneInfo[43]  = { offsetX  = 20850,   offsetY  = 21538, -- Gromril Crossing
                             mapsizeX = 21376,   mapsizeY = 21376,
                             chunkX   = 65536,   chunkY   = 65536 }
MapMonster.ZoneInfo[44]  = { offsetX  = 26470,   offsetY  = 29102, -- Howling Gorge
                             mapsizeX = 17984,   mapsizeY = 17984,
                             chunkX   = 65536,   chunkY   = 65536 }
MapMonster.ZoneInfo[134] = { offsetX  = 29635,   offsetY  = 35702, -- Reikland Hills
                             mapsizeX = 18880,   mapsizeY = 18880,
                             chunkX   = 65536,   chunkY   = 65536 }
MapMonster.ZoneInfo[136] = { offsetX  = 35309,   offsetY  = 34963, -- Battle for Praag
                             mapsizeX = 19744,   mapsizeY = 19744,
                             chunkX   = 65536,   chunkY   = 65536 }
MapMonster.ZoneInfo[234] = { offsetX  = 37009,   offsetY  = 36096, -- Serpent's Passage
                             mapsizeX = 16704,   mapsizeY = 16704,
                             chunkX   = 65536,   chunkY   = 65536 }
MapMonster.ZoneInfo[235] = { offsetX  = 29340,   offsetY  = 33540, -- Dragon's Bane
                             mapsizeX = 22848,   mapsizeY = 22848,
                             chunkX   = 65536,   chunkY   = 65536 }
-- Unknown --
MapMonster.ZoneInfo[133] = { offsetX  = 31417,   offsetY  = 36843, -- Maw of Madness
                             mapsizeX = 16512,   mapsizeY = 16512,
                             chunkX   = 65536,   chunkY   = 65536 }
MapMonster.ZoneInfo[34]  = { offsetX  = 34610,   offsetY  = 554266, -- Thunder Valley offsetX  = 34610,   offsetY  = 29975, commented second data from WARCommander
                             mapsizeX = 20832,   mapsizeY = 20832,
                             chunkX   = 65536,   chunkY   = 65536 }
MapMonster.ZoneInfo[137] = { offsetX  = 45201,   offsetY  = 33638, -- Grovod Caverns
                             mapsizeX = 10624,   mapsizeY = 10624,
                             chunkX   = 65536,   chunkY   = 65536 }
MapMonster.ZoneInfo[237] = { offsetX  = 22913,   offsetY  = 30567, -- Caledor Woods
                             mapsizeX = 20608,   mapsizeY = 20608,
                             chunkX   = 65536,   chunkY   = 65536 }
MapMonster.ZoneInfo[238] = { offsetX  = 23588,   offsetY  = 31368, -- Blood of the Black Cairn
                             mapsizeX = 18688,   mapsizeY = 18688,
                             chunkX   = 65536,   chunkY   = 65536 }
MapMonster.ZoneInfo[39]  = { offsetX  = 23432,   offsetY  = 31086, -- Logrin's Forge
                             mapsizeX = 18400,   mapsizeY = 18400,
                             chunkX   = 65536,   chunkY   = 65536 }

-- Live Event --
----------------
MapMonster.ZoneInfo[138] = { offsetX  = 32167,   offsetY  = 35398, -- Reikland Factory
                             mapsizeX = 14368,   mapsizeY = 14368,
                             chunkX   = 65536,   chunkY   = 65536 }

-- Instances --
---------------
-- Sacellum Dungeons Instance
MapMonster.ZoneInfo[173] = { chunkX   = 65536, chunkY   = 65536, noMap = true } -- Sacellum Dungeons
MapMonster.ZoneInfo[155] = { chunkX   = 65536, chunkY   = 65536, noMap = true } -- Sacellum Dungeons
MapMonster.ZoneInfo[156] = { chunkX   = 65536, chunkY   = 65536, noMap = true } -- Sacellum Dungeons
-- Bloodwrought Enclave
MapMonster.ZoneInfo[195] = { chunkX   = 65536, chunkY   = 65536, noMap = true } -- Bloodwrought Enclave
-- Sewers of Altdorf
MapMonster.ZoneInfo[152] = { chunkX   = 65536, chunkY   = 65536, noMap = true } -- The Sewers of Altdorf
MapMonster.ZoneInfo[153] = { chunkX   = 65536, chunkY   = 65536, noMap = true } -- The Sewers of Altdorf
MapMonster.ZoneInfo[169] = { chunkX   = 65536, chunkY   = 65536, noMap = true } -- The Sewers of Altdorf
-- Warpbalde Tunnels
MapMonster.ZoneInfo[177] = { chunkX   = 65536, chunkY   = 65536, noMap = true } -- Warpblade Tunnels
MapMonster.ZoneInfo[154] = { chunkX   = 65536, chunkY   = 65536, noMap = true } -- Warpblade Tunnels
-- Mount Gunbad							 
MapMonster.ZoneInfo[60]  = { chunkX   = 65536, chunkY   = 65536, noMap = true } -- Mount Gunbad
MapMonster.ZoneInfo[63]  = { chunkX   = 65536, chunkY   = 65536, noMap = true } -- Gunbad Nursery
MapMonster.ZoneInfo[64]  = { chunkX   = 65536, chunkY   = 65536, noMap = true } -- Gunbad Lab
MapMonster.ZoneInfo[65]  = { chunkX   = 65536, chunkY   = 65536, noMap = true } -- Squig Boss
MapMonster.ZoneInfo[66]  = { chunkX   = 65536, chunkY   = 65536, noMap = true } -- Gunbad Barracks
-- Bastion Stair
MapMonster.ZoneInfo[160] = { chunkX   = 65536, chunkY   = 65536, noMap = true } -- Bastion Stair
MapMonster.ZoneInfo[163] = { chunkX   = 65536, chunkY   = 65536, noMap = true } -- Thar'Ignan
MapMonster.ZoneInfo[164] = { chunkX   = 65536, chunkY   = 65536, noMap = true } -- Lord Slaurith
MapMonster.ZoneInfo[165] = { chunkX   = 65536, chunkY   = 65536, noMap = true } -- Kaarn The Vanquisher
MapMonster.ZoneInfo[166] = { chunkX   = 65536, chunkY   = 65536, noMap = true } -- Skull Lord Var'Ithrok							 
-- Uncertain
MapMonster.ZoneInfo[174] = { chunkX   = 65536, chunkY   = 65536, noMap = true } -- Elysium
MapMonster.ZoneInfo[175] = { chunkX   = 65536, chunkY   = 65536, noMap = true } -- Winds of Chaos
MapMonster.ZoneInfo[176] = { chunkX   = 65536, chunkY   = 65536, noMap = true } -- Sigmar Crypts
MapMonster.ZoneInfo[196] = { chunkX   = 65536, chunkY   = 65536, noMap = true } -- Bilerot Burrow
MapMonster.ZoneInfo[260] = { chunkX   = 65536, chunkY   = 65536, noMap = true } -- The Lost Vale

-- Bad Zones, don't even have placeholder maps --
-------------------------------------------------							
MapMonster.ZoneInfo[32]  = { badZone = true } -- The Blockade
MapMonster.ZoneInfo[36]  = { badZone = true } -- Kadrin Valley Pass
MapMonster.ZoneInfo[37]  = { badZone = true } -- Black Crag Keep
MapMonster.ZoneInfo[61]  = { badZone = true } -- Karak Eight Peaks
MapMonster.ZoneInfo[62]  = { badZone = true } -- Karaz-a-Karak
MapMonster.ZoneInfo[67]  = { badZone = true } -- Karak Eight Peaks
MapMonster.ZoneInfo[68]  = { badZone = true } -- Karaz-a-Karak
MapMonster.ZoneInfo[118] = { badZone = true } -- Kraken Sea
MapMonster.ZoneInfo[135] = { badZone = true } -- Twisting Tower
MapMonster.ZoneInfo[144] = { badZone = true } -- Outer Dark
MapMonster.ZoneInfo[171] = { badZone = true } -- The Screaming Cat
MapMonster.ZoneInfo[189] = { badZone = true } -- Bright Wizard College
MapMonster.ZoneInfo[190] = { badZone = true } -- Bright Wizard College
MapMonster.ZoneInfo[261] = { badZone = true } -- Fist of Malekith
MapMonster.ZoneInfo[262] = { badZone = true } -- Lothern
MapMonster.ZoneInfo[267] = { badZone = true } -- Fist of Malekith
MapMonster.ZoneInfo[268] = { badZone = true } -- Lothern
