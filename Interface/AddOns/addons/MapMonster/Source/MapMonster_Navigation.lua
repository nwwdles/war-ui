--------------------------------------------------------------------------------
-- Copyright (c) 2008 Bloodwalker <metagamegeek@gmail.com>
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
--
--------------------------------------------------------------------------------
--
-- Starting points for zone offsets and mapsize taken from Nevir27's work on
-- LibSurveyor, all other data and code is original
--
--------------------------------------------------------------------------------
-- File:      Source/MapMonster_Navigation.lua
-- Date:      2012-08-17T12:40:42Z
-- Author:    Philosound
-- Version:   v0.96
-- Revision:  123
-- File Rev:  123
-- Copyright: 2008
--------------------------------------------------------------------------------

MapMonster.NavigationFileLoaded = true

--------------------------------------------------------------------------------
--#
--#			MapMonster API Functions
--#
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.GetDirection(fromPosition, toPosition)
--#		Returns the direction between two points as a compass bearing 0-360
--#
--#	Parameters:
--#		fromPosition -(table) Starting point as position table
--#		toPosition   -(table) Destination as position table
--#	Returns:
--#		direction -(number) Returns the compass bearing
--#	Notes:
--#		Starting and ending position table must contain zoneId and valid 
--#	 	world positions. If only 1 position provided, players current position
--#		is used as the fromPosition
--#		Changed in v0.3.6
--------------------------------------------------------------------------------
function MapMonsterAPI.GetDirection(fromPosition, toPosition)

	-- if we dont have a starting position assume its the players current position
	if not toPosition then
		toPosition = fromPosition
		fromPosition = MapMonsterAPI.GetPlayerPosition()
		if not fromPosition then
			return false
		end
	end

	-- Validate positions and adjust for scenarios and instances
	fromPosition = MapMonsterAPI.ValidateWorldPos(fromPosition)
	toPosition = MapMonsterAPI.ValidateWorldPos(toPosition)

	if not fromPosition or not toPosition then
		return false
	end
	
	if fromPosition.zoneId ~= toPosition.zoneId then
		local zoneInfo = MapMonsterAPI.GetZoneData(fromPosition.zoneId)
		if not zoneInfo.sharedWorld or not zoneInfo.sharedWorld[toPosition.zoneId] then
			MapMonster.ERROR_CODE = 405
			return false
		end
	end
	
	local fromX = fromPosition.worldX or 0
	local fromY = fromPosition.worldY or 0
	local toX, toY = toPosition.worldX, toPosition.worldY

	local angle = math.deg( math.atan2( (toY - fromY), (toX - fromX) ) )

	angle = angle + 90
	if angle < 0 then
		angle = angle + 360
	end

	angle = tonumber(string.format("%.0f", angle))

	return angle

end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.GetDistance(fromPosition, toPosition)
--#		Returns the distance between two points using the same units as world position
--#
--#	Parameters:
--#		fromPosition -(table) Starting point
--#		toPosition   -(table) Destination
--#	Returns:
--#		distance -(number) Returns the units between the 2 points in a straight line
--#	Notes:
--#		Starting and ending position table must contain zoneId and valid 
--#	 	world positions. If only 1 position provided, players current position is used
--#		as the fromPosition
--#		Changed in v0.3.6
--------------------------------------------------------------------------------
function MapMonsterAPI.GetDistance(fromPosition, toPosition)

	-- if we dont have a starting position assume its the players current position
	if not toPosition then
		toPosition = fromPosition
		fromPosition = MapMonsterAPI.GetPlayerPosition()
	end

	-- Validate positions and adjust for scenarios and instances
	fromPosition = MapMonsterAPI.ValidateWorldPos(fromPosition)
	toPosition = MapMonsterAPI.ValidateWorldPos(toPosition)

	if not fromPosition or not toPosition then
		return false
	end	

	if ( fromPosition.worldX == nil or fromPosition.worldY == nil or 
		 toPosition.worldX == nil or toPosition.worldY == nil )
	then
		MapMonster.ERROR_CODE = 211
		return false
	end
	
	local deltaX = math.abs(fromPosition.worldX - toPosition.worldX)
	local deltaY = math.abs(fromPosition.worldY - toPosition.worldY)

	local distance = math.sqrt( (deltaX * deltaX) + (deltaY * deltaY) )

	return distance

end
