--------------------------------------------------------------------------------
-- Copyright (c) 2008 Bloodwalker <metagamegeek@gmail.com>
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
--
--------------------------------------------------------------------------------
--
-- Starting points for zone offsets and mapsize taken from Nevir27's work on
-- LibSurveyor, all other data and code is original
--
--------------------------------------------------------------------------------
-- File:      Source/MapMonster_EditorWindow.lua
-- Date:      2012-08-17T12:40:42Z
-- Author:    Philosound
-- Version:   v0.96
-- Revision:  123
-- File Rev:  123
-- Copyright: 2008
--------------------------------------------------------------------------------

if not MapMonster or not MapMonster.CoreLoaded then return end --shortcut loading the file if the core didnt load
local LibToolkit = LibStub("LibToolkit-0.1")
local T = LibStub("WAR-AceLocale-3.0"):GetLocale("MapMonster-Editor")

----------------------------------------------------------------------------------------------------
--#					
--#					Internal variables and definitions
--#					
----------------------------------------------------------------------------------------------------

-- Editor Window mode constants
local CLEAN_MODE = 0
local ADD_MODE = 1
local EDIT_MODE = 2
local VIEW_MODE = 3
local ADDON_EDIT = 4
local ADDON_ADD = 5

local editorWindow = "MapMonster_EditorWindow"
local editorMode = 0
local editorData = {}
local ERROR_COLOR = DefaultColor.RED
local MESSAGE_COLOR = DefaultColor.GREEN
local MARKER_COLOR = DefaultColor.RED

local hookShowZone

local allEditorFields = { ["PinLabelEditBox"]   = false,
						  ["PinLabel"]          = false,
						  ["PinTypeComboBox"]   = false,
						  ["PinTypeLabel"]      = false,
						  ["SubTypeComboBox"]   = false,
						  ["SubTypeLabel"]      = false,
						  ["ZoneNameComboBox"]  = false,
						  ["ZoneNameLabel"]     = false,
						  ["ZonePosXEditBox"]   = false,
						  ["ZonePosXLabel"]     = false,
						  ["ZonePosYEditBox"]   = false,
						  ["ZonePosYLabel"]     = false,
						  ["WorldPosXLabel"]    = false,
						  ["WorldPosYLabel"]    = false,
						  ["IsLockedCheckBox"]  = true,
						  ["IsPrivateCheckBox"] = true,
						  ["NoteEditBox"]       = false,
						  ["NoteLabel"]         = false,
						  ["DatestampLabel"]    = true,
						  ["LeftButton"]        = false,
						  ["MiddleButton"]      = false,
						  ["RightButton"]       = false,}

local defaultAddFields = {   "PinLabelEditBox",
							 "PinTypeComboBox",
							 "SubTypeComboBox",
							 "ZoneNameComboBox",
							 "ZonePosXEditBox",
							 "ZonePosYEditBox",
							 "NoteEditBox",
							 "DatestampLabel",
							 "LeftButton",
							 "MiddleButton",
							 "RightButton", }

local defaultEditFields = {  "PinLabelEditBox",
							 "PinTypeComboBox",
							 "SubTypeComboBox",
							 "ZoneNameLabel",
							 "ZonePosXEditBox",
							 "ZonePosYEditBox",
							 "NoteEditBox",
							 "DatestampLabel",
							 "LeftButton",
							 "MiddleButton",
							 "RightButton", }

local defaultViewFields = {  "PinLabel",
							 "PinTypeLabel",
							 "SubTypeLabel",
							 "ZoneNameLabel",
							 "ZonePosXLabel",
							 "ZonePosYLabel",
							 "NoteLabel", 
							 "DatestampLabel",
							 "MiddleButton", }

local addonAddFields = {   "PinLabelEditBox",
							 "PinTypeLabel",
							 "SubTypeComboBox",
							 "ZoneNameComboBox",
							 "ZonePosXEditBox",
							 "ZonePosYEditBox",
							 "NoteEditBox",
							 "DatestampLabel",
							 "LeftButton",
							 "MiddleButton",
							 "RightButton", }

local addonEditFields = { "PinLabelEditBox",
							 "PinTypeLabel",
							 "SubTypeComboBox",
							 "ZoneNameLabel",
							 "ZonePosXEditBox",
							 "ZonePosYEditBox",
							 "NoteEditBox", 
							 "DatestampLabel",
							 "LeftButton",
							 "MiddleButton",
							 "RightButton", }

local zoneNameList = {}
local zoneComboOrder = {}
local zoneComboOrderRev = {}

local pinTypeList = {}
local pinTypeComboOrder = {}
local pinTypeComboOrderRev = {}

local subTypeList = {}
local subTypeComboOrder = {}
local subTypeComboOrderRev = {}

local EDITOR_ICONSCALE = 2.3

----------------------------------------------------------------------------------------------------
--#					
--#					Internal Window Functions
--#					
----------------------------------------------------------------------------------------------------

local function CreateMarker(zoneId, mapX, mapY)
	
	--[===[@alpha@
	d("Create Map Marker")
	--@end-alpha@]===]

	if DoesWindowExist("MapMonster_MarkerPin") then
		DestroyWindow("MapMonster_MarkerPin")
	end

	-- get the mapsize in coords
	local zoneInfo = MapMonsterAPI.GetZoneData(zoneId)
	if not zoneInfo or zoneInfo.noMap then
		return false
	end
	
	-- get the window dimensions to scale
	local windowX, windowY = WindowGetDimensions("EA_Window_WorldMapZoneViewMapDisplay") -- 1000, 1000
	--local windowScale = WindowGetScale("EA_Window_WorldMapZoneViewMapDisplay")
	
	local xPosUnit = windowX / zoneInfo.mapsizeX
	local yPosUnit = windowY / zoneInfo.mapsizeY
	
	local anchorX = mapX * xPosUnit
	local anchorY = mapY * yPosUnit

	CreateWindowFromTemplate("MapMonster_MarkerPin", "MapMonster_MarkerPin", "EA_Window_WorldMapZoneView")		

	WindowSetTintColor("MapMonster_MarkerPin", MARKER_COLOR.r, MARKER_COLOR.g, MARKER_COLOR.b)
	WindowClearAnchors("MapMonster_MarkerPin")
	WindowAddAnchor("MapMonster_MarkerPin", "topleft", "EA_Window_WorldMapZoneViewMapDisplay", "center", anchorX, anchorY)
	WindowSetShowing("MapMonster_MarkerPin", true)

end

local function MoveMapPin(pinId, zoneId, zoneX, zoneY)

	local MapIconName = "MapMonster_MapPin" .. pinId
	if not DoesWindowExist(MapIconName) then
		return false
	end

	-- get the mapsize in coords
	local zoneInfo = MapMonsterAPI.GetZoneData(zoneId)
	if not zoneInfo or zoneInfo.noMap then
		return false
	end
	
	-- get the window dimensions to scale
	local windowX, windowY = WindowGetDimensions("EA_Window_WorldMapZoneViewMapDisplay") -- 1000, 1000
	
	local xPosUnit = windowX / zoneInfo.mapsizeX
	local yPosUnit = windowY / zoneInfo.mapsizeY

	local anchorX = zoneX * xPosUnit
	local anchorY = zoneY * yPosUnit

	WindowClearAnchors(MapIconName)
	WindowAddAnchor(MapIconName, "topleft", "EA_Window_WorldMapZoneViewMapDisplay", "center", anchorX, anchorY)

	return true
end

local function ResetMapPin(pinId)

	local pinData = MapMonsterAPI.GetPin(pinId)
	if pinData then
		MoveMapPin(pinData.id, pinData.zoneId, pinData.zoneX, pinData.zoneY)
	end
	
end

local function GetPinDataFromEditor()

	if editorMode == VIEW_MODE then
		return false
	end
	
	local editorFields = {}
	
	for windowPiece, _ in pairs(allEditorFields) do
		if string.match(windowPiece, "Label$") then
			editorFields[windowPiece] = LabelGetText(editorWindow .. windowPiece)
		elseif string.match(windowPiece, "EditBox$") then
			editorFields[windowPiece] = TextEditBoxGetText(editorWindow .. windowPiece)
		elseif string.match(windowPiece, "CheckBox$") then
			editorFields[windowPiece] = ButtonGetPressedFlag(editorWindow .. windowPiece)
		elseif string.match(windowPiece, "ComboBox$") then
			local selected = ComboBoxGetSelectedMenuItem(editorWindow .. windowPiece)
			local selectedValue
			if windowPiece == "PinTypeComboBox" then
				selectedValue = pinTypeComboOrder[selected]
			elseif windowPiece == "SubTypeComboBox" then
				selectedValue = subTypeComboOrder[selected]
			elseif windowPiece == "ZoneNameComboBox" then
				selectedValue = zoneComboOrder[selected]
			end
			editorFields[windowPiece] = selectedValue
		end
	end

	local editedPin = {}
	editedPin.id        = editorData.pinId
	editedPin.pinType   = editorFields.PinTypeComboBox
	editedPin.subType   = editorFields.SubTypeComboBox
	editedPin.label     = editorFields.PinLabel
	editedPin.note      = editorFields.NoteEditBox
	editedPin.isLocked  = editorFields.IsLockedCheckBox
	editedPin.isPrivate = editorFields.IsPrivateCheckBox
	editedPin.zoneId    = tonumber(editorFields.ZoneNameComboBox)
	editedPin.zoneX     = tonumber(editorFields.ZonePosXLabel)
	editedPin.zoneY     = tonumber(editorFields.ZonePosYLabel)
	editedPin.worldX    = tonumber(editorFields.WorldPosXLabel)
	editedPin.worldY    = tonumber(editorFields.WorldPosYLabel)
	
	return editedPin
	
end

----------------------------------------------------------------------------------------------------
--#					
--#					Internal Window Builder Functions
--#					
----------------------------------------------------------------------------------------------------
local function PopulateSubTypeComboBox(pinType)

	--[===[@alpha@
	d("Populate SubTypes ComboBox : " .. pinType)
	--@end-alpha@]===]
	
	subTypeList = {}
	subTypeComboOrder = {}
	subTypeComboOrderRev = {}
	local typeData = MapMonsterAPI.GetPinTypeOptions(pinType)

	for subType, subData in pairs(typeData.subTypes) do
		table.insert(subTypeList, { name = subData.label, id = subType} )
	end
	table.sort(subTypeList, MapMonster.CustomSortByName)

	ComboBoxClearMenuItems(editorWindow .. "SubTypeComboBox")
	local counter = 0
	for i, v in pairs(subTypeList) do
		counter = counter + 1
		ComboBoxAddMenuItem(editorWindow .. "SubTypeComboBox", v.name)
		subTypeComboOrderRev[v.id] = counter
		table.insert(subTypeComboOrder, v.id)
	end
	
end

local function SetMapPinIcon(mapIcon)

	--[===[@alpha@
	d("SetMapPinIcon")
	--@end-alpha@]===]
	DynamicImageSetTexture(editorWindow .. "MapPinIcon", mapIcon.texture, mapIcon.x or 0, mapIcon.y or 0)
	DynamicImageSetTextureScale(editorWindow .. "MapPinIcon", mapIcon.scale * EDITOR_ICONSCALE)

	if mapIcon.slice then
		DynamicImageSetTextureSlice(editorWindow .. "MapPinIcon", mapIcon.slice)
	end
	if mapIcon.tint then
		WindowSetTintColor(editorWindow .. "MapPinIcon", mapIcon.tint.r, mapIcon.tint.g, mapIcon.tint.b)
	else
		WindowSetTintColor(editorWindow .. "MapPinIcon", DefaultColor.ZERO_TINT.r, DefaultColor.ZERO_TINT.g, DefaultColor.ZERO_TINT.b)
	end

end
 
local function SetEditorMessage(msgString, errorFlag)
	
	--[===[@alpha@
	d("Set Editor Window Message")
	--@end-alpha@]===]
	local message = towstring(msgString)
	local color
	if errorFlag then
		color = ERROR_COLOR
	else
		color = MESSAGE_COLOR
	end
	LabelSetText(editorWindow .. "MessageBox", message)
	LabelSetTextColor(editorWindow .. "MessageBox", color.r, color.g, color.b)
	
end

local function SetPositionBoxes(newPos)

	--[===[@alpha@
	d("SetPositionBoxes")
	--@end-alpha@]===]
	if newPos == nil or type(newPos) ~= "table" then
		return false
	end
	
	ComboBoxSetSelectedMenuItem(editorWindow .. "ZoneNameComboBox", zoneComboOrderRev[newPos.zoneId])
	LabelSetText(editorWindow .. "ZoneNameLabel",    GetZoneName(newPos.zoneId))

	TextEditBoxSetText(editorWindow .. "ZonePosXEditBox", towstring(newPos.zoneX or "???"))
	TextEditBoxSetText(editorWindow .. "ZonePosYEditBox", towstring(newPos.zoneY or "???"))
	LabelSetText(editorWindow .. "ZonePosXLabel", towstring(newPos.zoneX or "???"))
	LabelSetText(editorWindow .. "ZonePosYLabel", towstring(newPos.zoneY or "???"))

	LabelSetText(editorWindow .. "WorldPosXLabel", towstring(newPos.worldX or "???"))
	LabelSetText(editorWindow .. "WorldPosYLabel", towstring(newPos.worldY or "???"))

end

local function SetComboBox(comboBox, selection)

	--[===[@alpha@
	d("SetComboBox " .. (comboBox or nil) .. " : " .. (selection or "Oops"))
	--@end-alpha@]===]
	
	local lookupTable
	local listTable
	local revTable
	if comboBox == "ZoneName" then
		listTable   = zoneNameList
		lookupTable = zoneComboOrder
		revTable    = zoneComboOrderRev
	elseif comboBox == "PinType" then
		listTable   = pinTypeList
		lookupTable = pinTypeComboOrder
		revTable    = pinTypeComboOrderRev
		PopulateSubTypeComboBox(lookupTable[selection])
	elseif comboBox == "SubType" then
		listTable   = subTypeList
		lookupTable = subTypeComboOrder
		revTable    = subTypeComboOrderRev

		local selectedPinType = ComboBoxGetSelectedMenuItem(editorWindow .. "PinTypeComboBox")
		local typeData = MapMonsterAPI.GetPinTypeOptions(pinTypeComboOrder[selectedPinType], lookupTable[selection])
		
		SetMapPinIcon(typeData.mapIcon)
	end

	ComboBoxSetSelectedMenuItem(editorWindow .. comboBox .. "ComboBox", selection)
	LabelSetText(editorWindow .. comboBox .. "Label", listTable[selection].name)

end

local function SetPinNote(note)
	
	local note = towstring(note)

	TextEditBoxSetText(editorWindow .. "NoteEditBox", note)
	WindowSetDimensions( editorWindow .. "NoteLabel", 400, 310 )
	LabelSetText(editorWindow .. "NoteLabel", note)
	
end

local function ShowEditorFields()

	--[===[@alpha@
	d("Show Editor Fields")
	--@end-alpha@]===]
	local filedList
	if editorMode == ADD_MODE then
		fieldList = defaultAddFields
	elseif editorMode == EDIT_MODE then
		fieldList = defaultEditFields
	elseif editorMode == ADDON_ADD then
		fieldList = addonAddFields
	elseif editorMode == ADDON_EDIT then
		fieldList = addonEditFields
	elseif editorMode == VIEW_MODE then
		fieldList = defaultViewFields
	end

	for _, windowPiece in ipairs(fieldList) do
		WindowSetShowing(editorWindow .. windowPiece, true)
	end
	
end

function DisableAllEdit()

	-- Hide all editor labels and boxes 
	for windowPiece, showing in pairs(allEditorFields) do
		if ( string.match(windowPiece, "Label$") and not string.match(windowPiece, "WorldPos") ) then
			WindowSetShowing(editorWindow .. windowPiece, true)
		elseif ( string.match(windowPiece, "EditBox$") ) then
			WindowSetShowing(editorWindow .. windowPiece, false)
		elseif ( string.match(windowPiece, "CheckBox$") ) then
			if not string.match(windowPiece, "IsLocked") then
				ButtonSetDisabledFlag(editorWindow .. windowPiece, true)
			end
		elseif ( string.match(windowPiece, "ComboBox$") ) then
			WindowSetShowing(editorWindow .. windowPiece, false)
		end
	end

end

function EnableAllEdit()

	-- Enable and hide all editor labels and boxes 
	for windowPiece, showing in pairs(allEditorFields) do
		if ( string.match(windowPiece, "Label$") ) then
			WindowSetShowing(editorWindow .. windowPiece, false)
		elseif ( string.match(windowPiece, "EditBox$") ) then
			WindowSetShowing(editorWindow .. windowPiece, false)
		elseif ( string.match(windowPiece, "CheckBox$") ) then
			ButtonSetDisabledFlag(editorWindow .. windowPiece, false)
		elseif ( string.match(windowPiece, "ComboBox$") ) then
			WindowSetShowing(editorWindow .. windowPiece, false)
		end
	end

end

local function CleanEditorWindow()

	--[===[@alpha@
	d("Cleanup Editor Window")
	--@end-alpha@]===]

	-- clean up the map marker if we used one, reset the map icon if it was an edit
	if DoesWindowExist("MapMonster_MarkerPin") then
		DestroyWindow("MapMonster_MarkerPin")
	end
	if editorMode == ADD_MODE or editorMode == EDIT_MODE then
		MapMonster.Editor.BuildPinTypeComboBox()
	end
	if editorMode == EDIT_MODE or editorMode == ADDON_EDIT then
		ResetMapPin(editorData.pinId)
	end

	editorMode = CLEAN_MODE
	editorData = {}

	LabelSetText(editorWindow .. "TitleBarText", T["Map Monster Pin Editor"])

	-- Empty and hide all editor labels and boxes 
	for windowPiece, showing in pairs(allEditorFields) do
		if ( string.match(windowPiece, "Label$") ) then
			LabelSetText(editorWindow .. windowPiece, L"")
		elseif ( string.match(windowPiece, "EditBox$") ) then
			TextEditBoxSetText(editorWindow .. windowPiece, L"")
		elseif ( string.match(windowPiece, "CheckBox$") ) then
			ButtonSetPressedFlag(editorWindow .. windowPiece, false)
			ButtonSetDisabledFlag(editorWindow .. windowPiece, false)
		elseif ( string.match(windowPiece, "ComboBox$") and 
			     windowPiece ~= "ZoneNameComboBox" and windowPiece ~= "PinTypeComboBox" )
		then
			ComboBoxClearMenuItems(editorWindow .. windowPiece)
		end
		WindowSetShowing(editorWindow .. windowPiece, showing)
	end

	SetEditorMessage("")
	-- set button labels		
	ButtonSetText(editorWindow .. "LeftButton",   T["Save"])
	ButtonSetText(editorWindow .. "MiddleButton", T["Reset"])
	ButtonSetText(editorWindow .. "RightButton",  T["Cancel"])


end

----------------------------------------------------------------------------------------------------
--#					
--#					Global Handlers and Functions
--#					
----------------------------------------------------------------------------------------------------

MapMonster.Editor = {}

function MapMonster.Editor.Initialize()

	--[===[@alpha@
	d("Initialize Editor Window")
	--@end-alpha@]===]
	-- Create the editor window
	CreateWindow(editorWindow, false)
	
	--Dynamic Image - MapPinIcon

	-- set labels
	LabelSetText(editorWindow .. "TitleBarText", 	T["Map Monster Pin Editor"])
	LabelSetText(editorWindow .. "PinLabelHeader",  T["Name:"])
	LabelSetText(editorWindow .. "PinTypeHeader",   T["Pin Type:"])
	LabelSetText(editorWindow .. "SubTypeHeader",   T["Sub Type:"])
	LabelSetText(editorWindow .. "ZoneNameHeader",  T["Zone:"])		
	LabelSetText(editorWindow .. "ZonePosHeader",   T["Position:"])
	LabelSetText(editorWindow .. "ZonePosXHeader",  T["X:"])
	LabelSetText(editorWindow .. "ZonePosYHeader",  T["Y:"])
	LabelSetText(editorWindow .. "OptionsHeader",   T["Options:"])
	LabelSetText(editorWindow .. "NoteHeader",      T["Notes:"])
	LabelSetText(editorWindow .. "DatestampHeader", T["Datestamp:"])

	LabelSetText(editorWindow .. "WorldPosHeader",  T["World:"])
	LabelSetText(editorWindow .. "WorldPosXHeader", T["X:"])
	LabelSetText(editorWindow .. "WorldPosYHeader", T["Y:"])

	WindowSetShowing(editorWindow .. "WorldPosHeader", false)
	WindowSetShowing(editorWindow .. "WorldPosXHeader", false)
	WindowSetShowing(editorWindow .. "WorldPosYHeader", false)
	
	SetEditorMessage("")

	-- set checkboxes
	LabelSetText(editorWindow .. "IsLockedHeader",  T["Locked"])
	ButtonSetCheckButtonFlag( editorWindow .. "IsLockedCheckBox", true)
	LabelSetText(editorWindow .. "IsPrivateHeader", T["Private"])
	ButtonSetCheckButtonFlag( editorWindow .. "IsPrivateCheckBox", true)

	-- build the zone list we are going to use
	for i = 1, 500, 1 do
		local name = GetZoneName(i)
		local nameS = tostring(name)
		local zoneInfo = MapMonsterAPI.GetZoneData(i)
		if name ~= L"" and 
		   not string.find(nameS, "^Dangerous Territory") and 
		   not string.find(nameS, " Test") and
		   zoneInfo and
		   not zoneInfo.badMap and
		   not zoneInfo.noMap
		then
			 table.insert(zoneNameList, { name = name, id = i })
		end
	end
	table.sort(zoneNameList, MapMonster.AlphabetizeByName)
	-- setup zonelist combobox
	ComboBoxClearMenuItems(editorWindow .. "ZoneNameComboBox")
	local counter = 0
	for i, v in pairs(zoneNameList) do
		counter = counter + 1
		ComboBoxAddMenuItem(editorWindow .. "ZoneNameComboBox", v.name)
		zoneComboOrderRev[v.id] = counter
		table.insert(zoneComboOrder, v.id)
	end

	MapMonster.Editor.BuildPinTypeComboBox()
	
	-- set button labels		
	ButtonSetText(editorWindow .. "LeftButton",   T["Save"])
	ButtonSetText(editorWindow .. "MiddleButton", T["Reset"])
	ButtonSetText(editorWindow .. "RightButton",  T["Cancel"])

	-- clean up the window
	CleanEditorWindow()

end

function MapMonster.Editor.BuildPinTypeComboBox(allTypes)

	--[===[@alpha@
	d("Build PinType ComboBox")
	--@end-alpha@]===]
	local comboTypeList
	if allTypes == nil or allTypes == true then
		comboTypeList = MapMonsterAPI.GetPinTypes()
	else
		comboTypeList = { MapMonster.GENERAL_PINTYPE, MapMonster.CHARACTER_PINTYPE }
	end
	
	pinTypeList = {}
	pinTypeComboOrder = {}
	pinTypeComboOrderRev = {}
	-- build the pintype combox box 
	for k, type in pairs(comboTypeList) do
		local pinType = MapMonsterAPI.GetPinTypeOptions(type)
		table.insert(pinTypeList, { name = pinType.label, id = type })
	end
	table.sort(pinTypeList, MapMonster.AlphabetizeByName)
	-- setup pintype combobox
	ComboBoxClearMenuItems(editorWindow .. "PinTypeComboBox")
	local counter = 0
	for i, v in pairs(pinTypeList) do
		counter = counter + 1
		ComboBoxAddMenuItem(editorWindow .. "PinTypeComboBox", v.name)
		pinTypeComboOrderRev[v.id] = counter
		table.insert(pinTypeComboOrder, v.id)
	end

end

function MapMonster.Editor.InitializeHooks()

	-- hook EA_Window_WorldMap.ShowZone
	hookShowZone = EA_Window_WorldMap.ShowZone
	EA_Window_WorldMap.ShowZone = MapMonster.Editor.ShowZoneHooked

end

function MapMonster.Editor.Shutdown()

	if DoesWindowExist(editorWindow) then
		DestroyWindow(editorWindow)
	end
	
end

----------------------------------------------------------------------------------------------------
--#					
--#  Hooked functions
--#					
----------------------------------------------------------------------------------------------------

function MapMonster.Editor.ShowZoneHooked(zoneId)

	hookShowZone(zoneId)
	if DoesWindowExist(editorWindow) then

		if editorMode == ADD_MODE then
			-- get the zonepos and adjust the world pos to match new zone
			local x = TextEditBoxGetText(editorWindow .. "ZonePosXEditBox")
			local y = TextEditBoxGetText(editorWindow .. "ZonePosYEditBox")

			if x ~= L"???" then
				local newPos = { zoneId = zoneId }
				newPos.zoneX = tonumber(x)
				newPos.zoneY = tonumber(y)

				newPos = MapMonsterAPI.ZoneToWorld(newPos)

				if newPos then
					-- set adjusted worldPos
					SetPositionBoxes(newPos)
					-- update map marker
					CreateMarker(newPos.zoneId, newPos.zoneX, newPos.zoneY)
				else
					local blankPos = false
					local zoneInfo = MapMonsterAPI.GetZoneData(zoneId)
					if not zoneInfo.badMap and not zoneInfo.noMap then
						local middlePos = { zoneId = zoneId}
						middlePos.zoneX = zoneInfo.mapsizeX / 2
						middlePos.zoneY = zoneInfo.mapsizeY / 2
						middlePos = MapMonsterAPI.ZoneToWorld(middlePos)
						if middlePos then
							SetPositionBoxes(middlePos)
							-- update map marker
							CreateMarker(middlePos.zoneId, middlePos.zoneX, middlePos.zoneY)
						else
							blankPos = true
						end
					else
						blankPos = true
					end
					if blankPos then
						-- if its no good then blank them all
						local emptyPos = { zoneId = zoneId, zoneX = "???", zoneY = "???", worldX = "???", worldY = "???", }
						SetPositionBoxes(emptyPos)
						-- destroy map marker
						DestroyWindow("MapMonster_MarkerPin")
					end
				end
				
				
			elseif DoesWindowExist("MapMonster_MarkerPin") then
				DestroyWindow("MapMonster_MarkerPin")
			end
			
		end
	end
end

----------------------------------------------------------------------------------------------------
--#					
--#  Window Event handlers
--#					
----------------------------------------------------------------------------------------------------

function MapMonster.Editor.OnClickRight() -- usually cancel

	--[===[@alpha@
	d("Click Right Editor Button")
	--@end-alpha@]===]
	MapMonster.Editor.Close()

end

function MapMonster.Editor.OnClickMiddle() -- usually Reset

	--[===[@alpha@
	d("Click Middle Editor Button")
	--@end-alpha@]===]

	if (editorMode == ADD_MODE) then
		MapMonster.Editor.OpenAddWindow()
		TextEditBoxSetText(editorWindow .. "PinLabelEditBox", T["New Map Monster Pin"])
		
		SetPinNote("")
		SetComboBox("PinType", 1)
		SetComboBox("SubType", 1)

	elseif (editorMode == EDIT_MODE or editorMode == ADDON_EDIT) then
		local pinId = editorData.pinId
		MapMonster.Editor.OpenEditWindow(pinId)

	elseif (editorMode == VIEW_MODE) then
		MapMonster.Editor.Close()
	end
	
end

function MapMonster.Editor.OnClickLeft() -- usualy save

	--[===[@alpha@
	d("Click Left Editor Button")
	--@end-alpha@]===]

	if MapMonster.Editor.SaveEditor() then
		AlertTextWindow.AddLine (MapMonster.AlertTypes.SUCCESS, T["SuccessAlert"])
		MapMonster.Editor.Close()
	else
		local code, message = MapMonsterAPI.GetError()
		SetEditorMessage(message, true)
		AlertTextWindow.AddLine (MapMonster.AlertTypes.ERROR, T["FailedAlert"])
	end

end

function MapMonster.Editor.Close()

	--[===[@alpha@
	d("Editor Window Close")
	--@end-alpha@]===]
	WindowSetShowing(editorWindow, false)

end

function MapMonster.Editor.Open()

	--[===[@alpha@
	d("Editor Window Open")
	--@end-alpha@]===]
	WindowSetShowing(editorWindow, true)

	if editorMode ~= VIEW_MODE then
		WindowAssignFocus(editorWindow .. "NoteEditBox", true)
		WindowAssignFocus(editorWindow .. "PinLabelEditBox", true)
	end

end

function MapMonster.Editor.OnHidden()

	--[===[@alpha@
	d("Editor On Hidden")
	--@end-alpha@]===]
	WindowUtils.RemoveFromOpenList(editorWindow)
	CleanEditorWindow()
	
end

function MapMonster.Editor.OnShown()

	--[===[@alpha@
	d("Editor On Shown")
	--@end-alpha@]===]
	WindowUtils.AddToOpenList(editorWindow, MapMonster.Editor.Close, WindowUtils.Cascade.MODE_NONE)

end

function MapMonster.Editor.OnZoneNameChange(selectedZone)

	--[===[@alpha@
	d("OnZoneNameChanged")
	--@end-alpha@]===]
	EA_Window_WorldMap.SetMap(GameDefs.MapLevel.ZONE_MAP, zoneComboOrder[selectedZone])
	--LabelSetText(editorWindow .. comboBox .. "Label", zoneNameList[selectedZone].name)

end

function MapMonster.Editor.OnPinTypeChange(selectedPinType)

	--[===[@alpha@
	d("OnPinTypeChanged")
	--@end-alpha@]===]
	-- clearing the select box triggers this with 0
	if selectedPinType == 0 then
		LabelSetText(editorWindow .. "PinTypeLabel", L"")
		return
	end
	PopulateSubTypeComboBox(pinTypeComboOrder[selectedPinType])
	LabelSetText(editorWindow .. "PinTypeLabel", pinTypeList[selectedPinType].name)
	SetComboBox("SubType", 1)

end

function MapMonster.Editor.OnSubTypeChange(selectedSubType)

	--[===[@alpha@
	d("OnSubTypeChanged " .. (selectedSubType or "nil"))
	--@end-alpha@]===]
	-- clearing the select box triggers this with 0
	if selectedSubType == 0 then
		LabelSetText(editorWindow .. "SubTypeLabel", L"")
		return
	end

	local selectedPinType = ComboBoxGetSelectedMenuItem(editorWindow .. "PinTypeComboBox")
	local typeData = MapMonsterAPI.GetPinTypeOptions(pinTypeComboOrder[selectedPinType], subTypeComboOrder[selectedSubType])
	
	LabelSetText(editorWindow .. "SubTypeLabel", subTypeList[selectedSubType].name)
	SetMapPinIcon(typeData.mapIcon)

end

function MapMonster.Editor.OnLabelChange()
	LabelSetText(editorWindow .. "PinLabel", TextEditBoxGetText(editorWindow .. "PinLabelEditBox"))
end

function MapMonster.Editor.OnNoteChange()
	WindowSetDimensions( editorWindow .. "NoteLabel", 400, 310 )
	LabelSetText(editorWindow .. "NoteLabel", TextEditBoxGetText(editorWindow .. "NoteEditBox"))
end

function MapMonster.Editor.OnPosEnterKey()

	local editedPin = GetPinDataFromEditor()
	
	if editedPin.zoneX ~= L"???" and editedPin.zoneY ~= L"???" then
		if editorMode == ADD_MODE then
			CreateMarker(editedPin.zoneId, editedPin.zoneX, editedPin.zoneY)
		elseif editorMode == EDIT_MODE or editorMode == ADDON_EDIT then
			MoveMapPin(editorData.pinId, editedPin.zoneId, editedPin.zoneX, editedPin.zoneY)
		end	
	end
	
end

function MapMonster.Editor.OnPosTextChanged()

	local windowField = SystemData.ActiveWindow.name

	local newPos = {}
	newPos.zoneId = ComboBoxGetSelectedMenuItem(editorWindow .. "ZoneNameComboBox")
	newPos.zoneId = tonumber(zoneComboOrder[newPos.zoneId])
	newPos.zoneX = tonumber(TextEditBoxGetText(editorWindow .. "ZonePosXEditBox"))
	newPos.zoneY = tonumber(TextEditBoxGetText(editorWindow .. "ZonePosYEditBox"))
	
	local goodPos = MapMonsterAPI.ValidateZonePos(newPos)
	if not goodPos then
		goodPos = newPos
		-- most likely out of bounds, set to boundary
		local zoneInfo = MapMonsterAPI.GetZoneData(goodPos.zoneId)
		if not zoneInfo.badZone and not zoneInfo.noMap then
			if goodPos.zoneX > zoneInfo.mapsizeX then
				goodPos.zoneX = zoneInfo.mapsizeX	
			end
			if goodPos.zoneY > zoneInfo.mapsizeY then
				goodPos.zoneY = zoneInfo.mapsizeY
			end
		end
	end

	goodPos = MapMonsterAPI.ZoneToWorld(goodPos)
	SetPositionBoxes(goodPos)

end

function MapMonster.Editor.OnLockChanged()
	local checkBoxName = SystemData.MouseOverWindow.name
	local state = ButtonGetPressedFlag(checkBoxName)
	
	if string.match(checkBoxName, "IsLocked") then
		if state then
			-- disable all the editboxes
			DisableAllEdit()
		else
			-- enable the editboxes
			EnableAllEdit()
			ShowEditorFields()
		end
	elseif string.match(checkBoxName, "IsPrivate") then
	
	end
end

function MapMonster.Editor.OnPrivateChanged()

end

----------------------------------------------------------------------------------------------------
--#					
--#  New Window Handlers
--#					
----------------------------------------------------------------------------------------------------

function MapMonster.Editor.OpenAddWindow(zonePos, pinType)

	--[===[@alpha@
	d("Open Add Window")
	--@end-alpha@]===]
	local zonePos = zonePos
	local pinType = pinType
	
	-- check function args
	if zonePos ~= nil and type(zonePos) ~= "table" then
		pinType = zonePos
		zonePos = nil
	end
	-- set editor mode
	local newMode
	if pinType == nil or 
	   pinType == MapMonster.GENERAL_PINTYPE or 
	   pinType == MapMonster.CHARACTER_PINTYPE 
	then
		newMode = ADD_MODE
	else
		if MapMonsterAPI.PinTypeIsRegistered(pinType) then
			newMode = ADDON_ADD
		else
			newMode = ADD_MODE
		end
	end
	
	-- if the editor isnt already Open or its not an add window set it up from scratch
	if not WindowGetShowing(editorWindow) or editorMode ~= newMode then

		CleanEditorWindow()
		editorMode = newMode

		LabelSetText(editorWindow .. "TitleBarText", T["New Map Monster Pin"])
		TextEditBoxSetText(editorWindow .. "PinLabelEditBox", T["New Map Monster Pin"])

		-- setup proper comboboxes
		if editorMode == ADD_MODE then
			MapMonster.Editor.BuildPinTypeComboBox(false)
			SetComboBox("PinType", 1)
			SetComboBox("SubType", 1)
		else
			local typeData = MapMonsterAPI.GetPinTypeOptions(pinType)
			SetComboBox("PinType", pinTypeComboOrderRev[typeData.id])
			SetComboBox("SubType", 1)
		end

		SetPinNote("")
		LabelSetText(editorWindow .. "DatestampLabel", L"New")

		ShowEditorFields()

		MapMonster.Editor.Open()
	
	end

	local currentPos
	if zonePos then
		currentPos = MapMonsterAPI.ZoneToWorld(zonePos)
	else
		currentPos = {}
		currentPos.zoneId = ComboBoxGetSelectedMenuItem(editorWindow .. "ZoneNameComboBox")
		currentPos.zoneId = tonumber(zoneComboOrder[currentPos.zoneId])
		local zoneInfo = MapMonsterAPI.GetZoneData(currentPos.zoneId)
		currentPos.zoneX = zoneInfo.mapsizeX / 2
		currentPos.zoneY = zoneInfo.mapsizeY / 2
	
		currentPos = MapMonsterAPI.ZoneToWorld(currentPos)
	end
	SetPositionBoxes(currentPos)

	CreateMarker(currentPos.zoneId, currentPos.zoneX, currentPos.zoneY)

end

function MapMonster.Editor.OpenEditWindow(pinId)

	--[===[@alpha@
	d("Open Edit Window")
	--@end-alpha@]===]
	CleanEditorWindow()

	editorMode = EDIT_MODE
	editorData.pinId = pinId

	-- get the pin info
	local pinData = MapMonsterAPI.GetPin(pinId)
	if not pinData then
		local code, message = MapMonsterAPI.GetError()
		SetEditorMessage(message, false)
		AlertTextWindow.AddLine (MapMonster.AlertTypes.ERROR, T["NotFoundAlert"])
		return false
	end

	-- get type info				
	local typeData = MapMonsterAPI.GetPinTypeOptions(pinData.pinType, pinData.subType)

	-- set pin name
	TextEditBoxSetText(editorWindow .. "PinLabelEditBox", pinData.label)

	-- if not mapmonster then limit to static subtypes
	if pinData.pinType ~= MapMonster.GENERAL_PINTYPE and pinData.pinType ~= MapMonster.CHARACTER_PINTYPE then
		editorMode = ADDON_EDIT
	else -- switch to mapmonster only pintypes
		MapMonster.Editor.BuildPinTypeComboBox(false)
	end
		
	-- set static pintype label
	SetComboBox("PinType", pinTypeComboOrderRev[typeData.parent.id])
	SetComboBox("SubType", subTypeComboOrderRev[pinData.subType])
	
	SetPositionBoxes(pinData)

	-- set islocked and isprivate checkboxes
	ButtonSetPressedFlag( editorWindow .. "IsLockedCheckBox", pinData.isLocked )
	ButtonSetPressedFlag( editorWindow .. "IsPrivateCheckBox", pinData.isPrivate )
		
	-- set notes
	local allNotes = L""
	local noteCount = 0
	for i, note in pairs(pinData.note) do
		noteCount = noteCount + 1
		if noteCount > 1 then
			allNotes = allNotes .. L"\n" .. note
		else
			allNotes = note
		end
	end
	--TextEditBoxSetText(editorWindow .. "NoteEditBox", allNotes)
	SetPinNote(allNotes)
	-- set datestamp
	LabelSetText(editorWindow .. "DatestampLabel", towstring(pinData.modified))

	ShowEditorFields()

	MapMonster.Editor.Open()

end

function MapMonster.Editor.OpenViewWindow(pinId)

	--[===[@alpha@
	d("Open View Window")
	--@end-alpha@]===]
	CleanEditorWindow()

	-- get the pin info
	local pinData
	if (type(pinId) == "table") then
		pinData = pinId
	else
		pinData = MapMonsterAPI.GetPin(pinId)
	end
	if not pinData then
		return
	end

	editorMode = VIEW_MODE
	editorData.pinId = pinData.id
	
	LabelSetText(editorWindow .. "TitleBarText", T["Map Monster Pin Viewer"])
	
	-- get type info				
	local typeData = MapMonsterAPI.GetPinTypeOptions(pinData.pinType, pinData.subType)

	-- set pin name
	LabelSetText(editorWindow .. "PinLabel", pinData.label)
	-- set static pintype label
	SetComboBox("PinType", pinTypeComboOrderRev[typeData.parent.id])
	SetComboBox("SubType", subTypeComboOrderRev[pinData.subType])
	-- static zone label
	SetPositionBoxes(pinData)

	-- set islocked and isprivate checkboxes
	ButtonSetPressedFlag( editorWindow .. "IsLockedCheckBox", pinData.isLocked )
	ButtonSetDisabledFlag( editorWindow .. "IsLockedCheckBox", true)
	
	ButtonSetPressedFlag( editorWindow .. "IsPrivateCheckBox", pinData.isPrivate )
	ButtonSetDisabledFlag( editorWindow .. "IsPrivateCheckBox", true)

	-- set notes
	local allNotes = L""
	local noteCount = 0
	for i, note in pairs(pinData.note) do
		noteCount = noteCount + 1
		if noteCount > 1 then
			allNotes = allNotes .. L"\n" .. note
		else
			allNotes = note
		end
	end
	SetPinNote(allNotes)
	-- set datestamp
	LabelSetText(editorWindow .. "DatestampLabel", towstring(pinData.modified))

	ShowEditorFields()

	-- set button labels		
	ButtonSetText(editorWindow .. "MiddleButton", T["Close"])
	
	MapMonster.Editor.Open()

end

function MapMonster.Editor.SaveEditor()

	--[===[@alpha@
	d("Save Pin Details from Editor")
	--@end-alpha@]===]
	local newPin = GetPinDataFromEditor()

	if editorMode == ADD_MODE then
		newPin.merge = false -- make sure we make a new pin not merge from the editor
		local pinId = MapMonsterAPI.CreatePin(newPin)
		if pinId then
			if newPin.isLocked then
				MapMonsterAPI.LockPin(pinId)
			end
		else
			return false
		end
	elseif editorMode == EDIT_MODE or editorMode == ADDON_EDIT then
		local currentPin = MapMonsterAPI.GetPin(editorData.pinId)
		if not currentPin then
			-- doesnt exist anymore
			return false
		else
			-- save it
			--AlertTextWindow.AddLine (MapMonster.AlertTypes.WP_ARRIVED, L"You've arrived at your Waypoint!")
			--MapMonster.Editor.Close()
			-- pin is locked
			local success = MapMonsterAPI.SetPinProperty(newPin)
			if not success then
				-- save failed
				return false
			end
		end
	else
		-- invalid editor mode
		MapMonster.ERROR_CODE = 406
		return false
	end

	return true

end

function MapMonster.DumpEditor()

	d("****Dumping Editor Window Fields****")
	for windowPiece, _ in pairs(allEditorFields) do
		if string.match(windowPiece, "Label$") then
			d(L"[Label] " .. towstring(windowPiece) .. L" = " .. LabelGetText(editorWindow .. windowPiece))
		elseif string.match(windowPiece, "EditBox$") then
			d(L"[EditBox] " .. towstring(windowPiece) .. L" = " .. TextEditBoxGetText(editorWindow .. windowPiece))
		elseif string.match(windowPiece, "CheckBox$") then
			d("[CheckBox] " .. windowPiece .. " = " .. tostring(ButtonGetPressedFlag(editorWindow .. windowPiece)))
		elseif string.match(windowPiece, "ComboBox$") then
			local selected = ComboBoxGetSelectedMenuItem(editorWindow .. windowPiece)
			local selString
			if windowPiece == "ZoneNameComboBox" then
				selString = zoneComboOrder[selected]
			elseif windowPiece == "PinTypeComboBox" then
				selString = pinTypeComboOrder[selected]
			elseif windowPiece == "SubTypeComboBox" then
				selString = subTypeComboOrder[selected]
			end
			d("[ComboBox] " .. windowPiece .. " = " ..  (selString or "nil"))
		end
	end
end

--------------------------------------------------------------------------------
--#
--#			MapMonster API Functions
--#
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.OpenPinAddWindow(zonePos, pinType)
--#		Opens the default MapMonster Pin Add Window for designed pinType.
--#
--#	Parameters:
--#		zonePos    -(table) OPTIONAL Position table for new pin
--#		pinType    -(string) OPTIONAL PinType for new pin
--#	Returns:
--#		none
--#	Notes:
--#		Added in 0.3.8
--#		Can be called by any addon to open the MapMonster New Pin window to add
--#		a pin of a specified type. DO NOT use this to create pins that do not
--#		belong to your addon, any pin created in this manner will not have any
--#		extra addon specific data attached. Just for simple generic pins
--------------------------------------------------------------------------------
function MapMonsterAPI.OpenPinAddWindow( zonePos, pinType )
	MapMonster.Editor.OpenAddWindow( zonePos, pinType )
end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.OpenPinEditorWindow(pinId)
--#		Opens the default MapMonster Pin Editor Window for designed pinId
--#
--#	Parameters:
--#		pinId    -(number) Pin Id to edit
--#	Returns:
--#		none
--#	Notes:
--#		Added in 0.3.8
--------------------------------------------------------------------------------
function MapMonsterAPI.OpenPinEditorWindow(pinId)
	MapMonster.Editor.OpenEditWindow(pinId)
end

--------------------------------------------------------------------------------
--#	Function: MapMonsterAPI.OpenPinDisplayWindow(pinId)
--#		Opens the default MapMonster Pin Viewer Window for designed pinId
--#
--#	Parameters:
--#		pinId    -(number) Pin Id to view
--#	Returns:
--#		none
--#	Notes:
--#		Added in 0.3.8
--------------------------------------------------------------------------------
function MapMonsterAPI.OpenPinDisplayWindow(pinId)
	MapMonster.Editor.OpenViewWindow(pinId)
end
