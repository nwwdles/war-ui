--------------------------------------------------------------------------------
-- Copyright (c) 2008 Bloodwalker <metagamegeek@gmail.com>
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
--
--------------------------------------------------------------------------------
--
-- Starting points for zone offsets and mapsize taken from Nevir27's work on
-- LibSurveyor, all other data and code is original
--
--------------------------------------------------------------------------------
-- File:      Source/MapMonster_Tooltips.lua
-- Date:      2012-08-17T12:40:42Z
-- Author:    Philosound
-- Version:   v0.96
-- Revision:  123
-- File Rev:  123
-- Copyright: 2008
--------------------------------------------------------------------------------

function MapMonster_Tooltip_Update()
end

MapMonster.TooltipsFileLoaded = true

MapMonster.Tooltips = {}

local tooltipWindow = "MapMonsterTooltip"
--------------------------------------------------------------------------------
--#
--#			MapMonster API Functions
--#
--------------------------------------------------------------------------------
local tooltipPieces = { "Icon",
						"Label",
						"Note",
						"Type",
						"SubType",
						"Datestamp",
						}

local MAX_TOOLTIPSECTIONS = 15
local MAX_WIDTH = 480

local SectionSize = {}

local TOTAL_HEIGHT = 0
local LONGEST_LABEL = 0
local LABEL_COUNT = 0
local EXTRAS_COUNT = 0

local LINE_PADDING = 2
local WINDOW_PADDING = 10

local isShowing = false
local curMouseOverWindow = ""

local function ClearTooltip()
	
	WindowClearAnchors(tooltipWindow)
	WindowSetShowing(tooltipWindow, false)
	
	-- clear all tooltip texts
	for i = 1, MAX_TOOLTIPSECTIONS do
		local infoWindow = tooltipWindow .. "Info" .. i
		for k, piece in pairs(tooltipPieces) do
			local pieceName = infoWindow .. piece
			if piece == "Icon" then
				WindowSetShowing( pieceName, false)
			else
				LabelSetText(pieceName, L"")
				WindowSetDimensions( pieceName, MAX_WIDTH, 0 )
			end
		end
		WindowSetShowing( infoWindow .. "DivLine", false)
		WindowSetDimensions( infoWindow, MAX_WIDTH, 0 )
	end
	WindowSetDimensions( tooltipWindow, MAX_WIDTH, 0 )

	TOTAL_HEIGHT = 0
	LONGEST_LABEL = 0
	LABEL_COUNT = 0
	EXTRAS_COUNT = 0
	
	isShowing = false
	curMouseOverWindow = ""
	
end

function MapMonster.InitializeTooltips()
	-- Create the tooltip window
	CreateWindow(tooltipWindow, false)

	function MapMonster_Tooltip_Update()
		if isShowing then            
			if curMouseOverWindow ~= SystemData.MouseOverWindow.name then
				ClearTooltip()
			end
		end
	end
	
end

function MapMonster.ShutdownTooltips()
	DestroyWindow(tooltipWindow)

	function MapMonster_Tooltip_Update()
	end

end

function MapMonster.CreateMapPinTooltip(pinData)

	ClearTooltip()

	curMouseOverWindow = SystemData.MouseOverWindow.name
	isShowing = true
	WindowSetShowing(tooltipWindow, true)
	
	local typeData = MapMonsterAPI.GetPinTypeOptions(pinData.pinType, pinData.subType)
	local indexId = 1
	local windowName = tooltipWindow .. "Info" .. indexId

	local note = L""
	if #pinData.note > 1 then
		for k, pinNote in pairs(pinData.note) do
			if k == 1 then
				note = pinNote
			else
				note = note .. L"\n" .. pinNote
			end
		end
	else
		note = pinData.note[1]
	end

	WindowSetShowing( windowName .. "Icon", true)
	DynamicImageSetTexture( windowName .. "Icon", typeData.mapIcon.texture, 0, 0)
	DynamicImageSetTextureScale( windowName .. "Icon", typeData.mapIcon.scale * 0.7)
	if typeData.mapIcon.slice then
		DynamicImageSetTextureSlice( windowName .. "Icon", typeData.mapIcon.slice)
	end
	if typeData.mapIcon.tint then
		WindowSetTintColor(windowName .. "Icon", typeData.mapIcon.tint.r, typeData.mapIcon.tint.g, typeData.mapIcon.tint.b)
	else
		WindowSetTintColor(windowName .. "Icon", 255, 255, 255)
	end
	
	LabelSetText( windowName .. "Label",    pinData.label)
	LabelSetText( windowName .. "Note",     note)
	LabelSetText( windowName .. "Type",     typeData.parent.label)
	LabelSetText( windowName .. "SubType",  typeData.label)
	LabelSetText( windowName .. "Datestamp", towstring(pinData.modified))

	local totalHeight = 0
	local widestLabel = 0
	local labelCount = 0
	
	for k, piece in pairs(tooltipPieces) do
		local pieceName = windowName .. piece
		local width, height = 0, 0
		if piece == "Icon" then
			-- dont check this
		else
			width, height = LabelGetTextDimensions(pieceName)
		end
		if width > widestLabel then
			widestLabel = width
		end
		totalHeight = totalHeight + height
		labelCount = labelCount + 1
	end
	totalHeight = totalHeight
	if widestLabel > LONGEST_LABEL then
		LONGEST_LABEL = widestLabel
	end
	
	SectionSize[1] = { h = totalHeight, w = widestLabel, count = labelCount }
	
	LABEL_COUNT = LABEL_COUNT + labelCount
	TOTAL_HEIGHT = TOTAL_HEIGHT + totalHeight
	
end

function MapMonster.SetExtraTooltipInfo(index, pinId)

	if index > MAX_TOOLTIPSECTIONS then
		return
	end

	-- show the divider line for the tooltip above this one
	WindowSetShowing( tooltipWindow .. "Info" .. index - 1 .. "DivLine", true)

	local windowName = tooltipWindow .. "Info" .. index
	local pinData = MapMonsterAPI.GetPin(pinId)
	local typeData = MapMonsterAPI.GetPinTypeOptions(pinData.pinType, pinData.subType)

	WindowSetShowing( windowName .. "Icon", true)
	DynamicImageSetTexture( windowName .. "Icon", typeData.mapIcon.texture, 0, 0)
	DynamicImageSetTextureScale( windowName .. "Icon", typeData.mapIcon.scale * 0.7)
	if typeData.mapIcon.slice then
		DynamicImageSetTextureSlice( windowName .. "Icon", typeData.mapIcon.slice)
	end
	if typeData.mapIcon.tint then
		WindowSetTintColor(windowName .. "Icon", typeData.mapIcon.tint.r, typeData.mapIcon.tint.g, typeData.mapIcon.tint.b)
	else
		WindowSetTintColor(windowName .. "Icon", 255, 255, 255)
	end

	LabelSetText( windowName .. "Label",    pinData.label)

	local width, height = LabelGetTextDimensions( windowName .. "Label" )
	if width > LONGEST_LABEL then
		LONGEST_LABEL = width
	end
	
	SectionSize[index] = { h = height, w = width, count = 1 }

	LABEL_COUNT = LABEL_COUNT + 1
	EXTRAS_COUNT = EXTRAS_COUNT + 1
	TOTAL_HEIGHT = TOTAL_HEIGHT + height
	
end

function MapMonster.ResizeTooltip()

	local LABEL_WIDTH = LONGEST_LABEL + 30 -- longest label + 30 for the icon
	local TOOLTIP_WIDTH = LABEL_WIDTH + ( WINDOW_PADDING * 2 )
	local TOOLTIP_HEIGHT  = 0
	
	for i = 1, EXTRAS_COUNT + 1 do
		local infoWindow = tooltipWindow .. "Info" .. i
		local sectionHeight = SectionSize[i].h + ( WINDOW_PADDING * 2 )
		WindowSetDimensions( infoWindow, LABEL_WIDTH, sectionHeight)
		TOOLTIP_HEIGHT = TOOLTIP_HEIGHT + sectionHeight
	end
	WindowSetDimensions( tooltipWindow, TOOLTIP_WIDTH , TOOLTIP_HEIGHT + WINDOW_PADDING )
	
end

function MapMonster.AnchorTooltip()

	local anchor = { Point="bottom",
					RelativeTo="CursorWindow",
					RelativePoint="topleft", 
					XOffset=0, YOffset=0 }

    WindowClearAnchors( tooltipWindow )
    WindowAddAnchor( tooltipWindow, anchor.Point, anchor.RelativeTo, anchor.RelativePoint, anchor.XOffset, anchor.YOffset ) 

    WindowSetAlpha( tooltipWindow, 1.0 )

end

