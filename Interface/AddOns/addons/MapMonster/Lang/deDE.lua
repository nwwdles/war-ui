--------------------------------------------------------------------------------
-- File:      Lang/deDE.lua
-- Date:      2012-08-17T12:40:42Z
-- Author:    Philosound
-- Version:   v0.96
-- Revision:  123
-- File Rev:  123
-- Copyright: 2011
--------------------------------------------------------------------------------
local ERRMSG = LibStub("WAR-AceLocale-3.0"):NewLocale("MapMonster-Errors", "deDE")
if not ERRMSG then return end
-- Missing Data
ERRMSG[101] = "** Es wurden keine Zone Mapping Daten f�r diese Zone gefunden. Weitere Informationen zur Beisteuerung von Map Daten auf MapMonster Seite auf curse.com"
ERRMSG[102] = "Es ist zu fr�h f�r Positionsinformationen. Die Welt ist noch nicht fertig geladen."
ERRMSG[103] = "Zonendaten nicht gefunden."
ERRMSG[104] = "Zone hat keine Map."
ERRMSG[105] = "Pin nicht gefunden"
ERRMSG[106] = "Icondaten nicht gefunden f�r diesen Subtyp."
-- Missing Arguments
ERRMSG[201] = "Keine Positionstabelle mitgeliefert."
ERRMSG[202] = "Die Zonen-Id wurde nicht in der Positionstabelle gefunden."
ERRMSG[203] = "Die Weltkoordinaten wurden nicht in der Positionstabelle gefunden."
ERRMSG[204] = "Die Zonenkoordinaten wurden nicht in der Positionstabelle gefunden."
ERRMSG[205] = "Unvollst�ndige Map-Icon Definition, Textur und Skalierung notwendig."
ERRMSG[206] = "Es fehlen Pindaten. Zwei Sets an Pindaten sind f�r eine Zusammenf�hrung erforderlich."
ERRMSG[207] = "Pintyp nicht gefunden."
ERRMSG[208] = "Pintyp und/oder Subtyp nicht gefunden."
ERRMSG[209] = "Fehlendes Pintyp Label."
ERRMSG[210] = "Fehlende Pin-Id."
ERRMSG[211] = "Fehlende Positionsdaten. Weltkoordinatentabellen f�r zwei Punkte m�ssen bereitgestellt werden, um die Distanz zu kalkulieren."
-- Invalid Arguments
ERRMSG[301] = "Ung�ltiger Pineigenschaftsname."
ERRMSG[302] = "Ung�ltiger Wertetyp f�r die Pineigenschaft."
ERRMSG[303] = "Ung�ltiger Pintyp Optionsname."
ERRMSG[304] = "Ung�ltiger Wertetyp f�r die Pinoption."
ERRMSG[305] = "Ung�ltiger Wertetyp. Map Icon Texturteil muss ein String sein."
ERRMSG[306] = "Ung�ltige Farbdefinition. Map Icon Texturfarbdefinition muss eine Tabelle mit r, g, und b Schl�sseln sein."
ERRMSG[307] = "Ung�ltiger Pintypname, muss ein String sein."
ERRMSG[308] = "Ung�ltiger Pintypname, es k�nnen nur alphanumerische Zeichen verwandt werden."
ERRMSG[309] = "Ung�ltiger Pinsubtypname, muss ein String sein."
ERRMSG[310] = "Ung�ltiger Pinsubtypname, es k�nnen nur alphanumerische Zeichen verwandt werden."
ERRMSG[311] = "Ung�ltige Pintyp oder Subtyp Option."
ERRMSG[312] = "Ung�ltiger Validationstyp, muss entweder \"world\" oder \"zone\" sein."
ERRMSG[313] = "Ung�ltige Positionstabelle. Welt- und Zonenkoordinaten beschreiben nicht den selben Ort."
ERRMSG[314] = "Ung�ltige Position. Koordinaten au�erhalb der Zonengrenzen."
ERRMSG[315] = "Ung�ltige Versionsnummern."
-- Invalid operation
ERRMSG[401] = "Ung�ltige L�schoperation. Es k�nnen NUR Optionen von einem Pinsubtyp gel�scht werden."
ERRMSG[402] = "Ung�ltiger L�schoperation. Es k�nnen nicht mehrere Optionen gleichzeitig gel�scht werden."
ERRMSG[403] = "Ung�ltiger L�schoperation. Der Pin ist verriegelt."
ERRMSG[404] = "Ung�ltiger Editieroperation. Der Pin ist verriegelt."
ERRMSG[405] = "Ung�ltige Richtungsoperation. Die Richtung kann nicht zonen�bergreifend geplottet werden."
ERRMSG[406] = "Ung�ltiger Editiermodus - Dieser Fehler sollte nie gesehen werden. Bitte reporten."
-- Failed operation
ERRMSG[501] = "Der PinTyp ist bereits registriert."
ERRMSG[502] = "Der PinSubTyp ist bereits registriert."
-- General
ERRMSG[601] = "MapMonsterAPI nicht verf�gbar."
ERRMSG[602] = "MapMonster minor version ist out of date"
ERRMSG[603] = "MapMonster major version ist out of date"

local T = LibStub("WAR-AceLocale-3.0"):NewLocale("MapMonster-Editor", "deDE")

-- Editor Labels
T["Map Monster Pin Editor"] = L"MapMonster Pin Editor"
T["New Map Monster Pin"] = L"Neuer MapMonster Pin"
T["Map Monster Pin Viewer"] = L"MapMonster Pin Viewer"
T["Name:"] = L"Name:"
T["Pin Type:"] = L"Pin Typ:"
T["Sub Type:"] = L"Sub Typ:"
T["Zone:"] = L"Zone:"
T["Position:"] = L"Position:"
T["X:"] = L"X:"
T["Y:"] = L"Y:"
T["Options:"] = L"Optionen:"
T["Notes:"] = L"Notizen:"
T["Datestamp:"] = L"Zeitstempel:"
T["World:"] = L"Welt:"
T["Locked"] = L"Verriegelt"
T["Private"] = L"Privat"
T["Save"] = L"Speichern"
T["Reset"] = L"Reset"
T["Cancel"] = L"Abbrechen"
T["Close"] = L"Schlie�en"
-- Messages
T["SuccessAlert"] = L"MapMonster Pin erfolgreich gespeichert!"
T["FailedAlert"] = L"MapMonster Pin Update ist fehlgeschlagen!"
T["NotFoundAlert"] = L"MapMonster Pin wurde nicht gefunden!"

-- PinType Editor Labels

T["Map Monster PinType Editor"] = L"MapMonster PinTyp Editor"
T["Map Monster PinType Viewer"] = L"MapMonster PinTyp Viewer"
T["Map Monster SubType Editor"] = L"MapMonster SubTyp Editor"
T["Map Monster SubType Viewer"] = L"MapMonster SubTyp Viewer"


T["Merge Radius"] = L"Fusionierungsradius"
T["Map Pin Icon:"] = L"Map Pin Icon:"
T["Change"] = L"�ndern"
T["Default Sub Type:"] = L"Default Sub Typ:"
T["Default Options:"] = L"Default Optionen:"
T["Red"] = L"Rot"
T["Green"] = L"Gr�n"
T["Blue"] = L"Blau"

T["PinTypeSuccessAlert"] = L"MapMonster Pin Typ Optionen erfolgreich gespeichert!"

T["PinTypeEditDesc"] = L"Anzeigename f�r diesen Pin Typ in den Filtern und Tooltips."
T["SubTypeComboDesc"] = L"Default Subtyp f�r neue Map Pins dieses Typs."
T["SubTypeEditDesc"] = L"Anzeigename f�r diesen Subtyp in den Filtern und Tooltips."
T["MapIconDesc"] = L"Icon und Farbe f�r diesen Pin Typ f�r Pin Marker auf der Map."
T["RadiusEditDesc"] = L"Wenn zwei Pins mit passenden Labels, Pintyp und Subtyp zusammengef�hrt werden, m�ssen sie innerhalb dieses Radius liegen um zusammengef�hrt zu werden.\n Anmerkung: Wenn dies auf 0 gesetzt ist, ist die Zusammenf�hrung effektiv abgeschaltet, wenn dies auf 65536 gesetzt ist, werden passende Pins immer zusammengef�hrt."
T["LockedDesc"] = L"Neue Pins dieses Typs werden automatisch als verriegelt markiert. Verriegelte Pins k�nnen nicht editiert, zusammengef�hrt oder in irgendeiner anderen Art ge�ndert werden."
T["PrivateDesc"] = L"Neue Pins dieses Typs werden automatisch als privat markiert. Private Pins k�nnen nicht mit anderen geteilt werden."

T["Choose Icon..."] = L"W�hle Icon..."

T["SubTypeCreatedSuccess"] = L"MapMonster Sub Typ erfolgreich erstellt!"
T["SubTypeCreatedFailed"] = L"MapMonster Sub Typ Erstellung fehlgeschlagen!"
--[[
T[""] = L""
T[""] = L""
T[""] = L""
T[""] = L""
T[""] = L""
T[""] = L""
T[""] = L""
T[""] = L""
T[""] = L""
T[""] = L""
T[""] = L""
T[""] = L""
T[""] = L""
--]]
