local T = LibStub("WAR-AceLocale-3.0"):NewLocale("LibDateTime", "esES", false)

if T then

T["January"] = 'Enero'
T["February"] = 'Febrero'
T["March"] = 'Marzo'
T["April"] = 'Abril'
T["May"] = 'Mayo'
T["June"] = 'Junio'
T["July"] = 'Julio'
T["August"] = 'Agosto'
T["September"] = 'Septiembre'
T["October"] = 'Octubre'
T["November"] = 'Noviembre'
T["December"] = 'Diciembre'
T["Monday"] = 'Lunes'
T["Tuesday"] = 'Martes'
T["Wednesday"] = 'Mi�rcoles'
T["Thursday"] = 'Jueves'
T["Friday"] = 'Viernes'
T["Saturday"] = 'S�bado'
T["Sunday"] = 'Domingo'
T["Jan"] = 'Ene'
T["Feb"] = 'Feb'
T["Mar"] = 'Mar'
T["Apr"] = 'Abr'
T["May"] = 'May'
T["Jun"] = 'Jun'
T["Jul"] = 'Jul'
T["Aug"] = 'Ago'
T["Sep"] = 'Sep'
T["Oct"] = 'Oct'
T["Nov"] = 'Nov'
T["Dec"] = 'Dic'
T["Mon"] = 'Lun'
T["Tue"] = 'Mar'
T["Wed"] = 'Mi�'
T["Thu"] = 'Jue'
T["Fri"] = 'Vie'
T["Sat"] = 'S�b'
T["Sun"] = 'Dom'

end