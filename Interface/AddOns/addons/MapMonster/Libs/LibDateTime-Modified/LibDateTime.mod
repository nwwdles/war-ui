<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >
	
    <UiMod name="LibDateTime-Modified" version="0.4.7" date="19/11/2008">
        <Author name="Garko" email="" />
        <Description text="LibDateTime provide locale date and time, formatting function, compare function and clock callback. Modified by Bloodwalker" />
		
		<Dependencies>
			<Dependency name="EA_ChatWindow" />
		</Dependencies>
		
		<Files>
			<File name="lib/LibStub.lua" />
			<File name="lib/AceLocale-3.0.lua" />
			<File name="lang/enEN.lua" />
			<File name="lang/frFR.lua" />
			<File name="lang/deDE.lua" />
			<File name="lang/esES.lua" />
			<File name="lang/itIT.lua" />
			<File name="LibDateTime.lua" />
		</Files>

		<OnUpdate>
			<CallFunction name="LibDateTime_Modified_OnUpdate" />
		</OnUpdate>

	</UiMod>
    
</ModuleFile>
