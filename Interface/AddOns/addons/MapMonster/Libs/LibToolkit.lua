--------------------------------------------------------------------------------
-- Copyright (c) 2008 Bloodwalker <metagamegeek@gmail.com>
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
--
--------------------------------------------------------------------------------
-- File:      Libs/LibToolkit.lua
-- Date:      2012-08-17T12:40:42Z
-- Author:    Philosound
-- Version:   v0.96
-- Revision:  123
-- File Rev:  123
-- Copyright: 2008
--------------------------------------------------------------------------------

local MAJOR,MINOR = "LibToolkit-0.1", 7

local LibToolkit, oldminor = LibStub:NewLibrary(MAJOR, MINOR)

if not LibToolkit then return end -- no upgrade needed

--------------------------------------------------------------------------------
--#
--#			Variable Functions
--#
--------------------------------------------------------------------------------
function LibToolkit.GetGlobal(f)

	local v = _G    -- start with the table of globals
	for w in string.gfind(f, "[%w_]+") do
		v = v[w]
		if v == nil then
			return nil
		end
    end
	return v

end

function LibToolkit.Dump(thing, prefix)

	if not prefix then 
		prefix = ""
	end 

	for k,v in pairs(thing) do 
		d(towstring(prefix..k.." ["..type(v).."]"))
		if type(v)=="table" then
			TomeTracker_Debug.Dump(v, "    "..prefix)
		end
	end

end

--------------------------------------------------------------------------------
--#
--#			Table Functions
--#
--------------------------------------------------------------------------------
function LibToolkit.CopyObject(object)

    if (object == nil) then
        d (L"LibToolkit CopyObject (object): Source variable was nil.")
        return nil
    end

    local lookup_table = {}

    local function _copy(object)
        if type(object) ~= "table" then
            return object
        elseif lookup_table[object] then
            return lookup_table[object]
        end
        local new_table = {}
        lookup_table[object] = new_table
        for index, value in pairs(object) do
            new_table[_copy(index)] = _copy(value)
        end
        return setmetatable(new_table, getmetatable(object))
    end

    return _copy(object)

end

function LibToolkit.IsTableEmpty(object)

    if( object == nil ) then
        return true
    end
	-- nasty way to check for empty
	local empty = true
	for i, k in pairs(object) do
		empty = false
		break
	end

	return empty

end

local KEY_COMPARE = 1
local VALUE_COMPARE = 2
local FULL_COMPARE = 3

local function CompareTable( tableA, tableB, compareType )

	-- Currently only does full compares, keys must match, values must be of same type and equal
	if tableA == nil or tableB == nil or
	   type(tableA) ~= "table" or type(tableB) ~= "table"
	then
		return false
	end

	for k, v in pairs(tableA) do
		if type(v) == "table" then
			if not CompareTable(v, tableB[k]) then
				return false
			end
		elseif tableB[k] == nil or type(tableB[k]) ~= type(v) then
			return false
		elseif tableB[k] ~= v then
			if type(v) == "number" then
				if tostring(v) ~= tostring(tableB[k]) then
					return false
				end
			else
				return false
			end
		end
	end
	return true
end

function LibToolkit.TableCompare( tableA, tableB, compareType )
	if compareType == nil then
		compareType = FULL_COMPARE
	end
	d("call comparetable from table compare")
	return CompareTable( tableA, tableB, compareType )
end

function LibToolkit.TableIntersect( tableA, tableB )
	-- Belongs to both A and B
end

function LibToolkit.TableUnion( tableA, tableB )
	-- Combine both A and B
end

function LibToolkit.TableDiff( tableA, tableB )
	-- Returns members of A not found in B
	if strict == nil then
		strict = true
	end
	
	local result = {}

	for k, v in pairs(tableA) do
		-- missing completely or different types
		if tableB[k] == nil or type(tableB[k]) ~= type(v) then
			result[k] = v
		-- if the values are different
		elseif tableB[k] ~= v then
			if type(v) == "table" then
				d("call comparetable from tablediff")
				if not CompareTable( v, tableB[k], FULL_COMPARE ) then
					result[k] = v
				end
			elseif type(v) == "number" then
				if tostring(v) ~= tostring(tableB[k]) then
					result[k] = v
				end
			else
				result[k] = v
			end
		end
	end
	
	return result
	
end

function LibToolkit.TableComplement( tableA, tableB )
	-- Returns members of B not found in A
	LibToolkit.TableDiff( tableB, tableA )
end


--------------------------------------------------------------------------------
--#
--#			String Functions
--#
--------------------------------------------------------------------------------
function LibToolkit.CleanString(original)
	local clean = string.gsub(original, "%^%a,%a%a", "")
	clean = string.gsub(clean, "%^%a", "")
	return clean
end

function LibToolkit.CleanWString(original)
	local clean = wstring.gsub(original, L"%^%a,in", L"")
	if clean == "" then
		clean = L""
	end
	clean = wstring.gsub(towstring(clean), L"%^%a", L"")
	return clean
end

function LibToolkit.CleanWStringToString(original)
	local clean = LibToolkit.CleanWString(original)
	return WStringToString(clean)
end


--------------------------------------------------------------------------------
--#
--#			Mathmatical Functions
--#
--------------------------------------------------------------------------------
function LibToolkit.roundNum(num, idp)
	return tonumber(string.format("%." .. (idp or 0) .. "f", num))
end

--------------------------------------------------------------------------------
--#
--#			Sorting Functions
--#
--------------------------------------------------------------------------------
function LibToolkit.AlphabetizeByField( table1, table2, field )

    if( table2 == nil ) then
        return false
    end
    if( table1 == nil ) then
        return false
    end
	if ( field == nil ) then
        ERROR(L"Empty specified field for AlphabetizeByField." )
        return false
	end
    if( table1[field] == nil or table2[field] == nil ) then
        ERROR(L"Table must contain specified field for AlphabetizeByField." )
        return false
    end

    return (table1[field] < table2[field])
end

--------------------------------------------------------------------------------
--#
--#			Chat window Functions
--#
--------------------------------------------------------------------------------

function LibToolkit.NewChatFilter(filterId, textColor)
	
	TextLogAddFilterType("Chat", filterId, L"")
	LogDisplaySetFilterColor("EA_ChatTab1TextLog", "Chat", filterId, textColor.r, textColor.g, textColor.b)

	-- Make sure we arent spamming all the chat tabs by turning the filter off for each of them
	for idx, tabData in ipairs(EA_ChatTabManager.Tabs) do
		if (tabData.used == true) then
			LogDisplaySetFilterState("EA_ChatTab" .. idx .. "TextLog", "Chat", filterId, false)
		end
	end

	-- Enable message to the first chat tab only
	LogDisplaySetFilterState("EA_ChatTab1TextLog", "Chat", filterId, true)

end

function LibToolkit.print(filterId, prefix, txt)
    TextLogAddEntry("Chat", filterId, towstring(prefix .. ": " .. txt))
end

--------------------------------------------------------------------------------
--#
--#			Game string Functions
--#
--------------------------------------------------------------------------------
-- WStrings as the game sees them
LibToolkit.PlayerNameW 	= GameData.Player.name
LibToolkit.ServerNameW 	= GameData.Account.ServerName
LibToolkit.RealmNameW 	= GetRealmName(GameData.Player.realm)
LibToolkit.Realm 			= GameData.Player.realm
-- WStrings without the end junk
LibToolkit.Clean = {}
LibToolkit.Clean.PlayerNameW = LibToolkit.CleanWString(LibToolkit.PlayerNameW)
LibToolkit.Clean.ServerNameW = LibToolkit.CleanWString(LibToolkit.ServerNameW)
LibToolkit.Clean.RealmNameW  = LibToolkit.CleanWString(LibToolkit.RealmNameW)
-- Simple clean strings
LibToolkit.Clean.PlayerName = LibToolkit.CleanWStringToString(LibToolkit.Clean.PlayerNameW)
LibToolkit.Clean.ServerName = LibToolkit.CleanWStringToString(LibToolkit.Clean.ServerNameW)
LibToolkit.Clean.RealmName  = LibToolkit.CleanWStringToString(LibToolkit.Clean.RealmNameW)

function LibToolkit.GetZoneName(zoneId)
	return LibToolkit.CleanWString(GetZoneName(zoneId))
end
