<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="PeaceOut" version="1.1" date="24-Jul-17 0:51:34">
        <Author name="NigelTufnel" email="adam.dew@gmail.com" />
        <Description text="Peace Out provides a logout dialog to give more direct feel to the logoff/exit process. 2017 update by anon" />
		<Dependencies />
        <Files>
            <File name="PeaceOut.lua" />
            <File name="PeaceOut.xml" />
        </Files>
        <OnInitialize>
            <CallFunction name="PeaceOut.Init" />
        </OnInitialize>
		<SavedVariables>
			<SavedVariable name="PeaceOut.settings"/>
		</SavedVariables>
		<OnUpdate />
        <OnShutdown />
    </UiMod>
</ModuleFile>