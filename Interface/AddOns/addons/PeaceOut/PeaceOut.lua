----------------------------------------------------------------
-- Peace Out
--
--    Peace Out provides a logout dialog to give more direct feel
--	  to the logoff/exit process. If libslash is enabled, you can 
--	  optionally enable the registering of the /exit and /logoff 
--	  commands with PeaceOut by using /peaceout to toggle the 
--	  usage of these commands.
--
-- Written By NigelTufnel ( Adam.Dew@gmail.com )
----------------------------------------------------------------

----------------------------------------------------------------
-- Global Variables
----------------------------------------------------------------

PeaceOut = {}
-- PO_UseSlash = false

----------------------------------------------------------------
-- Local Variables
----------------------------------------------------------------

local PO_Status = 0
local PO_Val = 0
local PO_ValRange = 0
local PO_Msg, PO_MsgCnt

local PO_TimeDelay = .1
local PO_Time = PO_TimeDelay
local PO_Tick

local PO_MenuHook

----------------------------------------------------------------
-- PeaceOut Functions
----------------------------------------------------------------

function PeaceOut.Init()
	if not PeaceOut.settings then
		PeaceOut.settings = {}
		PeaceOut.settings.sounds = {enable = true, start = 500, cancel = 220}
		PeaceOut.settings.vignette = false
	end
	-- Init some values here
	-- if PO_UseSlash == nil then PO_UseSlash = false end
	RegisterEventHandler(TextLogGetUpdateEventId("System"), "PeaceOut.LogOut")
	
	-- Basic events to use
	RegisterEventHandler(SystemData.Events.LOG_OUT, "PeaceOut.LogOut")
	RegisterEventHandler(SystemData.Events.EXIT_GAME, "PeaceOut.ExitGame")
	-- if PO_UseSlash then 
		-- RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "PeaceOut.SlashCheck")
	-- 	PO_TimeDelay = .8
	-- end
	
	-- Set window for use
	CreateWindow("PeaceOut", true)  
	WindowSetShowing("PeaceOut", false)
	-- ButtonSetText("PeaceOutCancel", L"Cancel")
	WindowSetTintColor("PeaceOutBg", 220, 220, 220)
	LabelSetText("PeaceOutDisplay", L"Logging out")--GetFormatStringFromTable("Hardcoded", 464, {20}))

	-- x,y = LabelGetTextDimensions("PeaceOutDisplay")
	-- WindowSetDimensions("PeaceOut", x + 40, 124)
	-- WindowSetDimensions("PeaceOutTitleBar", x + 45, 30)
	-- WindowSetDimensions("PeaceOutDisplay", x + 5, 20)

	-- Setup our message check
	PO_MsgCnt = TextLogGetNumEntries("System")
	if PO_MsgCnt > 0 then 
		PO_Msg = select(3, TextLogGetEntry("System", TextLogGetNumEntries("System") - 1))
	end
	
	-- Hook the menu toggle to utilize escaping out of logout
	PO_MenuHook = MainMenuWindow.ToggleShowing
	MainMenuWindow.ToggleShowing = PeaceOut.MenuHook
	RegisterEventHandler(SystemData.Events.LOADING_END, "PeaceOut.DelayInit")
	RegisterEventHandler(SystemData.Events.INTERFACE_RELOADED, "PeaceOut.DelayInit")
end 

function PeaceOut.DelayInit()
	if LibSlash then LibSlash.RegisterSlashCmd("peaceout", function(input) PeaceOut.Slash(input) end) end
	UnregisterEventHandler(SystemData.Events.LOADING_END, "PeaceOut.DelayInit")
	UnregisterEventHandler(SystemData.Events.INTERFACE_RELOADED, "PeaceOut.DelayInit")
end

-- Allows us to press escape to end logout
function PeaceOut.MenuHook(...)
	-- Bail if our window is open and end logout process
	if WindowGetShowing("PeaceOut") then 
		PeaceOut.Cancel()
		return
	end
	PO_MenuHook(...)
end

function PO_Tick()
	-- Get system messages
	--local msgcount = TextLogGetNumEntries("System")
	--if msgcount == 0 then return end
	--local msg = select(3, TextLogGetEntry("System", TextLogGetNumEntries("System") - 1))
	
	local _, filterId, msg = TextLogGetEntry( "System", TextLogGetNumEntries("System") - 1 ) 
	-- If we have a new message
	if msg ~= PO_Msg then
		PO_Msg = msg
		-- Init if we have a 20s message, adjust at other time intervals
		if msg:find(L"You will finish logging out in 20 seconds.") then--GetFormatStringFromTable("Hardcoded", 464, {20}) then
			PO_Val = .5
			PO_ValRange = 0
			PeaceOut.UpdateLabel()
			WindowSetShowing("PeaceOut", true)

			if PeaceOut.settings.sounds.enable then
				PlaySound(PeaceOut.settings.sounds.start)
			end

			-- if PeaceOut.settings.vignette then
				WindowSetShowing( "ScreenFlashWindow", PeaceOut.settings.vignette )

	   			WindowStartAlphaAnimation(  "ScreenFlashWindow",
	                                Window.AnimationType.SINGLE_NO_RESET,
	                                0,                                
	                                1,
	                                20-PO_Val,
	                                true, 0, 0 )
				WindowSetTintColor("ScreenFlashWindow", 0, 0, 0)
			-- end

			LabelSetTextColor("PeaceOutDisplay", 255, 255, 255)

		elseif msg:find(L"You will finish logging out in 15 seconds.") then--GetFormatStringFromTable("Hardcoded", 464, {15}) then
			PO_Val = 5
			PO_ValRange = 5
			PeaceOut.UpdateLabel()
		elseif msg:find(L"You will finish logging out in 10 seconds.") then--GetFormatStringFromTable("Hardcoded", 464, {10}) then
			PO_Val = 10
			PO_ValRange = 10
			PeaceOut.UpdateLabel()
		elseif msg:find(L"You will finish logging out in 5 seconds.") then--GetFormatStringFromTable("Hardcoded", 464, {5}) then
			PO_Val = 15
			PO_ValRange = 15
			PeaceOut.UpdateLabel()
			LabelSetTextColor("PeaceOutDisplay", 255, 0, 0)
			
		-- Bail us out if we are done logging out
		elseif msg:find(L"You are no longer logging out.") then--GetStringFromTable("Hardcoded", 465) then
			-- Hide window + disable updater
			WindowSetShowing("PeaceOut", false)
			UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "PeaceOut.TickCheck")
			PO_Status = 0

			--if PeaceOut.settings.vignette then
				WindowSetTintColor("ScreenFlashWindow", 
					ScreenFlashWindow.DAMAGE_ALERT_COLOR.r, ScreenFlashWindow.DAMAGE_ALERT_COLOR.g, ScreenFlashWindow.DAMAGE_ALERT_COLOR.b )
				WindowSetShowing("ScreenFlashWindow", false)
			--end

			if PeaceOut.settings.sounds.enable then
				PlaySound(PeaceOut.settings.sounds.cancel)
			end
			-- Re-init slash checker if we want to use it
			-- if PO_UseSlash then 
			-- 	PO_TimeDelay = .8
			-- 	RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "PeaceOut.SlashCheck") 
			-- end
			return
		end
	end
	
	-- If the current time is in allowable range then update the label to have it
	if math.floor(PO_Val) ~= PO_ValRange and math.floor(PO_Val) < PO_ValRange + 5 then
		PeaceOut.UpdateLabel()
	end
end

function PeaceOut.UpdateLabel()
	-- Clean up the label setting a bit
	-- if PO_Status == 1 then 
	-- 	LabelSetText("PeaceOutDisplay", GetFormatStringFromTable("Hardcoded", 464, {20 - math.floor(PO_Val)}))
	-- else
	-- 	if towstring("You will finish logging out in " .. 20 - math.floor(PO_Val) .. " seconds.") == GetFormatStringFromTable("Hardcoded", 464, {20 - math.floor(PO_Val)}) then
		LabelSetText("PeaceOutDisplay", towstring("" .. 20 - math.floor(PO_Val) .. " seconds left"))
	-- 	else
			-- LabelSetText("PeaceOutDisplay", GetFormatStringFromTable("Hardcoded", 464, {20 - math.floor(PO_Val)}))
	-- 	end
	-- end
end

-- function PeaceOut.SlashCheck(elapsed)
	
-- 	-- Setup basic throttling
-- 	PO_Time = PO_Time - elapsed
-- 	if PO_Time > 0 then return end
-- 	PO_Time = PO_TimeDelay
	
-- 	-- If we have a new system message
-- 	if slashMsgCount == TextLogGetNumEntries("System") then return end
-- 	PO_MsgCnt = TextLogGetNumEntries("System")
	
-- 	-- See if we are being logged out, and catch the tick up to us
-- 	if select(3, TextLogGetEntry("System", PO_MsgCnt - 1)) == GetStringFromTable("Hardcoded", 465) then
-- 		d(TextEditBoxGetHistory("EA_TextEntryGroupEntryBoxTextInput")[1])
-- 		if TextEditBoxGetHistory("EA_TextEntryGroupEntryBoxTextInput")[1] == L"/logout" then
-- 			SetupAction(1)
-- 		elseif TextEditBoxGetHistory("EA_TextEntryGroupEntryBoxTextInput")[1] == L"/exit" then
-- 			SetupAction(2)
-- 		elseif TextEditBoxGetHistory("EA_TextEntryGroupEntryBoxTextInput")[1] == L"/q" then
-- 			SetupAction(2)
-- 		end
-- 	end	
-- end

function PeaceOut.TickCheck(elapsed)
	-- Basic throttle for updating time left till logout
	PO_Time = PO_Time - elapsed
	PO_Val = PO_Val + elapsed
	if PO_Time > 0 then return end
	PO_Time = PO_TimeDelay
	
	PO_Tick()
end

function SetupAction(state)

	-- Canceling the logout so bail
	if WindowGetShowing("PeaceOut") then return end
	
	-- Hide window and make sure event registers are all good
	WindowSetShowing("MainMenuWindow", false)
	RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "PeaceOut.TickCheck")
	-- if PO_UseSlash then UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "PeaceOut.SlashCheck") PO_TimeDelay = .1 end
	
	if state == 2 then LabelSetText("PeaceOutTitle", L"Exiting Game")
	elseif state == 1 then LabelSetText("PeaceOutTitle", L"Logging Out") end
	
	-- Apply the 1st tick
	PO_Status = state
	PO_Tick()
end

-- Basic toggle for slash exit and logout command registration
-- function PeaceOut.Slash(input)
-- 	PO_UseSlash = not PO_UseSlash
	
-- 	if WindowGetShowing("PeaceOut") then return end
	
-- 	if PO_UseSlash then
-- 		RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "PeaceOut.SlashCheck")
-- 		PO_TimeDelay = .8
-- 		EA_ChatWindow.Print(L"PeaceOut will now use /exit and /logout")
-- 	else
-- 		UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "PeaceOut.SlashCheck") 
-- 		PO_TimeDelay = .1
-- 		EA_ChatWindow.Print(L"PeaceOut will no longer use /exit and /logout")
-- 	end
-- end

-- Some ultra basic utilities

function PeaceOut.LogOut()
	SetupAction(1)
end

function PeaceOut.ExitGame()
	SetupAction(2)
end 

function PeaceOut.Cancel()
	BroadcastEvent( SystemData.Events.SPELL_CAST_CANCEL )
	--BroadcastEvent( SystemData.Events.LOG_OUT )
end 

function PeaceOut.OnRButtonUp()
	EA_Window_ContextMenu.CreateContextMenu( SystemData.ActiveWindow.name)
	local function getCheckboxIconText(state)
    	if state then
    		return L"<icon00057> "
    	else
    		return L"<icon00058> "
    	end
    end

	EA_Window_ContextMenu.AddMenuItem( getCheckboxIconText(PeaceOut.settings.vignette)..L"Vignette", 
		function() PeaceOut.settings.vignette = not PeaceOut.settings.vignette 
			WindowSetShowing( "ScreenFlashWindow", PeaceOut.settings.vignette ) 
			if PeaceOut.settings.vignette then
				WindowStartAlphaAnimation(  "ScreenFlashWindow",
	                                Window.AnimationType.SINGLE_NO_RESET,
	                                0,                                
	                                1,
	                                20-PO_Val,
	                                true, 0, 0 )
			end
			end, false, true )

	EA_Window_ContextMenu.AddMenuItem( getCheckboxIconText(PeaceOut.settings.sounds.enable)..L"Sounds", 
		function() PeaceOut.settings.sounds.enable = not PeaceOut.settings.sounds.enable end, false, true )

	EA_Window_ContextMenu.Finalize()
end