<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">	
  <UiMod name="Lib RuString" version="1.3" date="26/01/2010" >		
    <Author name="Raidez" email="raidez(a)mail.ru" />		
    <Description text="Konvertiruet stroki s kirillicei.<br>Slash command: /forcedrustrings" />          
      <VersionSettings gameVersion="1.9.9" windowsVersion="1.0" savedVariablesVersion="1.0" />             
      <Dependencies/>         		
      <Files>			
        <File name="RuStringLib.lua" />		
      </Files>    
      <SavedVariables>      
        <SavedVariable name="LibRuString.Settings" />    
      </SavedVariables>    
      <OnInitialize>			
        <CallFunction name="LibRuString.Init" />		
      </OnInitialize>		     
      <OnUpdate/>		
      <OnShutdown/>      
      <WARInfo>        
        <Categories>          
          <Category name="SYSTEM" />          
          <Category name="DEVELOPMENT" />        
        </Categories>        
        <Careers>          
          <Career name="BLACKGUARD" />          
          <Career name="WITCH_ELF" />          
          <Career name="DISCIPLE" />          
          <Career name="SORCERER" />          
          <Career name="IRON_BREAKER" />          
          <Career name="SLAYER" />          
          <Career name="RUNE_PRIEST" />          
          <Career name="ENGINEER" />          
          <Career name="BLACK_ORC" />          
          <Career name="CHOPPA" />          
          <Career name="SHAMAN" />          
          <Career name="SQUIG_HERDER" />          
          <Career name="WITCH_HUNTER" />          
          <Career name="KNIGHT" />          
          <Career name="BRIGHT_WIZARD" />          
          <Career name="WARRIOR_PRIEST" />          
          <Career name="CHOSEN" />          
          <Career name="MARAUDER" />          
          <Career name="ZEALOT" />          
          <Career name="MAGUS" />          
          <Career name="SWORDMASTER" />          
          <Career name="SHADOW_WARRIOR" />          
          <Career name="WHITE_LION" />          
          <Career name="ARCHMAGE" />        
        </Careers>      
      </WARInfo>          	
  </UiMod>
</ModuleFile>