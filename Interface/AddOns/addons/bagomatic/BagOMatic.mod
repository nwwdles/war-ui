<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<UiMod name="BagOMatic" version="v1.33" date="2012-04-13T22:50:48Z" >
	<Author name="Differential and Eowoyn" email="no@way" />
	<VersionSettings gameVersion="1.4.5" windowsVersion="1.0" savedVariablesVersion="1.0" />

	<Description text="Backpack manager" />

	<WARInfo>
	    <Categories>
	        <Category name="OTHER" />
	    </Categories>
	</WARInfo>

	<Dependencies>
		<Dependency name="LibSlash"/>
		<Dependency name="EA_BackpackWindow"/>
		<Dependency name="EASystem_Strings"/>
		<Dependency name="EASystem_Utils"/>
	</Dependencies>

	<Files>
		<File name="BagOMatic.lua"/>
		<File name="BagOMaticData.lua"/>
		<File name="BagOMatic.xml"/>
	</Files>

	<SavedVariables>
		<SavedVariable name="BagOMatic.saved"/>
	</SavedVariables>

	<OnInitialize>
		<CallFunction name="BagOMatic.init"/>
	</OnInitialize>

	<OnUpdate>
		<CallFunction name="BagOMatic.update"/>
	</OnUpdate>

	<OnShutdown/>
</UiMod>
</ModuleFile>