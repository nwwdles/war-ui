LayoutEditor.Settings = 
{
	ClosetGoblinOptionWindow = 
	{
		locked = false,
		hidden = false,
	},
	CDownWindow = 
	{
		locked = false,
		hidden = false,
	},
	EnemyGuardDistanceIndicator = 
	{
		locked = false,
		hidden = false,
	},
	ObsidianCastbar = 
	{
		locked = false,
		hidden = false,
	},
	HostileTarget = 
	{
		locked = false,
		hidden = false,
	},
	EnemyCombatLogEpsWindow = 
	{
		locked = false,
		hidden = false,
	},
	WARCommanderButton = 
	{
		locked = false,
		hidden = true,
	},
	PrimaryTargetLayoutWindow = 
	{
		locked = false,
		hidden = false,
	},
	PureTargetUnitFrameFriendly = 
	{
		locked = false,
		hidden = false,
	},
	EA_Window_ScenarioTracker = 
	{
		locked = false,
		hidden = false,
	},
	RVMOD_PlayerStatus = 
	{
		locked = false,
		hidden = false,
	},
	EA_Window_OverheadMap = 
	{
		locked = false,
		hidden = false,
	},
	GroupMember6 = 
	{
		locked = false,
		hidden = false,
	},
	DAoCBuff_Hostile_All = 
	{
		locked = false,
		hidden = false,
	},
	WARCommander = 
	{
		locked = false,
		hidden = false,
	},
	GroupMember1 = 
	{
		locked = false,
		hidden = false,
	},
	BattlegroupHUDGroup2LayoutWindow = 
	{
		locked = false,
		hidden = true,
	},
	EnemyUnitFramesAnchor1 = 
	{
		locked = false,
		hidden = false,
	},
	EA_Window_BattlefieldObjectiveTracker = 
	{
		locked = false,
		hidden = false,
	},
	ZonePOPWnd = 
	{
		locked = false,
		hidden = false,
	},
	EnemyUnitFramesAnchor2 = 
	{
		locked = false,
		hidden = false,
	},
	RoR_SoR_Window = 
	{
		locked = false,
		hidden = false,
	},
	EA_ActionBar1 = 
	{
		locked = false,
		hidden = false,
	},
	EA_ChatWindowGroup1 = 
	{
		locked = false,
		hidden = false,
	},
	DAoCBuff_Friendly_All = 
	{
		locked = false,
		hidden = false,
	},
	EA_Window_OpenPartyFlyOutAnchor = 
	{
		locked = false,
		hidden = true,
	},
	followTheLeaderWindow = 
	{
		locked = false,
		hidden = false,
	},
	BattlegroupHUDGroup1LayoutWindow = 
	{
		locked = false,
		hidden = true,
	},
	LayerTimerWindow = 
	{
		locked = false,
		hidden = false,
	},
	EA_GrantedAbilities = 
	{
		locked = false,
		hidden = false,
	},
	RVMOD_ManagerToggleButtonWindow = 
	{
		locked = false,
		hidden = false,
	},
	EA_Window_CityTracker = 
	{
		locked = false,
		hidden = false,
	},
	PlayerHP = 
	{
		locked = false,
		hidden = false,
	},
	ResToggle = 
	{
		locked = false,
		hidden = true,
	},
	EnemyTimer = 
	{
		locked = false,
		hidden = false,
	},
	LIBGUI_Window90 = 
	{
		locked = false,
		hidden = false,
	},
	EA_MoraleBar = 
	{
		locked = false,
		hidden = false,
	},
	Castbar = 
	{
		locked = false,
		hidden = false,
	},
	TurretRangeDisplay = 
	{
		locked = false,
		hidden = false,
	},
	FloatingScenarioGroup4Window = 
	{
		locked = false,
		hidden = true,
	},
	EA_ChatWindowGroup4 = 
	{
		locked = false,
		hidden = false,
	},
	EA_Window_KeepObjectiveTracker = 
	{
		locked = false,
		hidden = false,
	},
	PlayerWindow = 
	{
		locked = false,
		hidden = false,
	},
	EA_Window_ZoneControl = 
	{
		locked = false,
		hidden = false,
	},
	PlayerAP = 
	{
		locked = false,
		hidden = false,
	},
	EA_ActionBar4 = 
	{
		locked = false,
		hidden = false,
	},
	GroupWindow = 
	{
		locked = false,
		hidden = true,
	},
	EA_StanceBar = 
	{
		locked = false,
		hidden = false,
	},
	EnemyIcon = 
	{
		locked = false,
		hidden = true,
	},
	PurePlayerUnitFrame = 
	{
		locked = false,
		hidden = false,
	},
	MotionWindow = 
	{
		locked = false,
		hidden = false,
	},
	EA_ActionBar3 = 
	{
		locked = false,
		hidden = false,
	},
	FriendlyTarget = 
	{
		locked = false,
		hidden = false,
	},
	EA_AssistWindow = 
	{
		locked = false,
		hidden = false,
	},
	Pet = 
	{
		locked = false,
		hidden = false,
	},
	EA_ActionBar1RightCap = 
	{
		locked = false,
		hidden = true,
	},
	EA_HelpTipsContainerWindow = 
	{
		locked = false,
		hidden = true,
	},
	FloatingScenarioGroup3Window = 
	{
		locked = false,
		hidden = true,
	},
	EA_Window_CampaignMap = 
	{
		locked = false,
		hidden = true,
	},
	PureTargetHUDHostile = 
	{
		locked = false,
		hidden = false,
	},
	EA_ActionBar1LeftCap = 
	{
		locked = false,
		hidden = true,
	},
	BattlegroupHUDGroup4LayoutWindow = 
	{
		locked = false,
		hidden = true,
	},
	PureTargetHUDFriendly = 
	{
		locked = false,
		hidden = false,
	},
	EA_Window_PublicQuestTracker = 
	{
		locked = false,
		hidden = false,
	},
	DuffTimer_Win3 = 
	{
		locked = false,
		hidden = false,
	},
	EnemyKillSpamDialog = 
	{
		locked = false,
		hidden = false,
	},
	PurePlayerPetTargetUnitFrame = 
	{
		locked = false,
		hidden = false,
	},
	XpBarWindow = 
	{
		locked = false,
		hidden = true,
	},
	EA_TacticsEditor = 
	{
		locked = false,
		hidden = false,
	},
	TargetHostile = 
	{
		locked = false,
		hidden = false,
	},
	FloatingScenarioGroup1Window = 
	{
		locked = false,
		hidden = true,
	},
	GroupMember4 = 
	{
		locked = false,
		hidden = false,
	},
	GroupMember2 = 
	{
		locked = false,
		hidden = false,
	},
	PlayerCareer = 
	{
		locked = false,
		hidden = false,
	},
	EnemyCombatLogTargetDefeseTotalWindow = 
	{
		locked = false,
		hidden = true,
	},
	EnemyTalismanAlerterIndicator = 
	{
		locked = false,
		hidden = false,
	},
	EnemyPlayerKDR = 
	{
		locked = false,
		hidden = false,
	},
	TarFriHUD = 
	{
		locked = false,
		hidden = false,
	},
	EA_Window_RvRTracker = 
	{
		locked = false,
		hidden = false,
	},
	WARCommanderInfo = 
	{
		locked = false,
		hidden = false,
	},
	EnemyMarksWindow = 
	{
		locked = false,
		hidden = false,
	},
	DAoCBuff_Self_All = 
	{
		locked = false,
		hidden = false,
	},
	CMapWindow = 
	{
		locked = false,
		hidden = false,
	},
	GroupMember5 = 
	{
		locked = false,
		hidden = false,
	},
	WhatsCookingWindow = 
	{
		locked = false,
		hidden = false,
	},
	FloatingScenarioGroup6Window = 
	{
		locked = false,
		hidden = true,
	},
	MotionIcon = 
	{
		locked = false,
		hidden = false,
	},
	SecondaryTargetLayoutWindow = 
	{
		locked = false,
		hidden = false,
	},
	RoR_Window_ScenarioSurrender = 
	{
		locked = false,
		hidden = false,
	},
	EA_CareerResourceWindowActionBar = 
	{
		locked = false,
		hidden = false,
	},
	["LibAddonButton.Created.1"] = 
	{
		locked = false,
		hidden = false,
	},
	PureGroupLayout = 
	{
		locked = false,
		hidden = false,
	},
	RpBarWindow = 
	{
		locked = false,
		hidden = true,
	},
	PureTargetUnitFrameHostile = 
	{
		locked = false,
		hidden = false,
	},
	EA_ActionBar2 = 
	{
		locked = false,
		hidden = false,
	},
	BattlegroupHUDGroup3LayoutWindow = 
	{
		locked = false,
		hidden = true,
	},
	EnemyTarget = 
	{
		locked = false,
		hidden = false,
	},
	PurePlayerPetUnitFrame = 
	{
		locked = false,
		hidden = false,
	},
	DAoCBuff_Hostile_Debuffs = 
	{
		locked = false,
		hidden = false,
	},
	EnemyCombatLogIDSAnchor = 
	{
		locked = false,
		hidden = false,
	},
	MiracleGrow2Icon = 
	{
		locked = false,
		hidden = false,
	},
	SwiftAssist = 
	{
		locked = false,
		hidden = false,
	},
	NerfedIcon = 
	{
		locked = false,
		hidden = false,
	},
	LibAddonButtonfx = 
	{
		locked = false,
		hidden = false,
	},
	ActionFractionWindow = 
	{
		locked = false,
		hidden = false,
	},
	MenuBarWindow = 
	{
		locked = false,
		hidden = true,
	},
	GroupMember3 = 
	{
		locked = false,
		hidden = false,
	},
	FloatingScenarioGroup2Window = 
	{
		locked = false,
		hidden = true,
	},
	EA_ActionBar5 = 
	{
		locked = false,
		hidden = false,
	},
	EnemyKilledBy = 
	{
		locked = false,
		hidden = false,
	},
	EnemyCombatLogTargetDefeseWindow = 
	{
		locked = false,
		hidden = true,
	},
	EA_Window_RRQTracker = 
	{
		locked = false,
		hidden = false,
	},
	TarHosHUD = 
	{
		locked = false,
		hidden = false,
	},
	DuffTimer_Win2 = 
	{
		locked = false,
		hidden = false,
	},
	EA_ChatWindowGroup2 = 
	{
		locked = false,
		hidden = false,
	},
	TargetFriendly = 
	{
		locked = false,
		hidden = false,
	},
	MouseOverTargetWindow = 
	{
		locked = false,
		hidden = false,
	},
	PlayerPet = 
	{
		locked = false,
		hidden = false,
	},
	Options = 
	{
		snapDistance = 5,
		snapWindowsEnabled = true,
	},
	EA_CareerResourceWindow = 
	{
		locked = false,
		hidden = false,
	},
	MiracleGrow2 = 
	{
		locked = false,
		hidden = false,
	},
	warwhispererMain = 
	{
		locked = false,
		hidden = false,
	},
	EA_ChatWindowGroup3 = 
	{
		locked = false,
		hidden = false,
	},
	PlayerPetTarget = 
	{
		locked = false,
		hidden = false,
	},
	FloatingScenarioGroup5Window = 
	{
		locked = false,
		hidden = true,
	},
	EA_Window_WinOMeter = 
	{
		locked = false,
		hidden = false,
	},
	EA_Window_QuestTrackerNub = 
	{
		locked = false,
		hidden = false,
	},
	DuffTimer_Win1 = 
	{
		locked = false,
		hidden = false,
	},
	EA_Window_QuestTracker = 
	{
		locked = false,
		hidden = false,
	},
	RoR_SoR_Button = 
	{
		locked = false,
		hidden = false,
	},
	Moth = 
	{
		locked = false,
		hidden = false,
	},
	ActionPointsWindow = 
	{
		locked = false,
		hidden = false,
	},
	QueueQueuer_GUI_MapButtons = 
	{
		locked = false,
		hidden = false,
	},
}



