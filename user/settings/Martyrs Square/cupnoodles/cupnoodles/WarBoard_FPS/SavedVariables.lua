WarBoard_FPSSettings = 
{
	AvgCounter = 
	{
		Low = 10,
		Med = 30,
	},
	ShowAvg = true,
	FPSLow = 
	{
		R = 255,
		G = 0,
		B = 0,
	},
	FPSHigh = 
	{
		R = 0,
		G = 255,
		B = 0,
	},
	FPSMed = 
	{
		R = 255,
		G = 255,
		B = 0,
	},
	version = "0.3.2",
	FPSCounter = 
	{
		Low = 10,
		Med = 30,
	},
	RefreshRate = 2,
	AvgLow = 
	{
		R = 255,
		G = 0,
		B = 0,
	},
	AvgMed = 
	{
		R = 255,
		G = 255,
		B = 0,
	},
	AvgHigh = 
	{
		R = 0,
		G = 255,
		B = 0,
	},
}



