Vectors.data = 
{
	profiles = 
	{
		default = 
		{
			["1.77"] = 
			{
				EA_Window_QuestTracker = 
				{
					anchors = 
					{
						[1] = 
						{
							parentpoint = "topright",
							x = 0,
							point = "topright",
							parent = "EA_Window_QuestTrackerNub",
							y = 0.03,
						},
					},
					name = "EA_Window_QuestTracker",
					scale = 0.8,
				},
			},
			["1.60"] = 
			{
				MotionWindow = 
				{
					anchors = 
					{
						[1] = 
						{
							parentpoint = "bottom",
							x = 0,
							point = "top",
							parent = "MotionIcon",
							y = 0,
						},
					},
					name = "MotionWindow",
					scale = 1.0227255821228,
				},
				MiracleGrow2 = 
				{
					anchors = 
					{
						[1] = 
						{
							parentpoint = "bottom",
							x = 0,
							point = "top",
							parent = "MiracleGrow2Icon",
							y = 0,
						},
					},
					name = "MiracleGrow2",
					scale = 0.98734736442566,
				},
			},
		},
	},
	activeprofile = "default",
}



