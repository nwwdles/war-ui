LPET_CONFIG = 
{
	autoswitch = 2,
	attackbuttoncombo = 1,
	ClawSweep = 
	{
		mode = 2,
		healthLimit = 5,
		priority = 1,
	},
	FlamesOfChange = 
	{
		mode = 2,
		healthLimit = 5,
		priority = 1,
	},
	career = 67,
	ShockGrenade = 
	{
		mode = 2,
		healthLimit = 5,
		priority = 1,
	},
	Shred = 
	{
		mode = 2,
		healthLimit = 5,
		priority = 1,
	},
	SteamVent = 
	{
		mode = 2,
		healthLimit = 5,
		priority = 1,
	},
	DaemonicConsumption = 
	{
		mode = 2,
		healthLimit = 5,
		priority = 1,
	},
	name = "Custom",
	autodefend = 1,
	DeathFromAbove = 
	{
		mode = 2,
		healthLimit = 5,
		priority = 1,
	},
	quickSettings = 0,
	HighExplosiveGrenade = 
	{
		mode = 2,
		healthLimit = 5,
		priority = 1,
	},
	DaemonicFire = 
	{
		mode = 2,
		healthLimit = 5,
		priority = 2,
	},
	SpineFling = 
	{
		mode = 2,
		healthLimit = 5,
		priority = 1,
	},
	attackrangecheck = 1,
	autofollow = 1,
	PoisonedSpine = 
	{
		mode = 2,
		healthLimit = 5,
		priority = 1,
	},
	autoattack = 1,
	Flamethrower = 
	{
		mode = 2,
		healthLimit = 5,
		priority = 1,
	},
	FangAndClaw = 
	{
		mode = 2,
		healthLimit = 5,
		priority = 1,
	},
	switchrangecheck = 1,
	Maul = 
	{
		mode = 2,
		healthLimit = 5,
		priority = 1,
	},
	FlameOfTzeentch = 
	{
		mode = 2,
		healthLimit = 5,
		priority = 1,
	},
	MachineGun = 
	{
		mode = 2,
		healthLimit = 5,
		priority = 1,
	},
	CoruscatingEnergy = 
	{
		mode = 2,
		healthLimit = 5,
		priority = 1,
	},
	Gore = 
	{
		mode = 2,
		healthLimit = 5,
		priority = 1,
	},
	selftargetfollow = 1,
	GoopShootin = 
	{
		mode = 2,
		healthLimit = 5,
		priority = 1,
	},
	defendrangecheck = 1,
	GutRipper = 
	{
		mode = 2,
		healthLimit = 5,
		priority = 1,
	},
	SquigSqueal = 
	{
		mode = 2,
		healthLimit = 5,
		priority = 1,
	},
	version = "2.9.2",
	followbuttoncombo = 1,
	LionsRoar = 
	{
		mode = 2,
		healthLimit = 5,
		priority = 1,
	},
	PenetratingRound = 
	{
		mode = 2,
		healthLimit = 5,
		priority = 1,
	},
	SporeCloud = 
	{
		mode = 2,
		healthLimit = 5,
		priority = 1,
	},
	Bite = 
	{
		mode = 2,
		healthLimit = 5,
		priority = 1,
	},
	HeadButt = 
	{
		mode = 2,
		healthLimit = 5,
		priority = 1,
	},
	LegTear = 
	{
		mode = 2,
		healthLimit = 5,
		priority = 1,
	},
	TerrifyingRoar = 
	{
		mode = 2,
		healthLimit = 5,
		priority = 1,
	},
	WarpingEnergy = 
	{
		mode = 2,
		healthLimit = 5,
		priority = 1,
	},
}



LPET_PROFILES = 
{
	[1] = 
	{
		autoswitch = 1,
		attackbuttoncombo = 1,
		ClawSweep = 
		{
			mode = 2,
			healthLimit = 5,
			priority = 1,
		},
		FlamesOfChange = 
		{
			mode = 2,
			healthLimit = 5,
			priority = 1,
		},
		career = 27,
		ShockGrenade = 
		{
			mode = 2,
			healthLimit = 5,
			priority = 1,
		},
		Shred = 
		{
			mode = 2,
			healthLimit = 5,
			priority = 1,
		},
		SteamVent = 
		{
			mode = 2,
			healthLimit = 5,
			priority = 1,
		},
		DaemonicConsumption = 
		{
			mode = 2,
			healthLimit = 5,
			priority = 1,
		},
		name = "Default Profile",
		autodefend = 1,
		DeathFromAbove = 
		{
			mode = 2,
			healthLimit = 5,
			priority = 1,
		},
		HighExplosiveGrenade = 
		{
			mode = 2,
			healthLimit = 5,
			priority = 1,
		},
		DaemonicFire = 
		{
			mode = 2,
			healthLimit = 5,
			priority = 1,
		},
		SpineFling = 
		{
			mode = 2,
			healthLimit = 5,
			priority = 1,
		},
		TerrifyingRoar = 
		{
			mode = 2,
			healthLimit = 5,
			priority = 1,
		},
		attackrangecheck = 1,
		autofollow = 1,
		autoattack = 1,
		PoisonedSpine = 
		{
			mode = 2,
			healthLimit = 5,
			priority = 1,
		},
		FangAndClaw = 
		{
			mode = 2,
			healthLimit = 5,
			priority = 1,
		},
		Flamethrower = 
		{
			mode = 2,
			healthLimit = 5,
			priority = 1,
		},
		Maul = 
		{
			mode = 2,
			healthLimit = 5,
			priority = 1,
		},
		FlameOfTzeentch = 
		{
			mode = 2,
			healthLimit = 5,
			priority = 1,
		},
		CoruscatingEnergy = 
		{
			mode = 2,
			healthLimit = 5,
			priority = 1,
		},
		LionsRoar = 
		{
			mode = 2,
			healthLimit = 5,
			priority = 1,
		},
		switchrangecheck = 1,
		version = "2.9.2",
		GoopShootin = 
		{
			mode = 2,
			healthLimit = 5,
			priority = 1,
		},
		defendrangecheck = 1,
		selftargetfollow = 1,
		SquigSqueal = 
		{
			mode = 2,
			healthLimit = 5,
			priority = 1,
		},
		GutRipper = 
		{
			mode = 2,
			healthLimit = 5,
			priority = 1,
		},
		followbuttoncombo = 1,
		Gore = 
		{
			mode = 2,
			healthLimit = 5,
			priority = 1,
		},
		PenetratingRound = 
		{
			mode = 2,
			healthLimit = 5,
			priority = 1,
		},
		SporeCloud = 
		{
			mode = 2,
			healthLimit = 5,
			priority = 1,
		},
		MachineGun = 
		{
			mode = 2,
			healthLimit = 5,
			priority = 1,
		},
		HeadButt = 
		{
			mode = 2,
			healthLimit = 5,
			priority = 1,
		},
		Bite = 
		{
			mode = 2,
			healthLimit = 5,
			priority = 1,
		},
		LegTear = 
		{
			mode = 2,
			healthLimit = 5,
			priority = 1,
		},
		WarpingEnergy = 
		{
			mode = 2,
			healthLimit = 5,
			priority = 1,
		},
	},
}



