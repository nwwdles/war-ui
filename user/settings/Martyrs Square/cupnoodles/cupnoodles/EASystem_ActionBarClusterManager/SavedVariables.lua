ActionBarClusterSettings = 
{
	EA_ActionBar3 = 
	{
		buttonCount = 12,
		scale = 0.93,
		columns = 12,
		buttonYPadding = 5,
		barId = 3,
		selector_prev = 43,
		buttonXPadding = 6,
		buttonYSpacing = 0,
		show = true,
		modificationSettings = 
		{
			true,
			true,
		},
		showEmptySlots = 46,
		caps = false,
		buttonFactory = "ActionButton",
		background = false,
		buttonXSpacing = 0,
		selector = 44,
	},
	EA_ActionBar1 = 
	{
		buttonCount = 12,
		scale = 0.93,
		columns = 12,
		buttonYPadding = 5,
		barId = 1,
		selector_prev = 43,
		buttonXPadding = 6,
		buttonYSpacing = 0,
		show = true,
		modificationSettings = 
		{
			true,
			true,
		},
		showEmptySlots = 45,
		caps = true,
		buttonFactory = "ActionButton",
		background = false,
		buttonXSpacing = 0,
		selector = 44,
	},
	EA_GrantedAbilities = 
	{
		lastSlot = 113,
		scale = 0.75,
		selector_prev = 44,
		buttonYPadding = 0,
		barId = 10,
		modificationSettings = 
		{
			true,
			false,
		},
		buttonXPadding = 0,
		buttonYSpacing = 4,
		show = false,
		buttonXSpacing = 4,
		showEmptySlots = 45,
		caps = false,
		buttonFactory = "ActionButton",
		background = false,
		firstSlot = 108,
		selector = 44,
	},
	EA_ActionBar4 = 
	{
		buttonCount = 12,
		scale = 0.93,
		columns = 12,
		buttonYPadding = 5,
		barId = 4,
		selector_prev = 43,
		buttonXPadding = 6,
		buttonYSpacing = 0,
		show = true,
		modificationSettings = 
		{
			true,
			true,
		},
		showEmptySlots = 46,
		caps = false,
		buttonFactory = "ActionButton",
		background = false,
		buttonXSpacing = 0,
		selector = 44,
	},
	layoutMode = 6,
	EA_StanceBar = 
	{
		lastSlot = 118,
		scale = 0.75,
		selector_prev = 44,
		buttonYPadding = 0,
		barId = 10,
		modificationSettings = 
		{
			false,
			false,
		},
		buttonXPadding = 0,
		buttonYSpacing = 4,
		show = false,
		buttonXSpacing = 4,
		showEmptySlots = 45,
		caps = false,
		buttonFactory = "StanceButton",
		background = false,
		firstSlot = 114,
		selector = 44,
	},
	EA_ActionBar2 = 
	{
		buttonCount = 12,
		scale = 0.93,
		columns = 12,
		buttonYPadding = 5,
		barId = 2,
		selector_prev = 43,
		buttonXPadding = 6,
		buttonYSpacing = 0,
		show = true,
		modificationSettings = 
		{
			true,
			true,
		},
		showEmptySlots = 45,
		caps = false,
		buttonFactory = "ActionButton",
		background = false,
		buttonXSpacing = 0,
		selector = 44,
	},
	EA_ActionBar5 = 
	{
		buttonCount = 12,
		scale = 0.93,
		columns = 12,
		buttonYPadding = 5,
		barId = 5,
		selector_prev = 43,
		buttonXPadding = 6,
		buttonYSpacing = 0,
		show = true,
		modificationSettings = 
		{
			true,
			true,
		},
		showEmptySlots = 46,
		caps = false,
		buttonFactory = "ActionButton",
		background = false,
		buttonXSpacing = 0,
		selector = 44,
	},
	EA_CareerResourceWindowActionBar = 
	{
		lastSlot = 103,
		barId = 9,
		parentWindow = "Root",
		buttonXSpacing = 0,
		scale = 0.75,
		modificationSettings = 
		{
			false,
			false,
		},
		buttonXPadding = 0,
		buttonYSpacing = 0,
		show = false,
		background = false,
		showEmptySlots = 45,
		caps = false,
		buttonFactory = "PetButton",
		buttonYPadding = 0,
		firstSlot = 97,
		selector = 44,
	},
}



ActionBarClusterPositions = 
{
	
	{
		
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		}, 
		ActionBarLockToggler = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topleft",
			RelativePoint = "topright",
			XOffset = 0,
			YOffset = 0,
		},
		[7] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		[16] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottomright",
			XOffset = 28,
			YOffset = 0,
		},
		[8] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottomright",
			XOffset = 94,
			YOffset = 0,
		},
		[17] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		[4] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottomright",
			XOffset = -112,
			YOffset = 0,
		},
		[9] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 9,
		},
		[19] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottomright",
			XOffset = 28,
			YOffset = 0,
		},
		[20] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		EA_ActionBar1LeftCap = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "bottomleft",
			RelativePoint = "bottomright",
			XOffset = 0,
			showing = false,
			scale = 1.0000000302754,
			YOffset = 0,
		},
		[21] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -12,
			YOffset = -13,
		},
		LayerTimerWindow = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = 0,
			showing = true,
			scale = 1.0000000302754,
			YOffset = -80,
		},
		[5] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		[11] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		[23] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		EA_MoraleBar = 
		{
			RelativeTo = "Root",
			Point = "bottomright",
			RelativePoint = "bottomright",
			XOffset = -16,
			showing = true,
			scale = 1.0000000302754,
			YOffset = 0,
		},
		[24] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			showing = true,
			scale = 1.0000000302754,
			YOffset = 0,
		},
		[12] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		EA_GrantedAbilities = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottomleft",
			XOffset = 110,
			showing = true,
			scale = 0.74999998486231,
			YOffset = -12,
		},
		EA_ActionBar1RightCap = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "bottomright",
			RelativePoint = "bottomleft",
			XOffset = 0,
			showing = false,
			scale = 1.0000000302754,
			YOffset = 0,
		},
		[6] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		EA_CareerResourceWindowActionBar = 
		{
			RelativeTo = "EA_CareerResourceWindow",
			Point = "left",
			RelativePoint = "right",
			XOffset = -4,
			YOffset = 10,
		},
		EA_StanceBar = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topleft",
			RelativePoint = "bottomleft",
			XOffset = 0,
			showing = true,
			scale = 0.74999998486231,
			YOffset = -12,
		},
		[22] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		EA_ActionBar1 = 
		{
			RelativeTo = "Root",
			Point = "bottom",
			RelativePoint = "bottom",
			XOffset = 11,
			showing = true,
			scale = 0.93000003269741,
			YOffset = 0,
		},
		EA_TacticsEditor = 
		{
			RelativeTo = "Root",
			Point = "bottomleft",
			RelativePoint = "bottomleft",
			XOffset = 5,
			showing = true,
			scale = 1.0000000302754,
			YOffset = 0,
		},
	},
	
	{
		
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = 0,
		},
		
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = 0,
		}, 
		ActionBarLockToggler = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topleft",
			RelativePoint = "bottomleft",
			XOffset = 0,
			YOffset = 0,
		},
		[7] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottom",
			XOffset = -1,
			YOffset = 0,
		},
		[16] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottomright",
			XOffset = 123,
			YOffset = 0,
		},
		[8] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottomright",
			XOffset = 95,
			YOffset = 0,
		},
		[17] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = 0,
		},
		[4] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottomright",
			XOffset = 135,
			YOffset = 0,
		},
		[9] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = 9,
		},
		[19] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottomright",
			XOffset = 123,
			YOffset = 0,
		},
		LayerTimerWindow = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = -80,
		},
		[20] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = 0,
		},
		EA_ActionBar1LeftCap = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "bottomleft",
			RelativePoint = "bottomright",
			XOffset = 0,
			YOffset = 0,
		},
		[21] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = -13,
		},
		EA_ActionBar1 = 
		{
			RelativeTo = "Root",
			Point = "bottom",
			RelativePoint = "bottomright",
			XOffset = 0,
			YOffset = 0,
		},
		[5] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = 0,
		},
		[11] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = 0,
		},
		[23] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = 0,
		},
		EA_GrantedAbilities = 
		{
			RelativeTo = "EA_ActionBar2",
			Point = "topleft",
			RelativePoint = "bottomleft",
			XOffset = 125,
			YOffset = -12,
		},
		[24] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = 0,
		},
		[12] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = 0,
		},
		EA_CareerResourceWindowActionBar = 
		{
			RelativeTo = "EA_CareerResourceWindow",
			Point = "left",
			RelativePoint = "right",
			XOffset = -2,
			YOffset = 12,
		},
		EA_ActionBar1RightCap = 
		{
			RelativeTo = "EA_ActionBar2",
			Point = "bottomright",
			RelativePoint = "bottomleft",
			XOffset = 0,
			YOffset = 0,
		},
		[6] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = 0,
		},
		EA_StanceBar = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottomleft",
			XOffset = 0,
			YOffset = -12,
		},
		[22] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = 0,
		},
		EA_MoraleBar = 
		{
			RelativeTo = "EA_ActionBar2",
			Point = "topright",
			RelativePoint = "bottomright",
			XOffset = -16,
			YOffset = 0,
		},
		EA_ActionBar2 = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "bottomright",
			RelativePoint = "bottomleft",
			XOffset = 0,
			YOffset = 0,
		},
		EA_TacticsEditor = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topleft",
			RelativePoint = "bottomleft",
			XOffset = 20,
			YOffset = 0,
		},
	},
	
	{
		
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		}, 
		ActionBarLockToggler = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topleft",
			RelativePoint = "topright",
			XOffset = 0,
			YOffset = 0,
		},
		[7] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		[16] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottomright",
			XOffset = 28,
			YOffset = 0,
		},
		[8] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottomright",
			XOffset = 94,
			YOffset = 0,
		},
		[17] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		[4] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottomright",
			XOffset = -112,
			YOffset = 0,
		},
		[9] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 9,
		},
		[19] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottomright",
			XOffset = 28,
			YOffset = 0,
		},
		LayerTimerWindow = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = -80,
		},
		[20] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		EA_ActionBar1LeftCap = 
		{
			RelativeTo = "EA_ActionBar2",
			Point = "bottomleft",
			RelativePoint = "bottomright",
			XOffset = 0,
			YOffset = 0,
		},
		[21] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -12,
			YOffset = -13,
		},
		EA_ActionBar1 = 
		{
			RelativeTo = "EA_ActionBar2",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = 0,
		},
		[5] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		[11] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		[23] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		EA_GrantedAbilities = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottomleft",
			XOffset = 108,
			YOffset = -12,
		},
		[24] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		[12] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		EA_CareerResourceWindowActionBar = 
		{
			RelativeTo = "EA_CareerResourceWindow",
			Point = "left",
			RelativePoint = "right",
			XOffset = -4,
			YOffset = 10,
		},
		EA_ActionBar1RightCap = 
		{
			RelativeTo = "EA_ActionBar2",
			Point = "bottomright",
			RelativePoint = "bottomleft",
			XOffset = 0,
			YOffset = 0,
		},
		[6] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		EA_StanceBar = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topleft",
			RelativePoint = "bottomleft",
			XOffset = 0,
			YOffset = -12,
		},
		[22] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		EA_MoraleBar = 
		{
			RelativeTo = "Root",
			Point = "bottomright",
			RelativePoint = "bottomright",
			XOffset = -16,
			YOffset = 0,
		},
		EA_ActionBar2 = 
		{
			RelativeTo = "Root",
			Point = "bottom",
			RelativePoint = "bottom",
			XOffset = 11,
			YOffset = 0,
		},
		EA_TacticsEditor = 
		{
			RelativeTo = "Root",
			Point = "bottomleft",
			RelativePoint = "bottomleft",
			XOffset = 5,
			YOffset = 0,
		},
	},
	
	{
		
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		}, 
		LayerTimerWindow = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = -80,
		},
		[7] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		[16] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottomright",
			XOffset = 28,
			YOffset = 0,
		},
		[8] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottomright",
			XOffset = 94,
			YOffset = 0,
		},
		[17] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		ActionBarLockToggler = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topleft",
			RelativePoint = "bottomleft",
			XOffset = 0,
			YOffset = 0,
		},
		[4] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottomright",
			XOffset = -112,
			YOffset = 0,
		},
		[9] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 9,
		},
		[19] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottomright",
			XOffset = 28,
			YOffset = 0,
		},
		EA_MoraleBar = 
		{
			RelativeTo = "EA_ActionBar3",
			Point = "topright",
			RelativePoint = "bottomright",
			XOffset = 0,
			YOffset = 0,
		},
		[20] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		EA_ActionBar1LeftCap = 
		{
			RelativeTo = "EA_ActionBar2",
			Point = "bottomleft",
			RelativePoint = "bottomright",
			XOffset = 0,
			YOffset = 0,
		},
		[21] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -12,
			YOffset = -13,
		},
		EA_ActionBar3 = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "bottom",
			RelativePoint = "topleft",
			XOffset = 0,
			YOffset = 0,
		},
		[5] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		[11] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		[23] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		EA_ActionBar2 = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "bottom",
			RelativePoint = "topright",
			XOffset = 0,
			YOffset = 0,
		},
		[24] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		[12] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		EA_CareerResourceWindowActionBar = 
		{
			RelativeTo = "EA_CareerResourceWindow",
			Point = "left",
			RelativePoint = "right",
			XOffset = -4,
			YOffset = 10,
		},
		EA_ActionBar1RightCap = 
		{
			RelativeTo = "EA_ActionBar3",
			Point = "bottomright",
			RelativePoint = "bottomleft",
			XOffset = 0,
			YOffset = 0,
		},
		[6] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		EA_StanceBar = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottomleft",
			XOffset = 0,
			YOffset = -12,
		},
		[22] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		EA_GrantedAbilities = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topleft",
			RelativePoint = "bottomleft",
			XOffset = 20,
			YOffset = 0,
		},
		EA_ActionBar1 = 
		{
			RelativeTo = "Root",
			Point = "bottom",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = -68,
		},
		EA_TacticsEditor = 
		{
			RelativeTo = "EA_ActionBar2",
			Point = "topleft",
			RelativePoint = "bottomleft",
			XOffset = 0,
			YOffset = 0,
		},
	},
	
	{
		
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = 0,
		},
		
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = 0,
		}, 
		ActionBarLockToggler = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topleft",
			RelativePoint = "bottomleft",
			XOffset = 0,
			YOffset = 0,
		},
		[7] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottom",
			XOffset = -1,
			YOffset = 0,
		},
		EA_ActionBar4 = 
		{
			RelativeTo = "EA_ActionBar3",
			Point = "bottomright",
			RelativePoint = "bottomleft",
			XOffset = 0,
			YOffset = 0,
		},
		[16] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottomright",
			XOffset = 123,
			YOffset = 0,
		},
		[8] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottomright",
			XOffset = 95,
			YOffset = 0,
		},
		[17] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = 0,
		},
		LayerTimerWindow = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = -100,
		},
		[4] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottomright",
			XOffset = 112,
			YOffset = 0,
		},
		[9] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = 9,
		},
		[19] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottomright",
			XOffset = 123,
			YOffset = 0,
		},
		EA_ActionBar1 = 
		{
			RelativeTo = "Root",
			Point = "bottom",
			RelativePoint = "bottomright",
			XOffset = 0,
			YOffset = -68,
		},
		[20] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = 0,
		},
		EA_ActionBar1LeftCap = 
		{
			RelativeTo = "EA_ActionBar3",
			Point = "bottomleft",
			RelativePoint = "bottomright",
			XOffset = 0,
			YOffset = 0,
		},
		[21] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = -13,
		},
		EA_ActionBar3 = 
		{
			RelativeTo = "Root",
			Point = "bottom",
			RelativePoint = "bottomright",
			XOffset = 0,
			YOffset = 0,
		},
		[5] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = 0,
		},
		[11] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = 0,
		},
		EA_CareerResourceWindowActionBar = 
		{
			RelativeTo = "EA_CareerResourceWindow",
			Point = "left",
			RelativePoint = "right",
			XOffset = -4,
			YOffset = 10,
		},
		EA_ActionBar2 = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "bottomright",
			RelativePoint = "bottomleft",
			XOffset = 0,
			YOffset = 0,
		},
		[24] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = 0,
		},
		[12] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = 0,
		},
		EA_StanceBar = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottomleft",
			XOffset = 0,
			YOffset = -12,
		},
		EA_ActionBar1RightCap = 
		{
			RelativeTo = "EA_ActionBar4",
			Point = "bottomright",
			RelativePoint = "bottomleft",
			XOffset = 0,
			YOffset = 0,
		},
		[6] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = 0,
		},
		[22] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = 0,
		},
		EA_MoraleBar = 
		{
			RelativeTo = "EA_ActionBar2",
			Point = "topright",
			RelativePoint = "bottomright",
			XOffset = -16,
			YOffset = 0,
		},
		EA_GrantedAbilities = 
		{
			RelativeTo = "EA_ActionBar2",
			Point = "topleft",
			RelativePoint = "bottomleft",
			XOffset = 125,
			YOffset = -12,
		},
		[23] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topright",
			RelativePoint = "bottom",
			XOffset = 0,
			YOffset = 0,
		},
		EA_TacticsEditor = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topleft",
			RelativePoint = "bottomleft",
			XOffset = 20,
			YOffset = 0,
		},
	},
	
	{
		
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			showing = true,
			scale = 1.0000000302754,
			YOffset = 0,
		},
		
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		}, 
		ActionBarLockToggler = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "topleft",
			RelativePoint = "bottomleft",
			XOffset = 0,
			YOffset = 0,
		},
		[7] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		EA_ActionBar4 = 
		{
			RelativeTo = "Root",
			Point = "bottomright",
			RelativePoint = "bottomright",
			XOffset = -0.055134184658527,
			showing = true,
			scale = 0.93000003269741,
			YOffset = -68.608085632324,
		},
		LayerTimerWindow = 
		{
			RelativeTo = "Root",
			Point = "center",
			RelativePoint = "center",
			XOffset = -5.1428589820862,
			showing = true,
			scale = 1,
			YOffset = 239.28573608398,
		},
		[16] = 
		{
			RelativeTo = "Root",
			Point = "right",
			RelativePoint = "right",
			XOffset = -335.19512939453,
			showing = false,
			scale = 1.0000000302754,
			YOffset = 293.28610229492,
		},
		[8] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottomright",
			XOffset = 94,
			YOffset = 0,
		},
		[17] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		EA_ActionBar2 = 
		{
			RelativeTo = "Root",
			Point = "bottomright",
			RelativePoint = "bottomright",
			XOffset = -0.97527307271957,
			showing = true,
			scale = 0.93000003269741,
			YOffset = -206.60806274414,
		},
		[4] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottomright",
			XOffset = -112,
			showing = false,
			scale = 1,
			YOffset = 0,
		},
		[9] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 9,
		},
		[19] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottomright",
			XOffset = 28,
			YOffset = 0,
		},
		EA_ActionBar1 = 
		{
			RelativeTo = "Root",
			Point = "bottomright",
			RelativePoint = "bottomright",
			XOffset = -0.49520733952522,
			showing = true,
			scale = 0.93000003269741,
			YOffset = -275.60809326172,
		},
		[20] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		EA_ActionBar1LeftCap = 
		{
			RelativeTo = "Root",
			Point = "bottomleft",
			RelativePoint = "bottomleft",
			XOffset = 237.64761352539,
			showing = false,
			scale = 1,
			YOffset = 0.003802725346759,
		},
		[21] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -12,
			YOffset = -13,
		},
		EA_ActionBar3 = 
		{
			RelativeTo = "Root",
			Point = "bottomright",
			RelativePoint = "bottomright",
			XOffset = -0.49520733952522,
			showing = true,
			scale = 0.93000003269741,
			YOffset = -137.60810852051,
		},
		[5] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		[11] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		EA_CareerResourceWindowActionBar = 
		{
			RelativeTo = "Root",
			Point = "right",
			RelativePoint = "right",
			XOffset = -451.19519042969,
			showing = false,
			scale = 0.75000006055075,
			YOffset = 303.28604125977,
		},
		EA_ActionBar5 = 
		{
			RelativeTo = "Root",
			Point = "bottomright",
			RelativePoint = "bottomright",
			XOffset = -0.015296636149287,
			showing = true,
			scale = 0.93000003269741,
			YOffset = 1.0466660569364e-006,
		},
		[24] = 
		{
			RelativeTo = "Root",
			Point = "bottom",
			RelativePoint = "bottom",
			XOffset = -15.396944046021,
			showing = true,
			scale = 1.5608449663435,
			YOffset = -1.5342735052109,
		},
		[12] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			showing = true,
			scale = 1,
			YOffset = 0,
		},
		[22] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		EA_ActionBar1RightCap = 
		{
			RelativeTo = "Root",
			Point = "bottomright",
			RelativePoint = "bottomright",
			XOffset = -237.6477355957,
			showing = false,
			scale = 1,
			YOffset = 0.003802725346759,
		},
		[6] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			YOffset = 0,
		},
		EA_GrantedAbilities = 
		{
			RelativeTo = "Root",
			Point = "bottom",
			RelativePoint = "bottom",
			XOffset = 245.85736083984,
			showing = true,
			scale = 0.75,
			YOffset = -101.42846679688,
		},
		EA_MoraleBar = 
		{
			RelativeTo = "Root",
			Point = "bottom",
			RelativePoint = "bottom",
			XOffset = -290.15097045898,
			showing = true,
			scale = 1.1721804482596,
			YOffset = -0.031260397285223,
		},
		[23] = 
		{
			RelativeTo = "EA_ActionBar1",
			Point = "top",
			RelativePoint = "bottom",
			XOffset = -11,
			showing = true,
			scale = 1,
			YOffset = 0,
		},
		EA_StanceBar = 
		{
			RelativeTo = "Root",
			Point = "bottom",
			RelativePoint = "bottom",
			XOffset = 271.35717773438,
			showing = true,
			scale = 0.75,
			YOffset = -50.42850112915,
		},
		EA_TacticsEditor = 
		{
			RelativeTo = "Root",
			Point = "bottom",
			RelativePoint = "bottom",
			XOffset = 196.8571472168,
			showing = true,
			scale = 1,
			YOffset = -0.42853778600693,
		},
	},
}



