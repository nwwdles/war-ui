Enemy.Settings = 
{
	combatLogIgnoreAbilityMinValue = 20000,
	combatLogTargetDefenseShow = 
	{
		true,
		true,
		true,
		true,
		true,
		true,
	},
	prevVersion = 279,
	combatLogTargetDefenseHideTimeout = 10,
	guardDistanceIndicatorAnimation = true,
	groupIconsOtherGroupsAlpha = 1,
	debug = false,
	combatLogIDSEnabled = false,
	unitFramesDirection1 = 4,
	groupIconsPetScale = 1,
	unitFramesMyGroupOnly = false,
	soundOnNewTarget = false,
	guardDistanceIndicatorAlphaNormal = 0.75,
	talismanAlerterColorWarning = 
	{
		255,
		255,
		100,
	},
	talismanAlerterAnimation = true,
	combatLogIDSRowPadding = L"3",
	killSpamKilledByYouSound = 219,
	combatLogTargetDefenseSize = 
	{
		60,
		20,
	},
	guardDistanceIndicatorWarningDistance = 30,
	combatLogTargetDefenseTotalBackgroundAlpha = 0.5,
	groupIconsPetLayer = 0,
	combatLogIDSDisplayTime = 20,
	combatLogIgnoreNpc = false,
	groupIconsEnabled = true,
	groupIconsBGColor = 
	{
		200,
		255,
		0,
	},
	groupIconsOffset = 
	{
		0,
		50,
	},
	combatLogTargetDefenseBackgroundAlpha = 0.5,
	combatLogIDSType = "dps",
	groupIconsPetIconColor = 
	{
		255,
		100,
		200,
	},
	combatLogIDSRowBackgroundAlpha = 0.7,
	clickCastings = 
	{
		[1] = 
		{
			archetypeMatch = 1,
			action = 2,
			mouseButton = 1,
			exceptMe = false,
			archetypes = 
			{
				false,
				false,
				false,
			},
			name = L"LOS alert",
			playerType = 1,
			command = L"/wb {name} is out of LOS",
			isEnabled = false,
			keyModifiers = 
			{
				true,
				false,
				false,
			},
			playerTypeMatch = 1,
		},
	},
	unitFramesDetachMyGroup = false,
	unitFramesHideWhenSolo = false,
	groupIconsBGAlpha = 0.5,
	effectsIndicators = 
	{
		
		{
			archetypes = 
			{
				false,
				false,
				false,
			},
			effectFilters = 
			{
				[1] = 
				{
					descriptionMatch = 2,
					type = "guard",
					durationType = 3,
					hasDurationLimit = false,
					castedByMe = 2,
					filterName = L"MyGuard",
					nameMatch = 1,
					typeMatch = 1,
				},
			},
			scale = 1.5,
			anchorFrom = 5,
			exceptMe = true,
			offsetX = 0,
			icon = "guard",
			canDispell = 1,
			isCircleIcon = false,
			id = "59",
			alpha = 1,
			archetypeMatch = 1,
			color = 
			{
				b = 127,
				g = 243,
				r = 191,
			},
			name = L"My guard",
			isEnabled = true,
			playerType = 3,
			offsetY = 20,
			anchorTo = 5,
			playerTypeMatch = 1,
		},
		
		{
			archetypes = 
			{
				true,
				false,
				false,
			},
			effectFilters = 
			{
				[1] = 
				{
					descriptionMatch = 2,
					type = "guard",
					durationType = 3,
					hasDurationLimit = false,
					castedByMe = 3,
					filterName = L"NotMyGuard",
					nameMatch = 1,
					typeMatch = 1,
				},
			},
			scale = 1.2,
			anchorFrom = 5,
			exceptMe = false,
			offsetX = 0,
			icon = "guard",
			canDispell = 1,
			isCircleIcon = false,
			id = "60",
			alpha = 1,
			archetypeMatch = 2,
			color = 
			{
				b = 255,
				g = 181,
				r = 127,
			},
			name = L"Other guard",
			isEnabled = true,
			playerType = 3,
			offsetY = -10,
			anchorTo = 5,
			playerTypeMatch = 1,
		},
		
		{
			archetypes = 
			{
				false,
				false,
				false,
			},
			effectFilters = 
			{
				[1] = 
				{
					typeMatch = 1,
					castedByMe = 1,
					descriptionMatch = 2,
					hasDurationLimit = false,
					filterName = L"Any",
					nameMatch = 1,
					durationType = 2,
				},
			},
			scale = 1,
			anchorFrom = 5,
			exceptMe = false,
			offsetX = 0,
			icon = "other",
			canDispell = 2,
			isCircleIcon = false,
			id = "61",
			alpha = 1,
			archetypeMatch = 1,
			color = 
			{
				b = 119,
				g = 60,
				r = 255,
			},
			name = L"Any dispellable",
			isEnabled = true,
			playerType = 1,
			offsetY = 0,
			anchorTo = 5,
			playerTypeMatch = 1,
		},
		
		{
			archetypes = 
			{
				false,
				false,
				false,
			},
			anchorTo = 9,
			scale = 1,
			anchorFrom = 9,
			exceptMe = false,
			offsetX = 0,
			customIcon = 22,
			icon = "other",
			canDispell = 1,
			isCircleIcon = false,
			id = "62",
			alpha = 1,
			archetypeMatch = 1,
			color = 
			{
				b = 0,
				g = 191,
				r = 255,
			},
			name = L"HoT",
			isEnabled = true,
			playerType = 1,
			offsetY = 0,
			effectFilters = 
			{
				[1] = 
				{
					descriptionMatch = 2,
					type = "isHealing",
					durationType = 2,
					hasDurationLimit = false,
					castedByMe = 2,
					filterName = L"MyHealing",
					nameMatch = 1,
					typeMatch = 1,
				},
			},
			playerTypeMatch = 1,
		},
		
		{
			archetypes = 
			{
				false,
				false,
				false,
			},
			anchorTo = 9,
			scale = 1,
			anchorFrom = 9,
			exceptMe = true,
			offsetX = -3,
			icon = "dot",
			canDispell = 1,
			isCircleIcon = false,
			id = "63",
			alpha = 1,
			logic = L"MyBuff and not MyHealing",
			archetypeMatch = 1,
			effectFilters = 
			{
				
				{
					descriptionMatch = 2,
					type = "isBuff",
					durationType = 2,
					hasDurationLimit = false,
					castedByMe = 2,
					filterName = L"MyBuff",
					nameMatch = 1,
					typeMatch = 1,
				},
				
				{
					descriptionMatch = 2,
					type = "isHealing",
					durationType = 2,
					hasDurationLimit = false,
					castedByMe = 2,
					filterName = L"MyHealing",
					nameMatch = 1,
					typeMatch = 1,
				},
			},
			name = L"Buff",
			playerType = 1,
			isEnabled = true,
			offsetY = 0,
			color = 
			{
				b = 255,
				g = 200,
				r = 50,
			},
			playerTypeMatch = 1,
		},
		
		{
			archetypes = 
			{
				false,
				false,
				false,
			},
			anchorTo = 9,
			scale = 1,
			anchorFrom = 9,
			exceptMe = false,
			offsetX = -3,
			icon = "dot",
			canDispell = 1,
			isCircleIcon = false,
			id = "64",
			alpha = 1,
			logic = L"MyBlessing and not MyHealing",
			archetypeMatch = 1,
			effectFilters = 
			{
				
				{
					durationType = 2,
					type = "isBlessing",
					filterName = L"MyBlessing",
					descriptionMatch = 2,
					castedByMe = 2,
					durationMax = 59,
					nameMatch = 1,
					hasDurationLimit = true,
					typeMatch = 1,
				},
				
				{
					descriptionMatch = 2,
					type = "isHealing",
					durationType = 2,
					hasDurationLimit = false,
					castedByMe = 2,
					filterName = L"MyHealing",
					nameMatch = 1,
					typeMatch = 1,
				},
			},
			name = L"Blessing",
			playerType = 1,
			isEnabled = true,
			offsetY = 0,
			color = 
			{
				b = 255,
				g = 200,
				r = 50,
			},
			playerTypeMatch = 1,
		},
		
		{
			archetypes = 
			{
				false,
				false,
				true,
			},
			effectFilters = 
			{
				[1] = 
				{
					descriptionMatch = 2,
					type = "healDebuffOut50",
					durationType = 1,
					hasDurationLimit = false,
					castedByMe = 3,
					filterName = L"OutHealDebuff",
					nameMatch = 1,
					typeMatch = 1,
				},
			},
			scale = 0.6,
			anchorFrom = 8,
			exceptMe = false,
			offsetX = -53,
			icon = "heal",
			canDispell = 1,
			isCircleIcon = false,
			id = "65",
			alpha = 1,
			archetypeMatch = 1,
			color = 
			{
				b = 64,
				g = 255,
				r = 191,
			},
			name = L"Outgoing 50% heal debuff",
			isEnabled = true,
			playerType = 1,
			offsetY = -5,
			anchorTo = 8,
			playerTypeMatch = 1,
		},
		
		{
			archetypes = 
			{
				false,
				false,
				false,
			},
			effectFilters = 
			{
				[1] = 
				{
					descriptionMatch = 2,
					type = "healDebuffIn50",
					durationType = 1,
					hasDurationLimit = false,
					castedByMe = 3,
					filterName = L"InHealDebuff",
					nameMatch = 1,
					typeMatch = 1,
				},
			},
			scale = 0.6,
			anchorFrom = 8,
			exceptMe = false,
			offsetX = -40,
			icon = "heal",
			canDispell = 1,
			isCircleIcon = false,
			id = "66",
			alpha = 1,
			archetypeMatch = 1,
			color = 
			{
				b = 64,
				g = 64,
				r = 255,
			},
			name = L"Incomming 50% heal debuff",
			isEnabled = true,
			playerType = 1,
			offsetY = -5,
			anchorTo = 8,
			playerTypeMatch = 1,
		},
		
		{
			archetypes = 
			{
				false,
				false,
				false,
			},
			effectFilters = 
			{
				[1] = 
				{
					descriptionMatch = 2,
					type = "stagger",
					durationType = 2,
					hasDurationLimit = false,
					castedByMe = 1,
					filterName = L"Stagger",
					nameMatch = 1,
					typeMatch = 1,
				},
			},
			scale = 0.75,
			anchorFrom = 8,
			exceptMe = false,
			offsetX = 5,
			icon = "stagger",
			canDispell = 1,
			isCircleIcon = false,
			id = "67",
			alpha = 1,
			archetypeMatch = 1,
			name = L"Stagger",
			color = 
			{
				b = 128,
				g = 255,
				r = 255,
			},
			playerType = 1,
			anchorTo = 5,
			isEnabled = true,
			playerTypeMatch = 1,
		},
		
		{
			archetypes = 
			{
				false,
				false,
				true,
			},
			effectFilters = 
			{
				[1] = 
				{
					typeMatch = 1,
					filterName = L"DoK_WP_Regen",
					abilityIds = L"9561, 8237",
					castedByMe = 1,
					descriptionMatch = 2,
					hasDurationLimit = false,
					abilityIdsHash = 
					{
						[9561] = true,
						[8237] = true,
					},
					nameMatch = 1,
					durationType = 1,
				},
			},
			scale = 0.5,
			anchorFrom = 8,
			exceptMe = false,
			offsetX = -65,
			icon = "disabled",
			canDispell = 1,
			isCircleIcon = false,
			id = "68",
			alpha = 1,
			archetypeMatch = 1,
			color = 
			{
				b = 128,
				g = 64,
				r = 255,
			},
			name = L"DoK/WP regen",
			isEnabled = true,
			playerType = 1,
			offsetY = -5,
			anchorTo = 8,
			playerTypeMatch = 1,
		},
		
		{
			archetypes = 
			{
				false,
				false,
				false,
			},
			effectFilters = 
			{
				[1] = 
				{
					typeMatch = 1,
					filterName = L"ID",
					abilityIds = L"613",
					castedByMe = 1,
					descriptionMatch = 2,
					hasDurationLimit = false,
					abilityIdsHash = 
					{
						[613] = true,
					},
					nameMatch = 1,
					durationType = 2,
				},
			},
			scale = 0.5,
			anchorFrom = 8,
			exceptMe = false,
			offsetX = 5,
			customIcon = 23175,
			icon = "other",
			canDispell = 1,
			isCircleIcon = false,
			id = "69",
			alpha = 1,
			archetypeMatch = 1,
			name = L"Immaculate Defense",
			color = 
			{
				b = 255,
				g = 255,
				r = 255,
			},
			playerType = 3,
			anchorTo = 2,
			isEnabled = true,
			playerTypeMatch = 1,
		},
		
		{
			archetypes = 
			{
				false,
				false,
				true,
			},
			effectFilters = 
			{
				[1] = 
				{
					typeMatch = 1,
					filterName = L"FM",
					abilityIds = L"653, 674, 695, 3882",
					castedByMe = 1,
					descriptionMatch = 2,
					hasDurationLimit = false,
					abilityIdsHash = 
					{
						[3882] = true,
						[653] = true,
						[695] = true,
						[674] = true,
					},
					nameMatch = 1,
					durationType = 2,
				},
			},
			scale = 0.5,
			anchorFrom = 8,
			exceptMe = false,
			offsetX = 5,
			customIcon = 23153,
			icon = "other",
			canDispell = 1,
			isCircleIcon = false,
			id = "70",
			alpha = 1,
			archetypeMatch = 1,
			name = L"Focused Mind",
			color = 
			{
				b = 255,
				g = 255,
				r = 255,
			},
			playerType = 3,
			anchorTo = 2,
			isEnabled = true,
			playerTypeMatch = 1,
		},
		
		{
			archetypes = 
			{
				false,
				false,
				false,
			},
			effectFilters = 
			{
				[1] = 
				{
					typeMatch = 1,
					filterName = L"TODB",
					abilityIds = L"9616",
					castedByMe = 1,
					descriptionMatch = 2,
					hasDurationLimit = false,
					abilityIdsHash = 
					{
						[9616] = true,
					},
					nameMatch = 1,
					durationType = 2,
				},
			},
			scale = 0.5,
			anchorFrom = 8,
			exceptMe = false,
			offsetX = 5,
			customIcon = 10965,
			icon = "other",
			canDispell = 1,
			isCircleIcon = false,
			id = "71",
			alpha = 1,
			archetypeMatch = 1,
			name = L"1001 Dark Blessings",
			color = 
			{
				b = 255,
				g = 255,
				r = 255,
			},
			playerType = 3,
			anchorTo = 2,
			isEnabled = true,
			playerTypeMatch = 1,
		},
		
		{
			archetypes = 
			{
				false,
				false,
				false,
			},
			effectFilters = 
			{
				[1] = 
				{
					typeMatch = 1,
					filterName = L"GOF",
					abilityIds = L"8308",
					castedByMe = 1,
					descriptionMatch = 2,
					hasDurationLimit = false,
					abilityIdsHash = 
					{
						[8308] = true,
					},
					nameMatch = 1,
					durationType = 2,
				},
			},
			scale = 0.5,
			anchorFrom = 8,
			exceptMe = false,
			offsetX = 5,
			customIcon = 8042,
			icon = "other",
			canDispell = 1,
			isCircleIcon = false,
			id = "72",
			alpha = 1,
			archetypeMatch = 1,
			name = L"Gift of Life",
			color = 
			{
				b = 255,
				g = 255,
				r = 255,
			},
			playerType = 3,
			anchorTo = 2,
			isEnabled = true,
			playerTypeMatch = 1,
		},
		
		{
			archetypes = 
			{
				false,
				false,
				false,
			},
			effectFilters = 
			{
				[1] = 
				{
					typeMatch = 1,
					filterName = L"AnyMarkOrRune",
					abilityIds = L"3746, 8551, 8617, 3748, 8560, 8619, 20458, 3747, 8556, 8618, 3038, 3773, 8567, 8620, 1591, 3670, 20476, 1588, 1600, 3570, 1608, 3650, 3671",
					castedByMe = 2,
					descriptionMatch = 2,
					hasDurationLimit = false,
					abilityIdsHash = 
					{
						[8560] = true,
						[1608] = true,
						[3746] = true,
						[3748] = true,
						[20458] = true,
						[8617] = true,
						[3570] = true,
						[8556] = true,
						[1588] = true,
						[20476] = true,
						[3671] = true,
						[1591] = true,
						[1600] = true,
						[3650] = true,
						[3773] = true,
						[8620] = true,
						[3670] = true,
						[3038] = true,
						[8551] = true,
						[3747] = true,
						[8567] = true,
						[8618] = true,
						[8619] = true,
					},
					nameMatch = 1,
					durationType = 1,
				},
			},
			scale = 1,
			anchorFrom = 9,
			exceptMe = false,
			offsetX = -36,
			icon = "dot",
			canDispell = 1,
			isCircleIcon = false,
			id = "73",
			alpha = 1,
			archetypeMatch = 1,
			name = L"My marks/runes",
			color = 
			{
				b = 221,
				g = 255,
				r = 0,
			},
			playerType = 3,
			anchorTo = 9,
			isEnabled = true,
			playerTypeMatch = 1,
		},
		
		{
			archetypes = 
			{
				false,
				false,
				false,
			},
			effectFilters = 
			{
				[1] = 
				{
					durationType = 2,
					filterName = L"WordOfPain",
					abilityIds = L"9475, 20535",
					castedByMe = 1,
					descriptionMatch = 2,
					durationMax = 5,
					hasDurationLimit = true,
					abilityIdsHash = 
					{
						[9475] = true,
						[20535] = true,
					},
					nameMatch = 1,
					typeMatch = 1,
				},
			},
			scale = 0.5,
			anchorFrom = 8,
			exceptMe = false,
			offsetX = 6,
			customIcon = 10908,
			icon = "other",
			canDispell = 1,
			isCircleIcon = false,
			id = "74",
			alpha = 1,
			archetypeMatch = 1,
			name = L"Improved Word of Pain",
			color = 
			{
				b = 255,
				g = 255,
				r = 255,
			},
			playerType = 3,
			anchorTo = 2,
			isEnabled = true,
			playerTypeMatch = 1,
		},
		
		{
			archetypes = 
			{
				false,
				false,
				false,
			},
			effectFilters = 
			{
				[1] = 
				{
					durationType = 2,
					filterName = L"BoilingBlood",
					abilityIds = L"8165",
					castedByMe = 1,
					descriptionMatch = 2,
					durationMax = 5,
					hasDurationLimit = true,
					abilityIdsHash = 
					{
						[8165] = true,
					},
					nameMatch = 1,
					typeMatch = 1,
				},
			},
			scale = 0.5,
			anchorFrom = 8,
			exceptMe = false,
			offsetX = 6,
			customIcon = 8015,
			icon = "other",
			canDispell = 1,
			isCircleIcon = false,
			id = "75",
			alpha = 1,
			archetypeMatch = 1,
			name = L"Improved Boiling Blood",
			color = 
			{
				b = 255,
				g = 255,
				r = 255,
			},
			playerType = 3,
			anchorTo = 2,
			isEnabled = true,
			playerTypeMatch = 1,
		},
	},
	groupIconsPetHPBGColor = 
	{
		100,
		0,
		0,
	},
	unitFramesDirection2 = 4,
	guardMarkTemplate = 
	{
		scale = 0.8,
		unique = false,
		firstLetters = 4,
		showCareerIcon = false,
		canClearOnClick = false,
		permanentTargets = 
		{
		},
		id = 445,
		targetOnClick = true,
		alpha = 1,
		text = L"G",
		color = 
		{
			65,
			150,
			255,
		},
		font = "font_default_text_giant",
		name = L"",
		layer = 3,
		display = 1,
		offsetY = 75,
		neverExpire = true,
		permanent = false,
	},
	groupIconsOtherGroupsHPBGColor = 
	{
		50,
		100,
		100,
	},
	unitFramesSorting = 
	{
		1,
		2,
		3,
	},
	objectWindowsInactiveTimeout = 1200,
	unitFramesGroupsCount1 = 8,
	killSpamEnabled = true,
	groupIconsParts = 
	{
	},
	groupIconsOtherGroupsOffset = 
	{
		0,
		50,
	},
	CombatLogStats = 
	{
	},
	groupIconsOtherGroupsBGColor = 
	{
		200,
		255,
		255,
	},
	unitFramesGroupsPadding2 = 40,
	talismanAlerterEnabled = true,
	groupIconsLayer = 0,
	groupIconsPetHPColor = 
	{
		255,
		225,
		255,
	},
	combatLogIDSTimeframe = 10,
	unitFramesHideMyGroup = false,
	groupIconsHPBGColor = 
	{
		0,
		100,
		0,
	},
	timerEnabled = true,
	killSpamListBottomUp = true,
	groupIconsScale = 1,
	playerKills = 5245,
	combatLogIDSMaxRows = L"5",
	groupIconsPetAlpha = 1,
	combatLogTargetDefenseTimeframe = 7.5,
	combatLogStatsWindowColumns = 
	{
		
		{
			sortDirection = false,
			sortColumn = 2,
			columns = 
			{
				"name",
				"totalPercent",
				"critPercent",
				"maxNormal",
				"maxCrit",
				"mitPercent",
			},
		},
		
		{
			sortDirection = false,
			sortColumn = 2,
			columns = 
			{
				"name",
				"totalPercent",
				"critPercent",
				"maxNormal",
				"maxCrit",
				"mitPercent",
			},
		},
		
		{
			sortDirection = false,
			sortColumn = 2,
			columns = 
			{
				"name",
				"totalPercent",
				"critPercent",
				"maxNormal",
				"maxCrit",
				"overhealPercent",
			},
		},
		
		{
			sortDirection = false,
			sortColumn = 2,
			columns = 
			{
				"name",
				"totalPercent",
				"critPercent",
				"maxNormal",
				"maxCrit",
				"overhealPercent",
			},
		},
	},
	combatLogTargetDefenseEnabled = false,
	markNewTarget = true,
	unitFramesTooltipMode = "always",
	unitFramesGroupsDirection1 = 2,
	combatLogEPSTimeframe = 7.5,
	combatLogEPSMaxValueMinTime = 4.5,
	combatLogTargetDefenseTotalBackground = 
	{
		0,
		0,
		0,
	},
	objectWindowsTimeout = 20,
	combatLogIgnoreWhenPolymorphed = true,
	combatLogStatsWindowType = 1,
	combatLogIDSRowBackground = 
	{
		225,
		50,
		50,
	},
	unitFramesPadding2 = 20,
	combatLogEPSEnabled = 
	{
		false,
		true,
		false,
		false,
	},
	unitFramesLayer = 1,
	groupIconsOtherGroupsScale = 0.6,
	targetShowDelay = 8,
	assistTargetOnNotifyClick = true,
	scenarioInfoReplaceStandardWindow = false,
	unitFramesTestMode = false,
	markTemplates = 
	{
		
		{
			scale = 1,
			firstLetters = 4,
			showCareerIcon = true,
			canClearOnClick = true,
			permanentTargets = 
			{
			},
			id = 1,
			layer = 3,
			alpha = 1,
			text = L"",
			color = 
			{
				191,
				255,
				0,
			},
			font = "font_clear_large_bold",
			name = L"A",
			targetOnClick = true,
			display = 2,
			offsetY = 50,
			neverExpire = false,
			permanent = false,
		},
		
		{
			scale = 0.5,
			unique = false,
			firstLetters = L"4",
			showCareerIcon = true,
			canClearOnClick = true,
			permanentTargets = 
			{
				[L"Erihon"] = false,
				[L"Ocara"] = false,
				[L"Diezhix"] = false,
				[L"Elvasoul"] = false,
				[L"Wody"] = false,
				[L"Diezhixx"] = false,
				[L"Slumberjack"] = false,
				[L"Moreina"] = false,
				[L"Delsun"] = false,
				[L"Ninjagon"] = false,
				[L"Bumba"] = false,
				[L"Snowpearlpbee"] = false,
				[L"Agentorange"] = false,
				[L"Cature"] = false,
				[L"Sixlores"] = false,
				[L"Bladepower"] = false,
				[L"Thorbolt"] = false,
				[L"Bloodtemplar"] = false,
				[L"Elaora"] = false,
			},
			id = 2,
			targetOnClick = true,
			alpha = 1,
			text = L"",
			color = 
			{
				255,
				64,
				255,
			},
			font = "font_clear_large_bold",
			name = L"B",
			layer = 3,
			display = 2,
			offsetY = 50,
			neverExpire = false,
			permanent = true,
		},
		
		{
			scale = 1,
			firstLetters = 4,
			showCareerIcon = false,
			canClearOnClick = true,
			permanentTargets = 
			{
			},
			id = 3,
			layer = 3,
			alpha = 1,
			text = L"G",
			color = 
			{
				65,
				150,
				255,
			},
			font = "font_default_text_giant",
			name = L"G",
			targetOnClick = true,
			display = 1,
			offsetY = 50,
			neverExpire = false,
			permanent = false,
		},
	},
	talismanAlerterDisplayMode = 2,
	unitFramesSize = 
	{
		64,
		64,
	},
	guardDistanceIndicatorScaleWarning = 1.5,
	scenarioInfoPlayers = 
	{},
	timerInactiveColor = 
	{
		255,
		255,
		255,
	},
	unitFramesSortingEnabled = true,
	groupIconsOtherGroupsHPColor = 
	{
		200,
		255,
		255,
	},
	groupIconsPetBGAlpha = 0.5,
	groupIconsPetOffset = 
	{
		0,
		20,
	},
	unitFramesScale = 1,
	stateMachineThrottle = 0.3,
	combatLogTargetDefenseBackground = 
	{
		0,
		0,
		0,
	},
	newTargetMarkTemplate = 
	{
		color = 
		{
			b = 0,
			g = 0,
			r = 255,
		},
		unique = false,
		firstLetters = 4,
		showCareerIcon = true,
		canClearOnClick = true,
		permanentTargets = 
		{
		},
		id = 1,
		text = L"KILL",
		alpha = 1,
		targetOnClick = true,
		layer = 3,
		font = "font_clear_large_bold",
		name = L"",
		scale = 1,
		display = 1,
		offsetY = 50,
		neverExpire = false,
		permanent = false,
	},
	timerActiveColor = 
	{
		255,
		255,
		75,
	},
	scenarioInfoData = 
	{
		maxTimer = 120,
		startingScenario = 0,
		destructionPoints = 101,
		id = 2002,
		queuedWithGroup = false,
		activeQueue = 0,
		timeLeft = 120,
		orderPoints = 500,
		pointMax = 500,
		mode = 2,
	},
	scenarioInfoEnabled = true,
	guardDistanceIndicator = 2,
	guardMarkEnabled = true,
	intercomPrivate = true,
	groupIconsAlpha = 1,
	combatLogEPSShow = 
	{
		false,
		true,
		false,
		false,
	},
	combatLogLogParseErrors = false,
	guardDistanceIndicatorMovable = true,
	version = 279,
	groupIconsTargetOnClick = true,
	guardDistanceIndicatorClickThrough = false,
	chatThrottleDelay = 10,
	playerDeaths = 1955,
	unitFramesEnabled = true,
	killSpamReparseChunkSize = 20,
	groupIconsShowOnMarkedPlayers = false,
	groupIconsHPColor = 
	{
		200,
		255,
		0,
	},
	groupIconsOtherGroupsLayer = 0,
	chatDelay = 2,
	combatLogStatisticsEnabled = true,
	combatLogIDSRowSize = 
	{
		250,
		30,
	},
	guardDistanceIndicatorColorNormal = 
	{
		127,
		181,
		255,
	},
	unitFramesParts = 
	{
		
		{
			type = "selectionFrame",
			id = "46",
			data = 
			{
				offlineHide = true,
				anchorTo = "topleft",
				scale = 1,
				layer = 0,
				anchorFrom = "topleft",
				pos = 
				{
					-1,
					-1,
				},
				vertical = false,
				alpha = 0.8,
				deadHide = false,
				color = 
				{
					255,
					255,
					255,
				},
				distHide = false,
				size = 
				{
					66,
					66,
				},
				texture = "plain",
			},
			exceptMe = false,
			name = L"Selection",
			playerType = 1,
			isEnabled = true,
			archetypeMatch = 1,
			archetypes = 
			{
				false,
				false,
				false,
			},
			playerTypeMatch = 1,
		},
		
		{
			type = "panel",
			id = "47",
			data = 
			{
				offlineColor = 
				{
					150,
					150,
					150,
				},
				offlineHide = false,
				anchorTo = "topleft",
				deadColor = 
				{
					200,
					50,
					50,
				},
				layer = 0,
				anchorFrom = "topleft",
				pos = 
				{
					2,
					2,
				},
				alpha = 0.75,
				vertical = false,
				color = 
				{
					0,
					0,
					0,
				},
				deadHide = false,
				scale = 1,
				distHide = false,
				size = 
				{
					60,
					60,
				},
				texture = "default2",
			},
			exceptMe = false,
			name = L"Background",
			playerType = 1,
			isEnabled = true,
			archetypeMatch = 1,
			archetypes = 
			{
				false,
				false,
				false,
			},
			playerTypeMatch = 1,
		},
		
		{
			type = "hpacbar",
			id = "48",
			data = 
			{
				color18 = 
				{
					255,
					255,
					255,
				},
				color24 = 
				{
					255,
					255,
					255,
				},
				color6 = 
				{
					255,
					255,
					255,
				},
				color23 = 
				{
					255,
					255,
					255,
				},
				anchorFrom = "bottomleft",
				color12 = 
				{
					255,
					255,
					255,
				},
				color11 = 
				{
					255,
					255,
					255,
				},
				distHide = false,
				size = 
				{
					60,
					60,
				},
				healColor = 
				{
					10,
					200,
					0,
				},
				color14 = 
				{
					255,
					255,
					255,
				},
				wrap = false,
				color2 = 
				{
					255,
					255,
					255,
				},
				deadHide = true,
				color9 = 
				{
					255,
					255,
					255,
				},
				pos = 
				{
					2,
					-2,
				},
				anchorTo = "bottomleft",
				color10 = 
				{
					255,
					255,
					255,
				},
				color20 = 
				{
					255,
					255,
					255,
				},
				color5 = 
				{
					255,
					255,
					255,
				},
				vertical = true,
				color4 = 
				{
					255,
					255,
					255,
				},
				color15 = 
				{
					255,
					255,
					255,
				},
				color16 = 
				{
					255,
					255,
					255,
				},
				color8 = 
				{
					255,
					255,
					255,
				},
				color = 
				{
					0,
					200,
					0,
				},
				color7 = 
				{
					255,
					255,
					255,
				},
				color22 = 
				{
					255,
					255,
					255,
				},
				offlineHide = true,
				color19 = 
				{
					255,
					255,
					255,
				},
				distAlpha = 0.5,
				color17 = 
				{
					255,
					255,
					255,
				},
				layer = 1,
				alpha = 1,
				color1 = 
				{
					255,
					255,
					255,
				},
				dpsColor = 
				{
					200,
					200,
					20,
				},
				scale = 1,
				color3 = 
				{
					255,
					255,
					255,
				},
				textureFullResize = false,
				texture = "default",
				tankColor = 
				{
					0,
					200,
					200,
				},
				color13 = 
				{
					255,
					255,
					255,
				},
				color21 = 
				{
					255,
					255,
					255,
				},
			},
			exceptMe = false,
			name = L"HP",
			playerType = 3,
			isEnabled = true,
			archetypeMatch = 1,
			archetypes = 
			{
				false,
				false,
				false,
			},
			playerTypeMatch = 1,
		},
		
		{
			type = "hpacbar",
			id = "49",
			data = 
			{
				anchorTo = "bottomleft",
				scale = 1,
				anchorFrom = "bottomleft",
				vertical = true,
				distHide = false,
				texture = "default",
				healColor = 
				{
					0,
					200,
					100,
				},
				tankColor = 
				{
					100,
					200,
					200,
				},
				distAlpha = 0.5,
				textureFullResize = false,
				wrap = false,
				alpha = 1,
				deadHide = true,
				dpsColor = 
				{
					200,
					200,
					100,
				},
				color = 
				{
					0,
					200,
					200,
				},
				size = 
				{
					60,
					60,
				},
				offlineHide = true,
				layer = 1,
				pos = 
				{
					2,
					-2,
				},
			},
			exceptMe = false,
			name = L"HP (other groups)",
			playerType = 3,
			isEnabled = true,
			archetypeMatch = 1,
			archetypes = 
			{
				false,
				false,
				false,
			},
			playerTypeMatch = 2,
		},
		
		{
			type = "apbar",
			id = "50",
			data = 
			{
				anchorTo = "bottomleft",
				scale = 1,
				anchorFrom = "bottomleft",
				vertical = false,
				distHide = false,
				texture = "plain",
				offlineHide = true,
				distAlpha = 0.5,
				textureFullResize = true,
				wrap = false,
				alpha = 1,
				maxLength = 10,
				layer = 2,
				deadHide = true,
				font = "font_clear_small_bold",
				align = "bottomleft",
				distColor = 
				{
					190,
					225,
					255,
				},
				size = 
				{
					60,
					3,
				},
				color = 
				{
					255,
					220,
					100,
				},
				pos = 
				{
					2,
					-8,
				},
			},
			exceptMe = false,
			name = L"AP",
			playerType = 3,
			isEnabled = true,
			archetypeMatch = 1,
			archetypes = 
			{
				false,
				false,
				false,
			},
			playerTypeMatch = 1,
		},
		
		{
			type = "careerIcon",
			id = "51",
			data = 
			{
				offlineHide = false,
				pos = 
				{
					0,
					0,
				},
				anchorTo = "center",
				scale = 1,
				layer = 3,
				alpha = 0.7,
				align = "center",
				anchorFrom = "center",
				font = "font_clear_small_bold",
				color = 
				{
					255,
					255,
					255,
				},
				vertical = false,
				deadHide = false,
				distHide = false,
				size = 
				{
					32,
					32,
				},
				texture = "aluminium",
			},
			exceptMe = false,
			name = L"Icon",
			playerType = 1,
			isEnabled = true,
			archetypeMatch = 1,
			archetypes = 
			{
				false,
				false,
				false,
			},
			playerTypeMatch = 1,
		},
		
		{
			type = "nameText",
			id = "52",
			data = 
			{
				anchorTo = "center",
				scale = 0.85,
				anchorFrom = "center",
				vertical = false,
				distHide = false,
				size = 
				{
					64,
					30,
				},
				distColor = 
				{
					190,
					225,
					255,
				},
				distAlpha = 0.5,
				align = "center",
				layer = 3,
				alpha = 1,
				maxLength = 10,
				deadHide = false,
				font = "font_clear_small_bold",
				color = 
				{
					255,
					255,
					255,
				},
				wrap = false,
				offlineHide = false,
				texture = "aluminium",
				pos = 
				{
					0,
					0,
				},
			},
			exceptMe = false,
			name = L"Name",
			playerType = 1,
			isEnabled = true,
			archetypeMatch = 1,
			archetypes = 
			{
				false,
				false,
				false,
			},
			playerTypeMatch = 1,
		},
		
		{
			type = "levelText",
			id = "53",
			data = 
			{
				offlineHide = false,
				pos = 
				{
					0,
					10,
				},
				anchorTo = "center",
				scale = 0.8,
				layer = 3,
				anchorFrom = "center",
				color = 
				{
					255,
					255,
					255,
				},
				alpha = 1,
				vertical = false,
				align = "center",
				font = "font_clear_small_bold",
				deadHide = false,
				distHide = false,
				size = 
				{
					30,
					20,
				},
				texture = "aluminium",
			},
			exceptMe = false,
			name = L"Level",
			playerType = 1,
			isEnabled = true,
			archetypeMatch = 1,
			archetypes = 
			{
				false,
				false,
				false,
			},
			playerTypeMatch = 1,
		},
		
		{
			type = "groupLeaderIcon",
			id = "54",
			data = 
			{
				pos = 
				{
					-7,
					-7,
				},
				offlineHide = false,
				textureFullResize = false,
				anchorTo = "topleft",
				color = 
				{
					255,
					220,
					0,
				},
				layer = 2,
				alpha = 1,
				anchorFrom = "topleft",
				deadHide = false,
				vertical = false,
				prefix = L"M ",
				suffix = L"",
				scale = 1.4,
				distHide = false,
				size = 
				{
					16,
					16,
				},
				texture = "star",
			},
			exceptMe = false,
			name = L"Leader",
			playerType = 1,
			isEnabled = true,
			archetypeMatch = 1,
			archetypes = 
			{
				false,
				false,
				false,
			},
			playerTypeMatch = 1,
		},
		
		{
			type = "moraleBar",
			id = "55",
			data = 
			{
				anchorTo = "bottomleft",
				scale = 1,
				farValue = 151,
				anchorFrom = "bottomleft",
				vertical = true,
				prefix = L"M ",
				suffix = L"",
				distHide = false,
				size = 
				{
					10,
					38,
				},
				offlineHide = true,
				textureFullResize = false,
				layer = 3,
				alpha = 1,
				deadHide = true,
				font = "font_clear_small_bold",
				align = "right",
				farText = L"FAR",
				texture = "4dots_vertical",
				color = 
				{
					120,
					200,
					255,
				},
				pos = 
				{
					4,
					-12,
				},
			},
			exceptMe = false,
			name = L"Morale",
			playerType = 3,
			isEnabled = true,
			archetypeMatch = 1,
			archetypes = 
			{
				false,
				false,
				false,
			},
			playerTypeMatch = 1,
		},
		
		{
			type = "distanceText",
			id = "56",
			data = 
			{
				anchorTo = "topright",
				scale = 0.9,
				farValue = 151,
				anchorFrom = "topright",
				vertical = false,
				prefix = L"",
				suffix = L"",
				distHide = false,
				texture = "aluminium",
				size = 
				{
					30,
					30,
				},
				level2 = 100,
				offlineHide = true,
				level1 = 65,
				textureFullResize = false,
				align = "right",
				layer = 2,
				alpha = 1,
				color1 = 
				{
					255,
					150,
					50,
				},
				color2 = 
				{
					255,
					50,
					50,
				},
				deadHide = true,
				color3 = 
				{
					255,
					50,
					50,
				},
				font = "font_clear_small_bold",
				level3 = 150,
				color = 
				{
					255,
					255,
					255,
				},
				farText = L"FAR",
				pos = 
				{
					-3,
					3,
				},
			},
			exceptMe = true,
			name = L"Distance",
			playerType = 1,
			isEnabled = false,
			archetypeMatch = 1,
			archetypes = 
			{
				false,
				false,
				false,
			},
			playerTypeMatch = 1,
		},
		
		{
			type = "distanceBar",
			id = "57",
			data = 
			{
				anchorTo = "topright",
				color = 
				{
					190,
					255,
					50,
				},
				level1 = 65,
				anchorFrom = "topright",
				level2 = 100,
				prefix = L"",
				suffix = L"%",
				distHide = false,
				texture = "4dots",
				offlineHide = true,
				size = 
				{
					40,
					10,
				},
				deadHide = true,
				textureFullResize = false,
				layer = 2,
				alpha = 1,
				color1 = 
				{
					255,
					150,
					50,
				},
				color2 = 
				{
					255,
					50,
					50,
				},
				font = "font_clear_small_bold",
				color3 = 
				{
					255,
					50,
					50,
				},
				scale = 1,
				level3 = 150,
				vertical = false,
				align = "rightcenter",
				pos = 
				{
					-3,
					4,
				},
			},
			exceptMe = false,
			name = L"Distance bar",
			playerType = 1,
			isEnabled = true,
			archetypeMatch = 1,
			archetypes = 
			{
				false,
				false,
				false,
			},
			playerTypeMatch = 1,
		},
		
		{
			type = "hppText",
			id = "58",
			data = 
			{
				anchorTo = "right",
				color = 
				{
					255,
					255,
					255,
				},
				anchorFrom = "right",
				vertical = false,
				prefix = L"",
				suffix = L"%",
				distHide = false,
				size = 
				{
					50,
					30,
				},
				offlineHide = true,
				textureFullResize = false,
				layer = 2,
				alpha = 1,
				deadHide = true,
				scale = 0.7,
				texture = "3dots",
				align = "rightcenter",
				font = "font_clear_small_bold",
				pos = 
				{
					-3,
					1,
				},
			},
			exceptMe = false,
			name = L"HP %",
			playerType = 1,
			isEnabled = false,
			archetypeMatch = 1,
			archetypes = 
			{
				false,
				false,
				false,
			},
			playerTypeMatch = 1,
		},
	},
	unitFramesPadding1 = 0,
	guardEnabled = true,
	guardDistanceIndicatorColorWarning = 
	{
		255,
		50,
		50,
	},
	groupIconsOtherGroupsBGAlpha = 0.5,
	playerKDRDisplayMode = 5,
	guardDistanceIndicatorAlphaWarning = 1,
	groupIconsHideOnSelf = true,
	scenarioAlerterEnabled = true,
	combatLogTargetDefenseTotalCalculate = 
	{
		true,
		true,
		true,
		true,
		true,
		true,
	},
	unitFramesCount1 = 6,
	guardDistanceIndicatorScaleNormal = 1,
	combatLogIDSRowScale = 1,
	combatLogTargetDefenseTotalEnabled = false,
	groupIconsShowOtherGroups = true,
	groupIconsPetBGColor = 
	{
		255,
		225,
		255,
	},
	unitFramesGroupsPadding1 = 16,
	scenarioInfoSelection = 
	{
		
		{
			sortColumn = "value1",
			columns = 
			{
				"db",
				"deaths",
			},
			sortDirection = false,
			id2 = "1",
			id = "All",
		},
		
		{
			sortColumn = "value1",
			columns = 
			{
				"db",
				"deaths",
			},
			sortDirection = false,
			id2 = "2",
			id = "All",
		},
	},
	unitFramesIsVertical = false,
	soundOnNewTargetId = 500,
	unitFramesMyGroupFirst = true,
	combatLogTargetDefenseScale = 1,
	unitFramesGroupsDirection2 = 4,
	combatLogEnabled = true,
}



