MM_Settings = 
{
	WHMapFilters = 
	{
		true,
		true,
		true,
		true,
		true,
		true,
	},
	CycleSetting = 0,
	LastUserType = 1,
	ZoneData_VERSION = 1,
	FlashColor = 
	{
		r = 226,
		g = 216,
		b = 45,
	},
	MonsterFilters = 
	{
	},
	Filters = 
	{
		["WARCommander:attack"] = true,
		["MapMonster:WPGreen"] = true,
		["WARCommander:target"] = true,
		["WARCommander:regroup"] = true,
		["WARCommander:group4"] = true,
		["WARCommander:position"] = true,
		["MapMonster:WPPurple"] = true,
		["WARCommander:other"] = true,
		["WARCommander:killtarget"] = true,
		WARCommander = true,
		["WARCommander:ress"] = true,
		["WARCommander:targetwb"] = true,
		["WARCommander:group3"] = true,
		MapMonster = true,
		["WARCommander:group2"] = true,
		["WARCommander:group1"] = true,
		["MapMonster:WPGold"] = true,
		MapMonsterCharacter = true,
		["WARCommander:targetgroup"] = true,
		["WARCommander:WARCommander"] = true,
		["MapMonster:WPRed"] = true,
		["MapMonster:WPBlue"] = true,
		["MapMonster:MapMonster"] = true,
	},
}



