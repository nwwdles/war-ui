CDownVar = 
{
	color = 1,
	CP = 
	{
		255,
		210,
		0,
	},
	optCDs = 
	{
		glass = false,
		rowcount = 4,
		hpte = true,
		CDsbelow = 1,
		back = false,
		nfont = 2,
		growleft = 1,
		showborder = true,
		CDRowStride = 2,
		bend = true,
		horizontal = 1,
		width = 170,
		bar_maxCDCount = 6,
		CDorder = 1,
		fade_start = 0,
		tfont = 3,
		name = false,
		maxCDCount = 24,
		growup = 1,
		bar = false,
	},
	minCD = 1,
	refresh = 2,
	maxCD = 6,
	version = 0.94,
}



