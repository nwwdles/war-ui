PotionBar.Settings = 
{
	ShowStackSizeEvenIf1 = true,
	Buttons = 
	{
		
		{
			Use = true,
			Type = 1,
			Preference = 4,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 2,
			Preference = 4,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 3,
			Preference = 4,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 4,
			Preference = 4,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 5,
			Preference = 4,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 6,
			Preference = 4,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 7,
			Preference = 4,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 8,
			Preference = 4,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 9,
			Preference = 4,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 10,
			Preference = 4,
			SplitName = true,
		},
		
		{
			Use = true,
			Type = 11,
			Preference = 4,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 12,
			Preference = 4,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 13,
			Preference = 4,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 14,
			Preference = 4,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 15,
			Preference = 4,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 16,
			Preference = 4,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 17,
			Preference = 4,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 18,
			Preference = 4,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 19,
			Preference = 4,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 20,
			Preference = 4,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 21,
			Preference = 2,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 22,
			Preference = 2,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 23,
			Preference = 2,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 24,
			Preference = 2,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 25,
			Preference = 2,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 26,
			Preference = 2,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 27,
			Preference = 2,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 28,
			Preference = 2,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 29,
			Preference = 2,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 30,
			Preference = 2,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 31,
			Preference = 2,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 32,
			Preference = 2,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 33,
			Preference = 2,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 34,
			Preference = 2,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 35,
			Preference = 2,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 36,
			Preference = 2,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 37,
			Preference = 2,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 38,
			Preference = 2,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 39,
			Preference = 2,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 40,
			Preference = 2,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 41,
			Preference = 2,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 42,
			Preference = 2,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 43,
			Preference = 2,
			SplitName = false,
		},
		
		{
			Use = true,
			Type = 44,
			Preference = 2,
			SplitName = true,
		},
		
		{
			Use = false,
			Type = 45,
			Preference = 4,
			SplitName = true,
		},
	},
	Scale = 0.5,
	Opacity = 1,
	Version = 7.2,
	BottomRight = 1,
	TopRight = 3,
	lastAnchor = 
	{
		y = 97.043918870123,
		x = 817.58563334533,
	},
	Activator = 1,
	ShowStackSizeEvenIfLast = true,
	Autohide = false,
	Build = 1,
}



