WARCommander_config = 
{
	briefpoiname = false,
	briefzonename = false,
	LONG_DURATION = 300,
	alertchannel = "channel2",
	customCommands = 
	{
		context = 
		{
		},
		alert = 
		{
		},
	},
	hide = true,
	scalertchannel = "sc",
	hideInfo = true,
	listening = 
	{
		[12] = 1,
		[0] = 1,
		[14] = 1,
		[57] = 1,
		[15] = 1,
		[8] = 1,
		[4] = 1,
		[58] = 1,
		[16] = 1,
	},
	chatColours = false,
	hideInfoTitle = true,
	state = "on",
	setWaypoints = true,
	version = 0.1,
	channel = "wb",
	scchannel = "sp",
	longpoiname = true,
	delay = 2,
	SHORT_DURATION = 180,
}



WARCommander_pins = 
{
	length = 0,
}



PointsOfInterest_custompoints = 
{
}



