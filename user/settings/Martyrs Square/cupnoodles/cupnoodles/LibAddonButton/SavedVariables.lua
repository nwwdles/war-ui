LibAddonButton.Settings = 
{
	Buttons = 
	{
		fx = 
		{
			Y = 37,
			X = 0,
			Scale = 1.2935997247696,
			Alpha = 1,
			IsShowing = true,
		},
		[".Created.1"] = 
		{
			Y = 845.95926339286,
			X = 1466.367280506,
			Scale = 0.91875004768372,
			Alpha = 1,
			IsShowing = true,
		},
	},
	Version = 2,
	CreatedButtons = 
	{
		[".Created.1"] = 
		{
			Icons = 
			{
				Default = "LibAddonButton_Default",
				Current = "LibAddonButton_Default",
				Highlight = 
				{
					Frames = 
					{
						"LibAddonButton_Gear1",
						"LibAddonButton_Gear2",
						"LibAddonButton_Gear3",
						"LibAddonButton_Gear4",
						"LibAddonButton_Gear5",
						"LibAddonButton_Gear6",
					},
					FPS = 10,
				},
				Pressed = "LibAddonButton_Pressed",
			},
			Menu = 
			{
				AutoSort = true,
				Subitems = 
				{
					[1] = 
					{
						Created = true,
						Text = L"debug menu",
						Macro = L"/d",
					},
				},
			},
			Size = 
			{
				Height = 40,
				Width = 40,
			},
		},
	},
}



