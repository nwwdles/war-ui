BuffHead.Settings = 
{
	Fading = 
	{
		Enable = false,
	},
	Trackers = 
	{
		Group = 
		{
			Enabled = true,
			Debuffs = 2,
			AlwaysShow = 
			{
			},
			Buffs = 2,
			AlwaysIgnore = 
			{
				[3234] = true,
				[3236] = true,
				[3238] = true,
				[402] = true,
				[3688] = true,
				[403] = true,
				[3692] = true,
				[408] = true,
				Unstoppable = true,
				[3233] = true,
				[3235] = true,
				[3237] = true,
				[412] = true,
				[405] = true,
				[413] = true,
				[407] = true,
				immovable = true,
				[406] = true,
				[414] = true,
				[3687] = true,
				[3689] = true,
				[3691] = true,
				[3693] = true,
				[404] = true,
				[3232] = true,
				[3690] = true,
			},
			Permanent = false,
		},
		Friendly = 
		{
			Enabled = true,
			Debuffs = 2,
			OnTargetChange = 
			{
				ClearBuffs = true,
				ClearAlwaysShow = false,
				ClearDebuffs = true,
			},
			AlwaysShow = 
			{
				[245] = true,
			},
			Buffs = 2,
			AlwaysIgnore = 
			{
				[3234] = true,
				[3236] = true,
				[3238] = true,
				[402] = true,
				[408] = true,
				[3688] = true,
				[3690] = true,
				[3692] = true,
				Unstoppable = true,
				[3235] = true,
				[3233] = true,
				[412] = true,
				[3237] = true,
				[3239] = false,
				[405] = true,
				[413] = true,
				[407] = true,
				immovable = true,
				[406] = true,
				[414] = true,
				[3687] = true,
				[3689] = true,
				[3691] = true,
				[3693] = true,
				[404] = true,
				[3232] = true,
				[403] = true,
			},
			Permanent = false,
		},
		Hostile = 
		{
			Enabled = true,
			Debuffs = 2,
			OnTargetChange = 
			{
				ClearBuffs = false,
				ClearAlwaysShow = false,
				ClearDebuffs = false,
			},
			AlwaysShow = 
			{
				[1928] = true,
				[8454] = true,
				[1674] = true,
				[1420] = true,
				[3982] = true,
				[1676] = true,
				[3218] = true,
				[1932] = true,
				[9249] = true,
				[649] = true,
				[9253] = true,
				[8492] = true,
				["9459    "] = true,
				[9008] = true,
				[1363] = true,
				[9544] = true,
				[652] = true,
				[1493] = true,
				[9058] = true,
				[1495] = true,
				[9325] = true,
				[14427] = true,
				["Oath Rune of Sanctuary"] = true,
				[3368] = true,
				[8576] = true,
				[1434] = true,
				[9345] = true,
				[9347] = true,
				[1499] = true,
				[9357] = true,
				[1691] = true,
				[9616] = true,
				[14975] = true,
				[1501] = true,
				[8606] = true,
				[9373] = true,
				[1757] = true,
				[9395] = true,
				[9397] = true,
				[1633] = true,
				[9407] = true,
				[1953] = true,
				[8401] = true,
				[8403] = true,
				[8043] = true,
				[1445] = true,
				[8419] = true,
				[8421] = true,
				[9192] = true,
				[1575] = true,
				[9198] = true,
				[9459] = true,
				[758] = true,
				[8449] = true,
				[631] = true,
				[9475] = true,
				[950] = true,
				[3915] = true,
				[1388] = true,
				[8075] = true,
				[696] = true,
				[3027] = true,
				[3028] = true,
				[3539] = true,
				[3630] = true,
				[3540] = true,
				[1756] = true,
				[245] = true,
				[3671] = true,
				[3032] = true,
				["Resolute Defense"] = true,
				[3033] = true,
				[8092] = true,
				[1773] = true,
				[9272] = true,
				[8095] = true,
				[1418] = true,
				[3029] = true,
				[9090] = true,
				[9537] = true,
				["33539    "] = true,
				[3038] = true,
				[9543] = true,
				["9382    "] = true,
				[8109] = true,
				[9189] = true,
				[1970] = true,
				[1524] = true,
				[8108] = true,
				[9302] = true,
				[8541] = true,
				[8111] = true,
				[8112] = true,
				[8099] = true,
				[1972] = true,
				[8418] = true,
				["Embrace the Warp"] = true,
				[3075] = true,
				[8308] = true,
				[1527] = true,
				[8304] = true,
				[3303] = true,
				[1464] = true,
				[9075] = true,
				[1424] = true,
				[9173] = true,
				[3178] = true,
				[606] = true,
				[1380] = true,
				[1419] = true,
				[3773] = true,
				[9346] = true,
				[1644] = true,
				[695] = true,
				[9602] = true,
				["Grimnir's Shield"] = true,
				[1850] = true,
				["3539    "] = true,
				[608] = true,
				[8013] = true,
				[9109] = true,
				[3569] = true,
				[8607] = true,
				[8605] = true,
				[1852] = true,
				[928] = true,
				[3443] = true,
				[8613] = true,
				[1853] = true,
				[9382] = true,
				[9224] = true,
				[9386] = true,
				[8455] = true,
				[8153] = true,
				[610] = true,
				[674] = true,
				[9396] = true,
				[8611] = true,
				[1521] = true,
				[673] = true,
				[8148] = true,
				[1410] = true,
				[611] = true,
				[7883] = true,
				["Mark of Remaking"] = true,
				[8165] = true,
				[8034] = true,
				[1444] = true,
				[9420] = true,
				[3220] = true,
				[9424] = true,
				[9426] = true,
				[8408] = true,
				[8325] = true,
				[5879] = true,
				["Dat Tickles!"] = true,
				[1459] = true,
				["Guardian Of Light"] = true,
				[613] = true,
				["Drop Da Basha"] = true,
				[8424] = true,
				[9191] = true,
				[8069] = true,
				[1905] = true,
				[8094] = true,
				["Focused Mind"] = true,
				[7244] = true,
				[1799] = true,
				[1608] = true,
				[1746] = true,
				[3650] = true,
			},
			Buffs = 1,
			AlwaysIgnore = 
			{
				[3690] = true,
				[3233] = true,
				[3691] = true,
				[3234] = true,
				[3692] = true,
				[3235] = true,
				[3693] = true,
				[3236] = true,
				[3894] = true,
				[3687] = true,
				[3237] = true,
				[3238] = true,
				[3688] = true,
				[3689] = true,
				[3232] = true,
				[3375] = true,
			},
			Permanent = false,
		},
		Self = 
		{
			Enabled = true,
			Debuffs = 3,
			AlwaysShow = 
			{
				[9466] = true,
				[1928] = true,
				[9474] = true,
				[1674] = true,
				[3471] = true,
				[3982] = true,
				[1676] = true,
				[3218] = true,
				[9500] = true,
				[1932] = true,
				[3220] = true,
				[9253] = true,
				["9459    "] = true,
				[1679] = true,
				[9265] = true,
				[3863] = true,
				[1363] = true,
				[9544] = true,
				["Unholy Empowerment"] = true,
				[9325] = true,
				[14427] = true,
				["8477    "] = true,
				[9333] = true,
				[1434] = true,
				[1753] = true,
				[9602] = true,
				[9606] = true,
				[1499] = true,
				[9616] = true,
				[14975] = true,
				[1501] = true,
				[8606] = true,
				[8021] = true,
				[1441] = true,
				[1633] = true,
				[9407] = true,
				["Cannon Smash"] = true,
				[1953] = true,
				[8401] = true,
				[1827] = true,
				[245] = true,
				["1576  "] = true,
				[9192] = true,
				[1575] = true,
				[9459] = true,
				[9475] = true,
				[950] = true,
				[3915] = true,
				[1516] = true,
				[1644] = true,
				[760] = true,
				["Divine Protection"] = false,
				[3028] = true,
				[1773] = true,
				[3540] = true,
				[9256] = true,
				[8088] = true,
				[9013] = true,
				[9272] = true,
				[1521] = true,
				[3929] = true,
				[9537] = true,
				[1905] = true,
				[9543] = true,
				[1970] = true,
				[9555] = true,
				["Drop Da Basha"] = true,
				[8112] = true,
				[1972] = true,
				[8304] = true,
				[3303] = true,
				[8308] = true,
				[9075] = true,
				[9332] = true,
				[1911] = true,
				[3178] = true,
				[9089] = true,
				[1850] = true,
				[9617] = true,
				[9109] = true,
				[8120] = false,
				[9008] = true,
				[3569] = true,
				[8607] = true,
				[9076] = true,
				[8611] = true,
				[8613] = true,
				[1853] = true,
				[3368] = true,
				[8148] = true,
				[8621] = true,
				[1595] = true,
				[1918] = true,
				[610] = true,
				[9392] = true,
				[8069] = true,
				[8333] = true,
				[8431] = true,
				[8075] = true,
				[8013] = true,
				["1595    "] = true,
				[8162] = true,
				[1846] = true,
				[1368] = true,
				[3075] = true,
				[1576] = true,
				[8153] = true,
				[9424] = true,
				[8402] = true,
				[9169] = true,
				[696] = true,
				[8245] = true,
				[8477] = true,
				[3712] = true,
				["Dat Tickles!"] = true,
				[3027] = true,
				["Guardian Of Light"] = true,
				[613] = true,
				[8422] = true,
				[8424] = true,
				[9191] = true,
				[8325] = true,
				[1746] = true,
				[928] = true,
				[1424] = true,
				[3539] = true,
				[1799] = true,
				[1418] = true,
				[3029] = true,
				[1756] = true,
			},
			Buffs = 2,
			AlwaysIgnore = 
			{
				[3234] = true,
				[3236] = true,
				[3238] = true,
				[8120] = true,
				[3374] = true,
				[3688] = true,
				[3690] = true,
				[3692] = true,
				[3233] = true,
				[3235] = true,
				[3237] = true,
				[3148] = true,
				[3894] = true,
				[3782] = true,
				[3687] = true,
				[3689] = true,
				[3691] = true,
				[3693] = true,
				[3705] = true,
				[3232] = true,
				[3832] = true,
			},
			Permanent = false,
		},
	},
	Indicators = 
	{
		Padding = 
		{
			Y = 5,
			X = 0,
		},
		Compression = 3,
	},
	Scale = 0.87,
	PriorityEffects = 
	{
		SortToFront = true,
		Effect = 
		{
		},
		Animation = 2,
	},
	AlwaysShow = 
	{
		[412] = true,
		[413] = true,
		[414] = true,
		Immovable = true,
		Unstoppable = true,
		[402] = true,
		[403] = true,
		[404] = true,
		[405] = true,
		[406] = true,
		[407] = true,
		[408] = true,
		["Improvised Upgrades"] = true,
	},
	AdvancedContainers = 
	{
	},
	Performance = 
	{
		PriorityUpdate = false,
		PriorityStart = 0,
		GeneralDelay = 1,
		EffectAnchoring = 2,
		PriorityDelay = 0.5,
		MaximumUpdates = 1,
	},
	Offset = 
	{
		Y = -20,
		X = 0,
	},
	Layer = 1,
	AdvancedCompression = 
	{
	},
	Layouts = 
	{
	},
	MaximumThreshold = 60,
	Version = 9,
	Layout = 
	{
		StatusBar = 
		{
			Enabled = false,
			Scale = 1,
			Layer = 1,
			Width = 48,
			Y = 48,
			X = 0,
			Reverse = false,
			Height = 16,
			Background = 
			{
				TextureDimensions = 
				{
					Height = 128,
					Width = 128,
				},
				Color = 
				{
					B = 0,
					G = 0,
					R = 0,
				},
				Stretch = true,
				Alpha = 0.8,
				Texture = "EA_TintableImage",
			},
			Orientation = 1,
			Foreground = 
			{
				Type = 1,
				TextureDimensions = 
				{
					Height = 128,
					Width = 128,
				},
				Color = 
				{
					B = 255,
					G = 255,
					R = 255,
				},
				Stretch = true,
				Alpha = 1,
				Texture = "EA_TintableImage",
			},
		},
		Height = 41,
		Name = 
		{
			Scale = 1,
			Layer = 2,
			Alpha = 0,
			Width = 48,
			Y = 50,
			X = 79,
			Alignment = "leftcenter",
			Height = 16,
			Font = "font_clear_small",
			Color = 
			{
				B = 255,
				G = 255,
				R = 255,
			},
		},
		StackCount = 
		{
			Scale = 0.93000000715256,
			Layer = 2,
			Alpha = 1,
			Width = 45,
			Y = 2,
			X = 0,
			Alignment = "rightcenter",
			Height = 16,
			Font = "font_clear_small_bold",
			Color = 
			{
				B = 255,
				G = 255,
				R = 255,
			},
		},
		Scale = 1,
		Duration = 
		{
			Scale = 0.85000002384186,
			Layer = 2,
			Alpha = 1,
			Width = 48,
			Y = 30,
			X = 0,
			Alignment = "center",
			Height = 16,
			Format = 3,
			Font = "font_clear_small_bold",
			Color = 
			{
				B = 255,
				G = 255,
				R = 255,
			},
		},
		Icon = 
		{
			Y = 0,
			X = 0,
			Scale = 0.86000001430511,
			Border = 
			{
				Color = 
				{
					B = 0,
					G = 0,
					R = 0,
				},
				Type = 1,
				Alpha = 1,
			},
			Height = 48,
			Layer = 1,
			Alpha = 1,
			Width = 48,
		},
		Width = 41,
	},
	Sync = 
	{
		Enable = false,
		ResyncEvery = 0,
	},
	Containers = 
	{
		AlwaysShow = 
		{
			Anchor = 1,
			Placement = 1,
			Size = 
			{
				Columns = 10,
				Rows = 1,
			},
		},
		Buffs = 
		{
			Anchor = 2,
			Placement = 1,
			Size = 
			{
				Columns = 10,
				Rows = 1,
			},
		},
		Padding = 
		{
			Y = 5,
			X = 0,
		},
		Debuffs = 
		{
			Anchor = 3,
			Placement = 1,
			Size = 
			{
				Columns = 10,
				Rows = 1,
			},
		},
	},
	AlwaysIgnore = 
	{
	},
	Sorting = 
	{
		Enabled = true,
		Type = 1,
		Direction = 2,
	},
}



