WarBoardSettings = 
{
	DefaultBoard = "Top",
	Top = 
	{
		RowAlign = 
		{
			2,
			2,
			2,
			2,
		},
		ModRows = 
		{
			[1] = 
			{
				"WarBoard_TogglerPhantom",
				"WarBoard_TogglerAggroMeter",
				"WarBoard_TogglerRes",
				"WarBoard_TogglerWARCommander",
				"WarBoard_TogglerQQueuer",
				"WarBoard_WarWhisperer",
				"WarBoard_ZonePOP",
				"WarBoard_XP",
				"WarBoard_RR",
				"WarBoard_Menu",
				"WarBoard_Coin",
				"WarBoard_Friend",
				"WarBoard_GuildF",
				"WarBoard_Alliance",
				"WarBoard_FPS",
				"WarBoard_Clock",
				"WarBoard_TogglerEnemy",
			},
		},
		Mods = 
		{
			Red = 0,
			Blue = 0,
			Alpha = 0.125,
			Green = 0,
		},
		Board = 
		{
			Enabled = true,
			ButtonsOn = true,
			Blue = 0,
			Green = 0,
			HorizSpace = 10,
			VertSpace = 5,
			Alpha = 0.375,
			Red = 0,
		},
	},
	Version = "0.5.2",
	Bottom = 
	{
		RowAlign = 
		{
			2,
			2,
			2,
			2,
		},
		ModRows = 
		{
			[1] = 
			{
			},
		},
		Mods = 
		{
			Red = 0,
			Blue = 0,
			Alpha = 0.5,
			Green = 0,
		},
		Board = 
		{
			Enabled = false,
			ButtonsOn = false,
			Blue = 0,
			Green = 0,
			HorizSpace = 10,
			VertSpace = 5,
			Alpha = 0.5,
			Red = 0,
		},
	},
}



