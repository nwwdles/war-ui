TurretRange.Settings = 
{
	General = 
	{
		Enabled = true,
		UpdateDelay = 0.01,
	},
	Position = 
	{
		Scale = 1.2577780768985,
		X = 1372.0003720238,
		Y = 651.00004650298,
	},
	Display = 
	{
		Circle = 
		{
			Type = 2,
			Mode = 2,
			Inverted = false,
		},
		Distance = 
		{
			Offset = 
			{
				Y = 0,
				X = 0,
			},
			Type = 2,
			Scale = 1,
			Layout = 1,
			Font = "font_clear_small_bold",
		},
		Graphic = 
		{
			Limit = 25,
			Type = 2,
		},
	},
	Distance = 
	{
		
		{
			Color = 
			{
				R = 255,
				G = 0,
				B = 0,
			},
			Distance = 25,
		},
		
		{
			Color = 
			{
				R = 255,
				G = 255,
				B = 0,
			},
			Distance = 25,
		},
		
		{
			Color = 
			{
				R = 0,
				G = 255,
				B = 0,
			},
			Distance = 0,
		},
	},
	Version = 1,
}



