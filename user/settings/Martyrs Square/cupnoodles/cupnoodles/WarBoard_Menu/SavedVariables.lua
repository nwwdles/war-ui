WarBoard_Menu_ElementTable = 
{
	
	{
		openingwindow = "AbilitiesWindow",
		name = "Abilities",
		position = 1,
		shown = true,
		friendlytext = "Abilities",
		template = "Spellbook",
	},
	
	{
		openingwindow = "EA_Window_Backpack",
		name = "BackPack",
		position = 2,
		shown = true,
		friendlytext = "Backpack",
		template = "Bag",
	},
	
	{
		openingwindow = "CharacterWindow",
		highlighticon = "PaperDoll-Button-Selected-Highlighted",
		name = "Character",
		position = 3,
		shown = true,
		friendlytext = "Character",
		template = "PaperDoll",
	},
	
	{
		openingwindow = "TomeWindow",
		name = "ToK",
		position = 4,
		shown = true,
		friendlytext = "Tome of Knowledge",
		template = "Tome",
	},
	
	{
		openingwindow = "MainMenuWindow",
		name = "MainMenu",
		position = 5,
		shown = true,
		friendlytext = "Main Menu",
		template = "MainMenu",
	},
	
	{
		openingwindow = "EA_Window_OpenParty",
		name = "OpenParty",
		position = 6,
		shown = true,
		friendlytext = "Parties and Warbands",
		template = "Scenario-Grouping",
	},
	
	{
		openingwindow = "SettingsWindowTabbed",
		name = "CustomizeInterface",
		position = 7,
		shown = true,
		friendlytext = "Customize UI",
		template = "CustomizeInterface",
	},
	
	{
		openingwindow = "GuildWindow",
		name = "Guild",
		position = 8,
		shown = true,
		friendlytext = "Guild",
		template = "Guild",
	},
	
	{
		openingwindow = "EA_Window_Help",
		name = "HelpElement",
		position = 9,
		shown = false,
		friendlytext = "Help",
		template = "Help",
	},
}



